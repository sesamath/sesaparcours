export default {
  /* Dans toutes les consignes de ce type d’exercices, £a permet de déclarer la variable a. Il faut alors préciser l’intervalle dans lequel se trouve cette valeur
    Les consignes seront le plus souvent sur plusieurs lignes, d’où l’idée de créer un tableau
    la propriété variables de chaque énoncé est un tableau où chaque élément est un tableau de deux ou trois éléments
    Par exemple ['a','[5;15]'] signifie que a sera un entier de l’intervalle [5;15]
    ['a','[5;15]',100] signifie que a prend une valeur décimale de [0,05;0,15] avec 2 chiffres après la virgule
    */
  sujets: [
    {
      nomSujet: 'Métropole juin 2015 ES', // numéro 1
      consignes: ['Le service marketing d’un magasin de téléphonie a procédé à une étude du comportement de sa clientèle. Il a ainsi observé que celle-ci est composée de £a% de femmes, £b% des femmes qui entrent dans le magasin y effectuent un achat, alors que cette proportion est de £c% pour les hommes.',
        'Une personne entre dans le magasin. On note :',
        '- $F$ l’évènement : «La personne est une femme»;',
        '- $A$ l’évènement : «La personne repart en ayant effectué un achat».',
        'On choisit une personne qui entre dans le magasin.'],
      variables: [['a', '[36;48]'], ['b', '[30;38]'], ['c', '[52;58]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['F', 'A'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['F', '£a/100', [['A', '£b/100'], ['\\overline{A}', '1-£b/100']]],
        ['\\overline{F}', '1-£a/100', [['A', '£c/100'], ['\\overline{A}', '1-£c/100']]]],
      prbEnonce: ['£a/100', '£b/100', '£c/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(F)=£a$, $P_{F}(A)=£b$ et $P_{\\overline{F}}(A)=£c$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que la personne soit une femme qui effectue un achat vaut &1&.',
        'La probabilité que la personne soit un homme qui effectue un achat vaut &1&.'],
      ['La probabilité que la personne soit une femme qui n’effectue aucun achat vaut &1&.',
        'La probabilité que la personne soit un homme qui n’effectue aucun achat vaut &1&.']],
      questionInter: [['Quelle est la probabilité que la personne soit une femme qui effectue un achat?',
        'Quelle est la probabilité que la personne soit un homme qui effectue un achat?'],
      ['Quelle est la probabilité que la personne soit une femme qui n’effectue aucun achat?',
        'Quelle est la probabilité que la personne soit un homme qui n’effectue aucun achat?']],
      questionInterFin: [['P(F\\cap A)', 'P(\\overline{F}\\cap A)'],
        ['P(F\\cap \\overline{A})', 'P(\\overline{F}\\cap \\overline{A})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['£a*£b/10000', '(100-£a)*£c/10000'], ['£a*(100-£b)/10000', '(100-£a)*(100-£c)/10000']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(F\\cap A)=P(F)\\times P_{F}(A)=calcule[£a/100]\\times calcule[£b/100]=calcule[£a*£b/10000]$',
        '$P(\\overline{F}\\cap A)=P(\\overline{F})\\times P_{\\overline{F}}(A)=calcule[(100-£a)/100]\\times calcule[£c/100]=calcule[(100-£a)*£c/10000]$'],
      ['$P(F\\cap \\overline{A})=P(F)\\times P_{F}(\\overline{A})=calcule[£a/100]\\times calcule[(100-£b)/100]=calcule[£a*(100-£b)/10000]$',
        '$P(\\overline{F}\\cap \\overline{A})=P(\\overline{F})\\times P_{\\overline{F}}(\\overline{A})=calcule[(100-£a)/100]\\times calcule[(100-£c)/100]=calcule[(100-£a)*(100-£c)/10000]$']],

      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que la personne effectue un achat vaut &1&.',
        'La probabilité que la personne reparte sans rien acheter vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que la personne effectue un achat?',
        'Quelle est la probabilité que la personne reparte sans rien acheter?'],
      questionPrbTotaleFin: ['P(A)', 'P(\\overline{A})'],
      // réponses associées
      probaTotales: ['(£a*£b+(100-£a)*£c)/10000', '(£a*(100-£b)+(100-£a)*(100-£c))/10000'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(A)=P(F\\cap A)+P(\\overline{F}\\cap A)$<br>$P(A)=calcule[£a/100]\\times calcule[£b/100]+calcule[(100-£a)/100]\\times calcule[£c/100]=calcule[(£a*£b+(100-£a)*£c)/10000]$',
        '$P(\\overline{A})=P(F\\cap \\overline{A})+P(\\overline{F}\\cap \\overline{A})$<br>$P(\\overline{A})=calcule[£a/100]\\times calcule[(100-£b)/100]+calcule[(100-£a)/100]\\times calcule[(100-£c)/100]=calcule[(£a*(100-£b)+(100-£a)*(100-£c))/10000]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['La personne a effectué un achat dans ce magasin, la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit une femme vaut &1&.',
        'La personne a effectué un achat dans ce magasin, la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit un homme vaut &1&.'],
      ['La personne repart du magasin sans rien acheter, la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit une femme vaut &1&.',
        'La personne repart du magasin sans rien acheter, la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit un homme vaut &1&.']],
      questionPrbSachant1: [['Sachant que la personne a effectué un achat dans ce magasin, la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit une femme vaut &1&.',
        'Sachant que la personne a effectué un achat dans ce magasin, la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit un homme vaut &1&.'],
      ['Sachant que la personne repart du magasin sans rien acheter, la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit une femme vaut &1&.',
        'Sachant que la personne repart du magasin sans rien acheter, la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit un homme vaut &1&.']],
      questionPrbCond: [['La personne a effectué un achat dans ce magasin. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit une femme?',
        'La personne a effectué un achat dans ce magasin. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit un homme?'],
      ['La personne repart du magasin sans rien acheter. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit une femme?',
        'La personne repart du magasin sans rien acheter. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit un homme?']],
      questionPrbSachant: [['Sachant que la personne a effectué un achat dans ce magasin, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit une femme?',
        'Sachant que la personne a effectué un achat dans ce magasin, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit un homme?'],
      ['Sachant que la personne repart du magasin sans rien acheter, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit une femme?',
        'Sachant que la personne repart du magasin sans rien acheter, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que ce soit un homme?']],
      questionPrbSachantFin: [['$P_{A}(F)$', '$P_{A}(\\overline{F})$'], ['$P_{\\overline{A}}(F)$', '$P_{\\overline{A}}(\\overline{F})$']],
      probaCond: [['£a*£b/(£a*£b+(100-£a)*£c)', '(100-£a)*£c/(£a*£b+(100-£a)*£c)'], ['£a*(100-£b)/(£a*(100-£b)+(100-£a)*(100-£c))', '(100-£a)*(100-£c)/(£a*(100-£b)+(100-£a)*(100-£c))']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{A}(F)=\\frac{P(A\\cap F)}{P(A)}=\\frac{calcule[£a*£b/10000]}{calcule[(£a*£b+(100-£a)*£c)/10000]}£s £r$',
        '$P_{A}(\\overline{F})=\\frac{P(A\\cap \\overline{F})}{P(A)}=\\frac{calcule[(100-£a)*£c/10000]}{calcule[(£a*£b+(100-£a)*£c)/10000]}£s £r$'],
      ['$P_{\\overline{A}}(F)=\\frac{P(\\overline{A}\\cap F)}{P(\\overline{A})}=\\frac{calcule[£a*(100-£b)/10000]}{calcule[(£a*(100-£b)+(100-£a)*(100-£c))/10000]}£s £r$',
        '$P_{\\overline{A}}(\\overline{F})=\\frac{P(\\overline{A}\\cap \\overline{F})}{P(\\overline{A})}=\\frac{calcule[(100-£a)*(100-£c)/10000]}{calcule[(£a*(100-£b)+(100-£a)*(100-£c))/10000]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat
    },
    {

      nomSujet: 'Pondichéry avril 2016 STMG', // numéro 2
      consignes: ['Dans le cadre d’une campagne de sensibilisation au tri des ordures ménagères, une enquête a été menée auprès de 1500 habitants d’une ville dans laquelle les moins de 35 ans représentent £a% et les plus de 50 ans, £b%.',
        'A la question : «Triez-vous le papier ?»,',
        '£c% des moins de 35 ans ont répondu «oui»,',
        '£d% des personnes âgés de 35 à 50 ans ont répondu «oui»,',
        '£e% des personnes de plus de 50 ans ont répondu «oui».',
        'On interroge au hasard une personne parmi celles qui ont répondu à cette enquête. On considère les évènements suivants :',
        '- $J$ : «la personne interrogée a moins de 35 ans»;',
        '- $M$ : «la personne interrogée a un âge compris entre 35 et 50 ans»;',
        '- $S$ : «la personne interrogée a plus de 50 ans»;',
        '- $T$ : «la personne interrogée trie le papier».'],
      variables: [['a', '[21;27]'], ['b', '[32;41]'], ['c', '[75;85]'], ['d', '[67;73]'], ['e', '[58;62]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['J', 'M', 'S', 'T'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['J', '£a/100', [['T', '£c/100'], ['\\overline{T}', '1-£c/100']]],
        ['M', '1-£a/100-£b/100', [['T', '£d/100'], ['\\overline{T}', '1-£d/100']]],
        ['S', '£b/100', [['T', '£e/100'], ['\\overline{T}', '1-£e/100']]]],
      prbEnonce: ['£a/100', '£b/100', '£c/100', '£d/100', '£e/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(J)=£a$, $P(S)=£b$, $P_{J}(T)=£c$, $P_{M}(T)=£d$ et $P_{S}(T)=£e$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que la personne ait moins de 35 ans et qu’elle trie le papier vaut &1&.',
        'La probabilité que la personne ait entre 35 et 50 ans et qu’elle trie le papier vaut &1&.',
        'La probabilité que la personne ait plus de 50 ans et qu’elle trie le papier vaut &1&.'],
      ['La probabilité que la personne ait moins de 35 ans et qu’elle ne trie pas le papier vaut &1&.',
        'La probabilité que la personne ait entre 35 et 50 ans et qu’elle ne trie pas le papier vaut &1&.',
        'La probabilité que la personne ait plus de 50 ans et qu’elle ne trie pas le papier vaut &1&.']],
      questionInter: [['Quelle est la probabilité que la personne ait moins de 35 ans et qu’elle trie le papier?',
        'Quelle est la probabilité que la personne ait entre 35 et 50 ans et qu’elle trie le papier?',
        'Quelle est la probabilité que la personne ait plus de 50 ans et qu’elle trie le papier?'],
      ['Quelle est la probabilité que la personne ait moins de 35 ans et qu’elle ne trie pas le papier?',
        'Quelle est la probabilité que la personne ait entre 35 et 50 ans et qu’elle ne trie pas le papier?',
        'Quelle est la probabilité que la personne ait plus de 50 ans et qu’elle ne trie pas le papier?']],
      questionInterFin: [['P(J\\cap T)', 'P(M\\cap T)', 'P(S\\cap T)'],
        ['P(J\\cap \\overline{T})', 'P(M\\cap \\overline{T})', 'P(S\\cap \\overline{T})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['£a*£c/10000', '(100-£a-£b)*£d/10000', '£b*£e/10000'], ['£a*(100-£c)/10000', '(100-£a-£b)*(100-£d)/10000', '£b*(100-£e)/10000']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(J\\cap T)=P(J)\\times P_{J}(T)=calcule[£a/100]\\times calcule[£c/100]=calcule[£a*£c/10000]$',
        '$P(M\\cap T)=P(M)\\times P_{M}(T)=calcule[(100-£a-£b)/100]\\times calcule[£d/100]=calcule[(100-£a-£b)*£d/10000]$',
        '$P(S\\cap T)=P(S)\\times P_{S}(T)=calcule[£b/100]\\times calcule[£e/100]=calcule[£b*£e/10000]$'],
      ['$P(J\\cap \\overline{T})=P(J)\\times P_{J}(\\overline{T})=calcule[£a/100]\\times calcule[(100-£c)/100]=calcule[£a*(100-£c)/10000]$',
        '$P(M\\cap \\overline{T})=P(M)\\times P_{M}(\\overline{T})=calcule[(100-£a-£b)/100]\\times calcule[(100-£d)/100]=calcule[(100-£a-£b)*(100-£d)/10000]$',
        '$P(S\\cap \\overline{T})=P(S)\\times P_{S}(\\overline{T})=calcule[£b/100]\\times calcule[(100-£e)/100]=calcule[£b*(100-£e)/10000]$']],

      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que la personne trie le papier vaut &1&.',
        'La probabilité que la personne ne trie pas le papier vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que la personne trie le papier?',
        'Quelle est la probabilité que la personne ne tie pas le papier?'],
      questionPrbTotaleFin: ['P(T)', 'P(\\overline{T})'],
      // réponses associées
      probaTotales: ['(£a*£c+(100-£a-£b)*£d+£b*£e)/10000', '(£a*(100-£c)+(100-£a-£b)*(100-£d)+£b*(100-£e))/10000'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(T)=P(J\\cap T)+P(M\\cap T)+P(S\\cap T)$<br>$P(T)=calcule[£a/100]\\times calcule[£c/100]+calcule[(100-£a-£b)/100]\\times calcule[£d/100]+calcule[£b/100]\\times calcule[£e/100]=calcule[(£a*£c+(100-£a-£b)*£d+£b*£e)/10000]$',
        '$P(\\overline{T})=P(J\\cap \\overline{T})+P(M\\cap \\overline{T})+P(S\\cap \\overline{T})$<br>$P(\\overline{T})=calcule[£a/100]\\times calcule[(100-£c)/100]+calcule[(100-£a-£b)/100]\\times calcule[(100-£d)/100]+calcule[£b/100]\\times calcule[(100-£e)/100]=calcule[(£a*(100-£c)+(100-£a-£b)*(100-£d)+£b*(100-£e))/10000]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['La personne trie le papier. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait moins de 35 ans vaut &1&.',
        'La personne trie le papier. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait entre 35 et 50 ans vaut &1&.',
        'La personne trie le papier. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait plus de 50 ans vaut &1&.'],
      ['La personne ne trie pas le papier. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait moins de 35 ans vaut &1&.',
        'La personne ne trie pas le papier. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait entre 35 et 50 ans vaut &1&.',
        'La personne ne trie pas le papier. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait plus de 50 ans vaut &1&.']],
      questionPrbSachant1: [['Sachant que la personne trie le papier, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait moins de 35 ans vaut &1&.',
        'Sachant que la personne trie le papier, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait entre 35 et 50 ans vaut &1&.',
        'Sachant que la personne trie le papier, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait plus de 50 ans vaut &1&.'],
      ['Sachant que la personne ne trie pas le papier, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait moins de 35 ans vaut &1&.',
        'Sachant que la personne ne trie pas le papier, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait entre 35 et 50 ans vaut &1&.',
        'Sachant que la personne ne trie pas le papier, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait plus de 50 ans vaut &1&.']],
      questionPrbCond: [['La personne trie le papier. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait moins de 35 ans?',
        'La personne trie le papier. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait entre 35 et 50 ans?',
        'La personne trie le papier. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait plus de 50 ans?'],
      ['La personne ne trie pas le papier. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait moins de 35 ans?',
        'La personne ne trie pas le papier. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait entre 35 et 50 ans?',
        'La personne ne trie pas le papier. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait plus de 50 ans?']],
      questionPrbSachant: [['Sachant que la personne trie le papier, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait moins de 35 ans?',
        'Sachant que la personne trie le papier, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait entre 35 et 50 ans?',
        'Sachant que la personne trie le papier, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait plus de 50 ans?'],
      ['Sachant que la personne ne trie pas le papier, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait moins de 35 ans?',
        'Sachant que la personne ne trie pas le papier, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait entre 35 et 50 ans?',
        'Sachant que la personne ne trie pas le papier, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ait plus de 50 ans?']],
      questionPrbSachantFin: [['$P_{T}(J)$', '$P_{T}(M)$', '$P_{T}(S)$'], ['$P_{\\overline{T}}(J)$', '$P_{\\overline{T}}(M)$', '$P_{\\overline{T}}(S)$']],
      probaCond: [['£a*£c/(£a*£c+(100-£a-£b)*£d+£b*£e)', '(100-£a-£b)*£d/(£a*£c+(100-£a-£b)*£d+£b*£e)', '£b*£e/(£a*£c+(100-£a-£b)*£d+£b*£e)'], ['£a*(100-£c)/(£a*(100-£c)+(100-£a-£b)*(100-£d)+£b*(100-£e))', '(100-£a-£b)*(100-£d)/(£a*(100-£c)+(100-£a-£b)*(100-£d)+£b*(100-£e))', '£b*(100-£e)/(£a*(100-£c)+(100-£a-£b)*(100-£d)+£b*(100-£e))']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{T}(J)=\\frac{P(T\\cap J)}{P(T)}=\\frac{calcule[£a*£c/10000]}{calcule[(£a*£c+(100-£a-£b)*£d+£b*£e)/10000]}£s £r$',
        '$P_{T}(M)=\\frac{P(T\\cap M)}{P(T)}=\\frac{calcule[(100-£a-£b)*£d/10000]}{calcule[(£a*£c+(100-£a-£b)*£d+£b*£e)/10000]}£s £r$',
        '$P_{T}(S)=\\frac{P(T\\cap S)}{P(T)}=\\frac{calcule[£b*£e/10000]}{calcule[(£a*£c+(100-£a-£b)*£d+£b*£e)/10000]}£s £r$'],
      ['$P_{\\overline{T}}(J)=\\frac{P(\\overline{T}\\cap J)}{P(\\overline{T})}=\\frac{calcule[£a*(100-£c)/10000]}{calcule[(£a*(100-£c)+(100-£a-£b)*(100-£d)+£b*(100-£e))/10000]}£s £r$',
        '$P_{\\overline{T}}(M)=\\frac{P(\\overline{T}\\cap M)}{P(\\overline{T})}=\\frac{calcule[(100-£a-£b)*(100-£d)/10000]}{calcule[(£a*(100-£c)+(100-£a-£b)*(100-£d)+£b*(100-£e))/10000]}£s £r$',
        '$P_{\\overline{T}}(S)=\\frac{P(\\overline{T}\\cap S)}{P(\\overline{T})}=\\frac{calcule[£b*(100-£e)/10000]}{calcule[(£a*(100-£c)+(100-£a-£b)*(100-£d)+£b*(100-£e))/10000]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat
    },
    {

      nomSujet: 'Amérique du Nord juin 2015 S', // numéro 3
      consignes: ['Une entreprise fabrique des tablettes de chocolat. Un service contrôle la qualité des fèves de cacao livrées par les producteurs. Un des critères de qualité est le taux d’humidité qui doit être de 7%. On dit alors que la fève est conforme.',
        'L’entreprise a trois fournisseurs différents :',
        'le premier fournisseur procure la moitié du stock de fèves, le deuxième £a% et le dernier apporte calcule[50-£a]% du stock.',
        'Pour le premier, £b% de sa production respecte le taux d’humidité ; pour le deuxième, qui est un peu moins cher, £c% de sa production est conforme, et le troisième fournit £d% de fèves non conformes.',
        'On choisit au hasard une fève dans le stock reçu. On note $F_i$ l’évènement «la fève provient du fournisseur $i$», pour $i$ prenant les valeurs 1, 2 ou 3, et $C$ l’évènement «la fève est conforme».'],
      variables: [['a', '[25;35]'], ['b', '[95;98]'], ['c', '[85;91]'], ['d', '[18;25]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['F_1', 'F_2', 'F_3', 'C'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['F_1', '0.5', [['C', '£b/100'], ['\\overline{C}', '1-£b/100']]],
        ['F_2', '£a/100', [['C', '£c/100'], ['\\overline{C}', '1-£c/100']]],
        ['F_3', '0.5-£a/100', [['C', '1-£d/100'], ['\\overline{C}', '£d/100']]]],
      prbEnonce: ['\\frac{1}{2}', '£a/100', '0.5-£a/100', '£b/100', '£c/100', '£d/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(F_1)=£a$, $P(F_2)=£b$, $P(F_3)=£c$, $P_{F_1}(C)=£d$, $P_{F_2}(C)=£e$ et $P_{F_3}(\\overline{C})=£f$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que la fève provienne du premier fournisseur et soit conforme vaut &1&.',
        'La probabilité que la fève provienne du deuxième fournisseur et soit conforme vaut &1&.',
        'La probabilité que la fève provienne du troisième fournisseur et soit conforme vaut &1&.'],
      ['La probabilité que la fève provienne du premier fournisseur et ne soit pas conforme vaut &1&.',
        'La probabilité que la fève provienne du deuxième fournisseur et ne soit pas conforme vaut &1&.',
        'La probabilité que la fève provienne du troisième fournisseur et ne soit pas conforme vaut &1&.']],
      questionInter: [['Quelle est la probabilité que la fève provienne du premier fournisseur et soit conforme?',
        'Quelle est la probabilité que la fève provienne du deuxième fournisseur et soit conforme?',
        'Quelle est la probabilité que la fève provienne du troisième fournisseur et soit conforme?'],
      ['Quelle est la probabilité que la fève provienne du premier fournisseur et ne soit pas conforme?',
        'Quelle est la probabilité que la fève provienne du deuxième fournisseur et ne soit pas conforme?',
        'Quelle est la probabilité que la fève provienne du troisième fournisseur et ne soit pas conforme?']],
      questionInterFin: [['P(F_1\\cap C)', 'P(F_2\\cap C)', 'P(F_3\\cap C)'],
        ['P(F_1\\cap \\overline{C})', 'P(F_2\\cap \\overline{C})', 'P(F_3\\cap \\overline{C})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['50*£b/10000', '£a*£c/10000', '(50-£a)*(100-£d)/10000'], ['50*(100-£b)/10000', '£a*(100-£c)/10000', '(50-£a)*£d/10000']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(F_1\\cap C)=P(F_1)\\times P_{F_1}(C)=0,5\\times calcule[£b/100]=calcule[50*£c/10000]$',
        '$P(F_2\\cap C)=P(F_2)\\times P_{F_2}(C)=calcule[£a/100]\\times calcule[£c/100]=calcule[£a*£c/10000]$',
        '$P(F_3\\cap C)=P(F_3)\\times P_{F_3}(C)=calcule[(50-£a)/100]\\times calcule[(100-£d)/100]=calcule[(50-£a)*(100-£d)/10000]$'],
      ['$P(F_1\\cap \\overline{C})=P(F_1)\\times P_{F_1}(\\overline{C})=0,5\\times calcule[(100-£b)/100]=calcule[50*(100-£b)/10000]$',
        '$P(F_2\\cap \\overline{C})=P(F_2)\\times P_{F_2}(\\overline{C})=calcule[£a/100]\\times calcule[(100-£c)/100]=calcule[£a*(100-£c)/10000]$',
        '$P(F_3\\cap \\overline{C})=P(F_3)\\times P_{F_3}(\\overline{C})=calcule[(50-£a)/100]\\times calcule[£d/100]=calcule[(50-£a)*£d/10000]$']],

      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que la fève soit conforme vaut &1&.',
        'La probabilité que la fève ne soit pas conforme vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que la fève soit conforme?',
        'Quelle est la probabilité que la fève ne soit pas conforme?'],
      questionPrbTotaleFin: ['P(C)', 'P(\\overline{C})'],
      // réponses associées
      probaTotales: ['(50*£b+£a*£c+(50-£a)*(100-£d))/10000', '(50*(100-£b)+£a*(100-£c)+(50-£a)*£d)/10000'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(C)=P(F_1\\cap C)+P(F_2\\cap C)+P(F_3\\cap C)$<br>$P(C)=0,5\\times calcule[£b/100]+calcule[£a/100]\\times calcule[£c/100]+calcule[(50-£a)/100]\\times calcule[(100-£d)/100]=calcule[(50*£b+£a*£c+(50-£a)*(100-£d))/10000]$',
        '$P(\\overline{C})=P(F_1\\cap \\overline{C})+P(F_2\\cap \\overline{C})+P(F_3\\cap \\overline{C})$<br>$P(\\overline{C})=0,5\\times calcule[(100-£b)/100]+calcule[£a/100]\\times calcule[(100-£c)/100]+calcule[(50-£a)/100]\\times calcule[£d/100]=calcule[(50*(100-£b)+£a*(100-£c)+(50-£a)*£d)/10000]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['La fève est conforme. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du premier fournisseur vaut &1&.',
        'La fève est conforme. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du deuxième fournisseur vaut &1&.',
        'La fève est conforme. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du troisième fournisseur vaut &1&.'],
      ['La fève est non conforme. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du premier fournisseur vaut &1&.',
        'La fève est non conforme. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du deuxième fournisseur vaut &1&.',
        'La fève est non conforme. La probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du troisième fournisseur vaut &1&.']],
      questionPrbSachant1: [['Sachant que la fève est conforme, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du premier fournisseur vaut &1&.',
        'Sachant que la fève est conforme, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du deuxième fournisseur vaut &1&.',
        'Sachant que la fève est conforme, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du troisième fournisseur vaut &1&.'],
      ['Sachant que la fève est non conforme, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du premier fournisseur vaut &1&.',
        'Sachant que la fève est non conforme, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du deuxième fournisseur vaut &1&.',
        'Sachant que la fève est non conforme, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du troisième fournisseur vaut &1&.']],
      questionPrbCond: [['La fève est conforme. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du premier fournisseur?',
        'La fève est conforme. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du deuxième fournisseur?',
        'La fève est conforme. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du troisième fournisseur?'],
      ['La fève est non conforme. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du premier fournisseur?',
        'La fève est non conforme. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du deuxième fournisseur?',
        'La fève est non conforme. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du troisième fournisseur?']],
      questionPrbSachant: [['Sachant que la fève est conforme, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du premier fournisseur?',
        'Sachant que la fève est conforme, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du deuxième fournisseur?',
        'Sachant que la fève est conforme, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du troisième fournisseur?'],
      ['Sachant que la fève est non conforme, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du premier fournisseur?',
        'Sachant que la fève est non conforme, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du deuxième fournisseur?',
        'Sachant que la fève est non conforme, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne du troisième fournisseur?']],
      questionPrbSachantFin: [['$P_{C}(F_1)$', '$P_{C}(F_2)$', '$P_{C}(F_3)$'], ['$P_{\\overline{C}}(F_1)$', '$P_{\\overline{C}}(F_2)$', '$P_{\\overline{C}}(F_3)$']],
      probaCond: [['50*£b/(50*£b+£a*£c+(50-£a)*(100-£d))', '£a*£c/(50*£b+£a*£c+(50-£a)*(100-£d))', '(50-£a)*(100-£d)/(50*£b+£a*£c+(50-£a)*(100-£d))'], ['50*(100-£b)/(50*(100-£b)+£a*(100-£c)+(50-£a)*£d)', '£a*(100-£c)/(50*(100-£b)+£a*(100-£c)+(50-£a)*£d)', '(50-£a)*£d/(50*(100-£b)+£a*(100-£c)+(50-£a)*£d)']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{C}(F_1)=\\frac{P(C\\cap F_1)}{P(C)}=\\frac{calcule[50*£b/10000]}{calcule[(50*£b+£a*£c+(50-£a)*(100-£d))/10000]}£s £r$',
        '$P_{C}(F_2)=\\frac{P(C\\cap F_2)}{P(C)}=\\frac{calcule[£a*£c/10000]}{calcule[(50*£b+£a*£c+(50-£a)*(100-£d))/10000]}£s £r$',
        '$P_{C}(F_3)=\\frac{P(C\\cap F_3)}{P(C)}=\\frac{calcule[(50-£a)*(100-£d)/10000]}{calcule[(50*£b+£a*£c+(50-£a)*(100-£d))/10000]}£s £r$'],
      ['$P_{\\overline{C}}(F_1)=\\frac{P(\\overline{C}\\cap F_1)}{P(\\overline{C})}=\\frac{calcule[50*(100-£b)/10000]}{calcule[(50*(100-£b)+£a*(100-£c)+(50-£a)*£d)/10000]}£s £r$',
        '$P_{\\overline{C}}(F_2)=\\frac{P(\\overline{C}\\cap F_2)}{P(\\overline{C})}=\\frac{calcule[£a*(100-£c)/10000]}{calcule[(50*(100-£b)+£a*(100-£c)+(50-£a)*£d)/10000]}£s £r$',
        '$P_{\\overline{C}}(F_3)=\\frac{P(\\overline{C}\\cap F_3)}{P(\\overline{C})}=\\frac{calcule[(50-£a)*£d/10000]}{calcule[(50*(100-£b)+£a*(100-£c)+(50-£a)*£d)/10000]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat
    },
    {
      nomSujet: 'Polynésie septembre 2015 S', // numéro 4
      consignes: ['On étudie une maladie dans la population d’un pays. Pour dépister chez une personne cette maladie, on effectue une prise de sang.',
        'On considère que le dépistage est positif si le taux de la substance Gamma est supérieur ou égal à 45 ng.mL$^{−1}$.',
        'Une personne étant choisie au hasard dans la population, on appelle:',
        '- $M$ l’évènement «le patient est atteint par la maladie étudiée»;',
        '- $D$ l’évènement «le patient a un dépistage positif».',
        'On admet que :',
        ' £b% des personnes atteintes par la maladie étudiée ont un dépistage positif ;',
        ' £c% des personnes non atteintes par cette maladie ont un dépistage négatif.',
        'On sait de plus que £a% de la population étudiée est atteinte par cette maladie.'],
      variables: [['a', '[8;13]'], ['b', '[80;85]'], ['c', '[70;75]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['M', 'D'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['M', '£a/100', [['D', '£b/100'], ['\\overline{D}', '1-£b/100']]],
        ['\\overline{M}', '1-£a/100', [['D', '1-£c/100'], ['\\overline{D}', '£c/100']]]],
      prbEnonce: ['£a/100', '£b/100', '£c/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(M)=£a$, $P_{M}(D)=£b$ et $P_{\\overline{M}}(\\overline{D})=£c$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que la personne soit malade et ait un dépistage positif vaut &1&.',
        'La probabilité que la personne ne soit pas malade et ait un dépistage positif vaut &1&.'],
      ['La probabilité que la personne soit malade et ait un dépistage négatif vaut &1&.',
        'La probabilité que la personne ne soit pas malade et ait un dépistage négatif vaut &1&.']],
      questionInter: [['Quelle est la probabilité que la personne soit malade et ait un dépistage positif?',
        'Quelle est la probabilité que la personne ne soit pas malade et ait un dépistage positif?'],
      ['Quelle est la probabilité que la personne soit malade et ait un dépistage négatif?',
        'Quelle est la probabilité que la personne ne soit pas malade et ait un dépistage négatif?']],
      questionInterFin: [['P(M\\cap D)', 'P(\\overline{M}\\cap D)'],
        ['P(M\\cap \\overline{D})', 'P(\\overline{M}\\cap \\overline{D})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['£a*£b/10000', '(100-£a)*(100-£c)/10000'], ['£a*(100-£b)/10000', '(100-£a)*£c/10000']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(M\\cap D)=P(M)\\times P_{M}(D)=calcule[£a/100]\\times calcule[£b/100]=calcule[£a*£b/10000]$',
        '$P(\\overline{M}\\cap D)=P(\\overline{M})\\times P_{\\overline{M}}(D)=calcule[(100-£a)/100]\\times calcule[(100-£c)/100]=calcule[(100-£a)*(100-£c)/10000]$'],
      ['$P(M\\cap \\overline{D})=P(M)\\times P_{M}(\\overline{D})=calcule[£a/100]\\times calcule[(100-£b)/100]=calcule[£a*(100-£b)/10000]$',
        '$P(\\overline{M}\\cap \\overline{D})=P(\\overline{M})\\times P_{\\overline{M}}(\\overline{D})=calcule[(100-£a)/100]\\times calcule[£c/100]=calcule[(100-£a)*£c/10000]$']],

      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que la personne ait un dépistage positif vaut &1&.',
        'La probabilité que la personne ait un dépistage négatif vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que la personne ait un dépistage positif?',
        'Quelle est la probabilité que la personne ait un dépistage négatif?'],
      questionPrbTotaleFin: ['P(D)', 'P(\\overline{D})'],
      // réponses associées
      probaTotales: ['(£a*£b+(100-£a)*(100-£c))/10000', '(£a*(100-£b)+(100-£a)*£c)/10000'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(D)=P(M\\cap D)+P(\\overline{M}\\cap D)$<br>$P(A)=calcule[£a/100]\\times calcule[£b/100]+calcule[(100-£a)/100]\\times calcule[(100-£c)/100]=calcule[(£a*£b+(100-£a)*(100-£c))/10000]$',
        '$P(\\overline{D})=P(M\\cap \\overline{D})+P(\\overline{M}\\cap \\overline{D})$<br>$P(\\overline{A})=calcule[£a/100]\\times calcule[(100-£b)/100]+calcule[(100-£a)/100]\\times calcule[£c/100]=calcule[(£a*(100-£b)+(100-£a)*£c)/10000]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['La personne a un dépistage positif, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle soit malade vaut &1&.',
        'La personne a un dépistage positif, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne soit pas malade vaut &1&.'],
      ['La personne a un dépistage négatif, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle soit malade vaut &1&.',
        'La personne a un dépistage négatif, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne soit pas malade vaut &1&.']],
      questionPrbSachant1: [['Sachant que la personne a un dépistage positif, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle soit malade vaut &1&.',
        'Sachant que la personne a un dépistage positif, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne soit pas malade vaut &1&.'],
      ['Sachant que la personne a un dépistage négatif, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle soit malade vaut &1&.',
        'Sachant que la personne a un dépistage négatif, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne soit pas malade vaut &1&.']],
      questionPrbCond: [['La personne a un dépistage positif. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle soit malade?',
        'La personne a un dépistage positif. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne soit pas malade?'],
      ['La personne a un dépistage négatif. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle soit malade?',
        'La personne a un dépistage négatif. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne soit pas malade?']],
      questionPrbSachant: [['Sachant que la personne a un dépistage positif, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle soit malade?',
        'Sachant que la personne a un dépistage positif, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne soit pas malade?'],
      ['Sachant que la personne a un dépistage négatif, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle soit malade?',
        'Sachant que la personne a un dépistage négatif, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne soit pas malade?']],
      questionPrbSachantFin: [['$P_{D}(M)$', '$P_{D}(\\overline{M})$'], ['$P_{\\overline{D}}(M)$', '$P_{\\overline{D}}(\\overline{M})$']],
      probaCond: [['£a*£b/(£a*£b+(100-£a)*(100-£c))', '(100-£a)*(100-£c)/(£a*£b+(100-£a)*(100-£c))'], ['£a*(100-£b)/(£a*(100-£b)+(100-£a)*£c)', '(100-£a)*£c/(£a*(100-£b)+(100-£a)*£c)']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{D}(M)=\\frac{P(D\\cap M)}{P(D)}=\\frac{calcule[£a*£b/10000]}{calcule[(£a*£b+(100-£a)*(100-£c))/10000]}£s £r$',
        '$P_{D}(\\overline{M})=\\frac{P(D\\cap \\overline{M})}{P(D)}=\\frac{calcule[(100-£a)*(100-£c)/10000]}{calcule[(£a*£b+(100-£a)*(100-£c))/10000]}£s £r$'],
      ['$P_{\\overline{D}}(M)=\\frac{P(\\overline{D}\\cap M)}{P(\\overline{D})}=\\frac{calcule[£a*(100-£b)/10000]}{calcule[(£a*(100-£b)+(100-£a)*£c)/10000]}£s £r$',
        '$P_{\\overline{D}}(\\overline{M})=\\frac{P(\\overline{D}\\cap \\overline{M})}{P(\\overline{D})}=\\frac{calcule[(100-£a)*£c/10000]}{calcule[(£a*(100-£b)+(100-£a)*£c)/10000]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat
    },
    {
      nomSujet: 'Pondichéry avril 2015 ES', // numéro 5
      consignes: ['L’entreprise MICRO vend en ligne du matériel informatique notamment des ordinateurs portables.',
        'Durant la période de garantie, les deux problèmes les plus fréquemment relevés par le service après-vente portent sur la batterie et sur le disque dur, ainsi :',
        '- parmi les ordinateurs vendus, £a% ont été retournés pour un défaut de batterie et parmi ceux-ci, £b% ont aussi un disque dur défectueux;',
        '- parmi les ordinateurs dont la batterie fonctionne correctement, £c% ont un disque dur défectueux.',
        'On suppose que la société MICRO garde constant le niveau de qualité de ses produits. Suite à l’achat en ligne d’un ordinateur, on note :',
        '- $B$ l’événement : «l’ordinateur a un défaut de batterie»;',
        '- $D$ l’événement : «l’ordinateur a un défaut de disque dur».'],
      variables: [['a', '[5;8]'], ['b', '[2;4]'], ['c', '[5;8]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['B', 'D'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['B', '£a/100', [['D', '£b/100'], ['\\overline{D}', '1-£b/100']]],
        ['\\overline{B}', '1-£a/100', [['D', '£c/100'], ['\\overline{D}', '1-£c/100']]]],
      prbEnonce: ['£a/100', '£b/100', '£c/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(B)=£a$, $P_{B}(D)=£b$ et $P_{\\overline{B}}(D)=£c$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que l’ordinateur acheté ait un problème de batterie et de disque dur vaut &1&.',
        'La probabilité que l’ordinateur acheté ait un problème de disque dur mais pas de batterie vaut &1&.'],
      ['La probabilité que l’ordinateur acheté ait un problème de batterie mais pas de disque dur vaut &1&.',
        'La probabilité que l’ordinateur acheté n’ait ni problème de batterie ni problème de disque dur vaut &1&.']],
      questionInter: [['Quelle est la probabilité que l’ordinateur acheté ait un problème de batterie et de disque dur?',
        'Quelle est la probabilité que l’ordinateur acheté ait un problème de disque dur mais pas de batterie?'],
      ['Quelle est la probabilité que l’ordinateur acheté ait un problème de batterie mais pas de disque dur?',
        'Quelle est la probabilité que l’ordinateur acheté n’ait ni problème de batterie ni problème de disque dur?']],
      questionInterFin: [['P(B\\cap D)', 'P(\\overline{B}\\cap D)'],
        ['P(B\\cap \\overline{D})', 'P(\\overline{B}\\cap \\overline{D})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['£a*£b/10000', '(100-£a)*£c/10000'], ['£a*(100-£b)/10000', '(100-£a)*(100-£c)/10000']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(B\\cap D)=P(B)\\times P_{B}(D)=calcule[£a/100]\\times calcule[£b/100]=calcule[£a*£b/10000]$',
        '$P(\\overline{B}\\cap D)=P(\\overline{B})\\times P_{\\overline{B}}(D)=calcule[(100-£a)/100]\\times calcule[£c/100]=calcule[(100-£a)*£c/10000]$'],
      ['$P(B\\cap \\overline{D})=P(B)\\times P_{B}(\\overline{D})=calcule[£a/100]\\times calcule[(100-£b)/100]=calcule[£a*(100-£b)/10000]$',
        '$P(\\overline{B}\\cap \\overline{D})=P(\\overline{B})\\times P_{\\overline{B}}(\\overline{D})=calcule[(100-£a)/100]\\times calcule[(100-£c)/100]=calcule[(100-£a)*(100-£c)/10000]$']],

      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que l’ordinateur acheté ait un problème de disque dur vaut &1&.',
        'La probabilité que l’ordinateur acheté n’ait pas de problème de disque dur vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que l’ordinateur acheté ait un problème de disque dur?',
        'Quelle est la probabilité que l’ordinateur acheté n’ait pas de problème de disque dur?'],
      questionPrbTotaleFin: ['P(D)', 'P(\\overline{D})'],
      // réponses associées
      probaTotales: ['(£a*£b+(100-£a)*£c)/10000', '(£a*(100-£b)+(100-£a)*(100-£c))/10000'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(D)=P(B\\cap D)+P(\\overline{B}\\cap D)$<br>$P(D)=calcule[£a/100]\\times calcule[£b/100]+calcule[(100-£a)/100]\\times calcule[£c/100]=calcule[(£a*£b+(100-£a)*£c)/10000]$',
        '$P(\\overline{D})=P(B\\cap \\overline{D})+P(\\overline{B}\\cap \\overline{D})$<br>$P(\\overline{D})=calcule[£a/100]\\times calcule[(100-£b)/100]+calcule[(100-£a)/100]\\times calcule[(100-£c)/100]=calcule[(£a*(100-£b)+(100-£a)*(100-£c))/10000]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['L’ordinateur acheté a un problème de disque dur, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait aussi un problème de batterie vaut &1&.',
        'L’ordinateur acheté a un problème de disque dur, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas de problème de batterie vaut &1&.'],
      ['L’ordinateur acheté n’a pas de problème de disque dur, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait un problème de batterie vaut &1&.',
        'L’ordinateur acheté n’a pas de problème de disque dur, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’en ait pas non plus à la batterie vaut &1&.']],
      questionPrbSachant1: [['Sachant que l’ordinateur acheté a un problème de disque dur, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait aussi un problème de batterie vaut &1&.',
        'Sachant que l’ordinateur acheté a un problème de disque dur, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas de problème de batterie vaut &1&.'],
      ['Sachant que l’ordinateur acheté n’a pas de problème de disque dur, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait un problème de batterie vaut &1&.',
        'Sachant que l’ordinateur acheté n’a pas de problème de disque dur, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’en ait pas non plus à la batterie vaut &1&.']],
      questionPrbCond: [['L’ordinateur acheté a un problème de disque dur. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait aussi un problème de batterie?',
        'L’ordinateur acheté a un problème de disque dur. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas de problème de batterie?'],
      ['L’ordinateur acheté n’a pas de problème de disque dur. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait un problème de batterie?',
        'L’ordinateur acheté n’a pas de problème de disque dur. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’en ait pas non plus à la batterie?']],
      questionPrbSachant: [['Sachant que l’ordinateur acheté a un problème de disque dur, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait aussi un problème de batterie?',
        'Sachant que l’ordinateur acheté a un problème de disque dur, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas de problème de batterie?'],
      ['Sachant que l’ordinateur acheté n’a pas de problème de disque dur, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait un problème de batterie?',
        'Sachant que l’ordinateur acheté n’a pas de problème de disque dur, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’en ait pas non plus à la batterie?']],
      questionPrbSachantFin: [['$P_{D}(B)$', '$P_{D}(\\overline{B})$'], ['$P_{\\overline{D}}(B)$', '$P_{\\overline{D}}(\\overline{B})$']],
      probaCond: [['£a*£b/(£a*£b+(100-£a)*£c)', '(100-£a)*£c/(£a*£b+(100-£a)*£c)'], ['£a*(100-£b)/(£a*(100-£b)+(100-£a)*(100-£c))', '(100-£a)*(100-£c)/(£a*(100-£b)+(100-£a)*(100-£c))']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{D}(B)=\\frac{P(D\\cap B)}{P(D)}=\\frac{calcule[£a*£b/10000]}{calcule[(£a*£b+(100-£a)*£c)/10000]}£s £r$',
        '$P_{D}(\\overline{B})=\\frac{P(D\\cap \\overline{B})}{P(D)}=\\frac{calcule[(100-£a)*£c/10000]}{calcule[(£a*£b+(100-£a)*£c)/10000]}£s £r$'],
      ['$P_{\\overline{D}}(B)=\\frac{P(\\overline{D}\\cap B)}{P(\\overline{D})}=\\frac{calcule[£a*(100-£b)/10000]}{calcule[(£a*(100-£b)+(100-£a)*(100-£c))/10000]}£s £r$',
        '$P_{\\overline{D}}(\\overline{B})=\\frac{P(\\overline{D}\\cap \\overline{B})}{P(\\overline{D})}=\\frac{calcule[(100-£a)*(100-£c)/10000]}{calcule[(£a*(100-£b)+(100-£a)*(100-£c))/10000]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat
    },
    {
      nomSujet: 'Liban juin 2015 ES', // numéro 6
      consignes: ['Une entreprise fabrique en grande quantité des médailles circulaires. La totalité de la production est réalisée par deux machines M$_A$ et M$_B$.',
        'La machine M$_A$ fournit £a% de la production totale et M$_B$ le reste.',
        'La machine M$_A$ produit £b% de médailles défectueuses et la machine M$_B$ produit £c% de médailles défectueuses.',
        'On prélève au hasard une médaille produite par l’entreprise et on considère les évènements suivants :',
        '- $A$ : «la médaille provient de la machine M$_A$»;',
        '- $D$ : «la médaille est défectueuse».'],
      variables: [['a', '[35;45]'], ['b', '[2;4]'], ['c', '[5;7]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['A', 'D'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['A', '£a/100', [['D', '£b/100'], ['\\overline{D}', '1-£b/100']]],
        ['\\overline{A}', '1-£a/100', [['D', '£c/100'], ['\\overline{D}', '1-£c/100']]]],
      prbEnonce: ['£a/100', '£b/100', '£c/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(A)=£a$, $P_{A}(D)=£b$ et $P_{\\overline{A}}(D)=£c$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que la médaille provienne de la machine M$_A$ et soit défectueuse vaut &1&.',
        'La probabilité que la médaille provienne de la machine M$_B$ et soit défectueuse vaut &1&.'],
      ['La probabilité que la médaille provienne de la machine M$_A$ et soit en bon état vaut &1&.',
        'La probabilité que la médaille provienne de la machine M$_B$ et soit en bon état vaut &1&.']],
      questionInter: [['Quelle est la probabilité que la médaille provienne de la machine M$_A$ et soit défectueuse?',
        'Quelle est la probabilité que la médaille provienne de la machine M$_B$ et soit défectueuse?'],
      ['Quelle est la probabilité que la médaille provienne de la machine M$_A$ et soit en bon état?',
        'Quelle est la probabilité que la médaille provienne de la machine M$_B$ et soit en bon état?']],
      questionInterFin: [['P(A\\cap D)', 'P(\\overline{A}\\cap D)'],
        ['P(A\\cap \\overline{D})', 'P(\\overline{A}\\cap \\overline{D})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['£a*£b/10000', '(100-£a)*£c/10000'], ['£a*(100-£b)/10000', '(100-£a)*(100-£c)/10000']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(A\\cap D)=P(A)\\times P_{A}(D)=calcule[£a/100]\\times calcule[£b/100]=calcule[£a*£b/10000]$',
        '$P(\\overline{A}\\cap D)=P(\\overline{A})\\times P_{\\overline{A}}(D)=calcule[(100-£a)/100]\\times calcule[£c/100]=calcule[(100-£a)*£c/10000]$'],
      ['$P(A\\cap \\overline{D})=P(A)\\times P_{A}(\\overline{D})=calcule[£a/100]\\times calcule[(100-£b)/100]=calcule[£a*(100-£b)/10000]$',
        '$P(\\overline{A}\\cap \\overline{D})=P(\\overline{A})\\times P_{\\overline{A}}(\\overline{D})=calcule[(100-£a)/100]\\times calcule[(100-£c)/100]=calcule[(100-£a)*(100-£c)/10000]$']],

      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que la médaille soit défectueuse vaut &1&.',
        'La probabilité que la médaille soit en bon état vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que la médaille soit défectueuse?',
        'Quelle est la probabilité que la médaille soit en bon état?'],
      questionPrbTotaleFin: ['P(D)', 'P(\\overline{D})'],
      // réponses associées
      probaTotales: ['(£a*£b+(100-£a)*£c)/10000', '(£a*(100-£b)+(100-£a)*(100-£c))/10000'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(D)=P(A\\cap D)+P(\\overline{A}\\cap D)$<br>$P(D)=calcule[£a/100]\\times calcule[£b/100]+calcule[(100-£a)/100]\\times calcule[£c/100]=calcule[(£a*£b+(100-£a)*£c)/10000]$',
        '$P(\\overline{D})=P(A\\cap \\overline{D})+P(\\overline{A}\\cap \\overline{D})$<br>$P(\\overline{D})=calcule[£a/100]\\times calcule[(100-£b)/100]+calcule[(100-£a)/100]\\times calcule[(100-£c)/100]=calcule[(£a*(100-£b)+(100-£a)*(100-£c))/10000]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['La médaille est défectueuse, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_A$ vaut &1&.',
        'La médaille est défectueuse, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_B$ vaut &1&.'],
      ['La médaille est en bon état, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_A$ vaut &1&.',
        'La médaille est en bon état, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_B$ vaut &1&.']],
      questionPrbSachant1: [['Sachant que la médaille est défectueuse, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_A$ vaut &1&.',
        'Sachant que la médaille est défectueuse, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_B$ vaut &1&.'],
      ['Sachant que la médaille est en bon état, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_A$ vaut &1&.',
        'Sachant que la médaille est en bon état, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_B$ vaut &1&.']],
      questionPrbCond: [['La médaille est défectueuse. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_A$?',
        'La médaille est défectueuse. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_B$?'],
      ['La médaille est en bon état. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_A$?',
        'La médaille est en bon état. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_B$?']],
      questionPrbSachant: [['Sachant que la médaille est défectueuse, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_A$?',
        'Sachant que la médaille est défectueuse, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_B$?'],
      ['Sachant que la médaille est en bon état, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_A$?',
        'Sachant que la médaille est en bon état, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle provienne de la machine M$_B$?']],
      questionPrbSachantFin: [['$P_{D}(A)$', '$P_{D}(\\overline{A})$'], ['$P_{\\overline{D}}(A)$', '$P_{\\overline{D}}(\\overline{A})$']],
      probaCond: [['£a*£b/(£a*£b+(100-£a)*£c)', '(100-£a)*£c/(£a*£b+(100-£a)*£c)'], ['£a*(100-£b)/(£a*(100-£b)+(100-£a)*(100-£c))', '(100-£a)*(100-£c)/(£a*(100-£b)+(100-£a)*(100-£c))']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{D}(A)=\\frac{P(D\\cap A)}{P(D)}=\\frac{calcule[£a*£b/10000]}{calcule[(£a*£b+(100-£a)*£c)/10000]}£s £r$',
        '$P_{D}(\\overline{A})=\\frac{P(D\\cap \\overline{A})}{P(D)}=\\frac{calcule[(100-£a)*£c/10000]}{calcule[(£a*£b+(100-£a)*£c)/10000]}£s £r$'],
      ['$P_{\\overline{D}}(A)=\\frac{P(\\overline{D}\\cap A)}{P(\\overline{D})}=\\frac{calcule[£a*(100-£b)/10000]}{calcule[(£a*(100-£b)+(100-£a)*(100-£c))/10000]}£s £r$',
        '$P_{\\overline{D}}(\\overline{A})=\\frac{P(\\overline{D}\\cap \\overline{A})}{P(\\overline{D})}=\\frac{calcule[(100-£a)*(100-£c)/10000]}{calcule[(£a*(100-£b)+(100-£a)*(100-£c))/10000]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat
    },
    {
      nomSujet: 'Liban mai 2014 S', // numéro 7
      consignes: ['Un élève part tous les jours à 7h40 de son domicile et doit arriver à 8h00 à son lycée.',
        'Il prend le vélo £a jours sur 10 et le bus le reste du temps.',
        'Les jours où il prend le vélo, il arrive à l’heure dans calcule[£b]% des cas et lorsqu’il prend le bus, il arrive en retard dans £c% des cas.',
        'On choisit une date au hasard en période scolaire et on note $V$ l’évènement «l’élève se rend au lycée à vélo» et R l’évènement «l’élève arrive en retard au lycée».'],
      variables: [['a', '[6;7]'], ['b', '[991;995]', '10'], ['c', '[4;6]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['V', 'R'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['V', '£a/10', [['R', '1-£b/100'], ['\\overline{R}', '£b/100']]],
        ['\\overline{V}', '1-£a/10', [['R', '£c/100'], ['\\overline{R}', '1-£c/100']]]],
      prbEnonce: ['£a/100', '£b/100', '£c/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(V)=£a$, $P_{V}(\\overline{R})=£b$ et $P_{\\overline{V}}(R)=£c$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que l’élève se rende a vélo au lycée et arrive en retard vaut &1&.',
        'La probabilité que l’élève se rende en bus au lycée et arrive en retard vaut &1&.'],
      ['La probabilité que l’élève se rende a vélo au lycée et arrive à l’heure vaut &1&.',
        'La probabilité que l’élève se rende en bus au lycée et arrive à l’heure vaut &1&.']],
      questionInter: [['Quelle est la probabilité que l’élève se rende a vélo au lycée et arrive en retard?',
        'Quelle est la probabilité que l’élève se rende en bus au lycée et arrive en retard?'],
      ['Quelle est la probabilité que l’élève se rende a vélo au lycée et arrive à l’heure?',
        'Quelle est la probabilité que l’élève se rende en bus au lycée et arrive à l’heure?']],
      questionInterFin: [['P(V\\cap R)', 'P(\\overline{V}\\cap R)'],
        ['P(V\\cap \\overline{R})', 'P(\\overline{V}\\cap \\overline{R})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['£a*(100-£b)/1000', '(10-£a)*£c/1000'], ['£a*£b/1000', '(10-£a)*(100-£c)/1000']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(V\\cap R)=P(V)\\times P_{V}(R)=calcule[£a/10]\\times calcule[1-£b/100]=calcule[£a*(100-£b)/1000]$',
        '$P(\\overline{V}\\cap R)=P(\\overline{V})\\times P_{\\overline{V}}(R)=calcule[(10-£a)/10]\\times calcule[£c/100]=calcule[(10-£a)*£c/1000]$'],
      ['$P(V\\cap \\overline{R})=P(V)\\times P_{V}(\\overline{R})=calcule[£a/10]\\times calcule[£b/100]=calcule[£a*£b/1000]$',
        '$P(\\overline{V}\\cap \\overline{R})=P(\\overline{V})\\times P_{\\overline{V}}(\\overline{R})=calcule[(10-£a)/10]\\times calcule[(100-£c)/100]=calcule[(10-£a)*(100-£c)/1000]$']],

      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que l’élève arrive en retard vaut &1&.',
        'La probabilité que l’élève arrive à l’heure vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que l’élève arrive en retard?',
        'Quelle est la probabilité que l’élève arrive à l’heure?'],
      questionPrbTotaleFin: ['P(R)', 'P(\\overline{R})'],
      // réponses associées
      probaTotales: ['(£a*(100-£b)+(10-£a)*£c)/1000', '(£a*£b+(10-£a)*(100-£c))/1000'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(R)=P(V\\cap R)+P(\\overline{V}\\cap V)$<br>$P(R)=calcule[£a/10]\\times calcule[1-£b/100]+calcule[(10-£a)/10]\\times calcule[£c/100]=calcule[(£a*(100-£b)+(10-£a)*£c)/1000]$',
        '$P(\\overline{R})=P(V\\cap \\overline{R})+P(\\overline{V}\\cap \\overline{R})$<br>$P(\\overline{R})=calcule[£a/10]\\times calcule[£b/100]+calcule[(10-£a)/10]\\times calcule[(100-£c)/100]=calcule[(£a*£b+(10-£a)*(100-£c))/1000]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['L’élève arrive en retard au lycée, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé à vélo vaut &1&.',
        'L’élève arrive en retard au lycée, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé en bus vaut &1&.'],
      ['L’élève arrive à l’heure au lycée, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé à vélo vaut &1&.',
        'L’élève arrive à l’heure au lycée, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé en bus vaut &1&.']],
      questionPrbSachant1: [['Sachant que l’élève arrive en retard au lycée, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé à vélo vaut &1&.',
        'Sachant que l’élève arrive en retard au lycée, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé en bus vaut &1&.'],
      ['Sachant que l’élève arrive à l’heure au lycée, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé à vélo vaut &1&.',
        'Sachant que l’élève arrive à l’heure au lycée, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé en bus vaut &1&.']],
      questionPrbCond: [['L’élève arrive en retard au lycée. Quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé à vélo?',
        'L’élève arrive en retard au lycée. Quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé en bus?'],
      ['L’élève arrive à l’heure au lycée. Quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé à vélo?',
        'L’élève arrive à l’heure au lycée. Quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé en bus?']],
      questionPrbSachant: [['Sachant que l’élève arrive en retard au lycée, quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé à vélo?',
        'Sachant que l’élève arrive en retard au lycée, quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé en bus?'],
      ['Sachant que l’élève arrive à l’heure au lycée, quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé à vélo?',
        'Sachant que l’élève arrive à l’heure au lycée, quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il y soit allé en bus?']],
      questionPrbSachantFin: [['$P_{R}(V)$', '$P_{R}(\\overline{V})$'], ['$P_{\\overline{R}}(V)$', '$P_{\\overline{R}}(\\overline{V})$']],
      probaCond: [['£a*(100-£b)/(£a*(100-£b)+(10-£a)*£c)', '(10-£a)*£c/(£a*(100-£b)+(10-£a)*£c)'], ['£a*£b/(£a*£b+(10-£a)*(100-£c))', '(10-£a)*(100-£c)/(£a*£b+(10-£a)*(100-£c))']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{R}(V)=\\frac{P(R\\cap V)}{P(R)}=\\frac{calcule[£a*(100-£b)/1000]}{calcule[(£a*(100-£b)+(10-£a)*£c)/1000]}£s £r$',
        '$P_{R}(\\overline{V})=\\frac{P(R\\cap \\overline{V})}{P(R)}=\\frac{calcule[(10-£a)*£c/1000]}{calcule[(£a*(100-£b)+(10-£a)*£c)/1000]}£s £r$'],
      ['$P_{\\overline{R}}(V)=\\frac{P(\\overline{R}\\cap V)}{P(\\overline{R})}=\\frac{calcule[£a*£b/1000]}{calcule[(£a*£b+(10-£a)*(100-£c))/1000]}£s £r$',
        '$P_{\\overline{R}}(\\overline{V})=\\frac{P(\\overline{R}\\cap \\overline{V})}{P(\\overline{R})}=\\frac{calcule[(10-£a)*(100-£c)/1000]}{calcule[(£a*£b+(10-£a)*(100-£c))/1000]}£s £r$']],
      arrondiCond: '4'// on arrondi au pire à 10^{-3} près le résultat
    },
    {
      nomSujet: 'Pondichéry avril 2015 STMG', // numéro 8
      consignes: ['Une urne contient calcule[3*£a] jetons rouges et £a jetons bleus. calcule[4*£b]% des jetons rouges sont gagnants et calcule[4*£c]% des jetons bleus sont gagnants. Un joueur tire au hasard un jeton de l’urne. On note :',
        '- $R$ l’évènement : «le jeton est rouge»;',
        '- $G$ l’évènement : «le jeton est gagnant».'],
      variables: [['a', '[5;10]'], ['b', '[4;7]'], ['c', '[8;12]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['R', 'G'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['R', '\\frac{3}{4}', [['G', '4*£b/100'], ['\\overline{G}', '1-4*£b/100']]],
        ['\\overline{R}', '\\frac{1}{4}', [['G', '4*£c/100'], ['\\overline{G}', '1-4*£c/100']]]],
      prbEnonce: ['\\frac{1}{4}', '4*£b/100', '4*£c/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(R)=£a$, $P_{R}(G)=£b$ et $P_{\\overline{R}}(G)=£c$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que le jeton tiré soit rouge et qu’il soit gagnant vaut &1&.',
        'La probabilité que le jeton tiré soit bleu et qu’il soit gagnant vaut &1&.'],
      ['La probabilité que le jeton tiré soit rouge et qu’il soit perdant vaut &1&.',
        'La probabilité que le jeton tiré soit bleu et qu’il soit perdant vaut &1&.']],
      questionInter: [['Quelle est la probabilité que le jeton tiré soit rouge et qu’il soit gagnant?',
        'Quelle est la probabilité que le jeton tiré soit bleu et qu’il soit gagnant?'],
      ['Quelle est la probabilité que le jeton tiré soit rouge et qu’il soit perdant?',
        'Quelle est la probabilité que le jeton tiré soit bleu et qu’il soit perdant?']],
      questionInterFin: [['P(R\\cap G)', 'P(\\overline{R}\\cap G)'],
        ['P(R\\cap \\overline{G})', 'P(\\overline{R}\\cap \\overline{G})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['0.75*4*£b/100', '0.25*4*£c/100'], ['0.75*(100-4*£b)/100', '0.25*(100-4*£c)/100']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(R\\cap G)=P(R)\\times P_{R}(G)=\\frac{3}{4}\\times calcule[4*£b/100]=calcule[0.75*4*£b/100]$',
        '$P(\\overline{R}\\cap G)=P(\\overline{R})\\times P_{\\overline{R}}(G)=\\frac{1}{4}\\times calcule[4*£c/100]=calcule[0.25*4*£c/100]$'],
      ['$P(R\\cap \\overline{G})=P(R)\\times P_{R}(\\overline{G})=\\frac{3}{4}\\times calcule[(100-4*£b)/100]=calcule[0.75*(100-4*£b)/100]$',
        '$P(\\overline{R}\\cap \\overline{G})=P(\\overline{R})\\times P_{\\overline{R}}(\\overline{G})=\\frac{1}{4}\\times calcule[(100-4*£c)/100]=calcule[0.25*(100-4*£c)/100]$']],

      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que le jeton tiré soit gagnant vaut &1&.',
        'La probabilité que le jeton tiré soit perdant vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que le jeton tiré soit gagnant?',
        'Quelle est la probabilité que le jeton tiré soit perdant?'],
      questionPrbTotaleFin: ['P(G)', 'P(\\overline{G})'],
      // réponses associées
      probaTotales: ['(0.75*4*£b+0.25*4*£c)/100', '(0.75*(100-4*£b)+0.25*(100-4*£c))/100'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(G)=P(R\\cap G)+P(\\overline{R}\\cap G)$<br>$P(G)=0.75\\times calcule[4*£b/100]+0.25\\times calcule[4*£c/100]=calcule[(0.75*4*£b+0.25*4*£c)/100]$',
        '$P(\\overline{G})=P(R\\cap \\overline{G})+P(\\overline{R}\\cap \\overline{G})$<br>$P(\\overline{G})=0.75\\times calcule[(100-4*£b)/100]+0.25\\times calcule[(100-4*£c)/100]=calcule[(0.75*(100-4*£b)+0.25*(100-4*£c))/100]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['Le jeton tiré est gagnant, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit rouge vaut &1&.',
        'Le jeton tiré est gagnant, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit bleu vaut &1&.'],
      ['Le jeton tiré est perdant, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit rouge vaut &1&.',
        'Le jeton tiré est perdant, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit bleu vaut &1&.']],
      questionPrbSachant1: [['Sachant que le jeton tiré est gagnant, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit rouge vaut &1&.',
        'Sachant que le jeton tiré est gagnant, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit bleu vaut &1&.'],
      ['Sachant que le jeton tiré est perdant, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit rouge vaut &1&.',
        'Sachant que le jeton tiré est perdant, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit bleu vaut &1&.']],
      questionPrbCond: [['Le jeton tiré est gagnant. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit rouge?',
        'Le jeton tiré est gagnant. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit bleu?'],
      ['Le jeton tiré est perdant. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit rouge?',
        'Le jeton tiré est perdant. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit bleu?']],
      questionPrbSachant: [['Sachant que le jeton tiré est gagnant, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit rouge?',
        'Sachant que le jeton tiré est gagnant, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit bleu?'],
      ['Sachant que le jeton tiré est perdant, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit rouge?',
        'Sachant que le jeton tiré est perdant, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il soit bleu?']],
      questionPrbSachantFin: [['$P_{G}(R)$', '$P_{G}(\\overline{R})$'], ['$P_{\\overline{G}}(R)$', '$P_{\\overline{G}}(\\overline{R})$']],
      probaCond: [['0.75*4*£b/(0.75*4*£b+0.25*4*£c)', '0.25*4*£c/(0.75*4*£b+0.25*4*£c)'], ['0.75*(100-4*£b)/(0.75*(100-4*£b)+0.25*(100-4*£c))', '0.25*(100-4*£c)/(0.75*(100-4*£b)+0.25*(100-4*£c))']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{G}(R)=\\frac{P(G\\cap R)}{P(G)}=\\frac{calcule[0.75*4*£b/100]}{calcule[(0.75*4*£b+0.25*4*£c)/100]}£s £r$',
        '$P_{G}(\\overline{R})=\\frac{P(G\\cap \\overline{R})}{P(G)}=\\frac{calcule[0.25*4*£c/100]}{calcule[(0.75*4*£b+0.25*4*£c)/100]}£s £r$'],
      ['$P_{\\overline{G}}(R)=\\frac{P(\\overline{G}\\cap R)}{P(\\overline{G})}=\\frac{calcule[0.75*(100-4*£b)/100]}{calcule[(0.75*(100-4*£b)+0.25*(100-4*£c))/100]}£s £r$',
        '$P_{\\overline{G}}(\\overline{R})=\\frac{P(\\overline{G}\\cap \\overline{R})}{P(\\overline{G})}=\\frac{calcule[0.25*(100-4*£c)/100]}{calcule[(0.75*(100-4*£b)+0.25*(100-4*£c))/100]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat
    },
    {
      nomSujet: 'Métropole juin 2016 S', // numéro 9
      consignes: ['Une usine fabrique un composant électronique. Deux chaînes de fabrication sont utilisées.',
        'La chaîne A produit calcule[5*£a]% des composants et la chaîne B produit le reste. Une partie des composants fabriqués présentent un défaut qui les empêche de fonctionner à la vitesse prévue par le constructeur. En sortie de chaîne A, calcule[4*£b]% des composants présentent ce défaut alors qu’en sortie de chaîne B, ils ne sont que calcule[2*£c]%.',
        'On choisit au hasard un composant fabriqué dans cette usine. On note : ',
        '- $A$ l’événement «le composant provient de la chaîne A»;',
        '- $S$ l’événement «le composant est sans défaut».'],
      variables: [['a', '[7;9]'], ['b', '[4;7]'], ['c', '[2;6]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['A', 'S'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['A', '5*£a/100', [['S', '1-4*£b/100'], ['\\overline{S}', '4*£b/100']]],
        ['\\overline{A}', '1-5*£a/100', [['S', '1-2*£c/100'], ['\\overline{S}', '2*£c/100']]]],
      prbEnonce: ['5*£a/100', '4*£b/100', '2*£c/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(A)=£a$, $P_{A}(\\overline{S})=£b$ et $P_{\\overline{A}}(\\overline{S})=£c$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que le composant provienne de la chaîne A et soit sans défaut vaut &1&.',
        'La probabilité que le composant provienne de la chaîne B et soit sans défaut vaut &1&.'],
      ['La probabilité que le composant provienne de la chaîne A et présente un défaut vaut &1&.',
        'La probabilité que le composant provienne de la chaîne B et présente un défaut vaut &1&.']],
      questionInter: [['Quelle est la probabilité que le composant provienne de la chaîne A et soit sans défaut?',
        'Quelle est la probabilité que le composant provienne de la chaîne B et soit sans défaut?'],
      ['Quelle est la probabilité que le composant provienne de la chaîne A et présente un défaut?',
        'Quelle est la probabilité que le composant provienne de la chaîne B et présente un défaut?']],
      questionInterFin: [['P(A\\cap S)', 'P(\\overline{A}\\cap S)'],
        ['P(A\\cap \\overline{S})', 'P(\\overline{A}\\cap \\overline{S})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['5*£a*(100-4*£b)/10000', '(100-5*£a)*(100-2*£c)/10000'], ['5*£a*4*£b/10000', '(100-5*£a)*2*£c/10000']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(A\\cap S)=P(A)\\times P_{A}(S)=calcule[5*£a/100]\\times calcule[(100-4*£b)/100]=calcule[5*£a*(100-4*£b)/10000]$',
        '$P(\\overline{A}\\cap S)=P(\\overline{A})\\times P_{\\overline{A}}(S)=calcule[(100-5*£a)/100]\\times calcule[(100-2*£c)/100]=calcule[(100-5*£a)*(100-2*£c)/10000]$'],
      ['$P(A\\cap \\overline{S})=P(A)\\times P_{A}(\\overline{S})=calcule[5*£a/100]\\times calcule[4*£b/100]=calcule[5*£a*4*£b/10000]$',
        '$P(\\overline{A}\\cap \\overline{S})=P(\\overline{A})\\times P_{\\overline{A}}(\\overline{S})=calcule[(100-5*£a)/100]\\times calcule[2*£c/100]=calcule[(100-5*£a)*2*£c/10000]$']],

      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que le composant soit sans défaut vaut &1&.',
        'La probabilité que le composant ait un défaut vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que le composant soit sans défaut?',
        'Quelle est la probabilité que le composant ait un défaut?'],
      questionPrbTotaleFin: ['P(S)', 'P(\\overline{S})'],
      // réponses associées
      probaTotales: ['(5*£a*(100-4*£b)+(100-5*£a)*(100-2*£c))/10000', '(5*£a*4*£b+(100-5*£a)*2*£c)/10000'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(S)=P(A\\cap S)+P(\\overline{A}\\cap S)$<br>$P(S)=calcule[5*£a/100]\\times calcule[(100-4*£b)/100]+calcule[(100-5*£a)/100]\\times calcule[(100-2*£c)/100]=calcule[(5*£a*(100-4*£b)+(100-5*£a)*(100-2*£c))/10000]$',
        '$P(\\overline{S})=P(A\\cap \\overline{S})+P(\\overline{A}\\cap \\overline{S})$<br>$P(\\overline{S})=calcule[5*£a/100]\\times calcule[4*£b/100]+calcule[(100-5*£a)/100]\\times calcule[2*£c/100]=calcule[(5*£a*4*£b+(100-5*£a)*2*£c)/10000]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['Le composant est sans défaut, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne A vaut &1&.',
        'Le composant est sans défaut, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne B vaut &1&.'],
      ['Le composant a un défaut, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne A vaut &1&.',
        'Le composant a un défaut, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne B vaut &1&.']],
      questionPrbSachant1: [['Sachant que le composant est sans défaut, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne A vaut &1&.',
        'Sachant que le composant est sans défaut, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne B vaut &1&.'],
      ['Sachant que le composant a un défaut, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne A vaut &1&.',
        'Sachant que le composant a un défaut, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne B vaut &1&.']],
      questionPrbCond: [['Le composant est sans défaut. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne A?',
        'Le composant est sans défaut. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne B?'],
      ['Le composant a un défaut. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne A?',
        'Le composant a un défaut. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne B?']],
      questionPrbSachant: [['Sachant que le composant est sans défaut, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne A?',
        'Sachant que le composant est sans défaut, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne B?'],
      ['Sachant que le composant a un défaut, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne A?',
        'Sachant que le composant a un défaut, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il provienne de la chaîne B?']],
      questionPrbSachantFin: [['$P_{S}(A)$', '$P_{S}(\\overline{A})$'], ['$P_{\\overline{S}}(A)$', '$P_{\\overline{S}}(\\overline{A})$']],
      probaCond: [['5*£a*(100-4*£b)/(5*£a*(100-4*£b)+(100-5*£a)*(100-2*£c))', '(100-5*£a)*(100-2*£c)/(5*£a*(100-4*£b)+(100-5*£a)*(100-2*£c))'], ['5*£a*4*£b/(5*£a*4*£b+(100-5*£a)*2*£c)', '(100-5*£a)*2*£c/(5*£a*4*£b+(100-5*£a)*2*£c)']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{S}(A)=\\frac{P(S\\cap A)}{P(S)}=\\frac{calcule[5*£a*(100-4*£b)/10000]}{calcule[(5*£a*(100-4*£b)+(100-5*£a)*(100-2*£c))/10000]}£s £r$',
        '$P_{S}(\\overline{A})=\\frac{P(S\\cap \\overline{A})}{P(A)}=\\frac{calcule[(100-5*£a)*(100-2*£c)/10000]}{calcule[(5*£a*(100-4*£b)+(100-5*£a)*(100-2*£c))/10000]}£s £r$'],
      ['$P_{\\overline{S}}(A)=\\frac{P(\\overline{S}\\cap A)}{P(\\overline{S})}=\\frac{calcule[5*£a*4*£b/10000]}{calcule[(5*£a*4*£b+(100-5*£a)*2*£c)/10000]}£s £r$',
        '$P_{\\overline{S}}(\\overline{A})=\\frac{P(\\overline{S}\\cap \\overline{A})}{P(\\overline{S})}=\\frac{calcule[(100-5*£a)*2*£c/10000]}{calcule[(5*£a*4*£b+(100-5*£a)*2*£c)/10000]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat
    },
    {
      nomSujet: 'Polynésie juin 2015 STMG', // numéro 10
      consignes: ['On a prouvé qu’une des origines d’une maladie était génétique. On estime que calcule[£a]% de la population est porteur du gène en cause. Lorsqu’un individu est porteur du gène, on estime à calcule[£b] la probabilité qu’il développe la maladie. Mais s’il n’est pas porteur du gène il y a tout de même une probabilité de calcule[£c] qu’il développe la maladie.',
        'On choisit au hasard un individu dans la population, on considère les évènements suivants :',
        '— $G$ : «l’individu est porteur du gène»;',
        '— $M$ : «l’individu développe la maladie».'],
      variables: [['a', '[1;4]', '10'], ['b', '[6;9]', '10'], ['c', '[1;3]', '100']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['G', 'M'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['G', '£a/100', [['M', '£b'], ['\\overline{M}', '1-£b']]],
        ['\\overline{G}', '1-£a/100', [['M', '£c'], ['\\overline{M}', '1-£c']]]],
      prbEnonce: ['£a/100', '£b', '£c'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(G)=£a$, $P_{G}(M)=£b$ et $P_{\\overline{G}}(M)=£c$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que l’individu soit porteur du gène et développe la maladie vaut &1&.',
        'La probabilité que l’individu ne soit pas porteur du gène et développe la maladie vaut &1&.'],
      ['La probabilité que l’individu soit porteur du gène et ne développe pas la maladie vaut &1&.',
        'La probabilité que l’individu ne soit pas porteur du gène et ne développe pas la maladie vaut &1&.']],
      questionInter: [['Quelle est la probabilité que l’individu soit porteur du gène et développe la maladie?',
        'Quelle est la probabilité que l’individu ne soit pas porteur du gène et développe la maladie?'],
      ['Quelle est la probabilité que l’individu soit porteur du gène et ne développe pas la maladie?',
        'Quelle est la probabilité que l’individu ne soit pas porteur du gène et ne développe pas la maladie?']],
      questionInterFin: [['P(G\\cap M)', 'P(\\overline{G}\\cap M)'],
        ['P(G\\cap \\overline{M})', 'P(\\overline{G}\\cap \\overline{M})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['£a*£b/100', '(100-£a)*£c/100'], ['£a*(1-£b)/100', '(100-£a)*(1-£c)/100']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(G\\cap M)=P(G)\\times P_{G}(M)=calcule[£a/100]\\times £b=calcule[£a*£b/100]$',
        '$P(\\overline{G}\\cap M)=P(\\overline{G})\\times P_{\\overline{G}}(M)=calcule[1-£a/100]\\times £c=calcule[(100-£a)*£c/100]$'],
      ['$P(G\\cap \\overline{M})=P(G)\\times P_{G}(\\overline{M})=calcule[£a/100]\\times calcule[1-£b]=calcule[£a*(1-£b)/100]$',
        '$P(\\overline{G}\\cap \\overline{M})=P(\\overline{G})\\times P_{\\overline{G}}(\\overline{M})=calcule[1-£a/100]\\times calcule[1-£c]=calcule[(100-£a)*(1-£c)/100]$']],

      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que l’individu développe la maladie vaut &1&.',
        'La probabilité que l’individu ne développe pas la maladie vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que l’individu développe la maladie?',
        'Quelle est la probabilité que l’individu ne développe pas la maladie?'],
      questionPrbTotaleFin: ['P(M)', 'P(\\overline{M})'],
      // réponses associées
      probaTotales: ['(£a*£b+(100-£a)*£c)/100', '(£a*(1-£b)+(100-£a)*(1-£c))/100'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(M)=P(G\\cap M)+P(\\overline{G}\\cap M)$<br>$P(M)=calcule[£a/100]\\times £b+calcule[1-£a/100]\\times £c=calcule[(£a*£b+(100-£a)*£c)/100]$',
        '$P(\\overline{M})=P(G\\cap \\overline{M})+P(\\overline{G}\\cap \\overline{M})$<br>$P(\\overline{M})=calcule[£a/100]\\times calcule[1-£b]+calcule[1-£a/100]\\times calcule[1-£c]=calcule[(£a*(1-£b)+(100-£a)*(1-£c))/100]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['L’individu développe la maladie, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il soit porteur du gène vaut &1&.',
        'L’individu développe la maladie, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il ne soit pas porteur du gène vaut &1&.'],
      ['L’individu ne développe pas la maladie, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il soit pourtant porteur du gène vaut &1&.',
        'L’individu ne développe pas la maladie, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il ne soit pas porteur du gène vaut &1&.']],
      questionPrbSachant1: [['Sachant que l’individu développe la maladie, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il soit porteur du gène vaut &1&.',
        'Sachant que l’individu développe la maladie, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il ne soit pas porteur du gène vaut &1&.'],
      ['Sachant que l’individu ne développe pas la maladie, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il soit pourtant porteur du gène vaut &1&.',
        'Sachant que l’individu ne développe pas la maladie, la probabilité (éventuellement arrondie à $10^{-4}$) qu’il ne soit pas porteur du gène vaut &1&.']],
      questionPrbCond: [['L’individu développe la maladie. Quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il soit porteur du gène?',
        'L’individu développe la maladie. Quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il ne soit pas porteur du gène?'],
      ['L’individu ne développe pas la maladie. Quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il soit pourtant porteur du gène?',
        'L’individu ne développe pas la maladie. Quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il ne soit pas porteur du gène?']],
      questionPrbSachant: [['Sachant que l’individu développe la maladie, quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il soit porteur du gène?',
        'Sachant que l’individu développe la maladie, quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il ne soit pas porteur du gène?'],
      ['Sachant que l’individu ne développe pas la maladie, quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il soit pourtant porteur du gène?',
        'Sachant que l’individu ne développe pas la maladie, quelle est la probabilité (éventuellement arrondie à $10^{-4}$) qu’il ne soit pas porteur du gène?']],
      questionPrbSachantFin: [['$P_{M}(G)$', '$P_{M}(\\overline{G})$'], ['$P_{\\overline{M}}(G)$', '$P_{\\overline{M}}(\\overline{G})$']],
      probaCond: [['£a*£b/(£a*£b+(100-£a)*£c)', '(100-£a)*£c/(£a*£b+(100-£a)*£c)'], ['£a*(1-£b)/(£a*(1-£b)+(100-£a)*(1-£c))', '(100-£a)*(1-£c)/(£a*(1-£b)+(100-£a)*(1-£c))']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{M}(G)=\\frac{P(M\\cap G)}{P(M)}=\\frac{calcule[£a*£b/100]}{calcule[(£a*£b+(100-£a)*£c)/100]}£s £r$',
        '$P_{M}(\\overline{G})=\\frac{P(M\\cap \\overline{G})}{P(M)}=\\frac{calcule[(100-£a)*£c/100]}{calcule[(£a*£b+(100-£a)*£c)/100]}£s £r$'],
      ['$P_{\\overline{M}}(G)=\\frac{P(\\overline{M}\\cap G)}{P(\\overline{M})}=\\frac{calcule[£a*(1-£b)/100]}{calcule[(£a*(1-£b)+(100-£a)*(1-£c))/100]}£s £r$',
        '$P_{\\overline{M}}(\\overline{G})=\\frac{P(\\overline{M}\\cap \\overline{G})}{P(\\overline{M})}=\\frac{calcule[(100-£a)*(1-£c)/100]}{calcule[(£a*(1-£b)+(100-£a)*(1-£c))/100]}£s £r$']],
      arrondiCond: '4'// on arrondi au pire à 10^{-4} près le résultat
    },
    {
      nomSujet: 'Ex 14 p. 339 Manuel Magnard', // numéro 11
      consignes: ['Après les contrôle de Mathématiques, calcule[£a*100]% du temps, Issa dit «Je suis sûr que j’ai loupé».',
        'Ses amis sont pourtant formels : «Quand il dit ça, il a quand même 15 ou plus les 3/4 du temps. Et quand il ne dit rien, on peut être sûr à £b% qu’il va avoir 15 ou plus».',
        'Après un devoir de Mathématiques, on considère les événements:',
        '— $L$ : «Issa dit qu’il a manqué le devoir»;',
        '— $B$ : «Issa a 15 ou plus au devoir».'],
      variables: [['a', '[5;7]', '10'], ['b', '[85;95]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['L', 'B'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['L', '£a', [['B', '\\frac{3}{4}'], ['\\overline{B}', '\\frac{1}{4}']]],
        ['\\overline{L}', '1-£a', [['B', '£b/100'], ['\\overline{B}', '1-£b/100']]]],
      prbEnonce: ['£a', '\\frac{3}{4}', '£b/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(L)=£a$, $P_{L}(B)=£b$ et $P_{\\overline{L}}(B)=£c$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité qu’Issa dise avoir manqué le devoir et ait 15 ou plus vaut &1&.',
        'La probabilité qu’Issa ne dise pas avoir manqué le devoir et ait 15 ou plus vaut &1&.'],
      ['La probabilité qu’Issa dise avoir manqué le devoir et ait moins de 15 vaut &1&.',
        'La probabilité qu’Issa ne dise pas avoir manqué le devoir et ait moins de 15 vaut &1&.']],
      questionInter: [['Quelle est la probabilité qu’Issa dise avoir manqué le devoir et ait 15 ou plus?',
        'Quelle est la probabilité qu’Issa ne dise pas avoir manqué le devoir et ait 15 ou plus?'],
      ['Quelle est la probabilité qu’Issa dise avoir manqué le devoir et ait moins de 15?',
        'Quelle est la probabilité qu’Issa ne dise pas avoir manqué le devoir et ait moins de 15?']],
      questionInterFin: [['P(L\\cap B)', 'P(\\overline{L}\\cap B)'],
        ['P(L\\cap \\overline{B})', 'P(\\overline{L}\\cap \\overline{B})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['£a*3/4', '(1-£a)*£b/100'], ['£a*1/4', '(1-£a)*(100-£b)/100']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(L\\cap B)=P(L)\\times P_{L}(B)=calcule[£a]\\times \\frac{3}{4}=calcule[£a*3/4]$',
        '$P(\\overline{L}\\cap B)=P(\\overline{L})\\times P_{\\overline{L}}(B)=calcule[1-£a]\\times calcule[£b/100]=calcule[(1-£a)*£b/100]$'],
      ['$P(L\\cap \\overline{B})=P(L)\\times P_{L}(\\overline{B})=£a\\times \\frac{1}{4}=calcule[£a*1/4]$',
        '$P(\\overline{L}\\cap \\overline{B})=P(\\overline{L})\\times P_{\\overline{L}}(\\overline{B})=calcule[1-£a]\\times calcule[(100-£b)/100]=calcule[(1-£a)*(100-£b)/100]$']],
      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité qu’Issa ait 15 ou plus au devoir vaut &1&.',
        'La probabilité qu’Issa ait moins de 15 au devoir vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité qu’Issa ait 15 ou plus au devoir?',
        'Quelle est la probabilité qu’Issa ait moins de 15 au devoir?'],
      questionPrbTotaleFin: ['P(B)', 'P(\\overline{B})'],
      // réponses associées
      probaTotales: ['£a*3/4+(1-£a)*£b/100', '£a*1/4+(1-£a)*(100-£b)/100'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(B)=P(L\\cap B)+P(\\overline{L}\\cap B)$<br>$P(B)=calcule[£a]\\times \\frac{3}{4}+calcule[1-£a]\\times calcule[£b/100]=calcule[£a*3/4+(1-£a)*£b/100]$',
        '$P(\\overline{B})=P(L\\cap \\overline{B})+P(\\overline{L}\\cap \\overline{B})$<br>$P(\\overline{B})=calcule[£a]\\times \\frac{1}{4}+calcule[1-£a]\\times calcule[1-£b/100]=calcule[£a*1/4+(1-£a)*(100-£b)/100]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['Issa a eu 15 ou plus au devoir, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait dit avoir manqué le devoir vaut &1&.',
        'Issa a eu 15 ou plus au devoir, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas dit avoir manqué le devoir vaut &1&.'],
      ['Issa a eu moins de 15 au devoir, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait dit avoir manqué le devoir vaut &1&.',
        'Issa a eu moins de 15 au devoir, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas dit avoir manqué le devoir vaut &1&.']],
      questionPrbSachant1: [['Sachant qu’Issa a eu 15 ou plus au devoir, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait dit avoir manqué le devoir vaut &1&.',
        'Sachant qu’Issa a eu 15 ou plus au devoir, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas dit avoir manqué le devoir vaut &1&.'],
      ['Sachant qu’Issa a eu moins de 15 au devoir, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait dit avoir manqué le devoir vaut &1&.',
        'Sachant qu’Issa a eu moins de 15 au devoir, la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas dit avoir manqué le devoir vaut &1&.']],
      questionPrbCond: [['Issa a eu 15 ou plus au devoir. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait dit avoir manqué le devoir?',
        'Issa a eu 15 ou plus au devoir. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas dit avoir manqué le devoir?'],
      ['Issa a eu moins de 15 au devoir. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait dit avoir manqué le devoir?',
        'Issa a eu moins de 15 au devoir. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas dit avoir manqué le devoir?']],
      questionPrbSachant: [['Sachant qu’Issa a eu 15 ou plus au devoir, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait dit avoir manqué le devoir?',
        'Sachant qu’Issa a eu 15 ou plus au devoir, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas dit avoir manqué le devoir?'],
      ['Sachant qu’Issa a eu moins de 15 au devoir, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il ait dit avoir manqué le devoir?',
        'Sachant qu’Issa a eu moins de 15 au devoir, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’il n’ait pas dit avoir manqué le devoir?']],
      questionPrbSachantFin: [['$P_{B}(L)$', '$P_{B}(\\overline{L})$'], ['$P_{\\overline{B}}(L)$', '$P_{\\overline{B}}(\\overline{L})$']],
      probaCond: [['£a*(3/4)/(£a*3/4+(1-£a)*£b/100)', '(1-£a)*(£b/100)/(£a*3/4+(1-£a)*£b/100)'], ['£a*(1/4)/(£a*1/4+(1-£a)*(100-£b)/100)', '(1-£a)*((100-£b)/100)/(£a*1/4+(1-£a)*(100-£b)/100)']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{B}(L)=\\frac{P(B\\cap L)}{P(B)}=\\frac{calcule[£a*(3/4)]}{calcule[£a*(3/4)+(1-£a)*£b/100]}£s £r$',
        '$P_{B}(\\overline{L})=\\frac{P(B\\cap \\overline{L})}{P(B)}=\\frac{calcule[(1-£a)*£b/100]}{calcule[£a*(3/4)+(1-£a)*£b/100]}£s £r$'],
      ['$P_{\\overline{B}}(L)=\\frac{P(\\overline{B}\\cap L)}{P(\\overline{B})}=\\frac{calcule[£a*(1/4)]}{calcule[£a*1/4+(1-£a)*(100-£b)/100]}£s £r$',
        '$P_{\\overline{B}}(\\overline{L})=\\frac{P(\\overline{B}\\cap \\overline{L})}{P(\\overline{B})}=\\frac{calcule[(1-£a)*(100-£b)/100]}{calcule[£a*1/4+(1-£a)*(100-£b)/100]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat
    },
    {
      nomSujet: 'Ex 21 p. 340 Manuel Magnard', // numéro 12
      consignes: ['Sophie a mis des dragées dans une boîte, les unes contiennent une amande, les autres non :',
        '— calcule[5*£a]% des dragées contiennent une amande;',
        '— calcule[2*£b]% des dragées avec amande sont bleues, les autres sont roses;',
        '— calcule[2*£c]% des dragées sans amande sont bleues, les autres sont roses.',
        'Sophie choisit au hasard une dragée dans la boîte et on considère les événements:',
        '— $A$ : «la dragée choisie contient une amande»;',
        '— $B$ : «la dragée choisie est bleue».'],
      variables: [['a', '[5;7]'], ['b', '[18;24]'], ['c', '[35;41]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['A', 'B'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['A', '5*£a/100', [['B', '2*£b/100'], ['\\overline{B}', '(100-2*£b)/100']]],
        ['\\overline{A}', '1-5*£a/100', [['B', '2*£c/100'], ['\\overline{B}', '(100-2*£c)/100']]]],
      prbEnonce: ['5*£a/100', '2*£b/100', '2*£c/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(A)=£a$, $P_{A}(B)=£b$ et $P_{\\overline{A}}(B)=£c$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que la dragée choisie contienne une amande et qu’elle soit bleue vaut &1&.',
        'La probabilité que la dragée choisie ne contienne pas d’amande et qu’elle soit bleue vaut &1&.'],
      ['La probabilité que la dragée choisie contienne une amande et qu’elle soit rose vaut &1&.',
        'La probabilité que la dragée choisie ne contienne pas d’amande et qu’elle soit rose vaut &1&.']],
      questionInter: [['Quelle est la probabilité que la dragée choisie contienne une amande et qu’elle soit bleue?',
        'Quelle est la probabilité que la dragée choisie ne contienne pas d’amande et qu’elle soit bleue?'],
      ['Quelle est la probabilité que la dragée choisie contienne une amande et qu’elle soit rose?',
        'Quelle est la probabilité que la dragée choisie ne contienne pas d’amande et qu’elle soit rose?']],
      questionInterFin: [['P(A\\cap B)', 'P(\\overline{A}\\cap B)'],
        ['P(A\\cap \\overline{B})', 'P(\\overline{A}\\cap \\overline{B})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['5*£a*2*£b/10000', '(100-5*£a)*2*£c/10000'], ['5*£a*(100-2*£b)/10000', '(100-5*£a)*(100-2*£c)/10000']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(A\\cap B)=P(A)\\times P_{A}(B)=calcule[5*£a/100]\\times calcule[2*£b/100]=calcule[5*£a*2*£b/10000]$',
        '$P(\\overline{A}\\cap B)=P(\\overline{A})\\times P_{\\overline{A}}(B)=calcule[1-5*£a/100]\\times calcule[2*£c/100]=calcule[(100-5*£a)*2*£c/10000]$'],
      ['$P(A\\cap \\overline{B})=P(A)\\times P_{A}(\\overline{B})=calcule[5*£a/100]\\times calcule[1-2*£b/100]=calcule[5*£a*(100-2*£b)/10000]$',
        '$P(\\overline{A}\\cap \\overline{B})=P(\\overline{A})\\times P_{\\overline{A}}(\\overline{B})=calcule[1-5*£a/100]\\times calcule[(100-2*£c)/100]=calcule[(100-5*£a)*(100-2*£c)/10000]$']],
      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que la dragée choisie soit bleue vaut &1&.',
        'La probabilité que la dragée choisie soit rose vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que la dragée choisie soit bleue?',
        'Quelle est la probabilité que la dragée choisie soit rose?'],
      questionPrbTotaleFin: ['P(B)', 'P(\\overline{B})'],
      // réponses associées
      probaTotales: ['(5*£a*2*£b+(100-5*£a)*2*£c)/10000', '(5*£a*(100-2*£b)+(100-5*£a)*(100-2*£c))/10000'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(B)=P(A\\cap B)+P(\\overline{A}\\cap B)$<br>$P(B)=calcule[5*£a/100]\\times calcule[2*£b/100]+calcule[(100-5*£a)/100]\\times calcule[2*£c/100]=calcule[(5*£a*2*£b+(100-5*£a)*2*£c)/10000]$',
        '$P(\\overline{B})=P(A\\cap \\overline{B})+P(\\overline{A}\\cap \\overline{B})$<br>$P(\\overline{B})=calcule[5*£a/100]\\times calcule[(100-2*£b)/100]+calcule[(100-5*£a)/100]\\times calcule[(100-2*£c)/100]=calcule[(5*£a*(100-2*£b)+(100-5*£a)*(100-2*£c))/10000]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['La dragée choisie est bleue, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle contienne une amande vaut &1&.',
        'La dragée choisie est bleue, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne contienne pas d’amande vaut &1&.'],
      ['La dragée choisie est rose, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle contienne une amande vaut &1&.',
        'La dragée choisie est rose, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne contienne pas d’amande vaut &1&.']],
      questionPrbSachant1: [['Sachant que la dragée choisie est bleue, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle contienne une amande vaut &1&.',
        'Sachant que la dragée choisie est bleue, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne contienne pas d’amande vaut &1&.'],
      ['Sachant que la dragée choisie est rose, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle contienne une amande vaut &1&.',
        'Sachant que la dragée choisie est rose, la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne contienne pas d’amande vaut &1&.']],
      questionPrbCond: [['La dragée choisie est bleue. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle contienne une amande?',
        'La dragée choisie est bleue. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne contienne pas d’amande?'],
      ['La dragée choisie est rose. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle contienne une amande?',
        'La dragée choisie est rose. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne contienne pas d’amande?']],
      questionPrbSachant: [['Sachant que la dragée choisie est bleue, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle contienne une amande?',
        'Sachant que la dragée choisie est bleue, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne contienne pas d’amande?'],
      ['Sachant que la dragée choisie est rose, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle contienne une amande?',
        'Sachant que la dragée choisie est rose, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) qu’elle ne contienne pas d’amande?']],
      questionPrbSachantFin: [['$P_{B}(A)$', '$P_{B}(\\overline{A})$'], ['$P_{\\overline{B}}(A)$', '$P_{\\overline{B}}(\\overline{A})$']],
      probaCond: [['5*£a*2*£b/(5*£a*2*£b+(100-5*£a)*2*£c)', '(100-5*£a)*2*£c/(5*£a*2*£b+(100-5*£a)*2*£c)'], ['5*£a*(100-2*£b)/(5*£a*(100-2*£b)+(100-5*£a)*(100-2*£c))', '(100-5*£a)*(100-2*£c)/(5*£a*(100-2*£b)+(100-5*£a)*(100-2*£c))']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{B}(A)=\\frac{P(B\\cap A)}{P(B)}=\\frac{calcule[5*£a*2*£b/10000]}{calcule[(5*£a*2*£b+(100-5*£a)*2*£c)/10000]}£s £r$',
        '$P_{B}(\\overline{A})=\\frac{P(B\\cap \\overline{A})}{P(B)}=\\frac{calcule[(100-5*£a)*2*£c/10000]}{calcule[(5*£a*2*£b+(100-5*£a)*2*£c)/10000]}£s £r$'],
      ['$P_{\\overline{B}}(A)=\\frac{P(\\overline{B}\\cap A)}{P(\\overline{B})}=\\frac{calcule[5*£a*(100-2*£b)/10000]}{calcule[(5*£a*(100-2*£b)+(100-5*£a)*(100-2*£c))/10000]}£s £r$',
        '$P_{\\overline{B}}(\\overline{A})=\\frac{P(\\overline{B}\\cap \\overline{A})}{P(\\overline{B})}=\\frac{calcule[(100-5*£a)*(100-2*£c)/10000]}{calcule[(5*£a*(100-2*£b)+(100-5*£a)*(100-2*£c))/10000]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat
    },

    {
      nomSujet: 'Ex 22 p. 340 Manuel Magnard', // numéro 13
      consignes: ['Ordralphabétix est un poissonnier et calcule[5*£a]% du poisson qu’il vend a été pêché par ses soins, calcule[5*£b]% vient d’un grossiste armoricain et le reste d’un grossiste de Lutèce.',
        'Il a remarqué que £c% de ses clients sont mécontents du poisson qu’il a lui-même pêché, £d% du poisson du grossiste armoricain et £e% du poisson de Lutèce.',
        'Un client achète un poisson à Ordralphabétix.',
        'On considère les événements suivants :',
        '— $O$ : «le poisson a été pêché par Ordralphabétix»;',
        '— $A$ : «le poisson provient du grossiste armoricain»;',
        '— $L$ : «le poisson provient du grossiste de Lutèce»;',
        '— $M$ : «le client est mécontent du poisson».'],
      variables: [['a', '[2;3]'], ['b', '[5;7]'], ['c', '[5;8]'], ['d', '[9;12]'], ['e', '[70;75]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['O', 'A', 'L', 'M'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['O', '5*£a/100', [['M', '£c/100'], ['\\overline{M}', '(100-£c)/100']]],
        ['A', '5*£b/100', [['M', '£d/100'], ['\\overline{M}', '(100-£d)/100']]],
        ['L', '(100-5*£a-5*£b)/100', [['M', '£e/100'], ['\\overline{M}', '(100-£e)/100']]]],
      prbEnonce: ['5*£a/100', '5*£b/100', '£c/100', '£d/100', '£e/100'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(O)=£a$, $P(A)=£b$, $P_{O}(M)=£c$, $P_{A}(M)=£d$ et $P_{L}(M)=£d$.',
      // différentes questions sur les proba d’une intersection (liées à la proba calculée à la question suivante)
      questionInter1: [['La probabilité que le poisson ait été pêché par Ordralphabétix et que le client en soit mécontent vaut &1&.',
        'La probabilité que le poisson provienne du grossiste armoricain et que le client en soit mécontent vaut &1&.',
        'La probabilité que le poisson provienne du grossiste de Lutèce et que le client en soit mécontent vaut &1&.'],
      ['La probabilité que le poisson ait été pêché par Ordralphabétix et que le client en soit satisfait vaut &1&.',
        'La probabilité que le poisson provienne du grossiste armoricain et que le client en soit satisfait vaut &1&.',
        'La probabilité que le poisson provienne du grossiste de Lutèce et que le client en soit satisfait vaut &1&.']],
      questionInter: [['Quelle est la probabilité que le poisson ait été pêché par Ordralphabétix et que le client en soit mécontent?',
        'Quelle est la probabilité que le poisson provienne du grossiste armoricain et que le client en soit mécontent?',
        'Quelle est la probabilité que le poisson provienne du grossiste de Lutèce et que le client en soit mécontent?'],
      ['Quelle est la probabilité que le poisson ait été pêché par Ordralphabétix et que le client en soit satisfait?',
        'Quelle est la probabilité que le poisson provienne du grossiste armoricain et que le client en soit satisfait?',
        'Quelle est la probabilité que le poisson provienne du grossiste de Lutèce et que le client en soit satisfait?']],
      questionInterFin: [['P(O\\cap M)', 'P(A\\cap M)', 'P(L\\cap M)'],
        ['P(O\\cap \\overline{M})', 'P(A\\cap \\overline{M})', 'P(L\\cap \\overline{M})']],
      // Réponses associées à chacune des questions précédentes
      // le remplacement des valeurs se fera dans la section
      probaInter: [['5*£a*£c/10000', '5*£b*£d/10000', '(100-5*£a-5*£b)*£e/10000'], ['5*£a*(100-£c)/10000', '5*£b*(100-£d)/10000', '(100-5*£a-5*£b)*(100-£e)/10000']],
      // Correction associée à la proba à calculer
      correctionInter: [['$P(O\\cap M)=P(O)\\times P_{O}(M)=calcule[5*£a/100]\\times calcule[£c/100]=calcule[5*£a*£c/10000]$',
        '$P(A\\cap M)=P(A)\\times P_{A}(M)=calcule[5*£b/100]\\times calcule[£d/100]=calcule[5*£b*£d/10000]$',
        '$P(L\\cap M)=P(L)\\times P_{L}(M)=calcule[(100-5*£a-5*£b)/100]\\times calcule[£e/100]=calcule[(100-5*£a-5*£b)*£e/10000]$'],
      ['$P(O\\cap \\overline{M})=P(O)\\times P_{O}(\\overline{M})=calcule[5*£a/100]\\times calcule[(100-£c)/100]=calcule[5*£a*(100-£c)/10000]$',
        '$P(A\\cap \\overline{M})=P(A)\\times P_{A}(\\overline{M})=calcule[5*£b/100]\\times calcule[(100-£d)/100]=calcule[5*£b*(100-£d)/10000]$',
        '$P(L\\cap \\overline{M})=P(L)\\times P_{L}(\\overline{M})=calcule[(100-5*£a-5*£b)/100]\\times calcule[(100-£e)/100]=calcule[(100-5*£a-5*£b)*(100-£e)/10000]$']],
      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale1: ['La probabilité que le client soit mécontent vaut &1&.',
        'La probabilité que le client soit satisfait vaut &1&.'],
      questionPrbTotale: ['Quelle est la probabilité que le client soit mécontent?',
        'Quelle est la probabilité que le client soit satisfait?'],
      questionPrbTotaleFin: ['P(M)', 'P(\\overline{M})'],
      // réponses associées
      probaTotales: ['(5*£a*£c+5*£b*£d+(100-5*£a-5*£b)*£e)/10000', '(5*£a*(100-£c)+5*£b*(100-£d)+(100-5*£a-5*£b)*(100-£e))/10000'],
      // Correction associée à la proba à calculer
      correctionTotale: ['$P(M)=P(O\\cap M)+P(A\\cap M)+P(L\\cap M)$<br>$P(M)=calcule[5*£a/100]\\times calcule[£c/100]+calcule[5*£b/100]\\times calcule[£d/100]+calcule[(100-5*£a-5*£b)/100]\\times calcule[£e/100]=calcule[(5*£a*£c+5*£b*£d+(100-5*£a-5*£b)*£e)/10000]$',
        '$P(\\overline{M})=P(O\\cap \\overline{M})+P(A\\cap \\overline{M})+P(L\\cap \\overline{M})$<br>$P(\\overline{M})=calcule[5*£a/100]\\times calcule[(100-£c)/100]+calcule[5*£b/100]\\times calcule[(100-£d)/100]+calcule[(100-5*£a-5*£b)/100]\\times calcule[(100-£e)/100]=calcule[(5*£a*(100-£c)+5*£b*(100-£d)+(100-5*£a-5*£b)*(100-£e))/10000]$'],

      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: [['Le client est mécontent, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson ait été pêché par Ordralphabétix vaut &1&.',
        'Le client est mécontent, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste armoricain vaut &1&.',
        'Le client est mécontent, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste de Lutèce vaut &1&.'],
      ['Le client est satisfait, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson ait été pêché par Ordralphabétix vaut &1&.',
        'Le client est satisfait, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste armoricain vaut &1&.',
        'Le client est satisfait, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste de Lutèce vaut &1&.']],
      questionPrbSachant1: [['Sachant que le client est mécontent, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson ait été pêché par Ordralphabétix vaut &1&.',
        'Sachant que le client est mécontent, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste armoricain vaut &1&.',
        'Sachant que le client est mécontent, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste de Lutèce vaut &1&.'],
      ['Sachant que le client est satisfait, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson ait été pêché par Ordralphabétix vaut &1&.',
        'Sachant que le client est satisfait, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste armoricain vaut &1&.',
        'Sachant que le client est satisfait, la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste de Lutèce vaut &1&.']],
      questionPrbCond: [['Le client est mécontent. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson ait été pêché par Ordralphabétix?',
        'Le client est mécontent. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste armoricain?',
        'Le client est mécontent. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste de Lutèce?'],
      ['Le client est satisfait. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson ait été pêché par Ordralphabétix?',
        'Le client est satisfait. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste armoricain?',
        'Le client est satisfait. Quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste de Lutèce?']],
      questionPrbSachant: [['Sachant que le client est mécontent, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson ait été pêché par Ordralphabétix?',
        'Sachant que le client est mécontent, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste armoricain?',
        'Sachant que le client est mécontent, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste de Lutèce?'],
      ['Sachant que le client est satisfait, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson ait été pêché par Ordralphabétix?',
        'Sachant que le client est satisfait, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste armoricain?',
        'Sachant que le client est satisfait, quelle est la probabilité (éventuellement arrondie à $10^{-3}$) que le poisson provienne du grossiste de Lutèce?']],
      questionPrbSachantFin: [['$P_{M}(O)$', '$P_{M}(A)$', '$P_{M}(L)$'], ['$P_{\\overline{M}}(O)$', '$P_{\\overline{M}}(A)$', '$P_{\\overline{M}}(L)$']],
      probaCond: [['5*£a*£c/(5*£a*£c+5*£b*£d+(100-5*£a-5*£b)*£e)', '5*£b*£d/(5*£a*£c+5*£b*£d+(100-5*£a-5*£b)*£e)', '(100-5*£a-5*£b)*£e/(5*£a*£c+5*£b*£d+(100-5*£a-5*£b)*£e)'], ['5*£a*(100-£c)/(5*£a*(100-£c)+5*£b*(100-£d)+(100-5*£a-5*£b)*(100-£e))', '5*£b*(100-£d)/(5*£a*(100-£c)+5*£b*(100-£d)+(100-5*£a-5*£b)*(100-£e))', '(100-5*£a-5*£b)*(100-£e)/(5*£a*(100-£c)+5*£b*(100-£d)+(100-5*£a-5*£b)*(100-£e))']],
      // correction de la probabilité conditionnelle
      correctionCond: [['$P_{M}(O)=\\frac{P(M\\cap O)}{P(M)}=\\frac{calcule[5*£a*£c/10000]}{calcule[(5*£a*£c+5*£b*£d+(100-5*£a-5*£b)*£e)/10000]}£s £r$',
        '$P_{M}(A)=\\frac{P(M\\cap A)}{P(M)}=\\frac{calcule[5*£b*£d/10000]}{calcule[(5*£a*£c+5*£b*£d+(100-5*£a-5*£b)*£e)/10000]}£s £r$',
        '$P_{M}(L)=\\frac{P(M\\cap L)}{P(M)}=\\frac{calcule[(100-5*£a-5*£b)*£e/10000]}{calcule[(5*£a*£c+5*£b*£d+(100-5*£a-5*£b)*£e)/10000]}£s £r$'],
      ['$P_{\\overline{M}}(O)=\\frac{P(\\overline{M}\\cap O)}{P(\\overline{M})}=\\frac{calcule[5*£a*(100-£b)/10000]}{calcule[(5*£a*(100-£c)+5*£b*(100-£d)+(100-5*£a-5*£b)*(100-£e))/10000]}£s £r$',
        '$P_{\\overline{M}}(A)=\\frac{P(\\overline{M}\\cap A)}{P(\\overline{M})}=\\frac{calcule[5*£b*(100-£d)/10000]}{calcule[(5*£a*(100-£c)+5*£b*(100-£d)+(100-5*£a-5*£b)*(100-£e))/10000]}£s £r$',
        '$P_{\\overline{M}}(L)=\\frac{P(\\overline{M}\\cap L)}{P(\\overline{M})}=\\frac{calcule[(100-5*£a-5*£b)*(100-£e)/10000]}{calcule[(5*£a*(100-£c)+5*£b*(100-£d)+(100-5*£a-5*£b)*(100-£e))/10000]}£s £r$']],
      arrondiCond: '3'// on arrondi au pire à 10^{-3} près le résultat

    }
  ]
}
