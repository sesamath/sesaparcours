export default {
  txt: 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAUeAAACygAAAQEAAAAAAAAAAQAAACT#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAVuYnZhcgABNQAAAAFAFAAAAAAAAAAAAAIA#####wAGbmJjYXMxAAE1AAAAAUAUAAAAAAAAAAAAAgD#####AAZuYmNhczIAATUAAAABQBQAAAAAAAAAAAACAP####8ABm5iY2FzMwABNQAAAAFAFAAAAAAAAAAAAAIA#####wAGbmJjYXM0AAE1AAAAAUAUAAAAAAAAAAAAAgD#####AAZuYmNhczUAATIAAAABQAAAAAAAAAAAAAACAP####8AAnIxABNpbnQocmFuZCgwKSpuYmNhczEp#####wAAAAIACUNGb25jdGlvbgL#####AAAAAQAKQ09wZXJhdGlvbgIAAAADEQAAAAEAAAAAAAAAAD+zcpE1VJ0A#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAIAAAACAP####8AAnIyABNpbnQocmFuZCgwKSpuYmNhczIpAAAAAwIAAAAEAgAAAAMRAAAAAQAAAAAAAAAAP9goOG+qx9wAAAAFAAAAAwAAAAIA#####wACcjMAE2ludChyYW5kKDApKm5iY2FzMykAAAADAgAAAAQCAAAAAxEAAAABAAAAAAAAAAA#5k2e1TVIVgAAAAUAAAAEAAAAAgD#####AAJyNAATaW50KHJhbmQoMCkqbmJjYXM0KQAAAAMCAAAABAIAAAADEQAAAAEAAAAAAAAAAD#rBxrmpvUQAAAABQAAAAUAAAACAP####8AAnI1ABNpbnQocmFuZCgwKSpuYmNhczUpAAAAAwIAAAAEAgAAAAMRAAAAAQAAAAAAAAAAP++CBA1YH0wAAAAFAAAABgAAAAIA#####wABYwAEMStyMQAAAAQAAAAAAT#wAAAAAAAAAAAABQAAAAcAAAACAP####8AAWQAEXNpKGM9MSwyK3IyLDErcjIp#####wAAAAEADUNGb25jdGlvbjNWYXIAAAAABAgAAAAFAAAADAAAAAE#8AAAAAAAAAAAAAQAAAAAAUAAAAAAAAAAAAAABQAAAAgAAAAEAAAAAAE#8AAAAAAAAAAAAAUAAAAIAAAAAgD#####AAF4AAQxK3IzAAAABAAAAAABP#AAAAAAAAAAAAAFAAAACQAAAAIA#####wABeQARc2koeD0xLDIrcjQsMStyNCkAAAAGAAAAAAQIAAAABQAAAA4AAAABP#AAAAAAAAAAAAAEAAAAAAFAAAAAAAAAAAAAAAUAAAAKAAAABAAAAAABP#AAAAAAAAAAAAAFAAAACgAAAAIA#####wABZgAEMStyNQAAAAQAAAAAAT#wAAAAAAAAAAAABQAAAAsAAAACAP####8ABGZvcm0AAWYAAAAFAAAAEAAAAAIA#####wADc29sAAdjKngrZCp5AAAABAAAAAAEAgAAAAUAAAAMAAAABQAAAA4AAAAEAgAAAAUAAAANAAAABQAAAA8AAAACAP####8ABnZhbHNvbAADc29sAAAABQAAABIAAAACAP####8ABWZvcm0xAAZmb3JtPTEAAAAECAAAAAUAAAARAAAAAT#wAAAAAAAA#####wAAAAEACUNGb25jTlZhcgD#####AAJmMQAHYyp4K2QqeQAAAAQAAAAABAIAAAAFAAAADP####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAAEAgAAAAUAAAANAAAACAAAAAEAAAACAAF4AAF5AAAABwD#####AAJmMgAHYyphK2QqYgAAAAQAAAAABAIAAAAFAAAADAAAAAgAAAAAAAAABAIAAAAFAAAADQAAAAgAAAABAAAAAgABYQABYv####8AAAACAAZDTGF0ZXgA#####wEAAAABAAD#####EEB7WAAAAAAAQCgAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAACxcSWZ7Zm9ybTF9CnsKXEZvclNpbXB7ZjF9Cn0KewpcRm9yU2ltcHtmMn0KfQAAAAkA#####wEAAAABAAD#####EECADAAAAAAAQBgAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAACZcSWZ7Zm9ybTF9CnsKeD1cVmFse3h9Cn0KewphPVxWYWx7eH0KfQAAAAkA#####wEAAAABAAD#####EECBrAAAAAAAQCAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAACZcSWZ7Zm9ybTF9CnsKeT1cVmFse3l9Cn0KewpiPVxWYWx7eX0KfQAAAAIA#####wADcmVwAAEwAAAAAQAAAAAAAAAA#####wAAAAQAEENUZXN0RXF1aXZhbGVuY2UA#####wAGcmVzb2x1AAAAEwAAABoBAAAAAAE#8AAAAAAAAAEBAQAAAAIA#####wAFZXhhY3QAB3JlcD1zb2wAAAAECAAAAAUAAAAaAAAABQAAABIAAAACAP####8AB3JlcG9uc2UAGnNpKHJlc29sdSwxLHNpKGV4YWN0LDIsMCkpAAAABgAAAAAFAAAAGwAAAAE#8AAAAAAAAAAAAAYAAAAABQAAABwAAAABQAAAAAAAAAAAAAABAAAAAAAAAAAAAAACAP####8AAmN4AANjKngAAAAEAgAAAAUAAAAMAAAABQAAAA4AAAACAP####8AAmR5AANkKnkAAAAEAgAAAAUAAAANAAAABQAAAA8AAAACAP####8ABGZvcjEAB2MqeCtkKnkAAAAEAAAAAAQCAAAABQAAAAwAAAAFAAAADgAAAAQCAAAABQAAAA0AAAAFAAAADwAAAAIA#####wAEZm9yMgAFY3grZHkAAAAEAAAAAAUAAAAeAAAABQAAAB8AAAAKAP####8AA3RlcQAAACAAAAAhAQEAAAABP#AAAAAAAAABAQEAAAAJAP####8AAAD#AQAIc29sdXRpb27#####GEAWAAAAAAAAQCYAAAAAAAABAe#v+wAAAAAAAAAAAAAAAQAAAAAAAAAAALxcYmVnaW57YXJyYXl9e2x9Clx0ZXh0e0ljaSAkXElme2Zvcm0xfXtcRm9yU2ltcHtmMX19e1xGb3JTaW1we2YyfX09XEZvclNpbXB7Zm9yMX1cSWZ7dGVxfXt9ez1cRm9yU2ltcHtmb3IyfX0kfQpcXCBcdGV4dHtkb25jICRcSWZ7Zm9ybTF9e1xGb3JTaW1we2YxfX17XEZvclNpbXB7ZjJ9fT1cVmFse3NvbH0kfQpcZW5ke2FycmF5ff###############w==',
  titre: 'Calculer mentalement',
  param: 'cdf',
  entete: '',
  symbexact: '=',
  symbnonexact: '\\ne',
  btnFrac: false,
  btnPuis: false,
  btnRac: false,
  bigSize: true,
  width: 0,
  height: 0,
  nbLatex: 3,
  consigne1: 'Il faut calculer la valeur de $£a$ quand $£b$ et $£c$.',
  consigne2: '',
  consigne3: '',
  consigne4: '<br>Appuyer sur la touche Entrée ou cliquer sur le bouton OK.',
  charset: '0123456789.,+\\-/\\*'
}
// Utilise le squelette sectionsquelettemtg32_Calc_Param_
// Demande decalculer mentalement cx+dy où ca+db en donnant les valeurs de x et y (ou de et et b)
