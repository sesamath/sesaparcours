export const titre = 'Algorithmique avec Psylvia : initiation'
export const exos = [
  {
    enonce: 'En entrée n,<br>En sortie, on demande x égal au double de n.',
    algoinit: 'demander n\n?*n→x\necrire x',
    algosolution: 'demander n\n2*n→x\necrire x',
    variablesentree: ['n'],
    variablessortie: ['x'],
    verif: '[5;15]'

  },
  {
    enonce: 'En entrée n,<br>En sortie, on demande x égal au carré de n.',
    algoinit: 'demander n\n?→?\necrire x',
    algosolution: 'demander n\nn*n→x\necrire x',
    variablesentree: ['n'],
    variablessortie: ['x'],
    verif: '[1;100]'

  },
  {
    enonce: 'En entrée a et b,<br>En sortie, on demande x égal à la somme de a et b.',
    algoinit: 'demander a\ndemander b\n?→?\necrire x',
    algosolution: 'demander a\ndemander b\na+b→x\necrire x',
    variablesentree: ['a', 'b'],
    variablessortie: ['x'],
    verif: '[1;100]'

  }
]
