export default {
  enonce: 'En complétant de l’algorithme ci-contre, déterminer la somme des n premiers nombres entiers où n est un paramètre',
  algoinit: 'demander n\nn→x\necrire x',
  algosolution: 'demander n\n0→x\npour k de 1 à n\nx+k→x\nfin pour\necrire x',
  variablesentree: ['n'],
  variablessortie: ['x'],
  verif: '[1;100]'

}
