export default {
  partie1: 'Pour retenir les limites de la fonction exp, il suffit de bien connaître l’allure de sa courbe représentative :',
  fonction: 'exp(x)',
  borne_inf: -10,
  borne_sup: 5,
  nb_points: 100,
  partie2: 'Formules à retenir:<br>$\\limite{x}{-\\infty}{\\mathrm{e}^x}=0$ et $\\limite{x}{+\\infty}{\\mathrm{e}^x}=+\\infty$',
  partie3: 'De plus $\\limite{x}{+\\infty}{\\frac{\\mathrm{e}^x}{x}}=+\\infty$ et $\\limite{x}{-\\infty}{x\\mathrm{e}^x}=0$ :<br>on dit que $\\mathrm{e}^x$ l’emporte sur $x$ en l’infini.'
}
