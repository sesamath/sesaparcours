export default {
  partie1: 'Pour retenir les limites de la fonction ln, il suffit de bien connaître l’allure de sa courbe représentative :',
  fonction: 'ln(x)',
  borne_inf: 0.01,
  borne_sup: 15,
  nb_points: 100,
  partie2: 'Formules à retenir:<br>$\\limite{x}{0}{\\ln(x)}=-\\infty$ et $\\limite{x}{+\\infty}{\\ln(x)}=+\\infty$',
  partie3: 'De plus $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$ : on dit que $x$ l’emporte sur $\\ln x$ en $+\\infty$.',
  partie4: 'Nous avons également le résultat suivant : $\\limite{x}{0}{x\\ln x}=0$.'
}
