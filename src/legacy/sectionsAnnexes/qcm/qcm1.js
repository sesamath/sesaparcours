export default {
  qcms: [
    {
      // la consigne du QCM 1:
      consigne: 'On pose $f(x)=\\frac{1}{£ax^2+£b}$. Quelle est l’expression de sa fonction dérivée ?<br>',
      // on précise les fourchettes des variables entières
      variables: [['a', '[2;15]'], ['b', '[2;15]']],
      // les réponses possibles du QCM (1 pour juste, 0 pour faux):
      reponses: [['$\\frac{-calcule[2*£a]x}{(£ax^2+£b)^2}$', true], ['$\\frac{1}{(£ax^2+£b)^2}$', false], ['$\\frac{1}{calcule[2*£a]x}$', false], ['$\\frac{1}{calcule[2*£a]x+£b}$', false]],
      // correction détaillée :
      correction: "On utilise la formule de la dérivée de $\\frac{1}{v}$, dont l’expression est $\\frac{-v'}{v^2}$ on obtient ainsi comme résultat  $\\frac{-calcule[2*£a]x}{(£ax^2+£b)^2}$."
    },
    {
      // la consigne du QCM 2:
      consigne: 'On pose $f(x)=£ax^2+£bx+£c$.<br>Quelle est l’expression de sa fonction dérivée ?<br>',
      // on précise les fourchettes des variables entières
      variables: [['a', '[2;15]'], ['b', '[2;15]'], ['c', '[2;15]']],
      // les réponses possibles du QCM (1 pour juste, 0 pour faux):
      reponses: [['$calcule[2*£a]x+£b$', true], ['$calcule[2*£a]x+£b+£c$', false], ['$£ax^2+£bx$', false]],
      // correction détaillée :
      correction: 'On utilise la formule de la dérivée de $x^n$ pour $n$ entier strictement positif, dont l’expression est $nx^{n-1}$ on obtient ainsi comme résultat  $calcule[2*£a]x+£b$.'

    }
  ]
}
