export default {
  consigne: "<b>Le programme doit afficher le texte:</b><br><span class='grasbleu'>_phrase_</span><br><br>Modifiez le code en conséquence...<br><br><span class='grasrouge'><i>Exécutez votre programme et cliquez sur OK pour valider votre réponse.</i></span>",
  parametres: 'a:nombre$s:chaine',
  solution: "print('_phrase_');",
  solutionalgobox: 'VARIABLES$DEBUT_ALGORITHME$AFFICHER _phrase_$FIN_ALGORITHME',
  aide: "La fonction <span class='grasrouge'>AFFICHER</span> permet d’afficher une chaîne.<br><br>$Par exemple,<br>$<span class='grasbleu'>AFFICHER 'mathenpoche'</span><br>$permet d’obtenir dans la sortie :<br>$<span class='grasbleu'>mathenpoche</span><br><br>$Attention, les lignes :<br>$<b>VARIABLES</b><br>$<b>DEBUT_ALGORITHME</b><br>$<b>FIN_ALGORITHME</b><br>$sont <span class='grasrouge'>obligatoires</span>",
  remarques: "VARIABLES$DEBUT_ALGORITHME$AFFICHER 'ceci est un texte'$FIN_ALGORITHME"
}
