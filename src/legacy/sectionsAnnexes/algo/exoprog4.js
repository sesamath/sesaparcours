export default {
  consigne: "<b>Le programme demande votre prénom.</b><br>$Ce prénom doit être stocké dans un variable appelée <span class='grasrouge'>s</span><br>$Puis il affiche votre prénom :<br>$<span class='grasbleu'>jean</span><br>$si on saisi le prénom <span class='grasbleu'>jean</span> par exemple.<br><br>$Complétez le code !<br><br>$<span class='grasrouge'><i>Exécutez votre programme et cliquez sur OK pour valider votre réponse.</i></span>",
  parametres: 's:chaine',
  solution: "var s;$s=input('Entrez s');$print(s);",
  solutionalgobox: 'VARIABLES$s EST_DU_TYPE CHAINE$DEBUT_ALGORITHME$LIRE s$AFFICHER s$FIN_ALGORITHME',
  aide: "Dans AlgoBox, <span class='grasrouge'>il faut déclarer les variables</span>.<br><br>$Par exemple, si on utilise une variable <span class='grasrouge'>s</span> de type <span class='grasrouge'>CHAINE</span> alors il faut la déclarer comme ceci: <br>$<span class='grasbleu'>s EST_DU_TYPE CHAINE</span><br>$<br>$Pour <span class='grasrouge'>entrer la valeur d’une variable déclarée</span>, on utilise l’instruction <g>LIRE</g>:<br>$<span class='grasbleu'>LIRE s</span><br>",
  remarques: 'VARIABLES$DEBUT_ALGORITHME$FIN_ALGORITHME'
}
