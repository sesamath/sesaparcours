export default {
  consigne: "<b>Le programme demande votre prénom, puis votre nom.</b><br><br>$Ce prénom doit être stocké dans un variable appelée <span class='grasrouge'>s1</span><br>$et le nom doit être stocké dans un variable appelée <span class='grasrouge'>s2</span><br><br>$<b>Puis il affiche la chaîne :</b><br>$<span class='grasbleu'>bonjour jean martin</span><br>$si on saisi le prénom jean et le nom martin par exemple.<br><br>$<i>Attention aux espaces devant être présents dans la chaîne.</i>",
  parametres: 's1:chaine$s2:chaine',
  solution: "var s1,s2;$s1=input('Entrez s1');$s2=input('Entrez s2');$print('bonjour '+s1+' '+s2);",
  solutionalgobox: "VARIABLES$s1 EST_DU_TYPE CHAINE$s2 EST_DU_TYPE CHAINE$DEBUT_ALGORITHME$LIRE s1$LIRE s2$AFFICHER 'bonjour '$AFFICHER s1$AFFICHER ' '$AFFICHER s2$FIN_ALGORITHME",
  aide: "L’instruction <span class='grasrouge'>AFFICHER</span> affiche la valeur d’une variable ou une chaîne,<br>$mais <span class='grasrouge'>SANS faire un retour à la ligne</span>.<br><br>$Par exemple si on a :<br>$s1 qui vaut 'lycée' et s2 qui vaut 'au'<br>$alors les instructions<br>$<span class='grasbleu'>AFFICHE s2</span><br>$<span class='grasbleu'>AFFICHE s1</span><br>$donneront<br>$<span class='grasbleu'>aulycée</span><br>$c’est à dire sans espace et sans retour à la ligne.",
  remarques: "VARIABLES$s1 ???$s2 ???$DEBUT_ALGORITHME$LIRE ???$LIRE ???$??? 'bonjour '$AFFICHER ?$AFFICHER ' '$AFFICHER ?$FIN_ALGORITHME"
}
