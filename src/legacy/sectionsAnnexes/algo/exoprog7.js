export default {
  consigne: "Le programme demande un nombre.<br>$Ce nombre doit être stocké dans un variable appelée <span class='grasrouge'>a</span><br>$Il calcule alors le double du nombre <span class='grasrouge'>a</span><br> et affiche le résultat:<br>$<span class='grasbleu'>28</span><br>$si on a saisi le nombre 14 par exemple.",
  parametres: 'a:4',
  solution: "var a,m;$a=inputNumber('Entrez a');$m = 2*a;$print(m);",
  solutionalgobox: 'VARIABLES$a EST_DU_TYPE NOMBRE$m EST_DU_TYPE NOMBRE$DEBUT_ALGORITHME$LIRE a$m PREND_LA_VALEUR 2*a$AFFICHER m$FIN_ALGORITHME',
  aide: "L’instruction <span class='grasrouge'>PREND_LA_VALEUR'</span> permet d’affecter une valeur à une variable.<br>$Attention à ne pas oublier de déclarer la variable cependant.<br><br>$Exemple<br>$<span class='grasbleu'>a EST_DU_TYPE NOMBRE</span><br>$<span class='grasbleu'>b EST_DU_TYPE NOMBRE</span><br>$<span class='grasbleu'>a PREND_LA_VALEUR 12</span><br>$<span class='grasbleu'>b PREND_LA_VALEUR 2*a</span><br><br>$Alors <span class='grasrouge'>b sera égal à 24</span><br>",
  remarques: 'VARIABLES$a ???$m ???$DEBUT_ALGORITHME$???$m PREND_LA_VALEUR 2*a$???$FIN_ALGORITHME'
}
