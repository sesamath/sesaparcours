// Apprendre les carrés de 1 à 10
export default {
  txt: 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAL2AAACOAAAAQEAAAAAAAAAAQAAAAn#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhABBpbnQocmFuZCgwKSo5KSsx#####wAAAAEACkNPcGVyYXRpb24A#####wAAAAIACUNGb25jdGlvbgIAAAADAgAAAAQRAAAAAQAAAAAAAAAAP+wP2HP0s4oAAAABQCIAAAAAAAAAAAABP#AAAAAAAAAAAAACAP####8AAmEyAANhXjL#####AAAAAQAKQ1B1aXNzYW5jZf####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAABAAAAAUAAAAAAAAAAAAAAAgD#####AANyZXAAATkAAAABQCIAAAAAAAAAAAACAP####8ABWV4YWN0AAZyZXA9YTIAAAADCAAAAAYAAAADAAAABgAAAAIAAAACAP####8AB3JlcG9uc2UADXNpKGV4YWN0LDEsMCn#####AAAAAQANQ0ZvbmN0aW9uM1ZhcgAAAAAGAAAABAAAAAE#8AAAAAAAAAAAAAEAAAAAAAAAAP####8AAAACAAZDTGF0ZXgA#####wEAAAAB#####xBAgUQAAAAAAEA+hR64UeuGAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAHXFZhbHthfQAAAAgA#####wEAAP8B#####xpAHgAAAAAAAEAjCj1wo9cMAQHv7#sAAAAAAAAAAAAAAAEAAAAAAAAAAAB2XGJlZ2lue2FycmF5fXtsfQpcdGV4dHtMZSBjYXJyfVxhY3V0ZXtlfSBcdGV4dHsgZGUgfVxWYWx7YX1cdGV4dHsgZXN0IH0gXFZhbHthMn0gClxcIFxGb3JTaW1we2EyfT1cVmFse2EyfQpcZW5ke2FycmF5ff####8AAAABABBDTWFjcm9BcHBhcml0aW9uAP####8BAAD#Af####8QQB4AAAAAAABAYJCj1wo9cQIAAAAAAAAAAAAAAAABAAAAAAAAAAAACHNvbHV0aW9uAAAAAAABAAAABwD###############8=',
  titre: 'Apprendre les carrés parfaits.',
  param: '',
  entete: '',
  symbexact: '\\text{ a pour carré :}',
  symbnonexact: '\\text{ n’a pas pour carré :}',
  btnFrac: false,
  btnPuis: false,
  numero: 1,
  consigne1: 'Quel est le carré de $£a$?',
  consigne3: '',
  consigne4: '<br>Appuie sur la touche Entrée pour valider ta réponse.',
  charset: '=0123456789'
}
// Utilise le squelette sectionsquelettemtg32_Calc_Param
