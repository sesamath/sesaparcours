//  Demande la racine carrée de carrés parfait de 1 à 12 sans utiliser la mot racine
export default {
  txt: 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAL2AAACOAAAAQEAAAAAAAAAAQAAAAn#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhABFpbnQocmFuZCgwKSoxMSkrMf####8AAAABAApDT3BlcmF0aW9uAP####8AAAACAAlDRm9uY3Rpb24CAAAAAwIAAAAEEQAAAAEAAAAAAAAAAD#pHrrozoSqAAAAAUAmAAAAAAAAAAAAAT#wAAAAAAAAAAAAAgD#####AAJhMgADYV4y#####wAAAAEACkNQdWlzc2FuY2X#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAAQAAAAFAAAAAAAAAAP####8AAAACAAZDTGF0ZXgA#####wEAAAAB#####xBAgUQAAAAAAEA+hR64UeuGAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAKXFZhbHthMiw2fQAAAAIA#####wADcmVwAAE5AAAAAUAiAAAAAAAAAAAAAgD#####AAVleGFjdAAFcmVwPWEAAAADCAAAAAYAAAAEAAAABgAAAAEAAAACAP####8AB3JlcG9uc2UADXNpKGV4YWN0LDEsMCn#####AAAAAQANQ0ZvbmN0aW9uM1ZhcgAAAAAGAAAABQAAAAE#8AAAAAAAAAAAAAEAAAAAAAAAAAAAAAcA#####wEAAP8B#####xpAGgAAAAAAAEAMKPXCj1wwAQHv7#sAAAAAAAAAAAAAAAEAAAAAAAAAAAB0XGJlZ2lue2FycmF5fXtsfQpcVmFse2EyLDZ9XHRleHR7IGVzdCBsZSBjYXJyfVxhY3V0ZXtlfSBcdGV4dHsgZGUgfVxWYWx7YSw2fSAKXFwgXEZvclNpbXB7YTJ9PVxWYWx7YTIsNn0KXGVuZHthcnJheX3#####AAAAAQAQQ01hY3JvQXBwYXJpdGlvbgD#####AQAAAAH#####EEAuAAAAAAAAQF7AAAAAAAACAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAhzb2x1dGlvbgAAAAAAAQAAAAcA################',
  titre: 'Connaître les carrés parfaits.',
  param: '',
  entete: '',
  symbexact: '\\text{ est le carré de :}',
  symbnonexact: '\\text{ n’est pas le carré de :}',
  btnFrac: false,
  btnPuis: false,
  numero: 1,
  consigne1: 'Quel nombre positif a pour carré $£a$?',
  consigne3: '',
  consigne4: '<br>Appuie sur la touche Entrée pour valider ta réponse.',
  charset: '=0123456789'
}
// Utilise le squelette sectionsquelettemtg32_Calc_Param
