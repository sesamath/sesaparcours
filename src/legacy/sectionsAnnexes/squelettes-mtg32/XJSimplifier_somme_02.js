//   Simplifier a x + b y avec b négatif mais résultat positif
export default {
  txt: 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAL2AAACOAAAAQEAAAAAAAAAAQAAABT#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhABBpbnQocmFuZCgwKSo5KSsx#####wAAAAEACkNPcGVyYXRpb24A#####wAAAAIACUNGb25jdGlvbgIAAAADAgAAAAQRAAAAAQAAAAAAAAAAP98XkR9ZFRAAAAABQCIAAAAAAAAAAAABP#AAAAAAAAAAAAACAP####8AAWIALSgtMSleKGludChyYW5kKDApKjEuOTkpKzEpKihpbnQocmFuZCgwKSo5KSsxKQAAAAMC#####wAAAAEACkNQdWlzc2FuY2X#####AAAAAQAMQ01vaW5zVW5haXJlAAAAAT#wAAAAAAAAAAAAAwAAAAAEAgAAAAMCAAAABBEAAAABAAAAAAAAAAA#2LwiOj3hOAAAAAE##9cKPXCj1wAAAAE#8AAAAAAAAAAAAAMAAAAABAIAAAADAgAAAAQRAAAAAQAAAAAAAAAAP+6pCpTQt7gAAAABQCIAAAAAAAAAAAABP#AAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wAEemVybwASYWJzKHgpPDAuMDAwMDAwMDAxAAAAAwQAAAAEAP####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAABPhEuC+gm1pUAAXgAAAAHAP####8ABGZvcm0AB2EqeCtiKngAAAADAAAAAAMC#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAEAAAAIAAAAAAAAAAMCAAAACQAAAAIAAAAIAAAAAAABeAAAAAIA#####wABbQADYStiAAAAAwAAAAAJAAAAAQAAAAkAAAACAAAABwD#####AANzb2wAA20qeAAAAAMCAAAACQAAAAUAAAAIAAAAAAABeAAAAAcA#####wADcmVwAAE3AAAAAUAcAAAAAAAAAAF4#####wAAAAMAEENUZXN0RXF1aXZhbGVuY2UA#####wAGcmVzb2x1AAAABgAAAAcBAAAAAAE#8AAAAAAAAAEAAAACAP####8AAXgAG3NpKHplcm8oc29sKDEpLXJlcCgxKSksMiwxKf####8AAAABAA1DRm9uY3Rpb24zVmFyAP####8AAAABAA5DQXBwZWxGb25jdGlvbgAAAAMAAAADAQAAAAwAAAAGAAAAAT#wAAAAAAAAAAAADAAAAAcAAAABP#AAAAAAAAAAAAABQAAAAAAAAAAAAAABP#AAAAAAAAAAAAACAP####8AB2Zvcm11bGUAB2EqeCtiKngAAAADAAAAAAMCAAAACQAAAAEAAAAJAAAACQAAAAMCAAAACQAAAAIAAAAJAAAACQAAAAIA#####wAIY29udHJvbGUABnJlcCh4KQAAAAwAAAAHAAAACQAAAAkAAAACAP####8ABWV4YWN0ACd6ZXJvKHNvbCh4KS1yZXAoeCkpJnplcm8oc29sKDUpLXJlcCg1KSkAAAADCgAAAAwAAAADAAAAAwEAAAAMAAAABgAAAAkAAAAJAAAADAAAAAcAAAAJAAAACQAAAAwAAAADAAAAAwEAAAAMAAAABgAAAAFAFAAAAAAAAAAAAAwAAAAHAAAAAUAUAAAAAAAAAAAAAgD#####AAdyZXBvbnNlABpzaShyZXNvbHUsMSxzaShleGFjdCwyLDApKQAAAAsAAAAACQAAAAgAAAABP#AAAAAAAAAAAAALAAAAAAkAAAAMAAAAAUAAAAAAAAAAAAAAAQAAAAAAAAAA#####wAAAAIABkNMYXRleAD#####AQAAAAH#####EEBsoAAAAAAAQEBhR64UeuEAAAAAAAAAAAAAAAAAAQAAAAAAAAAAABJcRm9yU2ltcHtmb3JtfQoKCgoAAAANAP####8BAAAAAf####8QQHOwAAAAAABAbSAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAB1xWYWx7eH0AAAANAP####8BAAAAAf####8QQFpAAAAAAABAa6AAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAADlxWYWx7Zm9ybXVsZX0KAAAADQD#####AQAAAAH#####EEB9oAAAAAAAQGZAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAA5cVmFse2NvbnRyb2xlfQAAAA0A#####wEAAP8B#####xRAMgAAAAAAAEAzwo9cKPXCAQHg4PgAAAAAAAAAAAAAAAEAAAAAAAAAAABnXGJlZ2lue2FycmF5fXtsfQpcdGV4dHtPbiBjb21wdGUgbGUgbm9tYnJlIGRlIH14ClxcIFxGb3JTaW1we2Zvcm19XHRleHR7ID0gfQpcRm9yU2ltcHtzb2x9CgpcZW5ke2FycmF5ff####8AAAABABBDTWFjcm9BcHBhcml0aW9uAP####8BAAD#Af####8QQDIAAAAAAABAabhR64UeuAIAAAAAAAAAAAAAAAABAAAAAAAAAAAACHNvbHV0aW9uAAAAAAABAAAAEgD###############8=',
  titre: 'Simplifier une somme.',
  param: '',
  entete: '$£a$ = ',
  fin: '',
  symbexact: '$£a$ = $£r$',
  symbnonexact: 'En prenant n’importe quelle valeur de $x$, $£a$ doit donner le même résultat que $£r$' +
                  '<br>Pour $x = £b$,  $£a = £c$ et $£r = £d$',
  btnFrac: false,
  btnPuis: false,
  btnRac: false,
  numero: 4,
  consigne1: 'Simplifie l’expression $A=£a$.',
  consigne2: '',
  consigne3: '',
  consigne4: '<br>Appuyer sur la touche Entrée pour valider toutes les étapes, puis sur le bouton OK après la réponse finale pour valider.',
  consigne5: '<br>Appuyer sur la touche Entrée pour valider la réponse.',
  charset: '=x()0123456789.,+\\-/*²^'
}
// Utilise le squelette sectionsectionsquelettemtg32_XJ
