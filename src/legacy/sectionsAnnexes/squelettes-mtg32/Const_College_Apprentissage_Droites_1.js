export default {
  param: '',
  fig: 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wAAAAAHAACAKgAAgHwAAIAPAACAEwAAgBIAAIAQAACASAAAAAAEoQAAArYAAAEBAAAAAAAAAAEAAAAO#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEACkNQb2ludEJhc2UA#####wAAAAAAEAABQQAAAAAAAAAAAEAIAAAAAAAACQABQFSAAAAAAABAYWPXCj1wpAAAAAIA#####wAAAAAAEAABQgAAAAAAAAAAAEAIAAAAAAAACQABQGwgAAAAAABAVUeuFHrhSAAAAAIA#####wAAAAAAEAABQwAAAAAAAAAAAEAIAAAAAAAACQABQGrAAAAAAABAaaPXCj1wpAAAAAIA#####wAAAAAAEAABRAAAAAAAAAAAAEAIAAAAAAAACQABQHfQAAAAAABAYEPXCj1wpAAAAAIA#####wAAAAAAEAABRQAAAAAAAAAAAEAIAAAAAAAACQABQGJAAAAAAABAcaHrhR64Uv####8AAAABAAlDRHJvaXRlQUIA#####wEAAP8AEAAAAQACAAAAAQAAAAL#####AAAAAQAIQ1NlZ21lbnQA#####wH#AAAAEAAAAQACAAAAAgAAAAT#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####AQB#AAAQAAABAAIAAAAFAAAABv####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BfwAAABAAAAEAAgAAAAMAAAAGAAAABgD#####Af8A#wAQAAABAAIAAAAEAAAAB#####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAAAGAAAACf####8AAAABAA1DRGVtaURyb2l0ZU9BAP####8BAP##AA0AAAEAAgAAAAEAAAAD#####wAAAAEAEENNYWNyb0FwcGFyaXRpb24A#####wEAAP8B#####xBAQQAAAAAAAEB3ceuFHrhSAgHv7#sAAAAAAAAAAAAAAAEAAAAAAAAAAAAKI1NvbHV0aW9uIwAAAAAABgAAAAYAAAAHAAAACAAAAAkAAAAKAAAADAD###############8=',
  width: 700,
  height: 550,
  titre: 'Construire des droites, segments, demi-droites.',
  nbLatex: 0,
  enonce: 'Avec les outils disponibles il faut créer la droite (AB), le segment [BD], la demi-droite [AC), la droite parallèle à (AB) passant par E, la droite perpendiculaire à (AB) passant par C, la perpendiculaire au segment [BD] passant par D.',
  nbsol: 1,
  titreSolution1: 'Solution :',
  solution1: 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAShAAACtgAAAQEAAAAAAAAAAQAAABD#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAQAAFBAAAAAAAAAAAAQAgAAAAAAAAJAAFAVIAAAAAAAEBhY9cKPXCkAAAAAgD#####AAAAAAAQAAFCAAAAAAAAAAAAQAgAAAAAAAAJAAFAbCAAAAAAAEBVR64UeuFIAAAAAgD#####AAAAAAAQAAFDAAAAAAAAAAAAQAgAAAAAAAAJAAFAasAAAAAAAEBpo9cKPXCkAAAAAgD#####AAAAAAAQAAFEAAAAAAAAAAAAQAgAAAAAAAAJAAFAd9AAAAAAAEBgQ9cKPXCkAAAAAgD#####AAAAAAAQAAFFAAAAAAAAAAAAQAgAAAAAAAAJAAFAYkAAAAAAAEBxoeuFHrhS#####wAAAAEACUNEcm9pdGVBQgD#####AAAA#wAQAAABAAIAAAABAAAAAv####8AAAABAAhDU2VnbWVudAD#####AP8AAAAQAAABAAIAAAACAAAABP####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8AAH8AABAAAAEAAgAAAAUAAAAG#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wB#AAAAEAAAAQACAAAAAwAAAAYAAAAGAP####8A#wD#ABAAAAEAAgAAAAQAAAAH#####wAAAAEAEENJbnREcm9pdGVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAAAYAAAAJ#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAT#xgJFvyXYVAAAACv####8AAAACABdDTWFycXVlQW5nbGVHZW9tZXRyaXF1ZQD#####AAAAAAACAAAAAUAwAAAAAAAAAAAAAwAAAAsAAAABAAAACQD#####AAAAAAACAAAAAUAwAAAAAAAAAAAAAgAAAAQAAAAM#####wAAAAEADUNEZW1pRHJvaXRlT0EA#####wAA##8AEAAAAQACAAAAAQAAAAP###############8=',
  width1: 700,
  height1: 500,
  nbLatex1: 0,
  explication1: 'Voici la figure demandée :'
}
// Utilise le squelette sectionsquelettemtg32_Ex_Const
