export default {
  fig: 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH###8BAP8AAAAADAAAgCcAAIAoAACAJQAAgA0AAIAPAACAEAAAgBoAAIBgAACATQAAgCIAAIAqAACAfAAAAAAFvAAAAzsAAAAAAAAAAAAAAAAAAAAY#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEACkNQb2ludEJhc2UA#####wAAAAAAEAABQQDAKgAAAAAAAEAWZmZmZmZmBQAAQEgAAAAAAABAZeAAAAAAAP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAAAkAAABAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAAQAAFPAAAAAAAAAAAAQBZmZmZmZmYFAABAYOAAAAAAAAAAAAIAAAACAP####8AAAAAABAAAUcAQBAAAAAAAADAOmZmZmZmZgUAAEBhAAAAAAAAQF+AAAAAAAD#####AAAAAQAHQ01pbGlldQD#####AQAA#wALAAFKAMAYAAAAAAAAwC7MzMzMzM0FAAAAAAEAAAAE#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAE#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAD#ACQAAADACAAAAAAAAMA6ZmZmZmZmBQAAAAAFAAAABv####8AAAABAAhDU2VnbWVudAD#####AAAAAAALAAABAQEAAAABAAAAAwAAAAgA#####wAAAAAACwAAAQEBAAAAAwAAAAQAAAAIAP####8BAAAAAAsAAAEBAQAAAAEAAAAFAAAACAD#####AQAAAAALAAABAQEAAAAFAAAABAAAAAgA#####wEAAAAACwAAAQEBAAAABAAAAAf#####AAAAAQAOQ01hcnF1ZVNlZ21lbnQA#####wH#AP8AAgAAAAAKAAAACQD#####Af8A#wACAAAAAAsAAAAJAP####8B#wD#AAIAAAAADP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAP8AAQAAAAMAAAABAAAABgD#####AAAAB#####8AAAABAAxDQ2VyY2xlSW1hZ2UA#####wEAfwAAAQAAABAAAAAR#####wAAAAEAEENJbnRDZXJjbGVDZXJjbGUA#####wAAABAAAAAS#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wH#AAAAJAAAAEAkAAAAAAAAwBWZmZmZmZoFAAEAAAATAAAADQD#####Af8AAAAkAAAAwDMAAAAAAADAJMzMzMzMzQUAAgAAABP#####AAAAAQAJQ1BvbHlnb25lAP####8B#wAAAAEAAAAEAAAAAQAAABQAAAAVAAAAAf####8AAAABABBDTWFjcm9BcHBhcml0aW9uAP####8B#wAAAf####8aQHBgAAAAAABAGAAAAAAAAAIAAAAAAAAAAAAACiNTb2x1dGlvbiMAByNjZXJjbGUAAAABAAAAFgD###############8=',
  width: 700,
  height: 550,
  titre: 'Trouver les deux sommets restants d’un triangle connaissant un sommet, le centre du cercle circonscrit et le centre de gravité',
  enonce: 'Avec les outils disponibles il faut créer le triangle ABC tels que G soit le centre de gravité<br>' +
        'du triangle ABC et O le centre de son cercle circonscrit.<br>' +
        'Pour créer le triangle, utiliser l’icône de  création de polygone.<br>' +
        'Cliquer sur OK quand la construction est finie.',
  nbsol: 1,
  solution1: 'TWF0aEdyYXBoSmF2YTEuMAAAABA#gAAAAANmcmH###8BAP8BAAAAAAAAAAAB4wAAAWYAAAAAAAAAAAAAAAAAAAAf#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEACkNQb2ludEJhc2UA#####wAAAAAACwABQQDAKgAAAAAAAEAWZmZmZmZmBQABQEgAAAAAAABAZeAAAAAAAP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAAALAAABAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAALAAFPAAAAAAAAAAAAQBZmZmZmZmYFAAFAYOAAAAAAAAAAAAIAAAACAP####8AAAAAAAsAAUcAwBAAAAAAAADANGZmZmZmZgUAAUBhAAAAAAAAQF+AAAAAAAD#####AAAAAQAHQ01pbGlldQD#####AAAA#wALAAFKAMAYAAAAAAAAwC7MzMzMzM0FAAAAAAEAAAAE#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAE#####wAAAAEAC0NQb2ludEltYWdlAP####8AAAD#AAsAAkEnAMAIAAAAAAAAwDpmZmZmZmYFAAAAAAUAAAAG#####wAAAAEACENTZWdtZW50AP####8AAAAAAAsAAAEBAQAAAAEAAAADAAAACAD#####AAAAAAALAAABAQEAAAADAAAABAAAAAgA#####wAAAAAACwAAAQEBAAAAAQAAAAUAAAAIAP####8AAAAAAAsAAAEBAQAAAAUAAAAEAAAACAD#####AAAAAAALAAABAQEAAAAEAAAAB#####8AAAABAA5DTWFycXVlU2VnbWVudAD#####AP8A#wACAAAAAAoAAAAJAP####8A#wD#AAIAAAAACwAAAAkA#####wD#AP8AAgAAAAAM#####wAAAAEACUNDZXJjbGVPQQD#####AAAA#wABAAAAAwAAAAEAAAAGAP####8AAAAH#####wAAAAEADENDZXJjbGVJbWFnZQD#####AAB#AAABAAAAEAAAABH#####AAAAAQAQQ0ludENlcmNsZUNlcmNsZQD#####AAAAEAAAABL#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AP8AAAALAAFDAEAkAAAAAAAAwBWZmZmZmZoFAAEAAAATAAAADQD#####AP8AAAALAAFCAMAwAAAAAAAAwCjMzMzMzM0FAAIAAAATAAAACAD#####AP8AAAALAAABAAEAAAABAAAAFQAAAAgA#####wD#AAAACwAAAQABAAAAFQAAABQAAAAIAP####8A#wAAAAsAAAEAAQAAABQAAAAB#####wAAAAEAD0NQb2ludExpZUNlcmNsZQD#####AAAAAAALAAFOAMAkAAAAAAAAwDhmZmZmZmYFAAFAAThkxo4higAAABAAAAAHAP####8AAAAAAAsAAAAAAAAAAAAAAD#jMzMzMzMzBQAAAAAZAAAAEQAAAAgA#####wAAf38ACwAAAQABAAAAGQAAAAcAAAAIAP####8AAH9#AAsAAAEAAQAAAAcAAAAaAAAACQD#####AAB#fwACAwAAABsAAAAJAP####8AAH9#AAIDAAAAHP###############w==',
  width1: 483,
  height1: 358,
  explication1: "Si on appelle A' le milieu du segment [BC] on sait que le point G<br>" +
        "est situé sur le segment [AA'] aux deux-tiers à partir de A.<br>" +
        "On crée donc le milieu J du segment [AG]. A' est alors le symétrique de J par rapport à G<br>" +
        "(on peut utiliser pour construire A' l’outil symétrie centrale ou l’outil translation).<br>" +
        'Comme O est le centre du cercle circonscrit au triangle ABC, on crée ce cercle circonscrit<br>' +
        'qui est le cercle de centre O et passant par A (en bleu sur la figure).<br>' +
        "Les points B et C sont alors sur ce cercle et on sait de plus que A' doit être le milieu de [BC].<br>" +
        "C est alors l’image de B dans la symétrie centrale de centre A'. B étant sur le cercle bleu<br>" +
        "son image C doit donc être sur le cercle image du cercle bleu dans la symétrie centrale de centre A'.<br>" +
        'On utilise donc l’outil de symétrie centrale et on crée le cercle image du cercle bleu<br>' +
        "dans la symétrie centrale de centre A' (en vert sur la figure).<br>" +
        'On crée ensuite l’intersection de ce cercle avec le cercle bleu.<br>' +
        'Le point C est alors nécessairement un des deux points d’intersection<br>' +
        'et B est l’autre point (on peut permuter les points B et C).<br>' +
        "<span style='text-decoration: underline;'>Remarque :</span><br>" +
        'Le point N est là uniquement pour illustrer le fait que le cercle vert<br>' +
        "est l’image du cercle bleu dans la symétrie centrale de centre A'.<br>" +
        'En capturant G on peut voir que la construction n’est pas toujours possible.<br>' +
        'Quand est-elle possible ? '
}
// Exercice de construction consistant à retrouver le centre d’un cercle donné
// Utilise le squelette sectionsquelettemtg32_Ex_Const
