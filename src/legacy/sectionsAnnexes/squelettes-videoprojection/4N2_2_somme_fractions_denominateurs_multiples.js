// À faire : une deuxième consigne en changeant l’ordre des termes

export default {
  intro: 'Calculer : ',
  titre: 'Somme de 2 fractions (dénominateurs multiples)',
  consigne: '$\\frac{£a}{calcule[£c*£k]}+\\frac{£b}{£c}$',
  variables: [['a', '[2;10]'], ['b', '[2;10]'], ['c', '[2;10]'], ['k', '[2;10]']],
  correction: '$=\\frac{£a}{calcule[£c*£k]}+\\frac{£b\\times{}£k}{£c\\times{}£k}=\\frac{calcule[£a+£b*£k]}{calcule[£c*£k]}$'
}
