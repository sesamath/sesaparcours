export default {
  intro: 'Calculer : ',
  titre: 'Produit de 2 nombres relatifs',
  consigne: '$(signe[£a])\\times(signe[£b])$',
  variables: [['a', '[-10;10]\\{1;0}'], ['b', '[-10;10]\\{1;0}']],
  correction: '$=calcule[£a*(£b)]$'
}
