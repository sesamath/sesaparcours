/* Ne fonctionne pas à partir de 10^7
Idée : crée une liste avec les corrections (pouvoir mettre les espaces...)

*/

export default {
  intro: 'Donner l’écriture décimale des nombres suivants : ',
  titre: 'Puissances de 10',
  consigne: '$10^{£n}$',
  variables: [['n', '[-9;9]']],
  correction: '$=calcule[10^(£n)]$'
}
