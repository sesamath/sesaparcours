// Remarques :
// * Encore le problème des parenthèses manquantes à un nombre négatif

export default {
  intro: 'Écrire les nombres suivants sous la forme $a^n$ : ',
  titre: 'Règles de calculs avec les puissances',
  consigne: '$£a^{£n}\\times{}£b^{£n}$',
  variables: [['a', '[2;10]'], ['b', '[2;10]'], ['n', '[-9;9]\\{0;1}']],
  correction: '$=(£a\\times{}£b)^{£n}=calcule[£a*(£b)]^{£n}$'
}
