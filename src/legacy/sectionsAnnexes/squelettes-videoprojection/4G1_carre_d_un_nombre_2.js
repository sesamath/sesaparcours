// Remarques :
// à mélanger avec l’autre exercice
//

export default {
  intro: 'Calculer : ',
  titre: 'Carré d’un nombre',
  consigne: '$£n^2$',
  variables: [['n', '[2;10]']],
  correction: '$=calcule[£n*(£n)]$'
}
