export default {
  intro: 'Simplifier au maximum les fractions suivantes : ',
  titre: 'Simplification de fractions',
  consigne: '$\\frac{calcule[£a*(£k)]}{calcule[£b*(£k)]}$',
  variables: [['a', [1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 9, 10, 10, 10]],
    ['b', [2, 3, 4, 5, 6, 7, 8, 9, 10, 3, 5, 7, 9, 2, 4, 5, 7, 8, 10, 3, 5, 7, 9, 2, 3, 4, 6, 7, 8, 9, 10, 5, 7, 3, 4, 5, 8, 9, 10, 3, 5, 7, 9, 2, 4, 5, 7, 10, 3, 7, 9]], ['k', '[2;10]']],
  variables_liees: true,
  correction: '$=\\frac{£k\\times{}£a}{£k\\times{}£b}=\\frac{£a}{£b}$'

}
