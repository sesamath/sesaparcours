/*

Remarques :
    * Comment simplifier le résultat ?
*/

export default {
  intro: 'Calculer : ',
  titre: 'Produit de 2 fractions',
  consigne: '$\\frac{£a}{£b}\\div\\frac{£c}{£d}$',
  variables: [['a', '[2;10]'], ['b', '[2;10]\\{£1}'], ['c', '[2;10]'], ['d', '[2;10]\\{£3}']],
  correction: '$=\\frac{£a}{£b}\\times\\frac{£d}{£c}=\\frac{calcule[£a*(£d)]}{calcule[£b*(£c)]}$'
}
