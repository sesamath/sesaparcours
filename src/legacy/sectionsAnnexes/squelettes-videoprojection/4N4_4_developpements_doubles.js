// Remarques :
// * Encore les problèmes de signe du style 5+-3
// * Mettre des couleurs dans le corrigé ?

export default {
  intro: 'Développer les expressions suivantes : ',
  titre: 'Double distributivité',
  consigne: '$(£a+£bx)(£c+£dx)$',
  variables: [['a', '[2;7]'], ['b', '[2;7]'], ['c', '[2;7]'], ['d', '[2;7]']],
  correction: '$=calcule[£a*(£c)]+calcule[£a*(£d)]x+calcule[£b*(£c)]x+calcule[£b*(£d)]x^2=calcule[£b*(£d)]x^2+calcule[£a*(£d)+£b*(£c)]x+calcule[£a*(£c)]$'
}
