// Remarques :
// * Encore les problèmes de signe du style 5+-3
// * Deuxième consigne avec le x comme premier terme dans la parenthèse
// * Cas particulier du moins devant la parenthèse

export default {
  intro: 'Développer les expressions suivantes : ',
  titre: 'Développement simple',
  consigne: '$£k(£a+£bx)$',
  variables: [['a', '[2;10]'], ['b', '[2;10]'], ['k', '[-9;9]\\{-1;0;1}']],
  correction: '$=calcule[£k*(£a)]+calcule[£k*(£b)]x$'
}
