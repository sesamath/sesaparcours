export default {
  intro: 'Calculer : ',
  titre: 'Somme de relatifs',
  consigne: '$(signe[£a])+(signe[£b])$',
  variables: [['a', '[-10;10]\\{-1;0;1}'], ['b', '[-10;10]\\{0;1}']],
  correction: '$=calcule[£a+£b]$'
}
