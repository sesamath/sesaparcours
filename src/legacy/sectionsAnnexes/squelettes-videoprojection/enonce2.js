export default {
  titre: 'Somme de relatifs',
  intro: 'Simplifie la fraction : :',
  consigne: '$\\frac{calcule[£a*(£k)]}{calcule[£b*(£k)]}$',
  variables: [['a', '[0;10]\\{-1;0;1}'], ['b', '[0;10]\\{-1;0;1;premier_avec_1}'], ['k', '[2;10]']],
  correction: '=$\\frac{£a\\times{}£k}{£b\\times{}£k}=\\frac{£a}{£b}$'
}
