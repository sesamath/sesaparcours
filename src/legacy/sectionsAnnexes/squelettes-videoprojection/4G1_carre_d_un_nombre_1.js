export default {
  intro: 'Compléter les égalités suivantes : ',
  titre: 'Carré d’un nombre',
  consigne: '$£l^2=calcule[£n*(£n)]$ donc $£l=$',
  variables: [['n', '[2;10]'], ['l', ['AB', 'CD', 'EF', 'x', 'y', 'MN', 'RS', 'JK', 'AC', 'MO', 'UV']]],
  correction: '$£n$'
}
