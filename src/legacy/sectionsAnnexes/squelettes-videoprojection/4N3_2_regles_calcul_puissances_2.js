// Remarques :
// * Il manque le cas particulier a^n/a sans écrire a^1
// * Pour le détail de la correction, il faudrait différencier le cas m>0 et m<0 pour écrire une somme algébrique

export default {
  intro: 'Écrire les nombres suivants sous la forme $a^n$ : ',
  titre: 'Règles de calculs avec les puissances',
  consigne: '$\\frac{£a^{£n}}{£a^{£m}}$',
  variables: [['a', '[2;10]'], ['n', '[-9;9]\\{0;1}'], ['m', '[-9;9]\\{0;1}']],
  correction: '$=£a^{£n-(signe[£m])}=£a^{calcule[£n-(£m)]}$'
}
