export default {
  titre: 'Simplification de fraction',
  intro: 'Simplifie au maximum les fractions suivantes :',
  consigne: '$\\frac{£a}{£b}$',
  variables: [['a', [2, 3, 5]], ['b', [12, 9, 10]], ['c', [1, 1, 1]], ['d', [6, 3, 2]]],
  variables_liees: true,
  correction: '=$\\frac{£c}{£d}$'
}

// possibilité :
// "variables":[["a","[2;10]"],["b","[2;10]\\{£1}"],["c","[2;10]"],["d","[2;10]\\{£3}"]],
// ["b","[2;10]\\{premier_avec_1"}]
