// Remarques :
// * Encore le problème des parenthèses manquantes à un nombre négatif

export default {
  intro: 'Écrire les nombres suivants sous la forme $a^n$ : ',
  titre: 'Règles de calculs avec les puissances',
  consigne: '$\\left(£a^{£n}\\right)^{£m}$',
  variables: [['a', '[2;10]'], ['n', '[-9;9]\\{0;1}'], ['m', '[-9;9]\\{0;1}']],
  correction: '$=£a^{£n\\times{}£m}=£a^{calcule[£n*(£m)]}$'
}
