export default {
  titre: 'Somme de relatifs',
  consigne: [['Calcule la somme suivante : (-2)+(+4)', 'Calcule la somme suivante : (-2)+(-5)'], ['Calcule la somme suivante : (-7)+(-3)', 'Calcule la somme suivante : (-8)+(+1)'], ['ABC est un triangle tel que AB=3cm, BC=4cm et AC=5 cm. Est-il rectangle ?', 'DEF est un triangle tel que DE=3,6cm, DF=4,8cm et EF=6 cm. Est-il rectangle ?']],
  variables: [],
  correction: [['(-2)+(+4)=+2', '(-2)+(-5)=-7'], ['(-7)+(-3)=-10', '(-8)+(+1)=-7'], ['$AB^2=9$, $BC^2=15$ et $AC^2=25$. On a donc $AB^2+BC^2=AC^2$ donc d’après le théorème de Pythagore, le triangle ABC est rectangle en B.', '$DE^2={3,6}^2=12,96$, $DF^2={4,8}^2=23,04$ et $EF^2=6^2=36$. On a donc $DE^2+DF^2=EF^2$ donc d’après le théorème de Pythagore le triangle DEF est rectangle en D.']]
}
