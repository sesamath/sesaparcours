export default {
  titre: 'Somme de relatifs',
  intro: 'Calcule la somme :',
  consigne: '($signe[£a])\\times(signe[£b])$',
  variables: [['a', '[-5;2]\\{-1;0;1}'], ['b', '[-3;7]\\{0;1}']],
  correction: '=$calcule[£a+£b]$'
}
