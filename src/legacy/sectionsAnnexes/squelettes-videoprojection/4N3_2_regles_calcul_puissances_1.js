// Remarques :
// * Il manque le cas particulier a^n*a=a^(n+1)
// * Dans la correction £n+£m peut donner 3+-5, il manque une fonction écrivant entre parenthèses un nombre négatif

export default {
  intro: 'Écrire les nombres suivants sous la forme $a^n$ : ',
  titre: 'Règles de calculs avec les puissances',
  consigne: '$£a^{£n}\\times{}£a^{£m}$',
  variables: [['a', '[2;10]'], ['n', '[-9;9]\\{0;1}'], ['m', '[-9;9]\\{0;1}']],
  correction: '$=£a^{£n+£m}=£a^{calcule[£n+£m]}$'
}
