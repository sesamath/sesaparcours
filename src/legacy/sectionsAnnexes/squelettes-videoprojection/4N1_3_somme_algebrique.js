export default {
  intro: 'Calculer : ',
  titre: 'Somme algébrique de 2 nombres relatifs',
  consigne: '$£asigne[£b]$',
  variables: [['a', '[-10;10]\\{-1;0;1}'], ['b', '[-10;10]\\{0;1}']],
  correction: '$=calcule[£a+£b]$'
}
