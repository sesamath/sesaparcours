/*
Remarques :
    * Comment simplifier le résultat ?
    * En attente de la fonction premier_avec_£1

Proposition d’Alexis :

 "consigne":"$\\frac{£a}{calcule[£b*(£e)]}\\times\\frac{calcule[£c*(£e)]}{£d}$",
 "variables":[["a","[2;10]"],["b","[2;10]"],["c","[2;10]"],["d","[2;10]"],["e","[2;10]"]],

Tu pourras au moins simplifier par e.
Ensuite j’ajouterai ["b","[2;10]"\\{premier_avec_1}] pour être sûr que a/b soit non simplifiable.

*/

export default {
  intro: 'Calculer : ',
  titre: 'Produit de 2 fractions',
  consigne: '$\\frac{£a}{£b}\\times\\frac{£c}{£d}$',
  variables: [['a', '[2;10]'], ['b', '[2;10]\\{£1}'], ['c', '[2;10]'], ['d', '[2;10]\\{£3}']],
  correction: '$=\\frac{£a\\times{}£c}{£b\\times£d}=\\frac{calcule[£a*(£c)]}{calcule[£b*(£d)]}$'
}
