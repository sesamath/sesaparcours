export default {
  intro: 'Calculer : ',
  titre: 'Quotient de 2 nombres relatifs',
  consigne: '$calcule[£k*(£a)]\\div{}£a$ ',
  variables: [['a', '[-10;10]\\{1;0}'], ['k', '[-9;9]\\{-1;0;1}']],
  correction: ' $=£k$'
}
