import { hasProp } from 'src/lib/utils/object'

import 'roboto-fontface/css/roboto/sass/roboto-fontface-regular.scss'
import 'roboto-fontface/css/roboto/sass/roboto-fontface-bold.scss'
// pour les titres (utilisé en css dans .meftdHG)
import '@fontsource/noto-emoji/400.css'

export const colorEnonce = '#000000'
export const colorOk = '#008A73'
export const colorKo = '#D64700'
export const colorCorrection = '#3366FF'

/**
 * Styles de j3p, utilisé uniquement par le constructeur Parcours
 * @param {string} [fontFamily=Roboto]
 * @constructor
 */
function StylesJ3p (fontFamily = 'Roboto') {
  const getValues = (size, color) => ({
    fontFamily,
    fontSize: size + 'px',
    color
  })
  const getGroupValues = (size) => ({
    enonce: getValues(size, colorEnonce),
    explications: getValues(size, colorCorrection),
    correction: getValues(size, colorCorrection)
  })

  const size = 32
  this.grand = getGroupValues(size)
  this.moyen = getGroupValues(size - 4)
  this.petit = getGroupValues(size - 8)
  this.toutpetit = getGroupValues(size - 12)

  this.cbien = colorOk
  this.cfaux = colorKo
  this.colorCorrection = colorCorrection
} // StylesJ3p

/**
   * Ajoute des propriétés CSS à un style de J3P
   * Exemple :
   * this.styles.etendre("moyen.enonce",{border:"1px solid black")
   * va retourner un objet CSS
   * J3PDiv(this.zones.MG,{
   *   id: 'parent',
   *   contenu: '',
   *   coord: [30,50],
   *   style: this.styles.etendre('moyen.enonce', {border:"1px solid #000",width:"400px",height:"350px","background":"#f0f0f0",padding:"5px"})
   * @param {string} prop séparer par des points pour descendre dans le style
   * @param {Object} style
   * @return {Object}
   */
StylesJ3p.prototype.etendre = function (prop, style) {
  let styleValue = this
  const props = prop.split('.')
  let p
  // on descend dans l’objet style courant d’après prop
  for (let k = 0; k < props.length; k++) {
    p = props[k]
    if (!hasProp(styleValue, p)) {
      console.error(Error('Impossible de trouver la propriété ' + p + ' (demandée via ' + prop + ')'))
      return style
    }
    if (typeof styleValue[p] !== 'object') {
      console.error(Error('La propriété ' + p + ' existe dans l’objet'), 'elle vaut', styleValue, 'mais ce n’est pas un objet', typeof styleValue[p], styleValue[p])
      return style
    }
    styleValue = styleValue[p]
  }
  return Object.assign({}, styleValue, style)
} // etendre

export default StylesJ3p

export const legacyStyles = new StylesJ3p()
