// @todo virer tous les ts-ignore et as … (ou bien commenter pour expliquer pourquoi ils sont nécessaires)
import $ from 'jquery'

import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pDimfenetre, j3pElement, j3pEmpty, j3pModale, j3pNotify, j3pSetProps, j3pShowError } from 'src/legacy/core/functions'
import StylesJ3p, { legacyStyles } from 'src/legacy/core/StylesJ3p'
import { loadThemeCss } from 'src/legacy/themes/index'
import { isLegacyConnector, isLegacyNode } from 'src/lib/core/checks'
import loadJqueryDialog from 'src/lib/core/loadJqueryDialog'
import textesGeneriques from 'src/lib/core/textes'
import Modal from 'src/lib/entities/dom/Modal'
import { ParamValue } from 'src/lib/entities/GraphNode'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { LegacyParcoursProp } from 'src/lib/player/types'
import { isArrayOfBooleans, isArrayOfNumbers, isArrayOfStrings } from 'src/lib/utils/array'
import { getDelayMs, getDelaySec } from 'src/lib/utils/date'
import { isHtmlElement } from 'src/lib/utils/dom/main'
import { hasProp, isPlainObject, stringify } from 'src/lib/utils/object'
import { isStringNotEmpty } from 'src/lib/utils/string'

import { EditGrapheOptsV1, LegacyBilan, LegacyConnector, LegacyContenu, LegacyFenetresJquery, LegacyGraph, LegacyNode, LegacyNodeOptions, LegacyNodeParams, LegacyResultat, LegacyResultatCallback, PlainObject, TimeoutId } from 'src/lib/types'
import type Calculatrice from 'src/legacy/outils/calculatrice/Calculatrice'
import type Tableur from 'src/legacy/outils/tableur/Tableur'

const { cBien, cFaux, parcoursTermine, reponseManquante } = textesGeneriques

// FIXME s’il on a du stor.donneesPersistantes lors de l’enregistrement d’un résultat non terminé, alors il faut le mettre dans le résultat et le gérer dans un démarrage avec un tel lastResultat (en l’état la reprise de graphe l’ignore)

const knownSesatheques = new Map()
knownSesatheques.set('bibliotheque.sesamath.net', 'sesabibli')
knownSesatheques.set('bibliotheque.sesamath.dev', 'sesabidev')
knownSesatheques.set('commun.sesamath.net', 'sesacommun')
knownSesatheques.set('commun.sesamath.dev', 'sesacomdev')

/**
 * La liste des thèmes existants, le premier est celui par défaut
 * @private
 */
const themes = ['standard', 'zonesAvecImageDeFond']
/** Thème par défaut */
const defaultTheme = themes[0]

const typeToConstructor = {
  string: String,
  number: Number,
  boolean: Boolean
}

const erreurInterne = () => j3pShowError('Erreur interne, désolé. Cet exercice (ou le suivant qui allait démarrer) présente une anomalie, fais une capture d’écran et explique à ton professeur comment c’est arrivé pour qu’il puisse le signaler.')

// bizarrement y’a plein de sections qui veulent ajouter un bouton continuer après avoir vidé la zone qui contient son conteneur…
// cette fct recrée BoutonsJ3P si besoin (et râle en console dans ce cas)
/**
 * Retourne le conteneur de bouton (le crée si besoin)
 * @private
 */
function getBoutonsContainer (parcours: Parcours): HTMLDivElement {
  const divBoutons = j3pElement('BoutonsJ3P', null) as HTMLDivElement | null
  if (divBoutons) {
    // on vérifie qu’on a la bonne ref
    if (parcours.buttonsElts.container !== divBoutons) {
      console.error(Error('le conteneur des boutons n’est pas celui enregistré dans buttonsElts'))
      // on màj notre ref avec ce qu’il y a dans le dom
      parcours.buttonsElts.container = divBoutons
    }
    return divBoutons
  }
  // on le crée
  console.error(Error('Il n’ avait pas de conteneur #BoutonsJ3P, on le crée'))
  return parcours.ajouteBoutons()
}

/**
 * Normalise nbrepetitions, nbetapes, nbchances (init à 1 si non fournis) et calcule nbitems d’après ces valeurs
 * @private
 */
function checkDonneesSection (parcours: Parcours): void {
  if (typeof parcours.donneesSection !== 'object') throw Error('Erreur d’initialisation')
  // ils sont facultatifs, nbchances peut être 0 (illimité)
  for (const p of ['nbrepetitions', 'nbetapes', 'nbchances', 'limite']) {
    const zeroAllowed = (p === 'nbchances' || p === 'limite')
    const min = zeroAllowed ? 0 : 1
    const max = zeroAllowed ? 1000 : 100
    const defaultValue = p === 'limite' ? 0 : 1
    const value = parcours.donneesSection[p]
    if (typeof value === 'number' && Number.isInteger(value) && value >= min && value <= max) continue // rien à corriger c’est tout bon
    if (value == null) {
      parcours.donneesSection[p] = defaultValue
      continue
    }
    if (typeof value === 'string') {
      const v = Number(value)
      if (v >= min && v <= max) {
        console.warn(p + ' doit être de type number, string fournie')
        parcours.donneesSection[p] = Number(value)
        continue
      }
    }
    console.error(Error(`${p} invalide : ${value} (${typeof value}), ${defaultValue} imposé`))
    parcours.donneesSection[p] = defaultValue
  }
  // nbitems toujours imposé par les deux autres
  parcours.donneesSection.nbitems = parcours.donneesSection.nbetapes * parcours.donneesSection.nbrepetitions
}

/**
 * Attend qu’un test soit ok avant de passer à la suite
 * @private
 * @param validator
 * @param todoThen
 * @param until timestamp (en ms) du moment max (si dépassé on timeout)
 */
function waitFor (validator: Function, todoThen: Function, until: number): void {
  if (typeof validator !== 'function' || typeof todoThen !== 'function' || typeof until !== 'number') throw Error('arguments invalides')
  if (validator()) return todoThen()
  if (Date.now() > until) return j3pShowError('Délai d’attente max dépassé, Abandon')
  // pas encore bon et pas de timeout, on se rappellera dans 1/4s
  setTimeout(waitFor.bind(null, validator, todoThen, until), 250)
}

/**
 * Retourne un graphe normalisé (id en string, nn et snn aussi, fin en minuscule, nœuds fin ajoutés avec nn et snn toujours des id, jamais "fin", elt d'index 0 est un array vide)
 * Retournera null en cas de pb. Attention, l'objet graphe passé en argument peut être modifié.
 * @private
 */
function getNormalizedGraphe (graphe: unknown): LegacyGraph | null {
  const grapheOriginal = stringify(graphe)
  try {
    if (typeof graphe === 'string') {
      graphe = JSON.parse(graphe)
    }
    if (!Array.isArray(graphe)) throw Error('graphe invalide')
    if (!graphe.length) throw Error('graphe vide')
    // chaque élément suivant du graphe est un nœud, il doit être un array avec
    // - le numéro du nœud en string en 1er
    // - un nombre variable de branchements, des object (nombre éventuellement nul, mais à priori au moins un nœud fin)
    // - en dernier un objet avec le paramétrage du nœud
    const cleanGraphe: LegacyGraph = []
    for (const [index, noeud] of graphe.entries()) {
      // on vire les éléments non Array
      if (!Array.isArray(noeud)) {
        console.error(Error('noeud invalide', noeud))
        throw Error('noeud d’index ' + index + ' invalide (pas un array)')
      }
      // on ajoute le 1er élément vide s’il n’y était pas, ça devrait devenir inutile depuis qu’on a des id et index clairement distincts,
      // mais il reste du code qui utilise l’id comme index de tableau
      if (index === 0) {
        // @ts-ignore c'est pas bien d’avoir toujours un 1er nœud vide mais plein de code est basé là-dessus (id est l’index du nœud dans le graphe)
        cleanGraphe.push([])
        // si y’avait déjà le nœud vide en 0, on arrête là
        if (!noeud.length) continue
      }
      // on accepte un number pour le nodeId (nodeNuméro) => cast en string
      if (typeof noeud[0] === 'number') noeud[0] = String(noeud[0])
      // le 2e doit être le nom de la section ou fin ou Fin
      // on nettoie d’abord les nœuds fin, car on peut trouver du [null] pour les options
      const isFin = typeof noeud[1] === 'string' && noeud[1].toLowerCase() === 'fin'
      if (isFin) {
        // on fixe en minuscule si c’est pas le cas
        if (noeud[1] !== 'fin') noeud[1] = 'fin'
        // et on vire le reste qui sert à rien
        while (noeud.length > 2) noeud.pop()
      }
      if (
        noeud[0] &&
        typeof noeud[0] === 'string' &&
        noeud[1] &&
        typeof noeud[1] === 'string' &&
        // si y’a un 3e elt ça doit être un tableau d’objet
        (
          isFin ||
          (Array.isArray(noeud[2]) && noeud[2].every(elt => elt && typeof elt === 'object'))
        )
      ) {
        // le cast des nn & snn en strings se fait avec l’ajout des nœuds fin, juste après
        cleanGraphe.push(noeud as LegacyNode)
      } else {
        j3pNotify(Error('noeud d’index ' + index + ' invalide'), { graphe: grapheOriginal })
      }
    } // fin boucle sur les nœuds

    // on regarde si tous les branchements pointent qq part, et on ajoute les nœuds fin éventuels (tous les nn & snn seront bien des ids)
    const ids: string[] = []
    let nextNum = 1
    let id
    const sectionsNodes = cleanGraphe.filter(function (noeud, index) {
      if (index === 0) return false
      ids.push(noeud[0])
      if (Number(noeud[0]) >= nextNum) nextNum = Number(noeud[0]) + 1
      return noeud[1] !== 'fin'
    })
    for (const node of sectionsNodes) {
      const [numNode, , nodeOpts] = node
      // le dernier est éventuellement le paramétrage, mais pas forcément (il peut ne pas y avoir de paramétrage)
      if (nodeOpts == null) continue
      const nbParams = Array.isArray(nodeOpts) && nodeOpts.length
      if (nbParams) {
        const uselessComparators = ['>=0', '<=1']
        for (const [i, br] of nodeOpts.entries()) {
          if (uselessComparators.includes(br.pe as string) || uselessComparators.includes(br.score as string)) {
            if (br.pe) {
              console.warn('branchement avec la condition pe >= 0 qui est à éviter (si pe n’est pas numérique ça plante, et sinon ça revient au même que sans condition)', node)
              delete br.pe
            }
            br.score = 'sans+condition'
          } else if (/^[<>]=?[0-9.]$/.test(br.pe as string)) {
            console.warn(`Le branchement d’index ${i} du nœud ${numNode} porte sur une pe numérique, ça doit être remplacé par le score`)
            br.score = br.pe
            delete br.pe
          }
          if (typeof br.nn === 'number') br.nn = String(br.nn)
          if (typeof br.nn === 'string') {
            if (br.nn.toLowerCase() === 'fin') {
              // on ajoute un nœud fin
              id = String(nextNum)
              nextNum++
              br.nn = id
              cleanGraphe.push([id, 'fin'])
              ids.push(id)
            } else if (!ids.includes(br.nn)) {
              console.error('Le branchement d’index ' + i + ' du nœud n° ' + numNode + ' pointe vers ' + br.nn + ' qui n’existe pas', grapheOriginal)
            } else {
              // on vérifie aussi que la condition existe
              if (!hasProp(br, 'pe') && !hasProp(br, 'score')) console.error(Error('Le branchement d’index ' + i + ' du nœud n° ' + numNode + ' n’a pas de condition (ni pe ni score)'), grapheOriginal)
            }
          } else if (i !== nbParams - 1) { // le dernier elt peut être le paramétrage
            console.error(Error('Le branchement d’index ' + i + ' du nœud n° ' + numNode + ' est invalide'), br, grapheOriginal)
          }
          // idem pour snn
          if (typeof br.snn === 'number') br.snn = String(br.snn)
          if (typeof br.snn === 'string') {
            if (br.snn.toLowerCase() === 'fin') {
              // on ajoute un nœud fin
              id = String(nextNum)
              nextNum++
              br.snn = id
              cleanGraphe.push([id, 'fin'])
              ids.push(id)
            } else if (!ids.includes(br.snn)) {
              console.error(Error('Le branchement sinon d’index ' + i + ' du nœud n° ' + numNode + ' pointe vers ' + br.snn + ' qui n’existe pas'), grapheOriginal)
            }
          }
        }
      } else {
        // on plante pas mais on le signale
        console.error(Error('nœud n° ' + node[0] + ' sans branchement'), grapheOriginal)
      }
    }
    if (getNbSectionNodes(cleanGraphe) < 1) throw Error('graphe sans section')
    return cleanGraphe as LegacyGraph
  } catch (error) {
    console.error(error, grapheOriginal)
    return null
  }
} // getNormalizedGraphe

/**
 * Retourne le nb de nœuds de type section (non vide et non fin)
 * @private
 */
function getNbSectionNodes (graphe: LegacyGraph): number {
  return graphe.filter(function (node) {
    return node.length === 3 && node[1] !== 'fin'
  }).length
}

/**
 * Ajoute éventuellement des propriétés manquantes à contenu (sauf editgraphes, graphe bilans, donneesPersistantes)
 * @private
 */
function normalizeResultatContenu (contenu: Partial<LegacyContenu>): Partial<LegacyContenu> {
  return {
    noeuds: isArrayOfStrings(contenu.noeuds) ? contenu.noeuds : [],
    pe: isArrayOfStrings(contenu.pe) ? contenu.pe : [],
    scores: isArrayOfNumbers(contenu.scores) ? contenu.scores : [],
    ns: isArrayOfStrings(contenu.ns) ? contenu.ns : [],
    boucle: isArrayOfBooleans(contenu.boucle) ? contenu.boucle : [],
    nextId: typeof contenu.nextId === 'string' ? contenu.nextId : '',
    boucleGraphe: isPlainObject(contenu.boucleGraphe) ? contenu.boucleGraphe : {}
  }
}

type ZoneName = 'HG' | 'HD' | 'MG' | 'MD' | 'IG' | 'ID' | 'BG' | 'TBG' | 'TBD' | 'table' | 'question' | 'delai' | 'score' | 'delaiGraphique'

/**
 * La liste des zones créées par construitStructurePage (clé structure et en valeur le tableau des noms de zones)
 * On a toujours le titre dans HG et l’état dans HD (sauf
 */
const zonesByPres: Record<string, ZoneName[]> = {
  // présentations classiques avec titre en HG, état en HD et boutons en ID
  presentation1: ['HG', 'HD', 'MG', 'MD', 'IG', 'ID'], // les 6 zones classiques
  presentation1bis: ['HG', 'HD', 'MG', 'MD', 'ID'], // une seule zone en bas
  presentation1ter: ['HG', 'HD', 'MG', 'MD', 'IG', 'ID'], // variante où MD est sous MG, pour les 2 énoncés en vidéoprojection
  presentation2: ['HG', 'HD', 'MG', 'IG', 'ID'], // pas de MD
  // exception avec boutons dans MG qui prend toute la largeur, sans les zones du bas
  presentation3: ['HG', 'HD', 'MG']
}

type StructureName = keyof typeof zonesByPres

/**
 * Pour chaque structure la liste des zones à vider par videLesZones
 * @private
 */
const zonesToEmpty: Record<StructureName, ZoneName[]> = {
  presentation1: ['MG', 'MD'],
  presentation1bis: ['MG', 'MD'],
  presentation1ter: ['MG', 'MD'],
  presentation2: ['MG'],
  presentation3: ['MG']
}

export interface ParcoursOptions {
  editgraphes?: EditGrapheOptsV1
  /** Le graphe (avec 1er élément vide ou pas, on l’ajoutera si nécessaire) */
  graphe: LegacyGraph
  /**
   * Permet de commencer directement à un noeud donné (démarre à 1 !) j3p.html?graphe=[[....]]&indexInitial=n, imposé par lastResultat s’il existe.
   * Attention, c’est l’index du nœud dans le graphe (après ajout des éventuels nœuds fin), pas son n°
   */
  indexInitial?: number
  /** pour ajouter des infos en console et afficher les boutons de navigation inter-sections. */
  isDebug?: boolean
  /** Le résultat précédent, on reprendra le parcours si le parcours n’était pas terminé et si le graphe n’a pas trop changé */
  lastResultat?: LegacyResultat
  /** Une éventuelle fonction pour récupérer le résultat */
  resultatCallback?: LegacyResultatCallback
  /**
   * Passer true pour ne pas lancer init à la fin du constructeur
   * (c’est alors l’appelant qui devra le faire une fois cette instance mise en globale dans j3p)
   */
  skipAutoInit?: boolean
}
interface LastCallHistoryItem {
  etat: string
  nomSection: string
  ts: number
}
interface LastCall {
  etat: string
  isInitPending?: boolean
  isCallPending?: boolean
  history: LastCallHistoryItem[]
}
interface LegacyDonneesSection {
  /** temps limite (en s), illimité si 0 */
  limite: number
  /** nb de chances (1 par défaut), pour corriger une erreur signalée en correction */
  nbchances: number
  /** nb d’étapes pour chaque répétition (1 par défaut) */
  nbetapes: number
  /** nb de questions totales, calculé dans finEnonce (nbetapes*nbrepetitions) */
  nbitems: number
  /** Le nb de répétitions (nb de questions si nbetapes = 1) */
  nbrepetitions: number
  /** Certaines sections peuvent exporter ça pour convertir d’anciens code de pe pouvant encore être dans de vieux bilans */
  replacedPe?: Record<string, string>
  /** un theme d'affichage */
  theme?: string
  [prop: string]: unknown
}
interface LegacyTempsProp {
  /** timestamp de début du graphe (ms) */
  debutGraphe: number
  /** timestamp de début de l'item (ms) */
  debutItem: number
  /** Liste de timestamp (ms) */
  debutNoeud: number[]
  /** timestamp de fin (ms) */
  finItem?: number
}

type Zones = { [K in ZoneName]: string }

type ZonesElts = { [K in ZoneName]: HTMLElement }

type ConstruitStructurePageOptions = {
  structure?: string
  ratioGauche?: number
  theme?: string
}

class Parcours {
  private _idPrefix: string
  private _isPassive: boolean
  private _isResultatSent?: boolean
  private _isSectionInitDone?: boolean
  private _keyupListener?: (event: KeyboardEvent) => void
  private _lastSectionCallTs?: number
  /**
   * Index du prochain nœud, déterminé par _findNextAndSendResultat (appelé par finNavigation)
   * et utilisé par sectionSuivante (qui appelle _findNextAndSendResultat si ça n'avait pas
   * été fait, car il reste des sections sans appel de finNavigation)
   * puis remis à undefined dans _startSection
   */
  private _nextIndex?: number
  private _props?: string[]
  private _ratioGauche?: number
  private _timerId: TimeoutId | null
  private _tickTime?: number
  private _tickTimerId: TimeoutId | null
  bilans: LegacyBilan[]
  /**
   * La liste des boutons (créés par ajouteBoutons)
   */
  buttonsElts: Record<string, HTMLElement>
  conclusion?: string
  conteneur: HTMLElement
  debutDeLaSection?: boolean
  donneesPersistantes: Record<string, unknown>
  donneesSection: LegacyDonneesSection
  editgraphes?: EditGrapheOptsV1
  errors: Array<string | Error>
  /** initialisé à 0 dans enoncé et incrémenté juste avant l'appel de correction (revient au nb de clics sur ok) */
  essaiCourant: number
  etapeCourante: number
  etat: string
  fenetresjq: LegacyFenetresJquery
  graphe: LegacyGraph
  indexCourant?: number
  indexProgression?: number
  isDebug: boolean
  isElapsed?: boolean
  isGrapheFin?: boolean
  lastCall?: LastCall
  lastResultat?: LegacyResultat
  nbSections: number
  nomSection?: string
  mepcalculatrice: Calculatrice | null
  parcours: LegacyParcoursProp
  questionCourante: number
  repetitionCourante: number
  resultatCallback?: LegacyResultatCallback
  score?: number
  sesathequeBaseId: string
  skipDialog: boolean
  stockage!: unknown[]
  storage!: Object
  structure?: StructureName
  styles: StylesJ3p
  tableur?: Tableur
  temps: LegacyTempsProp
  typederreurs: number[]
  validOnEnter: boolean
  /** key zoneName value zoneEltId */
  zones: Zones
  /** key zoneName value zoneElt */
  zonesElts: ZonesElts

  /**
   * j3p est un objet GLOBAL, instance de ce constructeur, et utilisé un peu partout.
   * Cf j3pLoad qui fait `window.j3p = new Parcours("Mepact", "Mep", options)`
   * @param conteneur le conteneur html du parcours
   * @param [prefix='Mep'] préfixe de certains id de j3p (ne pas le changer, bcp de sections l’utilisent en dur)
   * @param options
   * @constructor
   */
  constructor (conteneur: string | HTMLElement, prefix: string = 'Mep', options: ParcoursOptions) {
    if (prefix !== 'Mep') {
      console.warn('Le préfixe des classes css est sensé être paramétrable mais de très nombreuse sections utilisent Mep en dur => on impose ce préfixe')
      prefix = 'Mep'
    }
    this._idPrefix = prefix
    const ct = typeof conteneur === 'string' ? j3pElement(conteneur) : conteneur
    if (!isHtmlElement(ct)) throw Error('conteneur invalide, impossible d’afficher l’exercice')
    /**
     * conteneur html du parcours j3p (historiquement #Mepact rien n’y oblige)
     */
    this.conteneur = ct
    this.etat = ''
    this.fenetresjq = []

    this.donneesSection = {
      limite: 0,
      nbchances: 1,
      nbitems: 0,
      nbetapes: 0,
      nbrepetitions: 0
    }
    this.temps = {
      debutGraphe: 0,
      debutItem: 0,
      debutNoeud: []
    }
    /**
     * Un objet Calculatrice mis par j3pLoad si une des sections du graphe l’utilise
     */
    this.mepcalculatrice = null
    /**
     * Un tableau de message d’erreurs (ou de Error), qui sera affiché après reconstruction des zones (dans construitStructurePage)
     */
    this.errors = []
    /**
   * Des infos sur l’état courant du parcours
   * Le nom est mal choisi (propriété parcours de l’instance d’un objet Parcours)
   * mais c’est risqué de changer maintenant
   */
    this.parcours = {
      /**
       * incrémenté en sortie de nœud quand on prend un branchement qui pointe sur le même nœud (comparaison avec la propriété max du branchement)
       */
      boucle: 1,
      noeud: -1,
      pe: ''
    }
    /**
   * À priori toujours false, mais certaines sections peuvent demander à ne pas afficher de dialogue à la fin
   * (les sections passives)
   */
    this.skipDialog = false
    this._isPassive = false
    const cleanGraph = getNormalizedGraphe(options.graphe)
    // il vaut mieux faire un throw ici, ce sera récupéré par le catch de j3pLoad et retourné à l’appelant,
    // dans labomep ça évitera de compter un affichage (si on affichait l’erreur dans j3p sans planter ça compterait pour un essai)
    if (cleanGraph == null) throw Error('Graphe invalide, impossible d’initialiser le parcours')
    this.graphe = cleanGraph
    /**
     * Nombre de sections distinctes (non vide et non fin, en cas de boucle les sections ne sont comptées qu’une seule fois)
     */
    this.nbSections = getNbSectionNodes(this.graphe)

    if (typeof options.resultatCallback === 'function') {
      const rCb = options.resultatCallback as LegacyResultatCallback
      /**
       * Fonction de callback pour envoyer le résultat
       */
      this.resultatCallback = function (resultat: LegacyResultat): void {
        if (resultat.fin) {
          if (typeof resultat.score !== 'number' || resultat.score < 0 || resultat.score > 1) {
            console.error(Error(`score invalide : ${resultat.score}`))
            // si c'est fini il faut toujours un score (en cas de condition sur le minimum de réussite),
            // on regarde si on peut le calculer
            if (resultat.contenu && resultat.contenu.scores && resultat.contenu.scores.length) {
              let nb = 0
              const reducer = (total: number, score?: number | null): number => {
                if (score == null) return total // une section sans score
                const localScore = Number(score)
                if (localScore >= 0 && localScore <= 1) {
                  nb++
                  total += localScore
                } else if (score) {
                  // y’avait un score dont le cast en number est pas entre 0 & 1
                  console.error(Error(`score ${localScore} invalide`), resultat.contenu.scores)
                } // sinon undefined ou null simplement ignoré dans le calcul global
                return total
              }
              const total = resultat.contenu.scores.reduce(reducer, 0)
              if (nb) resultat.score = total / nb
              else resultat.score = 1 // y’avait que des falsy non number => on met 1 pour pas bloquer la séquence
            } else {
              console.error('j3p renvoie un résultat avec fin mais sans score valide (ni contenu.scores)', resultat)
              resultat.score = 1
            }
          }
        }
        rCb(resultat)
      }

      // un listener pour envoyer un resultat partiel en cas d'abandon
      // Cf https://developer.mozilla.org/fr/docs/Web/API/Window/beforeunload_event
      // faut l'attacher à window pour que ça fonctionne… (sur body on est jamais appelé)
      window.addEventListener('beforeunload', (/* event: BeforeUnloadEvent */) => {
        // bizarrement, on peut être appelé deux fois…
        if (this._isResultatSent) return
        const startedAt = this.temps?.debutNoeud[this.indexCourant as number]
        const duree = typeof startedAt === 'number' ? getDelaySec(startedAt) : 0
        // on va envoyer un résultat partiel, on ne fourni donc jamais de score,
        // ça affichera parcours non terminé + le score du nœud courant dans la réponse
        const partialScore = this.score != null && Number.isInteger(this.score)
          ? this.score / this.donneesSection.nbrepetitions
          : 0
        const numQuestion = this._getNumQuestion()
        let nodeLabel = ''
        if (this.indexCourant != null) {
          const nodeId = this.graphe[this.indexCourant]?.[0]
          if (nodeId) nodeLabel = ` (du nœud ${nodeId})` // idem affichage labomep
        }
        const reponse = `Abandon à la question ${numQuestion}/${this.donneesSection.nbrepetitions}${nodeLabel} avec un score partiel de ${Math.round(100 * partialScore)}%`
        const resultat = {
          contenu: this.lastResultat?.contenu ?? {} as LegacyContenu,
          date: new Date(),
          duree,
          fin: false,
          score: this._isPassive ? 1 : 0,
          reponse
        }
        rCb(resultat)
        this._isResultatSent = true
      })
    } // fin gestion envoi des résultats

    if (options.editgraphes) {
    /**
     * Information sur les positions des nœuds dans la représentation du graphe, pour l’envoyer avec le résultat
     * (sera utilisé par showParcours pour l’affichage)
     * @type {Object}
     */
      this.editgraphes = options.editgraphes
    }

    /**
   * Si true on ajoute les boutons pour passer rapidement d’un noeud à l’autre
   * Initialisé par options.isDebug passé au constructeur
   */
    this.isDebug = Boolean(options.isDebug)

    /**
   * La bibliothèque sur laquelle on est, si on la connaît
   */
    this.sesathequeBaseId = knownSesatheques.get(window.location.hostname)

    /**
   * Objet de styles
   * par exemple this.styles.grand.enonce est un objet contenant quelques styles CSS que l’on applique aux DIV créés
   */
    this.styles = legacyStyles
    this.zones = {} as Zones
    /**
   * La liste des conteneurs de chaque zone.
   * La clé est le nom de la zone (HG, HD, …) et la valeur l’élément
   */
    this.zonesElts = {} as ZonesElts
    /**
   * La liste des boutons (créés par ajouteBoutons)
   */
    this.buttonsElts = {}

    /**
   * La liste des bilans du parcours (les étapes déjà réalisées)
   */
    this.bilans = []

    /**
   * Validation de la réponse sur touche entrée (évalué à chaque finEnonce() pour activer ou pas le _keyupListener).
   * Ce listener valide en état correction sur la touche entrée)
   */
    this.validOnEnter = true

    /**
   * Un objet destiné à l’échange d’infos entre sections du même graphe, cf {@tutorial chainageNoeuds}
   * Initialisé au démarrage du graphe (avec un objet vide ou avec lastResultat.donneesPersistantes)
   */
    this.donneesPersistantes = {}

    /**
   * timerId interne pour gérer la minuterie
   * @type {null|number}
   */
    this._timerId = null
    /**
   * timerId pour le setInterval qui réduit le rectangle rouge du temps restant
   */
    this._tickTimerId = null
    /**
     * La question courante, 1 pour la 1re question, incrémenté dans sectionCourante()
     * (juste avant l’affichage de l'énoncé de la question)
     */
    this.questionCourante = 0
    this.essaiCourant = 0
    this.repetitionCourante = 0
    this.etapeCourante = 0

    // analyse de lastResultat, la propriété lastResultat de l’objet parcours en cours d’init n’existera qu’en cas de reprise de parcours
    if (
      options.lastResultat?.contenu?.noeuds?.length &&
      (options.lastResultat.contenu.nextId || Array.isArray(options.lastResultat.contenu.ns))
    ) {
      const lastResultat = options.lastResultat
      // si y’a un pb dans ce lastResultat, on le veut dans la notification
      if (typeof window.bugsnagClient?.addMetadata === 'function') {
        window.bugsnagClient.addMetadata('j3p', 'lastResultat', stringify(lastResultat))
      }
      try {
        const contenu = lastResultat.contenu
        // console.log('lastResultat', stringify(contenu))
        delete options.indexInitial // ne fait rien s’il n’était pas là
        const lastGraphe = getNormalizedGraphe(contenu.graphe)
        if (lastResultat.fin) {
        // le graphe était terminé
          if (lastGraphe && lastGraphe.length > 2) {
          // on met un message à l’élève, sauf si graphe à un noeud (le nœud fin a déjà été ajouté par getNormalizedGraphe)
            options.skipAutoInit = true // pour lancer init seulement à la fermeture de la modale
            j3pModale({
              divparent: this.conteneur,
              titre: 'Information',
              contenu: parcoursTermine,
              onClose: this.init.bind(this)
            })
          }
        // sinon rien, on laisse la suite se dérouler et ça démarrera au début
        } else if (!lastGraphe) {
          j3pNotify(Error('Le graphe de lastResultat est invalide => ignoré'), { lastResultat, graphe: this.graphe })
        } else {
        // on compare que les nœud faits (avec leur paramétrage et leurs branchements) avec les même dans le graphe actuel,
        // pour vérifier que ça n’a pas changé et que l’on peut faire une reprise de parcours
          const checkLast: LegacyNode[] = []
          const checkNew: LegacyNode[] = []
          // pour chaque id contenu dans lastResultat.contenu.noeuds on regarde s’il a changé
          for (const id of contenu.noeuds) {
            const lastNoeud = lastGraphe.find(n => n[0] === id)
            if (isLegacyNode(lastNoeud)) {
              checkLast.push(lastNoeud)
            }
            const noeud = this.graphe.find(n => n[0] === id)
            if (noeud != null) checkNew.push(noeud)
          }
          if (stringify(checkLast) !== stringify(checkNew)) {
          // console.log('le graphe était', lastGraphe, 'et il est devenu', this.graphe, 'avec les nœuds déjà traités', lastResultat.noeuds)
          // on ne peut pas encore utiliser j3pShowError car les zones ne sont pas construites (ça resterait très peu de temps)
            this.errors.push('Il y avait un parcours entamé mais sa construction a changé depuis, impossible de le reprendre, il va falloir recommencer au début')
          } else {
          // On peut étudier la reprise de graphe (lastGraphe et this.graphe sont identiques), init options.indexInitial
          // Attention, si le graphe est [[], ['2',...], ['5', ...]] et qu’après le noeud numéroté '2' on oriente vers le '5', alors ns vaut 5, ce n’est pas un index du tableau...
          // depuis le 11/05/2020, y’a une propriété nextId pour ça, plus besoin de ns (qu’on pourrait déduire des bilans)
            let nextId = contenu.nextId || contenu.ns[contenu.ns.length - 1]
            if (typeof nextId === 'number') nextId = String(nextId) // dans ns c'était des number
            if (typeof nextId === 'string') {
              if (nextId === 'fin' || this.isNoeudFin(nextId)) {
                console.error(Error(`On avait un lastResultat non fini avec nœud suivant ${nextId} pointant sur fin !`), stringify(lastResultat))
              } else if (nextId) {
                const nextIndex = this.getIndexFromId(nextId)
                if (nextIndex >= 0 && Array.isArray(lastGraphe[nextIndex]) && (lastGraphe[nextIndex] as LegacyNode).length) {
                  options.indexInitial = nextIndex
                  if (options.skipAutoInit) {
                    console.error(Error('l’option skipAutoInit est incompatible avec la reprise de parcours, elle sera ignorée'))
                    options.skipAutoInit = false
                  }
                  // Cas particulier de la récupération de la boucle :  this.parcours.boucle sera initialisé plus tard à 1, sauf si on est en train de reprendre au sein d’une boucle...
                  // ie si nextIndex est le noeud courant
                  console.info('reprise de parcours, nextId', nextId, 'pointe vers le nœud', lastGraphe[nextIndex])
                  const lastIndexEtape = contenu.noeuds.length - 1 // le dernier index des tableaux noeuds/scores/boucle
                  const lastId = contenu.noeuds[lastIndexEtape] as string
                  if (lastId === nextId) {
                    this.parcours.boucle = contenu.boucle[lastIndexEtape] as number
                  }
                  if (contenu.boucleGraphe && typeof contenu.boucleGraphe === 'object' && hasProp(contenu.boucleGraphe, lastId)) {
                    // @ts-ignore
                    this.parcours.boucleGraphe = contenu.boucleGraphe[lastId] as number[]
                  }
                  /**
                   * Le (section x) affiché à l’élève pour montrer la progression dans le graphe, il est incrémenté à chaque fois qu’on change de nœud
                   * même si on boucle sur le même.
                   */
                  this.indexProgression = contenu.noeuds.length // il sera incrémenté à l’init de la première section à faire

                  if (contenu.donneesPersistantes && Object.keys(contenu.donneesPersistantes).length) this.donneesPersistantes = contenu.donneesPersistantes

                  // et les bilans à récupérer
                  if (Array.isArray(contenu.bilans) && contenu.bilans.length) this.bilans = [...contenu.bilans]

                  /**
                   * Le résultat précédent cette reprise de parcours (qui reste undefined si on ne reprend pas le graphe en cours de route)
                   */
                  this.lastResultat = lastResultat
                  // console.log('fin init lastResultat reçu', lastResultat)
                } else {
                  // y’avait un lastResultat qu’on a pas su interpréter, on reprendra au début
                  j3pNotify(Error('Impossible de trouver où reprendre le parcours'), { lastResultat })
                  this.errors.push('Il y avait un parcours entamé mais impossible de retrouver où le reprendre')
                }
              }
            } else {
              console.error(Error(`On avait un lastResultat non fini avec nœud suivant ${nextId} invalide (${typeof nextId} ${nextId})`), stringify(lastResultat))
            }
          }
        } // else pas de lastResultat
      } catch (error) {
        console.error(error)
        j3pNotify(Error('Plantage lors de l’analyse de lastResultat'), { parcours: this, lastResultat })
        this.errors.push('Il y avait un parcours entamé mais la reprise a échouée, retour au début')
      }
    } // fin analyse lastResultat

    /**
     * Un tableau contenant des infos sur les résultats, lorsque la section le renseigne (pas utilisé par le modèle)
     * Les index autres que 0|1|2|10 peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     * @property 0 Nb de bonnes réponses
     * @property 1 Nb de fois où l’élève a utilisé une chance supplémentaire
     * @property 2 Nb de mauvaises réponses
     * @property 10 Nb de fois où l’élève a eu faux à cause d’un temps limite
     */
    this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    if (!options.skipAutoInit) {
      this.init(options.indexInitial)
    }

    // on charge jQuery en tache de fond
    loadJqueryDialog().catch(j3pShowError)
  } // Parcours

  /**
 * Méthode d’initialisation de l’objet j3p :
 * - création des propriétés de l’objet
 * - initialisation de nombreuses variables
 * - enfin lancement du graphe via l’appel de la méthode navigation
 * Attention, on est appelé par le constructeur, donc le parcours en cours d’instanciation
 * n’est pas encore mis en global dans la variable j3p (qui contient toujours la
 * config exportée par la section)
 * Méthode à priori privée, mais on peut être appelé par un lanceur qui voudrait séparer la création de l’instance et son init
 * @param indexInitial Le numéro du nœud dans le graphe sur lequel initialiser le Parcours
 */
  init (indexInitial?: number): void {
    const me = this

    // indexInitial n’est défini que si on a : j3p.html?graphe=....]];&noeud=k ou dans le cas de l’appel par la bibli pour reprise de parcours
    // Attention (suite aux modifs pour editgraphes permettant un graphe avec trous) indexInitial n’est pas le numéro du nœud mais son index dans le graphe
    if (indexInitial) console.info('Dans init, indexInitial demandé :', indexInitial)
    if (typeof indexInitial === 'string') {
      console.warn('indexInitial doit être un number, c’est un index dans le graphe')
      indexInitial = Number(indexInitial)
    } else if (!indexInitial || typeof indexInitial !== 'number') {
      indexInitial = 1
    }
    if (indexInitial < 1 || indexInitial >= this.graphe.length) {
      console.warn('indexInitial ' + indexInitial + ' invalide (hors graphe)')
      indexInitial = 1
    }
    // on vérifie qu’on tombe bien sur un nœud de section
    const nodeInitial = this.graphe[indexInitial]
    if (!isLegacyNode(nodeInitial) || nodeInitial[1] === 'fin') {
      // on tombe sur un nœud fin mis en premier, c’est vicieux mais syntaxiquement correct, on prend le premier non fin
      // cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/some
      console.warn('indexInitial ' + indexInitial + ' invalide (pas un nœud de section)')
      if (!this.graphe.some(function (noeud, index) {
        if (Array.isArray(noeud) && noeud.length > 2) {
          indexInitial = index
          return true // on a trouvé, on arrête là
        }
        return false
      })) {
        indexInitial = 1 // ce sera un nœud fin mais tant pis y’a que ça…
      }
    }

    // lui a pu être init par l’analyse de lastResultat, avant on l’initialisait toujours, à partir du 3/06/2021 seulement s’il n’a jamais été affecté
    if (!this.parcours.boucleGraphe) this.parcours.boucleGraphe = []
    // On récupère alors potentiellement deux infos de la bibli, le dernier résultat enregistré au format
    // https://bibliotheque.sesamath.dev/doc/Resultat.html et la callback de rappel pour la fin
    // et normalement si lastResultat il y aura resultatCallback

    /**
   * les différentes zones : ce sont des strings
   */
    this.zones = {
      table: this._idPrefix + 'table',
      HG: this._idPrefix + 'HG',
      HD: this._idPrefix + 'HD',
      MG: this._idPrefix + 'MG',
      MD: this._idPrefix + 'MD',
      IG: this._idPrefix + 'IG',
      ID: this._idPrefix + 'ID',
      BG: this._idPrefix + 'BG',
      TBG: this._idPrefix + 'TBG',
      TBD: this._idPrefix + 'TBD'
    } as Zones

    this.parcours.boucle = 1
    this.etat = 'enonce'

    $(window).resize(function () {
      if (!me._ratioGauche) return // un resize avant que la section n’ait construit la page
      try {
        me._initDom(me._ratioGauche)
      } catch (error) {
        if (error instanceof Error) {
          error.message += ' (plantage au resize)'
        }
        me.notif(error as Error | string)
        me._initDom()
      }
    })

    try {
      const noeud = this.graphe[indexInitial]
      if (noeud == null) throw Error(`Aucun nœud ${indexInitial}`)
      const nomSection = noeud[1]
      const nomFn = 'Section' + nomSection
      // @ts-ignore faudrait trouver le moyen d’ajouter dynamiquement des propriétés à Parcours
      if (typeof this[nomFn] === 'function') {
        this.nomSection = nomSection
        // on ajoute cette info pour l’éventuel rapport bugsnag qui serait envoyé si ça plantait plus loin
        // Ça n’arrive bien sûr jamais, mais au cas où ça arriverait quand même ;-)
        if (window.bugsnagClient && typeof window.bugsnagClient.addMetadata === 'function') {
          try {
            window.bugsnagClient.addMetadata('j3p', { section: nomSection, parcours: this })
          } catch (error) {
            console.error(error)
            // pour le retrouver dans les breadcrumbs bugsnag
            console.warn('pb de parsing json du parcours, impossible de l’ajouter dans les métadata bugsnag', this)
          }
        }
        /**
       * Un numéro qu’on affiche à coté du n° de question, pour informer de l’avancement dans le graphe
       * (sinon un graphe à N nœud d’une question donne l’impression se repartir à 0 à chaque fois)
       * @type {number}
       */
        if (!this.indexProgression) this.indexProgression = 0 // sinon il a déjà été initialisé à partir de lastResultat
        // et on la lance, mais il faut un setTimeout pour que le constructeur rende la main et que l’appelant ait pu mettre j3p en global
        // (et ajouter éventuellement ensuite j3p.mepcalculatrice, avant que le reste du code ne veuille s’en servir)
        setTimeout(this._startSection.bind(this, indexInitial), 0)
      } else {
        throw new Error(nomSection + ' n’a pas été chargé')
      }
    } catch (error) {
      if (error instanceof Error) {
        error.message = 'Impossible de lancer la section : ' + error.message
      }
      this.notif(error as Error | string)
      erreurInterne()
    }
  } // init

  /**
 * Dimensionne les parties gauche et droite de la scène
 * @private
 */
  _initDom (ratioGauche?: number) {
    interface SetZoneOptions { className?: string, width?: string, height?: string, overflow?: string }
    // affecte les props à la zone (fct fléchée pour que le this reste celui de la méthode)
    const setZone = (zone: ZoneName, { className, width, height, overflow }: SetZoneOptions, stylesSup?: CSSStyleDeclaration) => {
      const elt = this.zonesElts[zone]
      if (!elt) {
        erreurInterne()
        return this.notif(Error('La zone ' + zone + ' n’existe pas'))
      }
      if (!height) {
      // par défaut 100px pour le haut et 80 pour le bas
        if (zone.startsWith('H')) height = '100px'
        else if (zone.startsWith('I')) height = '80px'
      }
      // largeur en % d’après le ratioGauche par défaut
      if (!width && zone.endsWith('G')) width = widthG
      // les props (ce qui est undefined sera ignoré par j3pSetProps)
      const props: { className?: string, style: PlainObject } = {
        style: { width, height, overflow, position: 'relative' }
      }
      // on ajoute className que si c'était vide (sinon on laisse ce qu’on avait déjà), pour qu’en cas de resize (qui appelle _initDom) on ne modifie pas l’existant
      if (!elt.className) {
        if (className != null) props.className = className
        // idem pour ça, on ne le fait que la première fois
        if (stylesSup) {
          for (const [p, v] of Object.entries(stylesSup)) {
            props.style[p] = v
          }
        }
      }
      // on peut nous demander explicitement de ne rien préciser (cas particulier 1ter) en passant null
      if (width === null) delete props.style.width
      if (height === null) delete props.style.height
      j3pSetProps(elt, props)
    }

    // init this._ratioGauche
    if (!ratioGauche) ratioGauche = 0.75
    if (!Number.isFinite(ratioGauche)) {
      console.error(TypeError('ratio invalide, 0.75 imposé'))
      ratioGauche = 0.75
    } else if (ratioGauche > 0.9) {
      console.error(RangeError('ratio trop élevé => 0.9 max'))
      ratioGauche = 0.9
    } else if (ratioGauche < 0.1) {
      console.error(RangeError('ratio trop faible => 0.1 min'))
      ratioGauche = 0.1
    }
    this._ratioGauche = ratioGauche
    const widthG = (ratioGauche * 100) + '%'

    // init structure
    if (!this.structure) this.structure = 'presentation1'
    if (!(this.structure in zonesByPres)) {
      console.error(Error('structure de page inconnue : ' + this.structure))
      this.structure = 'presentation1'
    }

    // on passe à la (re)création du dom
    const [fWidth, fHeight] = j3pDimfenetre()

    // identique pour toutes les structures
    this.conteneur.style.width = (<number>fWidth - 10) + 'px'
    this.conteneur.style.marginLeft = '-' + (<number>fWidth / 2 - 5) + 'px'
    this.conteneur.style.marginTop = '5px'

    switch (this.structure) {
      case 'presentation1':
        setZone('HG', { className: 'meftdHG w60 left' })
        setZone('HD', { className: 'meftdHD mod' })
        setZone('MG', { height: `${<number>fHeight - 225}px`, overflow: 'auto', className: 'divZone w60 left' })
        setZone('MD', { height: `${<number>fHeight - 225}px`, overflow: 'auto', className: 'divZone mod' })
        setZone('IG', { overflow: 'auto', className: 'divZone w60 left' })
        setZone('ID', { className: 'divZone mod' })
        break

      case 'presentation1bis':
        setZone('HG', { className: 'meftdHG w60 left' })
        setZone('HD', { className: 'meftdHD mod' })
        setZone('MG', { height: `${<number>fHeight - 125}px`, overflow: 'auto', className: 'divZone w60 left' })
        setZone('MD', { height: `${<number>fHeight - 205}px`, overflow: 'auto', className: 'divZone mod' })
        setZone('ID', { className: 'divZone mod' })
        break

        // utilisé seulement par vidéoprojection
      case 'presentation1ter':
      // MG au dessus de MD
        setZone('HG', { className: 'meftdHG w60 left' })
        setZone('HD', { className: 'meftdHD mod' })
        // un coté gauche qui doit pas avoir le ratio par défaut (on précise pas et ça prendra la place dispo)
        setZone('MG', { height: `${(<number>fHeight - 240) / 2}px`, width: '100%', overflow: 'auto', className: 'divZone mod' })
        setZone('MD', { height: `${(<number>fHeight - 240) / 2}px`, overflow: 'auto', className: 'divZone mod' })
        setZone('IG', { className: 'divZone w60 left' })
        setZone('ID', { className: 'divZone mod' })
        break

      case 'presentation2':
        setZone('HG', { className: 'meftdHG w60 left' })
        setZone('HD', { className: 'meftdHD mod' })
        setZone('MG', { height: `${<number>fHeight - 240}px`, overflow: 'auto', className: 'divZone mod', width: '100%' })
        setZone('IG', { overflow: 'auto', className: 'divZone w60 left' })
        setZone('ID', { className: 'divZone mod' })
        break

      case 'presentation3':
        setZone('HG', { className: 'meftdHG w60 left' })
        setZone('HD', { className: 'meftdHD mod' })
        setZone('MG', { overflow: 'auto', className: 'divZone mod', width: '100%' })
        // encore un cas particulier, pourquoi seulement pour cette structure ?
        try {
          if (this.donneesSection.typesection === 'tablette') {
            this.zonesElts.HG.style.height = '70px'
            this.zonesElts.HD.style.height = '70px'
            this.zonesElts.MG.style.height = `${<number>fHeight - 105}px`
          } else {
            this.zonesElts.MG.style.height = `${<number>fHeight - 135}px`
          }
        } catch (error) {
          console.error('Pb d’initialisation du dom', error)
        }
        break

      default:
        throw Error(`structure ${this.structure} inconnue`)
    }
  } // _initDom

  /**
 * Avance à la question suivante
 */
  avance ():void {
    if (this.sectionTerminee()) {
    // dernière question
      this.etat = 'navigation'
      this.sectionCourante()
    } else {
      this.etat = 'enonce'
      this.sectionCourante()
    }
  }

  // structure = "presentation1" || "presentation2" || "presentation3"  || "presentation1bis"  bool=false si l’on ne veut pas nettoyer le contenu de mepact (utile par exemple dans section exo_mep)
  /**
   * Applique la présentation demandée (appellera ajouteBoutons). L’ancienne syntaxe construitStructurePage(structure) reste acceptée.
   * @param [options] Si c’est une string ce sera pris comme structure
   * @param [options.structure=presentation1] Le nom de la structure de page
   * @param [options.ratioGauche=0.75] Le ratio de la colonne de gauche (entre 0.1 et 0.9)
   * @param [options.theme=] Passer le nom d’un dossier de legacy/themes pour appliquer ce theme
   */
  construitStructurePage (options: string | ConstruitStructurePageOptions = {}):void {
    function ajouteBoutons () {
      const parent = me.ajouteBoutons()
      // si debug on ajoute les shortcuts
      if (me.isDebug) {
        j3pAjouteBouton(parent, me.avance.bind(me), { value: 'Question suivante (mode test)' })
        // pas de bind ici car on veut que _init soit appelée avec les valeurs que me aura au moment de l’appel (et pas les actuelles)
        j3pAjouteBouton(parent, () => me._startSection(Number(me.indexCourant) - 1), {
          value: 'Noeud précédent'
        }, { minDelay: 500, debounceMessage: 'Doucement…' })
        j3pAjouteBouton(parent, () => me._startSection(Number(me.indexCourant) + 1), {
          value: 'Noeud suivant'
        }, { minDelay: 500, debounceMessage: 'Doucement…' })
        j3pAddElt(me.conteneur, 'span', '  Noeud courant : ' + me.getIdFromIndex(me.indexCourant as number))
      }
    }

    function resetCss () {
      let className = 'j3pContainer'
      if (me.donneesSection.typesection === 'grand') {
      // au cas ou changement de css
        className += ' w1200 line gut'
      } else if (me.donneesSection.typesection === 'petit') {
      // au cas ou changement de css
        className += ' w720 line gut'
      }
      me.conteneur.setAttribute('class', className)

      let theme = (opts.theme ?? defaultTheme) as string
      if (me.donneesSection.theme) {
        if (themes.includes(me.donneesSection.theme)) {
          theme = me.donneesSection.theme
        } else {
          console.error(Error(`Thème ${me.donneesSection.theme} inconnu => ignoré`))
        }
      }
      if (!me.conteneur.classList.contains(theme)) {
      // faut changer, on vire les themes éventuels
        me.conteneur.classList.remove(...themes)
        // et on met le bon
        me.conteneur.classList.add(theme)
        // et on charge sa css (ça ne fait rien si elle était déjà chargée)
        loadThemeCss(theme)
      }
    } // resetCss

    function addZone (nom: ZoneName): void {
      const props = {
        id: 'Mep' + nom,
        style: {} as PlainObject
      }
      props.style = { position: 'absolute' }
      me.zonesElts[nom] = j3pAddElt(me.conteneur, 'div', '', props)
    }

    // affiche les éventuelles erreurs accumulées avant l’affichage de la page
    function printErrors () {
      if (Array.isArray(me.errors) && me.errors.length) {
      // si on passait directement j3pShowError à forEach il récupérerait l’index en 2e argument
        me.errors.forEach(message => j3pShowError(message))
      }
      me.errors = []
    }

    // compatibilité ascendante, le 1er argument peut être la structure
    let opts: ConstruitStructurePageOptions
    if (options && typeof options === 'string') {
      opts = { structure: options }
    } else if (!options || typeof options !== 'object') {
      console.error(Error('options invalides'))
      opts = {}
    } else {
      opts = options
    }
    let structure = opts.structure || 'presentation1'
    if (!(structure in zonesByPres)) {
      if (structure) console.error(Error('La structure de page ' + structure + ' est inconnue => presentation1 imposé'))
      structure = 'presentation1'
    }

    this.structure = structure as StructureName
    const me = this
    // on vide le conteneur
    this.zonesElts = {} as ZonesElts
    j3pEmpty(this.conteneur)
    // reset des éléments

    switch (structure) {
      case 'presentation1':
      case 'presentation1bis':
      case 'presentation1ter':
      case 'presentation2':
      case 'presentation3': {
        this.conteneur.style.position = 'absolute'
        const zones = zonesByPres[structure as StructureName] as ZoneName[]
        for (const zone of zones) addZone(zone)
        resetCss()
        ajouteBoutons()
        printErrors()
        this._initDom(opts.ratioGauche)
        break
      }
      default:
        throw Error(`Erreur interne, structure de présentation connue mais non gérée (${structure})`)
    }
  } // construitStructurePage

  /**
 * Retourne l’id du nœud en connaissant son index dans le graphe (avec elt 0 vide)
 // un graphe peut s'écrire [2,sectiontruc,[]];[5;sectiontruc2,[]]; et donc EquivalentNoeud(1)=2,  EquivalentNoeud(2)=5
 */
  getIdFromIndex (index: number): string {
    if (typeof index !== 'number') {
      erreurInterne()
      this.notif(Error('Index non numérique (' + index + ':' + typeof index + '), impossible de récupérer un id'))
      return ''
    }
    if (index >= this.graphe.length) {
      erreurInterne()
      this.notif(Error('L’index ' + index + ' n’existe pas dans ce graphe de longueur ' + this.graphe.length))
      return ''
    }
    const node = this.graphe[index]
    if (node == null) {
      console.error(Error('Pas de nœud d’index ' + index))
      return ''
    }
    let id = node[0]
    if (typeof id !== 'string') {
      console.error(Error('L’id du nœud d’index ' + index + ' n’est pas une string'), this.graphe)
      id = String(id)
    }
    return id
  }

  /**
   * Retourne l’index du nœud dans le graphe d’après son id (ou -1 si on l'a pas trouvé)
   */
  getIndexFromId (id: string): number {
    if (typeof id !== 'string') {
      erreurInterne()
      this.notif(Error('Cet identifiant n’est pas une string, impossible de récupérer l’index ' + id))
      return -1
    }
    const i = this.graphe.findIndex(node => node[0] === id)
    if (i === -1) {
      console.error(Error('Pas de nœud d’id ' + id + ' dans le graphe'))
    }
    return i
  }

  /**
   * Appelle resultatCallback avec un resultat vide pour initier le démarrage de l'exo
   * (ça incrémente nbEssai dans Labomep)
   */
  _sendInitResult (): void {
    if (typeof this.resultatCallback !== 'function') return
    if (this.lastResultat) {
      // reprise de parcours, on renvoie le même résultat (pour le récupérer au prochain démarrage si l'élève abandonne avant de passer au nœud suivant)
      this.resultatCallback(this.lastResultat)
    } else {
      const contenu = normalizeResultatContenu({}) as LegacyContenu
      contenu.graphe = this.graphe
      contenu.editgraphes = this.editgraphes
      this.resultatCallback({
        score: 0,
        reponse: '',
        date: new Date(),
        fin: false,
        duree: 0,
        contenu
      })
    }
  }

  /**
   * Envoi le résultat (à priori à la bibli, mais en fait à n’importe quel demandeur,
   * ou en console si personne ne le demande, pour contrôler qu’il aurait bien été envoyé)
   */
  _sendResult ():void {
    if (this._isResultatSent) return console.error(Error('Les résultats ont déjà été envoyés pour ce nœud du graphe'))
    // le 21/04/17 on a dégagé du graphe le 1er nœud vide, puis remis le 18/08/17 car produisait des effets de bords
    // (décalage de l’indexCourant), c’est relégué en correctif à l’afficheur de parcours
    const resultat: Partial<LegacyResultat> = {}
    // le stringify permet de figer la valeur en console (sinon quand on consulte
    // on a la valeur au moment de la consultation, pas au moment du console.log)
    if (this.lastResultat) {
    // c’est une reprise de parcours, on récupère oid (ça va écraser l’ancien résultat) et contenu
      if (this.lastResultat.oid) resultat.oid = this.lastResultat.oid
      resultat.contenu = this.lastResultat.contenu
    // console.log('dans _sendResult on a récupéré le contenu de lastResultat', stringify(resultat.contenu))
    } else {
    // ni reprise ni poursuite, on est au nœud 1
      resultat.contenu = {} as LegacyContenu
    }

    // on complète la propriété contenu :
    const contenu = normalizeResultatContenu(resultat.contenu) as LegacyContenu
    resultat.contenu = contenu
    // on prend les infos editgraphes si y’en a (positions et titres) pour showParcours qui lira ce résultat
    contenu.editgraphes = this.editgraphes
    // Ajout des infos du noeud
    const lastBilan = this.bilans[this.bilans.length - 1] as LegacyBilan
    if (lastBilan == null) throw Error('Impossible d’envoyer un résultat vide')
    // avec ces 3 là on devrait pouvoir déterminer tout le reste
    contenu.noeuds.push(lastBilan.id)
    contenu.pe.push(lastBilan.pe)
    contenu.scores.push(lastBilan.score)
    // mais ajouter la suite évitera du boulot à la récupération
    contenu.ns.push(lastBilan.ns ?? '')
    contenu.nextId = lastBilan.nextId ?? ''
    contenu.boucle.push(lastBilan.boucle ?? 0)
    contenu.boucleGraphe[lastBilan.id] = this.parcours.boucleGraphe as number[]
    // on sauvegarde aussi le graphe, pour réutilisation dans l’affichage du bilan via showParcours
    // (et vérification qu’il n’a pas changé avant la reprise de graphe)
    contenu.graphe = this.graphe
    // et les bilans (y’a du code qui peut piocher dedans, faudra les remettre à la reprise),
    // ça devrait remplacer tout ce qui précède mais on verra pour nettoyer tout ça dans sesaparcours
    contenu.bilans = this.bilans
    // l’objet donneesPersistantes (pour la transmission de données d’une section à l’autre au sein du graphe), éventuellement enrichi par la section
    contenu.donneesPersistantes = this.donneesPersistantes

    // fin ou pas ?
    resultat.fin = lastBilan.fin

    // calcul du score global
    let scoreTotal = 0
    const nbBilansWithScore = this.bilans.reduce((acc, bilan) => {
      if (typeof bilan.score === 'number') {
      // normalement _addBilan a déjà contrôlé ça, au cas où des petits malins trafiqueraient ça en console
        if (!Number.isFinite(bilan.score) || bilan.score > 1 || bilan.score < 0) throw Error('Score invalide')
        scoreTotal += bilan.score
        acc++
      }
      return acc
    }, 0)
    if (nbBilansWithScore) {
      resultat.score = scoreTotal / nbBilansWithScore
    } else {
      resultat.score = 1 // que des nodes sans score
    }

    // il reste à répercuter resultat sur this.lastResultat pour qu’au prochain passage ici on ajoute les résultats (on sera plus en nœud 1)
    this.lastResultat = resultat as LegacyResultat

    // si on passe un resultat.score il serait pris en compte,
    // mais sinon le plugin j3p le calcule d’après resultat.contenu.scores,
    // il vire les undefined et fait la moyenne des autres (s’ils sont bien
    // entre 0 et 1, sinon c’est ignoré avec un feedback à l’élève)
    if (typeof this.resultatCallback === 'function') {
    // console.log('noeuds et scores du résultat', resultat.contenu.noeuds, resultat.contenu.scores)
      this.resultatCallback(resultat as LegacyResultat)
    } else {
      console.info('Résultat qui aurait été envoyé s’il avait été demandé', stringify(resultat))
    }
    this._isResultatSent = true
  } // _sendResult

  /**
   * Ajoute un bilan à this.bilans et le retourne (pour le compléter plus loin dans le code)
   * @todo centraliser tout ça au même endroit
   * @param duree en s
   */
  _addBilan (duree: number): LegacyBilan {
    if (Number.isNaN(duree)) {
      this.notif(Error('durée invalide (NaN)'))
      duree = 0
    }
    const nextIndex = this.bilans.length
    const score = Number.isFinite(this.score)
      ? j3pArrondi(<number> this.score / this.donneesSection.nbitems, 3)
      : 0
    const bilan: LegacyBilan = {
      index: nextIndex,
      id: this.getIdFromIndex(this.indexCourant as number),
      pe: this.parcours.pe,
      duree,
      score,
      fin: false
    }
    if (score < 0) {
      this.notif(Error(`Score négatif : ${bilan.score}`), { bilan, bilans: this.bilans })
      bilan.score = 0
    } else if (score > 1) {
      this.notif(Error(`Score supérieur à 1 : ${bilan.score}`), { bilan, bilans: this.bilans })
      bilan.score = 1
    }
    // un petit check quand même
    if (this._isPassive === Number.isFinite(this.score)) {
      if (this._isPassive) {
        console.error(Error('La section appelle setPassive() et retourne quand même un score'))
      } else {
        console.error(Error('La section ne renvoie pas de score sans avoir appelé setPassive(), elle devrait le faire (pour bien gérer l’affichage du score)'))
      }
    }
    if (this._isPassive) bilan.score = null
    this.bilans.push(bilan)
    return bilan
  } // _addBilan

  /**
 * Affiche la boite de dialogue de fin de section
 */
  _displayEndDialog ():void {
    function getLabelMinutesSecondes (secondes: number) {
      let label = ''
      if (secondes > 60) {
        const minutes = Math.floor(secondes / 60)
        label += `${minutes} minute${minutes > 1 ? 's' : ''} `
        secondes = secondes % 60
      }
      label += `${secondes} seconde${secondes > 1 ? 's' : ''}`
      return label
    }

    const conclusion = (this.conclusion || 'Fin')
    // on garde d’éventuels <br>
      .replace(/<br *\/?>/g, '\n')
    // mais on vire d’éventuels autres tags html mis dans la conclusion
    // (sur le branchement, dans l’édition du graphe)
      .replace(/<[^ ][^>]+>/g, '') // le [^ ] en premier est là pour autoriser un < suivi d’une espace dans une phrase de conclusion (qui aurait ensuite un >)
    const dialog = j3pAddElt(this.conteneur, 'div', conclusion + '\n\n')

    const me = this
    const titre = this.isGrapheFin ? 'Activité terminée' : 'Info' // si c’est pas fini on affiche seulement la phrase de conclusion
    const taille = this.isGrapheFin ? 600 : 450

    if (this.isGrapheFin) {
      let total = 0
      let nbScores = 0
      for (const [i, bilan] of this.bilans.entries()) {
        let scoreFeedback: string
        if (bilan.score != null && Number.isFinite(bilan.score)) {
          total += bilan.score
          nbScores++
          scoreFeedback = ` -- Score : ${Math.round(bilan.score * 100)}%`
        } else {
          scoreFeedback = ' -- Sans score'
        }
        if (i) j3pAddElt(dialog, 'br')
        j3pAddTxt(dialog, 'Nœud : ')
        j3pAddElt(dialog, 'span', bilan.id, { className: 'grasrouge' })
        j3pAddTxt(dialog, '-- Temps passé : ' + getLabelMinutesSecondes(bilan.duree))
        j3pAddTxt(dialog, scoreFeedback)
      }
      if (nbScores) {
        j3pAddElt(dialog, 'br')
        j3pAddTxt(dialog, `Score global : ${Math.round(total / nbScores * 100)}%`)
      }
    }

    const $dialog = $(dialog)
    const [width] = j3pDimfenetre()

    loadJqueryDialog().then(() => {
      $dialog.dialog({
      // pour passer à la section suivante à la fermeture de la boîte de dialogue
        beforeClose: function () {
        // getElementsByTagName retourne une https://developer.mozilla.org/en-US/docs/Web/API/HTMLCollection qui n’a pas de forEach
          Array.prototype.slice.call(document.getElementsByTagName('div')).forEach(function (div) {
            if (div.classList.contains('ui-dialog')) div.parentNode.removeChild(div)
          })
          if (me.isGrapheFin) {
          // il y a des cas où on est appelé alors que me.structure est déjà undefined, arrive peut-être au changement de section,
          // en tout cas si me est déjà vide y’a plus grand chose à vider à l’écran
            if (!me.structure) return console.warn('le parcours courant a perdu sa structure, rien à vider', me)
            // on ne passe pas par videLesZones car on veut vraiment tout effacer (sauf le titre), sans recréer les boutons
            const zones = zonesByPres[me.structure]
            if (!zones) return me.notif(Error('structure ' + me.structure + ' sans zones'))
            zones.forEach(function (zone) {
              if (zone !== 'HG') j3pEmpty(me.zonesElts[zone])
              if (zone === 'MG') j3pAddElt(me.zonesElts.MG, 'p', 'Parcours terminé', { style: { padding: '2em' } })
            })
          } else if (typeof me._nextIndex === 'number') {
            me._startSection(me._nextIndex)
          } // else rien, y’a pas de nœud suivant et le pb a déjà été signalé, on reste donc planté là
        },
        // cache le bouton de fermeture, cf css & https://api.jqueryui.com/dialog/
        dialogClass: 'no-close',
        buttons: {
          OK: function () {
            $dialog.dialog('close')
          }
        },
        title: titre,
        position: [(<number>width - taille) / 2, 140], // "center",
        width: taille,
        height: 'auto',
        resizable: false
      })
    }).catch(j3pShowError)
  } // _displayEndDialog

  /**
   * Retourne la pe renvoyée lors du dernier passage dans idNoeud
   * @return la pe (undefined si on l’a pas trouvé ou si y’en avait pas)
   */
  getLastPe (idNoeud: string | undefined): string {
    if (idNoeud == null) return ''
    // il faut partir du dernier
    for (let k = this.bilans.length - 1; k >= 0; k--) {
      const bilan = this.bilans[k]
      if (bilan == null) continue
      if (bilan.id === idNoeud) return bilan.pe
    }
    return ''
  }

  /**
   * Retourne true si idNoeud correspond à un nœud fin
   */
  isNoeudFin (idNoeud: string): boolean {
    const index = this.getIndexFromId(idNoeud)
    if (index === -1) {
      console.error(Error('Il n’y a pas de nœud ' + idNoeud + ' dans ce graphe'))
      return false
    }
    return this.graphe?.[index]?.[1] === 'fin'
  }

  /**
   * Fonction appelée à chaque fin de section (à priori par le bouton sectionContinuer)
   * Compile le résultat et finalise les bilans, chercher le nœud suivant et appelle _displayEndDialog
   * qui appellera ensuite le nœud suivant.
   */
  sectionSuivante ():void {
    // dans tous les cas on vire les timers si y’en avait
    this._stopTimer()

    // y’a de rares sections qui n’ont pas de case navigation, donc n’appellent pas finNavigation
    // et dans ce cas c’est logique de n'être appelé qu’au clic sur sectionSuivante
    if (typeof this._nextIndex !== 'number' || !this.graphe[this._nextIndex]) {
      this._findNextAndSendResultat()
    }

    // si on est à la fin du graphe, on affiche toujours la boite de dialogue
    if (this.isGrapheFin && this.skipDialog) {
      // la section a demandé de ne pas afficher le dialog, on respecte ça, sauf si y’a eu des scores dans le parcours
      if (this.bilans.some(bilan => Number.isFinite(bilan.score))) this.skipDialog = false
    }
    // entre deux nœuds, on affiche la boîte que s’il y a une phrase à afficher
    if (!this.isGrapheFin && !this.conclusion) this.skipDialog = true
    if (this.skipDialog) {
      // on n’affiche aucune boite de dialogue (avant de passer à la section suivante ou d'afficher parcours terminé)
      if (this.isGrapheFin) {
        this.videLesZones()
        if (this.zonesElts.MG) j3pAddElt(this.zonesElts.MG, 'p', 'Parcours terminé', { style: { padding: '2em' } })
      } else if (this._nextIndex == null || !this.graphe[this._nextIndex]) {
        // _findNextAndSendResultat appelé un peu plus haut a déjà affiché le pb sur la page
        console.error(Error('Au moment de passer à la section suivante on la trouve pas, une erreur aurait déjà due être affichée'))
      } else {
        this._startSection(this._nextIndex)
      }
    } else {
      this._displayEndDialog()
    }
  }

  /**
   * Appelé par finNavigation si le graphe est terminé, envoie le bilan
   * et retourne l’index du nœud suivant dans le graphe (pas son id)
   * @private
   */
  _findNextAndSendResultat ():void {
    /**
     * Vérifie que la condition est réalisée
     * Attention, à appeler après _addBilan car on va lire les bilans pour savoir si la condition est ok ou pas
     * @param type pe|score
     * @param condition La condition, par ex
     *                            ">=10000"
     *                            "<=10000"
     *                            "in[10002;10004]"
     *                            "pe_1,pe_2"  // une des pe renvoyées par la section courante
     *                            "bien,mal" // chaine ne commençant pas par "pe" : pe renvoyée par un noeud précédent, associé au type pek
     *                            "sans condition"
     * @return undefined si la condition était invalide ou incompatible avec le type
     */
    function isConditionRealisee (type: string, condition: string): boolean {
      // console.log('isConditionRealisee de type', type, 'avec la condition', condition)
      if (typeof condition !== 'string') {
        me.notif(Error('condition n’est pas une string'))
        return false
      }

      // cas particulier de ("pe","sans condition")
      // {pe:"sans condition",...}
      // if (condition === 'sans condition' || condition === 'sanscondition' || condition === 'sans_condition' || condition === 'sans+condition') {
      // apparemment on peut avoir plusieurs écritures
      if (/^sans.?condition$/.test(condition)) return true

      const operator = condition.substring(0, 2)
      switch (operator) {
        case '<=':
        case '>=': { // ">=10000"
          const limite = Number(condition.substring(2))
          if (Number.isNaN(limite)) {
            me.notif(Error('condition invalide : ' + condition))
            return false
          }
          const isSup = operator === '>='
          // on a plein de graphe avec pe>=0, parce que les sections faisaient souvent du this.parcours.pe = this.score / this.donneesSection.nbitems
          // (ce qui est complètement stupide car c’est le score)
          // => si c’est une comparaison numérique on regarde toujours le score, donc pe>=0.5 reviendra à score>=0.5
          const res = (me.score as number) / me.donneesSection.nbitems
          if (Number.isNaN(res)) {
            me.notif(Error(`test de condition score avec ${condition} mais score (${me.score}) / donneesSection.nbitems (${me.donneesSection.nbitems}) n’est pas un nombre`))
            return false
          }
          return isSup ? res >= limite : res <= limite
        }

        case 'pe':
          if (type === 'pe') {
            const donneesSectionProps = Object.keys(me.donneesSection)
            if (donneesSectionProps.length < 1) {
              me.notif('donneesSection est vide, impossible de retrouver les pe possibles')
              return false
            }
            // on a un type pe avec une condition "peXxx" ou "peXxx,peYyy"
            //  on veut les phrases qui correspondent à ces pe_1,pe_4 (pour comparer à parcours.pe qui est la pe courante),
            //  on devrait les trouver dans donneesSection.pe_1 et donneesSection.pe_4
            for (let peCode of condition.split(',')) {
              // peCode peut être une ancienne pe qui a été remplacée dans la section (mais est toujours dans le paramétrage du graphe)
              if (me.donneesSection?.replacedPe?.[peCode] != null) peCode = me.donneesSection.replacedPe[peCode] as string
              if (hasProp(me.donneesSection, peCode)) {
                if (me.donneesSection[peCode] === me.parcours.pe) return true
              } else {
                me.notif(`Branchement avec une condition pe:${condition} mais donneesSection.${peCode} n’existe pas`)
              }
            }
            return false
          }
          me.notif(Error(`condition « ${condition} » incompatible avec le type ${type}`))
          return false

        default :
        // une ou des pe directement (sans opérateur) ?
        // avant le 06/05/2020 le return false fait ici n’était jamais vérifié et on tombait dans le return true par défaut
        // Désormais on regarde si une des pe retournée par le nœud courant correspond
          return condition.split(',').includes(me.getLastPe(idNoeud))
      }
    } // isConditionRealisee

    /**
     * Retourne true si l’objet passé en paramètre est un branchement valide
     */
    function isBranchementValide (branchement: LegacyConnector | PlainObject): branchement is LegacyConnector {
      if (typeof branchement !== 'object' || !branchement) {
        me.notif(Error('paramètre incorrect passé à isBranchementValide'))
        return false
      }
      // Cf le type ConnectorV1 dans lib/types.d.ts

      // il faut toujours un nn
      if (typeof branchement.nn === 'number') branchement.nn = String(branchement.nn)
      if (!isStringNotEmpty(branchement.nn)) return false
      // on passe snn en string si besoin
      if (typeof branchement.snn === 'number') branchement.snn = String(branchement.snn)
      // avant on comparait chaque branchement.prop avec du if (branchement.prop != undefined) return true
      // donc ça marchait pour une prop qui existe et qui n’est ni null ni undefined, tout le reste était ok
      // on veut au moins une condition score ou pe
      return isStringNotEmpty(branchement.pe) || isStringNotEmpty(branchement.score)
    } // isBranchementValide

    this._nextIndex = undefined
    const me: Parcours = this
    const idNoeud = this.getIdFromIndex(this.indexCourant as number) // id en string
    const startedAt = this.temps?.debutNoeud[this.indexCourant as number]
    const dureeNoeudSec = typeof startedAt === 'number' ? getDelaySec(startedAt) : 0
    // il sera mis à jour plus loin
    const lastBilan = this._addBilan(dureeNoeudSec)

    try {
      let nomSectionSuivante, _nextIndex, nextId
      // les options du nœud (branchements avec éventuellement en dernier un paramétrage)
      // on est pas sur un nœud fin donc ça doit être un tableau
      const node = this.graphe[this.indexCourant as number] as LegacyNode
      const opts = node[2] as LegacyNodeOptions
      // ex : [
      //   {pe:"pe1_1",nn:"2"},
      //   {score:">=0.5",nn:"2"},
      //   {pe:"pe1_3",nn:"1",nb:"1",max:"2",snn:"2"},
      //   {nbitems:5,limite:7}
      // ]
      const branchements: LegacyConnector[] = []
      for (const opt of opts) {
        if (isLegacyConnector(opt)) branchements.push(opt)
      }

      // phrase d’état renvoyée par la section
      let phraseDeConclusion = ''

      // boucle sur les branchements, y’a un break à la fin du if (isBranchementFound) pour sortir dans ce cas
      for (const branchement of branchements) {
        // Il faut traiter les surcharges comme {nbitems:5,limite:7} !
        // chaque branchement est un objet contenant une propriété pe et/ou score et/ou pek
        // si ce n’est pas le cas c’est que c’est un objet contenant les surcharges
        // dans cas on passe à l’élement suivant
        if (!isBranchementValide(branchement)) {
          console.error(Error(`Branchement invalide : ${stringify(branchement)}`))
          continue
        }

        // Il faut examiner chacune des conditions, on parcours les props qui démarrent par pe ou score et on s’arrête à la première qui match
        const isBranchementFound = Object.entries(branchement)
          .some(([prop, value]) => (/^(pe|score)/.test(prop)) && isConditionRealisee(prop, value))

        if (isBranchementFound) {
          // pour mémoriser cette valeur avant éventuelle modif
          if (typeof this.parcours.boucle !== 'number') this.parcours.boucle = 1
          const boucleParcours = this.parcours.boucle

          // on détermine nextId/phraseDeConclusion en fonction des ≠ cas, boucle ou pas, max atteint ou pas, et on affecte si besoin this.parcours.boucle
          if (this.isNoeudFin(branchement.nn)) {
            nextId = branchement.nn
            phraseDeConclusion = branchement.conclusion ?? ''
            this.skipDialog = false // dans tous les cas, pour avoir le bilan
          } else {
          // ALEX au 02/10/15 : pour noeuds non ordonnés (avec des 'trous' dans les numéros de noeuds)
            if (branchement.nn === idNoeud) {
              // on reste donc dans le même noeud donc boucle
              this.parcours.boucle++
              if ((branchement.max) && (branchement.max < this.parcours.boucle)) {
              // il y a un maximum et il a été dépassé
                nextId = branchement.snn
                phraseDeConclusion = branchement.sconclusion ?? ''
              } else {
                nextId = idNoeud
                phraseDeConclusion = branchement.conclusion ?? ''
              }
            } else {
            // vers un autre nœud
              this.parcours.boucle = 1
              // AU 31/10/16 : ajout d’une autre possibilité, boucle sur tout un graphe à l’aide d’une prop maxParcours (qui utilisera aussi snn et scondition,
              // car pas pertinent d’avoir nn=noeud_en_cours donc pas de doublon d’utilisation de snn et scondition)
              // Normalement on a une propriété max ajouté automatiquement par l’éditeur quand ça boucle sur le même nœud
              // (nb max d’exécution du nœud évalué en sortie de nœud,
              // this.parcours.boucle remis à 1 quand on rentre dans le nœud en venant d’un autre),
              // et maxParcours quand ça boucle vers un nœud précédent du graphe (idem, mais comparé à this.parcours.boucleGraphe[idNoeud] qui n’est jamais remis à 1)
              if (branchement.maxParcours != null) {
                // le nb de fois où l’on a terminé le nœud courant
                // @ts-ignore
                if (this.parcours.boucleGraphe[idNoeud] == null) this.parcours.boucleGraphe[idNoeud] = 0
                // @ts-ignore
                this.parcours.boucleGraphe[idNoeud]++
                // si max atteint on oriente vers snn, sinon nn
                // @ts-ignore
                if (this.parcours.boucleGraphe[idNoeud] < branchement.maxParcours) {
                  nextId = branchement.nn
                  phraseDeConclusion = branchement.conclusion ?? ''
                } else {
                  nextId = branchement.snn
                  phraseDeConclusion = branchement.sconclusion ?? ''
                }
              } else {
                nextId = branchement.nn
                phraseDeConclusion = branchement.conclusion ?? ''
              }
            }
          }

          // ces affectations ne dépendent que de nextId
          _nextIndex = this.getIndexFromId(nextId as string)
          const node = this.graphe[_nextIndex]
          if (node != null) {
            nomSectionSuivante = node[1] // éventuellement fin

            // on maj le bilan avant d’envoyer un résultat
            lastBilan.nextId = nextId
            lastBilan.fin = (nomSectionSuivante === 'fin')
            if (lastBilan.fin) {
              lastBilan.ns = 'fin' // dans ce cas c’est le pseudo nom de la section, pas terrible (car pas un index) mais comme ça depuis toujours
            } else {
            // on change de section, la préparation de la section suivante était faite ici mais elle est désormais
            // dans _startSection qui sera appelée par dialogue
              lastBilan.ns = _nextIndex // sinon c’est bien l’index
            }
            lastBilan.boucle = boucleParcours ?? 0 // la valeur avant modif
          } else {
            j3pShowError('Parcours invalide avec un nœud suivant qui n’existe pas (' + nextId + ')')
            _nextIndex = undefined
          }
          // on envoie dans tous les cas (si le nœud suivant n’existe pas faut quand même le faire)
          this._sendResult()
          this.isGrapheFin = lastBilan.fin
          this._nextIndex = _nextIndex
          this.conclusion = phraseDeConclusion
          // si on a trouvé le bon branchement, on arrête là
          return
        } // if (isBranchementFound)
      } // for (var i=0;i<opts.length;i++){

      // on a passé en revue tous les branchements et rien trouvé, pas normal… (mais on envoie à bugsgnag que les pbs de nos graphes en prod)
      if (this.sesathequeBaseId === 'sesabibli') j3pNotify(Error('pas trouvé de nœud suivant…'), { parcoursAvecSuivantIntrouvable: this })
      j3pShowError('Graphe du parcours incorrect, aucun nœud suivant.')
    } catch (error) {
    // ça peut arriver si un nn pointe vers un truc qui n’existe pas, normalement c’est vérifié par normalizeGraphe
    // mais on sait jamais, un bug est vite arrivé, vaudrait mieux le savoir…
      j3pNotify(Error('plantage à la détermination du nœud suivant'), { parcours: this })
      j3pShowError('Impossible de déterminer la section suivante pour continuer :-(')
    }
  } // _findNextAndSendResultat

  /**
 * Vide les zones principales (cf constante zonesToEmpty pour la liste suivant la structure de page)
 */
  videLesZones ():void {
    // on vide les zones que construitStructurePage a créé, mais pas toutes !
    const s = this.structure
    if (s == null) return console.error(Error('Il faut construire la page avant de vider ses zones'))
    const zones = zonesToEmpty[s]
    if (zones == null) return console.error(Error('structure ' + s + ' inconnue'))
    const me = this
    for (const zone of zones) {
      if (me.zonesElts[zone]) {
        j3pEmpty(me.zonesElts[zone])
      } else {
        console.error(Error('La zone ' + zone + ' n’avait pas été créée, impossible de la vider'))
      }
    }
    // on remet les boutons (s’ils sont toujours là, comme dans la plupart des cas, ça ne fera rien)
    this.ajouteBoutons()
  }

  /**
   * Anime l’affichage de l’indication au clic sur le bouton indication
   */
  indication (zone: ZoneName, message: string) {
    try {
      const div = j3pAddElt(zone, 'div', '', { className: 'indication' })
      const a = j3pAddElt(div, 'a', 'Voir un indice', { href: '#', className: 'indication' })
      const eltIndic = j3pAffiche(div, 'phraseIndicIG', message)
      eltIndic.parent.className = 'hidden'
      a.addEventListener('click', () => {
        a.style.display = 'none'
        eltIndic.parent.classList.remove('hidden')
      })
    } catch (error) {
      console.error(error)
    }
  }

  /**
   * Affiche le titre fourni
   * @param titre Le titre à afficher
   * @param [options]
   * @param [options.replace=false] Passer true our remplacer le titre courant (sinon on ajoute)
   */
  afficheTitre (titre: string, { replace = false } = {}) {
    if (!this.zonesElts.HG) return console.error(Error('La zone HG n’existe pas, impossible de mettre le titre'))
    if (typeof titre !== 'string') return console.error(Error('titre invalide'), typeof titre)
    if (!titre) return console.warn(Error('titre vide'))
    if (replace) j3pEmpty(this.zonesElts.HG)
    const lines = titre.split(/<br *\/?>|\n/i)
    if (lines.length === 1) return j3pAddElt(this.zonesElts.HG, 'p', titre)
    // sinon plusieurs lignes, on ajoute du span pour décaler la 2e
    const props = { style: { lineHeight: '40px' } }
    const p = j3pAddElt(this.zonesElts.HG, 'p', lines[0], props)
    j3pAddElt(p, 'br')
    j3pAddElt(p, 'span', lines[1])
    if (lines.length > 2) console.error(Error('titre sur plus de deux lignes, seules les deux premières sont affichées'), lines)
  }

  /**
 * Met le focus sur l’élément _idPrefix + id
 */
  focus (id: string) {
    const elt = j3pElement(this._idPrefix + id)
    if (elt) elt.focus()
  }

  /**
   * Rappelle la section courante (après la fin de son chargement complet s’il est encore en cours)
   */
  sectionCourante (nouvelEtat?: string) {
    /**
     * Encapsule le code dans un wrapper pour l’exécuter dans un setTimeout, pour corriger le pb des sections
     * qui nous appellent en plein milieu d’un case correction|navigation, qui relancerait la section dans un autre case
     * alors que le case courant n’a pas terminé !
     * typiquement du
     * case 'correction':
     *   if (…) {
     *     this.sectionCourante()
     *   }
     *   finCorrection()
     * @private
     * @this {Parcours}
     */
    const wrapper = () => {
      // le bouton valider nous appelle avec un event, on l’ignore
      if (nouvelEtat && typeof nouvelEtat === 'string') this.etat = nouvelEtat
      const nomFn = 'Section' + this.nomSection
      // @ts-ignore
      if (typeof this[nomFn] !== 'function') return j3pShowError('La section ' + this.nomSection + ' n’a pas été chargée, impossible de la lancer')
      // init terminé ?
      if (this.lastCall?.isInitPending) {
        if (this.lastCall.isCallPending) {
        // y’a déjà un appel en attente, on ne veut pas en empiler un 2e
          return j3pShowError('Il y a déjà une action en attente, merci de patienter…')
        }
        // faut pas rappeler la section tout de suite car elle n’a pas terminé son init
        const isInitDone = () => !this.lastCall?.isInitPending
        const todoThen = () => {
          const currentState = this.etat // normalement forcément enonce, mais on le note quand même
          try {
            // @ts-ignore
            this[nomFn]()
          } catch (error) {
            console.error(`Après attente de la fin de son init, la section a planté à l’appel de sectionCourante avec l’état ${currentState}`, error)
          }
        }
        // on note qu’on a déjà un appel en attente, pour pas en empiler un 2e
        this.lastCall.isCallPending = true
        // et on démarre l’attente de la fin de l’init
        return waitFor(isInitDone, todoThen, Date.now() + 60000)
      }

      /**
       * Stocke l’état de la section lors de son dernier appel, pour éviter de la rappeler avant qu’elle n’ait terminé son init
       */
      this.lastCall = {
        etat: this.etat,
        isInitPending: this.debutDeLaSection,
        isCallPending: false,
        history: (this.lastCall && this.lastCall.history) || []
      }
      /**
       * Un élément d’historique par appel de la section
       */
      this.lastCall.history.push({ etat: this.etat, nomSection: this.nomSection ?? '', ts: Date.now() })

      // et on lance la section
      try {
        // incrémentations des compteurs et choix du bouton affiché
        switch (this.etat) {
          case 'enonce':
          // juste avant l’appel on masque tous les boutons, si y’a des boutons (pas le cas à l’init)
            if (this.buttonsElts.container) {
              this.cacheBoutonValider()
              this.cacheBoutonSuite()
              this.cacheBoutonSectionSuivante()
            }
            this.essaiCourant = 0
            this.questionCourante++
            if (this.etapeCourante < this.donneesSection.nbetapes) {
              this.etapeCourante++
            } else {
              this.etapeCourante = 1
              this.repetitionCourante++
            }
            // on vide d’éventuelle erreurs
            // @ts-ignore
            j3pEmpty(this.conteneur?.parentElement?.querySelector('.j3pErrors'))

            // console.log(`incrément => etapeCourante ${this.etapeCourante} et repetitionCourante ${this.repetitionCourante}`)
            // on affiche pas tout de suite le bouton valider, c’est finEnonce() qui le fera
            break

          case 'correction':
            this.essaiCourant++
            break

          case 'navigation':
            if (this._isPassive) {
              return setTimeout(this.sectionSuivante.bind(this), 0)
            }
            // c’est normalement finNavigation() qui l’affiche si on est toujours dans l’état navigation
            // mais y’a plein de sections qui l’appellent pas
            this.afficheBoutonSectionSuivante()
            break

          default:
            throw TypeError('etat invalide ' + this.etat)
        }
        // pour le keyup (au cas où ce lancement est provoqué par un keydown sur entrée
        // il ne faut pas que relâcher la touche entrée provoque un appel du case correction
        // et affiche "il faut donner une réponse)
        this._lastSectionCallTs = Date.now()
        // on peut appeler la section
        const currentState = this.etat
        try {
          // @ts-ignore
          this[nomFn]()
          // on passe debutDeLaSection à false ici, la section n’a plus besoin de le faire
          this.debutDeLaSection = false
        } catch (error) {
          console.error(`La section a planté à l’appel de sectionCourante avec l’état ${currentState}`, error)
          erreurInterne()
          this.notif(error)
        }
      // le changement d’état enonce => correction sera fait dans finEnonce qui doit impérativement être appelé
      } catch (error) {
        console.error(error)
        erreurInterne()
        this.notif(error)
      }
    } // wrapper

    // console.log('appel sectionCourante()', (typeof nouvelEtat === 'string' && nouvelEtat) || this.etat)

    setTimeout(wrapper.bind(this), 0)
  }

  /**
   * Décrémente essaiCourant et affiche un message si elt est fourni (en lui appliquant le style cfaux)
   * @param [elt] Le conteneur dans lequel on va afficher le message
   * @param [message] Fournir un message personnalisé si besoin (sinon c’est la phrase reponseManquante par défaut)
   * @param [doNotReplace=false] Passer true pour ajouter le message au conteneur plutôt que de remplacer le contenu
   */
  reponseManquante (elt: HTMLElement | string, message: string, doNotReplace = false) {
    if (this.essaiCourant > 0) this.essaiCourant--
    else console.error(Error('appel de reponseManquante avec essaiCourant ' + this.essaiCourant))
    if (elt) {
      if (typeof elt === 'string') elt = j3pElement(elt) as HTMLElement // on vérifie juste après que le as est vrai
      if (!isHtmlElement(elt)) return console.error(Error('Conteneur introuvable, impossible d’afficher'))
      if (!message) message = '<br>' + reponseManquante
      const options = doNotReplace ? {} : { replace: true }
      j3pAddContent(elt, message, options)
      elt.style.color = this.styles.cfaux
    }
  }

  /**
   * Affiche la réponse KO avec le bon style
   * @param elt
   * @param [message] Le message (message cfaux par défaut)
   * @param [doNotReplace] Passer true pour ajouter plutôt que remplacer le contenu
   */
  reponseKo (elt: HTMLElement | string, message = cFaux, doNotReplace = false) {
    if (typeof elt === 'string') elt = j3pElement(elt) as HTMLElement // on vérifie juste après que le as est vrai
    if (!isHtmlElement(elt)) throw Error('Conteneur introuvable, impossible d’afficher')
    const options = doNotReplace ? {} : { replace: true }
    j3pAddContent(elt, message, options)
    elt.style.color = this.styles.cfaux
  }

  /**
   * Affiche la réponse OK avec le bon style
   * @param elt
   * @param [message] Le message (message cbien par défaut)
   * @param [doNotReplace] Passer true pour ajouter plutôt que remplacer le contenu
   */
  reponseOk (elt: HTMLElement | string, message = cBien, doNotReplace = false) {
    if (typeof elt === 'string') elt = j3pElement(elt) as HTMLElement // on vérifie juste après que le as est vrai
    if (!isHtmlElement(elt)) throw Error('Conteneur introuvable, impossible d’afficher')
    const options = doNotReplace ? {} : { replace: true }
    j3pAddContent(elt, message, options)
    elt.style.color = this.styles.cbien
  }

  /**
   * Retourne true si on est sur la dernière répétition (dernière question)
   */
  sectionTerminee () {
    return this._isPassive || this.questionCourante >= this.donneesSection.nbitems
  }

  /**
   * Ajoute les boutons valider/suite/sectionSuivante, en ne laissant visible que le bouton valider
   * Les boutons sont toujours dans la zone ID, sauf pour presentation3 où ils sont dans la zone MG
   * (qui a aussi du contenu, d’où la gestion particulière quand on vide cette zone avec cette structure,
   * il faut rappeler la création des boutons, videLesZones le gère)
   * @return le conteneur #BoutonsJ3P
   */
  ajouteBoutons (): HTMLDivElement {
    if (this.buttonsElts == null) throw Error('Impossible de retrouver les boutons')
    const isPres3 = this.structure === 'presentation3'
    const parent = isPres3 ? this.zonesElts.MG : this.zonesElts.ID
    if (!parent) throw Error('Impossible de trouver un conteneur pour y placer les boutons')
    // on a des cas où parent existe mais n’est pas dans le dom, des gamins qui jouent avec le dom dans les devTools…
    if (parent.id !== (isPres3 ? 'MepMG' : 'MepID') || j3pElement(parent.id) !== parent) {
      throw Error('La construction de la page a échoué, il faudrait recharger cette page')
    }
    let container = j3pElement('BoutonsJ3P', null)
    if (!container || container !== this.buttonsElts.container) {
      if (container) j3pDetruit(container)
      // on crée le conteneur
      container = j3pAddElt(parent, 'div', '', { id: 'BoutonsJ3P' })
      if (isPres3) {
      // le conteneur des boutons est en absolute, et avec pres3 ça le met par dessus le contenu
      // C’est vraiment compliqué de le caler en bas à droite sans superposition
      // (faudrait essayer des trucs comme https://css-tricks.com/float-an-element-to-the-bottom-corner/
      // mais ça oblige à changer la structure html et pas sûr que ça fonctionne avec le positionnement absolute de MG)
      // => on passe le bouton en float, donc à priori en haut à droite
        container.style.position = 'static'
        container.style.float = 'right'
        container.style.margin = '10px'
      }
      this.buttonsElts = { container }
      // et les boutons
      this.ajouteBoutonValider(false, container)
      this.ajouteBoutonSuite(false, container)
      this.ajouteBoutonSectionSuivante(false, container)
    }
    if (this.buttonsElts.container == null) throw Error('Impossible de trouver les boutons')
    // et on les masque tous, le bouton valider sera affiché dans finEnonce
    this.cacheBoutonValider()
    this.cacheBoutonSuite()
    this.cacheBoutonSectionSuivante()
    // si on est au stade enoncé on ajoute la classe loading au div des boutons (sera retiré dans finEnonce)
    if (this.etat === 'enonce') {
      this.buttonsElts.container.classList.add('loading')
    }
    return this.buttonsElts.container as HTMLDivElement
  }

  /**
   * Ajoute le bouton valider (avec son listener sectionCourante)
   * @param [withFocus] Passer true pour lui donner le focus
   * @param [container] Le passer pour éviter d’appeler getBoutonsContainer
   */
  ajouteBoutonValider (withFocus = false, container?: HTMLElement) {
  // lui on ne le cache pas dès le clic, car c’est finEnonce() qui l’affiche, et on peut être en état correction et y rester après un clic OK sans repasser par finEnonce
    const onClick = this.sectionCourante.bind(this)
    if (!container) container = getBoutonsContainer(this)
    const ajouteBoutonOptions = window.isPlaywright
      ? {}
      : { minDelay: 1000, debounceMessage: 'Il faut prendre le temps de lire le message avant de cliquer de nouveau.', vanishAfter: 3 }
    this.buttonsElts.valider = j3pAjouteBouton(container, onClick, {
      id: this._idPrefix + 'boutonvalider', // playwright l'utilise
      className: 'big ok',
      value: 'OK'
    }, ajouteBoutonOptions)
    if (withFocus && this.buttonsElts.valider != null) this.buttonsElts.valider.focus()
    return this.buttonsElts.valider
  }

  /**
   * Affiche le bouton valider
   * @param [withFocus] Passer true pour lui donner le focus
   */
  afficheBoutonValider (withFocus?:boolean) {
    if (!this.buttonsElts.valider) {
      console.error(Error('Pas de bouton valider, on crée les 3 et ne montre que valider'))
      this.ajouteBoutons()
    }
    if (this.buttonsElts.valider) {
      this.buttonsElts.valider.style.display = 'inline'
      if (withFocus) this.buttonsElts.valider.focus()
      // et on cache les deux autres
      this.cacheBoutonSuite()
      this.cacheBoutonSectionSuivante()
    } // sinon rien, y’a déjà eu les erreurs en console à la tentative précédente
  }

  /**
 * Cache le bouton valider (à appeler juste après finEnonce() si vous voulez gérer la validation autrement, avec la touche entrée par ex)
 */
  cacheBoutonValider () {
    if (this.buttonsElts.valider) this.buttonsElts.valider.style.display = 'none'
    else console.error(Error('Pas de bouton valider'))
  }

  /**
   * Ajoute le bouton suite (avec son listener clicBoutonSuite qui rappelle sectionCourante)
   * @param [withFocus] Passer true pour lui donner le focus
   * @param [container] Le passer pour éviter d’appeler getBoutonsContainer
   */
  ajouteBoutonSuite (withFocus = false, container?: HTMLElement): HTMLElement {
    const onClick = () => {
      this.cacheBoutonSuite()
      this.sectionCourante()
    }
    if (!container) container = getBoutonsContainer(this)
    if (this.buttonsElts == null) throw Error('Impossible de trouver les boutons')
    this.buttonsElts.suite = j3pAjouteBouton(container, onClick, {
      id: this._idPrefix + 'boutoncontinuer',
      className: 'big suite',
      value: 'Suite'
    }, { minDelay: 500, message: 'Un peu de patience…' })
    if (withFocus) this.buttonsElts.suite.focus()
    return this.buttonsElts.suite
  }

  /**
 * Masque le bouton suite
 */
  cacheBoutonSuite (): void {
    if (this.buttonsElts.suite) this.buttonsElts.suite.style.display = 'none'
    else console.error(Error('Le bouton suite n’existe pas'))
  }

  /**
   * Affiche le bouton suite (il faut l’avoir créé avant, sinon ça râle)
   * @param [withFocus] Passer true pour lui donner le focus
   */
  afficheBoutonSuite (withFocus = false) {
    if (this.buttonsElts.suite) {
      this.buttonsElts.suite.style.display = 'inline'
      if (withFocus) this.buttonsElts.suite.focus()
      // et on cache les deux autres
      this.cacheBoutonValider()
      this.cacheBoutonSectionSuivante()
    } else {
      console.error(Error('Pas de bouton suite, on crée les 3 et ne montre que suite'))
      this.ajouteBoutons()
      this.afficheBoutonSuite()
    }
  }

  /**
   * Ajoute le bouton section suivante (qui appellera sectionSuivante, mais une seule fois)
   * À ne pas utiliser dans une section, il faut laisser le modèle gérer les boutons
   * @param [withFocus] Passer true pour lui donner le focus
   * @param [container] Le passer pour éviter d’appeler getBoutonsContainer
   */
  ajouteBoutonSectionSuivante (withFocus = false, container ?: HTMLElement): HTMLElement {
    const onClickDelayed = () => {
      // faut cacher le bouton tout de suite
      this.cacheBoutonSectionSuivante()
      // toujours appeler sectionSuivante en async pour laisser le code sync en cours se terminer
      setTimeout(this.sectionSuivante.bind(this), 0)
    }
    if (!container) container = getBoutonsContainer(this)
    this.buttonsElts.sectionSuivante = j3pAjouteBouton(container, onClickDelayed, {
      id: this._idPrefix + 'sectioncontinuer',
      className: 'big suivant',
      value: 'Section suivante'
    }, { minDelay: -1, debounceMessage: 'Il faut attendre que la section suivante se charge…' })
    if (withFocus) this.buttonsElts.sectionSuivante.focus()
    return this.buttonsElts.sectionSuivante
  }

  /**
   * Masque le bouton section suivante
   */
  cacheBoutonSectionSuivante () {
    if (this.buttonsElts.sectionSuivante) this.buttonsElts.sectionSuivante.style.display = 'none'
    else console.error(Error('Pas de bouton sectionSuivante'))
  }

  /**
   * Affiche le bouton section suivante
   * @param [withFocus] Passer true pour lui donner le focus
   */
  afficheBoutonSectionSuivante (withFocus?: boolean): void {
    if (this.buttonsElts.sectionSuivante) {
      this.buttonsElts.sectionSuivante.style.display = 'inline'
      if (withFocus) this.buttonsElts.sectionSuivante.focus()
      this.cacheBoutonValider()
      this.cacheBoutonSuite()
    } else {
      console.error(Error('Pas de bouton sectionSuivante, on crée les 3 et n’affiche que sectionSuivante'))
      this.ajouteBoutons()
      this.afficheBoutonSectionSuivante()
    }
  }

  /**
   * Augmente nbrepetitions et met à jour nbitems et l’affichage
   */
  ajouteRepetition (n = 1):void {
    if (!Number.isInteger(n)) throw Error(`paramètre invalide (${n})`)
    this.donneesSection.nbrepetitions += n
    this.donneesSection.nbitems = this.donneesSection.nbetapes * this.donneesSection.nbrepetitions
    this._rafraichirEtat()
  }

  /**
   * Initialise MepHD avec le n° de question, l’éventuel temps restant et le score
   * (sauf si _isPassive)
   * @private
   */
  _rafraichirEtat ():void {
    const container = this.zonesElts.HD
    if (!container) return this.notif(Error('Impossible d’initialiser la barre d’état sans zone HD'))
    const elts = [this.zonesElts.question, this.zonesElts.delai, this.zonesElts.score]
    if (elts.some(function (elt) {
      return !elt
    })) {
    // il en manque un => initialisation du html
      j3pEmpty(container)
      this.zonesElts.question = j3pAddElt(container, 'p', '', { id: 'etatQuestion' }) // #etatQuestion utilisé par testBrowser uniquement
      // on ajoute toujours un <p> pour le temps limite, éventuellement vide
      this.zonesElts.delai = j3pAddElt(container, 'p')
      this.zonesElts.score = j3pAddElt(container, 'p', '', { id: 'etatScore' }) // #etatScore utilisé par testBrowser uniquement
    } else if (elts.some(elt => !container.contains(elt))) {
    // on a nos 3 ref sur les HTMLElement mais y’en a au moins un qui n’est plus dans HD, on reset…
      j3pEmpty(container)
      for (const elt of elts) container.appendChild(elt)
    }

    // Question X sur Y
    const question = this.zonesElts.question
    j3pEmpty(question)
    if (this._isPassive) {
      j3pAddContent(question, 'Pas de question interactive')
    } else {
      const numQuestion = this._getNumQuestion()
      j3pAddTxt(question, 'Question : ')
      j3pAddElt(question, 'span', numQuestion, { className: 'numQuestion' })
      j3pAddElt(question, 'span', ' sur ', { className: 'petit' })
      j3pAddTxt(question, String(this.donneesSection.nbrepetitions))
      // console.log(`on a affiché Question ${numQuestion} sur ${this.donneesSection.nbrepetitions}`)
      // section S
      if (this.nbSections > 1) {
        j3pAddElt(question, 'span', ' (section&nbsp;' + this.indexProgression + ')', { className: 'section' })
      }
    }

    // temps limité
    const eltDelai = this.zonesElts.delai
    if (this.donneesSection.limite) {
    // en est appelé par _timeout, faut le gérer
      if (this.isElapsed) {
        j3pAddContent(eltDelai, 'Délai max de ' + this.donneesSection.limite + 's dépassé', { replace: true })
      } else {
      // y’a des sections qui nous rappellent après _timeout…
        if (this.temps.debutItem) {
          const elapsedMs = (this.temps.finItem ?? Date.now()) - this.temps.debutItem
          const reste = Math.max(0, Math.round(this.donneesSection.limite - elapsedMs / 1000))
          const msgDelai = 'Reste : ' + reste + ' seconde' + (reste > 1 ? 's' : '')
          j3pAddContent(eltDelai, msgDelai, { replace: true })
          // et on remet le rectangle dedans si y’en avait un
          if (this.zonesElts.delaiGraphique) {
            eltDelai.appendChild(this.zonesElts.delaiGraphique)
            this.zonesElts.delaiGraphique.style.width = this.isElapsed ? '0' : '100%'
          }
        }
        // sinon il sera ajouté par le prochain appel de _tickTimer, dans max 100ms
      }
    } else if (eltDelai.innerHTML) {
      j3pEmpty(eltDelai)
    }

    if (!this._isPassive) {
    // score
      const score = this.zonesElts.score
      j3pEmpty(score)
      j3pAddTxt(score, 'Score : ')
      const scoreMsg = j3pArrondi(<number> this.score, 2) + ' sur ' + this.donneesSection.nbitems
      j3pAddElt(score, 'span', scoreMsg)
    }
  // console.log(`fin _rafraichirEtat avec repetitionCourante = ${this.repetitionCourante} et etapeCourante = ${this.etapeCourante}`)
  } // _rafraichirEtat

  /**
 * Détruit la minuterie en cours si elle existe (et laisse l’affichage du temps restant figé)
 * Appelé par le modèle juste avant le case navigation, donc au clic sur suite.
 * Les sections ne devraient pas l’appeler, mais peuvent le faire par exemple pour couper avant une correction asynchrone.
 */
  _stopTimer () {
    if (this._timerId) clearTimeout(this._timerId)
    if (this._tickTimerId) clearInterval(this._tickTimerId)
    this._timerId = null
    this._tickTimerId = null
    if (this.temps.debutItem) this.temps.finItem = Date.now()
  }

  /**
   * Doit être appelé en fin d’énoncé (debutDeLaSection ou pas)
   * Initialise la gestion du temps si on est en temps limité, passe l’état en correction
   */
  finEnonce () {
    try {
      // console.log('finEnonce avec l’état ' + this.etat)
      // on vérifie que la section n’a pas tripoté l’instance courante avec ses mains sales
      if (this._props != null) {
        const actualProps = Object.keys(this)
        if (actualProps.length !== this._props.length) {
          const lost = this._props.filter(p => !actualProps.includes(p))
          // @ts-ignore
          const added = actualProps.filter(p => !this._props.includes(p))
          if (lost.length) console.error(Error(`La section ${this.nomSection} a supprimé des propriété de l’instance courante de Parcours (${lost.join(', ')}), il ne faut surtout pas faire ça !!!`))
          if (added.length) console.error(Error(`La section ${this.nomSection} a ajouté des propriété à l’instance courante de Parcours (${added.join(', ')}), il ne faut pas faire ça !`))
        }
        this._props = undefined
      }
      if (!this.structure) {
        console.error(Error('La section appelle finEnonce() avant d’avoir appelé construitStructurePage'))
        throw Error('Erreur d’initialisation')
      }
      this.etat = this._isPassive ? 'navigation' : 'correction'
      const now = Date.now()
      if (!this.temps) this.temps = {} as LegacyTempsProp
      if (!this.indexCourant) {
        console.error(Error('Index courant non initialisé'))
        this.indexCourant = 1
      }
      this.temps.debutItem = now
      this.temps.finItem = undefined
      // si on est au premier appel de ce nœud le ts vaut 0, pour l’initialiser (ici, après la fin du chargement et pas au début)
      if (this.temps.debutNoeud[this.indexCourant] === 0) {
        this.temps.debutNoeud[this.indexCourant] = now
      }
      // idem pour le graphe
      if (this.temps.debutGraphe === 0) {
        this.temps.debutGraphe = now
      }
      if (!this.lastCall) {
        this.lastCall = {} as LastCall
      }
      this.lastCall.isInitPending = false
      if (this.buttonsElts.container) {
        this.buttonsElts.container.classList.remove('loading')
        if (this._isPassive) {
          this.afficheBoutonSectionSuivante()
        } else {
          this.afficheBoutonValider()
        }
      } else {
        console.error(Error('pas de boutons…'))
      }

      if (!this._isSectionInitDone) {
        // check donneesSection (on vérifie un minimum d’initialisation correcte)
        checkDonneesSection(this)

        // ajout gestion touche entrée
        if (this.validOnEnter) {
          if (!this._keyupListener) {
            const me = this
            this._keyupListener = function (evt: KeyboardEvent) {
              // on ne s’occupe que de l’état correction (ça suffit pas, cf plus bas car une touche entrée sur le bouton OK
              // en état correction peut nous appeler avant que la section ne passe en état navigation|enonce, mais ça élimine déjà des cas)
              if (me.etat === 'navigation' || me.etat === 'enonce') return
              // faut gérer la touche entrée pendant le case correction
              if (evt.keyCode !== 13) return
              // FIXME on écoute tous les événements claviers, même la validation de zones, même l’activation d’un bouton
              // on essaie de ne pas réagir à la touche entrée sur un bouton qui aurait le focus (ça déclenche par ex suite au keydown puis ce code au keyup, et appelle 2× sectionCourante
              // => on se retrouve avec le message "il faut saisir une valeur" dès l’affichage de l’énoncé
              // this est toujours le conteneur, on utilise https://developer.mozilla.org/fr/docs/Web/API/Document/activeElement
              // si c’est un bouton qui a le focus on laisse tomber
              const currentFocusElt = document.activeElement
              if (Object.values(me.buttonsElts).some(button => button === currentFocusElt)) return
              // la ligne précédente sert pas à grand chose car la section rappelée au keydown a déjà changé le currentFocusElt

              // @todo ajouter dans le modèle un vrai tag <form> avec un listener submit, qui réagira différemment suivant que this.validOnEnter est true ou pas
              // on pourrait aussi regarder le currentFocusElt.constructor && currentFocusElt.constructor.name pour ne réagir que sur du button,
              // mais même ça ne marche pas pour la même raison (currentFocusElt a déjà changé depuis le keydown)

              // on tente une autre sortie pour ne pas réagir si le keydown vient de provoquer le case enonce (mais si l’utilisateur met plus de 500ms
              // à relâcher la touche entrée ça va quand même afficher "il faut entrer une réponse")
              if (me._lastSectionCallTs && getDelayMs(me._lastSectionCallTs) < 500) return

              evt.preventDefault() // à priori ne sert à rien, peu de chance que qqun écoute le keyup pour faire qqchose
              me.sectionCourante()
            } // _keyupListener
            this.conteneur.addEventListener('keyup', this._keyupListener, false)
          } // sinon il y est déjà et on le veut toujours, on le laisse
        } else if (this._keyupListener) {
          this.conteneur.removeEventListener('keyup', this._keyupListener, false)
        }

        this._sendInitResult()
        // fin du code effectué à la fin du 1er enoncé de la section
        this._isSectionInitDone = true
      }

      // on affiche d’office l’état, sauf pour les structures sans HD
      if (this.zonesElts.HD) this._rafraichirEtat()

      // on repositionne le scroll en haut pour toutes les zones qui en aurait
      try {
        this.conteneur.scrollTop = 0
        this.conteneur.scrollLeft = 0
        for (const zone of Object.values(this.zonesElts)) {
          zone.scrollTop = 0
          zone.scrollLeft = 0
        }
      } catch (error) {
        console.error(error)
      }

      // et on ajoute le timer (si on est pas passé par _rafraichirEtat car pas de HD y’aura rien de visible
      // sur l’écoulement du délai, ça enverra une notification bugsnag, mais y’aura quand même le stop de fin…)
      if (this.donneesSection.limite) {
        if (!this.zonesElts.HD) console.error(Error('Temps limité imposé avec une structure d’affichage sans zone HD, l’écoulement du temps ne sera donc pas visible'))
        if (this.questionCourante === 1) {
          // on crée une modale pour prévenir que c'est en temps limité, mais faut mémoriser l'élément qui a le focus pour le lui redonner
          // le pb est que les sections mettent souvent ce focus dans un setTimeout
          // (probablement à cause de mathquill), qui n'est pas encore passé lors de l'appel de finEnonce()
          // => on met tout ça dans un setTimeout aussi :-/
          setTimeout(() => {
            const focusedElt = (document.activeElement ?? document.body) as HTMLElement
            const modal = new Modal({ title: 'Attention, temps limité', inset: '10px', withoutClose: true })
            const { limite } = this.donneesSection
            const min = Math.floor(limite / 60)
            const sec = Math.floor(limite % 60)
            let limitStr
            if (min > 0) {
              limitStr = `${min} minute${min > 1 ? 's' : ''}`
              if (sec > 0) {
                limitStr += ` et ${sec} seconde${sec > 1 ? 's' : ''}`
              }
            } else {
              limitStr = `${sec} seconde${sec > 1 ? 's' : ''}`
            }
            const style = { textAlign: 'center' }
            modal.addElement('p', {
              content: `Cet exercice est en temps limité, tu auras ${limitStr} par question.`,
              style
            })
            const p = modal.addElement('p', { content: 'Prêt ?', style })
            j3pAddElt(p, 'br')
            const btn = j3pAddElt(p, 'button', 'Partez !')
            btn.addEventListener('click', () => {
              modal.destroy()
              this._startTimer()
              if (typeof focusedElt?.focus === 'function') {
                focusedElt.focus()
              }
            })
            modal.show()
          }, 0)
        } else {
          this._startTimer()
        }
      }
    } catch (error) {
      j3pShowError(error as Error)
    }
  } // finEnonce

  /**
   * Affiche le bouton suite si on est au dernier essai pour cette question
   * Désactive le timer si on est sorti de l’état correction
   * @param [nouvelEtat]
   * @param [recallMe] passer true pour rappeler sectionCourante()
   */
  finCorrection (nouvelEtat?: string, recallMe = false) {
    if (nouvelEtat && typeof nouvelEtat === 'string') this.etat = nouvelEtat
    // console.log('appel finCorrection()', this.etat)
    // console.log('finCorrection avec l’état', this.etat)
    if (this.etat === 'correction') {
      if (this.buttonsElts.valider != null) this.buttonsElts.valider.focus()
    } else {
      if (this._timerId) this._stopTimer()
      if (this.essaiCourant >= this.donneesSection.nbchances) this.afficheBoutonSuite(true)
    }
    // normalement le score est modifié dans le case navigation, mais tout le monde ne le fais pas, on met aussi à jour le score affiché en fin de correction
    this._rafraichirEtat()
    if (recallMe) this.sectionCourante()
  }

  /**
   * Met à jour le score affiché et éventuellement le temps restant
   * Affichera le bouton section suivante si on est toujours dans l’état navigation
   * ou suite si on est repassé dans l’état enonce
   * Pour les sections passive ça passe directement à la section suivante
   * @param [withFocus] passer true pour donner le focus au bouton suite/section suivante (si on l’affiche)
   */
  finNavigation (withFocus = true) {
    // console.log('finNavigation avec l’état ' + this.etat)
    this._rafraichirEtat()
    // on affiche d’office suite ou section suivante dans ces deux cas
    if (this.etat === 'navigation') {
      if (this.sectionTerminee()) {
        this._findNextAndSendResultat()
        if (typeof this._nextIndex === 'number') {
          if (this._isPassive) {
            // IMPORTANT, il ne faut pas appeler sectionSuivante() en sync,
            // car on pourrait être sync avec l'appel de wrapper, donc avant qu'il mette
            // debutDeLaSection à false (pour celle qui vient de se terminer ici)
            // et dans ce cas le 1er appel de cette section suivante se fait avec un début false
            // tester par ex avec sesacommun/646f0f40f9aba80df3b45f42 (section alea puis mtg qui a un init async)
            setTimeout(this.sectionSuivante.bind(this), 0)
          } else {
            this.afficheBoutonSectionSuivante(withFocus)
          }
        } else if (this.buttonsElts.container) {
          // on masque tous les boutons, ça va rester planté là (l’erreur est déjà affichée)
          this.buttonsElts.container.style.display = 'none'
        }
      } else {
        this.notif(Error('finNavigation appelé avec un état navigation mais sans que la section ne soit terminée'))
      }
    } else if (this.etat === 'enonce') {
      this.afficheBoutonSuite(withFocus)
    } else {
    // sinon c’est louche, on le signale
      console.error(Error('etat ' + this.etat + ' dans finNavigation'))
    }
  }

  setPassive () {
    this._isPassive = true
    this.skipDialog = true
    this.score = undefined
  }

  /**
   * Imposer une propriété de donneesSection que l’on ne veut pas rendre paramétrable, seulement parmi limite|nbetapes|nbrepetitions|nbchances
   * (pour le reste ça n’aurait pas de sens, soit c’est paramétrable soit c’est une variable interne à la section)
   * Avant cette fonction surchargeait donneesSection avec les paramètres fournis par le graphe, mais c’est désormais fait d’office avant le premier appel de la section.
   * @param values les propriétés à surcharger (avec leurs valeurs)
   */
  surcharge (values: PlainObject) {
    if (!this.donneesSection) {
      erreurInterne()
      return this.notif(Error('surcharge appelé alors que donneesSection n’a pas été initialisé'))
    }

    // on regarde les params exportés par la section
    const params = this._getCurrentParametres()
    const paramsKeys = Object.keys(params)
    // il ne faut rien vérifier s’il n’y a aucune surcharge
    const dsKeys = Object.keys(this.donneesSection)
    if (typeof values === 'object') {
    // On nous passe des valeurs imposées (non paramétrables) pour surcharger donneesSection
      for (const [prop, value] of Object.entries(values)) {
        if (['limite', 'nbetapes', 'nbrepetitions', 'nbchances'].includes(prop)) {
          if (Number.isInteger(value)) this.donneesSection[prop] = value
          else console.error(Error(`${prop} doit être un entier`))
        } else {
          console.error(Error(`${prop} n’est pas une propriété gérée par surcharge()`))
        }
      }
    } else {
    // message pour annoncer que c’est désormais obsolète
      let msg = 'L’affectation de donneesSection et l’appel de surcharge() est désormais inutile, donneesSection est initialisé avec les bonnes valeurs (déjà surchargées) avant le premier appel de la section.'
      const dsAjouts = dsKeys.filter(k => !paramsKeys.includes(k))
      if (dsAjouts.length) {
        const plural = dsAjouts.length > 1 ? 's' : ''
        msg += `\nATTENTION, avant de supprimer l’affectation de donneesSection et l’appel de surcharge(), il faudra mettre en constante (ou dans storage) ${plural ? 'ces' : 'cette'} propriété${plural} qui étai${plural ? 'en' : ''}t dans donneesSection (sans raison d’y être car ce ${plural ? 'ne sont pas des' : 'n’est pas un'} paramètre${plural}). Vous pouvez ajouter avant l’export de la fonction principale le code qui suit (et modifier la lecture de ce${plural ? 's' : 'tte'} variable${plural} dans la section) :\n`
        for (const k of dsAjouts) {
          msg += `\nconst ${k} = ${stringify(this.donneesSection[k])}`
        }
      }
      console.warn(msg)

      for (let [prop, value] of Object.entries(params)) {
        if (typeof values === 'boolean' && values) {
          // on cast d’après la valeur par défaut mise par la section (avant surcharge donc)
          const defaultValue = this.donneesSection[prop] as ParamValue<boolean>
          switch (typeof defaultValue) {
            case 'string':
              value = String(value)
              break
            case 'number':
              value = Number(value)
              break
            case 'boolean':
              value = Boolean(value)
              break
            case 'object':
              if (typeof value === 'string') {
                try {
                  value = JSON.parse(value)
                } catch (error) {
                  console.error(`paramètre ${prop} invalide (pas du json valide)`, error)
                  value = defaultValue
                }
              }
              // on check la consistance de l’array
              if (Array.isArray(defaultValue)) {
                if (Array.isArray(value)) {
                  const type = typeof defaultValue[0] as 'string' | 'number' | 'boolean'
                  const caster = typeToConstructor[type]
                  if (caster == null) {
                    console.error(Error(`Le paramètre ${prop} a une valeur par défaut de type non géré`))
                  } else {
                    value = value.map(v => caster(v))
                  }
                } else {
                  console.error(`paramètre ${prop} invalide (pas un tableau)`)
                  value = defaultValue
                }
              } else if (typeof value !== 'object') {
              // object non array
                console.error(`paramètre ${prop} invalide (pas un object)`)
                value = defaultValue
              }
              break
            case 'undefined':
              console.error(Error(`paramètre ${prop} invalide (pas de valeur par défaut mise par la section)`))
              value = defaultValue // c'est undefined mais ts le voit pas
              break
            default:
              console.error(`Le paramètre ${prop} a une valeur par défaut de type incorrect ${typeof defaultValue}`)
          }
        }
        this.donneesSection[prop] = value
      }
    }
    if (params.debug) this.donneesSection.debug = true // debug mis dans le graphe
    if (this.donneesSection.debug) this.isDebug = true
    // la section peut avoir besoin de notre calcul de nbitems avant l’appel de finEnonce()
    checkDonneesSection(this)
  }

  /**
   * Log en console si on est en mode debug (ne fait rien sinon)
   * @param args arguments quelconques (passés tels quels à console.debug)
   */
  logIfDebug (...args: unknown[]): void {
    if (this.isDebug) console.debug(...args)
  }

  /**
   * Wrapper de j3pNotify (qui ajoute le parcours courant dans les metadatas)
   * @param error
   * @param [data] D’éventuelles données à joindre à la notification
   */
  notif (error: unknown, data: PlainObject = {}) {
    let err: Error
    if (typeof error === 'string') err = Error(error)
    else if (!(error instanceof Error)) err = Error('error')
    else err = error
    j3pNotify(err, { ...data, parcours: this })
  }

  /**
   * Gère le temps limité (donneesSection.limite), appelé par finEnonce()
   * Si on est en temps limité affecte temps.debutItem et (re)initialise la minuterie, ne fait rien sinon
   */
  private _startTimer () {
    this.isElapsed = false
    // Peut avoir déjà été appelé par une section, parfois plusieurs fois pour la même question (dans ce cas c’est le dernier appel qui reset le précédent)
    if (typeof this.donneesSection.limite === 'number' && this.donneesSection.limite > 0) {
      if (this._timerId) {
        console.error(Error('_startTimer appelé alors que le précédent tourne toujours'))
        this._stopTimer()
      }
      // le timer pour couper lorsque le temps est écoulé
      this._timerId = setTimeout(this._timeout.bind(this), this.donneesSection.limite * 1000)
      this.temps.debutItem = Date.now()
      this.temps.finItem = undefined

      if (!this.zonesElts.delai) this._rafraichirEtat()
      if (!this.zonesElts.delai) return // _rafraichirEtat n’a pas pu le créer et a déjà généré une erreur

      // Timer pour modifier le rectangle rouge, modifié par Yves pour avoir une avancée graphique tous les 100ms
      // variable incrémentée dans _tickTimer
      this._tickTime = 0
      this._tickTimerId = setInterval(this._tickTimer.bind(this), 100)
    }
  }

  /**
   * Actualise le compte à rebours présent dans la barre d’état et la barre rouge d’avancement
   * @private
   */
  _tickTimer () {
    if (typeof this._tickTime !== 'number') return console.error(Error('_tickTimer sans _tickTime'))
    this._tickTime++
    // Modification de Yves : ON compte le temps restant en dixièmes de seconde
    let resteMs = 0
    try {
      resteMs = getDelayMs(this.temps.debutItem)
    } catch (error) {
      console.error(error)
      j3pShowError('Horloge modifiée => temps écoulé')
      this._timeout()
    }
    const reste10 = this.donneesSection.limite * 10 - Math.round(resteMs / 100)
    if (this.zonesElts.delai && reste10 >= 0) {
      // à priori toujours vrai, au cas où le listener aurait pas été viré avant de passer négatif
      // on actualise le texte une fois sur 10 (toutes les secondes)
      if (this._tickTime % 10 === 0) {
        const resteSeconde = Math.round(reste10 / 10)
        this.zonesElts.delai.innerHTML = 'Reste : ' + resteSeconde + (resteSeconde > 1 ? ' secondes' : ' seconde')
      }
      if (!this.zonesElts.delaiGraphique) {
        this.zonesElts.delaiGraphique = j3pAddElt(this.zonesElts.delai, 'p', '', {
          style: {
            width: '100%',
            height: '2px',
            backgroundColor: '#F00'
          }
        })
      } else if (!this.zonesElts.delai.contains(this.zonesElts.delaiGraphique)) {
      // on nous a effacé du dom depuis la dernière fois, mais l’élément est toujours référencé par this.zonesElts.delaiGraphique
        this.zonesElts.delai.appendChild(this.zonesElts.delaiGraphique)
      }
      // màj barre rouge
      this.zonesElts.delaiGraphique.style.width = String(reste10 / this.donneesSection.limite * 10) + '%'
    }
  }

  /**
   * Met fin à l’exo (affiche que le temps est écoulé, passe isElapsed à true et appelle sectionCourante)
   * @private
   */
  _timeout () {
    this.isElapsed = true
    this._stopTimer()
    this._rafraichirEtat()
    this.sectionCourante()
  }

  /**
   * (ré)initialise l’objet parcours avant d’entrer dans un nœud du graphe (ça peut être le méme en cas de boucle)
   * @private
   */
  _startSection (index: number) {
    if (this.isGrapheFin) return
    const noeud = this.graphe[index]
    if (!noeud) {
      const errorMsg = (typeof index === 'number') ? 'Le nœud ' + index + ' n’existe pas' : 'Erreur interne, impossible de démarrer un nœud sans préciser lequel'
      this.notif(Error(errorMsg))
      return j3pShowError(errorMsg)
    }
    const nomSection = noeud[1]
    console.info('init du nœud ' + index + ' du graphe => ' + nomSection)
    const nomMethode = 'Section' + nomSection
    // @ts-ignore
    if (!(nomMethode in this) || typeof this[nomMethode] !== 'function') throw Error('La section suivante n’a pas été chargée ' + nomSection)
    this.nomSection = nomSection
    if (window.bugsnagClient && typeof window.bugsnagClient.addMetadata === 'function') {
      window.bugsnagClient.addMetadata('j3p', 'section', nomSection)
    }
    if (this.structure) this.videLesZones()
    this.structure = undefined // permettra de râler dans finEnonce() si construitStructurePage n’a pas été appelé
    this._ratioGauche = undefined // pour que le resize laisse tomber
    this.zonesElts = {} as ZonesElts
    if (typeof this.indexProgression !== 'number') this.indexProgression = 0
    this.indexProgression++
    this._nextIndex = undefined
    /**
     * Index du nœud courant dans le graphe (démarre à 0)
     * @type {number}
     */
    this.indexCourant = index
    this.temps.debutNoeud[index] = 0 // sera mis au bon ts par finEnonce
    /**
     * L’état courant du nœud, peut valoir enonce|correction|navigation
     * @type {string}
     */
    this.etat = 'enonce'
    // lui doit rester à true par défaut, seules quelques sections le mettent à false avant d’appeler finEnonce() qui l’utilise
    this.validOnEnter = true
    /**
     * Vaut true lors du premier appel d’une section dans un nœud (repassera à true si la meme section est rappelée plus tard dans le graphe)
     */
    this.debutDeLaSection = true
    /**
     * flag mis à false dans _startSection et à true dans _sendResult,
     * pour éviter d’envoyer le même résultat deux fois
     */
    this._isResultatSent = false
    /**
     * mis à true dans finEnonce
     */
    this._isSectionInitDone = false
    /**
     * Le numéro de la question (2 pour la question 2c, l’étape correspond à la lettre, le cumul étant questionCourante)
     * Incrémenté avant l’appel de la section en case enonce
     */
    this.repetitionCourante = 0
    /**
     * Incrémenté avant l’appel de la section en case correction (mis à 0 avant appel en case enonce au début de la section)
     * Il vaudra donc 0 au premier appel du case enonce et 1 au premier appel du case correction.
     */
    this.essaiCourant = 0
    /**
     * Initialisé à 1, incrémenté avant chaque case enonce si inférieur à donneesSection.nbetapes (sinon remis à 1)
     */
    this.etapeCourante = 1
    /**
     * Numéro de la question courante, démarre à 1 (avec 2 étapes questionCourante vaut 3 pour la question 2a)
     * Incrémenté avant l’appel de la section en case enonce
     */
    this.questionCourante = 0
    /**
     * Mettre à true pour les sections qui ne renvoient pas de score (truc à lire seulement par ex), et dans ce cas
     * - le score n’est plus indiqué dans l’état (dans HD)
     * - il est ignoré dans le calcul global du score (évite de fausser les résultats d’un score global
     *   en prenant dans la moyenne un 0 ou un 1 qui n’est pas significatif), sauf si toutes les sections faites
     *   sont dans ce cas (le score global est alors de 1 pour permettre le passage à la suite)
     * - il n’y a pas de n° de question indiqué dans l’état (HD) mais un texte précisant
     *   qu’il n’y a pas de question (sinon certains croient à un bug)
     *
     * Si la section retourne un score non number ça reviendra au même sur le calcul final, mais pas pour l’affichage
     * (on a besoin de savoir dès le départ qu’il n’y aura pas de score)
     */
    this._isPassive = false
    /**
     * Un tableau que la section utilise comme elle veut pour stocker ses infos
     * Utiliser storage à la place qui est un objet (et donc permet de nommer ce qu’on stocke)
     */
    this.stockage = []
    /**
     * Un Objet que la section utilise comme elle veut pour stocker ses infos (quand même plus pratique avec un objet pour nommer les propriétés)
     */
    this.storage = {
      // bcp de sections utilisent ça et certaines peuvent s’attendre à y trouver déjà un tableau
      solution: []
    }
    /**
     * Le score du nœud courant (en entier, il sera convertit en float dans [0;1] lors de l’envoi du résultat)
     */
    this.score = 0
    /**
     * Les données de la section (son paramétrage), à priori qui ne changent pas pendant le parcours
     * (mais certaines sections l’utilisent comme var globale pour y stocker des infos sur leur état)
     */
    this.donneesSection = {} as LegacyDonneesSection
    // on init donneesSection avec les params par défaut puis on surcharge avec ceux du graphe
    this._initParametres()
    // avant d’appeler la section, on note les propriétés de notre objet, pour râler dans finEnonce si la section a tripoté cette instance courante
    // mais faut ajouter ici toutes celles que le modèle va créer dans l’appel sectionCourante
    this.lastCall = {} as LastCall
    this._lastSectionCallTs = 0
    this._props = [] // pour que la clé existe ligne suivante
    this._props = Object.keys(this)
    // ALEX : si on voulait implémenter les donnees_graphe pour le noeud suivant et non le noeud courant (idée de Rémi) ce serait ici qu’il faudrait le faire
    this.sectionCourante()
  } // _startSection

  /**
 * Retourne les paramètres mis dans le graphe pour le nœud courant
 * @private
 */
  _getCurrentParametres ():LegacyNodeParams {
    if (typeof this.indexCourant !== 'number') throw Error('Problème d’initialisation du graphe')
    const noeud = this.graphe[this.indexCourant]
    if (!Array.isArray(noeud) || noeud.length < 2) return {} as LegacyNodeParams
    const paramBranchements = (Array.isArray(noeud) && noeud[2]) || []
    // les params sont le dernier elt de ce tableau
    const graphParams = paramBranchements[paramBranchements.length - 1] as LegacyNodeParams
    // mais ils n’existent pas toujours, et il peut n’y avoir que des branchements…
    if (!graphParams || typeof graphParams !== 'object' || graphParams.nn) return {} as LegacyNodeParams
    // pour compatibilité ascendante
    if (graphParams.MiseEnPagePlusClaire) {
      if (!graphParams.theme) graphParams.theme = 'zonesAvecImageDeFond'
      delete graphParams.MiseEnPagePlusClaire
    }
    return graphParams
  }

  /**
   * Retourne le n° de la question (suivi d'une lettre si on est pas à la 1re étape)
   */
  _getNumQuestion ():string {
    let numQuestion = String(this.repetitionCourante)
    if (this.donneesSection.nbetapes > 1) {
      let lettre
      // y’a des étapes, faut un suffixe en lettre
      if (this.etapeCourante > 26) {
        j3pNotify('Trop d’étapes, y’a pas assez de lettres dans l’alphabet', { donneesSection: this.donneesSection })
        // on ajoute un suffixe numérique, z2, z3… tant pis pour la pub pour BMW, à z80 ce sera un clin d'œil aux cpus des années 1980
        lettre = `z${this.etapeCourante - 26}`
      } else {
        const charCodeA = 'a'.charCodeAt(0) // le code utf16 de 'a' => 97
        lettre = String.fromCharCode(charCodeA + this.etapeCourante - 1)
      }
      numQuestion += lettre
    }
    return numQuestion
  }

  /**
 * init donneesSection avec les params par défaut puis surcharge avec les valeurs du graphe
 * @private
 */
  _initParametres () {
    const nomFn = `Section${this.nomSection}`
    // @ts-ignore
    if (!Array.isArray(this?.[nomFn]?.parametres)) return console.error(Error(`Les paramètres de la section ${this.nomSection} n’ont pas été initialisés par le loader`))
    const graphParams = this._getCurrentParametres()
    // @ts-ignore
    this.logIfDebug('surcharge des params', this[nomFn].parametres, ' avec ceux du graphe', graphParams)
    // on converti ça d’office tellement c’est fréquent
    // @ts-ignore incohérence de type de l’appelant que l’on corrige ici
    if (graphParams.limite === '') graphParams.limite = 0
    // on applique un upgrade si la section en exporte un (c’est j3pLoad qui l’a mis là)
    // @ts-ignore
    if (typeof this[nomFn].upgradeParametres === 'function') this[nomFn].upgradeParametres.call(this, graphParams)
    // on boucle sur les params exportés par la section
    // @ts-ignore
    for (let [name, defaultValue, type, help, options] of this[nomFn].parametres) {
      if (!help || typeof help !== 'string') console.error(Error(`la section ${this.nomSection} n’exporte pas de texte d’aide pour le param ${name}`))
      // valeur passée par les paramètres du graphe (qui peut être undefined)
      let value = graphParams[name]
      switch (type) {
      // on vérifie la valeur par défaut exportée par la section (qui doit être cohérente avec le type déclaré)
      // et si la valeur passée par le graphe existe, son type (si incohérent on cast ou on ignore)
        case 'chaine':
        case 'string':
          if (typeof defaultValue !== 'string') {
            console.error(`La valeur par défaut du paramètre ${name} exporté par la section ${this.nomSection} n’est pas une string : ${typeof defaultValue}`)
            defaultValue = String(defaultValue)
          }
          if (value != null && typeof value !== 'string') {
            console.error(`Le paramètre ${name} fourni par le graphe n’est pas une string : ${typeof value} (section ${this.nomSection})`)
            value = String(value)
          }
          break
        case 'entier':
        case 'integer':
          if (!Number.isInteger(defaultValue)) {
            console.error(`La valeur par défaut du paramètre ${name} exporté par la section ${this.nomSection} n’est pas un entier : ${defaultValue}`)
            defaultValue = Number(defaultValue)
            if (!Number.isInteger(defaultValue)) {
              console.error(`Impossible de convertir la valeur par défaut du paramètre ${name} en entier => 0`)
              defaultValue = 0
            }
          }
          if (value != null && !Number.isInteger(value)) {
          // ça arrive tellement souvent qu’on zappe
            if (name === 'limite' && value === '') {
              console.warn('Le graphe nous donne une chaîne vide pour limite, ce n’est pas un entier ! => 0')
              value = 0
            } else {
              if (typeof value === 'string') {
                console.warn(`Le paramètre ${name} fourni par le graphe est une string et pas un entier : ${value} (section ${this.nomSection})`)
                value = Number(value)
              } else {
                console.error(`Le paramètre ${name} fourni par le graphe n’est pas un entier : ${value} (section ${this.nomSection})`)
                value = undefined
              }
            }
            if (!Number.isInteger(value)) {
              console.error(`Impossible de convertir le paramètre ${name} fourni par le graphe en entier`)
              value = undefined
            }
          }
          break
        case 'reel':
        case 'number':
          if (typeof defaultValue !== 'number') {
            console.error(`La valeur par défaut du paramètre ${name} exporté par la section ${this.nomSection} n’est pas un number : ${typeof defaultValue}`)
            defaultValue = Number(defaultValue)
            if (typeof defaultValue !== 'number') {
              console.error(`Impossible de convertir en number la valeur par défaut du paramètre ${name} exporté par la section ${this.nomSection} => 0`)
              defaultValue = 0
            }
          }
          if (value != null && !Number.isFinite(value)) {
            if (typeof value === 'string') {
              console.warn(`Le paramètre ${name} fourni par le graphe est une string et pas un nombre : ${value} (section ${this.nomSection})`)
              value = Number(value)
              if (!Number.isFinite(value)) console.error(`Paramètre ${name} impossible à convertir en nombre`)
            } else {
              console.error(`Le paramètre ${name} fourni par le graphe n’est pas un nombre : ${value} (section ${this.nomSection})`)
            }
            if (!Number.isFinite(value)) value = undefined
          }
          break
        case 'boolean':
          if (typeof defaultValue !== 'boolean') {
            console.error(`La valeur par défaut du paramètre ${name} exporté par la section ${this.nomSection} n’est pas un booléen : ${typeof defaultValue}`)
            defaultValue = defaultValue !== 'false' && Boolean(defaultValue)
          }
          if (value != null && typeof value !== 'boolean') {
            console.error(`Le paramètre ${name} fourni par le graphe n’est pas un booléen : ${typeof value} (section ${this.nomSection})`)
            if (value === 'true') value = true
            else if (value === 'false') value = false
            else value = undefined
          }
          break
        case 'intervalle': {
        // forcément intervalle d’entier, éventuellement négatifs
          const re = /^\[-?[0-9]+;-?[0-9]+]$/
          // mais parfois y’a des espaces dans ce qu’on nous file, on les vire
          defaultValue = defaultValue.replace(/\s/g, '')
          if (!re.test(defaultValue)) console.error(`La valeur par défaut du paramètre ${name} exporté par la section ${this.nomSection} n’est pas un intervalle valide : ${defaultValue}`)
          if (typeof value === 'string') {
            value = value.replace(/\s/g, '')
            if (value && !re.test(value)) {
              console.error(`Le paramètre ${name} fourni par le graphe n’est pas un intervalle valide : ${value} (section ${this.nomSection})`)
              value = undefined
            }
          }
          break
        }
        case 'liste':
        // une seule valeur choisie dans la liste prédéfinie passée en option
          if (Array.isArray(options)) {
            if (!options.includes(defaultValue)) console.error(`La valeur par défaut du paramètre ${name} (${defaultValue}) n’est pas dans la liste imposée : [${options.join(', ')}] (section ${this.nomSection})`)
            if (value != null && !options.includes(value)) {
              console.error(`Le paramètre ${name} fourni par le graphe (${value}) n’est pas dans la liste imposée : [${options.join(', ')}] (section ${this.nomSection})`)
              value = undefined
            }
          } else {
            console.error(`Le paramètre ${name} de type liste exporté par la section ${this.nomSection} ne fournit pas de liste (en index 4) !`)
          }
          break
        case 'array':
        case 'tableau':
          if (!Array.isArray(defaultValue)) console.error(`La valeur par défaut du paramètre ${name} (${defaultValue}) exporté par la section ${this.nomSection} n’est pas un array`)
          if (value != null) {
            if (typeof value === 'string') {
              try {
                value = JSON.parse(value)
              } catch (error) {
                console.error(`json invalide : ${value}`, error)
              }
            }
            if (!Array.isArray(value)) {
              console.error(`Le paramètre ${name} fourni par le graphe (${value}) n’est pas un array (section ${this.nomSection})`)
              value = undefined
            }
          }
          break
        case 'editor':
        case 'multiEditor':
          if (typeof options !== 'function') console.error(`Le paramètre ${name} de la section ${this.nomSection} est de type ${type} mais elle ne fournit pas la fonction d’édition (en index 4)`)
          break
        default:
          console.error(Error(`type de paramètre ${type} inconnu (param ${name}, section ${this.nomSection})`))
      }
      this.donneesSection[name] = value ?? defaultValue
    }
    // on ajoute les phrases d’état possibles pour les sections qui exportent ça (attention, l’ancien éditeur de graphe ne les retrouve pas)
    // @ts-ignore
    if (typeof this?.[nomFn]?.phrasesEtat === 'object') {
      // @ts-ignore
      for (const [prop, value] of Object.entries(this[nomFn].phrasesEtat)) {
        this.donneesSection[prop] = value
      }
    } else {
      this.notif(`pas d’objet phrasesEtat pour ${nomFn}`)
    }
    checkDonneesSection(this)
  } // _initParametres
}

/**
 * Un nœud d’un graphe
 * @typedef Node
 * @type Array
 * @property {string} 0 id du node
 * @property {string} 1 section (ou 'fin')
 * @property {NodeOpts} [2] Les branchements et paramétrage éventuel
 */

/**
 * Le 3e élément d’un nœud, liste de branchements, dont le dernier peut être un paramétrage
 * @typedef NodeOpts
 * @type {Array<Branchement|NodeParams>}
 */

/**
 * Un branchement
 * @typedef Branchement
 * @type Object
 * @property {string} nn id du node destination
 * @property {string} [snn] id du node destination si la condition n’est pas remplie
 * @property {string} [pe]
 * @property {string} [score]
 * @property {string} [conclusion]
 */

/**
 * Paramètres d’un nœud, toutes les valeurs sont soit des string soit des numbers
 * @typedef NodeParams
 * @type Object
 */

export default Parcours
