import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pAddElt, j3pElement } from 'src/legacy/core/functions'
import { afficheSystemeEquations } from 'src/lib/widgets/systemeEquations'
import { colorCorrection, colorOk, colorKo } from 'src/legacy/core/StylesJ3p'
import { getRgbaFromCssColor } from 'src/lib/utils/css'

// pour normaliser une couleur afin de les comparer entre elles
const serializeColor = (color) => {
  try {
    const values = getRgbaFromCssColor(color)
    return values.join('-')
  } catch (error) {
    console.error(error)
  }
}

const correctionSerialized = serializeColor(colorCorrection)
const okSerialized = serializeColor(colorOk)
const koSerialized = serializeColor(colorKo)
const blackSerialized = serializeColor('#000000')

/**
 * Affiche un système d’équation, avec accolade à gauche qui se dimensionne automatiquement
 * S’il n’y a qu’un système, alors, il est préférable d’utiliser afficheSystemeEquations directement (en lui donnant un span comme conteneur si on veut le système inline)
 * Cette fonction sert à intégrer un système dans une phrase voire dans mettre plusieurs dans une même phrase
 * @param {HTMLElement|string} conteneur le conteneur d
 * @param id ignoré
 * @param {string} chaine Une chaîne avec délimiteur ‡, par ex 'foo ‡1‡ bar ‡2‡ baz' va insérer deux systèmes, dont les équations seront dans equations
 * @param {Array<Array<string>>} datas4equations un tableau dont le premier élément concerne ‡1‡, qui est lui-même un tableau de string qui sera passé à afficheSystemeEquations dans equations.contenu
 * @param {Object} objAffiche sera passé à j3pAffiche sur les parties en dehors des ‡ et à afficheSystemeEquations pour les parties entre ‡
 * @param {string|boolean} [couleurAcc] Passer une couleur pour l’accolade, seule les couleurs ok/ko/correction sont gérées (mettre la classe css correction|cbien|cfaux sur un des parents reviendrait au même)
 * @returns {ResultatAffichage[]} Le tableau des éléments générés par les j3pAffiche de afficheSystemeEquations (ne contient pas les éléments générés par les parties hors ‡)
 */
export function j3pAfficheSysteme (conteneur, id, chaine, datas4equations, objAffiche, couleurAcc) {
  // examine expr et créé les éléments (ajouté à elts), en récursif
  function parse (expr, elts) {
    const chunks = /^([^‡]*)‡([^‡]*)‡(.*)$/.exec(expr)
    if (chunks) {
      addTexte(chunks[1])
      elts = elts.concat(addSysteme(chunks[2]))
      if (chunks[3]) return parse(chunks[3], elts)
    } else if (expr) {
      addTexte(expr)
    }
    return elts
  }
  function addTexte (texte) {
    if (!texte) return
    const spanTexte = j3pAddElt(conteneur, 'span')
    j3pAffiche(spanTexte, '', texte, objAffiche, { marginTop: '10px' })
  }
  function addSysteme (numero, elts) {
    const index = Number(numero) - 1 // pour ‡1‡ faut aller chercher datas4equations[0]
    const datas = datas4equations[index]
    // console.log('datas', datas)
    if (!datas) return console.error(Error(`Aucun index ${index} trouvé dans le paramètre datas4equations (pour ‡${numero}‡)`))
    if (!Array.isArray(datas)) return console.error(Error(`Données datas4equations[${index}] invalides (pas un tableau, c’était les paramètres pour ‡${numero}‡)`))
    const spanSysteme = j3pAddElt(conteneur, 'span')
    const equations = datas.map(eq => ({ contenu: eq, options: objAffiche }))
    // console.log('equations', equations)
    return afficheSystemeEquations(spanSysteme, equations)
  }

  // traitement des paramètres
  if (typeof conteneur === 'string') conteneur = j3pElement(conteneur)
  // la couleur optionnelle
  if (couleurAcc) {
    const serializedColor = serializeColor(couleurAcc)
    let className = ''
    switch (serializedColor) {
      case blackSerialized: break
      case correctionSerialized: className = 'correction'; break
      case okSerialized: className = 'cbien'; break
      case koSerialized: className = 'cfaux'; break
      default:
        if (serializedColor !== blackSerialized) console.warn(Error(`j3pAfficheSysteme ne gère que les couleurs cbien|cfaux|correction (mettre cette classe css sur un parent revient au même que passer la couleur ici), ${couleurAcc} sera ignorée`))
        // sinon c'était une couleur invalide et c’est déjà indiqué en console
    }
    if (className) conteneur.classList.add(className)
  }

  // plus qu'à lancer le parsing
  return parse(chaine, [])
}
