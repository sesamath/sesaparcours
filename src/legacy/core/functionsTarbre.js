// j3pEvalueExpression({exp:"AB/CD",inc:["AB","CD"]},[35,14])
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { j3pMathquillXcas } from 'src/legacy/core/functions'

export function j3pEvalueExpression (expression, valeurs) {
  // voir outils/calculatrice/Tarbre.js
  const unarbre = new Tarbre(expression.exp, expression.inc)
  return unarbre.evalue(valeurs)
}

// j3pEvalue("-2*(4/3)");
export function j3pEvalue (expression) {
  // voir outils/calculatrice/Tarbre.js
  const unarbre = new Tarbre(expression, [])
  return unarbre.evalue([])
}

export function j3pTableauValeurs (expression, min, max, nbvaleurs) {
  const unarbre = new Tarbre(expression, ['x'])
  let unresultat
  const tabval = []
  let valeurx = min
  const pas = (max - min) / (nbvaleurs - 1)

  for (let k = 1; k <= nbvaleurs; k++) {
    valeurx = min + (k - 1) * pas
    unresultat = unarbre.evalue([valeurx])
    tabval[k - 1] = [valeurx, unresultat]
  }

  return tabval
}

export function j3pCalculValeur (chaine) {
  // un nombre écrit sous forme d’une chaine de caractères va être calculé pour qu’on ait sa valeur décimale
  function ecriturePourCalcul (texteFonction) {
    // texte fonction est au format latex ou tout autre format intermédiaire
    // on transforme cette écriture pour avoir l’expression sous la forme permettant de créer un arbre et d’appliquer la fonction evalue
    // je convertis pour que ce soit plus facile à utiliser
    let newExpressionFct = j3pMathquillXcas(texteFonction)
    // gestion de la racine carrée
    // il faut que je transforme sqrt en racine
    while (newExpressionFct.indexOf('sqrt') > -1) {
      newExpressionFct = newExpressionFct.replace('sqrt', 'racine')
    }
    // mais j’ai encore un problème avec l’exponentielle : je peux avoir e isolé, ou bien e^x ou bien e^{...}. Ceschoses ne sont pas comprises, il faut modifier ça et écrire sous la forme exp(...)
    newExpressionFct = transformeExp(newExpressionFct)
    // on peut encore avoir un soucis si l’élève écrit exp(), cad s’il ne met rien en puissance. On va le considérer comme exp(1)
    while (newExpressionFct.includes('exp()')) {
      newExpressionFct = newExpressionFct.replace('exp()', 'exp(1)')
    }
    return newExpressionFct
  }

  function transformeExp (fDeX) {
    // fDeX est une chaine de caractère correspondant à l’espression d’une fonction : f(x)
    // elle revoie la même chaine en remplaçant e^... par exp(...)
    // on appelle cette fonction après avoir utilisé j3pMathquillXcas, donc il n’y a plus frac{..}{..} mais (..)/(..)
    // Attention car e n’est pas seulement pour exp, il peut aussi l'être pour racine
    let newFdeX = ''
    let finFdeX = fDeX
    while (finFdeX.indexOf('ne') > -1) {
      newFdeX += finFdeX.substring(0, finFdeX.indexOf('ne') + 2)
      finFdeX = finFdeX.substring(finFdeX.indexOf('ne') + 2)
    }
    let posE = finFdeX.indexOf('e')
    while (posE > -1) {
      // e est présent et ce n’est pas celui de racine
      newFdeX += finFdeX.substring(0, posE)
      // on regarde ce qui suit e
      if ((finFdeX.charAt(posE + 1) === 'x') && (finFdeX.charAt(posE + 2) === 'p')) {
        // il est déjà écrit exp, donc on passe
        newFdeX += 'exp'
        finFdeX = finFdeX.substring(posE + 3)
      } else if ((finFdeX.charAt(posE + 1) === '+') || (finFdeX.charAt(posE + 1) === '-') || (finFdeX.charAt(posE + 1) === '*') || (finFdeX.charAt(posE + 1) === ')') || (finFdeX.charAt(posE + 1) === 'x')) {
        // e est tout seul, je le remplace par exp(1)
        newFdeX += 'exp(1)'
        finFdeX = finFdeX.substring(posE + 1)
      } else {
        // ce qui suit e est ^. Il faut alors trouver les parenthèses qui encadrent ce dont on prend l’exponentielle
        if (finFdeX.charAt(posE + 2) === '(') {
          // c’est de la forme e^(...)
          let parOuverte = 1
          let posParFerme = posE + 3
          while (parOuverte > 0) {
            if (finFdeX.charAt(posParFerme) === '(') {
              parOuverte++
            } else if (finFdeX.charAt(posParFerme) === ')') {
              parOuverte--
            }
            posParFerme++
          }
          newFdeX += 'exp(' + finFdeX.substring(posE + 3, posParFerme)
          finFdeX = finFdeX.substring(posParFerme)
        } else {
          // c’est juste de la forme e^., c.-à-d. que seul le caractère suivant est en puissance
          newFdeX += 'exp(' + finFdeX.substring(posE + 2, posE + 3) + ')'
          finFdeX = finFdeX.substring(posE + 3)
        }
      }
      posE = finFdeX.indexOf('e')
    }
    newFdeX += finFdeX
    // J’ai encore un souci car il ne reconnait pas 3e comme un produit, donc il faut le lui expliquer...
    newFdeX = newFdeX.replace(/(\d)e/g, '$1*e')
    return newFdeX
  }

  const nbAccOuvrante = String(chaine).split('{').length - 1
  const nbAccFermante = String(chaine).split('}').length - 1
  let newChaine
  let valeurChaine
  if (nbAccOuvrante === nbAccFermante) {
    // on a autant d’accolades ouvrantes que fermantes
    newChaine = ecriturePourCalcul(chaine)
    const arbreChaine = new Tarbre(newChaine, [])
    valeurChaine = arbreChaine.evalue([])
  } else {
    valeurChaine = chaine
  }
  return valeurChaine
}
