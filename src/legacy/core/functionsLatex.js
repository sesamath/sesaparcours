import { j3pMeilleurArrondi, j3pMonome, j3pNombre, j3pPGCD, j3pShowError, j3pSimplifieQuotient, j3pVirgule } from 'src/legacy/core/functions'

/**
 * Fonctions qui servent à effectuer des calculs sur les nombres écrits au format latex, éventuellement des nombres fractionnaires
 * @module legacy/core/functions.latex
 * @private
 */

const epsilon = 1e-10

/**
 * @typedef FractionArray
 * @type {Array}
 * @property {boolean} 0 true si c’est un nombre fractionnaire
 * @property {string} 1 numérateur
 * @property {string} 2 dénominateur
 * @property {string} 3 Le signe '+' ou '-'
 */

/**
 * Transforme une string \\frac{}{} en tableau [isFractionnaire, num, den, signe]
 * Attention, ne fonctionne que si numérateur et dénominateur sont des entiers ou des décimaux
 * (pas des expressions latex contenant des racines ou autres fractions, si c’est le cas le numérateur retourné est nb tel quel et le dénominateur sera 1 avec un signe vide)
 * Attention, ne modifie pas le séparateur décimal (point ou virgule)
 * @param {string|number} nb
 * @return {FractionArray} La fraction décomposée en strings [isFrac, num, den, signe]
 */
export function j3pExtraireNumDen (nb) {
  if (typeof nb === 'number') {
    if (!Number.isFinite(nb)) throw Error('Il faut passer un nombre fini')
    nb = String(nb)
  }
  if (typeof nb !== 'string') throw Error('type invalide (il faut string ou number) ' + typeof nb)
  nb = nb.replace(/\s/g, '').replace(/^\++/, '') // pour enlever un éventuel signe + devant le nombre (y compris si j’en ai plusieurs successifs)
  if (!nb) throw Error('Nombre invalide (chaîne vide)')
  const chunks = /^(-*)\\frac{(-?[\d,.]+)}{(-?[\d,.]+)}$/.exec(nb)
  if (chunks) {
    // c’est une fraction
    let [, signe, num, den] = chunks
    // on gère plusieurs signes - au début
    if (signe.length > 1) signe = signe.length % 2 ? '-' : ''
    // on met le - précédent la fraction sur le numérateur (sans simplifier si num et den sont négatifs)
    if (signe) {
      if (/^-/.test(num)) num = num.substr(1)
      else num = signe + num
    }
    signe = (j3pNombre(num) * j3pNombre(den) < 0) ? '-' : '+'
    return [true, num, den, signe]
  }

  // pas une fraction, on vire d’éventuels '-' multiples au début
  while (/^--/.test(nb)) nb = nb.replace(/^--/, '')
  const n = j3pNombre(nb)
  const signe = Number.isNaN(n) // ça arrive si on file autre chose qu’une fraction de nombres ou un nombre
    ? ''
    : n < 0
      ? '-'
      : '+'
  if (!signe) console.error(Error(`j3pExtraireNumDen appelé avec autre chose qu’un nombre ou fraction de nombres, ça ne décompose rien (retourne un dénominateur 1 avec en numérateur le nb d’origine : ${nb}`))
  return [false, nb, '1', signe]
}

/**
 * Retourne le nombre en latex, sous forme de fraction ou d’entier
 * @param {number} num numérateur (pas de \frac, utiliser getLatexQuotient pour ça)
 * @param {number} den dénominateur (pas de \frac, utiliser getLatexQuotient pour ça)
 * @return {string}
 */
export function j3pGetLatexFrac (num, den) {
  if (!Number.isFinite(num) || !Number.isFinite(den)) throw Error(`Paramètres invalides (pas des number, ${num} et ${den})`)
  const q = j3pSimplifieQuotient(num, den)
  // num & den sont entiers, inutile de passer par j3pVirgule
  if (q.num === 0) return '0'
  if (q.den === 1) return String(q.num)
  return '\\frac{' + q.num + '}{' + q.den + '}'
}

/**
 * Retourne le monome demandé sous la forme d’une string latex
 * @param rang position du monome dans l’écriture
 * @param {string|number} puiss puissance de nomVar
 * @param {string|number} nb coef écrit sous forme latex (\\frac{...}{...} pour des nb fractionnaires)
 * @param {string} [nomVar=x]
 * @return {string}
 */
export function j3pGetLatexMonome (rang, puiss, nb, nomVar = 'x') {
  if (typeof rang === 'string') rang = Number(rang)
  if (typeof rang !== 'number') {
    console.error(Error('rang doit être de type number'))
    rang = Number(rang)
  }
  if (typeof puiss === 'string') puiss = Number(puiss)
  if (typeof puiss !== 'number') {
    console.error(Error('puissance doit être de type number'))
    puiss = Number(puiss)
  }
  const [isFrac, num, den, signe] = j3pExtraireNumDen(nb)
  if (isFrac) { // c’est un nb fractionnaire'
    const txtVar = (puiss === 0)
      ? ''
      : (puiss === 1)
          ? nomVar
          : (String(puiss).length > 1)
              ? nomVar + '^{' + puiss + '}' // je mets des accolades autour de la puissance seulement si le nombre de caractères est supérieur à 1
              : nomVar + '^' + puiss
    if (signe === '-') {
      // nb est négatif, dans ce cas, il faut que je retire le signe du numérateur de la fraction pour le mettre devant \frac
      return '-\\frac{' + num.substring(1) + '}{' + den + '}' + txtVar
    }
    // sinon on retourne la fraction fournie,
    // je n’ai pas mis nb pour le cas où nb aurait été de la forme +\\frac{1}{3} (ce signe + ne doit pas être conservé
    return (rang === 1 ? '' : '+') + '\\frac{' + num + '}{' + den + '}' + txtVar
  }
  // c’est pas fractionnaire, c’est la fct j3pMonome habituelle
  return j3pMonome(rang, puiss, nb, nomVar)
}

/**
 * Retourne l’opposé au  format latex
 * @param {string|number} nb
 * @return {string}
 */
export function j3pGetLatexOppose (nb) {
  return j3pGetLatexProduit('-1', nb)
}

/**
 * Retourne le produit des nombres au format latex
 * @param {string|number} nb1 entier, décimal ou fraction d’entiers|décimaux
 * @param {string|number} nb2 entier, décimal ou fraction d’entiers|décimaux
 * @return {string}
 */
export function j3pGetLatexProduit (nb1, nb2) {
  const [, num1, den1] = j3pExtraireNumDen(nb1)
  const [, num2, den2] = j3pExtraireNumDen(nb2)
  return j3pGetLatexFrac(j3pNombre(num1) * j3pNombre(num2), j3pNombre(den1) * j3pNombre(den2))
}

/**
 * Retourne nb^puissance au format latex (sans ^ dedans, calcul effectué et simplifications éventuelles appliquées)
 * @param {string|number} nb nombre entier, décimal ou fractionnaire au format latex
 * @param {string|number} puissance entier relatif
 * @return {string}
 */
export function j3pGetLatexPuissance (nb, puissance) {
  // check nb
  if (typeof nb === 'number') {
    if (!Number.isFinite(nb)) throw Error(`Nombre invalide ${nb}`)
    nb = String(nb)
  }
  if (typeof nb !== 'string') throw Error(`Nombre invalide ${nb}`)
  // check puissance
  if (typeof puissance === 'string') puissance = Number(puissance)
  if (!Number.isFinite(puissance)) throw Error(`puissance invalide ${puissance}`)
  // on s’assure que c’est un entier
  if (!Number.isInteger(puissance)) {
    // on râle, sauf si c'était presque un entier
    if (Math.abs(puissance - Math.round(puissance)) > epsilon) console.error(RangeError(`Puissance non entière ${puissance}`))
    puissance = Math.round(puissance)
  }
  // ok, nb est une string et puissance un number entier
  if (puissance === 0) return '1'
  if (puissance === 1) return nb
  if (puissance < 0) return j3pGetLatexQuotient('1', j3pGetLatexPuissance(nb, -puissance))
  return j3pGetLatexProduit(nb, j3pGetLatexPuissance(nb, puissance - 1))
}

/**
 * Retourne le quotient sous la forme latex la plus simple possible
 * @param {string|number} num
 * @param {string|number} den
 * @return {string}
 */
export function j3pGetLatexQuotient (num, den) {
  const [, numNum, denNum] = j3pExtraireNumDen(num)
  const [, numDen, denDen] = j3pExtraireNumDen(den)
  return j3pGetLatexFrac(j3pNombre(numNum) * j3pNombre(denDen), j3pNombre(denNum) * j3pNombre(numDen))
}

/**
 * Retourne la string latex de la racine simplifiée au maximum, un entier ou "a\sqrt{b}" avec a & b entiers
 * @param entier
 * @private
 * @return {string}
 */
function racineLatexEntier (entier) {
  if (typeof entier === 'string') entier = Number(entier)
  if (!Number.isInteger(entier)) throw Error(`Il faut fournir un entier (${entier})`)
  if (entier < 0) throw Error(`Il faut fournir un entier positif (${entier})`)
  const sqrt = Math.sqrt(entier)
  // si la racine est entière on la retourne
  if (Number.isInteger(sqrt)) return String(sqrt)
  // quasi entière ?
  if (Math.abs(sqrt - Math.round(sqrt)) < epsilon) return String(Math.round(sqrt))

  // pas un entier
  let radical = entier
  let nbAvantRadical = 1
  let a = Math.floor(sqrt)
  while (a > 1) {
    if (radical % Math.pow(a, 2) === 0) {
      radical = Math.round(radical / Math.pow(a, 2))
      nbAvantRadical = Math.round(nbAvantRadical * a)
    }
    a--
  }
  const prefix = nbAvantRadical === 1 ? '' : String(nbAvantRadical)
  return prefix + '\\sqrt{' + String(radical) + '}'
}

/**
 * Retourne la string latex de la racine simplifiée de nb sous la forme n ou a\sqrt{b} ou \frac{\sqrt{n}}{d} ou \frac{n}{d}\sqrt{a}
 * @param nb
 * @return {string}
 * @throws {Error} si nb n’est pas un entier positif ou une fraction d’entiers positifs
 */
export function j3pGetLatexRacine (nb) {
  const [isFrac, n, d] = j3pExtraireNumDen(nb)
  if (isFrac) {
    // c’est un nb fractionnaire \\frac{n}{d}
    // On fait alors en sorte d’avoir un résultat sans radical au dénominateur
    const sqrtDen = racineLatexEntier(d)
    if (sqrtDen.includes('sqrt')) {
      // la racine carrée du dénominateur n’est pas un entier, on multiplie n & d par d
      const sqrtNum = racineLatexEntier(j3pNombre(n) * j3pNombre(d))
      const indexSqrtNum = sqrtNum.indexOf('\\sqrt')
      if (indexSqrtNum > 0) {
        // on a un nombre devant le radical
        const nbDevantRadicalNum = sqrtNum.substring(0, sqrtNum.indexOf('\\sqrt'))
        // on a un nombre de la forme a*sqrt{b}/d
        // il faut encore voir si on peut simplifier a/d
        const { num, den } = j3pSimplifieQuotient(nbDevantRadicalNum, d)
        return '\\frac{' + num + '}{' + den + '}' + sqrtNum.substr(indexSqrtNum)
      }
      // cas sqrt{b}/d
      if (sqrtNum.includes('sqrt')) return '\\frac{' + sqrtNum + '}{' + d + '}'
      return j3pGetLatexFrac(j3pNombre(sqrtNum), j3pNombre(d)) // il reste peut-être une simplification à faire
    }
    // sqrtDen entier
    const num = racineLatexEntier(n)
    if (num.includes('sqrt')) return '\\frac{' + num + '}{' + sqrtDen + '}'
    return j3pGetLatexFrac(j3pNombre(num), j3pNombre(sqrtDen)) // il reste peut-être une simplification à faire
  }
  return racineLatexEntier(n)
}

/**
 * Ajoute nb1 et nb2 et retourne le résultat en latex (fraction simplifiée si c’est une fraction)
 * @param {string|number} nb1 entier, décimal ou fraction de nombres en latex
 * @param {string|number} nb2 entier, décimal ou fraction de nombres en latex
 * @return {string} le nombre (pas forcément fractionnaire) en latex
 */
export function j3pGetLatexSomme (nb1, nb2) {
  const [isFrac1, num1, den1] = j3pExtraireNumDen(nb1)
  const [isFrac2, num2, den2] = j3pExtraireNumDen(nb2)
  // les mêmes en number
  const [n1, d1, n2, d2] = [num1, den1, num2, den2].map(j3pNombre)
  // si y’a pas de fraction c’est simple
  if (!isFrac1 && !isFrac2) return j3pVirgule(j3pMeilleurArrondi(n1 + n2))
  // sinon on laisse le taf à j3pGetLatexFrac
  return j3pGetLatexFrac(j3pMeilleurArrondi(n1 * d2 + d1 * n2), j3pMeilleurArrondi(d1 * d2))
}

export function j3pSimplificationRacineTrinome (b, plusmoins, delta, a) {
  // b, delta et a sont les coef présents dans le calcul (-b-sqrt{delta})/(2*a)  ou (-b+sqrt{delta})/(2*a)
  /// /plusmoins vaut "+" ou "-"
  // cette fonction renvoie le résultat au format latex
  if (String(a)[0] === '-') {
    // a est négatif, j’ai un pb
    a = j3pGetLatexProduit(a, '-1')
    b = j3pGetLatexProduit(b, '-1')
    plusmoins = (plusmoins === '-') ? '+' : '-'
  }
  const deno = j3pGetLatexProduit('2', a)
  let nume, quotient // réponse renvoyée
  if (String(delta).indexOf('.') > -1) {
    // delta est trop grand
    j3pShowError('delta est un nombre trop grand')
  }
  const sqrtDelta = j3pGetLatexRacine(delta)
  if (!sqrtDelta.includes('sqrt')) {
    if (plusmoins === '+') {
      nume = j3pGetLatexSomme(j3pGetLatexProduit('-1', b), sqrtDelta)
    } else {
      nume = j3pGetLatexSomme(j3pGetLatexProduit('-1', b), j3pGetLatexProduit('-1', sqrtDelta))
    }
    quotient = j3pGetLatexQuotient(nume, j3pGetLatexProduit(a, '2'))
  } else {
    let coefDevantSqrt
    const sqrtNbReg = /\\sqrt{[0-9]+}/g
    const tabSqrtDelta = sqrtDelta.match(sqrtNbReg)
    let newSqrtDelta = sqrtDelta
    for (let i = 0; i < tabSqrtDelta.length; i++) {
      newSqrtDelta = newSqrtDelta.replace(tabSqrtDelta[i], '')
    }
    if (newSqrtDelta === '') {
      coefDevantSqrt = '1'
    } else if (newSqrtDelta === '-') {
      coefDevantSqrt = '-1'
    } else {
      if (newSqrtDelta.indexOf('frac{}') > -1) {
        newSqrtDelta = newSqrtDelta.replace('frac{}', 'frac{1}')
      }
      coefDevantSqrt = newSqrtDelta
    }
    const fracB = j3pGetLatexQuotient(j3pGetLatexProduit('-1', b), deno)// c’est -b/(2a) simplifié
    const fracSqrt = j3pGetLatexQuotient(coefDevantSqrt, deno)// dans sqrt{Delta}/(2a), c/(2a) où c est tel que sqrt{delta}=c*sqrt{b}
    let denFracB = 1
    let numFracB
    if (String(fracB).indexOf('\\frac') > -1) {
      denFracB = Number(fracB.substring(fracB.lastIndexOf('{') + 1, fracB.length - 1))
      numFracB = Number(fracB.substring(fracB.indexOf('{') + 1, fracB.indexOf('}')))
    } else {
      numFracB = Number(fracB)
    }
    let denFracSqrt = 1
    let numFracSqrt
    if (String(fracSqrt).indexOf('\\frac') > -1) {
      denFracSqrt = Number(fracSqrt.substring(fracSqrt.lastIndexOf('{') + 1, fracSqrt.length - 1))
      numFracSqrt = Number(fracSqrt.substring(fracSqrt.indexOf('{') + 1, fracSqrt.indexOf('}')))
    } else {
      numFracSqrt = Number(fracSqrt)
    }
    const pgcdDen = j3pPGCD(denFracB, denFracSqrt)
    const denCommun = denFracB * denFracSqrt / pgcdDen
    const newNumFracB = numFracB * denCommun / denFracB
    const newNumFracSqrt = numFracSqrt * denCommun / denFracSqrt
    // pour la suite, j’ai besoin d’enlever le dénominateur présent dans sqrtDelta
    if (plusmoins === '+') {
      if (String(newNumFracSqrt).charAt(0) === '-') {
        if (String(newNumFracSqrt) === '-1') {
          if (newNumFracB === 0) {
            quotient = '-' + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          } else {
            quotient = String(newNumFracB) + '-' + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          }
        } else {
          if (newNumFracB === 0) {
            quotient = String(newNumFracSqrt) + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          } else {
            quotient = String(newNumFracB) + String(newNumFracSqrt) + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          }
        }
      } else {
        if (String(newNumFracSqrt) === '1') {
          if (newNumFracB === 0) {
            quotient = sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          } else {
            quotient = String(newNumFracB) + '+' + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          }
        } else {
          if (newNumFracB === 0) {
            quotient = String(newNumFracSqrt) + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          } else {
            quotient = String(newNumFracB) + '+' + String(newNumFracSqrt) + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          }
        }
      }
    } else {
      if (newNumFracSqrt < 0) {
        if (newNumFracSqrt === -1) {
          if (newNumFracB === 0) {
            quotient = sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          } else {
            quotient = String(newNumFracB) + '+' + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          }
        } else {
          if (newNumFracB === 0) {
            quotient = String(newNumFracB) + '+' + String(newNumFracSqrt).substring(1) + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          } else {
            quotient = String(newNumFracSqrt).substring(1) + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          }
        }
      } else {
        if (newNumFracSqrt === 1) {
          if (newNumFracB === 0) {
            quotient = '-' + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          } else {
            quotient = String(newNumFracB) + '-' + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          }
        } else {
          if (newNumFracB === 0) {
            quotient = '-' + String(newNumFracSqrt) + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          } else {
            quotient = String(newNumFracB) + '-' + String(newNumFracSqrt) + sqrtDelta.substring(sqrtDelta.indexOf('\\sqrt'), sqrtDelta.indexOf('}') + 1)
          }
        }
      }
    }
    if (denCommun > 1) {
      // la réponse simplifiée s'écrit sous la forme (a+b\sqrt{c})/d
      // quotient déterminé précédemment correspond juste à a+b\sqrt{c}
      quotient = '\\frac{' + quotient + '}{' + String(denCommun) + '}'
    }
  }
  return quotient
}
