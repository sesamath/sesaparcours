import $ from 'jquery'

/**
 * Ajoute la méthode textWidth à toutes les instances jQuery `$(selector)`
 * @fileOverview
 */

/**
 * Permet de déterminer la taille d’une zone de texte saisie au clavier
 * @param {string} text Le texte dont on cherche la taille
 * @param {string} fontFamily La police utilisée pour le rendu
 * @param {string} fontSize La taille (par ex 12px)
 * @return {number} Le nb de pixels
 */
$.fn.textWidth = function textWidth (text, fontFamily, fontSize) {
  if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span></span>').hide().appendTo(document.body)
  $.fn.textWidth.fakeEl.text(text)// || this.val() || this.text()|| this.css('font'));
  $.fn.textWidth.fakeEl.css('font-size', fontSize)
  $.fn.textWidth.fakeEl.css('font-family', fontFamily)
  // on cherche le nombre d’espaces présents car il faut effectuer un correctif dans ce cas
  let nbEspace = 0 // on compte 5 px pour chaque espace, mais ça doit dépendre de la police et sa taille…
  if (text.indexOf(' ') !== -1) {
    nbEspace = text.split(' ').length - 1
  }
  return $.fn.textWidth.fakeEl.width() + nbEspace * 5
}

// on exporte rien, on se contente d’ajouter qqchose au $ importé, comme les plugins
// D’où l’importance de l’alias jquery dans la config webpack|vite, pour que tous les
// `import $ from 'jquery'` retourne le même module js
