/** @module legacy/themes/index */

const loadingPromises = {}
/**
 * Charge la css de nos &lt;table> (seulement au 1er appel, ne fait rien ensuite)
 * @return {Promise<boolean>} Une promesse toujours résolue, mais avec false en cas de pb de chargement (true sinon)
 */
export function loadThemeCss (theme) {
  if (!loadingPromises[theme]) {
    let promise
    switch (theme) {
      case 'zonesAvecImageDeFond': promise = import('./zonesAvecImageDeFond/zonesAvecImageDeFond.scss'); break
      case 'standard': promise = import('./standard/standard.scss'); break
    }
    loadingPromises[theme] = promise
      .then(() => true)
      .catch(error => {
        console.error(error)
        return false
      })
  }
  return loadingPromises[theme]
}
