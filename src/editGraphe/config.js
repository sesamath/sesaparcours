import { addSesatheque } from 'sesatheque-client/src/sesatheques'

/** @module editGraphe/config */

// variables qui dépendent de là où on est
const isDev = /(\.local|sesamath\.dev)$/.test(window.location.hostname) || window.location.hostname === 'localhost'

// exports nommés
export const sourceTreeRid = isDev ? 'sesabidev/50045' : 'sesabibli/50045'
if (isDev) {
  addSesatheque('sesabidev', 'https://bibliotheque.sesamath.dev/')
}

export const typeBibliConnus = ['am', 'em', 'j3p', 'ecjs', 'iep']

// export par défaut qui regroupe toutes nos constantes dans un objet
export default { sourceTreeRid, typeBibliConnus }

/**
 * La liste des propriétés possibles pour un branchement
 * @type {string[]}
 */
export const brProps = ['nn', 'pe', 'score', 'conclusion', 'sconclusion', 'snn', 'max', 'maxParcours']
