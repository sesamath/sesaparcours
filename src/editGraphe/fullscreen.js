import { j3pShowError } from 'src/legacy/core/functions'
import dom from 'sesajstools/dom/index'
import { getRootElt } from './store'

/** @module editGraphe/fullscreen */
/**
 * Ajoute la fonction fullscreen au bouton si le navigateur le gère
 * @param {HTMLElement} fsButton
 */
function fullScreen (fsButton) {
  // au cas où on nous rappelle sur le même bouton
  if (fsButton['data-hasFullscreeBehavior']) return
  fsButton['data-hasFullscreeBehavior'] = true
  // pas de fullscreen par défaut
  const fullScreenApi = {
    supportsFullScreen: false,
    isFullScreen: function () { return false },
    requestFullScreen: function () {},
    cancelFullScreen: function () {},
    fullScreenEventName: '',
    prefix: ''
  }

  // check for native support
  if (typeof document.cancelFullScreen !== 'undefined') {
    fullScreenApi.supportsFullScreen = true
  } else {
    // check for fullscreen support by vendor prefix
    const browserPrefixes = ['webkit', 'moz', 'o', 'ms', 'khtml']
    for (let i = 0, il = browserPrefixes.length; i < il; i++) {
      fullScreenApi.prefix = browserPrefixes[i]
      if (typeof document[fullScreenApi.prefix + 'CancelFullScreen'] !== 'undefined') {
        fullScreenApi.supportsFullScreen = true
        break
      }
    }
  }

  if (fullScreenApi.supportsFullScreen) {
    fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange'

    fullScreenApi.isFullScreen = function () {
      switch (this.prefix) {
        case '':
          return document.fullScreen
        case 'webkit':
          return document.webkitIsFullScreen
        default:
          return document[this.prefix + 'FullScreen']
      }
    }
    fullScreenApi.requestFullScreen = function (el) {
      return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']()
    }
    fullScreenApi.cancelFullScreen = function (el) {
      return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']()
    }

    // le listener au clic
    const rootElement = getRootElt()
    fsButton.addEventListener(
      'click',
      function () {
        if (fullScreenApi.isFullScreen()) {
          dom.empty(fsButton)
          dom.addText(fsButton, 'Plein écran')
          fullScreenApi.cancelFullScreen(rootElement)
        } else {
          dom.empty(fsButton)
          dom.addText(fsButton, 'Quitter le plein écran')
          fullScreenApi.requestFullScreen(rootElement)
        }
      },
      true
    )
  } else {
    // pas de plein écran dispo, on ajoute quand même un listener sur le bouton pour expliquer pourquoi ça fait rien
    fsButton.addEventListener(
      'click',
      function () {
        j3pShowError('Votre navigateur ne supporte pas le plein écran')
      },
      true
    )
  }
}
export default fullScreen
