/**
 * Fonctions travaillant sur le tableau grapheBibli
 * @module editGraphe/grapheBibli
*/

let grapheBibli = []

/**
 * Ajoute un nœud au graphe
 * @param noeud
 */
export function addGraphebibli (noeud) {
  grapheBibli.push(noeud)
}

/**
 * Réinitialise le graphe (à vide)
 */
export function resetGraphebibli () {
  grapheBibli = []
}

/**
 * Ajoute une valeur à un node du graphe
 * @param {number} index Index du node
 * @param {number} sousIndex Index de la propriété dans le node
 * @param valeur la valeur à ajouter au tableau node[sousIndex]
 */
export function addPropertyGrapheBibli (index, sousIndex, valeur) {
  grapheBibli[index][sousIndex].push(valeur)
}

/**
 * Retourne la taille du graphe (nb de nodes)
 * @return {number}
 */
export function lenGrapheBibli () {
  return grapheBibli.length
}

/**
 * Retourne le node i
 * @param {number} i
 * @return {Node}
 */
export function extraitGrapheBibli (i) {
  return grapheBibli[i]
}

/**
 * Ajoute un node
 * @param {TabNode} tabNode
 * @return {number} L’index du node qu’on vient d’ajouter
 */
export function addGrapheBibli (tabNode) {
  grapheBibli.push(tabNode)
  return grapheBibli.length - 1
}

/**
 * Retourne l’index du nodeNumero (car dans le graphe c’est pas forcément dans l’ordre des numéros)
 * @param {number} nodeNum
 * @return {number} l’index ou undefined si on a pas trouvé nodeNum
 */
export function getIndexFromNumero (nodeNum) {
  const nodeId = String(nodeNum)
  for (const nodeIndex in grapheBibli) {
    if (grapheBibli[nodeIndex][0] === nodeId) return nodeIndex
  }
}

/**
 * Retourne le graphe
 * @return {Array}
 */
export function getGrapheBibli () {
  return grapheBibli
}
