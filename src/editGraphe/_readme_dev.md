Informations pour les développeurs
==================================

Ce dossier contient deux "applications" :
- editGraphe : permet l’édition d’un graphe (nodes et branchements)
- showParcours : pour afficher le parcours d’un élève, à partir d’un résultat

Le layout est expliqué dans src/css/layout.css (pour editgraphe, car pour showParcours c'est bcp plus simple il n’y a que la scene, pas de menu)

Concernant le format des objets manipulés, cf la doc en fin de src/store.js (tous les @typedef), car il faut distinguer
- le graphe au format "bibli", tel qu'il est stocké dans les ressources (TabGraphe), c'est un tableau de tableau difficile à appréhender car aucune des propriétés n’est nommée: il faut savoir que dans un noeud le 1er argument est son nodeId, un numéro mais sous forme de chaîne, le 2e est le nom de la section, et le 3e un tableau contenant les branchements et les paramètres par défaut de la section, toujours en dernier dans le tableau, mais branchements et paramètres sont facultatifs, on déduit que c'est un branchement si l’objet a une propriété nn (NumeroNode)
- le graphe à un format interne, un peu plus lisible (ObjetGraphe)

Lors du chargement, les nodes sont créés un par un, mais il faut ensuite ajouter tous les nœuds fin (qui ne figurent pas forcément dans le graphe de départ).

Il faut aussi créer les branchements alternatifs, car dans un branchement on peut préciser un maximum de passage et un branchement alternatif quand le max est atteint. On a 
- nn : le node de destination du branchement
- snn : le node de destination si y’a un max et qu'il est atteint
- conclusion : le texte à afficher au passage dans le branchement (avant d’aller vers nn)
- sconclusion : le texte à afficher au passage dans le branchement si le max est atteint (avant d’aller vers snn)

On a donc sur la scene des branchements "virtuels" pour visualiser les connexions "si le max est atteint", ils sont déduit du branchement d’origine (qui a la propriété snn) et ne sont donc pas éditable, ils ont la propriété isSnn à true.

Conventions
-----------

On essaie de suivre les conventions suivantes sur les noms de variable
- xxxIndex est un index de tableau, donc un number qui démarre à 0
- nodeNum est un number, le n° du node (y’a jamais de node 0)
- nodeId est le nodeNum en string, propriété de graphe.nodes
- nodeIndex est rarement utilisé, mais suppose un index de tableau, qui démarre donc à 0 (donc on peut supposer que nodeIndex = nodeNum - 1, mais vaut mieux ne pas trop supposer car rien n’oblige à ranger les nodes dans l’ordre de leur numéro dans tabGraphe)
- domXXXid est une string, id d’un élément html
- xxxYyy laisse supposer que yyy est un détail de xxx (une par ex une propriété), on essaiera donc de toujours écrire nodeId et jamais idNode 

Évolutions
==========

Trucs à faire (ou pas) pour la suite, en vrac
- renommer les variables pour bien distinguer 
  - branchement (qui peut avoir nn et snn) et connexion (l’un ou l’autre, 
    un branchement pouvant donner deux connexion si y’a du snn)
  - nodeNum (number) et nodeId (le même en string) et nodeDomId (idem préfixé avec 'node')
  - tout ce qui pourrait prêter à confusion ;-)
- Ajouter dans le store une méthode getFreeNodeId qui renverrait un nodeId libre, 
  pour éviter de continuer à gérer du store.maxNumeroNode (avec ses actions et ses reducers).
  Attention, on peut pas utiliser le nb de nodes car lorsqu'on en supprime on ne veut pas tout
  renuméroter (donc avec les nodes 1, 3, 4, on a 3 nodes mais le suivant sera 5).
- utiliser le même code pour l’import initial d’un graphe et les suivant (au drop de graphe)
- Ajouter les titres et les positions à chaque objet node, pour que toute l’info soit dans objetGraphe.
- Ajouter dans le store les propriétés qui manquent pour ne plus avoir besoin d’interroger le dom
  (par ex isConditionPe sur un branchement pour savoir si la condition porte sur le score ou la PE)
- Le point précédent devrait permettre de faire toujours 
  modif (via souris ou boite de dialogue) => action redux qui modifie le state => mettre à jour la vue si besoin 
  Ce sera alors peut-être utile de créer une fct addNode qui génère le nodeId et le renvoie (pour sortir de scene.js 
  le code qui gère l’évitement de collision sur le drop d’un node ou d’un graphe). idem pour addBranchement si besoin.
