import dom from 'sesajstools/dom/index'
import { importerGrapheAColorer, coloreParcours, importerGrapheFinSP } from './importGraphe'
import { init as sceneInit } from './scene'
import { actualiseGestionnaireEvenementsColoration } from './fonctionsNodes'
import { findOrCreateElement } from './fonctionsUtiles'

import './css/layout.css'

/** @module editGraphe/showParcours */

/**
 * Montre le parcours coloré de ce résultat, dans rootElt
 * @param {string|HTMLElement} rootElt
 * @param {LegacyResultat} result Ce que j3p à envoyé à la sesathèque (mis dans resultat.contenu)
 * et qu’elle stocke dans la propriété contenu d’un Resultat
 */
function showParcours (rootElt, result) {
  function termine (error) {
    if (error) {
      console.error(error)
      dom.empty(rootElt)
      dom.addElement(rootElt, 'p', { class: 'error' }, 'Une erreur est survenue')
    } else if (waitElt) {
      // c’est fini normalement, on vire notre message d’attente
      waitElt.parentNode.removeChild(waitElt)
    }
  }

  let waitElt
  try {
    // const result_copie = Object.assign({}, result)
    // correctif si tab vide en graphe[0], on dégage le premier elt
    if (result.graphe[0].length === 0) {
      result.graphe.shift()
    }
    // au cas où on passerait un id et pas un HTMLElement
    if (typeof rootElt === 'string') rootElt = document.getElementById(rootElt)
    console.info('showParcours du résultat', result)
    // notre css
    // dom.addCss(require('./css/layout.css'))
    // un message d’attente avec une variable dessus pour faire un remove à la fin
    waitElt = findOrCreateElement(rootElt, 'p', { className: 'edgWait' }, 'Chargement en cours…')
    // header
    // @todo mettre des id uniques car on peut être appelés plusieurs fois dans le même dom ! (faudra peut-être passer par des class pour les css)
    // dans labomep le position absolute du header casse tout, et le fullscreen marche pas
    // pour le fullscreen faudrait un setRootElt, mais en lui passant le rootElt qu’on a reçu
    // ça donne un résultat catastrophique, on s’abstient pour le moment
    // const headerElt = dom.addElement(rootElt, 'div', { id: 'edgHeader' }, 'Afficheur de parcours')
    // const fullScreenButton = dom.addElement(headerElt, 'button', { id: 'edgFullScreenButton' }, 'Plein écran')
    // fullscreen(fullScreenButton)
    const contentElt = findOrCreateElement(rootElt, 'div', { id: 'parcoursContent' })
    // footer
    // dom.addElement(rootElt, 'div', { id: 'edgFooter' }, 'Sésamath')
    // on commence à détailler, si on veut juste colorer un parcours donné, pas besoin de toute l’artillerie,
    // on charge jsPlumb et les trucs nécessaires
    // if (coloreParcours) {
    //   const sceneElt = dom.addElement(contentElt, 'div', { id: 'edgScene' })
    //   sceneInit(sceneElt, coloreParcours, function () {
    //     isInitDone = true
    //     // on charge ensuite le parcours et on colore
    //   }, graphe_a_colorer, parametres_graphe_a_colorer)
    // } else {
    // menu
    // scene
    const sceneElt = findOrCreateElement(contentElt, 'div', { id: 'edgScene' })
    sceneElt.style.width = '100%' // dans ce cas on corrige le css
    // ici on appelle nos autres modules en leur filant une callback à appeller quand ce sera fini, attention là c’est la cascade de callbacks...
    sceneInit(sceneElt, function (error) { // la callback
      if (error) return termine(error)
      try {
        importerGrapheAColorer(result)
        actualiseGestionnaireEvenementsColoration(result)
        importerGrapheFinSP(result.graphe)
        coloreParcours(result)
        termine()
      } catch (error) {
        termine(error)
      }
    })
  } catch (error) {
    termine(error)
  }
}

export default showParcours
