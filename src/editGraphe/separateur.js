import $ from 'jquery'

/** @module editGraphe/separateur */

/**
 * Ajoute un écouteur sur le séparateur pour modifier les tailles de menuElt et sceneElt
 * @param menuElt
 * @param sepElt
 * @param sceneElt
 */
export function resize (menuElt, sepElt, sceneElt) {
  if (menuElt['data-hasResizeBehavior']) return
  menuElt['data-hasResizeBehavior'] = true
  // pose encore pb sous chrome, faudra voir la méthode utilisée dans new.labomep... elle est pourrie aussi (vu en démo à paris ;-))
  // sinon regarder le code de http://interactjs.io/
  // nos variables d’éléments jQuery
  const $sepElt = $(sepElt)
  // @todo voir si ça marche sur les différents navigateurs en mettant tous les écouteurs sur $sepElt
  const $window = $(window)
  // un flag
  let isSepDragged = false
  // le mouseup devrait pouvoir être occasionnel mais parfois il est pas déclenché
  // (visiblement il est pas encore actif si on est rapide, et on se retrouve avec
  // le séparateur qui "colle" à la souris alors qu’on a lâché le bouton, le clic
  // suivant décollera)
  function onMouseMove (evt) {
    if (isSepDragged) {
      const windowWidth = $(window).width()
      const largeur = windowWidth - $sepElt.width()
      const pourcentageSep = 10 / largeur * 100
      const pourcentageMenu = (evt.pageX - 5) / largeur * 100
      const pourcentageScene = 100 - pourcentageMenu - pourcentageSep
      menuElt.style.width = pourcentageMenu + '%'
      sepElt.style.width = pourcentageSep + '%'
      sceneElt.style.width = pourcentageScene + '%'
    }
  }
  function onMouseUp (evt) {
    if (isSepDragged) {
      isSepDragged = false
      // et on débranche nos 2 écouteurs
      $window.off('mousemove', onMouseMove)
      // $window.off('mouseup', onMouseUp)
    }
  }
  function onMouseDown (evt) {
    isSepDragged = true
    // on devrait pouvoir ne l’attacher que sur $sepElt, mais au cas où…
    // (si le navigateur rame et que la souris n’est plus sur sepElt quand on lâche le bouton pour une raison ou une autre,
    // pas très gênant d’ajouter un appel de fct avec un if (boolean) à chaque fois qu’on lâche le bouton de la souris)
    $window.on('mousemove', onMouseMove)
    // $window.on('mouseup', onMouseUp)
  }

  // le principal est toujours là (active 1 ou 2 autres quand on clique sur le séparateur)
  $sepElt.mousedown(onMouseDown)
  // mais on laisse celui-là permanent, sinon sous chrome on arrive trop facilement à passer au travers
  // et le déplacement reste actif bouton lâché
  $window.on('mouseup', onMouseUp)
}
