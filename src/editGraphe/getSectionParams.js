/**
 * Des fonctions pour interagir entre editGraphe et le player ou les sections
 * @module editGraphe/getSectionParams
 */
import { dispatch, getConfig } from 'src/editGraphe/store'
import { setConfigSectionsParams } from 'src/editGraphe/actions'
import { loadSectionV1 } from 'src/lib/core/loadSection'

/**
 * Récupère l’export params de la section (charge la section et ajoute ces params dans le store si besoin)
 * @param {string} sectionName
 * @returns {Promise<Object, Error>}
 */
async function getSectionParams (sectionName, node) {
  if (!sectionName || typeof sectionName !== 'string') throw Error('Appel invalide')
  if (sectionName.toLowerCase() === 'fin') {
    console.error(Error('Une section fin n’a jamais de paramètres'))
    return {}
  }
  const { sectionsParams } = getConfig()
  // si on a déjà chargé la section on retourne directement les params
  if (sectionsParams[sectionName]) return sectionsParams[sectionName]
  const { params, upgradeParametres } = await loadSectionV1(sectionName, { v1: true })
  if (typeof upgradeParametres === 'function' && node?.nodeParametres?.parametres) {
    // Il faut modifier les params du graphe avant édition
    // => ça va modifier l’objet node mais pas le store redux, pas grave c’est pour init le form.
    // Le store sera modifié avec les bonnes valeurs à la validation du form
    upgradeParametres(node.nodeParametres.parametres)
  }
  if (!params || typeof params !== 'object') throw Error(`Paramètres exportés par la section ${sectionName} invalides (${typeof params})`)
  // on dispatch ça
  dispatch(setConfigSectionsParams(sectionName, params))
  return params
}

export default getSectionParams
