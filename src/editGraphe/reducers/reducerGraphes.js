// les réducteurs sur objetGraphe sont undo-able, on exporte donc par défaut un réducteur undoableObjetGraphe
import undoable from 'redux-undo'
// import {combineFilters} from 'redux-undo'
import ignoreActions from 'redux-ignore'

/**
 * Fct de debug piquée à https://github.com/substack/deep-freeze/blob/master/index.js
 * pour que ça râle en console si on modifie un state (au lieu d’en retourner un nouveau différent)
 * @param {Object} o
 * @returns {Object}
 * @private
 */ /*
function deepFreeze (o) {
  if (Object.freeze && Object.isFrozen) {
    Object.freeze(o)
    Object.getOwnPropertyNames(o).forEach(function (prop) {
      if (
        hasProp(o, prop) &&
        o[prop] !== null &&
        (typeof o[prop] === 'object' || typeof o[prop] === 'function') &&
        !Object.isFrozen(o[prop])
      ) {
        deepFreeze(o[prop])
      }
    })
  }
  return o
} /* */

// le state peut être en fait un sous_state du store, tout ce qui a trait à objetGraphe
// ici state représente la ppte branchements d’un node précis
const reducerBranchement = (branchements = [], action) => {
  /**
   * Écrit en console si action.branchementIndex n’existe pas dans branchements
   * @private
   * @returns {boolean} true si action.branchementIndex existe
   */
  function checkIndex () {
    // on vérifie déjà que l’on a bien un tableau
    if (Array.isArray(branchements)) {
      // et le branchement voulu, si oui on sort tout de suite
      if (branchements[action.branchementIndex]) return true
      else console.error(new Error('Le branchement ' + action.branchementIndex + ' n’existe pas'))
    } else {
      console.error(new Error('reducerBranchement a reçu autre chose qu’un tableau'))
    }
    return false
  }

  // on traite d’abord ceux qui n’ont pas de action.branchementIndex
  // (et AFFECTE_BRANCHEMENT où branchementIndex n’existe pas forcément avant)
  switch (action.type) {
    case 'INIT':
    case 'RESET_BRANCHEMENT':
      return []

    case 'REINDEXE_BRANCHEMENT':
      if (branchements.indexOf(undefined) !== -1) {
        // faut virer les undefined
        return branchements.filter((branchement) => branchement !== undefined)
      } else {
        // rien à faire
        return branchements
      }

    // et celui-là où branchementIndex peut ne pas exister dans branchements
    case 'AFFECTE_BRANCHEMENT':
      // on a deux cas, suivant que le branchement existe déjà ou pas
      if (action.branchementIndex < branchements.length) {
        // l’index existe
        // objetGraphe.nodes[nodeId].branchements[branchementIndex] = branchement
        // on pourrait écrire ça en une ligne
        // return branchements.map((branchement, index) => (index === action.branchementIndex) ? action.branchement : branchement)
        // mais c’est p’tet plus lisible en plusieurs sans ternaire
        return branchements.map(function (branchement, index) {
          if (index === action.branchementIndex) return action.branchement
          else return branchement
        })
      } else if (action.branchementIndex === branchements.length) {
        // le cas simple ou on ajoute un élément à la fin du tableau
        return branchements.concat(action.branchement)
      } else {
        // faut ajouter un nouveau branchement, plus loin dans la fin du tableau,
        // on le signale parce que ça parait bizarre
        // console.log('ajout du branchement d’index ' + action.branchement + ' alors qu’on avait ' + branchements.length + ' éléments')
        const newBranchements = [...branchements]
        // et on ajoute un elt
        newBranchements[action.indexBranchement] = action.branchement
        return newBranchements
      }
  }

  // tous les autres doivent avoir un action.branchementIndex qui existe dans branchements
  if (checkIndex()) {
    switch (action.type) {
      case 'SUPPRESSION_BRANCHEMENT_PROP':
        return branchements.map(function (branchement, index) {
          if (index === action.branchementIndex) {
            // c’est celui qui doit changer, on clone ses propriétés avec ...branchement,
            // et on ajoute la propriété qui va bien
            const newBranchement = { ...branchement }
            delete newBranchement[action.prop]
            return newBranchement
          } else {
            // les autres ne changent pas
            return branchement
          }
        })

      case 'AFFECTE_BRANCHEMENT_PROP':
        return branchements.map(function (branchement, index) {
          if (index === action.branchementIndex) {
            // c’est celui qui doit changer, on clone ses propriétés avec ...branchement,
            // et on ajoute la propriété qui va bien
            const newBranchement = { ...branchement }
            newBranchement[action.prop] = action.valeur
            return newBranchement
          } else {
            // les autres ne changent pas
            return branchement
          }
        })

      case 'AFFECTE_COULEUR_BRANCHEMENT':
        // objetGraphe.nodes[nodeId].branchements[branchementIndex].style.stroke = couleur
        return branchements.map(function (branchement, index) {
          if (index === action.branchementIndex) {
            // faut pas modifier son style mais le cloner
            const style = { ...branchement.style, stroke: action.couleur }
            return { ...branchement, style }
          } else {
            return branchement
          }
        })

      case 'COMPLETE_BRANCHEMENT':
        // faut retourner un nouveau branchement complété pour action.branchementIndex
        return branchements.map(function (branchement, index) {
          if (index === action.branchementIndex) {
            // c’est celui qui doit changer, on clone ses propriétés avec ...branchement,
            // que l’on écrase avec celles de  action.branchement
            return { ...branchement, ...action.branchement }
          } else {
            // les autres ne changent pas
            return branchement
          }
        })
        // for (let ppte in branchement) {
        //   if (hasProp(branchement, ppte)) {
        //     objetGraphe.nodes[nodeId].branchements[branchementIndex][ppte] = branchement[ppte]
        //   }
        // }

      case 'SUPPRIME_BRANCHEMENT':
        // version transitoire qui met undefined à la place de supprimer le branchement (moins bien, mais je vois plus l’intérêt de reindexebranchement...)
        // return branchements.map((branchement, index) => {
        //   if (index === Number(action.branchementIndex)) {
        //     return undefined
        //   } else {
        //     return branchement
        //   }
        // })
        // pour mémoire
        return branchements.filter(function (branchement, index) {
          return index !== Number(action.branchementIndex)
        })

      default:
        console.error(new Error('reducerBranchement a reçu une action non gérée'))
        return branchements
    }
  } else {
    // on fait rien, checkIndex a déjà listé le pb en console
    return branchements
  }
} // reducerBranchement

// ici state représente la ppte parametres d’un node précis
const reducerParametres = (node = {}, action) => {
  switch (action.type) {
    case 'INIT':
      return {}
    case 'AFFECTE_PARAM_NODE':
      // on crée un nouvel objet avec le truc à surcharger
      return { ...node, [action.param]: action.value }
    default:
      return node
  }
}
// ici state représente un node (ayant une ppté branchements (un tableau), une ppte parametres (un objet), left, top, section, title)
const reducerNode = (state = {}, action) => {
  switch (action.type) {
    case 'INIT':
      return {}
    case 'AFFECTE_BRANCHEMENT':
    case 'AFFECTE_BRANCHEMENT_PROP':
    case 'SUPPRESSION_BRANCHEMENT_PROP':
    case 'AFFECTE_COULEUR_BRANCHEMENT':
    case 'RESET_BRANCHEMENT':
    case 'COMPLETE_BRANCHEMENT':
    case 'REINDEXE_BRANCHEMENT':
    case 'SUPPRIME_BRANCHEMENT':
      return { ...state, branchements: reducerBranchement(state.branchements, action) }
    case 'AFFECTE_PARAM_NODE':
      return { ...state, parametres: reducerParametres(state.parametres, action) }
    case 'AFFECTE_TITRE_NODE':
      return { ...state, title: action.title }
    default :
      return state
  }
}
// Ici state représente la ppte nodes de objetGraphe
const reducerNodesObjetGraphe = (nodes = {}, action) => {
  const { type, node, nodeId } = action
  switch (type) {
    case 'INIT':
      return {}
    case 'SUPPRESSION_NODE': {
      const newNodes = {}
      // on vire le node qui a le même id que action.nodeId
      // mais on a un objet et pas un tableau
      if (!nodes[nodeId]) {
        console.error(new Error('reducerNodesObjetGraphe avec SUPPRESSION_NODE et un action.nodeId qui n’existe pas ' + action.nodeId))
        return nodes
      }
      Object.keys(nodes).forEach(id => {
        if (id !== nodeId) newNodes[id] = nodes[id]
      })
      return newNodes
    }

    case 'AJOUTE_NODE':
      if (node.parametres?.nn) console.error(Error('Ajout d’un node avec des paramètres qui ont une prop nn, ça devrait plutôt être un branchement !'), node)
      return { ...nodes, [nodeId]: node }

    case 'AFFECTE_BRANCHEMENT':
    case 'AFFECTE_BRANCHEMENT_PROP':
    case 'SUPPRESSION_BRANCHEMENT_PROP':
    case 'AFFECTE_PARAM_NODE':
    case 'SUPPRIME_BRANCHEMENT':
    case 'AFFECTE_TITRE_NODE':
    case 'AFFECTE_COULEUR_BRANCHEMENT':
    case 'RESET_BRANCHEMENT':
    case 'COMPLETE_BRANCHEMENT':
    case 'REINDEXE_BRANCHEMENT':
      return { ...nodes, [nodeId]: reducerNode(nodes[nodeId], action) }

    default :
      return nodes
  }
}
// Ici state représente la ppte maxNumeroNode
const reducerMaxNNobjetGraphe = (state = 0, action) => {
  switch (action.type) {
    case 'INIT':
      return 0
    case 'INCREMENTE_MAXNN': // anciennement setObjetGrapheMaxNumIncrement
      return state + 1
    case 'DECREMENTE_MAXNN':
      return state - 1
    default :
      return state
  }
}

const emptyGraphes = { nodes: {}, maxNumeroNode: 0 }
// Ici state représente la ppté objetGraphe de mon store,
// c’est le reducer principal qui appelle les autres
const reducerGraphes = (state = emptyGraphes, action) => {
  // si on est lancé localement sur http://localhost:3000,
  // on ajoute du deepFreeze pour interdire toute modif
  // en prod on ne le fait pas car c’est coûteux en perfs
  // Le pb reste que ça ne râle pas si on affecte une valeur, ça ignore simplement l’affectation
  // donc pas si utile ici, faudrait plutôt mettre ça dans les tests
  // on le vire car ça fait planter valideBranchement (qui modifie directement le state)
  // if (window.location.host === 'localhost:3000') deepFreeze(state)

  try {
    switch (action.type) {
      case 'INIT':
        return emptyGraphes

      case 'SUPPRESSION_NODE':
      case 'AJOUTE_NODE':
      case 'AFFECTE_PARAM_NODE':
      case 'AFFECTE_TITRE_NODE':
      case 'AFFECTE_BRANCHEMENT':
      case 'AFFECTE_BRANCHEMENT_PROP':
      case 'SUPPRESSION_BRANCHEMENT_PROP':
      case 'SUPPRIME_BRANCHEMENT':
      case 'AFFECTE_COULEUR_BRANCHEMENT':
      case 'RESET_BRANCHEMENT':
      case 'COMPLETE_BRANCHEMENT':
      case 'REINDEXE_BRANCHEMENT':
        return { ...state, nodes: reducerNodesObjetGraphe(state.nodes, action) }

      case 'INCREMENTE_MAXNN':
      case 'DECREMENTE_MAXNN':
        return { ...state, ...{ maxNumeroNode: reducerMaxNNobjetGraphe(state.maxNumeroNode, action) } }

      case 'AFFECTE_PPTE':
        return { ...state, [action.prop]: action.value }

      default :
        return state
    }
  } catch (error) {
    console.error(error)
    return state
  }
}

// ces actions ne sont pas historisées sinon ça met le brin dans le redo
const ignoredActions = ['ENLEVE_INDEX', 'AJOUTE_INDEX_FUTUR', 'ENLEVE_PREMIER_FUTUR', 'AJOUTE_DERNIER_HISTORIQUE']
export default ignoreActions(
  undoable(reducerGraphes),
  ignoredActions
)
