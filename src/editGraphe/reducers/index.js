import { combineReducers } from 'redux'
import graphes from './reducerGraphes'
import positionNodes from './reducerPositionNodes'
import ressource from './reducerRessource'
import titreNodes from './reducerTitreNodes'
import config from './reducerConfig'
import compteurHistorique from './reducerHistorique'
import compteurFutur from './reducerFutur'

const mainReducer = combineReducers({
  graphes, positionNodes, titreNodes, config, ressource, compteurHistorique, compteurFutur
})

export default mainReducer

/*
Identique à

function mainReducer(state = {}, action) {
  return {
    graphes: graphes(state.graphes, action),
    ...
  }
}

je le laisse en commentaire car je trouve ça plus simple pour piger ce qui se passe...
*/
