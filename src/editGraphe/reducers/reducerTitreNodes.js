const reducerTitreNodes = (state = [], action) => {
  switch (action.type) {
    case 'INIT':
      return []

    case 'CHANGE_UN_TITRE':
    //   if (action.index < state.length) {
    //     return [...state.slice(0, action.index), action.titre, ...state.slice(0, action.index + 1)]
    //   } else {
    //     return state
    //   }
      return state.map((item, index) => (index === action.index) ? action.titre : item)

    case 'CHANGE_LES_TITRES':
      return [...action.titres]

    default:
      return state
  }
}

export default reducerTitreNodes

/*
export const setTitres = (titres) => {
  if (Array.isArray(titres)) titres = titres.map((titre, index) => setTitre(index, titre))
  return titres
}
*/
