const reducerRessource = (state = {}, action) => {
  switch (action.type) {
    case 'INIT':
      return {}

    case 'AJOUTE_RESSOURCE':
      return { ...action.ressource }

    default:
      return state
  }
}

export default reducerRessource
