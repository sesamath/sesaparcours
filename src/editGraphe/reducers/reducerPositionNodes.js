/**
 * Indique si l’argument est valide pour une position (tableau de deux number)
 * @param {Position} position
 * @returns {boolean}
 * @private
 */
const isPosition = (position) => Array.isArray(position) && position.length === 2 && typeof position[0] === 'number' && typeof position[1] === 'number'

const reducerPositionNodes = (positions = [], action) => {
  switch (action.type) {
    case 'INIT':
      return []

    case 'AFFECTE_LES_POSITIONS':
      if (Array.isArray(action.positions)) {
        // si on nous a filé une nouvelle position on la retourne
        // sinon c’est l’ancienne, mais on sait pas si y’a plus de positions dans l’un ou l’autre,
        // donc on fait pas de map mais un for classique
        const newPositions = action.positions.map(function (position, index) {
          // y’a une nouvelle position
          if (position) return position
          // c’est un trou dans le tableau actions.positions mais y’avait une ancienne position
          else if (positions[index]) return positions[index]
          // un trou dans action.positions qui n’existe pas dans positions
          else return undefined
        })
        // faut ajouter d’éventuelles anciennes positions qui n’auraient pas été fournies dans actions.positions
        if (positions.length > action.positions.length) {
          const reste = positions.slice(action.positions.length)
          return newPositions.concat(reste)
        } else {
          return newPositions
        }
      } else {
        console.error(new Error('action AFFECTE_LES_POSITIONS avec action.positions qui n’est pas un tableau'))
        return positions
      }
      // if (Array.isArray(positions)) positions = positions.map((position, index) => setPosition(index, position))
      // return positions

    case 'AFFECTE_UNE_POSITION':
      if (isPosition(action.position)) {
        return positions.map(function (position, index) {
          if (index === action.index) return action.position
          else return position
        })
      } else {
        console.error(new Error('setPosition réclame une position, tableau de deux number'), action.position)
        // on crée quand même l’index s’il n’existait pas
        if (positions.length <= action.index) {
          const newPositions = [...positions]
          newPositions[action.index] = undefined
          return newPositions
        } else {
          return positions
        }
      }
      // if (typeof index === 'number' && index >= 0) {
      //   if (isPosition(position)) {
      //     positions[index] = position
      //   } else {
      //     console.error(new Error('setPosition réclame une position, tableau de deux number'), position)
      //     // on crée quand même l’index s’il n’existait pas
      //     if (positions.length <= index) positions[index] = undefined
      //   }
      //   return positions[index]
      // } else {
      //   console.error(new Error('setPosition réclame un index en 1er argument'), index)
      // }

    default:
      return positions
  }
}

export default reducerPositionNodes
