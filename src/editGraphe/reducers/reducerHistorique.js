const reducerHistorique = (state = [], action) => {
  switch (action.type) {
    case 'INIT':
      return []
    case 'ENREGISTRE_INDEX':
      // ajoute la valeur action.index comme dernier element du tableau
      // Rq DC, je sais pas si c’est ça qu’il faut, ça fait doublon avec AJOUTE_DERNIER_HISTORIQUE
      return [...state, action.index]

    case 'ENLEVE_INDEX':
      // on retourne le state sans son dernier élément
      return state.slice(0, -1)

    case 'AJOUTE_DERNIER_HISTORIQUE':
      return [...state, action.value]

    default:
      return state
  }
}

export default reducerHistorique
