// un state par défaut pour la config, à voir ce qu’il faudra dégager..., initialisé à la création du store

const DEFAULT_STATE_CONFIG = {
  noeudcourant: 0,
  fenetre: 'parametres', // || 'conditions'
  parametresnoeuds: {},
  parametres: 'tous', // 'surcharges' tous
  // construit au fur et à mesure du chargement des sections, sectionsParams[nomSection] = export params de la section
  sectionsParams: {},
  bdd: [],
  arboeditgraphes: '', // contenu du fichier arboeditgraphes.txt
  arbo: [], // variable utilisée pour l’affichage des ressources du fichier arboeditgraphes.txt
  affichageressourcesdivD: { ressource: { code: -1, index: -1 }, option: 0 },
  dimensionsdivD: { larg: 0.7, haut: 0.6 },
  indexdansbdd: {},
  afficheExplicationsGraphique: true,
  chargement_ressources_j3p: 'bibli', // pour savoir si on charge les ressources j3p en utilisant j3pbdd.txt avec 'bdd' ou si on interroge la bibli (avec 'bibli')'
  // spanressourcecourante : -1, //dans la fenêtre ressource = k du spanlaressourcek précedemment sélectionné
  modelanceur: 'ressources', // 'graphes' , 'developpeur' ,  'ressources', 'graphique',
  graphTmp: [], // attention, n’existait pas originalement
  // une liste de params masqués pour certaines sections
  // (lors du paramétrage du noeud, il ne faut pas afficher certains paramètres de la section,
  // pour les sections outils par exemple car le fichier externe a déjà été choisi)
  parametresMasques: {
    exo_mep: ['exo'],
    ancienexo_mep: ['exo'],
    aide_mep: ['aide'],
    squeletteatome: ['atome'],
    lecteuriepparurl: ['url']
  }
}

const reducerConfig = (stateConfig = DEFAULT_STATE_CONFIG, action) => {
  switch (action.type) {
    case 'INIT':
      return { ...DEFAULT_STATE_CONFIG }

    case 'TOGGLE_EXPLICATIONS_GRAPHIQUE':
      return { ...stateConfig, afficheExplicationsGraphique: !stateConfig.afficheExplicationsGraphique }

    case 'SET_GRAPHE_TEMP':
      return { ...stateConfig, graphTmp: action.graphe }

    case 'SET_CONFIG_SECTIONPARAMS':
      return { ...stateConfig, sectionsParams: { ...stateConfig.sectionsParams, [action.nomSection]: action.params } }

    case 'SET_GRAPHE_TEMP_SECTION_OPTS':
    case 'SET_GRAPHE_TEMP_NN':
    case 'PUSH_GRAPHE_TEMP_NOEUD':
      return { ...stateConfig, graphTmp: reducerConfigGrapheTmp(stateConfig.graphTmp, action) }

    default: return stateConfig
  }
}

/**
 * Retourne tab à l’identique sauf pour un élément (précisé par indexAModifier) qui sera transformé par modifCb
 * @private
 * @param {Array} tab
 * @param {number} indexAModifier
 * @param {function} modifCb
 */
const changeOneItem = (tab, indexAModifier, modifCb) => tab.map((item, index) => (index === indexAModifier) ? modifCb(item) : item)

const reducerConfigGrapheTmp = (stateGraphTmp = [], action) => {
  // modification d’une ppte d’un branchement
  const brModifier = (branchement) => {
    return { ...branchement, [action.prop]: action.value }
  }
  // modification d’un branchement suivant la callback brModifier
  const nodeModifier = (node) => {
    return [node[0], node[1], changeOneItem(node[2], action.index2, brModifier)]
  }
  switch (action.type) {
    case 'INIT':
      return []

    case 'SET_GRAPHE_TEMP_SECTION_OPTS':
      return stateGraphTmp.map((node, index) => {
        if (index === action.index) {
          const [id, section] = node
          return [id, section, action.sectionOpts]
        }
        return node
      })

    case 'SET_GRAPHE_TEMP_NN': // du stateGraphTmp - graphTmp -  on doit modifier la ppté 'nn' d’un branchement d’index2 d’un node d’index
      return changeOneItem(stateGraphTmp, action.index, nodeModifier)

    case 'PUSH_GRAPHE_TEMP_NOEUD':
      return [...stateGraphTmp, action.noeud] // action.noeud

    default :
      return stateGraphTmp
  }
}

export default reducerConfig

// sera mis en listener sur la case à cocher "ne plus afficher les infos…"
// window.toggleExplicationsGraphique = function (o) {
// if (o.checked) setConfigProp('afficheExplicationsGraphique', false)
// }
