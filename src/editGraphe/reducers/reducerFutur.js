const reducerFutur = (state = [], action) => {
  switch (action.type) {
    case 'AJOUTE_INDEX_FUTUR': {
      const newState = state.map((truc) => truc + action.value + 1)
      return [action.value, ...newState]
    }

    case 'INIT':
    case 'VIDE_FUTUR':
      return []

    case 'ENLEVE_PREMIER_FUTUR': {
      // la valeur du 1er elt
      const value = state[0]
      // un nouveau state sans le 1er elt, attention c’est du shallow copy
      // donc faut pas modifier le contenu
      const newState2 = state.slice(1)
      return newState2.map((truc) => truc - (value + 1))
      // on pouvait aussi faire ça sur une ligne
      // return state.slice(1).map((truc) => truc - (state[0] + 1))
    }

    default:
      return state
  }
}

export default reducerFutur
