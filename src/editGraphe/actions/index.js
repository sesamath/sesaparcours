/**
 * La liste des actions redux pour editgraphe
 * @module editGraphe/actions/index
 */

export const init = () => ({ type: 'INIT' })

/**
 * Supprime un node
 * @param {string} nodeId
 */
export const delObjetGrapheNode = (nodeId) => ({ type: 'SUPPRESSION_NODE', nodeId })
/**
 * Affecte un node
 * @param {string} nodeId
 * @param {Node} node
 * @param {boolean} [recordable=true]
 */
export const setObjetGrapheNode = (nodeId, node, recordable = true) => ({ type: 'AJOUTE_NODE', node, nodeId, recordable })
export const setObjetGrapheProp = (prop, value) => ({ type: 'AFFECTE_PPTE', prop, value })

export const setRessource = (ressource) => ({ type: 'AJOUTE_RESSOURCE', ressource })

export const setObjetGrapheBranchementProp = (nodeId, branchementIndex, prop, valeur) => ({ type: 'AFFECTE_BRANCHEMENT_PROP', nodeId, branchementIndex, prop, valeur })
export const delObjetGrapheBranchementProp = (nodeId, branchementIndex, prop) => ({ type: 'SUPPRESSION_BRANCHEMENT_PROP', nodeId, branchementIndex, prop })

export const setObjetGrapheBranchement = (nodeId, branchementIndex, branchement, recordable = true) => ({ type: 'AFFECTE_BRANCHEMENT', nodeId, branchementIndex, branchement, recordable })

export const setObjetGrapheTitle = (nodeId, title) => ({ type: 'AFFECTE_TITRE_NODE', nodeId, title })

export const delObjetGrapheBranchement = (nodeId, branchementIndex) => ({ type: 'SUPPRIME_BRANCHEMENT', nodeId, branchementIndex })

export const setObjetGrapheParam = (nodeId, param, value) => ({ type: 'AFFECTE_PARAM_NODE', nodeId, param, value })

export const setObjetGrapheColor = (nodeId, branchementIndex, couleur) => ({ type: 'AFFECTE_COULEUR_BRANCHEMENT', nodeId, branchementIndex, couleur })

export const reindexObjetGrapheBranchements = (nodeId) => ({ type: 'REINDEXE_BRANCHEMENT', nodeId })
export const resetObjetGrapheBranchements = (nodeId) => ({ type: 'RESET_BRANCHEMENT', nodeId })
export const addObjetGrapheBranchement = (nodeId, branchementIndex, branchement) => ({ type: 'COMPLETE_BRANCHEMENT', nodeId, branchementIndex, branchement })

export const incrementeMaxNN = () => ({ type: 'INCREMENTE_MAXNN' })
export const decrementeMaxNN = () => ({ type: 'DECREMENTE_MAXNN' })

export const enregistreIndex = (index) => ({ type: 'ENREGISTRE_INDEX', index })
export const enleveIndex = () => ({ type: 'ENLEVE_INDEX' })

// comme enregistre_index mais non historisé (cf reducerObjetGraphe)
export const ajouteDernierHistorique = (value) => ({ type: 'AJOUTE_DERNIER_HISTORIQUE', value })
export const videFutur = () => ({ type: 'VIDE_FUTUR' })
export const ajouteIndexFutur = (value) => ({ type: 'AJOUTE_INDEX_FUTUR', value })
export const enlevePremierFutur = () => ({ type: 'ENLEVE_PREMIER_FUTUR' })
export const setPositions = (positions) => ({ type: 'AFFECTE_LES_POSITIONS', positions })

/**
 * Retourne l’action AFFECTE_UNE_POSITION
 * @param index
 * @param position
 * @returns {{type: string, position: *, index: *}}
 */
export const setPosition = (index, position) => {
  // on vérifie le type avant de créer l’action
  if (typeof index !== 'number') {
    console.error(new Error('setPosition veut un index numérique'))
    index = Number(index)
  }
  return { type: 'AFFECTE_UNE_POSITION', position, index }
}

export const setTitre = (index, titre) => ({ type: 'CHANGE_UN_TITRE', index, titre })
export const setTitres = (titres) => ({ type: 'CHANGE_LES_TITRES', titres })

export const toggleExplicationsGraphique = () => ({ type: 'TOGGLE_EXPLICATIONS_GRAPHIQUE' })

export const setConfigGraphTmp = (graphe) => ({ type: 'SET_GRAPHE_TEMP', graphe })

/**
 * Ajoute le chemin de la section dans le store
 * @param {string} section Le chemin du fichier de la section "sections/….js"
 * @return {{section: string, type: string}}
 */
export const setConfigSectionsParams = (nomSection, params) => ({ type: 'SET_CONFIG_SECTIONPARAMS', params, nomSection })
export const setConfigGraphTmpSectionOpts = (index, sectionOpts) => ({ type: 'SET_GRAPHE_TEMP_SECTION_OPTS', index, sectionOpts })
export const setConfigGraphTmpSsElt = (index, index2, prop, value) => ({ type: 'SET_GRAPHE_TEMP_NN', index, index2, prop, value })

export const pushConfigGraphTmp = (noeud) => ({ type: 'PUSH_GRAPHE_TEMP_NOEUD', noeud })
