/**
 * Initialise l’éditeur de graphes en mode édition de ressource (si elle est fournie) ou création
 * Pour l’autre mode (coloration de parcours) voir showParcours.js
 * @module
 */
import { empty } from 'src/lib/utils/dom/main'
import { init as menuInit } from './menu'
import { importerGrapheFin, importerGraphe } from './importGraphe'
import { init as sceneInit, getCurrentRessourceParametres, actualiseGestionnaireEvenements } from './scene'
import { findOrCreateElement } from './fonctionsUtiles'
import { resize } from './separateur'
import edgModale from './boiteDialogue'
import fullscreen from './fullscreen'
import { dispatch, setRootElt } from './store'
import { init, setRessource } from 'src/editGraphe/actions'

import './css/layout.css'

// dispatch(incrementeMaxNN())
// dispatch(decrementeMaxNN())
// const nodetest = { section: 'parallelesfaisceau', parametres: {'inclinaison': '[-90;90]', 'nbdroites': 8, 'nbrepetitions': 3}, left: 200, top: 50, title: 'Nœud 1', style: '', branchements: [{'pe': '>=0.9', 'nn': 'fin', 'conclusion': 'Bravo ! Parcours terminé '}] }
// dispatch(setObjetGrapheNode(3, nodetest))
// dispatch(setObjetGrapheNode(5, nodetest))
// dispatch(delObjetGrapheNode(3))
// dispatch(toggleExplicationsGraphique())

/**
 * @private
 * @param {Error} error
 */
function showError (error) {
  console.error(error)
  edgModale({ id: 'modaleError', titre: 'Editeur de graphes', contenu: error.message, width: 600, height: 200 })
}

/**
 * @typedef EditGrapheOptions
 * @property {boolean} [isFullscreenAllowed=true]
 * @property {string} [sourceTreeRid]
 */

/**
 * Charge l’éditeur de graphe dans rootElt et l’initialise avec un graphe (au format ressource d’une sesatheque)
 * Passera à la callback la fct permettant de récupérer le graphe
 * @param {HTMLElement} rootElt
 * @param {Ressource} ressource
 * @param {object} [options]
 * @param {boolean} [options.isFullscreenAllowed=true] passer false pour ne pas mettre le bouton fullscreen
 * @param {string} [options.sourceTreeRid] Le rid de l’arbre racine à charger à gauche (sinon on prendra celui de la configuration par défaut)
 * @param {editCallback} [next] Sera appelé à la fin du chargement en passant une erreur éventuelle et la méthode getRessourceParametres en 2e argument
 */
function editGraphe (rootElt, ressource, options, next) {
  // récupère l’élément ou le crée, à ajouter à sesajstools.dom
  try {
    if (typeof options === 'function') {
      next = options
      options = {}
    } else if (!options) {
      options = {}
    }

    // console.debug('le state à l’init d’editGraphe', getState())
    empty(rootElt)
    setRootElt(rootElt)
    dispatch(init())

    // pour mes tests :
    // optionsDisparue = {noeuds: ['1', '2'], scores: [0.5, 1], pe: [0.5, 1], graphe: [['1', 'EtudeFonction_derivee', [{'nn': '2', 'score': 'sans condition', 'conclusion': 'Etudions les limites', 'label': 'Sans condition'}, {'imposer_fct': '', 'modele': [3], 'nbrepetitions': '1', 'indication': '', 'limite': '', 'nbchances': '2', 'imposer_domaine': []}]], ['2', 'EtudeFonction_limite', [{'nn': '3', 'pe': 'sans condition', 'conclusion': 'Etude du signe de la dérivée', 'label': 'Sans condition'}, {'imposer_fct': 'j3p.parcours.donnees[1]'}]], ['3', 'EtudeFonction_facteursderivee', [{'nn': '4', 'pe': 'sans condition', 'conclusion': 'Poursuivons l’étude de la dérivée', 'label': 'Sans condition'}, {'imposer_fct': 'j3p.parcours.donnees[1]'}]], ['4', 'EtudeFonction_signederivee', [{'nn': '5', 'pe': 'sans condition', 'conclusion': 'Passons aux variations de la fonction', 'label': 'Sans condition'}, {'imposer_fct': 'j3p.parcours.donnees[1]'}]], ['5', 'EtudeFonction_variations', [{'nn': '6', 'pe': 'sans condition', 'conclusion': 'Etude de fonction terminée', 'label': 'Sans condition'}, {'imposer_fct': 'j3p.parcours.donnees[1]'}]], ['6', 'fin', [null]]], editgraphes: {'positionNodes': [[213, 287], [441, 415], [663, 372], [853, 398], [929, 470], [1019, 576]], 'titreNodes': ['Dérivée de (ax+b)exp(x)', 'Limites aux bornes de l’ensemble de définition', 'Nombre de facteurs de la dérivée', 'Signe de la dérivée', 'Variations', 'Fin']}}
    dispatch(setRessource(ressource)) // on file la ressource pour déjà renseigner des trucs
    // un message d’attente avec une variable dessus pour faire un remove à la fin
    const waitElt = findOrCreateElement(rootElt, 'p', { id: 'edgWait' }, 'Chargement en cours…')
    // header
    const headerElt = findOrCreateElement(rootElt, 'div', { id: 'edgHeader' }, 'Éditeur de graphe')
    // bouton fullscreen par défaut
    if (!options || options.isFullscreenAllowed !== false) {
      const fullScreenButton = findOrCreateElement(headerElt, 'button', { id: 'edgFullScreenButton' }, 'Plein écran')
      fullscreen(fullScreenButton)
    }
    // contenu
    const contentElt = findOrCreateElement(rootElt, 'div', { id: 'edgContent' })
    // footer
    findOrCreateElement(rootElt, 'div', { id: 'edgFooter' }, 'Sésamath')
    const menuElt = findOrCreateElement(contentElt, 'div', { id: 'edgMenu' })
    const sepElt = findOrCreateElement(contentElt, 'div', { id: 'edgSeparateur' })
    const sceneElt = findOrCreateElement(contentElt, 'div', { id: 'edgScene' })
    // redimensionnement possible des div
    resize(menuElt, sepElt, sceneElt)
    // ici on appelle nos autres modules en leur filant une callback à appeller quand ce sera fini, attention là c’est la cascade de callbacks...
    sceneInit(sceneElt, function () { // la callback
      menuInit(menuElt, options, function (error) {
        if (error) showError(error)
        // on continue même si le menu n’a pas réussi à s’initialiser
        importerGraphe(ressource, function () {
          // var graphe = ressource && ressource.parametres && ressource.parametres.g || []
          importerGrapheFin(function () {
            // c’est fini, on vire notre message d’attente
            waitElt.parentNode.removeChild(waitElt)
            actualiseGestionnaireEvenements()
            if (next) next(null, getCurrentRessourceParametres)
            else console.error('pas de callback fournie à editGraphe')
          }) // importerGrapheFin
        }) // importerGraphe
      }) // menuInit
    }) // sceneInit
  } catch (error) {
    console.error(error)
    if (next) next(error)
  }
}

export default editGraphe

/**
 * @callback editCallback
 * @param {Error|null} error une erreur éventuelle
 * @param {getRessourceParametres} getRessourceParametres Une fonction pour récupérer les paramètre du graphe en cours d’édition
 */
/**
 * @callback getRessourceParametres
 * @return {RessourceJ3pParametresV1}
 */
