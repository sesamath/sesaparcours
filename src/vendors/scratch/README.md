Blockly et scratch-blocks
=========================

Le module npm scratch-blocks fournit un Blockly transformé pour scratch.

Le pb est qu'avec vite l'import classique de `scratch-blocks` marche plus, parce que ça importe node_modules/scratch-blocks/shim/vertical.js qui contient 

```js
module.exports = require('imports-loader?Blockly=../shim/blocks_compressed_vertical-blockly_compressed_vertical-messages,goog=../shim/blockly_compressed_vertical.goog!exports-loader?Blockly!../msg/scratch_msgs');
```

Et donne l'erreur `[commonjs--resolver] Invalid URL`

On copie donc le contenu des fichiers de ce module (ce que shim/vertical.js devait charger) dans notre Blockly.js

Ça concerne
* node_modules/scratch-blocks/blockly_compressed_vertical.js
* node_modules/scratch-blocks/blocks_compressed.js
* node_modules/scratch-blocks/blocks_compressed_vertical.js
* node_modules/scratch-blocks/msg/messages.js (seulement en et fr)
* node_modules/scratch-blocks/msg/scratch_msgs.js (seulement en et fr)

**Attention**, il faut remplacer dedans 
* `goog.global=this` => `goog.global=globalThis`
* `this.nodeCallback_ =` => `globalThis.nodeCallback_ = `

Après un changement de version de scratch-blocks, il faut donc relancer (depuis la racine) `src/vendors/scratch/refresh.sh`
