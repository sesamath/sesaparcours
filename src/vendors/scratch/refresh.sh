#!/usr/bin/env bash
dst=src/vendors/scratch/Blockly.js
echo "/* généré automatiquement par $0 le $(date +%F) (cf src/vendors/scratch/README.md) */" > $dst
for f in blockly_compressed_vertical.js blocks_compressed.js blocks_compressed_vertical.js msg/messages.js; do
  echo -e "\n\n/* $f */" >> $dst
  cat node_modules/scratch-blocks/$f |sed -e '1,/use strict/d; s/goog.global=this/goog.global=globalThis/g; s/this.nodeCallback_ *=/globalThis.nodeCallback_=/g' >> $dst
done
# et les messages fr only
echo -e "\n\n/* node_modules/scratch-blocks/msg/scratch_msgs.js (fr only) */" >> $dst
sed -nre '/^goog/p; /^Blockly.ScratchMsgs.locales\["fr"]/,/^\}/ p;' node_modules/scratch-blocks/msg/scratch_msgs.js >> $dst
# export par défaut
echo -e "\n\n/* export par défaut ajouté par $0 */" >> $dst
echo -e "export default Blockly" >> $dst
