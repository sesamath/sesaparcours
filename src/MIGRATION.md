# La liste des choses à faire pour passer la V2 en prod pour tous

Avant de n’utiliser que le moteur v2 pour toutes les ressources des biblis, il faut vérifier que l’on gère bien tout ce
qui l’était et que ça n’ajoute pas de bugs.

Lorsque les tests locaux (et sur j3p2.sesamath.net) seront concluants, on pourra modifier sesatheque-plugin-j3p pour
qu'il utilise j3p2.sesamath.net et le déployer sur les deux biblis de dev.

**ATTENTION, une fois la v2 passée en prod, plus de retour en v1 possible, car on aura déjà des données au format v2
dans les bases de données (des ressources dans les biblis et des résultats dans labomep), il faudra donc régler tous les
bugs qui se présenteront, mieux vaut qu'ils ne soient pas trop nombreux ni trop compliqués à résoudre.**

## À faire

- supprimer tous les appels de this.sectionSuivante() dans les sections v1 (ça passera pas en v2 car c'est au moteur de
  gérer ça)
- lors de la reprise de graphe, comparer le graphe actuel avec celui contenu dans le résultat pour vérifier que l’on
  peut faire une reprise (si le graphe a changé sur un nœud déjà fait dans le résultat, faut repartir au début avec un
  message expliquant pourquoi, cf src/dev/start.runners.ts:259). J-C : à priori, la comparaison fonctionne quand c'est
  Ok, reste à tester quand le graphe a changé, mais je ne vois pas comment faire pour tester ça.

## À vérifier

Lancer le test en local (avec un pnpm start lancé par
ailleurs) `baseUrl=http://localhost:8081/ PRODLIKE=2 USAGE=1 pnpm test:browser loadAll` et regarder ensuite
log/loadAll.log

### Éditeur

~~Tester un noeud à coder~~ Ok.
Semble ok, à tester depuis labomep (de dev) quand les biblis de dev seront en v2

### Player

Tester le plus de ressources possibles via prod2.html, au moins une par grande famille de ressources.

Il y a une liste en bas de la home, en local il faut faire un pnpm run build pour que ça fonctionne (car le pnpm start
ne semble fonctionner que pour index.html, les autres html vont chercher leur js dans build, faudra régler ça,
probablement à cause de docroot qui est toujours outdir mais aussi publicDir au start)

#### ~~Reprise de graphe~~

- OK le 8/6 avec un graphe v1 et un lastResultat v1 (testé par JCL avec resultatV1NonFini et resultatV2NonFini de
  src/dev/exemples.ts)

### ~~Afficheur de parcours~~

- ~~Tester l’affichage d’un résultat v1 avec showPathway.~~ Ok le 10/06 testé avc un résultat d’élève de JCL tiré de
  LaboMep.
- ~~Tester l’affichage d’un résultat v2 avec showPathway, qui doit conserver les positions choisies dans l’éditeur.~~ Ok
  le 10/06 testé avec le résultat fourni par pathway.getResultat() sur le graphTest par JCL (prendre l'habitude de
  renseigner les positions dans le graph, sinon, tous les noeuds s'empilent.)

# Liste des ressources testées avec le player V2 #

sesacommun/5bcc83f4c0c5330c8489e053 Ok
sesacommun/6174119c92d5305fbfa18f61 Ok
sesacommun/5eb255a3db489f645f08992e Ok
sesabibli/53889 Ok
sesabibli/5ab7bc9c8183546ba38c28c8 Ok
sesacommun/5bd16f268e37180c82b3479b Ok
sesacommun/622f3215db5925093393479d Ok
sesabibli/5e729887d1d2d74c7d1b422b Ok retourne un score de 0 pour le noeud (il serait plus cohérent de renvoyer -1 comme
score)
sesabibli/62a314feac11c318c71c5f1a Ok
