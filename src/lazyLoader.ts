/**
 * loader.js a déclaré en global toutes nos fonctions de chargement :
 * - j3pLoad (player v1)
 * - editGraphe (éditeur v1)
 * - showParcours (afficheur de parcours v1)
 * - spEdit (éditeur v2)
 * - spPlay (player v2)
 * - spView (afficheur de parcours v2)
 * - spValidate (validateur de ressource v2)
 * - start (interface de la home)
 * et il nous charge ensuite.
 *
 * Ce script qui peut être chargé avec un type module remplace alors ces fonctions pour charger la fct finale lors du premier appel et l’appeler ensuite.
 * cf {@tutorial chargement}
 *
 * ATTENTION, ce fichier ne doit pas avoir d’import statique, pour pouvoir être appelé dans un tag script ordinaire
 * (compatibilité ascendante)
 * @fileOverview
 */

import type { LoaderName, SpLoading } from 'src/lib/types'

/** @private */
const labels: Record<LoaderName, string> = {
  j3pLoad: 'du lecteur de graphe j3p',
  spPlay: 'du lecteur de graphe v2 j3p',
  editGraphe: 'de l’éditeur de graphe j3p',
  spEdit: 'de l’éditeur de graphe j3p v2',
  showParcours: 'de l’afficheur de parcours j3p (v1)',
  spView: 'de l’afficheur de parcours j3p (v2)',
  spValidate: 'du vérificateur de ressource j3p'
}

async function lazyLoader (loaderName: LoaderName, ...args: unknown[]) {
  const timeoutId = setTimeout(function timeout () {
    // eslint-disable-next-line no-alert
    alert('Après 30s d’attente le chargement de j3p n’est toujours pas terminé, c’est probablement inutile d’attendre davantage.')
  }, 30000)

  let module
  try {
    await jQueryPromised
    switch (loaderName) {
      // on liste les cas explicitement pour dire à vite quel fichier inclure dans le bundle
      // éditeur v1
      case 'editGraphe':
        module = await import('src/editGraphe/editGraphe')
        break
      // player v1
      case 'j3pLoad':
        module = await import('src/j3pLoad')
        break
      // afficheur de parcours v1
      case 'showParcours':
        module = await import('src/editGraphe/showParcours')
        break
      // éditeur v2
      case 'spEdit':
        module = await import('src/lib/editor/spEdit')
        break
      // player v2 (qui accepte des graphe v1 ou v2)
      case 'spPlay':
        module = await import('src/lib/player/spPlay')
        break
      // afficheur de parcours v2 (qui accepte des résultats v1 ou v2)
      case 'spView':
        module = await import('src/lib/viewer/spView')
        break
      case 'spValidate':
        module = await import('src/lib/core/spValidate')
        break
      default:
        throw Error(`${loaderName} n’est pas une fonction exportée par j3p en global et ne peut être appelée directement`)
    }
  } catch (error) {
    console.error(error)
    // eslint-disable-next-line no-alert
    alert(`Le chargement ${labels[loaderName]} a échoué`)
    return
  }

  try {
    clearTimeout(timeoutId)
    const launcher = module.default
    // @ts-ignore (on vient d'affecter spLoading s'il n'existait pas)
    window.spLoading[loaderName] = 'ready'
    if (typeof launcher !== 'function') throw Error(`${loaderName} a été chargé mais ce n’est pas une fonction`)
    // on remplace le loader global mis dès le chargement de ce script par le vrai
    Object.assign(window, { [loaderName]: launcher })
    // et on peut faire l’appel voulu au départ
    // @ts-ignore (trop dynamique, vraiment pénible à typer)
    const result: unknown = launcher.apply(null, args)
    if (result instanceof Promise) result.catch(error => console.error(error))
  } catch (error) {
    console.error(error)
    const msg = error instanceof Error ? error.message : String(error)
    // eslint-disable-next-line no-alert
    alert(`Le chargement ${labels[loaderName]} a réussi mais son initialisation a provoqué une erreur : ${msg}`)
  }
}

if (window.spLoading == null) {
  // curieux, mais possible
  console.info('lazyLoader appelé sans appel préalable de loader')
  window.spLoading = {} as SpLoading
}
for (const loader of Object.keys(labels)) {
  const loaderName = loader as LoaderName
  if (typeof window[loaderName] !== 'function' || window.spLoading[loaderName] === 'preload') {
    window[loaderName] = (...args: unknown[]) => lazyLoader(loaderName, ...args)
    window.spLoading[loaderName] = 'lazy'
  } else {
    console.error(Error(`${loaderName} était déjà déclaré en global, avec le loadState ${window.spLoading[loaderName]}`))
  }
}

// on lance tout de suite le chargement de jQuery pour gagner du temps au 1er appel d’une fct, quasiment toutes en ont besoin
// FIXME trouver pourquoi si on le fait pas avant ici le chargement dans loadJqueryDialog plante (pas en test local, seulement au build)
/** @private */
const jQueryPromised = import('jquery')
  .then(({ default: jQuery }) => {
    Object.assign(globalThis, { jQuery })
  })
  .catch((error) => {
    console.error(error)
    // eslint-disable-next-line no-alert
    alert(`Le chargement de jQuery a échoué : ${error.message}`)
  })
