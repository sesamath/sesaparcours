import { j3pGetRandomInt } from 'src/legacy/core/functions'
import SectionContext from 'src/lib/entities/SectionContext'

import type { PartialResult, SectionParameters } from 'src/lib/types'
/**
 * Une section V2 type
 * Avec un énoncé comprenant du latex static et un input mathlive.
 *
 */

/**
 * La liste des paramètres de la section (ce qui est modifiable dans l’édition du nœud)
 */
export const parameters: SectionParameters = {
  title: {
    type: 'string',
    defaultValue: 'Exemple 2'
  }
  // pas paramétrable, c’est juste un exemple dont les paramètres sont fixés en dur dans start.form.ts
  /*  nbQuestions: {
   type: 'number',
   defaultValue: 5,
   help: 'Cet section produit 5 questions par défaut (10 maxi)',
   min: 1,
   max: 10
   },
   nbTries: {
   type: 'number',
   defaultValue: 2,
   help: 'Le nombre de tentatives',
   min: 1,
   max: 2
   }
   */
}

/**
 * La liste des évaluations possibles (pour une section qualitative), ex pe
 */
export const evaluations = {
  erreurClassique: 'Ne pas confondre $a^n\\textcolor{red}{\\mathbf{+}}a^n$ et $a^n\\textcolor{red}{\\mathbf{×}}a^n$.',
  pasSimplife: 'La réponse est correcte, mais ce n’est pas la forme demandée.',
  faux: 'Ce n’est pas la bonne réponse.',
  parfait: 'parfait !',
  bien: 'Exercice réussi.'
}

/**
 * La fonction qui initialise les données de la section choisie aléatoirement dans cet exemple
 */
export async function init ({ pathway, storage }: SectionContext): Promise<void> {
  const exposants: number[] = []
  do {
    const int = j3pGetRandomInt(2, 20)
    if (!exposants.includes(int)) exposants.push(int)
  } while (exposants.length < 5)
  Object.assign(storage, { exposants })
  pathway.logIfDebug(`Dans init  : storage =${exposants.join(' ; ')}`)
}

/**
 * La fonction qui crée l’énoncé à l’étape courante. Appelée autant de fois qu’il le faut par le player.
 * @param {SectionContext} sectionContext
 */
export async function enonce ({
  pathway,
  playground,
  question,
  storage
}: SectionContext): Promise<void> {
  const exposants = storage.exposants as number[]
  // on va chercher l’exposant pour la question courante.

  const n = exposants[question - 1] ?? 5 // Je mets une valeur par défaut sinon ts râle pour un n possiblement
  // undefined.
  // On met à blanc la zone de travail
  playground.reset({ work: true })
  // on écrit l’énoncé avec du latex 'statique' entre $ $
  await playground.displayWork(`Il faut écrire $A=x^{${n}}+x^{${n}}$ sous la forme la plus simple possible.\nAppuyer sur la touche entrée pour valider toutes les étapes, puis sur le bouton OK après la réponse finale pour valider.\n`, {}, // il n’y a pas d’input à paramétrer.
    { persistent: true } // Ce display restera en cas de reset de la zone de travail avec l’option butPersistent à
    // true.
  )
  pathway.logIfDebug(`Dans énoncé  : storage =${exposants.join(' ; ')}`)

  // On ajoute à l’énoncé un input mathlive. L’input mathlive est un conteneur d’inputs : les items récupérables sont
  // dans sa propriété value.
  await playground.displayWork('A=%{reponse}', { // Les paramètres de l’input 'reponse'
    reponse: {
      type: 'mathlive', // le type 'mathlive' prend un string Latex dans lequel les %{variable} sont autant
      // de 'placeholers', c’est à dire des boites de saisies dans l’input et chacune est
      // associée à un 'item'. Ici, il y a un item du nom de 'formule'.
      value: ''
    }
  }, { persistent: false } // Le display a l’option *persistent* à false, ce qui veut dire qu’il disparait au reset
    // de la zone de travail, même si l’option *butPersistent* est true.
  )
  playground.setFocus('reponse')
}

/**
 * La fonction qui corrige. Elle est appelée par le player lorsque l’utilisateur clique sur le bouton 'OK'.
 * @param {SectionContext} context
 * @param {Playground} [context.playground] // le playground (objet qui permet l’interface avec les éléments affichés).
 * @param {number} [context.nbTries] // le n° de l’essai courant
 * @param {number} [context.question] // l’indeice de la question courante
 */
export async function check ({
  pathway,
  storage,
  playground,
  nbTries,
  question
}: SectionContext): Promise<PartialResult> {
  const exposants = storage.exposants as number[]
  pathway.logIfDebug(`Dans check  : storage =${exposants.join(' ; ')}`)
  // On interroge la methode getItemValue() avec le nom de l’item voulu.
  const saisie = await playground.getItemValue('reponse') as string
  const n: number = exposants[question - 1] ?? 5

  // Une fonction que j’ai bricolée, et qui devrait se développer afin de couvrir les cas les plus courants de
  // correction automatique. Un chantier entamé début octobre : utilise le compute-engine de cortex-js qui est en
  // pleine évolution permanente
  if (saisie === `2x^{${n}}` || saisie === `2x^${n}` || saisie === `2\\times x^${n}`) { // la réponse attendue
    await playground.setFeedbackItem('reponse', true, '🙂')
    return {
      ok: true, feedBackMessage: evaluations.parfait, partialScore: 1, evaluation: evaluations.parfait
    }
  }
  if (saisie === `x^{${2 * n}}`) { // La faute qu’on attendait
    await playground.setFeedbackItem('reponse', false, '😕')

    return { // Pas de reset la première fois. On laisse l’utilisateur éditer l’input mathlive pour corriger.
      ok: false,
      feedBackMessage: evaluations.erreurClassique,
      partialScore: 0,
      evaluation: nbTries === 1 ? 'erreurClassique' : 'faux'
    }
  }
  await playground.setFeedbackItem('reponse', false, '😣')
  return {
    ok: false, feedBackMessage: evaluations.faux, partialScore: 0, evaluation: evaluations.faux
  }
}

/**
 * La fonction appelée une seule fois si tous les essais ont été tentés où si l’élève a bien répondu
 * @param {SectionContext} context
 * @param {Record<string, unknown>} context.storage
 * @param {Playground} context.playground
 * @param {number} context.question
 */
export async function solution ({ storage, playground, question }: SectionContext): Promise<void> {
  const exposants = storage.exposants as number[]
  const n = exposants[question - 1]
  const corrige = `$x^{${n}}+x^{${n}}=2x^{${n}}$`
  await playground.displaySolution(`La correction de la question ${question} pour l’exposant ${n}`, {}, {})
  await playground.displaySolution(corrige, {}, {})
}
