import { j3pGetRandomInt } from 'src/legacy/core/functions'
import textes from 'src/lib/core/textes' // à mettre au début du module ce sont les textes standardisés
import SectionContext from 'src/lib/entities/SectionContext'
import type { PartialResult, SectionParameters } from 'src/lib/types'

/**
 * Une section V2 type
 */
const { smile, anger, confused, cFaux, cBien, essaieEncore } = textes

/**
 * La liste des paramètres de la section (ce qui est modifiable dans l’édition du nœud)
 */
export const parameters: SectionParameters = {
  title: {
    type: 'string',
    defaultValue: 'compléments à 100'
  },
  nbQuestions: {
    type: 'number',
    defaultValue: 5,
    help: 'Cette section produit 5 questions par défaut (10 maxi)',
    min: 1,
    max: 10
  },
  maxTries: {
    type: 'number',
    defaultValue: 2,
    help: 'Le nombre de tentatives',
    min: 1,
    max: 2
  },
  formeExplication: {
    type: 'number',
    defaultValue: 20,
    help: 'L’explication peut être donnée',
    min: 1,
    max: 50
  },
  limite: {
    type: 'number',
    defaultValue: 10,
    help: 'Le délai pour répondre à une question',
    min: 1,
    max: 60
  },
  suivant: {
    type: 'boolean',
    defaultValue: false,
    help: 'Si true alors il faut cliquer sur question suivante, sinon, c’est automatique'
  }
}

/**
 * La liste des évaluations possibles (pour une section qualitative), ex pe
 */
export const evaluations = {}

export async function init ({ storage, params, nbQuestions }: SectionContext): Promise<void> {
  const complements: number[] = []
  let max: number = Number(params.maxComplement)
  if (max < nbQuestions) max = nbQuestions + 5 // La valeur max du complément doit permettre d’avoir assez de valeurs différentes sinon, cela provoquerait une boucle infinie ci-dessous.
  do {
    const int = j3pGetRandomInt(1, max)
    if (!complements.includes(int)) complements.push(int) // On vérifie que cet entier ne fait pas partie de la liste
  } while (complements.length < nbQuestions) // On boucle jusqu'à avoir le nombre de valeurs souhaitées
  Object.assign(storage, { complements }) // On stocke les valeurs des compléments qui seront demandés dans storage.
}

export async function enonce ({ storage, playground, question, nbTries }: SectionContext): Promise<void> {
  const complements = storage.complements as number[]
  const complement = complements[question - 1] ?? 10
  if (nbTries < 2) {
    await playground.displayWork(`Quel nombre faut-il ajouter à ${100 - complement} pour faire 100 ? %{reponse}`,
      {
        reponse: {
          type: 'mathlive',
          value: ''
        }
      }
    )
  }
}

export async function check ({ storage, playground, question, nbTries }: SectionContext): Promise<PartialResult> {
  const complements = storage.complements as number[]
  const complement = complements[question - 1]
  const saisie = await playground.getItemValue('reponse')
  if (Number(saisie) === complement) {
    await playground.setFeedbackItem('reponse', true, smile)
    return {
      feedBackMessage: cBien, ok: true, partialScore: 1//, evaluation: null
    }
  } else {
    if (nbTries < 2) {
      await playground.setFeedbackItem('reponse', false, confused)
      return {
        feedBackMessage: essaieEncore, ok: false, partialScore: 0
      }
    } else {
      await playground.setFeedbackItem('reponse', false, anger)
      return {
        feedBackMessage: cFaux, ok: false, partialScore: 0
      }
    }
  }
}

/**
 * La fonction appelée une seule fois si tous les essais ont été tentés où si l’élève a bien répondu
 * @param {SectionContext} context
 * @param {Playground} context.playground
 * @param {number} context.question
 */
export async function solution ({ storage, playground, question }: SectionContext): Promise<void> {
  const complements = storage.complements as number[]
  const complement = complements[question - 1] ?? 10
  await playground.displaySolution(`$${100 - complement}+\\mathbf{${complement}}=100$ car $100-${100 - complement}=\\mathbf{${complement}}$`, {}, {})
}
