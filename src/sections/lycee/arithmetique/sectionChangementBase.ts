import { j3pGetRandomInt, j3pGetRandomElt } from 'src/legacy/core/functions'
import textes from 'src/lib/core/textes'
import type { ParamsValues } from 'src/lib/entities/GraphNode' // à mettre au début du module ce sont les textes standardisés
import SectionContext from 'src/lib/entities/SectionContext'
import type { PartialResult, SectionParameters, PlainObject } from 'src/lib/types'

const { smile, anger, confused, cFaux, cBien, essaieEncore } = textes

/**
 * @description
 * Section changementBase
 *
 * Section destinée à tester le changement de base entre les bases 2, 10 et 16
 */

// on type notre storage
interface StorageChangementBase extends PlainObject {
  baseInit: number
  baseFin: number
  listeNombres: number[]
  premierPassage: boolean[]
}

interface SectionContextChangementBase extends SectionContext {
  storage: StorageChangementBase
}

// @todo ajouter la base 8, l'octal ça sert encore ;-)
/** Les bases gérées ici */
const knownBases = [2, 10, 16]

/**
 * La liste des paramètres de la section (ce qui est modifiable dans l’édition du nœud)
 */
export const parameters: SectionParameters = {
  title: {
    type: 'string',
    defaultValue: 'Changement de base'
  },
  nbQuestions: {
    type: 'number',
    defaultValue: 5,
    help: 'Cette section produit 5 questions par défaut (10 maximum)',
    min: 1,
    max: 10
  },
  baseInit: {
    type: 'integer',
    defaultValue: 10,
    controlledValues: knownBases,
    help: 'base dans laquelle le nombre de départ est écrit (choix entre 2, 10 et 16)',
    validate: (value: number, values: ParamsValues) => {
      if (values.baseFin === value) return { errors: ['La base du nombre de départ doit être différente de celle vers laquelle le convertir'] }
      return { ok: true }
    }
  },
  baseFin: {
    type: 'integer',
    defaultValue: 2,
    controlledValues: knownBases,
    help: 'base dans laquelle le nombre sera converti (choix entre 2, 10 et 16, différente de baseInit)',
    validate: (value: number, values: ParamsValues) => {
      if (values.baseInit === value) return { ok: false, errors: ['La base laquelle convertir doit être différente de celle du nombre de départ'], warnings: [] }
      return { ok: true, errors: [], warnings: [] }
    }
  },
  maxTries: {
    type: 'integer',
    defaultValue: 2,
    help: 'Le nombre de tentatives',
    min: 1,
    max: 5
  },
  formeExplication: {
    type: 'string',
    defaultValue: 'divisions',
    controlledValues: ['divisions', 'extraction'],
    help: 'L’explication peut être donnée par des divisions successives (par défaut) ou par l’extraction des plus grandes puissances de la base souhaitée'
  },
  limite: {
    type: 'integer',
    defaultValue: 0,
    help: 'Le délai (en secondes) pour répondre à une question',
    min: 1,
    max: 600
  },
  suivant: {
    type: 'boolean',
    defaultValue: false,
    help: 'Si true alors il faut cliquer sur question suivante, sinon, c’est automatique'
  }
}

/**
 * La liste des évaluations possibles (pour une section qualitative), ex pe
 * Ici il n'y en a pas
 */
// export const evaluations = {}

/**
 * fonction de validation globale des paramètres, appelée par l’éditeur
 */
export async function validateParameters (params: ParamsValues) : Promise<Record<string, string>> {
  const { baseInit, baseFin } = params
  if (baseInit === baseFin) return { baseFin: 'La base dans laquelle on doit convertir doit être différente de la base de départ' }
  return { }
}

const textesSection = {
  consigne1: 'Le nombre %{nb1} est écrit en base %{b1}.',
  question: 'Saisir sa conversion en base %{b2} %{reponse}.',
  consigne2: 'Des étapes intermédiaires permettent d’effectuer pas à pas cette conversion.',
  corr1_1: 'On effectue les divisions successives par %{baseFin}.',
  corr1_2: 'On extrait successivement les plus grandes puissances possibles de %{baseFin}.',
  corr1_3: 'On ajoute les puissances successives de %{baseInit}.',
  corr3: 'Donc le nombre %{nb} écrit en base %{baseInit} est converti sous la forme %{rep} en base %{baseFin}.',
  corr1_4: 'La conversion s’effectue par paquets de 4 bits (en partant de la droite).',
  corr2_1: 'On prend alors les restes successifs dans l’ordre inverse.',
  corr2_2: 'On prend alors les restes successifs dans l’ordre inverse (et on les convertit en base 16).',
  corr2_3: 'Les facteurs des différentes puissances de %{baseFin} correspondent à la conversion recherchée.',
  corr2_4: 'Les facteurs des différentes puissances de %{baseFin} (convertis en base 16) correspondent à la conversion recherchée.',
  corr2_5: '%{nbBase2} donne %{nbBase10} en base 10, ce qui s’écrit %{conv16} en base 16',
  corr1_5: 'Chaque caractère en base 16 se convertit en base 2 sur 4 bits.',
  corr2_6: '%{nbBase16} écrit en base 16 donne %{nbBase10} en base 10, soit %{nbBase2} en base2'
}

export async function init ({ storage, params, nbQuestions, pathway }: SectionContextChangementBase): Promise<void> {
  // le as pour que ts les prenne comme number (si c'est pas le cas ça va planter avec le test d’include qui suit)
  const { baseInit, baseFin } = params as StorageChangementBase
  if (baseInit === baseFin) throw Error('La base dans laquelle on doit convertir doit être différente de la base de départ')
  if (!knownBases.includes(baseInit)) throw Error(`Base de départ non gérée (${baseInit})`)
  if (!knownBases.includes(baseFin)) throw Error(`Base vers laquelle convertir non gérée (${baseFin})`)
  // Maintenant les différents nombres à convertir
  const listeNombres: number[] = []
  do {
    if (((baseInit === 10) && (baseFin === 2)) || ((baseInit === 2) && (baseFin === 10))) {
      // bases 2 & 10
      const nb = j3pGetRandomInt(65, 254)
      if (listeNombres.includes(nb)) continue
      // on le garde tant qu'il a plus de deux 1 en base 2
      const convNbBase2 = nb.toString(2)
      if (convNbBase2.replace(/0/, '').length > 2) listeNombres.push(nb)
    } else if (((baseInit === 10) && (baseFin === 16)) || ((baseInit === 16) && (baseFin === 10))) {
      // bases 10 et 16
      const nb = j3pGetRandomInt(257, 1200)
      if (!listeNombres.includes(nb)) listeNombres.push(nb)
    } else {
      // bases 2 et 16
      let nbBase2 = ''
      do {
        for (let i = 1; i <= 3; i++) nbBase2 += j3pGetRandomElt(['0', '1'])
      } while (nbBase2 === '000')
      for (let i = 4; i <= 10; i++) nbBase2 += j3pGetRandomElt(['0', '1'])
      // et au moins 5 0 en tout
      if (nbBase2.replace(/0/, '').length < 5) continue
      const nb = Number.parseInt(nbBase2, 2)
      if (!listeNombres.includes(nb)) listeNombres.push(nb)
    }
  } while (listeNombres.length < nbQuestions)
  pathway.logIfDebug('listeNombres:', listeNombres)
  // On stocke les valeurs dans storage
  storage.listeNombres = listeNombres
  storage.baseInit = baseInit
  storage.baseFin = baseFin
  const premierPassage:boolean[] = []
  if (params.nbQuestions == null) throw Error('nbQuestions est un paramètre à renseigner')
  for (let i = 0; i < params.nbQuestions; i++) premierPassage.push(true)
  storage.premierPassage = premierPassage
  // console.log('Fin de init')
}

export async function enonce ({ storage, playground, question, nbTries, maxTries }: SectionContextChangementBase): Promise<void> {
  const { baseInit, baseFin, listeNombres, premierPassage } = storage
  if (premierPassage[question - 1]) {
    const nb = listeNombres[question - 1]
    if (nb == null) throw Error('Le nombre est invalide')
    const nb1 = baseInit === 10
      ? String(nb)
      : (baseInit === 2)
          ? nb.toString(2)
          : nb.toString(16).toUpperCase()
    const restriction = baseFin === 2
      ? /[01]/
      : baseFin === 10
        ? /[0-9]/
        : /[0-9A-Fa-f]/
    const size = baseFin === 2
      ? 10
      : baseFin === 10
        ? 4
        : 3
    if (nbTries < maxTries) {
      await playground.displayWork(textesSection.consigne1, { nb1, b1: baseInit })
      await playground.displayWork(textesSection.question, {
        b2: baseFin,
        reponse: { type: 'input', inputProps: { size }, restriction }
      }, { asBlock: true })
    }
    premierPassage[question - 1] = false
  }
}

export async function check ({ storage, playground, nbTries, maxTries, question }: SectionContextChangementBase): Promise<PartialResult> {
  const saisie = await playground.getItemValue('reponse')
  const nb = storage.listeNombres[question - 1]
  if (nb == null) throw Error('Le nombre est invalide')
  const rep = nb.toString(storage.baseFin)
  if (saisie === rep) {
    await playground.setFeedbackItem('reponse', true, smile)
    return {
      feedBackMessage: cBien, ok: true, partialScore: 1//, evaluation: null
    }
  } else {
    if (nbTries < maxTries) {
      await playground.setFeedbackItem('reponse', false, confused)
      return {
        feedBackMessage: essaieEncore, ok: false, partialScore: 0
      }
    } else {
      await playground.setFeedbackItem('reponse', false, anger)
      return {
        feedBackMessage: cFaux, ok: false, partialScore: 0
      }
    }
  }
}

/**
 * La fonction appelée une seule fois si tous les essais ont été tentés où si l’élève a bien répondu
 * @param {SectionContext} context
 * @param {Playground} context.playground
 * @param {number} context.question
 */
export async function solution ({ storage, playground, question, params }: SectionContextChangementBase): Promise<void> {
  const { baseInit, baseFin, listeNombres } = storage
  const { formeExplication } = params
  let phrase = ''
  const nb = listeNombres[question - 1]
  if (nb == null) throw Error('Le nombre est invalide')
  if (baseInit === 10) {
    let a = nb
    if (formeExplication === 'divisions') {
      await playground.displaySolution(textesSection.corr1_1, { baseFin }, { asBlock: true })
      let q = Math.floor(a / baseFin)
      let r = a - baseFin * q
      phrase = `$$ ${a}=${q}\\times ${baseFin}+${r} $$`
      while (q > 0) {
        a = q
        q = Math.floor(a / baseFin)
        r = a - baseFin * q
        phrase += `$$ ${a}=${q}\\times ${baseFin}+${r} $$`
      }
    } else {
      await playground.displaySolution(textesSection.corr1_2, { baseFin }, { asBlock: true })
      const nbConverti = nb.toString(baseFin)
      if (nbConverti == null) throw Error(`Souci avec ${a} ou ${baseFin}`)
      let pos = 0
      let conversion = ''
      let reste = nb
      while (pos < nbConverti.length) {
        const chiffre = nbConverti[pos]
        if (chiffre !== '0') {
          phrase += `$$ ${nb}=`
          if (chiffre == null) throw Error(`L’élément en position ${pos} de ${nbConverti} n’existe pas`)
          conversion += ((conversion === '') ? '' : '+') + Number.parseInt(chiffre, baseFin) + `\\times ${baseFin}^{${nbConverti.length - 1 - pos}}`
          reste -= Number.parseInt(chiffre, baseFin) * Math.pow(baseFin, nbConverti.length - 1 - pos)
          phrase += conversion + ((reste === 0) ? '$$' : `+${reste} $$`)
        }
        pos++
      }
    }
    await playground.displaySolution(phrase, {}, {})
    if (formeExplication === 'divisions') {
      await playground.displaySolution((baseFin === 2) ? textesSection.corr2_1 : textesSection.corr2_2, {}, { asBlock: true })
    } else {
      await playground.displaySolution((baseFin === 2) ? textesSection.corr2_3 : textesSection.corr2_4, { baseFin }, { asBlock: true })
    }
  } else {
    if (baseFin === 10) {
      await playground.displaySolution(textesSection.corr1_3, { baseInit }, { asBlock: true })
      phrase = '$$'
      const nbBase2 = nb.toString(2)
      let pos = 0
      for (const elt of nbBase2) {
        phrase += ((phrase === '$$') ? '' : '+') + elt + `\\times 2^{${nbBase2.length - pos}}$$`
        pos++
      }
      await playground.displaySolution(phrase, {}, {})
    } else if (baseInit === 2) {
      await playground.displaySolution(textesSection.corr1_4, { }, { asBlock: true })
      // baseFin vaut 16
      let nbBase2Init = nb.toString(2)
      let nbBase2, nbBase10, conv16
      while (nbBase2Init.length > 0) {
        nbBase2 = nbBase2Init.slice(-4)
        nbBase10 = Number.parseInt(nbBase2, 2)
        conv16 = nbBase10.toString(16).toUpperCase()
        nbBase2Init = nbBase2Init.slice(0, -4)
        await playground.displaySolution(textesSection.corr2_5, { nbBase2, nbBase10, conv16 }, { asBlock: true })
      }
    } else {
      // baseInit vaut 16 et baseFin vaut 2
      await playground.displaySolution(textesSection.corr1_5, { }, { asBlock: true })
      const nbBase16Init = nb.toString(16).toUpperCase()
      for (const nbBase16 of nbBase16Init) {
        const nbBase10 = Number.parseInt(nbBase16, 16)
        const nbBase2 = nbBase10.toString(2)
        await playground.displaySolution(textesSection.corr2_6, { nbBase2, nbBase10, nbBase16 }, { asBlock: true })
      }
    }
  }
  const rep = (baseFin !== 10)
    ? nb.toString(baseFin).toUpperCase()
    : nb
  await playground.displaySolution(textesSection.corr3, { nb, baseInit, baseFin, rep }, { asBlock: true })
}
