import { formatCheckResult } from 'src/lib/core/checks'
import textes from 'src/lib/core/textes'
import SectionContext from 'src/lib/entities/SectionContext'

import type { CheckResults, PartialResult, SectionParameters } from 'src/lib/types'

const { smile, anger, confused } = textes

/**
 * Une section V2 type
 */
// l’editeur pour la figure MTG32 (on utilise initEditor pour passer un string comme argument)
async function initEditor (container: HTMLElement, fig: string) {
  const { default: editor } = await import('../legacy/sections/outils/mtg32/mtgEditor')
  await editor(container, { fig })
}

/**
 * La liste des paramètres de la section (ce qui est modifiable dans l’édition du nœud)
 */
export const parameters: SectionParameters = {
  title: {
    type: 'string',
    defaultValue: 'Exemple 1'
  },
  nbQuestions: {
    type: 'number',
    defaultValue: 3,
    help: 'Ne pas mettre plus de questions qu’il n’y a de choix car sinon il y aura des doublons',
    min: 1,
    max: 4
  },
  unCarre: {
    type: 'number',
    defaultValue: 49,
    // exemple idiot (on demanderait plutôt l’entier que son carré), juste pour avoir un validate simple
    help: 'Un carré parfait entre 4 et 100 (inclus)',
    min: 4,
    max: 100,
    validate: async (value: number) => {
      // le core a déjà vérifié que c'était un integer entre 4 et 100 (mais ts comprend pas que value est un number, on ajoute le test…)
      if (typeof value !== 'number' || !Number.isInteger(Math.sqrt(value))) return Promise.resolve(formatCheckResult({ errors: ['unCarre doit être le carré d’un entier compris entre 2 et 10'] }))
      return Promise.resolve(formatCheckResult({ ok: true }))
    }
  },
  desCarres: {
    type: 'integer',
    defaultValue: [4, 9, 16, 64, 25],
    help: 'Une liste de carrés parfaits inférieurs à 100',
    controlledValues: [4, 9, 16, 25, 36, 49, 64, 81],
    multiple: true,
    // validate est inutile si les valeurs sont saisies via le formulaire, car les valeurs du formulaire sont les controlledValues et on ne peut pas en ajouter
    // Mais validate servira si les paramètres sont saisis en mode texte, ou passés via une URL, ou…
    validate: async (values: Array<unknown>): Promise<CheckResults> => {
      if (!Array.isArray(values)) {
        values = [values]
      }
      for (const value of values) {
        const val = Number(value)
        if (!Number.isInteger(Math.sqrt(val)) || val < 4 || val > 100) {
          return Promise.resolve(formatCheckResult({ errors: [`desCarres doit être une liste de carrés d’entiers compris entre 2 et 10, ce qui n’est pas le cas de ${value}`] }))
        }
      }
      return Promise.resolve(formatCheckResult({ ok: true }))
    }
  },
  quadrilateres: {
    type: 'string',
    defaultValue: ['parallélogramme'],
    help: 'Une liste de quadrilatères particuliers',
    controlledValues: ['carré', 'losange', 'rectangle', 'parallélogramme', 'trapèze'],
    multiple: true,
    validate: async (values: Array<string>) => {
      if (!Array.isArray(values)) {
        values = [values]
      }
      for (const value of values) {
        if (!['carré', 'losange', 'rectangle', 'parallélogramme', 'trapèze'].includes(String(value))) return Promise.resolve(formatCheckResult({ errors: ['quadrilateres doit contenir uniquement des quadrilatères particuliers'] }))
      }
      return Promise.resolve(formatCheckResult({ ok: true }))
    }
  },
  listeDeNombres: {
    type: 'number',
    defaultValue: [],
    help: 'saisissez ce que vous voulez',
    multiple: true
  },
  listeDeMots: {
    type: 'string',
    defaultValue: [],
    help: 'saisissez ce que vous voulez',
    multiple: true
  },
  listeDeBooleans: {
    type: 'boolean',
    defaultValue: [],
    help: 'Une liste de checkbox pour avoir une liste de valeur vrai/faux (cochée = vrai)',
    multiple: true
  },
  figure: {
    type: 'string',
    help: 'Une figure MG32 en codebase64',
    editor: initEditor,
    defaultValue: 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAALvAAACWAAAAQEAAAAAAAAAAQAAAC7#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAuAxJul41QQC4DEm6XjVD#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA+AxJul41QAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAFPAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBn0AAAAAAAQHIz1wo9cKQAAAACAP####8BAAAAABAAAUkAAAAAAAAAAABACAAAAAAAAAAABQABQHhIAAAAAABAcpPXCj1wpP####8AAAABABFDUG9pbnRQYXJBYnNjaXNzZQD#####AAAAAAAQAAFBAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACAAAAAkAAAABAAAAAAAAAAAAAAAJAP####8AAAAAABAAAUIAAAAAAAAAAABACAAAAAAAAAAABQAAAAAIAAAACQAAAAE#8AAAAAAAAAAAAAIA#####wD#AAAAEAABTQAAAAAAAAAAAEAIAAAAAAAAAAABAAFAcjgAAAAAAEBpp64UeuFI#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAoAAAAM#####wAAAAEAC0NNZWRpYXRyaWNlAAAAAA0BAAAAABAAAAEAAAABAAAACgAAAAwAAAAGAAAAAA0BAAAAABAAAAEAAAUAAAAACgAAAAz#####AAAAAgAJQ0NlcmNsZU9SAAAAAA0BAAAAAAAAAQAAAA8AAAABQDAAAAAAAAAB#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUAAAAADQAAAA4AAAAQ#####wAAAAEAEENQb2ludExpZUJpcG9pbnQAAAAADQEAAAAAEAAAAQAABQABAAAAEQAAAAgBAAAADQAAAAoAAAAM#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAANAQAAAAEAAAAAABIRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAEwAAAAoA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAADAAAAAsAAAALAAAAABUBAAAAABAAAAEAAAABAAAADAAAAAsAAAAGAAAAABUBAAAAABAAAAEAAAUAAAAADAAAAAsAAAAMAAAAABUBAAAAAAAAAQAAABcAAAABQDAAAAAAAAABAAAADQAAAAAVAAAAFgAAABgAAAAOAAAAABUBAAAAABAAAAEAAAUAAQAAABkAAAAIAQAAABUAAAAMAAAACwAAAA8BAAAAFQEAAAABAAAAAAAaEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAABsAAAAKAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAoAAAALAAAACwAAAAAdAQAAAAAQAAABAAAAAQAAAAoAAAALAAAABgAAAAAdAQAAAAAQAAABAAAFAAAAAAoAAAALAAAADAAAAAAdAQAAAAAAAAEAAAAfAAAAAUAwAAAAAAAAAQAAAA0AAAAAHQAAAB4AAAAgAAAADgAAAAAdAQAAAAAQAAABAAAFAAEAAAAhAAAACAEAAAAdAAAACgAAAAsAAAAPAQAAAB0BAAAAAQAAAAAAIhEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAjAAAABQD#####AAAAAAAQAAABAAAAAQAAAAoAAAALAAAACgD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAAALAAAACgAAAAz#####AAAAAQAMQ0Jpc3NlY3RyaWNlAAAAACYBAAAAABAAAAEAAAABAAAACwAAAAoAAAAMAAAABAAAAAAmAQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAACf#####AAAAAQAXQ01lc3VyZUFuZ2xlR2VvbWV0cmlxdWUBAAAAJgAAAAsAAAAKAAAADP####8AAAACABdDTWFycXVlQW5nbGVHZW9tZXRyaXF1ZQEAAAAmAf8AAAAAAAEAAAABQEUGF39UkbsAAAALAAAACgAAAAwAAAAPAQAAACYB#wAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAACgPAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAALCsAAAAAAp#####wAAAAEAB0NDYWxjdWwA#####wAFZWdUcmkAEWFicyhBTStNQi1BQik8MC4x#####wAAAAEACkNPcGVyYXRpb24E#####wAAAAIACUNGb25jdGlvbgAAAAAUAQAAABQA#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAABMAAAAWAAAAGwAAABYAAAAjAAAAAT+5mZmZmZmaAAAAEwD#####AANiYW0ACGludChCQU0pAAAAFQIAAAAWAAAAKQAAAAf##########w=='
  }
}

/**
 * La liste des évaluations possibles (pour une section qualitative), ex pe
 */
export const evaluations = {
  pe_1: 'Une erreur type', parfait: 'Tout est parfait', bien: 'Exercice réussi'
}

export async function init ({ pathway, storage }: SectionContext): Promise<void> {
  pathway.logIfDebug('init avec pathway : ', pathway, 'storage : ', storage)
  storage.quads = ['carré', 'losange', 'parallèlogramme', 'rectangle', 'trapèze']
}

export async function enonce (sectionContext: SectionContext): Promise<void> {
  const { pathway, playground, question, storage } = sectionContext
  pathway.logIfDebug('enonce avec', pathway)
  let itemStart = ''
  switch (question) {
    case 3:
      await playground.displayWork('Placer le point M sur le segment [AB].%{figureMtg32}', {
        figureMtg32: {
          type: 'mathgraph',
          value: '%{egTri}%{bam}',
          persistent: false,
          name: 'MSurAB',
          mtgOptions: {
            fig: 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAALvAAACWAAAAQEAAAAAAAAAAQAAAC7#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAuAxJul41QQC4DEm6XjVD#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA+AxJul41QAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAFPAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBn0AAAAAAAQHIz1wo9cKQAAAACAP####8BAAAAABAAAUkAAAAAAAAAAABACAAAAAAAAAAABQABQHhIAAAAAABAcpPXCj1wpP####8AAAABABFDUG9pbnRQYXJBYnNjaXNzZQD#####AAAAAAAQAAFBAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACAAAAAkAAAABAAAAAAAAAAAAAAAJAP####8AAAAAABAAAUIAAAAAAAAAAABACAAAAAAAAAAABQAAAAAIAAAACQAAAAE#8AAAAAAAAAAAAAIA#####wD#AAAAEAABTQAAAAAAAAAAAEAIAAAAAAAAAAABAAFAcjgAAAAAAEBpp64UeuFI#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAoAAAAM#####wAAAAEAC0NNZWRpYXRyaWNlAAAAAA0BAAAAABAAAAEAAAABAAAACgAAAAwAAAAGAAAAAA0BAAAAABAAAAEAAAUAAAAACgAAAAz#####AAAAAgAJQ0NlcmNsZU9SAAAAAA0BAAAAAAAAAQAAAA8AAAABQDAAAAAAAAAB#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUAAAAADQAAAA4AAAAQ#####wAAAAEAEENQb2ludExpZUJpcG9pbnQAAAAADQEAAAAAEAAAAQAABQABAAAAEQAAAAgBAAAADQAAAAoAAAAM#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAANAQAAAAEAAAAAABIRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAEwAAAAoA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAADAAAAAsAAAALAAAAABUBAAAAABAAAAEAAAABAAAADAAAAAsAAAAGAAAAABUBAAAAABAAAAEAAAUAAAAADAAAAAsAAAAMAAAAABUBAAAAAAAAAQAAABcAAAABQDAAAAAAAAABAAAADQAAAAAVAAAAFgAAABgAAAAOAAAAABUBAAAAABAAAAEAAAUAAQAAABkAAAAIAQAAABUAAAAMAAAACwAAAA8BAAAAFQEAAAABAAAAAAAaEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAABsAAAAKAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAoAAAALAAAACwAAAAAdAQAAAAAQAAABAAAAAQAAAAoAAAALAAAABgAAAAAdAQAAAAAQAAABAAAFAAAAAAoAAAALAAAADAAAAAAdAQAAAAAAAAEAAAAfAAAAAUAwAAAAAAAAAQAAAA0AAAAAHQAAAB4AAAAgAAAADgAAAAAdAQAAAAAQAAABAAAFAAEAAAAhAAAACAEAAAAdAAAACgAAAAsAAAAPAQAAAB0BAAAAAQAAAAAAIhEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAjAAAABQD#####AAAAAAAQAAABAAAAAQAAAAoAAAALAAAACgD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAAALAAAACgAAAAz#####AAAAAQAMQ0Jpc3NlY3RyaWNlAAAAACYBAAAAABAAAAEAAAABAAAACwAAAAoAAAAMAAAABAAAAAAmAQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAACf#####AAAAAQAXQ01lc3VyZUFuZ2xlR2VvbWV0cmlxdWUBAAAAJgAAAAsAAAAKAAAADP####8AAAACABdDTWFycXVlQW5nbGVHZW9tZXRyaXF1ZQEAAAAmAf8AAAAAAAEAAAABQEUGF39UkbsAAAALAAAACgAAAAwAAAAPAQAAACYB#wAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAACgPAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAALCsAAAAAAp#####wAAAAEAB0NDYWxjdWwA#####wAFZWdUcmkAEWFicyhBTStNQi1BQik8MC4x#####wAAAAEACkNPcGVyYXRpb24E#####wAAAAIACUNGb25jdGlvbgAAAAAUAQAAABQA#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAABMAAAAWAAAAGwAAABYAAAAjAAAAAT+5mZmZmZmaAAAAEwD#####AANiYW0ACGludChCQU0pAAAAFQIAAAAWAAAAKQAAAAf##########w==',
            isEditable: false,
            loadApi: true
          },
          svgOptions: {
            width: '600', height: '400' // si on ne précise pas l’idSvg, il sera affecté avec le nom de la
            // propriété (ici figureMtg32)
          },
          inputProps: {
            egTri: {
              initialValue: 5 // Problème avec les calculs booléens qui n’ont que deux valeurs 0 ou 1 :
              // comment savoir si l’item est isAnswered = true... en mettant une valeur
              // improbable ici ? ou en évitant les booléens.
            },
            bam: {
              initialValue: 42
            }
          }
        }
      }, {})
      // pas d’itemStart
      break
    case 2: // input type 'latex' avec plusieurs zones de saisies
      await playground.displayWork('Quelle est la forme irréductible de $\\dfrac{24}{36}$ ? : %{fraction}', {
        fraction: {
          type: 'radios',
          choices: [{ latex: '\\frac{12}{18}', value: '12Sur18' }, {
            latex: '\\frac{6}{9}',
            value: '6Sur9'
          }, { latex: '\\frac{2}{3}', value: '2Sur3' }, {
            latex: '\\frac{18}{12}',
            value: '18Sur12'
          }, { latex: '\\frac{3}{2}', value: '3Sur2' }],
          inputProps: {
            name: 'fraction'
          }
        }
      }, {})
      itemStart = 'fraction'
      break
    case 1: // input de type checkboxes
      await playground.displayWork('Sélectionnez dans la liste suivante, les quadrilatères possédant un centre de symétrie :\n %{quads}', {
        quads: {
          type: 'checkboxes',
          choices: storage.quads as string[],
          inputProps: {
            name: 'quads'
          }
        }
      }, {})
      await playground.displayWork(`
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce condimentum suscipit tellus, a pretium nisi. Integer varius arcu sit amet nunc eleifend, vitae pretium elit eleifend. Pellentesque quis nisl odio. Etiam at lacus id justo consectetur tempor. Vivamus lacinia consectetur mauris vel vestibulum. Praesent euismod tortor a varius sagittis. Etiam tincidunt cursus velit non faucibus. Vivamus in ipsum vitae dolor mollis dignissim. Nam porta ex vel purus aliquam, vitae tristique justo lacinia. Aenean placerat vel purus in tempor. Aliquam ut dolor lorem. Sed fermentum erat metus, mollis hendrerit ipsum finibus ac.

Quisque eu mauris commodo, dapibus eros sed, tristique nibh. Quisque eleifend enim blandit nulla consectetur, quis gravida lectus commodo. Vivamus sit amet suscipit velit. Quisque imperdiet eget turpis et lacinia. In mollis libero turpis, id posuere purus efficitur sit amet. Quisque sagittis scelerisque eros sit amet ullamcorper. Maecenas nunc justo, suscipit ac ullamcorper eu, rutrum pellentesque tellus. Quisque euismod magna vitae leo varius eleifend. Cras condimentum tristique elit, ut pellentesque mi dictum rhoncus. Donec elementum varius lectus, sed ultrices nibh faucibus et. Quisque at lobortis mauris. Duis varius enim vel dapibus sodales. Vivamus vel lectus semper neque malesuada sodales. Nam malesuada lobortis lorem sit amet dapibus.

Sed sed vehicula nisi, eu sagittis purus. Donec non blandit diam. In est lorem, facilisis vel dignissim ac, dictum at metus. Nullam ac nunc metus. Duis malesuada convallis mauris eu blandit. Etiam egestas ligula fringilla efficitur congue. Suspendisse potenti. Aenean eleifend efficitur bibendum. Praesent finibus eu tortor sit amet egestas. Fusce ac pretium erat. Nullam dictum sapien dolor, eu semper sem ultrices vel. Donec sit amet libero purus. Sed a quam libero. Suspendisse id tempus felis, sit amet consectetur arcu. Proin dolor purus, ultrices elementum elit vitae, faucibus elementum neque.

Quisque tellus lectus, semper aliquet suscipit at, facilisis ac purus. Proin ullamcorper bibendum diam at scelerisque. Aliquam mollis accumsan turpis, et consectetur ex pulvinar et. Sed ligula ipsum, molestie id tincidunt ac, commodo ut neque. Vestibulum nec nisl non orci rhoncus ultricies a vel purus. Sed ac luctus nisi. In eget suscipit tortor, non convallis elit. Pellentesque maximus justo vel est tincidunt dapibus. Pellentesque libero diam, molestie sit amet velit non, posuere sagittis nulla. Vivamus in elementum ligula, sit amet aliquam odio. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed ex metus, scelerisque vitae lorem molestie, imperdiet convallis neque. Nam commodo, leo nec imperdiet sollicitudin, sem quam suscipit ipsum, id eleifend justo dui et leo. Vestibulum hendrerit arcu eget turpis hendrerit, vitae convallis neque posuere.

Aenean id elit id nisl ultricies elementum. Nullam augue erat, pharetra eu urna a, dignissim viverra mauris. Nunc vitae pulvinar leo. Vivamus in vehicula ipsum. Nullam vel ipsum ac magna laoreet sodales. Aenean nulla orci, consectetur commodo posuere et, porta ornare nibh. Fusce viverra ante id sodales volutpat. Integer pulvinar nisl eu massa faucibus, non interdum nisl placerat. Phasellus gravida nunc nulla, sed sagittis lacus luctus et. Nunc malesuada lobortis elit, eu aliquam metus accumsan lobortis.

Aliquam a rhoncus mauris. Donec volutpat lacus vitae interdum porta. Duis eu nulla auctor, vestibulum orci quis, mattis lectus. Sed commodo purus sed nulla finibus, et sollicitudin risus ultricies. Maecenas dignissim, dolor id luctus placerat, ex mi laoreet est, nec feugiat nunc quam congue tellus. Vivamus vulputate aliquam venenatis. Curabitur sed ante nec sapien euismod consectetur. Pellentesque eget turpis at urna luctus feugiat fermentum commodo nisi.

Mauris at magna nec urna dignissim auctor. Nunc quis sodales magna. Maecenas vehicula mauris at purus malesuada, vel gravida arcu laoreet. Vestibulum eleifend consequat rhoncus. Nunc ut diam vitae nulla congue ornare. Aenean id convallis urna. Quisque a dictum justo.

Aenean lacus arcu, ullamcorper ornare rhoncus sit amet, finibus sed lacus. Suspendisse potenti. Vivamus eu bibendum lectus, sed tristique enim. Quisque eu fermentum orci. Ut ut sodales erat. Pellentesque non posuere mi, eget placerat dolor. In ultricies urna accumsan, tempus risus at, imperdiet ipsum. Cras pharetra mauris sit amet massa tristique, vitae lacinia augue molestie. Nullam neque arcu, porttitor non metus id, malesuada vulputate orci. Morbi auctor volutpat lorem, quis tempus risus lobortis iaculis. Donec id erat eget dui feugiat finibus vel vel dui. Pellentesque id risus vel enim gravida feugiat. Curabitur neque mi, sodales eget tempor vitae, convallis ut lorem.

Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec efficitur nunc quis odio sodales elementum. Nulla viverra tellus eget ligula blandit gravida. Nulla facilisi. Cras hendrerit felis aliquet, luctus nisi non, scelerisque felis. Duis in pharetra est, id sagittis massa. Aliquam felis nunc, lacinia sed dictum quis, facilisis vel nibh. Pellentesque id metus non massa lacinia venenatis. Sed nunc nisi, tincidunt et pretium at, commodo nec ligula.

Sed arcu lectus, pretium eget libero non, efficitur finibus ex. Donec convallis enim sit amet libero vestibulum convallis. Nullam lobortis rhoncus consequat. Nullam varius eget arcu sed varius. Proin a sem tellus. Integer fermentum libero lacus, sed luctus mauris eleifend vitae. Nam tempus est vitae erat laoreet lobortis. Quisque dolor leo, luctus et interdum nec, suscipit eget ex. Proin metus mi, dignissim vel venenatis non, porta non purus. In sit amet finibus nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;

Donec quis est arcu. Cras non orci purus. Duis ac turpis quis tellus pulvinar euismod sit amet a orci. Vestibulum imperdiet metus at sapien auctor mollis. Nullam euismod cursus purus eu pellentesque. Vestibulum ut massa quis lectus dictum dapibus eget blandit justo. Etiam luctus orci nisi, non eleifend elit pharetra porttitor.

Maecenas tincidunt et elit nec porttitor. Pellentesque gravida in neque sit amet pellentesque. Quisque vel quam nunc. Vestibulum varius hendrerit urna sed congue. Integer et augue scelerisque nisi ultricies mollis in non nisi. Nulla justo sapien, vehicula vitae diam ac, rutrum fermentum ipsum. Duis vehicula vulputate est, ac feugiat ex varius quis. Aliquam gravida risus et magna euismod rhoncus.
`)
      itemStart = 'quads'
      break
    case 4: // input 'latex' avec une seule zone de saisie et du latex statique inline dans la question
      await playground.displayWork('Donnez la valeur approchée de $\\pi$ par défaut au centième pres (c’est un input de type text, c’est pourquoi le point n’est pas remplacé par une virgule. Le label est stylisé et contient du latex): %{piReponse}', {
        piReponse: {
          type: 'input',
          label: '$\\pi\\approx $',
          inputProps: {
            type: 'text'
          },
          restriction: /[0-9.]/
        }
      }, {})
      itemStart = 'piReponse'
      break
    case 5: // idem 4 avec délimiteurs $ pour le latex static inline
    default:
      await playground.displayWork('Donnez la valeur de $45\\degree$ en radians (on a retiré volontairement les touches sqrt, times et int du clavier c’est pourquoi il est un peu désordonné): %{piSurQuatre}', {
        piSurQuatre: {
          type: 'mathlive',
          value: '\\dfrac{%{piReponse}}{4}',
          commands: ['pi']
        }
      }, {})
      itemStart = 'piSurQuatre'
      break
  }
  if (itemStart) sectionContext.playground.setFocus(itemStart)
}

export async function check ({ pathway, playground, question, storage }: SectionContext): Promise<PartialResult> {
  pathway.logIfDebug('correction avec', pathway)
  switch (question) {
    case 3:
      if (Boolean(await playground.getItemValue('egTri')) && Number(await playground.getItemValue('bam')) < 1) {
        // Attention : la figure a plusieurs calculs, donc deux items. Seul l’item du premier calcul a un
        // span.feedback qui lui est associé. C’est celui-là qu’il faut utiliser.
        await playground.setFeedbackItem('egTri', true, smile)
        return {
          feedBackMessage: evaluations.parfait, ok: true, partialScore: 1
        }
      } else {
        await playground.setFeedbackItem('egTri', true, anger)
        return {
          ok: false, partialScore: 0
        }
      }
    case 1: {
      const reponse = await playground.getItemValue('quads') as string[]
      let nbPoints = 0
      const quads = storage.quads as string[]
      for (let i = 0; i < 5; i++) {
        const quad = quads[i]
        if (quad) {
          if (quad !== 'trapèze') {
            if (reponse.includes(quad)) {
              await playground.setFeedbackItem('quads', true, smile, i)
              nbPoints++
            } else {
              await playground.setFeedbackItem('quads', true, anger, i)
            }
          } else {
            if (reponse.includes(quad)) {
              await playground.setFeedbackItem('quads', true, anger, i)
            } else {
              await playground.setFeedbackItem('quads', true, smile, i)
              nbPoints++
            }
          }
        }
      }

      return {
        feedBackMessage: nbPoints === 5 ? evaluations.parfait : nbPoints < 3 ? 'pas terrible !' : 'vérifie tes réponses.',
        ok: true,
        partialScore: nbPoints / 5
      }
    }
    case 2: {
      const choices = ['12Sur18', '6Sur9', '2Sur3', '18Sur12', '3Sur2']
      const fraction = await playground.getItemValue('fraction')
      // compare utilise cortex.io/ComputeEngine pour analyser et comparer deux strings latex
      // ici on peut comparer \\sqrt9 et \\sqrt{9}, c’est égal pour ComputeEngine
      if (fraction === '2Sur3') {
        await playground.setFeedbackItem('fraction', true, smile, choices.indexOf(String(fraction)))
        return {
          feedBackMessage: evaluations.parfait, ok: true, partialScore: 1
        }
      } else if (fraction === '6Sur9' || fraction === '12Sur18') {
        await playground.setFeedbackItem('fraction', true, confused, choices.indexOf(String(fraction)))
        return {
          feedBackMessage: 'Pas assez simplifié.', ok: true, partialScore: 0.5
        }
      } else {
        await playground.setFeedbackItem('fraction', true, anger, choices.indexOf(String(fraction)))
        return {
          feedBackMessage: 'Compare ta réponse à 1 !', ok: false, partialScore: 0
        }
      }
    }
    case 4: {
      const reponse = await playground.getItemValue('piReponse')
      if (reponse === '3,14') {
        await playground.setFeedbackItem('piReponse', false, smile)
        return {
          feedBackMessage: evaluations.parfait, ok: true, partialScore: 1
        }
      } else if (reponse === '3.14') { // c’est la bonne réponse
        await playground.setFeedbackItem('piReponse', true, confused)
        return {
          feedBackMessage: 'le séparateur décimal est la virgule !', ok: true, partialScore: 1
        }
      } else {
        await playground.setFeedbackItem('piReponse', false, anger)
        return {
          feedBackMessage: 'pas ça du tout', ok: false, partialScore: 0
        }
      }
    }
    case 5:
    default: {
      const reponse = await playground.getItemValue('piSurQuatre') as { piReponse: string }
      if (reponse.piReponse === '\\pi') {
        await playground.setFeedbackItem('piSurQuatre', true, smile)
        return {
          feedBackMessage: evaluations.parfait, ok: true, partialScore: 1
        }
      } else {
        await playground.setFeedbackItem('piSurQuatre', false, anger)
        return {
          feedBackMessage: evaluations.pe_1, ok: false, partialScore: 0
        }
      }
    }
  }
}

/**
 * La fonction appelée une seule fois si tous les essais ont été tentés où si l’élève a bien répondu
 * @param {SectionContext} context
 * @param {Playground} context.playground
 * @param {number} context.question
 */
export async function solution ({ playground, question }: SectionContext): Promise<void> {
  await playground.displaySolution(`La correction de la question ${question}.`, {}, {})
}
