import SectionContext from 'src/lib/entities/SectionContext'

import type { PartialResult, SectionParameters } from 'src/lib/types'

/**
 * Une section V2 type
 */

/**
 * La liste des paramètres de la section (ce qui est modifiable dans l’édition du nœud)
 */
export const parameters: SectionParameters = {
  title: {
    type: 'string',
    defaultValue: 'Pour les tests à JC'
  },
  nbQuestions: {
    type: 'number',
    defaultValue: 1,
    help: 'Ne pas mettre plus de questions',
    min: 1,
    max: 1
  },
  limite: {
    type: 'number',
    defaultValue: 10,
    help: 'Le délai en seconde pour répondre à la question',
    min: 1,
    max: 100
  }
}

/**
 * La liste des évaluations possibles (pour une section qualitative), ex pe
 */
export const evaluations = {
  pe_1: 'Une erreur type', parfait: 'Tout est parfait', bien: 'Exercice réussi'
}

export async function init ({ pathway, storage }: SectionContext): Promise<void> {
  pathway.logIfDebug('init avec pathway : ', pathway, 'storage : ', storage)
}

export async function enonce (sectionContext: SectionContext): Promise<void> {
  const { pathway, playground } = sectionContext
  pathway.logIfDebug('enonce avec', pathway)
  let itemStart = ''
  await playground.displayWork('Quelle est la forme irréductible de $\\dfrac{24}{36}$ ? : %{fraction}', {
    fraction: {
      type: 'mathlive',
      value: '', // avec une chaine vide on va obtenir un éditeur Mathlive vide sans placeholder
      restriction: /./
    }
  })
  await playground.displayWork('Quelle est la forme irréductible de $\\dfrac{12}{36}$ ? : %{fraction2}', {
    fraction2: {
      type: 'mathlive',
      value: '\\frac{%{num}}{%{den}}', // Avec une telle value, on va obtenir un éditeur mathlive read-only avec deux placeholders : 'num' et 'den'.
      restriction: /./
    }
  })

  itemStart = 'fraction'
  if (itemStart) sectionContext.playground.setFocus(itemStart)
}

export async function check ({ pathway, playground }: SectionContext): Promise<PartialResult> {
  pathway.logIfDebug('correction avec', pathway)
  const reponse = await playground.getItemValue('fraction')
  const reponse2 = await playground.getItemValue('fraction2') as { num: string, den: string }
  const { num, den }: { num: string, den: string } = reponse2
  let partialScore = 0
  if (reponse === '\\frac23') {
    await playground.setFeedbackItem('fraction', true, '🙂')
    partialScore++
  } else {
    await playground.setFeedbackItem('fraction', false, '😕')
  }
  if (num === '1' && den === '3') {
    await playground.setFeedbackItem('fraction2', true, '🙂')
    partialScore++
  } else {
    await playground.setFeedbackItem('fraction2', false, '😕')
  }
  return {
    feedBackMessage: reponse === '\\frac23' && (reponse2.num === '1' && reponse2.den === '3')
      ? evaluations.parfait
      : reponse === '\\frac23' || (reponse2.num === '1' && reponse2.den === '3')
        ? 'Une faute'
        : 'C’est faux !',
    ok: reponse === '\\frac23' && (reponse2.num === '1' && reponse2.den === '3'),
    partialScore: partialScore / 2
  }
}

/**
 * La fonction appelée une seule fois si tous les essais ont été tentés où si l’élève a bien répondu
 * @param {SectionContext} context
 * @param {Playground} context.playground
 * @param {number} context.question
 */
export async function solution ({ playground }: SectionContext): Promise<void> {
  await playground.displaySolution('La correction de la question est $\\dfrac{2}{3}$.', {}, {})
}
