import { j3pGetRandomElt, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import SectionContext from 'src/lib/entities/SectionContext'
import Tableau from 'src/lib/outils/tableaux/Tableau'

import type { PartialResult, SectionParameters } from 'src/lib/types'
import { addElement } from 'src/lib/utils/dom/main'

/**
 * Une section V2 pour montrer les ≠ inputs
 * @module
 */

// On aimerait ajouter ici qqchose pour que TS vérifie qu’on exporte ce qu’il faut,
// mais c’est vraiment lourd, on lâche l’affaire,
// (si c’est pas le cas ça plantera au build avec le message qui va bien)
// Cf https://github.com/microsoft/TypeScript/issues/420

/**
 * La liste des paramètres de la section (ce qui est modifiable dans l’édition du nœud)
 */
export const parameters: SectionParameters = {
  title: {
    type: 'string',
    defaultValue: 'Exemple 2'
  },
  deuxPairesDeDes: {
    type: 'integer',
    multiple: true,
    min: 2,
    max: 12,
    defaultValue: [4, 12, 8, 6]
  }
  // pas d’autre paramètre pour cette section
}

/**
 * La liste des évaluations possibles (pour une section qualitative), ex pe
 */
export const evaluations = {
  pe_1: 'Une erreur type', parfait: 'Quand tout est nickel', bien: 'Exercice réussi'
}
let setDeDesJoueur1: [number, number]
let setDeDesJoueur2: [number, number]
let nombreCible: number

export async function init ({ params }: SectionContext): Promise<void> {
  // On distribue les dés.
  let desDes: number[] = j3pShuffle(params.deuxPairesDeDes as number[])
  const d1a = j3pGetRandomElt(desDes)
  desDes = desDes.filter((val) => val !== d1a)
  const d1b = j3pGetRandomElt(desDes)
  desDes = desDes.filter((val) => val !== d1b)
  const d2a = j3pGetRandomElt(desDes)
  desDes = desDes.filter((val) => val !== d2a)
  const d2b = desDes[0]
  setDeDesJoueur1 = [d1a ?? 4, d1b ?? 8]
  setDeDesJoueur2 = [d2a ?? 6, d2b ?? 10]
  nombreCible = j3pGetRandomInt(3, Math.min(setDeDesJoueur1[0] + setDeDesJoueur1[1], setDeDesJoueur2[0] + setDeDesJoueur2[1]) - 1)
}

export async function enonce (sectionContext: SectionContext): Promise<void> {
  const { pathway, playground } = sectionContext
  pathway.logIfDebug('enonce avec', pathway)
  // on génère de l’aléatoire ou pas

  await playground.displayWork(`Arthur propose à Léa de jouer aux dés celui qui choisira le programme télé. Il lui dit :\nPrenons chacun 2 dés : moi, je prends celui à $${setDeDesJoueur1[0].toString()}$ faces et celui à $${setDeDesJoueur1[1].toString()}$ faces et toi, tu prends ce dé à $${setDeDesJoueur2[0].toString()}$ faces et ce dé à $${setDeDesJoueur2[1].toString()}$ faces.\n`, {}, {})
  await playground.displayWork(`Le premier qui fait ${nombreCible.toString()} en ajoutant les scores de ses dés choisit le programme.\n Qui de Arthur ou Léa a le plus de propbabilité de choisir le programme télé ?\n Calculer la probabilité qu’a Arthur de réaliser un total de ${nombreCible.toString()} : Proba Arthur = %{probaArthur}\n Calculer la probabilité qu’a Léa de réaliser un total de ${nombreCible.toString()} sous forme décimale : Proba Léa = %{probaLea} .`, {
    probaArthur: {
      type: 'mathlive', // le type 'mathlive' prend un string Latex dans lequel les %{variable} sont autant de
      // 'placeholers', c’est à dire des boites de saisies dans l’input et chacune est associée
      // à un 'item'. Ici, il y a deux items (on force la proba à être une fraction)
      value: '\\frac{%{numArthur}}{%{denArthur}}'
    },
    probaLea: {
      type: 'mathlive', // Ici, il n’y a qu’un item : on peut saisir n’importe quoi : fraction ou décimal.
      value: ''
    }
  }, {})
  await playground.displayWork('\nOn fournit les tableaux des issues ci-dessous :')
  // affichage des deux tableaux :
  // Pour Arthur
  const leTableau1: {
    headingLines: string[],
    headingCols: string[],
    raws: Array<string[]>,
    options: { className: string }
  } = {
    headingLines: [], headingCols: [], raws: [], options: { className: 'tableauMathlive' }
  }
  leTableau1.headingCols[0] = 'dé1\\dé2 (Arthur)'
  for (let i = 1; i <= setDeDesJoueur1[0]; i++) {
    leTableau1.headingLines.push(String(i))
    leTableau1.raws.push([])
    for (let j = 1; j <= setDeDesJoueur1[1]; j++) {
      if (i === 1) {
        leTableau1.headingCols.push(String(j))
      }
      const rang = leTableau1.raws[i - 1]
      if (rang != null) {
        rang[j - 1] = String(j + i)
      }
    }
  }
  // On recommence pour Léa
  const leTableau2: {
    headingLines: string[],
    headingCols: string[],
    raws: Array<string[]>,
    options: { className: string }
  } = {
    headingLines: [], headingCols: [], raws: [], options: { className: 'tableauMathlive' }
  }
  leTableau2.headingCols[0] = 'dé1\\dé2 (Léa)'
  for (let i = 1; i <= setDeDesJoueur2[0]; i++) {
    leTableau2.headingLines.push(String(i))
    leTableau2.raws.push([])
    for (let j = 1; j <= setDeDesJoueur2[1]; j++) {
      if (i === 1) {
        leTableau2.headingCols.push(String(j))
      }
      const rang = leTableau2.raws[i - 1]
      if (rang != null) {
        rang[j - 1] = String(j + i)
      }
    }
  }

  // On veut un conteneur dans divWork pour y mettre les deux tableaux
  const divWork = playground.getContainer('work')
  const div2Tableaux = addElement(divWork, 'div')
  Tableau.create(div2Tableaux, leTableau1)
  Tableau.create(div2Tableaux, leTableau2)
  sectionContext.playground.setFocus('probaArthur')
}

export async function check ({ pathway, playground }: SectionContext): Promise<PartialResult> {
  pathway.logIfDebug('correction avec', pathway, playground)
  /**
   * une fonction pour compter le nombre d’issues favorables avec de1 + de2 = somme
   * @param {[number,number]} des : Les deux dés à ajouter
   * @param {number} somme : La somme souhaitée
   * @return {number} Le nombre d’issues favorables
   */
  const nbSomme = function (des: [number, number], somme: number): number {
    let count = 0
    for (let i = 1; i <= des[0]; i++) {
      for (let j = 1; j <= des[1]; j++) {
        if (somme === i + j) {
          count++
        } else if (i + j > somme) break
      }
    }
    return count
  }
  const probaArthur = await playground.getItemValue('probaArthur') as { numArthur: string, denArthur: string }
  const probaLea = await playground.getItemValue('probaLea') as string
  const numArthur = Number(probaArthur.numArthur)
  const denArthur = Number(probaArthur.denArthur)
  const nbIssueJ1 = setDeDesJoueur1[0] * setDeDesJoueur1[1]
  const nbIssueJ2 = setDeDesJoueur2[0] * setDeDesJoueur2[1]
  const numJ1 = nbSomme(setDeDesJoueur1, nombreCible)
  const numJ2 = nbSomme(setDeDesJoueur2, nombreCible)

  let score = 0
  // on met canonical à true pour que les fractions à comparer soit simplifiées.
  if (Number(numArthur) * Number(denArthur) && Number(numArthur) * nbIssueJ1 === Number(denArthur) * numJ1) {
    score += 0.5
    await playground.setFeedbackItem('probaArthur', true, '🙂')
  } else {
    await playground.setFeedbackItem('probaArthur', false, '😣')
  }
  if (Number(probaLea) && Number(probaLea.replace(',', '.')) * nbIssueJ2 === numJ2) {
    score += 0.5
    await playground.setFeedbackItem('probaLea', true, '🙂')
  } else {
    await playground.setFeedbackItem('probaLea', false, '😣')
  }
  return {
    ok: score === 1,
    feedBackMessage: score === 1 ? evaluations.parfait : score === 0.5 ? evaluations.bien : evaluations.pe_1,
    partialScore: score,
    evaluation: score === 1 ? 'parfait' : score === 0.5 ? 'bien' : 'pe_1'
  }
}

/**
 * La fonction appelée une seule fois si tous les essais ont été tentés où si l’élève a bien répondu
 * @param {SectionContext} context
 * @param {Playground} context.playground
 * @param {number} context.question
 */
export async function solution ({ playground, question }: SectionContext) {
  await playground.displaySolution(`La correction de la question ${question}.`, {}, {})
}
