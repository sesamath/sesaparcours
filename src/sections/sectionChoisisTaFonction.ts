import SectionContext from 'src/lib/entities/SectionContext'
import { Choice } from 'src/lib/outils/listeDeroulante/ListeDeroulante'

import type { PartialResult, SectionParameters } from 'src/lib/types'

/**
 * Une section section juste pour choisir une fonction parmi celles proposées afin d’enchainer d’autres section V1 avec la fonction choisie
 * Pour mettre au point la gestion de données persistantes dans le moteur V2.
 *
 */

export const parameters: SectionParameters = {
  title: {
    type: 'string',
    defaultValue: 'Choisir une fonction'
  },
  nbQuestions: {
    type: 'integer',
    defaultValue: 1
  }
}

/**
 * La fonction qui initialise les données de la section choisie aléatoirement dans cet exemple
 * les paramètres fournies par le player :
 * @param {SectionContext} contexte
 */
export async function init ({ storage }: SectionContext): Promise<void> {
  storage.choices = ['(3x+2)/(5x+4)', '4x^2+5x+2', 'x^3+2x^2-x-7']
  storage.objFunctionWithModele = [
    {
      fonction: '(3x+2)/(5x+4)',
      modele: 3,
      domaineDef: ['-infini', '\\frac{-4}{5}', ']', '[', '\\frac{-4}{5}', '+infini', ']', '['],
      domaineDeriv: ['-infini', '\\frac{-4}{5}', ']', '[', '\\frac{-4}{5}', '+infini', ']', '['],
      val_interdites: ['\\frac{-4}{5}'],
      limites: [['-\\infty', '\\frac{3}{5}'], ['\\frac{-4}{5}^-', '+\\infty'], ['\\frac{-4}{5}^+', '-\\infty'], ['+\\infty', '\\frac{3}{5}']],
      limites_num: ['-\\infty', '\\frac{-2}{5}', '\\frac{-2}{5}', '+\\infty'],
      limites_nden: ['-\\infty', '0', '0', '+\\infty']
    },
    {
      fonction: '4x^2+5x+2',
      modele: 8,
      domaineDef: ['-infini', '+infini', ']', '['],
      domaineDeriv: ['-infini', '+infini', ']', '['],
      val_interdites: [],
      limites: [['-\\infty', '+\\infty'], ['+\\infty', '+\\infty']]
    },
    {
      fonction: 'x^3+2x^2-x-7',
      modele: 2,
      domaineDef: ['-infini', '+infini', ']', '['],
      domaineDeriv: ['-infini', '+infini', ']', '['],
      val_interdites: [],
      limites: [['-\\infty', '-\\infty'], ['+\\infty', '+\\infty']]
    }
  ]
}

/**
 * La fonction qui crée l’énoncé à l’étape courante. Appelée autant de fois qu’il le faut par le player.
 * @param {SectionContext} sectionContext
 */
export async function enonce (sectionContext: SectionContext): Promise<void> {
  const { playground, storage } = sectionContext
  await playground.displayWork('Choisis une fonction dans la liste ci-contre : %{fonction}',
    {
      fonction: {
        type: 'select',
        choices: storage.choices as Choice[]
      }
    },
    {}
  )
}

/**
 * La fonction qui corrige. Elle est appelée par le player lorsque l’utilisateur clique sur le bouton 'OK'.
 * @param {SectionContext} context
 * @param {Playground} [context.playground] // le playground (objet qui permet l’interface avec les éléments affichés).
 * @param {Record<string,unknown>} [context.storage] // le store de la section
 */
export async function check ({ playground, storage }: SectionContext): Promise<PartialResult> {
  // recup storage
  const { choices, objFunctionWithModele } = storage as {
    choices: string[],
    objFunctionWithModele: {
      fonction: string,
      modele: number,
      domaineDef: string[],
      domaineDeriv: string[],
      val_interdites: (number | string)[],
      limites: Array<[string, string]>,
      limites_num?: string[],
      limites_den?: string[]
    }[]
  }
  let fonction = await playground.getItemValue('fonction') as string
  let feedBackMessage = ''
  if (fonction == null) {
    feedBackMessage = 'Il fallait choisir ! j’impose le premier choix.'
    fonction = '(3x+2)/(5x+4)'
  }
  if (objFunctionWithModele == null) throw Error('Un problème avec storage qui ne contient pas ce qui est nécessaire.')
  const fonctionChoisie = objFunctionWithModele.find(el => el.fonction === fonction)
  if (fonctionChoisie == null) throw Error(`On n’a pas trouvé la fonction choisie : ${fonction}`)
  const modele = fonctionChoisie.modele ?? 3
  const domaineDef = fonctionChoisie.domaineDef
  const domaineDeriv = fonctionChoisie.domaineDeriv
  // eslint-disable-next-line camelcase
  const val_interdites = fonctionChoisie.val_interdites
  const limites = fonctionChoisie.limites
  // eslint-disable-next-line camelcase
  const limites_num = fonctionChoisie.limites_num ?? []
  // eslint-disable-next-line camelcase
  const limites_den = fonctionChoisie.limites_den ?? []

  if (!choices.includes(fonction)) throw Error(`Curieux, la fonction choisie n’est pas dans la liste : ${fonction}`)
  await playground.setFeedbackItem('fonction', true, '')
  Object.assign(storage, {
    fonctionWithModele: {
      fonction,
      modele,
      domaineDef,
      domaineDeriv,
      // eslint-disable-next-line camelcase
      val_interdites,
      limites,
      // eslint-disable-next-line camelcase
      limites_num,
      // eslint-disable-next-line camelcase
      limites_den
    }
  })
  return {
    feedBackMessage, ok: true, partialScore: 1
  }
}

/**
 * La fonction appelée une seule fois si tous les essais ont été tentés où si l’élève a bien répondu
 * @param {SectionContext} context
 * @param {Record<string, unknown>} context.storage
 * @param {Playground} context.playground
 */
export async function solution ({ storage, playground, pathway }: SectionContext): Promise<void> {
  // tout ça pour envoyer la fonction choisie dans pathway.perstistentStorage au bon format pour sectionEtudeFonction_derivee
  const { fonctionWithModele } = storage as {
    fonctionWithModele: {
      fonction: string,
      modele: number,
      domaineDef: string[],
      domaineDeriv: string[],
      val_interdites: number[],
      limites: Array<[string, string]>
      limites_num: string[],
      limites_den: string[]
    }
  }
  let fonction: string
  let modele: number
  let domaineDef: string[]
  let domaineDeriv: string[]
  // eslint-disable-next-line camelcase
  let val_interdites: number[]
  let limites: Array<[string, string]>
  // eslint-disable-next-line camelcase
  let limites_num: string[]
  // eslint-disable-next-line camelcase
  let limites_den: string[]
  if ('fonction' in fonctionWithModele) {
    fonction = fonctionWithModele?.fonction ?? '4x^2+5x+2'
    modele = fonctionWithModele?.modele ?? 8
    domaineDef = fonctionWithModele?.domaineDef ?? ['-infini', '+infini', ']', '[']
    domaineDeriv = fonctionWithModele?.domaineDeriv ?? ['-infini', '+infini', ']', '[']
    // eslint-disable-next-line camelcase
    val_interdites = fonctionWithModele.val_interdites ?? []
    limites = fonctionWithModele.limites ?? [['-infini', '+\\infty'], ['+infini', '+\\infty']]
    // eslint-disable-next-line camelcase
    limites_num = fonctionWithModele.limites_num ?? []
    // eslint-disable-next-line camelcase
    limites_den = fonctionWithModele.limites_den ?? []
  } else {
    fonction = '4x^2+5x+2'
    modele = 8
    domaineDef = ['-infini', '+infini', ']', '[']
    domaineDeriv = ['-infini', '+infini', ']', '[']
    // eslint-disable-next-line camelcase
    val_interdites = []
    limites = [['-infini', '+\\infty'], ['+infini', '+\\infty']]
    // eslint-disable-next-line camelcase
    limites_num = []
    // eslint-disable-next-line camelcase
    limites_den = []
  }

  pathway.persistentStorage.etudeFonctions = {
    fonction,
    modele,
    domaineDef,
    domaineDeriv,
    // eslint-disable-next-line camelcase
    val_interdites,
    limites,
    // eslint-disable-next-line camelcase
    limites_num,
    // eslint-disable-next-line camelcase
    limites_den
  }
  playground.reset({ solution: true })
  await playground.displaySolution(`Merci d’avoir choisi : ${fonction}`, {}, {})
}
