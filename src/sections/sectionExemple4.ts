import { j3pGetRandomInt } from 'src/legacy/core/functions'
import SectionContext from 'src/lib/entities/SectionContext'
import type { Icell } from 'src/lib/outils/tableaux/tableMathlive'

import type { PartialResult, SectionParameters } from 'src/lib/types'

/**
 * Une section V2 type
 * Avec un énoncé comprenant du latex static et un input mathlive.
 */

/**
 * La liste des paramètres de la section (ce qui est modifiable dans l’édition du nœud)
 */
export const parameters: SectionParameters = {
  nbQuestions: {
    type: 'number',
    defaultValue: 3,
    help: 'Ce paramètres n’est pas modifiable',
    min: 3,
    max: 3
  },
  nbSteps: {
    type: 'number',
    defaultValue: 2,
    help: 'Ce paramètres n’est pas modifiable',
    min: 2,
    max: 2
  },
  maxTries: {
    type: 'number',
    defaultValue: 2,
    help: 'Le nombre d\'essais maximum',
    min: 1,
    max: 3
  },
  title: {
    type: 'string',
    defaultValue: 'Une section de démo',
    help: 'Le titre par défaut de la section'
  }
}

type TwoNumber = [number, number]
/**
 * La liste des évaluations possibles (pour une section qualitative), ex pe
 */
export const evaluations = {}

/**
 * La fonction qui initialise les données de la section choisie aléatoirement dans cet exemple
 * les paramètres fournies par le player :
 */
export async function init ({ storage }: SectionContext): Promise<void> {
  const variables: number[] = []
  let plusEtMoins: [string, string]
  const antecedent: number = j3pGetRandomInt(1, 5)
  do {
    const int = j3pGetRandomInt(2, 20)
    if (!variables.includes(int)) variables.push(int)
    const signe1 = Math.random() < 0.5 ? '-' : '+'
    const signe2 = Math.random() < 0.5 ? '-' : '+'
    plusEtMoins = [signe1, signe2]
  } while (variables.length < 4)
  const coeffProp: number = j3pGetRandomInt(2, 9)
  const grandeurs: TwoNumber[] = []
  do {
    const g1: number = j3pGetRandomInt(2, 20)
    const g2 = coeffProp * g1
    if (grandeurs.find(el => el[0] === g1 && el[1] === g2) == null) {
      grandeurs.push([g1, g2])
    }
  } while (grandeurs.length < 5)
  Object.assign(storage, { variables, plusEtMoins, antecedent, grandeurs })
}

/**
 * La fonction qui crée l’énoncé à l’étape courante. Appelée autant de fois qu’il le faut par le player.
 * @param {SectionContext} sectionContext
 */
export async function enonce (sectionContext: SectionContext): Promise<void> {
  const { playground, storage, question, step } = sectionContext
  if (!Array.isArray(storage.variables) || storage.variables.length !== 4) {
    throw Error('storage.variables ne contient pas 4 éléments.')
  }
  if (!Array.isArray(storage.plusEtMoins) || storage.plusEtMoins.length !== 2) {
    throw Error('storage.plusEtMoins ne contient pas 2 éléments.')
  }
  const [a, b, c, d] = storage.variables
  const [signe1, signe2] = storage.plusEtMoins
  const formeDeveloppee = `${String(a * c)}x^2${signe1}${String(b * c)}x${signe2}${String(a * d)}x${signe1 === signe2 ? '+' : '-'}${String(b * d)}`
  const formeFausse = `${String(a)}x${signe1}${String(b * c)}x${signe2}${String(d)}`
  storage.formeDeveloppee = formeDeveloppee
  storage.formeFausse = formeFausse
  // On met à blanc la zone de travail
  if (question === 1) { // question 1
    if (step === 1) {
      const grandeurs = storage.grandeurs as TwoNumber[]
      const nbColonnes = grandeurs.length + 1
      const firstLine: boolean[] = []
      for (let i = 0; i < nbColonnes - 1; i++) {
        firstLine.push(Math.random() < 0.5)
      }
      const ligne1: Icell[] = [{ texte: 'grandeur 1', latex: false, gras: true, color: 'black' }]
      const ligne2: Icell[] = [{ texte: 'grandeur 2', latex: false, gras: true, color: 'black' }]
      const indice = j3pGetRandomInt(1, nbColonnes - 1)
      for (let i = 1; i < nbColonnes; i++) {
        const paire = grandeurs[i - 1] as [number, number]
        if (paire == null || !Array.isArray(paire)) {
          throw Error(`Un problème avec cette paire de valeurs : ${grandeurs[i - 1]}`)
        }
        const [g1, g2] = paire

        if (g1 != null && g2 != null) {
          ligne1[i] = {
            texte: firstLine[i - 1] || indice === i ? String(g1) : '',
            latex: true,
            gras: true,
            color: 'blue'
          }
          ligne2[i] = {
            texte: firstLine[i - 1] && indice !== i ? '' : String(g2),
            latex: true,
            gras: true,
            color: 'blue'
          }
        }
      }
      const name = 'tabProp1'
      await playground.displayWork(`Compléter le tableau de proportionnalité suivant : 
%{${name}}`, {
        [name]: {
          type: 'table',
          tableau: {
            nbColonnes, ligne1, ligne2
          },
          inputProps: {
            name, sousType: 'prop', classes: ''
          },
          persistent: false
        }
      }, {})
      sectionContext.playground.setFocus('tabProp1')
      return
    }
    // step 2
    const headingCols: Icell[] = [{ texte: '\\times', color: 'black', gras: true, latex: true }]
    headingCols.push({ texte: `${a}x`, gras: true, color: 'black', latex: true }, {
      texte: `${signe1}${b}`,
      gras: true,
      color: 'black',
      latex: true
    })
    const headingLines: Icell[] = [{
      texte: `${c}x`,
      gras: true,
      color: 'black',
      latex: true
    }, { texte: `${signe2}${d}`, gras: true, color: 'black', latex: true }]
    const raws: Icell[][] = [[{ texte: '', gras: true, color: 'black', latex: true }, {
      texte: '',
      gras: true,
      color: 'black',
      latex: true
    }], [{ texte: '', gras: true, color: 'black', latex: true }, {
      texte: '',
      gras: true,
      color: 'black',
      latex: true
    }]]
    await playground.displayWork('Compléter la table de multiplication suivante : \n%{tabDistri1}', {
      tabDistri1: {
        type: 'table',
        tableau: {
          raws, headingCols, headingLines
        },
        inputProps: {
          name: 'tabDistri1', sousType: 'dblEntry', classes: ''
        },
        persistent: false
      }
    }, {})
    return
  }
  // question 2
  if (question === 2) {
    if (step === 1) {
      playground.reset({ work: true })
      // Une question statique pour mettre au point l’item de type 'input' avec ses nouvelles restrictions
      await playground.displayWork('Quelle est la valeur décimale de $A=\\frac18$ ? %{huitieme}', {
        huitieme: {
          type: 'input',
          inputProps: {
            name: 'huitieme'
          },
          restriction: 'decimal'
        }
      }, // il n’y a pas d’input à paramétrer.
      { persistent: true } // Ce display restera en cas de reset de la zone de travail avec l’option
        // butPersistent à true.
      )
      await playground.displayWork(`
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce condimentum suscipit tellus, a pretium nisi. Integer varius arcu sit amet nunc eleifend, vitae pretium elit eleifend. Pellentesque quis nisl odio. Etiam at lacus id justo consectetur tempor. Vivamus lacinia consectetur mauris vel vestibulum. Praesent euismod tortor a varius sagittis. Etiam tincidunt cursus velit non faucibus. Vivamus in ipsum vitae dolor mollis dignissim. Nam porta ex vel purus aliquam, vitae tristique justo lacinia. Aenean placerat vel purus in tempor. Aliquam ut dolor lorem. Sed fermentum erat metus, mollis hendrerit ipsum finibus ac.

Quisque eu mauris commodo, dapibus eros sed, tristique nibh. Quisque eleifend enim blandit nulla consectetur, quis gravida lectus commodo. Vivamus sit amet suscipit velit. Quisque imperdiet eget turpis et lacinia. In mollis libero turpis, id posuere purus efficitur sit amet. Quisque sagittis scelerisque eros sit amet ullamcorper. Maecenas nunc justo, suscipit ac ullamcorper eu, rutrum pellentesque tellus. Quisque euismod magna vitae leo varius eleifend. Cras condimentum tristique elit, ut pellentesque mi dictum rhoncus. Donec elementum varius lectus, sed ultrices nibh faucibus et. Quisque at lobortis mauris. Duis varius enim vel dapibus sodales. Vivamus vel lectus semper neque malesuada sodales. Nam malesuada lobortis lorem sit amet dapibus.

Sed sed vehicula nisi, eu sagittis purus. Donec non blandit diam. In est lorem, facilisis vel dignissim ac, dictum at metus. Nullam ac nunc metus. Duis malesuada convallis mauris eu blandit. Etiam egestas ligula fringilla efficitur congue. Suspendisse potenti. Aenean eleifend efficitur bibendum. Praesent finibus eu tortor sit amet egestas. Fusce ac pretium erat. Nullam dictum sapien dolor, eu semper sem ultrices vel. Donec sit amet libero purus. Sed a quam libero. Suspendisse id tempus felis, sit amet consectetur arcu. Proin dolor purus, ultrices elementum elit vitae, faucibus elementum neque.

Quisque tellus lectus, semper aliquet suscipit at, facilisis ac purus. Proin ullamcorper bibendum diam at scelerisque. Aliquam mollis accumsan turpis, et consectetur ex pulvinar et. Sed ligula ipsum, molestie id tincidunt ac, commodo ut neque. Vestibulum nec nisl non orci rhoncus ultricies a vel purus. Sed ac luctus nisi. In eget suscipit tortor, non convallis elit. Pellentesque maximus justo vel est tincidunt dapibus. Pellentesque libero diam, molestie sit amet velit non, posuere sagittis nulla. Vivamus in elementum ligula, sit amet aliquam odio. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed ex metus, scelerisque vitae lorem molestie, imperdiet convallis neque. Nam commodo, leo nec imperdiet sollicitudin, sem quam suscipit ipsum, id eleifend justo dui et leo. Vestibulum hendrerit arcu eget turpis hendrerit, vitae convallis neque posuere.

Aenean id elit id nisl ultricies elementum. Nullam augue erat, pharetra eu urna a, dignissim viverra mauris. Nunc vitae pulvinar leo. Vivamus in vehicula ipsum. Nullam vel ipsum ac magna laoreet sodales. Aenean nulla orci, consectetur commodo posuere et, porta ornare nibh. Fusce viverra ante id sodales volutpat. Integer pulvinar nisl eu massa faucibus, non interdum nisl placerat. Phasellus gravida nunc nulla, sed sagittis lacus luctus et. Nunc malesuada lobortis elit, eu aliquam metus accumsan lobortis.

Aliquam a rhoncus mauris. Donec volutpat lacus vitae interdum porta. Duis eu nulla auctor, vestibulum orci quis, mattis lectus. Sed commodo purus sed nulla finibus, et sollicitudin risus ultricies. Maecenas dignissim, dolor id luctus placerat, ex mi laoreet est, nec feugiat nunc quam congue tellus. Vivamus vulputate aliquam venenatis. Curabitur sed ante nec sapien euismod consectetur. Pellentesque eget turpis at urna luctus feugiat fermentum commodo nisi.

Mauris at magna nec urna dignissim auctor. Nunc quis sodales magna. Maecenas vehicula mauris at purus malesuada, vel gravida arcu laoreet. Vestibulum eleifend consequat rhoncus. Nunc ut diam vitae nulla congue ornare. Aenean id convallis urna. Quisque a dictum justo.
`)
      sectionContext.playground.setFocus('huitieme')
      return
    }
    playground.reset({ work: true, butPersistent: true })
    // Une question statique pour mettre au point l’item de type 'input' avec ses nouvelles restrictions
    await playground.displayWork('\nQuelle est la valeur fractionnaire de $2A$ ? %{inputML2}', {
      inputML2: {
        type: 'mathlive', value: ''
      }
    }, // il n’y a pas d’input à paramétrer.
    { persistent: false } // Ce display restera en cas de reset de la zone de travail avec l’option
      // butPersistent à true.
    )
    sectionContext.playground.setFocus('inputML2')
    return
  }
  // question 3
  // if (question === 3) {
  if (step === 1) {
    playground.reset({ work: true })
    // on écrit l’énoncé avec du latex 'statique' entre $ $ et un input de type select contenant du latex !
    await playground.displayWork(`Quelle est la forme développée de $A=(${String(a)}x${signe1}${String(b)})(${String(c)}x${signe2}${String(d)})$ sous la forme la plus simple possible ? %{liste}`, {
      liste: {
        type: 'select',
        choices: [{
          latex: formeFausse, value: 'faux'
        }, {
          latex: formeDeveloppee, value: 'bien'
        }],
        inputProps: {
          name: 'liste', heading: 'Choisis la bonne réponse'
        }
      }
    }, // il n’y a pas d’input à paramétrer.
    { persistent: false } // Ce display restera en cas de reset de la zone de travail avec l’option butPersistent
      // à true.
    )
    sectionContext.playground.setFocus('formule')
    return
  }
  playground.reset({ work: true, butPersistent: true })
  const n = storage.antecedent as number
  await playground.displayWork(`Calculer la valeur de $A$ pour $x=${n}$ : %{inputML1}`, {
    inputML1: {
      type: 'mathlive', value: ''
    }
  }, // il n’y a pas d’input à paramétrer.
  { persistent: false } // Ce display restera en cas de reset de la zone de travail avec l’option butPersistent
    // à true.
  )
  sectionContext.playground.setFocus('inputML1')
}

// le résultat à renvoyer, helper de check
const getResult = (ok: boolean) => ({
  ok,
  feedBackMessage: ok ? 'Bien' : 'Faux',
  partialScore: ok ? 1 : 0
})

/**
 * La fonction qui corrige. Elle est appelée par le player lorsque l’utilisateur clique sur le bouton 'OK'.
 * @param {SectionContext} context
 * @param {Playground} [context.playground] // le playground (objet qui permet l’interface avec les éléments affichés).
 * @param {number} [context.retry] // l’indice de l’essai courant
 * @param {number} [context.question] // l’indice de la question courante
 */
export async function check ({ playground, question, step, storage }: SectionContext): Promise<PartialResult> {
  // recup storage
  const { formeDeveloppee, formeFausse, antecedent } = storage

  // helper pour setFeedbackItem
  const setFeedback = (itemName: string, ok: boolean) => playground.setFeedbackItem(itemName, ok, ok ? '🙂' : '😣')

  // on passe en revue les cas
  // question 1
  if (question === 1) {
    if (step === 1) {
      const reponses = await playground.getItemValue('tabProp1')
      const grandeurs = storage.grandeurs as TwoNumber[]
      // const nbColonnes = grandeurs.length + 1
      let ok = true
      for (const [key, value] of Object.entries(reponses)) {
        if (key != null && value != null) {
          const [lString, cString] = key.match(/\d/g) as string[]
          const l = Number(lString)
          const c = Number(cString)
          const colonne = grandeurs[c - 1]
          if (colonne != null) {
            if (colonne[l] !== Number(value)) {
              ok = false
            }
          }
        }
      }
      return getResult(ok)
    }
    // step 2 question 1
    const [a, b, c, d] = storage.variables as number[]
    const [signe1, signe2] = storage.plusEtMoins as string[]
    if (a != null && b != null && c != null && d != null && signe1 != null && signe2 != null) {
      const values = [[`${String(a * c)}x^2`, `${signe2 === '+' ? '' : signe2}${String(a * d)}x`], [`${signe1 === '+' ? '' : signe1}${String(b * c)}x`, `${signe1 === signe2 ? '' : '-'}${String(b * d)}`]]
      let ok: boolean = true
      const saisies = await playground.getItemValue('tabDistri1')
      for (const [key, value] of Object.entries(saisies) as [string, string][]) {
        if (key != null && value != null) {
          const [lString, cString] = key.match(/\d/g) as string[]
          const l = Number(lString)
          const c = Number(cString)
          const colonne = values[c - 1]
          if (colonne != null) {
            const reponse = colonne[l - 1]
            if (reponse != null && value != null) {
              if (reponse !== value) {
                ok = false
              }
            } else {
              ok = false
            }
          }
        }
      }
      return getResult(ok)
    }
  }
  // question 2
  if (question === 2) {
    if (step === 1) {
      const saisie = await playground.getItemValue('huitieme')
      const ok = saisie === '0,125' // la réponse attendue
      await setFeedback('huitieme', ok)
      return getResult(ok)
    }
    const saisie = await playground.getItemValue('inputML2') as string
    const ok = saisie === '\\frac14'
    await setFeedback('inputML2', ok)
    return getResult(ok)
  }
  // question 3
  // if (question === 3) {
  if (step === 1) {
    const saisie = await playground.getItemValue('liste')
    const ok = saisie === 'bien'
    await setFeedback('liste', ok)
    const formeChoisie = (ok ? formeDeveloppee : formeFausse) as string
    storage.formeChoisie = formeChoisie
    await playground.displayWork(`Vous avez choisi l’expression :  $A=${formeChoisie}$\n`, {}, { persistent: true })
    return getResult(ok)
  }
  // Trouver un moyen de calculer formChoisie pour storage.antecedent
  const saisie = await playground.getItemValue('inputML1') as string
  /*  Ici on utilisait computeEngine... y a-t-il une solution avec mtgApp ?
                     const exp = ce.parse(storage.formeChoisie as string, {})
                     ce.assign('x', antecedent as number)
                     const resultatAttendu = exp.N()
                   */
  // En attendant, on va tout faire en js
  const x = antecedent as number
  const [a, b, c, d] = storage.variables as number[]
  const [signe1, signe2] = storage.plusEtMoins as string[]
  let ok: boolean = false
  if (a && b && c && d && x) {
    let resultatDeveloppe = a * c * x ** 2
    resultatDeveloppe += (signe1 === '+' ? b * c * x : -b * c * x)
    resultatDeveloppe += (signe1 === signe2 ? b * d : -b * d)
    const resultatFaux = a * x + (signe1 === '+' ? b * c : -b * c) * x + (signe2 === '+' ? d : -d)

    const resultatAttendu = storage.formeChoisie === storage.formeDeveloppee ? resultatDeveloppe : resultatFaux
    ok = Number(saisie) === resultatAttendu
    storage.resultatAttendu = resultatAttendu
  }
  await setFeedback('inputML1', ok)
  return getResult(ok)
}

/**
 * La fonction appelée une seule fois si tous les essais ont été tentés où si l’élève a bien répondu
 * @param {SectionContext} context
 * @param {Record<string, unknown>} context.storage
 * @param {Playground} context.playground
 * @param {number} context.question
 */
export async function solution ({ storage, playground, question, step }: SectionContext): Promise<void> {
  playground.reset({ solution: true })
  if (question === 1) {
    if (step === 1) {
      const grandeurs = storage.grandeurs as TwoNumber[]
      const nbColonnes = grandeurs.length + 1
      const ligne1Complete: Icell[] = [{ texte: 'grandeur 1', latex: false, gras: true, color: 'black' }]
      const ligne2Complete: Icell[] = [{ texte: 'grandeur 2', latex: false, gras: true, color: 'black' }]
      for (let i = 1; i < nbColonnes; i++) {
        const paire = grandeurs[i - 1] as [number, number]
        if (paire == null || !Array.isArray(paire)) {
          throw Error(`Un problème avec cette paire de valeurs : ${grandeurs[i - 1]}`)
        }
        const [g1, g2] = paire
        if (g1 != null && g2 != null) {
          ligne1Complete[i] = { texte: String(g1), latex: true, gras: true, color: 'blue' }
          ligne2Complete[i] = { texte: String(g2), latex: true, gras: true, color: 'blue' }
        }
      }
      await playground.displaySolution('Voici le tableau complété :\n%{tabComplet}', {
        tabComplet: {
          type: 'table',
          tableau: {
            nbColonnes, ligne1: ligne1Complete, ligne2: ligne2Complete
          },
          inputProps: {
            name: 'tabProp1', sousType: 'prop', classes: ''
          },
          persistent: false
        }
      }, {})
    } else { // question 1 step2

    }
  } else if (question === 2) {
    if (step === 1) {
      await playground.displaySolution('Il fallait répondre 0,125.', {}, {})
    } else {
      await playground.displaySolution('Il fallait répondre $\\frac14$.', {}, {})
    }
  } else {
    // if (question === 3) {
    if (step === 1) {
      await playground.displaySolution(`Il fallait choisir $${storage.formeDeveloppee}$.`, {}, {})
    } else {
      await playground.displaySolution(`Pour $x=${storage.antecedent}$, l’expression $${storage.formeChoisie}$ est égale à $${storage.resultatAttendu}$`, {}, {})
    }
  }
}
