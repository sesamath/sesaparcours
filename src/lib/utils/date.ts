/**
 * Retourne une Date (celle passée, éventuellement en string, ou la date courante)
 * @param {Date|string|undefined} date
 * @param {Object} [options]
 * @param {boolean} [options.doNotThrow=false] Passer true pour récupérer la date courante (et l’erreur en console)
 * @throws {Error} si la date fournie est invalide
 */
export function dateRevirer (date?: Date | string, { doNotThrow = false } = {}) : Date {
  if (date == null) return new Date()
  let _date
  if (typeof date === 'string') {
    _date = date
    date = new Date(date)
  }
  // on peut avoir une date invalide
  if (date instanceof Date && !Number.isNaN(date.valueOf())) return date
  const error = Error(`Date invalide ${_date ?? date}`)
  if (doNotThrow) {
    console.error(error)
    return new Date()
  }
  throw error
}

/**
 * Retourne la durée écoulée depuis startedAtMs (ms), en ms
 * @param startedAtMs Le timestamp de départ, en ms
 * @returns durée écoulée en ms
 * @throws {Error} si startedAtMs est dans le futur
 */
export function getDelayMs (startedAtMs: number): number {
  const now = Date.now()
  if (startedAtMs > now) throw Error(`Date de départ dans le futur ! (${startedAtMs} > ${now})`)
  return Date.now() - startedAtMs
}

/**
 * Retourne la durée écoulée depuis startedAtMs (ms), en secondes entières
 * @param startedAtMs Le timestamp de départ, en ms, pour calculer la durée écoulée
 * @returns durée écoulée en s (arrondie) depuis startedAtMs
 * @throws {Error} si startedAtMs est dans le futur
 */
export function getDelaySec (startedAtMs: number): number {
  // on pourrait appeler getDelayMs, mais vaut mieux recopier ces 2 lignes de code qu'ajouter un appel de fct
  const now = Date.now()
  const delay = Date.now() - startedAtMs
  if (delay < 0) throw Error(`Date de départ dans le futur ! (${startedAtMs} > ${now})`)
  if (delay > 10_000_000) console.error(Error(`timestamp ${startedAtMs} probablement invalide (il donne un délai de ${delay}s, on en est à ${now}), il faut passer un timestamp en ms`))
  return Math.round(delay / 1000)
}
