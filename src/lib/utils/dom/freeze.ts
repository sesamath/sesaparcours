import { MathfieldElement } from 'mathlive'
import DomEltInput from 'src/lib/entities/dom/DomEltInput'

import DomEltSelect from 'src/lib/entities/dom/DomEltSelect'
import type { Item } from 'src/lib/player/types'
import { getDelayMs } from 'src/lib/utils/date'

/**
 * Gèle les enfants d’un élément du DOM (pas l’élément lui-même, ses attributs restent modifiables)
 * Remet son innerHTML à l’identique s’il change (pas un vrai freeze car ce n’est pas possible)
 * @param {Item|HTMLElement} elt L’élément ou son id
 * @throws {Error} Si l’ajout de l’observer n’a pas marché
 * @return {MutationObserver} void si l’ajout de l’observer
 */
export function freeze (elt: Item | HTMLElement): MutationObserver {
  let element: HTMLElement | HTMLInputElement | MathfieldElement | null
  if (elt instanceof HTMLElement) {
    element = elt
  } else {
    switch (elt?.constructor?.name) {
      case 'DomEltSelect': // là, il va falloir un truc violent du genre : On prend le contenu du elt.listeDeroulante.spanSelected, on vire tout ce qu’il y a dans elt.container, puis on remet un simple span.currentchoice avec le contenu sauvegardé...
        element = (elt as DomEltSelect).listeDeroulante.container.querySelector('span.currentChoice')
        break
      case 'DomEltInput':
        element = (elt as DomEltInput).element
        element.setAttribute('value', (element as HTMLInputElement).value)
        break

      default:
        element = elt?.container
        break
    }
  }
  if (element == null) throw Error('Il faut fournir un élément ou le nom d’un item')
  if (typeof MutationObserver !== 'function') throw Error('Navigateur trop ancien, MutationObserver inconnu')

  // on diminue légèrement l’opacité
  const opacity = Number(element.style.opacity) || 1
  element.style.opacity = String(opacity * 0.8)

  // on récupère tous les inputs contenus dans elt pour mettre leur valeurs dans le innerHTML
  for (const input of element.querySelectorAll('input')) {
    // on récupère la valeur pour qu’elle soit dans le innerHTML de element
    input.setAttribute('value', input.value)
    // et lui ajoute un listener pour que chaque modif déclenche une modif du dom
    input.addEventListener('input', () => {
      input.value = input.getAttribute('value') || ''
    })
  }
  // si l’élément à geler est lui-même un input, faut mettre sa valeur dans ses attributs
  if (element.tagName === 'INPUT') element.setAttribute('value', (element as HTMLInputElement).value)

  const htmlFrozen = element.innerHTML
  const attrs: Record<string, string> = {}
  for (const attr of element.getAttributeNames()) attrs[attr] = element.getAttribute(attr) || ''

  // pour gérer le debounce
  let lastReset = 0

  const observer = new MutationObserver(function () {
    if (element === null) return console.error(Error('L’élément a disparu du dom'))
    try {
      // gestion debounce (car notre action ici va nous rappeler)
      if (getDelayMs(lastReset) < 100) return
      lastReset = Date.now()
      // on remet innerHTML
      element.innerHTML = htmlFrozen
      // et les attributs
      for (const [name, value] of Object.entries(attrs)) {
        element.setAttribute(name, value)
        if (name === 'value') (element as HTMLInputElement).value = value
      }
      // en virant ceux qui auraient pu être ajoutés
      for (const attr of element.getAttributeNames()) {
        if (!(attr in attrs)) element.removeAttribute(attr)
      }

      // ATTENTION, il faut refaire ça à chaque fois qu’on reset le html
      // Ça supprime tous les objets du DOM actuel et en recrée de nouveaux
      // => faut repasser en revue tous les inputs
      for (const input of element.querySelectorAll('input')) {
        input.addEventListener('input', () => {
          input.value = input.getAttribute('value') ?? ''
        })
      }
    } catch (error) {
      console.error(error, '=> le code continue en ignorant MutationObserver')
    }
  })
  observer.observe(element, { subtree: true, attributes: true, characterData: true, childList: true })
  return observer
}
