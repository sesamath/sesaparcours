// tout ça est déjà dans typescript/lib/lib.dom.d.ts, mais c’est pas exporté…
// => on le recopie ici

import type { MathfieldElement } from 'mathlive'
import type { RecursivePartial, ValueOf } from 'src/lib/types'
import type { ContentTagsAllowed } from 'src/lib/utils/dom/main'

export type NodeListOf<T extends Node> = globalThis.NodeListOf<T>

export type HtmlElementTagNameMap = globalThis.HTMLElementTagNameMap

export type SvgElementTagNameMap = globalThis.SVGElementTagNameMap

export type CustomElementTagNameMap = {
  'math-field': MathfieldElement
}

// et on ajoute qq types bien pratiques
// html
/** Liste des tags html */
export type TagNameHtml = keyof HtmlElementTagNameMap
/** Liste des tags html custom que l’on gère (math-field par ex) */
export type TagNameCustom = keyof CustomElementTagNameMap

/* export type EltHtml<TagName extends TagNameHtml | TagNameCustom> = TagName extends TagNameHtml
    ? HtmlElementTagNameMap[TagName]
    : CustomElementTagNameMap[TagName]
 */
/** liste des tags svg */
export type TagNameSvg = keyof SvgElementTagNameMap
/* export type EltSvg<TagName extends keyof SvgElementTagNameMap> = SvgElementTagNameMap[TagName]
*/

// l’un ou l’autre, mais ça marche pas terrible, TS n’infère pas ça correctement…
export type TagName = TagNameHtml | TagNameCustom | TagNameSvg

/** Un élément HTML de type Tag */
export type HtmlElt<Tag extends keyof HtmlElementTagNameMap> = HtmlElementTagNameMap[Tag]

// …faut remettre le ternaire dans les déclarations de type de retour des fcts
/** Un élément géré par nos fonctions génériques (html|svg|custom) */
export type Elt<Tag extends TagName> = Tag extends keyof HtmlElementTagNameMap
  ? HtmlElt<Tag>
  : Tag extends keyof CustomElementTagNameMap
    ? CustomElementTagNameMap[Tag]
    : SvgElementTagNameMap[Tag]

export type EltPropsPartial<Tag> = RecursivePartial<Elt<Tag>>

// l’intersection prend toutes les propriétés, elles sont toutes facultatives et on a pas de collision
// (sinon ça pourrait donner un type inutilisable avec des valeurs obligatoires de type never)

export interface RichEltOptions<Tag extends ContentTagsAllowed> {
  tag: Tag
  props?: EltPropsPartial<Tag>
  attrs?: Record<string, string>
  /** Le contenu, qui peut contenir du ${foo} pour l’enrichir */
  content?: string
  /** Chaque clé foo correspond aux options pour ${foo} trouvé dans content */
  contentOptions?: Record<string, RichEltOptions<ContentTagsAllowed>>
}

/**
 * Format des options de addElement
 */
export type EltOptions<Tag extends TagName> = EltPropsPartial<Tag> & {
  attrs?: Record<string, string>
  content?: string
}

/**
 * Format d’un choix pour checkbox
 */
interface CheckBoxChoice {
  label: string
  value: string
  checked?: boolean
}

/**
 * Liste de choix pour checkboxes
 */
export type CheckBoxesChoices = Record<string, CheckBoxChoice>

/** Un nom de propriété de style  */
export type StyleProp = keyof CSSStyleDeclaration
/** Un nom de propriété de style modifiable (tous sauf length et parentRule) */
export type StylePropWritable = Exclude<keyof CSSStyleDeclaration, 'length' | 'parentRule' | Symbol | number> // faut exclure Symbol pour que TS soit sûr que l’on ne récupère que des strings (keyof peut retourner des Symbol)
/** Une valeur de style, autant laisser string en général, sinon préciser par ex CSSUnitValue */
export type StyleValue = ValueOf<CSSStyleDeclaration>

/**
 * Un objet qui peut être mergé dans elt.style (toutes ses clés sont des propriétés valides de style, et ses valeurs valides également
 */
export type StyleObject = Record<StylePropWritable, string>
// cf node_modules/typescript/lib/lib.dom.d.ts les Style* et CSS*
