/** @module lib/utils/array */

/**
 * Retourne true si arg est un tableau non vide de tableaux (éventuellement vides)
 * @param arg
 */
export const isArrayOfArrays = (arg: unknown): arg is unknown[][] => Array.isArray(arg) && arg.length > 0 && arg.every(elt => Array.isArray(elt))
/**
 * Retourne true si arg est un tableau de booléens non vide
 * @param arg
 */
export const isArrayOfBooleans = (arg: unknown): arg is boolean[] => Array.isArray(arg) && arg.length > 0 && arg.every(elt => typeof elt === 'boolean')
/**
 * Retourne true si arg est un tableau d'entiers non vide
 * @param arg
 */
export const isArrayOfIntegers = (arg: unknown): arg is number[] => Array.isArray(arg) && arg.length > 0 && arg.every(elt => Number.isInteger(elt))
/**
 * Retourne true si arg est un tableau de number finis non vide
 * @param arg
 */
export const isArrayOfNumbers = (arg: unknown): arg is number[] => Array.isArray(arg) && arg.length > 0 && arg.every(elt => Number.isFinite(elt))
/**
 * Retourne true si arg est un tableau non vide d'objets non vides
 * @param arg
 */
export const isArrayOfPlainObjects = (arg: unknown): arg is object[] => Array.isArray(arg) && arg.length > 0 && arg.every(elt => Object.prototype.toString.call(elt) === '[object Object]' && Object.keys(elt).length > 0)
/**
 * Retourne true si arg est un tableau de string non vide
 * @param arg
 */
export const isArrayOfStrings = (arg: unknown): arg is string[] => Array.isArray(arg) && arg.length > 0 && arg.every(elt => typeof elt === 'string')
/**
 * Retourne true si arg est un tableau de longueur nulle
 * @param arg
 */
export const isEmptyArray = (arg: unknown): arg is [] => Array.isArray(arg) && arg.length === 0

/**
 * Retourne le dernier élément d’un tableau
 * @param {Array} list
 * @return {*} Le dernier élément, ou undefined si list n’était pas un tableau non vide
 */
export function lastElement<T> (list: T[]): T | undefined {
  return (Array.isArray(list) && list.length > 0) ? list[list.length - 1] : undefined
}

/**
 * Déplace un élément dans le tableau (qui est modifié)
 * @param arr
 * @param oldIndex
 * @param newIndex
 */
export function arrayMove (arr: any[], oldIndex: number, newIndex: number): void {
  if (newIndex >= arr.length || newIndex < 0) throw TypeError(`nouvel index invalide ${newIndex}`)
  if (oldIndex >= arr.length || oldIndex < 0) throw TypeError(`ancien index invalide ${oldIndex}`)
  // on vire l’élément qui bouge
  const elt = arr.splice(oldIndex, 1)[0]
  // et on l’insère à sa nouvelle place
  arr.splice(newIndex, 0, elt)
}

/**
 * Fusionne des tableaux et dédoublonne les éléments
 * @param {...unknown} args Autant de tableaux ou d’éléments qu’on veut
 * @return {unknown[]} Un tableau qui dedup tous les éléments des tableaux (ou éléments) passés en arguments
 */
export function dedup (...args: Array<unknown>) {
  const listeVide: Array<unknown> = []
  const ar: Array<unknown> = listeVide.concat(...args)
  const s = new Set(ar)
  return Array.from(s)
}
