class Queue {
  /**
   * Notre pile interne de promesses, cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Classes/Private_class_fields#champs_dinstance_priv%C3%A9s
   * @private
   * @type {Promise}
   */
  #stack: Promise<void>

  /**
   * Pile d’exécution de fonctions, qui seront lancées en séquentiel, qu’elles soient sync ou async (dans ce cas elles doivent retourner une Promise)
   */
  constructor () {
    /**
     * La pile de promesse
     * @type {Promise<void>}
     * @private
     */
    this.#stack = Promise.resolve()
  }

  /* global VoidFunction */
  /**
   * Ajoute une fonction à la pile d’exécution
   * @param {function} fn
   * @param {errorCallback} errorCallback
   * @return {Promise} La promesse résolue ou rejetée, son traitement est facultatif car l’erreur a déjà été envoyée à errorCallback, peut servir pour savoir que fn a terminé
   */
  async add (fn: VoidFunction, errorCallback: VoidFunction): Promise<void> {
    if (typeof fn !== 'function' || typeof errorCallback !== 'function') throw Error('add réclame deux fonctions')
    const promise = this.#stack.then(fn)
    this.#stack = promise.catch(errorCallback)
    return await promise
  }
}

export default Queue

/**
 * @typedef errorCallback
 * @type function
 * @param {Error} error
 */
