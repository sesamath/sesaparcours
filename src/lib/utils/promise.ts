/** Attend delay ms avant de résoudre la promesse retournée */
export const sleep = (delay:number): Promise<void> => new Promise(resolve => { setTimeout(resolve, delay) })
