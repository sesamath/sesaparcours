/** @module lib/utils/net */
/**
 * Charge un json et retourne l’objet associé
 * @param {string} url
 * @return {Promise<any>}
 * @throws {Error} en cas de pb
 */
export async function fetchJson (url: string): Promise<any> {
  const response = await fetch(url)
  if (!response.ok) throw Error(`HTTP error on ${url} : ${response.status} ${response.statusText}`)
  return await response.json()
}

/**
 * Charge un fichier (html|txt|etc.) et retourne le contenu textuel
 * @param {string} url
 * @return {Promise<string>}
 * @throws {Error} en cas de pb
 */
export async function fetchText (url: string): Promise<string> {
  const response = await fetch(url)
  if (!response.ok) throw Error(`HTTP error on ${url} : ${response.status} ${response.statusText}`)
  return await response.text()
}

/**
 * Charge un xml et retourne l’objet Document associé
 * @param {string} url
 * @return {Promise<Document>}
 * @throws {Error} en cas de pb
 */
export async function fetchXml (url: string): Promise<Document> {
  const response = await fetch(url)
  if (!response.ok) throw Error(`HTTP error on ${url} : ${response.status} ${response.statusText}`)
  const xmlString = await response.text()
  const parser = new window.DOMParser()
  return parser.parseFromString(xmlString, 'text/xml')
}

/**
 * Résoud avec true si l'url correspond à une image (ça teste un chargement via <img> et répond true si y'a une taille)
 * @param url
 */
export function isImgUrl (url: string): Promise<boolean> {
  return new Promise((resolve) => {
    const img = new Image()
    // on peut pas avoir accès aux headers (dont content-type) avec cette méthode de chargement
    // mais si c'est une image on aura une taille
    img.onload = () => resolve(img.width > 0 && img.height > 0)
    img.onerror = () => resolve(false)
    img.src = url
  })
}

/**
 * Analyse url pour vérifier que c'est une image (essaie de récupérer l’url de l’image si url est celle de l’affichage de l’image sur un cloud connu)
 * @param url
 * @returns La promesse retournée l’url d’une image (la même ou la vraie url de l’image qu'on a retrouvée à partir de celle fournie)
 */
export async function checkImgUrl (url: string): Promise<string> {
  // Pour retrouver la vraie url de l'image si on nous fourni l'url d'une page de cloud,
  // il faut faire du fetch et regarder le header Content-Type
  // mais cela pose le pb de CORS, si le domaine tiers n'a pas mis de header
  // Access-Control-Allow-Origin sur cette url, le fetch plante et même avec un catch ça
  // pollue la console.
  // => si l'url ressemble à celle d'une image on se contente de isImgUrl et sinon on tente le fetch
  try {
    if (/\.(gif|jpeg|jpg|png|svg|webp)/.test(url)) {
      const isOk = await isImgUrl(url)
      if (isOk) return url
    }
    // cf https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch
    const response = await fetch(url, { redirect: 'follow', credentials: 'omit' })

    // cf https://developer.mozilla.org/fr/docs/Web/API/Response
    if (!response.ok) {
      // pas de throw car on veut pas tomber dans le catch plus bas, on retourne directement une promesse rejetée
      if (response.status === 404) return Promise.reject(Error(`${url} n’existe pas`))
      return Promise.reject(Error(`${url} retourne l’erreur ${response.status} ${response.statusText}`))
    }
    // on vérifie que c'est une image, cf https://developer.mozilla.org/en-US/docs/Web/API/Headers
    const contentType = response.headers.get('Content-Type')
    if (!contentType) return Promise.reject(Error(`${url} ne répond pas correctement (pas de Content-Type)`))
    if (/^image\//.test(contentType)) return url // c'était bien l’url d’une image
    if (contentType.startsWith('text/html')) return guessImgUrl(url, response)
    return Promise.reject(Error(`${url} ne correspond pas à une image (${contentType})`))
  } catch (error) {
    console.error(error)
    // erreur réseau, dns ou autre
    throw Error(`Impossible d’accéder à ${url} pour vérifier qu'il s'agit d'une image, probablement à cause d’un problème CORS (requête inter-domaine sans autorisation explicite du domaine visé)`)
  }
}

/**
 * Retourne l’url absolue du premier tag img trouvé dans htmlContent (string vide si on trouve pas)
 * @todo gérer les urls relatives
 * @param {string} htmlContent
 * @returns {string}
 */
function getImgSrc (htmlContent: string): string {
  const chunks = /<img[^>]+src *= *["'](https?:\/\/[^'"]+)/m.exec(htmlContent)
  return chunks?.[1] ?? ''
}

/**
 * Essaie de retrouver l’url de l’image dans la page web
 * @todo à tester et finir d’implémenter
 * @param url
 * @param response
 * @returns {Promise<string, Error>} La promesse retournée avec l’url de l’image retrouvée, ou une erreur si on y arrive pas
 */
async function guessImgUrl (url: string, response: Response): Promise<string> {
  const htmlContent = await response.text()
  // on passe en revue les clouds connus

  // google drive
  if (/^https:\/\/drive.google.com/.test(url)) {
    // une page d’un drive, on cherche dedans (c'est dans du code js) une url du genre https://*.googleusercontent.com/drive-viewer/*
    const chunks = /(https:\/\/[^.]+\.googleusercontent\.com\/drive-viewer\/[^/'"]+)/.exec(htmlContent)
    if (chunks?.[1] == null) throw Error(`Pas trouvé l’url de l’image dans la page ${url}`)
    return chunks[1].replace(/(=|\\u).*/, '')
  }

  // pcloud
  if (/^https:\/\/[a-z]+\.pcloud\.link/.test(url)) {
    // on cherche dans le source une ligne du genre
    // <meta property="og:image" content="https://eapi.pcloud.com/getpubthumb?code=xxx&fileid=yyy&size={width}x{height}&crop=0&type=jpg" />
    const chunks = /<meta property="og:image" content="([^"]+)"/.exec(htmlContent)
    if (chunks?.[1] == null) throw Error(`Pas trouvé l’url de l’image dans la page ${url}`)
    return chunks[1]
  }

  // diapo.php des manuels sesamath
  if (/^https:\/\/manuel\.sesamath\.(net|dev|local)\/numerique\/diapo.php\?atome=\d+/.test(url)) {
    const chunks = /<img +id=["']vignette["'] +src=["'](https:\/\/manuel\.sesamath\.[^"']+)["']/.exec(htmlContent)
    if (chunks?.[1] == null) throw Error(`Pas trouvé l’url de l’image dans la page ${url}`)
    return chunks[1]
  }

  // pas trouvé
  // throw Error(`${url} correspond à une page qui n’est pas celle d’un fournisseur connu, il faut fournir l’url de l’image voulue`)
  console.warn(`${url} ne correspond pas à un cloud géré, on va retourner l’url du premier tag <img> trouvé dans le contenu`, htmlContent)
  return getImgSrc(htmlContent)
}
