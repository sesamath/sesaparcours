/** @module lib/utils/number */

export interface Fraction {
  num: number
  den: number
}

/**
 * Retourne true si les nombres sont égaux en regardant 15 chiffres significatifs
 * Renverra false si les deux valent NaN (true s’ils sont tous les deux +Infinity ou -Infinity)
 * @param {number} nb1
 * @param {number} nb2
 * @return {boolean}
 * @throws {Error} si nb1 ou nb2 n’est pas un number
 */
export function areEqual (nb1: number, nb2: number) {
  return fixArrondi(nb1) === fixArrondi(nb2)
}

/**
 * Rectifie les erreurs d’arrondi (transforme 0.00033099999999999997 en 0.000331, 4059999,9999999995 en 4060000, etc.)
 * en ne conservant que 15 chiffres significatifs au maximum
 * @param {number} nb
 * @return {number}
 @throws {Error} si nb n’est pas un number
 */
export function fixArrondi (nb: number) {
  if (typeof nb !== 'number') throw Error(`Il faut passer un nombre : ${nb} (${typeof nb}) fourni`)
  if (Number.isInteger(nb)) return nb
  // on arrondi à 15 chiffres significatifs
  return Number(nb.toPrecision(15))
}

/**
 * Retourne la string 'x minutes y secondes' suivant le nb de secondes passées
 * @param secondes
 * @returns {string}
 */
export function getMinutesSecondesLabel (secondes: number): string {
  let label = ''
  if (secondes > 60) {
    const minutes = Math.floor(secondes / 60)
    label += `${minutes} minute${minutes > 1 ? 's' : ''} `
    secondes = secondes % 60
  }
  label += `${secondes} seconde${secondes > 1 ? 's' : ''}`
  return label
}

/**
 * Alias de Number.isInteger avec type checking de retour (pour inférer n en number si ça retourne true)
 */
export function isInteger (n: unknown): n is number {
  return Number.isInteger(n)
}

/**
 * Retourne n s’il est entier et defaultInteger sinon
 */
export function ensureInteger (n: unknown, defaultInteger: number): number {
  return isInteger(n) ? n : defaultInteger
}

/**
 * Retourne le pgcd de deux entiers (l’un des deux peut être nul mais pas les deux)
 * @param {number} n
 * @param {number} m
 * @param {Object} [options]
 * @param {Object} [options.acceptOnlyZeros=false] Passer true pour accepter (0, 0), ça retournera alors 0, sinon ça throw dans ce cas
 * @param {Object} [options.negativesAllowed=false] Passer true pour accepter les négatifs (le pgcd sera toujours positif)
 * @return {number}
 * @throws {Error} si le calcul du PGCD n’est pas possible (si on a pas fourni deux entiers positifs par ex)
 */
export function pgcd (n: number, m: number, { acceptOnlyZeros = false, negativesAllowed = false } = {}): number {
  // On vérifie d’abord que l’on a deux entiers positifs
  // en traitant le cas double 0 d’abord
  if (Math.abs(n) < 1e-10 && Math.abs(m) < 1e-10) {
    if (acceptOnlyZeros) return 0
    throw Error('Le calcul du pgcd de 0 et 0 n’est pas possible')
  }
  if (negativesAllowed) {
    if (n < 0) n = -n
    if (m < 0) m = -m
  }
  if (n >= 0 && m >= 0 && Math.abs(n - Math.round(n)) < 1e-10 && Math.abs(m - Math.round(m)) < 1e-10) {
    let a: number = Math.round(n)
    let b: number = Math.round(m)
    while (b !== 0) {
      // si b > a, alors r vaudra a et le premier tour de boucle inversera a et b
      const r = a % b
      a = b
      b = r
    }
    return a
  }
  throw Error(`Le calcul du pgcd de ${n} et ${m} n’est pas possible`)
}

/**
 * Retourne le pgcd des entiers positifs fournis (de 0 à n, mais pas tous nuls, sauf avec l’option acceptOnlyZeros où ça retournera 0 s’ils sont tous nuls)
 * @param {number[]} entiers
 * @param {Object} [options]
 * @param {Object} [options.acceptOnlyZeros=false] Passer true pour accepter des entiers tous nul, ça retournera alors 0, sinon ça throw dans de cas
 * @return {number}
 * @throws {Error} si le calcul du PGCD n’est pas possible (si on a pas fourni deux entiers positifs par ex)
 */
export function pgcdMulti (entiers: number[], { acceptOnlyZeros = false } = {}): number {
  if (!Array.isArray(entiers)) throw Error('arguments invalides')
  if (entiers.length < 2) throw Error('Il n’est pas possible de calculer un PGCD sans fournir au moins 2 entiers')
  // on dédoublonne en passant par un set
  const nombres: Set<number> = new Set()
  for (const nb of entiers) {
    if (!Number.isFinite(nb) || nb < -1e-10 || Math.abs(Math.round(nb) - nb) > 1e-10) throw Error(`argument invalide (${nb} n’est pas entier positif)`)
    nombres.add(Math.round(nb))
  }
  entiers = Array.from(nombres)
  // on a que des entiers ≥ 0, faut vérifier qu’ils sont pas tous nul
  if (!acceptOnlyZeros && entiers.every(nb => Math.abs(nb) < 1e-10)) throw Error('Il n’est pas possible de calculer un pgcd d’entiers tous nul')
  return entiers.reduce((nb, pgcdProvisoire, index) => (index === 0) ? nb : pgcd(nb, pgcdProvisoire, { acceptOnlyZeros }))
}

/**
 * Retourne un entier compris entre min et max (inclus)
 * @param {number} min
 * @param {number} max
 * @return {number}
 * @throws TypeError si min ou max ne sont pas entiers, ou si min > max
 */
export function randomInt (min: number, max: number): number {
  if (!Number.isInteger(min) || !Number.isInteger(max) || min > max) throw TypeError(`Arguments invalides (${min} et ${max})`)
  if (min === max) return min
  return Math.floor(Math.random() * (max - min + 1) + min)
}

/**
 * Retourne la puissance de 10 du nombre (2 pour 42.3 et -2 pour -0.24)
 * @param {number|BigInt} nb
 */
export function puissance10 (nb: number | bigint): number {
  if (typeof nb === 'bigint') {
    const max = BigInt(Number.MAX_SAFE_INTEGER)
    if (nb > max) throw RangeError(`BigInt trop grand : ${nb}`)
    if (nb < -max) throw RangeError(`BigInt négatif trop grand (en valeur absolue) : ${nb}`)
    if (nb > 0) return nb.toString().length - 1
    return nb.toString().length - 2
  }
  // on travaille sur la représentation en string du nb plutôt qu’avec Math.log10 pour des questions de perf
  const nbAbs = Math.abs(nb)
  const nbStr = String(nbAbs)
  // le cas 1e42 ou 1e-24
  if (nbStr.includes('e')) return Number(nbStr.replace(/.*e/, ''))
  if (nbAbs < 1) {
    if (nbAbs === 0) return 0
    // -(longueur de la chaîne après le point)
    return Number(nbStr.replace(/[^.]+\./, '').length) * -1
  }
  // longueur de la partie entière moins un
  return Number(nbStr.replace(/\..*/, '').length) - 1
}

/**
 * Retourne le nombre avec des parenthèses autour s’il est négatif
 * @param {number} nombre
 * @return {string}
 */
export function signeFois (nombre: number): string {
  return (nombre < 0) ? '(' + String(nombre) + ')' : String(nombre)
}

type TupleTwoNumbers = [number, number]

/**
 * Simplifie une fraction
 * @param {number} num
 * @param {number} den
 */
export function simplifieFraction (num: number, den: number): TupleTwoNumbers {
  if (den === 0) throw RangeError(`division par 0, ${num}/${den} n’est pas une fraction`)
  if (num === 0) return [0, 1]
  // dénominateur toujours positif
  if (den < 0) {
    den *= -1
    num *= -1
  }
  // si ce ne sont pas des entiers faut multiplier par la bonne puissance de 10
  const mult10 = [num, den].map(n => {
    if (Number.isInteger(n)) return 0
    const partDec = Math.abs(n - Math.trunc(n))
    return String(partDec).length - 2
  })
  const max = Math.max(...mult10)
  if (max > 0) {
    num *= Math.pow(10, max)
    den *= Math.pow(10, max)
  }
  // on regarde s’il y a un pgcd
  const div = pgcd(num, den, { negativesAllowed: true })
  if (div > 1) {
    num /= div
    den /= div
  }
  return [num, den]
}

/**
 * retourne le premier multiple de facteur qui sera supérieur à n
 * @param n
 * @param facteur
 */
export const premierMultipleSuperieur = (n: number, facteur: number) => {
  // Normalement on passe des entiers, mais au cas où on tronque ou on arrondit
  if (!Number.isInteger(n)) n = Math.floor(n)
  if (!Number.isInteger(facteur)) facteur = Math.round(facteur)
  if (n % facteur === 0) return n
  return n - n % facteur + facteur
}

/**
 * Formate un nombre pour affichage, avec séparateur de milliers et virgule (la notation scientifique est convertie en écriture décimale complète, même pour de grands nombres)
 * @param {string|number} nb
 * @param {Object} [options]
 * @param {string} [options.lang=fr] passer "en" pour avoir un séparateur décimal "." et séparateur de millier ",", toutes les autres valeurs seront ignorées
 */
export function formatNumber (nb: number | string, { lang = 'fr', maxDecimales = null } = {}): string {
  let decimalSep = ','
  let thousandSep = ' '
  if (lang === 'en') {
    decimalSep = '.'
    thousandSep = ','
  }
  if (typeof nb !== 'number') {
    if (!nb || typeof nb !== 'string') {
      throw Error(`Il faut passer un nombre : ${nb}`)
    }
    // nb est une string non vide, on accepte un nombre déjà formaté
    nb = Number(nb.replace(decimalSep, '.').replaceAll(thousandSep, ''))
  }
  if (!Number.isFinite(nb)) throw Error(`Il faut passer un nombre fini : ${nb}`)
  // nb est un number
  let nbStr
  if (Number.isInteger(maxDecimales) && maxDecimales! >= 0) {
    if (maxDecimales === 0) return String(Math.round(nb))
    nbStr = nb.toFixed(maxDecimales!)
  } else {
    nbStr = nb.toPrecision(15)
  }
  // nbStr est un nb en string avec séparateur décimal point "presque" toujours présent (sauf si y’avait pile 15 chiffres)
  if (!nbStr.includes('.')) nbStr += '.0' // sera viré plus loin
  // on a forcément un point avec au moins un chiffre derrière
  const chunks = /([-+])?(\d+)\.(\d+)?(e[+-][0-9]+)?/.exec(nbStr)
  if (!chunks) {
    console.error(Error(`Il y a un bug dans prettyNumber (pas trouvé nos petits dans ${nbStr}`))
    return nbStr.replace('.', ',')
  }
  let [, signe = '', ent = '', dec = '', sci = ''] = chunks
  // on vire le + éventuel
  if (signe !== '-') signe = ''
  // les chiffres en array de strings
  const chiffresPe = Array.from(ent)
  const chiffresPd = Array.from(dec)

  // si y’a une notation scientifique, on passe en écriture décimale complète
  if (sci) {
    const [, signe, exp] = /e([+-])(\d+)/.exec(sci)!
    let expo = Number(exp) ?? 0
    if (signe === '-') {
      while (expo) {
        // faut décaler la virgule de exp vers la droite
        chiffresPd.unshift(chiffresPe.pop() ?? '0')
        expo--
      }
      // si y’a a plus de partie entière faut remettre 0
      if (!chiffresPe.length) chiffresPe.push('0')
    } else {
      // faut décaler vers la gauche
      while (expo) {
        chiffresPe.push(chiffresPd.shift() ?? '0')
        expo--
      }
    }
  }

  let partieEntiere = ''
  while (chiffresPe.length) {
    if (partieEntiere) partieEntiere = thousandSep + partieEntiere
    partieEntiere = chiffresPe.splice(-3).join('') + partieEntiere
  }
  let partieDecimale = ''
  while (chiffresPd.length) {
    if (partieDecimale) partieDecimale += thousandSep
    partieDecimale += chiffresPd.splice(0, 3).join('')
  }
  // on vire les 0 non significatifs (et éventuels espaces de fin)
  if (partieDecimale) partieDecimale = partieDecimale.replace(/[0 ]+$/, '')
  // le séparateur si besoin
  if (partieDecimale) partieDecimale = decimalSep + partieDecimale
  // et on retourne le tout
  return signe + partieEntiere + partieDecimale
}
