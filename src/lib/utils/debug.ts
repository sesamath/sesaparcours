import { addElement, destroy } from 'src/lib/utils/dom/main'

/** @module lib/utils/debug */

interface DivConsole extends HTMLElement {
  resume: () => void
}

interface EltWithHistory extends HTMLElement {
  history: Array<{
    mutations: MutationRecord[]
    html: string
  }>
}

/**
 * Démarre la surveillance de parent pour vérifier que l’on y supprime pas domIdToTrack
 * @param {HTMLElement} parent
 * @param {string} domSelector
 * @returns {MutationObserver} observer Appeler observer.disconnect() pour arrêter le tracking
 */
export function startDomDeletionTracking (parent: HTMLElement, domSelector: string): MutationObserver | undefined {
  const elts: Node[] = Array.from(document.querySelectorAll(domSelector))
  if (elts.length < 1) {
    console.error(Error(`Aucun élément à surveiller, le sélecteur ${domSelector} ne remonte aucun élément`))
    return
  }
  console.debug('tracking deletion on', elts, 'from', parent)
  const observer = new MutationObserver(function (mutations) {
    if (mutations.some(mutation => Array.from(mutation.removedNodes).some((node: Node) => elts.includes(node)))) {
      // eslint-disable-next-line no-console
      console.trace('MUTATIONS avec suppression d’un des éléments de', elts, 'dans', parent, mutations)
    }
  })
  observer.observe(parent, { subtree: true, childList: true })
  return observer
}

/**
 * Démarre la surveillance de elt et lui ajoute une propriété history dans laquelle on colle toutes les mutations
 * @param {HTMLElement} elt
 * @param {Object} [options]
 * @param {boolean} [options.withLog]
 * @param {boolean} [options.withTrace]
 * @returns {MutationObserver|void} observer Appeler observer.disconnect() pour arrêter le tracking
 */
export function startMutationHistory (elt: EltWithHistory, {
  withLog = false,
  withTrace = false
} = {}): MutationObserver | undefined {
  if (elt.parentNode == null) {
    console.error(Error('élément sans parentNode, impossible de le surveiller'), elt)
    return
  }
  // on a un elt avec parent, on peut init l’historique
  elt.history = [{ mutations: [], html: elt.outerHTML }]
  const observer = new MutationObserver(function (mutations) {
    elt.history.push({ mutations, html: elt.outerHTML })
    // eslint-disable-next-line no-console
    if (withTrace) console.trace('mutations sur', elt, mutations)
    else if (withLog) console.debug('mutations sur', elt, mutations)
  })
  observer.observe(elt.parentNode, { subtree: true, attributes: true, characterData: true, childList: true })
  return observer
}
type GlobalChecker = (arg: string) => boolean
/**
 * Ajoute une fct globale globalFctName (pour pouvoir l’utiliser dans la console du navigateur)
 * qui vérifie que #{id} est toujours elt et râle en console sinon (avec la trace)
 * @param {HTMLElement} elt
 * @param {string} id
 * @param {string} globalFctName
 * @param {Object} [options]
 * @param {boolean} [options.withLog] Passer true pour ajouter une ligne de log quand l’élément id existe au check
 * @returns {Function} La fct mise en global (dans window[globalFctName])
 */
export function addGlobalChecker (elt: HTMLElement | null, id: string, globalFctName: string, { withLog = false } = {}): GlobalChecker {
  const checker: GlobalChecker = (msgPrefix = `${globalFctName} call : `) => {
    const result = (document.getElementById(id) === elt)
    if (result) {
      if (withLog) console.debug(`check #${id} ok`)
    } else {
      // eslint-disable-next-line no-console
      console.trace(`${msgPrefix}#${id} KO`)
    }
    return result
  }
  // @ts-ignore (on affecte une var globale quelconque et on a pas envie de toutes les déclarer dans window)
  window[globalFctName] = checker
  return checker
}

/**
 * Ajoute la sortie de console à l’écran, dans un div mis dans container (10 derniers appels seulement, les précédents sont effacés au fur et à mesure)
 * Très utile pour le debug sur tablette / téléphone (ou tout autre device sans devTools)
 * @param {HTMLElement} container
 * @return {HTMLElement} le div ajouté, appeler div.resume() pour le supprimer et remettre la console comme elle était
 */
export function addConsoleOnScreen (container: HTMLElement): DivConsole {
  console.debug('addConsoleOnScreen dans', container)
  // on ajoute un div pour afficher ce qui sortirait en console, pour le debug sur iPad
  // eslint-disable-next-line no-console
  const consoleLog = console.log
  const consoleError = console.error
  const consoleWarn = console.warn
  const consoleDebug = console.debug
  const resume = (): void => {
    // eslint-disable-next-line no-console
    console.log = consoleLog
    console.error = consoleError
    console.warn = consoleWarn
    console.debug = consoleDebug
    destroy(divConsole)
  }

  const divConsole: DivConsole = Object.assign(addElement(container, 'div', {
    style: {
      height: '300px',
      overflow: 'auto'
    }
  }), { resume })
  const pList: HTMLElement[] = []
  const purge = (): void => {
    while (pList.length > 10) {
      const el: HTMLElement | undefined = pList.shift()
      if (el !== undefined) destroy(el) // parce que shift() peut retourner undefined si pList est vide et que Typescript ne sait pas qu’il n’est pas vide :-/
    }
  }

  const addToScreen = (p: HTMLElement): void => { // à priori, le type HTMLElement inclus de par son héritage de Element, une propriété scollIntoView
    pList.push(p)
    purge()
    p.scrollIntoView(false)
  }
  // eslint-disable-next-line no-console
  console.log = function (...args) {
    // on sort quand même dans la vraie console
    consoleLog.call(console, ...args)
    const p = addElement(divConsole, 'p', { content: Array.from(args).map(String).join(' ') })
    addToScreen(p)
  }
  console.error = function (...args) {
    consoleError.call(console, ...args)
    const content = 'Error : ' + Array.from(args).map(arg => (arg instanceof Error) ? `${arg.message}\n${String(arg.stack)}` : String(arg)).join(' ')
    const style = { color: '#a00' }
    const p = addElement(divConsole, 'p', { content, style })
    addToScreen(p)
  }
  console.warn = function (...args) {
    consoleWarn.call(console, ...args)
    const content = 'Warning : ' + Array.from(args).map(String).join(' ')
    const style = { color: '#7c3204' }
    const p = addElement(divConsole, 'p', { content, style })
    addToScreen(p)
  }
  console.debug = function (...args) {
    consoleDebug.call(console, ...args)
    const p = addElement(divConsole, 'p', { content: Array.from(args).map(String).join(' ') })
    addToScreen(p)
  }
  return divConsole
}
