import { j3pElement } from 'src/legacy/core/functions'

export function setMathliveCss (container) {
  if (!container) container = document.body
  if (typeof container === 'string') container = j3pElement(container)
  container.classList.add('mlOverrides')
  // Ligne suivante parce que sinon pour les matrices on ne voit plus grand chose en cas d’erreur de syntaxe
  // Plus nécessaire avec la  version 0.98.2 de Mathlive
  // document.body.style.setProperty('--contains-highlight-background-color', 'transparent')
}
