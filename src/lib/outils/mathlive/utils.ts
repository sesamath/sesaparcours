// La fonction suivante a été créé car les indices des \editable{} sont traités de façon différente pat
import { j3pElement, j3pRestrict } from 'src/legacy/core/functions'
import type { InputWithKeyboard, MathfieldWithKeyboard } from 'src/lib/types'
import { focusIfExists } from 'src/lib/utils/dom/main'
import { convertRestriction } from 'src/lib/utils/regexp'
// Mathlive et MathQuill
import { getIndexFermant } from 'src/lib/utils/string'

/**
 * Fonction filtrant les caractères pouvant être tapés au clavier dans un éditeur Mathlive
 * @param {MathfieldWithKeyboard | InputWithKeyboard | string} input éditeur Mathlive ou input muni d’un clavier
 * @param {string|regex} restriction
 */
export function mathliveRestrict (input: MathfieldWithKeyboard | InputWithKeyboard | string, restriction: string | RegExp) {
  // La variable input est peut-être un string à l’entrée dans cette fonction. On doit utiliser l'"Element" associé.
  const realInput = typeof input === 'string' ? j3pElement(input) : input
  if (!realInput) return // j3pElement a déjà râlé en console
  if (!restriction) restriction = /./ // on autorise tout
  if (typeof restriction === 'string') restriction = convertRestriction(restriction)
  realInput.addEventListener('keypress', function (event) {
    j3pRestrict(realInput, event, restriction)
  })
}

/**
 * Fonction crée un bouton servant à faire basculer la visibilité d’un éditeur Mathlive ou un éditeur
 * de texte auquel est associé un clavier virtuel
 * @param {MathfieldWithKeyboard | InputWithKeyboard} editor
 * @returns {HTMLButtonElement}
 */
export function createToggleButton (editor: MathfieldWithKeyboard | InputWithKeyboard) {
  const button = document.createElement('button')
  editor.triggerBtn = button
  button.innerHTML = '⇑'
  button.tabIndex = -1
  // Pas trouvé moyen de faire ce qui suit via l css, la classe ML__virtual-keyboard-toggle prenant le dessus
  const style = button.style
  style.minHeight = '25px'
  // style.textAlign = 'center'
  style.display = 'inline-block'
  style.width = '22px'
  style.height = '100%'
  style.backgroundColor = 'rgba(65, 154, 243, 0.2)'
  button.classList.add('ML__virtual-keyboard-toggle')
  const onclick = (event: Event) => {
    event.stopPropagation()
    if (editor.virtualKeyboard) editor.virtualKeyboard.changeKbdVisibility(button.innerHTML)
    // ça arrive, cf https://app.bugsnag.com/sesamath/j3p/errors/667282d21de2660008a84ccf
    else console.error(Error('Cet input a perdu son VirtualKeyboard'), editor)
  }
  button.addEventListener('mousedown', onclick, { capture: true, passive: false })
  // Ne pas mettre la ligne suivante car sur écran tactile on a alors deux événements fired
  // button.addEventListener('touchstart', onclick, { capture: true, passive: false })
  return button
}

/**
 * Donne le focus à un placeholder mathfield
 * @param {MathfieldWithKeyboard|string} mathFieldElt
 * @param {string} placeholderId
 */
export function focusPlaceholder (mathFieldElt: MathfieldWithKeyboard | string, placeholderId: string) {
  const mfe = (typeof mathFieldElt === 'string' ? j3pElement(mathFieldElt) : mathFieldElt) as MathfieldWithKeyboard
  if (!mfe) return // j3pElement a déjà râlé en console
  // pour donner le focus au placeholder on réécrit dedans son contenu
  const range = mfe.getPromptRange(placeholderId)
  // @ts-expect-error // Problème de typage dans Mathlive, mais c'est censé fonctionner correctement.
  if (range != null) mfe.selection = range
  // pour info, il y avait des versions avec
  const content = mfe.getPromptValue(placeholderId)
  mfe.focus() // Indispensable sinon le clavier virtuel n’apparaît pas
  mfe.setPromptValue(placeholderId, content, {})
  // puis on donne le focus au mathFieldElt
  focusIfExists(mathFieldElt)
}

/**
 * Fonction remplaçant dans la chaîne LaTeX ch les matrices par leurs transposées
 * @param {string} ch la chaîne contenant le code LaTeX
 * @returns {string} Une chaîne contenant le code LaTeX correspondant dans lequel les matrices onté été transposées
 */
export function transposeMatrices (ch: string) {
  let st = ch
  let indstart = 0
  const debmat = '\\begin{matrix}'
  const lendebmat = debmat.length
  const finmat = '\\end{matrix}'
  const lenfinmat = finmat.length
  while ((indstart = st.indexOf(debmat, indstart)) !== -1) {
    const indFin = st.indexOf(finmat, indstart)
    const indmatsuiv = st.indexOf(debmat, indstart + 1)
    if (indFin === -1 || ((indmatsuiv !== -1) && (indFin > indmatsuiv))) return ch
    const cont = st.substring(indstart + lendebmat, indFin)
    const tab: string[][] = []
    const tablig: string[] = cont.split('\\\\')
    // Si la matrice n’est formée que d’une seule ligne
    for (let i = 0; i < tablig.length; i++) {
      const ligne = tablig[i]
      if (ligne != null) tab.push(ligne.split('&'))
    }
    const nblig = tab.length
    if (tab[0] == null) throw Error('Pas de première ligne dans cette matrice !')
    const nbcol = tab[0].length
    const tabres = []
    for (let i = 0; i < nbcol; i++) {
      const lig = []
      for (let j = 0; j < nblig; j++) {
        const ligne = tab[j]
        if (ligne != null) lig.push(ligne[i])
      }
      tabres.push(lig)
    }
    let rescont = ''
    tabres.forEach((lig, indexlig) => {
      if (indexlig !== 0) rescont += '\\\\'
      lig.forEach((col, indexcol) => {
        if (indexcol !== 0) rescont += '&'
        rescont += col
      })
    })
    st = st.substring(0, indstart + lendebmat) + rescont + st.substring(indFin)
    indstart = indFin + lenfinmat + rescont.length - cont.length // Les deux derniers pour tenir compte du fait que la longueur de st a changé
  }
  return st
}

/**
 * Fonction qui remplace les anciens codes mathquill pour écrire des systèmes par le code LaTeX
 * @param {string} ch
 * @returns {*}
 */
function replaceMQSystemCodes (ch: string) {
  if (!ch.includes('\\system')) return ch
  const st = ch
  const tab = ['two', 'three']
  // Ne pas utiliser ici un forEach à cause des return en cas d’erreur
  for (let index = 0; index < tab.length; index++) {
    const val = tab[index]
    const deb = '\\system' + val
    while (ch.includes(deb)) {
      const inddeb = ch.indexOf(deb)
      let indstart = ch.indexOf('{', inddeb + deb.length)
      if (indstart === -1) return st // Pb de syntaxe
      let remp = '\\left\\{\\begin{array}{l}'
      const nbarg = index + 2 // Nombre de lignes du système
      for (let i = 0; i < nbarg; i++) {
        indstart = ch.indexOf('{', indstart)
        if (indstart === -1) return st // Erreur
        const indacf = getIndexFermant(ch, indstart) // On recherche l’accolade correspondante
        if (indacf === -1) return st // Pb de syntaxe
        const contacc = ch.substring(indstart + 1, indacf)
        if (contacc === '') return st // Une des accolades ne contient rien : faute de syntaxe
        if (i !== 0) remp += '\\\\'
        remp += contacc
        indstart = indacf + 1
      }
      ch = ch.substring(0, inddeb) + remp + '\\end{array} \\right.' + ch.substring(indstart)
    }
  }
  return ch
}

/**
 * // @todo commenter cette fonction
 * @param {string} ch
 * @return {string}
 */
export function replaceMQMatrixCodes (ch: string) {
  if (!ch.includes('\\matrix')) return ch
  const st = ch
  const tab = ['one', 'two', 'three', 'four', 'five', 'six']
  // Ne pas utiliser ici un forEach à cause des return en cas d’erreur
  for (let ind1 = 0; ind1 < tab.length; ind1++) {
    const val1 = tab[ind1]
    for (let ind2 = 0; ind2 < tab.length; ind2++) {
      const val2 = tab[ind2]
      const deb = '\\matrix' + val1 + val2
      while (ch.includes(deb)) {
        const inddeb = ch.indexOf(deb)
        let indstart = ch.indexOf('{', inddeb + deb.length)
        if (indstart === -1) return st // Pb de syntaxe
        let remp = '\\begin{matrix}'
        const nblig = ind1 + 1
        const nbcol = ind2 + 1
        for (let i = 0; i < nblig * nbcol; i++) {
          indstart = ch.indexOf('{', indstart)
          if (indstart === -1) return st // Erreur
          const indacf = getIndexFermant(ch, indstart) // On recherche l’accolade correspondante
          if (indacf === -1) return st // Pb de syntaxe
          const contacc = ch.substring(indstart + 1, indacf)
          if (contacc === '') return st // Une des accolades ne contient rien : faute de syntaxe
          const indcol = i % nbcol
          if (indcol === 0) remp += ((i !== 0) ? '\\\\ ' : ' ') + contacc + ' '
          else remp += ' & ' + contacc + ' '
          indstart = indacf + 1
        }
        ch = ch.substring(0, inddeb) + remp + '\\end{matrix}' + ch.substring(indstart)
      }
    }
  }
  return ch
}

/**
 * @todo Commenter cette fonction
 * @param {string} ch
 * @return {string}
 */
export function replaceOldMQCodes (ch: string) {
  return ajouteParMatSansPar(replaceMQSystemCodes(replaceMQMatrixCodes(ch)))
}

/**
 * Fonction qui repère dans la chaîne ch (qui contient du code LaTeX) les matrices qui ne sont précédées
 * ni par un \left( ni par un \left| et rajoute les parenthèses manquantes.
 * Utilisée pour compatibilité avec les anciens exercices Mathquill
 * @param {string} ch
 * @returns {string}
 */
export function ajouteParMatSansPar (ch: string) {
  let st = ch
  let indstart = 0
  let indMat
  const chstart = '\\left('
  const chend = '\\right)'
  const debmat = '\\begin{matrix}'
  const finmat = '\\end{matrix}'
  const lenfinmat = finmat.length
  while ((indMat = st.indexOf(debmat, indstart)) !== -1) {
    const indFin = st.indexOf(finmat, indMat)
    const indmatsuiv = st.indexOf(debmat, indMat + 1)
    if (indFin === -1 || ((indmatsuiv !== -1) && (indFin > indmatsuiv))) return ch
    const deb = st.substring(0, indMat).trimEnd()
    if (!deb.endsWith('\\left(') && !deb.endsWith('\\left|')) {
      st = st.substring(0, indMat) + chstart + st.substring(indMat, indFin + lenfinmat) + chend + st.substring(indFin + lenfinmat)
    }
    indstart = indFin + lenfinmat
  }
  return st
}

/**
 * Fonction qui recherche dans le code LaTeX conteu dans ch les matrices, remplace dans leur contenu les \frac{
 * par des \dfrac{ et qui gère l’espacement entre les lignes suivant qu’elles contiennent ou non des \frac{
 * @param ch
 * @returns {*|string}
 */
export function traiteEspacementLignesMatrices (ch: string) {
  const debmat: [string, string] = ['\\begin{matrix}', '\\begin{array}']
  const finmat: [string, string] = ['\\end{matrix}', '\\end{array}']
  if (!ch.includes(debmat[0]) && !ch.includes(debmat[1])) return ch
  let st = ch
  // Pas de forEach ici à cause  des return en cas d’erreur
  const indices: (0 | 1)[] = [0, 1] // tout ce cirque pour que typescript ne craigne pas que debmat[ind] puisse être undefined !
  for (const ind of indices) {
    const val: string = debmat[ind]
    let indstart = 0
    const lendebmat = val.length
    while ((indstart = st.indexOf(val, indstart)) !== -1) {
      const indFin = st.indexOf(finmat[ind], indstart)
      if (indFin === -1) return ch
      const content = st.substring(indstart + lendebmat, indFin)
      const tab = content.split('\\\\')
      const newtab: string[] = []
      let frac = false
      let dfrac = false
      tab.forEach((value) => {
        if (dfrac) {
          newtab.push('[1em]' + value)
        } else {
          if (frac) {
            newtab.push('[0.5em]' + value)
          } else newtab.push(value)
        }
        frac = value.includes('\\frac')
        dfrac = value.includes('\\dfrac')
      })
      const newcontent = newtab.join('\\\\')
      st = st.substring(0, indstart + lendebmat) + newcontent + st.substring(indFin)
      indstart = indFin + newcontent.length - content.length
    }
  }
  return st
}

/**
 * Nettoie la chaîne strLaTex en retirant les éléments que Mathlive n’a pas traité
 * @param {string} strLatex
 * @param {Object} [options]
 * @param {boolean} [options.keepText=false] passer true pour séparer tous les \text{} par leur contenu suivi d’un <br>
 * @returns {string}
 */
export function cleanLatexForMl (strLatex: string, { keepText = false } = {}) {
  if (typeof strLatex !== 'string') {
    console.error(Error('Il faut passer une string'))
    return ''
  }
  // si y’a rien à faire on shunt
  if (!strLatex) return ''
  let ch = strLatex
    // ça semblerait plus logique de remplacer les \n par des <br>, mais il y avait une bonne raison de les virer complètement (indiquer ici laquelle)
    .replace(/\^{}/g, ' ') // On retire les exposants vides
    .replace(/{\^/g, '{') // Pour corriger les puissances de puissances involontaires
    .replace(/\n/g, ' ')
    .replace(/\\,/g, ' ')
    // On remplace les séparateurs décimaux . par des virgules
    .replace(/(\d)\.(\d)/g, '$1,$2')
    // .replace(/\\mathbb\{R}/g, '\\R')
    .replace(/\$\$/g, ' ') // A revoir mais on en trouve dans certaines ressources et Mathlive n’aime pas
    .replace(/<br>/g, '\n').replace(/<BR>/g, '\n')
    .replace(/\\;/g, '') // On vire le \; qu’on a rajouté au début pour faciliter le focus sur les éditeurs Mathlive
    .replace((/\\vecteur{([a-zA-Z])}/g), '\\vec{$1}') // Ancien code Mathquill plus utilisé
    .replace(/\\vecteur/g, '\\overrightarrow')
  // transformation des \text éventuels, on ne récupère que leur contenu, et on les sépare par des <br>
  if (!keepText && ch.includes('\\text{')) {
    // Pour l’écriture des systèmes d’équations, le \left{ ne correspondent pas à un caractère fermant }
    // On les remplace provisoirement pour les remplacer de nouveau à la fin. Idem pour les \right\}
    ch = ch.replace(/\\left\\{/g, '\\leftt').replace(/\\right\\}/g, '\\rightt')
    let i = ch.indexOf('\\text{')
    // faut chercher l’accolade fermante de ce \text
    let res = ''
    while (i !== -1) {
      const idaf = getIndexFermant(ch, i + 5)
      if (idaf === -1) {
        console.error(Error(`Accolade fermante manquante dans : ${ch}`))
        // on laisse tout
        res += ch.substring(i + 6)
        break
      }
      // trouvé le fermant, on met ça sur une ligne
      res += ch.substring(i + 6, idaf) + '<br>'
      i = ch.indexOf('\\text{', idaf + 1)
    }
    // On vire un éventuel <br> de fin
    ch = res.replace(/<br>$/, '')
    ch = ch.replace(/\\leftt/g, '\\left\\{').replace(/\\rightt/g, '\\right\\}')
  }
  // transformation des \lim éventuels inutile maitenant avec Mathlive
  return ch
}

/**
 * Corrige un code LateX concernant des array avec un {l} et sans \\ (array ligne)
 * Mathjax affiche cela comme une matrice colonne mais Mathlive comme une matrice ligne
 * @param ch la chaîne à traiter
 * @returns la chaîne traitée (éventuellement identique)
 */
export function correctArrayLignes (ch: string): string {
  if (typeof ch !== 'string') throw Error('Argument invalide (pas une chaîne)')
  let st = ch
  let iStart = 0
  const searchStart = '\\begin{array}{l}'
  const searchStartLength = searchStart.length
  const searchEnd = '\\end{array}'
  const searchEndLength = searchEnd.length
  while ((iStart = st.indexOf(searchStart, iStart)) !== -1) {
    const iEnd = st.indexOf(searchEnd, iStart)
    const iNext = st.indexOf(searchStart, iStart + 1)
    if (iEnd === -1 || ((iNext !== -1) && (iEnd > iNext))) return ch
    const content = st.substring(iStart + searchStartLength, iEnd)
    if (!content.includes('\\\\')) { // array ligne. Avec le {l} Mathlive l’affiche comme une colonne au lieu d’une ligne
      // On supprime le {l}, paraît que c’est du code LaTeX incorrect
      st = st.substring(0, iStart) + '\\begin{array}' + content + st.substring(iEnd)
      iStart = iEnd + searchEndLength - 3 // On a retiré les 3 caractères {l}
    } else {
      iStart = iEnd + searchEndLength
    }
  }
  return st
}
