import { addElement, empty, isHtmlElement } from 'src/lib/utils/dom/main'
import { loadAceEditor } from 'src/lib/outils/ace/loadAce'

import { Ace } from 'ace-builds/ace.d'

interface EditorValues {
  inputElt: HTMLElement
  outputElt: HTMLElement
  withConsole: boolean
}

class Editor {
  private readonly editorElt: HTMLElement
  private readonly outputElt: HTMLElement
  private readonly consoleElt?: HTMLElement
  private editor?: Ace.Editor

  constructor ({ inputElt, outputElt, withConsole }: EditorValues) {
    if (!isHtmlElement(inputElt)) throw Error('Il faut fournir un élément pour l’éditeur')
    empty(inputElt)
    /**
     * Le conteneur de l’éditeur
     * @type {HTMLDivElement}
     */
    this.editorElt = addElement(inputElt, 'div')

    if (withConsole) {
      /**
       * Le conteneur de la console
       * @type {HTMLDivElement}
       */
      this.consoleElt = addElement(inputElt, 'div')
    }
    empty(outputElt)
    this.outputElt = addElement(outputElt, 'div')
  }

  async init (lang: string, initialCode: string): Promise<void> {
    const editor = await loadAceEditor(this.editorElt)
    editor.session.setMode(`ace/mode/${lang}`)
    editor.setValue(initialCode)
    editor.resize()
    editor.focus()
    this.editor = editor
  }

  getCode (): string {
    if (this.editor == null) throw Error('L’éditeur n’a pas été initialisé')
    return this.editor?.getValue()
  }

  setCode (code: string): void {
    if (this.editor == null) throw Error('L’éditeur n’a pas été initialisé')
    this.editor.setValue(code)
  }

  getConsoleOutput (): string {
    return this.consoleElt?.innerText ?? ''
  }

  getOutput (): string {
    return this.outputElt.innerText
  }
}

export default Editor
