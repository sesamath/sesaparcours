/** @module lib/outils/ace/loadSkulpt */
import { j3pBaseUrl } from 'src/lib/core/constantes'
import { loadJs } from 'src/lib/utils/dom/main'
import { loadAcePythonEditor } from './loadAce'

import { Sk } from './types.d'

/**
 * Charge Skulpt (interpréteur python) et lui affecte un éditeur ace configuré pour python (et chargé également)
 * @param container
 */
export default async function loadSkulpt (container: HTMLElement): Promise<Sk> {
  // faut du chargement séquentiel
  await loadJs(j3pBaseUrl + 'externals/editeurs/skulpt/skulpt.min.js')
  await loadJs(j3pBaseUrl + 'externals/editeurs/skulpt/skulpt-stdlib.js')
  if (window.Sk == null) throw Error('Le chargement de Skulpt (interpréteur python) a échoué')
  window.Sk.editor = await loadAcePythonEditor(container)
  return window.Sk
}
