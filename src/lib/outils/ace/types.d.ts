// on pique ça dans node_modules/@types/ace/index.d.ts (https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/ace/index.d.ts)
// mais on ne garde pas ce module car on a déjà
// node_modules/.pnpm/ace-builds@1.15.0/node_modules/ace-builds/ace.d.ts qui contient plus de choses
// (dont editor.completers)

import { IEditSession } from 'ace-builds/ace.d'
import { type Editor } from 'ace-builds/ace.d'

interface Completion {
  value: string;
  meta: string;
  type?: string | undefined;
  caption?: string | undefined;
  snippet?: any;
  score?: number | undefined;
  exactMatch?: number | undefined;
  docHTML?: string | undefined;
}
interface Position {
  row: number;
  column: number;
}
interface TokenInfo {
  type: string;
  value: string;
  index?: number | undefined;
  start?: number | undefined;
}

interface Tokenizer {
  /**
   * Returns an object containing two properties: `tokens`, which contains all the tokens; and `state`, the current state.
   */
  removeCapturingGroups(src: string): string;
  createSplitterRegexp(src: string, flag?: string): RegExp;
  getLineTokens(line: string, startState: string | string[]): TokenInfo[];
}

interface TextMode {
  getTokenizer(): Tokenizer;
  toggleCommentLines(state: any, session: IEditSession, startRow: number, endRow: number): void;
  toggleBlockComment(state: any, session: IEditSession, range: Range, cursor: Position): void;
  getNextLineIndent(state: any, line: string, tab: string): string;
  checkOutdent(state: any, line: string, input: string): boolean;
  autoOutdent(state: any, doc: Document, row: number): void;
  createWorker(session: IEditSession): any;
  createModeDelegates(mapping: { [key: string]: string }): void;
  transformAction(state: string, action: string, editor: Editor, session: IEditSession, text: string): any;
  getKeywords(append?: boolean): Array<string | RegExp>;
  getCompletions(state: string, session: IEditSession, pos: Position, prefix: string): Completion[];
}

export { Editor }

export interface Ace {
  /**
   * Provides access to require in packed noconflict mode
   * @param moduleName
   **/
  require(moduleName: string): any;

  /**
   * Embeds the Ace editor into the DOM, at the element provided by `el`.
   * @param el Either the id of an element, or the element itself
   **/
  edit(el: HTMLElement | string): Editor;

  /**
   * Creates a new [[EditSession]], and returns the associated [[Document]].
   * @param text {:textParam}
   * @param mode {:modeParam}
   **/
  createEditSession(text: string | Document, mode: TextMode): IEditSession;
}

export interface Sk {
  editor?: Editor
}

declare global {

  interface Window {
    ace?: Ace
    Sk?: Sk
  }
}
