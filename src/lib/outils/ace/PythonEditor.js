import Editor from './Editor'
import loadSkulpt from './loadSkulpt'

/**
 * Un éditeur python avec l’interpréteur skulpt (utiliser basthon/Editor pour la même chose avec basthon,
 * qui utilise l’interpréteur pyodide, un peu plus lourd mais plus performant, cf https://basthon.fr/about.html)
 */
class PythonEditor extends Editor {
  constructor ({ inputElt, outputElt, withConsole }) {
    super({ inputElt, outputElt, withConsole })
  }

  async init (initialCode) {
    const Sk = await loadSkulpt(this.editorElt)
    this.editor = Sk.editor
    this.setCode(initialCode)
  }

  verifDeuxPoints () {
    const regexes = ['def', 'for', 'if', 'while', 'elif', 'else'].map(mc => new RegExp(`^\t*${mc}`))
    const reEndOk = /:$/
    const linesWithPb = []
    this.getCode().split('\n').forEach((line, i) => {
      if (regexes.some(re => re.test(line) && !(reEndOk.test(line)))) linesWithPb.push(i + 1)
    })
    return linesWithPb
  }
}

export default PythonEditor
