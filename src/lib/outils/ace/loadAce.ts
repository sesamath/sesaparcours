/**
 * On ne passe pas par vite pour ace-build, trop lourd (consomme énormément de RAM et demande des plugin webpack)
 * On charge donc via loadJs docroot/externals/editeurs/ace/
 * (qui a été peuplé avec `rsync -av --delete node_modules/ace-builds/src-min-noconflict/ docroot/externals/editeurs/ace/`)
 *
 * Pour les types, il ne faut pas avoir à la fois ace-builds et @types/ace dans les dépendances, ça se crêpe le chignon…
 * (on recrée une déclaration minimaliste dans types.d.ts pour le type de window.ace)
 * @see https://stackoverflow.com/questions/46501568/import-ace-code-editor-into-webpack-es6-typescript-project
 * @see https://github.com/thlorenz/brace
 * @module lib/outils/ace/loadAce
 */
import { j3pBaseUrl } from 'src/lib/core/constantes'
import { loadJs } from 'src/lib/utils/dom/main'

import type { Ace, Editor } from './types.d'

export function loadAce (): Promise<Ace> {
  return loadJs(j3pBaseUrl + 'externals/editeurs/ace/ace.js')
    .then(() => {
      if (window.ace == null) throw Error('L’éditeur n’a pas été correctement chargé')
      return window.ace
    })
}

export async function loadAceEditor (container: HTMLElement): Promise<Editor> {
  const ace = await loadAce()
  return ace.edit(container)
}

export async function loadAceJsEditor (container: HTMLElement): Promise<Editor> {
  const editor = await loadAceEditor(container)
  const session = editor.getSession()
  session.setMode('ace/mode/javascript')
  return editor
}
export async function loadAcePythonEditor (container: HTMLElement): Promise<Editor> {
  const editor = await loadAceEditor(container)
  const session = editor.getSession()
  session.setMode('ace/mode/python')
  return editor
}
