// inspiré de l’original, dans sa version qui utilise @basthon/gui-base 0.50.10 :
// https://framagit.org/basthon/basthon-console/-/blob/5a038b1e711ca28855ed55fd4b78968d24198922/src/ts/gui.ts

// notie
import * as notie from 'notie'
import 'notie/dist/notie.css'
// fontawesome
// import '@fortawesome/fontawesome-free/css/all.css'
// open-sans
// import '@fontsource/open-sans'
// basthon
import { GUIBase, GUIOptions } from '@basthon/gui-base'

import { addElement } from 'src/lib/utils/dom/main'

import Shell from './Shell'
import Editor from './Editor'
import Graphics from './Graphics'

import './style.scss'

const dummyFn = (): void => undefined
// const errorLogger = (error): void => console.error(error)

type callbackVoid = () => void

export interface ConsoleOptions {
  darkMode?: boolean
  language?: string
  noButtons?: boolean
  onlyEditor?: boolean
  onlyConsole?: boolean
  uiName?: string
  noCheckpointsInit?: boolean
  withBanner?: boolean
}

export interface EvalResponse {
  result: any[]
  stdOut: string
  stdErr: string
}

function getGuiOptions (options?: ConsoleOptions): GUIOptions {
  return {
    // pourquoi TS me dit que GUIOptions veut pas de kernelOptions ????
    kernelOptions: {
      // ça c’est imposé par notre arborescence
      rootPath: '/externals/basthon-console/assets',
      language: options?.language ?? 'python3'
    },
    // false par défaut, mais faut le fournir
    noCheckpointsInit: Boolean(options?.noCheckpointsInit),
    // et on impose ça
    uiName: 'console'
  }
}

/**
 * Interface graphique de basthon-console
 */
class Gui extends GUIBase {
  private readonly _shell: Shell
  private readonly _editor: Editor
  private readonly _graphics: Graphics
  // les options passées au constructeur
  private readonly _options: ConsoleOptions
  public readonly _withBanner: boolean
  // nos éléments
  private readonly _container: HTMLElement
  private readonly _divLeft: HTMLElement
  private readonly _divRight: HTMLElement
  private readonly _divBtns: HTMLElement
  // boutons facultatifs
  private readonly _btnExec: HTMLElement | null
  private readonly _btnHideEditor: HTMLElement | null
  private readonly _btnHideConsole: HTMLElement | null
  private readonly _btnShowBoth: HTMLElement | null

  public divEditor: HTMLElement
  public divShell: HTMLElement
  public divGraphics: HTMLElement

  // tant que TS arrive pas à récupérer proprement les types de @basthon/gui-base faut lui repréciser ça
  protected _contentFilename: string
  protected _urlKey: string

  /**
   *
   * @param {HTMLElement} container
   * @param {ConsoleOptions} options
   */
  public constructor (container: HTMLElement, options?: ConsoleOptions) {
    // mod DC, on a pas la même signature de constructeur
    super(getGuiOptions(options))
    this._container = container
    this._options = options ?? {}
    this._withBanner = options?.withBanner ?? false
    // on ajoute nos éléments, en reprenant la construction du html de basthon-console:src/templates/index.html
    this._container.classList.add('bastonConsole')

    this._divLeft = addElement(this._container, 'div', { className: 'wrapperEditor' })
    this.divEditor = addElement(this._divLeft, 'div', { className: 'editor' })

    this._divRight = addElement(this._container, 'div', { className: 'wrapperShell' })
    this.divShell = addElement(this._divRight, 'div', { className: 'shell darklighted' })
    this.divGraphics = addElement(this._divRight, 'div', { className: 'graphics' })
    if (this._options?.darkMode !== true) this.divShell.classList.add('light')

    this._divBtns = addElement(this._container, 'div', { className: 'wrapperButtons' })
    this._btnExec = addElement(this._divBtns, 'button', { content: 'Exec', className: 'btn' })

    if (this._options.onlyConsole === true || this._options.noButtons === true) {
      this._btnHideConsole = null
      this._btnHideEditor = null
      this._btnShowBoth = null
    } else {
      this._btnHideConsole = addElement(this._divBtns, 'button', { content: 'Éditeur seul', className: 'btn' })
      this._btnHideEditor = addElement(this._divBtns, 'button', { content: 'Console seule', className: 'btn' })
      this._btnShowBoth = addElement(this._divBtns, 'button', { content: 'Afficher éditeur et console', className: 'btn', style: { display: 'none' } })
    }
    // fin mod DC

    this._urlKey = 'script'
    this._contentFilename = 'script'
    this._shell = new Shell(this)
    this._editor = new Editor(this)
    this._graphics = new Graphics(this)

    /* when kernel is available, set contentFilename extension */
    this.kernelLoader.kernelAvailable().then((kernel) => {
      const exts: string[] = kernel.moduleExts()
      if (exts[0] != null) this._contentFilename += `.${exts[0]}`
    }).catch(error => console.error(error))
  }

  /**
   * Get editor’s content.
   */
  public getContent (): string { return this._editor.getContent() }

  /**
   * Set editor’s content.
   */
  public setContent (content: string): void { this._editor.setContent(content) }

  /**
   * Editor getter.
   */
  public get editor (): Editor { return this._editor }

  /**
   * Shell getter.
   */
  public get shell (): Shell { return this._shell }

  /**
   * Graphics getter.
   */
  public get graphics (): Graphics { return this._graphics }

  /**
   * Notify the user with an error.
   */
  public error (title: string, message: string): void {
    notie.alert({
      type: 'error',
      title,
      text: message,
      stay: false,
      time: 3,
      position: 'top'
    })
  }

  /**
   * Notify the user.
   */
  public info (title: string, message: string): void {
    notie.alert({
      type: 'success',
      title,
      text: message,
      stay: false,
      time: 3,
      position: 'top'
    })
  }

  /**
   * Ask the user to confirm or cancel.
   */
  public confirm (
    title: string,
    message: string,
    text: string,
    callback: callbackVoid,
    textCancel: string,
    callbackCancel: callbackVoid
  ): void {
    notie.confirm({
      text: message,
      title,
      submitText: text,
      cancelText: textCancel,
      position: 'top',
      submitCallback: callback,
      cancelCallback: callbackCancel
    })
  }

  /**
   * Ask the user to select a choice.
   */
  public select (
    title: string,
    message: string,
    choices: Array<{
      text: string
      handler: callbackVoid
    }>,
    textCancel: string,
    callbackCancel: callbackVoid
  ): void {
    let count = 0
    notie.select({
      title,
      text: message,
      cancelText: textCancel,
      position: 'top',
      choices: choices.map(
        (c) => ({
          text: c.text,
          type: (count++ % 2 === 1) ? 'error' : 'success',
          handler: c.handler ?? dummyFn
        })),
      cancelCallback: callbackCancel ?? dummyFn
    })
  }

  protected async setupUI (options: any): Promise<void> {
    await super.setupUI(options)

    await Promise.all([
      this._editor.init(),
      this._shell.init(this.divShell),
      this._graphics.init()
    ])

    /* connecting buttons */
    if (this._btnExec != null) this._btnExec.addEventListener('click', this.runScript.bind(this))
    if (this._btnHideConsole != null) this._btnHideConsole.addEventListener('click', this.hideConsole.bind(this))
    if (this._btnHideEditor != null) this._btnHideEditor.addEventListener('click', this.hideEditor.bind(this))
    if (this._btnShowBoth != null) this._btnShowBoth.addEventListener('click', this.showEditorConsole.bind(this))

    /* binding ctrl+s */
    window.addEventListener('keydown', (event) => {
      if ((event.ctrlKey || event.metaKey) && event.key === 's') {
        event.preventDefault()
        this.download()
      }
    })

    /* backup before closing */
    window.onbeforeunload = () => {
      this.backup().catch(this.notifyError.bind(this))
    }

    // lancer ici setLigthMode ne change rien, `await this.loaded()` est pas possible (ça attendrait qu’on ait rendu la main) => en le met en setTimeout
    // mais toujours pas…
    const setModeWhenReady = (): void => {
      this.loaded().then(() => {
        if (this._options.darkMode === true) this.setDarkMode()
        else this.setLigthMode()
      }).catch(error => console.error(error))
    }
    setTimeout(setModeWhenReady, 0)
  }

  /**
   * Run the editor script in the shell.
   */
  public runScript (): void {
    this.loaded().then(() => {
      const src = this.getContent()
      this._shell.launch(src, false)
      this._shell.domElement?.focus()
    }).catch(error => console.error(error))
  }

  /**
   * eval code and return output
   * @param {string} script
   * @return {Promise<EvalResponse>}
   */
  public async eval (script: string): Promise<EvalResponse> {
    await this.loaded()
    let stdOut = ''
    let stdErr = ''
    const outCb = (out: string): void => { stdOut += out }
    const errCb = (err: string): void => { stdErr += err }
    const result = await this.kernelSafe?.evalAsync(script, outCb, errCb)
    return { result, stdOut, stdErr }
  }

  /**
   * eval code and return output
   * @param {string} script
   * @return {Promise<EvalResponse>}
   * /
  public async eval (script: string): Promise<EvalResponse> {
    await this.loaded()
    let stdOut :string = ''
    let stdErr :string = ''
    const result = await this.kernelSafe?.evalAsync(script, out => { stdOut += out }, err => { stdErr += err })
    return { result, stdOut, stdErr }
  } /* */

  /**
   * RAZ function.
   */
  public restart (): void {
    this.kernelRestart()
    this._shell.clear()
    this._graphics.clean()
  }

  /**
   * Open *.py file by asking user what to do:
   * load in editor or put on (emulated) local filesystem.
   */
  public async openModuleFile (file: File): Promise<void> {
    this.confirm('', `Que faire de ${file.name} ?`,
      'Charger dans l’éditeur',
      () => {
        this.open(file)
          // aj catch par DC
          .catch(error => {
            console.error(error)
            this.error('', `Impossible de charger ${file.name} dans l’éditeur`)
          })
      },
      'Installer le module',
      () => {
        this.putFSRessource(file)
          // aj catch par DC
          .catch(error => {
            console.error(error)
            this.error('', `Impossible d’installer le module ${file.name}`)
          })
      }
    )
  }

  /**
   * Opening file: If it has kernel extension, loading it in the editor
   * or put on (emulated) local filesystem (user is asked to),
   * otherwise, loading it in the local filesystem.
   */
  public async openFile (): Promise<void> {
    await this.kernelLoader.kernelAvailable()
    const callback = this.openModuleFile.bind(this)
    const callbacks: { [key: string]: ((_: File) => Promise<void>) } = {}
    this.kernelSafe?.moduleExts().forEach((ext: string) => {
      callbacks[ext] = callback
    })
    await this._openFile(callbacks)
  }

  /**
   * Displaying editor alone.
   */
  public hideConsole (): void {
    // let button = this._container.querySelector<HTMLElement>('.btn-hide-console')
    if (this._btnHideConsole != null) this._btnHideConsole.style.display = 'none'
    if (this._btnShowBoth != null) this._btnShowBoth.style.display = ''
    if (this._btnHideEditor != null) this._btnHideEditor.style.display = ''
    this._divRight.style.display = 'none'
    this._divLeft.style.display = ''
    this._divLeft.style.width = '100%'
  }

  /**
   * Displaying console alone.
   */
  public hideEditor (): void {
    if (this._btnHideEditor != null) this._btnHideEditor.style.display = 'none'
    if (this._btnShowBoth != null) this._btnShowBoth.style.display = ''
    if (this._btnHideConsole != null) this._btnHideConsole.style.display = ''
    this._divLeft.style.display = 'none'
    this._divRight.style.display = ''
    this._divRight.style.width = '100%'
  }

  /**
   * Editor and console side by side.
   */
  public showEditorConsole (): void {
    if (this._btnHideEditor != null) this._btnHideEditor.style.display = ''
    if (this._btnHideConsole != null) this._btnHideConsole.style.display = ''
    if (this._btnShowBoth != null) this._btnShowBoth.style.display = 'none'
    // on retire l’override mis sur l’élément pour laisser faire le css
    this._divLeft.style.width = ''
    this._divLeft.style.display = ''
    this._divRight.style.display = ''
    this._divRight.style.width = ''
  }

  // méthodes virées car on s’en sert pas
  // _getSwitchState
  // updateSwitchView
  // switchView

  /**
   * Set dark mode (pour toutes les consoles basthon du dom)
   */
  public setDarkMode () {
    // console.log('elts => dark', document.querySelectorAll('.darklighted'))
    // on a un #basthon-loader en dehors de notre conteneur
    // for (const elt of this._container.querySelectorAll('darklighted')) elt.classList.add('ligth')
    // for (const elt of document.querySelectorAll('.darklighted')) elt.classList.remove('ligth')
    document.querySelectorAll('.darklighted').forEach(el => el.classList.remove('light'))
    this._editor.setTheme(true).catch(error => console.error(error))
  }

  /**
   * Set light mode (pour toutes les consoles basthon du dom)
   */
  public setLigthMode () {
    // console.log('elts => light', document.querySelectorAll('.darklighted'))
    // for (const elt of document.querySelectorAll('.darklighted')) elt.classList.add('ligth')
    // pourquoi ce qui précède marche pas (ça ajoute bien la classe css mais l’élément jQuery.terminal bouche pas) et ce qui suit fonctionne ???
    document.querySelectorAll('.darklighted').forEach(el => el.classList.add('light'))
    this._editor.setTheme(false).catch(error => console.error(error))
  }

  /**
   * Toggle light / dark mode
   */
  public toggleMode (): void {
    const isLight = this.divShell.classList.contains('light')
    if (isLight) this.setDarkMode()
    else this.setLigthMode()
  }

  /**
   * Toggle hide/show shell/graph.
   */
  public showShell (): void {
    this._shell.show()
    this._graphics.hide()
  }

  /**
   * Toggle hide/show shell/graph.
   */
  public showGraph (): void {
    this._shell.hide()
    this._graphics.show()
  }

  /**
   * Modifie la hauteur éditeur/console
   * @param {number} height Hauteur voulue (en % du viewport)
   */
  public setHeight (height: number): void {
    if (height < 10) return console.error(Error(`Hauteur trop petite (10 min, ${height} fourni)`))
    if (height > 100) return console.error(Error(`Hauteur trop grande (100 max, ${height} fourni)`))
    this.divEditor.style.height = `${height}vh`
    this.divGraphics.style.height = `${height}vh`
    this.divShell.style.height = `${height}vh`
    if (this._divBtns.style.display !== 'none') {
      // on ajoute la hauteur du div des boutons
      const { height: hBtns } = this._divBtns.getBoundingClientRect()
      const { height: hConsole } = this._divLeft.getBoundingClientRect()
      this._container.style.height = `${hBtns + hConsole + 20}px`
    }
  }
}

export default Gui
