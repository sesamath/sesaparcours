import Gui from './Gui'
import { KernelBase } from '@basthon/kernel-base'

import { loadAceEditor } from 'src/lib/outils/ace/loadAce'

// ts retrouve AceAjax dans le module '@types/ace'
// mais y’a pas Editor.completers dedans, alors que c’est défini dans node_modules/ace-builds/ace.d.ts
import { Ace } from 'ace-builds/ace.d'

class Editor {
  private _aceEditor?: Ace.Editor
  private readonly _gui: Gui

  public constructor (gui: Gui) {
    this._gui = gui
  }

  /**
   * Kernel getter.
   */
  public get kernel (): KernelBase | undefined { return this._gui.kernel }
  public get kernelSafe (): KernelBase | null { return this._gui.kernelSafe }

  /**
   * Initialize the Ace editor.
   */
  public async init (): Promise<void> {
    // ace/mode/python3 does not exists...
    const language = this._gui.language
    const aceLanguage: string = language !== 'python3' ? language : 'python'
    const editor = await loadAceEditor(this._gui.divEditor)
    editor.session.setMode(`ace/mode/${aceLanguage}`)
    editor.focus()

    editor.setOptions({
      enableLiveAutocompletion: true,
      enableBasicAutocompletion: true,
      highlightActiveLine: false,
      highlightSelectedWord: true,
      fontSize: '12pt'
    })

    // editor’s completion

    // removing all completers and keeping track of local completer
    const localCompleter = editor.completers?.[1] // any hint to detect this dynamically?
    editor.completers = [{
      getCompletions: (editor: Ace.Editor, session: unknown, pos: { row: number, column: number }, prefix: string, callback: Function) => {
        // get local completions (from editor, not namespace)
        let locals: any[] = []
        localCompleter?.getCompletions(editor, session, pos, prefix, function (_: any, completions: any[]) { locals = completions })
        // recover editor content up to position
        let lines = editor.getValue().split('\n')
        lines = lines.slice(0, pos.row + 1)
        if (lines.length > 0) {
          const lastLine = lines[lines.length - 1] ?? ''
          lines[lines.length - 1] = lastLine.slice(0, pos.column)
        }
        const src = lines.join('\n')
        // Basthon completion
        const kernelCompletions = this.kernelSafe?.complete(src)
        if (kernelCompletions == null || kernelCompletions.length < 1) return
        const start = kernelCompletions[1] ?? 0
        let completions = kernelCompletions[0] ?? []
        // removing prefix
        const basthonPrefix = src.slice(start, -prefix.length)
        completions = completions.map((c: string) => c.slice(basthonPrefix.length))
        // union with local completions
        const completionsSet = new Set<string>(completions)
        for (let c of locals) {
          c = c.value
          if (!(completionsSet.has(c) || completionsSet.has(String(c) + '('))) { completionsSet.add(c) }
        }

        // rendering
        callback(null, [...completionsSet].map((value: string) => ({
          caption: value,
          value,
          meta: 'code',
          score: 100
        })))
      }
    }]
    this._aceEditor = editor
  } // init

  /**
   * Set the readOnly prop to true (content can’t be changed, but still visible and can be selected for copying)
   * opacity set to 0.6 to show the "disabled" state
   */
  public setReadOnly (): void {
    if (this._aceEditor == null) throw Error('L’éditeur n’a pas été initialisé')
    this._aceEditor.setReadOnly(true)
    this._aceEditor.container.style.opacity = '0.6'
  }

  /**
   * Set editor’s content (undo selection).
   */
  public setContent (content?: string): void {
    if (this._aceEditor == null) throw Error('L’éditeur n’a pas été initialisé')
    if (content != null) this._aceEditor.setValue(content)
    this._aceEditor.scrollToRow(0)
    this._aceEditor.gotoLine(0, 0, false)
  }

  /**
   * Get editor’s content
   */
  public getContent (): string {
    if (this._aceEditor == null) throw Error('L’éditeur n’a pas été initialisé')
    return this._aceEditor.getValue()
  }

  // depuis qu’on a débranché le parsing tsc dans eslint (bcp trop lent), eslint signale du no-undef pour les types que tsc trouve très bien
  /* global EventListener */
  /**
   * Pass events to Ace editor.
   */
  public addEventListener (name: string, callback: EventListener, capturing?: boolean | undefined): void {
    if (this._aceEditor == null) throw Error('L’éditeur n’a pas été initialisé')
    return this._aceEditor.addEventListener(name, callback, capturing)
  }

  /**
   * Set ace theme.
   */
  public async setTheme (dark: boolean): Promise<void> {
    if (this._aceEditor == null) throw Error('L’éditeur n’a pas été initialisé')
    const theme = dark ? 'monokai' : 'xcode'
    this._aceEditor.setTheme(`ace/theme/${theme}`)
  }
}

export default Editor
