Basthon
=======

Plus d’infos sur https://basthon.fr/

Pour que cet outil fonctionne, il faut
* récupérer localement https://framagit.org/basthon/basthon-console avec `git clone https://framagit.org/basthon/basthon-console.git`
* installer les dépendances **avec npm** (ça build pas si on installe avec pnpm) `npm i`
* lancer le build `npm run build`
* récupérer le contenu de `build/assets/x.y.z/` et le mettre dans `sesaparcours:docroot/externals/basthon-console/assets/x.y.z/`
* tester localement avec /?section=basthonConsole
* en cas de pb, il faut parfois ajouter des liens symbolique (par ex avec le commit 7cd49b5 le build crée un dossier assets/0.40.5 mais ensuite basthon cherche des trucs dans 0.40.16, qui est la version du kernel), avec basthon 0.50.10 (commit 5a038b1 de @basthon/basthon-console) y’a pas eu de souci.
