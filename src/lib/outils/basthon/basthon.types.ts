/** @module lib/outils/basthon/basthon.types */
// l’import des types marche pas (visiblement basthon est pas compatible pnpm), faut les remettre ici
// on se sert dans (0.50.10)

// from @basthon/kernel-base

/**
 * API that any Basthon kernel should fill to be supported
 * in console/notebook.
 */
export declare class KernelBase {
  private readonly _rootPath
  private readonly _ready
  private readonly _loaded
  _execution_count: number
  constructor (options: any)
  /**
   * Kernel version number (string).
   */
  version (): string
  /**
   * Language implemented in the kernel (string).
   * Generally lower case.
   */
  language (): string
  /**
   * Language name implemented in the kernel (string).
   * As it should be displayed in text.
   */
  languageName (): string
  /**
   * Script (module) file extensions
   */
  moduleExts (): string[]
  /**
   * Launch the kernel.
   */
  launch (): Promise<void>
  /**
   * Execution count getter.
   */
  get execution_count (): number
  /**
   * Async code evaluation that resolves with the result.
   */
  evalAsync (code: string, outCallback: (_: string) => void, errCallback: (_: string) => void, data?: any): Promise<any>
  restart (): void
  putFile (filename: string, content: ArrayBuffer): Promise<void>
  putModule (filename: string, content: ArrayBuffer): Promise<void>
  userModules (): string[]
  getFile (path: string): void
  getUserModuleFile (filename: string): void
  more (source: string): boolean
  complete (code: string): [string[], number] | []
  banner (): string
  ps1 (): string
  ps2 (): string
  /**
   * Initialize the kernel.
   */
  init (): Promise<void>
  /**
   * Is the kernel ready?
   */
  get ready (): boolean
  /**
   * Promise that resolve when the kernel is loaded.
   */
  loaded (): Promise<void>
  /**
   * Root for kernel files. This is always the language directory
   * inside the version number directory inside the kernel directory.
   */
  basthonRoot (absolute?: boolean): string
  /**
   * Downloading data (bytes array or data URL) as filename
   * (opening browser dialog).
   */
  download (data: Uint8Array | string, filename: string): void
  /**
   * Dynamically load a script asynchronously.
   */
  static loadScript (url: string): Promise<any>
  /**
   * Wrapper around document.dispatchEvent.
   * It adds the 'basthon.' prefix to each event name and
   * manage the event lookup to retreive relevent data.
   */
  dispatchEvent (eventName: string, data: any): void
  /**
   * Wrapper around document.addEventListener.
   * It manages the 'basthon.' prefix to each event name and
   * manage the event lookup to retreive relevent data.
   */
  addEventListener (eventName: string, callback: (_: any) => void): void
  /**
   * Send eval.input event then wait for the user response and return it.
   */
  inputAsync (prompt: string | null | undefined, password?: boolean, data?: any): Promise<unknown>
  /**
   * Simple clone via JSON copy.
   */
  clone (obj: any): any
  /**
   * Put a ressource (file or module).
   * Detection is based on extension.
   */
  putRessource (filename: string, content: ArrayBuffer): Promise<void>
  /**
   * Internal. Code evaluation after an eval.request event.
   */
  evalFromEvent (data: any): Promise<void>
}

// from @basthon/kernel-loader/lib/main.d.ts

/**
 * Helper class to dynamically load kernels.
 */
export declare class KernelLoader {
  private readonly _kernel?
  private readonly _pageLoad
  private readonly _kernelAvailable
  private readonly _loaderId
  /**
   * Flag to force loader to be shown.
   * Basically, loader can be hidden after Basthon loading.
   */
  private readonly _doNotHideLoader
  private readonly _rootLoader?
  private readonly _loaderTextElem?
  private readonly _loaderTextError?
  constructor (options: any, loaderId?: string)
  /**
   * Returns a promise that resolves when page is loaded
   * (document.body available).
   */
  pageLoad (): Promise<void>
  /**
   * Is the kernel object available (not null)?
   * Be carefukl, it does not resolves when the kernel is loaded but when
   * it is set. See kernelLoaded.
   */
  kernelAvailable (): Promise<KernelBase>
  /**
   * Is the kernel loaded?
   */
  kernelLoaded (): Promise<KernelBase>
  /**
   * Kernel getter.
   */
  get kernel (): KernelBase | undefined
  get kernelSafe (): KernelBase | null
  /**
   * Show a fullscreen loader that disapear when Basthon is loaded.
   * If you want to manually hide the loader, set hideAfter to false.
   */
  showLoader (text: string, fullscreen?: boolean, hideAfter?: boolean): Promise<void>
  /**
   * Setting the text loader.
   */
  setLoaderText (text: string): void
  /**
   * Setting the error text.
   */
  setErrorText (text: string): void
  /**
   * Hide the Basthon’s loader.
   */
  hideLoader (): void
  /**
   * Get browser info (name and version).
   */
  browser (): {
    name: string
    version: string
  }
}

// from @basthon/checkpoints

/**
 * A class to manage checkpoints (multiple backups).
 * Stored data is of type string (json conversion if needed).
 * One can add tags to checkpoints.
 *
 * name is a string used to identify each instance.
 * maxItems is the maximum number of checkpoints we keep before
 *          removing old ones. Set to zero to disable.
 */
export declare class CheckpointsManager<T> {
  private readonly _storage
  private readonly _tags
  private readonly _maxItems
  ready: () => Promise<void>
  /**
   * Name for the checkpoints DB.
   */
  private static readonly _storageName
  /**
   * Name for the tags DB.
   */
  private static readonly _tagsName
  constructor (name: string, maxItems?: number)
  /**
   * Remove checpoints to honor maxItems-1 (use this before push).
   */
  private readonly _freeOneSlot
  /**
   * Push checkpoint to DB. Timestamps are computed here.
   * tags is an array of strings.
   */
  push (data: T, tags?: string[] | null): Promise<string>
  /**
   * Set a tag to a checkpoint.
   */
  tag (timestamp: string, tag: string): Promise<void>
  /**
   * Remove a tag from a checkpoint.
   */
  untag (timestamp: string, tag: string): Promise<void>
  /**
   * Add a tag to last checkpoint.
   */
  tagLast (tag: string): Promise<void>
  /**
   * Remove a tag from last checkpoint.
   */
  untagLast (tag: string): Promise<void>
  /**
   * Get checkpoint from its timestamp.
   */
  get (timestamp: string): Promise<T | null | undefined>
  /**
   * Get tags from timestamp.
   */
  getTags (timestamp: string): Promise<string[] | undefined>
  /**
   * Does this checkpoint has this tag?
   */
  hasTag (timestamp: string, tag: string): Promise<boolean | null>
  /**
   * Get the total number of checkpoints.
   */
  length (): Promise<number>
  /**
   * Get the array of all timestamps sorted in reverse historical order.
   */
  times (): Promise<string[] | undefined>
  /**
   * Get timestamp at a specific index (0 is the most recent).
   */
  getIndexTimestamp (index: number): Promise<string | undefined>
  /**
   * Get checkpoint at a specific index (0 is the most recent).
   */
  getIndex (index: number): Promise<T | null | undefined>
  /**
   * Get tags at a specific index (0 is the most recent).
   */
  getIndexTags (index: number): Promise<string[] | undefined>
  /**
   * Get the most recent timestamp. Equivalent to getIndexTimestamp(0).
   */
  getLastTimestamp (): Promise<string | undefined>
  /**
   * Get the most recent checkpoint. Equivalent to getIndex(0).
   */
  getLast (): Promise<T | null | undefined>
  /**
   * Get the most recent tags. Equivalent to getIndex(0).
   */
  getLastTags (): Promise<string[] | undefined>
  /**
   * Does the most recent checkpoint has this tag?
   */
  lastHasTag (tag: string): Promise<boolean | null>
  /**
   * Remove checkpoint at a specific timestamp.
   */
  remove (timestamp: string): Promise<void>
  /**
   * Remove the oldest checkpoint.
   */
  removeOldest (): Promise<void>
  /**
   * Clear all checkpoints.
   */
  clear (): Promise<void>
  /**
   * Convert a timestamp to a (french) human date : DD/MM/YYYY, hh:mm:ss
   */
  static toHumanDate: (timestamp: string | number) => string
  /**
   * Drop a checkpoint table from its name.
   */
  static drop (name: string): Promise<void>
}

// from @basthon/checkpoints/lib/storage.d.ts

/**
 * A class to manage key/value browser storage.
 *
 * name is a string used to identify each storage instance.
 */
export declare class Storage<T> {
  private readonly _ready
  ready: (callback?: ((error: any) => void) | undefined) => Promise<void>
  private readonly _store
  private static readonly mainDB
  constructor (name: string)
  get (key: string): Promise<T | null | undefined>
  set (key: string, value: T): Promise<void>
  remove (key: string): Promise<void>
  clear (): Promise<void>
  length (): Promise<number>
  keys (): Promise<string[] | undefined>
  iterate (callback: (a: T) => void): Promise<void>
  static drop (name: string): Promise<void>
}

// from @basthon/gui-base/lib/main.d.ts

export interface GUIOptions {
  kernelOptions: any
  uiName?: string
  noCheckpointsInit?: boolean
}
/**
 * Base class for console and notebook GUI.
 */
export declare class GUIBase {
  private readonly _language
  private readonly _loaded
  private readonly _loader
  private readonly _checkpoints?
  private readonly _maxCheckpoints
  protected _contentFilename: string
  protected _urlKey: string
  private readonly _extensions
  protected _stateStorage: Storage<boolean>
  private readonly _console_error
  constructor (options: GUIOptions)
  /**
   * Language getter.
   */
  get language (): string
  /**
   * Kernel getter.
   */
  get kernel (): KernelBase | undefined
  get kernelSafe (): KernelBase | null
  /**
   * KernelLoader getter.
   */
  get kernelLoader (): KernelLoader
  /**
   * Check if GUI is loaded.
   */
  loaded (): Promise<void>
  /**
   * Wait for par load (document.body available).
   */
  pageLoad (): Promise<void>
  /**
   * Set the checkpoints manager
   * (typically use with GUIOptions.noCheckpointsInit set to true).
   */
  setCheckpointsManager (checkpoints: CheckpointsManager<string>): void
  /**
   * Notify the user.
   */
  info (title: string, message: string): void
  /**
   * Notify the user with an error.
   */
  error (title: string, message: string): void
  /**
   * Ask the user to confirm or cancel.
   */
  confirm (title: string, message: string, text: string, callback: (() => void), textCancel: string, callbackCancel: (() => void)): void
  /**
   * Ask the user to select a choice.
   */
  select (title: string, message: string, choices: Array<{
    text: string
    handler: () => void
  }>, textCancel: string, callbackCancel: (() => void)): void

  /**
   * The error notification system.
   */
  notifyError (error: Error | ErrorEvent | PromiseRejectionEvent): void
  /**
   * Initialize the GUI.
   */
  init (options?: any): Promise<void>
  /**
   * Get the content (script or notebook content).
   */
  content (): string
  /**
   * Set the content (script or notebook content).
   */
  setContent (content: string): void
  /**
   * Loading the content from query string (ipynb=/script= or from=).
   */
  loadFromQS (key: string): Promise<string | null>
  /**
   * Load all URL requested (and registered) extensions
   * (call submited callback).
   */
  private readonly _loadExtensions
  /**
   * Setup the UI (typically connect events, load extensions etc..).
   */
  protected setupUI (options: any): Promise<void>
  /**
   * Get state from strorage.
   */
  protected getState (state: string, def?: any): Promise<any>
  /**
   * Set state in storage.
   */
  protected setState (state: string, value: any): Promise<void>
  /**
   * Load content at startup.
   */
  protected loadInitialContent (options: any): Promise<void>
  /**
   * Load content from local forage.
   */
  loadFromStorage (setContent?: boolean): Promise<string | null>
  /**
   * Tag last backup as "approved".
   */
  validateBackup (): Promise<void>
  /**
   * Is last backup tagged as "approved"?
   */
  lastBackupValid (): Promise<boolean | null | undefined>
  /**
   * Select a checkpoint to load in the script.
   */
  selectCheckpoint (): Promise<string | null>
  /**
   * Backup to checkpoints.
   */
  backup (approved?: boolean): Promise<string | undefined>
  /**
   * Internal GUI init.
   * It calls setupUI then loadInitialContent.
   */
  protected _init (options?: any): Promise<void>
  /**
   * Change loader text and call init function.
   * If catchError is false, in case of error, we continue the
   * init process, trying to do our best...
   */
  initCaller (func: () => Promise<any>, message: string, catchError: boolean): Promise<any>
  /**
   * Get mode as a string (dark/light).
   */
  theme (): Promise<'dark' | 'light' | undefined>
  /**
   * Restart the kernel.
   */
  kernelRestart (): void
  /**
   * Load ressources from URL (common part to files and modules).
   */
  private readonly _loadFromURL
  /**
   * Loading file in the (emulated) local filesystem (async).
   */
  putFSRessource (file: File): Promise<void>
  /**
   * Load auxiliary files submited via URL (aux= parameter) (async).
   */
  loadURLAux (): Promise<void>
  /**
   * Load modules submited via URL (module= parameter) (async).
   */
  loadURLModules (): Promise<void>
  /**
   * Register an extension and its callback.
   */
  registerExtension (name: string, callback: (() => Promise<void>)): void
  /**
   * Opening file: If it has ext as extension, loading it in
   * the editor or put on (emulated) local filesystem
   * (user is asked to), otherwise, loading it in the local
   * filesystem.
   */
  protected _openFile (callbacks: {
    [key: string]: ((_: File) => Promise<void>)
  }): Promise<void>

  /**
   * Open an URL in a new tab or download a file.
   */
  static openURL (url: string, download?: string): void
  /**
   * Returning the sharing link for the content.
   */
  sharingURL (key: string): Promise<string>
  /**
   * Share content via URL.
   */
  protected share (): Promise<void>
  /**
   * Opening file (async) and load its content.
   */
  open (file: Blob | File): Promise<void>
  /**
   * Download content to file.
   */
  download (filename?: string): void
  /**
   * Copy a text to clipboard.
   */
  static copyToClipboard: (text: string) => void
  /**
   * Compress a string to another string (URL safe).
   */
  deflate (content: string): Promise<string>
  /**
   * Reverse version of deflate.
   */
  inflate (content: string): Promise<string>
}
