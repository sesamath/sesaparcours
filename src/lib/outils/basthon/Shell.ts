import { KernelBase } from '@basthon/kernel-base/lib/kernel'

import { j3pBaseUrl } from 'src/lib/core/constantes'
import { loadJs } from 'src/lib/utils/dom/main'

import Gui from './Gui'

// le css 'jquery.terminal/css/jquery.terminal.css' est foireux (y’a un let à la place de left dedans)
import './jquery.terminal.css'

const dummyFn = (): void => undefined

const hideShowConverter: { [key: string]: string } = {
  none: 'block',
  '': 'none',
  block: 'none'
}

class Shell {
  // dans l’original c’est
  // private _elem = document.getElementById("shell");
  // on préfère l’affecter dans le constructeur
  private readonly _elem: HTMLElement
  private readonly _gui: Gui
  private _term: any
  // A buffer to handle multiline shell input.
  private _buffer: string[] = []
  // tell wether we are waiting for user input during
  // program execution (Python’s input())
  private _inputMode = false
  // promise resolver for input mode
  private readonly _inputResolve?: ((_?: string) => void)

  public constructor (gui: Gui) {
    this._gui = gui
    this._elem = gui.divShell
  }

  /**
   * Kernel getter.
   */
  public get kernel (): KernelBase | undefined { return this._gui.kernel }
  public get kernelSafe (): KernelBase | null { return this._gui.kernelSafe }

  /**
   * Getter for DOM element.
   */
  public get domElement (): HTMLElement { return this._elem }

  /**
   * Mimic CPython’s REPL banner.
   */
  public banner (): string {
    let banner = this.kernel?.banner() ?? ''
    // Python 3.8
    banner = banner?.replace('on WebAssembly VM', '')
    // Python 3.10
    banner = banner?.replace('on WebAssembly/Emscripten', '')
    return banner
  }

  /**
   * Reset the multiline input buffer.
   */
  public reset_buffer (): void {
    this._buffer = []
  }

  private _interpreter (command: string): void {
    // user answers to its program’s input
    if (this._inputMode) {
      this._term.set_prompt('')
      this._inputMode = false
      this.pause()
      this._inputResolve?.(command)
      return
    }
    // user interacts with REPL
    this._buffer.push(command)
    const source = this._buffer.join('\n')
    if (this.kernelSafe?.more(source) != null) {
      this._term.set_prompt(this.kernel?.ps2())
    } else {
      this._term.set_prompt(this.kernel?.ps1())
      this.reset_buffer()
      this.launch(source, true)
    }
  }

  /**
   * Initialize the shell.
   */
  public async init (elt: HTMLElement): Promise<void> {
    // just to be safe
    this.reset_buffer()
    // faut mettre notre jQuery en global avant de charger jquery.terminal
    const { default: $ } = await import('jquery')
    Object.assign(window, { jQuery: $ })
    await Promise.all([
      this._gui.kernelLoader.kernelAvailable(),
      // si on ne prend pas notre version locale, on se retrouve avec l’erreur `$(...).terminal is not a function`
      loadJs(j3pBaseUrl + 'externals/jquery.terminal/jquery.terminal.min.js')
    ])
    this._term = $(elt).terminal(
      this._interpreter.bind(this),
      {
        greetings: (this._gui._withBanner) ? this.banner() : '',
        prompt: this.kernel?.ps1(),
        completionEscape: false,
        completion: (command: string, callback: ((_: any) => void)) => {
          const completions = this.kernelSafe?.complete(command) ?? [[]]
          if (completions.length > 0) callback(completions[0])
        }
      })
    // this prevents JQuery Terminal to redraw terminal and remove
    // script wrapping feature
    this._term.resize = dummyFn

    /* Event connections */
    this._gui.kernelLoader.kernelLoaded().then(() => {
      this.kernel?.addEventListener('eval.finished', (data) => {
        if (Boolean(data.interactive) && 'result' in data) {
          this.echo(`${String(data.result['text/plain'])}\n`)
        }
        this.wakeup()
      })

      this.kernel?.addEventListener('eval.output', (data) => {
        switch (data.stream) {
          case 'stdout':
            this.echo(data.content)
            break
          case 'stderr':
            this.error(data.content)
            break
        }
      })

      this.kernel?.addEventListener('eval.error', () => this.wakeup())
    }).catch(error => console.error(error))
  }

  /**
   * Print a string in the shell.
   */
  public echo (message: string): void {
    // JQuery.terminal adds a trailing newline that is not easy
    // to remove with the current API. Doing our best here.

    message = message.toString()

    // prompt can be polluted with previous echo
    // _prompt_tail is the line part just before the prompt
    const term = this._term
    const tail: string = term._prompt_tail ?? ''
    term._prompt_tail = tail
    let prompt: string = term.get_prompt()
    if (tail !== '' && prompt.startsWith(tail)) {
      prompt = prompt.slice(term._prompt_tail.length)
    } else {
      term._prompt_tail = ''
    }
    // here, prompt and _prompt_tail should be correctly isolated
    const lines = message.split('\n')
    lines[0] = `${tail}${lines[0] ?? ''}`
    term._prompt_tail = lines.pop()
    const tailBis: string = term._prompt_tail ?? ''
    term.set_prompt(`${tailBis}${prompt}`)
    if (lines.length > 0) {
      // term.echo("") does not display blank line
      // so we replace this call with term.echo('\b')
      term.echo(lines.join('\n') ?? '\b')
    }
  }

  /**
   * Print error in shell (in red).
   */
  public error (message: string): any {
    // JQuery.terminal adds a trailing newline that is not easy
    // to remove with the current API. Doing our best here.
    message = message.toString()
    return this._term.error(message.replace(/\n+$/, ''))
  }

  /**
   * Pause the shell execution (usefull to wait for event or promise).
   */
  public pause (): void {
    this._term.pause()
  }

  /**
   * Resume the shell execution (see `pause`).
   */
  public wakeup (): void { this._term.resume() }

  /**
   * Recover the shell as it was at start.
   */
  public clear (withoutBanner = false): void {
    this._term.clear()
    if (!withoutBanner) this.echo(this.banner() + '\n')
    this.reset_buffer()
  }

  /**
   * Show script in shell with content as dropdown
   * (should improve readability).
   */
  public showScript (code: string): void {
    code = `# script executed\n${code}`
    const ps1: string = this.kernel?.ps1() ?? ''
    const ps2: string = this.kernel?.ps2() ?? ''
    let script = ps1
    script += code.split('\n').join(`\n${ps2}`)
    script += '\n'
    this.echo(script)
    const output = this._elem?.querySelector('.terminal-output')?.lastElementChild
    const header = output?.firstElementChild
    if (header == null) return
    header.classList.add('exec-script-header')
    for (const e of Array.from(header.children)) (e as HTMLElement).style.backgroundColor = 'inherit'
    const hideShow = (): void => {
      if (output == null) return
      for (const e of Array.from(output.children)) {
        if (e === header) continue
        const style = (e as HTMLElement).style
        style.display = hideShowConverter[style.display] ?? 'block'
      }
    }
    header.addEventListener('click', hideShow)
    hideShow()
  }

  /**
   * Launch code in shell.
   */
  public launch (code: string, interactive = true): void {
    if (this.kernelSafe == null) return
    if (!interactive) this.showScript(code)
    this.pause()
    this.kernelSafe.dispatchEvent('eval.request', {
      code,
      interactive
    })
  }

  /**
   * Hide shell view.
   */
  public hide (): void {
    if (this._elem != null) this._elem.style.display = 'none'
  }

  /**
   * Show shell view.
   */
  public show (): void {
    if (this._elem != null) this._elem.style.display = 'block'
  }
}

export default Shell
