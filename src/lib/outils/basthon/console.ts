import Gui, { ConsoleOptions } from './Gui'
import { isHtmlElement } from 'src/lib/utils/dom/main'

/**
 * @module lib/outils/basthon
 */

/**
 * Ajoute une console basthon dans container (attention c’est async)
 * @param {HTMLElement} container
 * @param {ConsoleOptions} [options]
 * @return {Promise<Gui>}
 */
export async function addBasthonConsole (container: HTMLElement, options?: ConsoleOptions): Promise<Gui> {
  if (!isHtmlElement(container)) throw Error('Conteneur invalide')
  try {
    const gui = new Gui(container, options)
    await gui.init()
    await gui.loaded()
    return gui
  } catch (error) {
    console.error(error)
    throw Error('Impossible de finaliser le chargement de la console Basthon')
  }
}
