import type { Item } from 'src/lib/player/types'
import { MathfieldWithKeyboard } from 'src/lib/types'
import { appendCell, fillLine, getValue, isAnswered, setFeedback } from './main'

import type { Icell, ItableDblEntry, Raws, TableMathliveOptions } from './tableMathlive'

import './tableMathlive.scss'

class TableMathliveDblEntry implements Item {
  id: string // ce sera nécessaire pour retrouver le tableau s’il y en a plusieurs dans la page.
  suffix: string // c’est une partie de l’id, mais j’ai pas envie de jouer avec les regex pour l’extraire chaque fois
  // que j’en ai besoin
  output!: string
  raws!: Raws
  headingCols: Icell[]
  headingLines: Icell[]
  className: string
  container!: HTMLTableElement
  persistent: boolean

  private constructor (suffix: string, tableau: ItableDblEntry, {
    className,
    persistent
  }: Required<TableMathliveOptions>) {
    this.headingCols = tableau.headingCols
    this.headingLines = tableau.headingLines
    this.id = `tabML${suffix}`
    this.suffix = suffix
    this.raws = tableau.raws
    this.className = className
    this.persistent = persistent
  }

  static create (suffix: string, tableau: ItableDblEntry, {
    className = '',
    persistent = false
  }: TableMathliveOptions = {}) {
    // tableau doit contenir headingCols et headingLines, qui peuvent être vides, mais doivent être fournis.
    if (!Array.isArray(tableau.headingCols) || !Array.isArray(tableau.headingLines)) {
      throw Error(`ajouteTableauMathlive : vérifiez vos paramètres : ${JSON.stringify({
        headingCols: tableau.headingCols, headingLines: tableau.headingLines
      })}`)
    }
    const tableauMathlive: TableMathliveDblEntry = new TableMathliveDblEntry(suffix, tableau, { className, persistent })
    const table = document.createElement('table')
    table.className = className !== '' ? className : 'tableauMathlive'
    table.id = tableauMathlive.id
    const firstLine = document.createElement('tr')
    table.appendChild(firstLine)
    if (tableau.headingCols != null) {
      fillLine({ line: firstLine, content: tableau.headingCols, index: 0, tag: 'th', className, suffix })
    }
    // lignes suivantes
    for (let j = 0; j < tableau.raws.length; j++) {
      const newLine = document.createElement('tr')
      table.appendChild(newLine)
      const icell = tableau.headingLines[j]
      if (tableau.headingLines != null && icell != null) {
        appendCell({
          line: newLine,
          icell,
          indexCol: 0,
          indexLine: tableau.headingCols != null ? 1 + j : j,
          tag: 'th',
          className,
          suffix
        })
      }
      const raw = tableau.raws[j]
      if (Array.isArray(raw) && raw.length > 0) {
        for (let i = 0; i < raw.length; i++) {
          const icell = raw[i]
          if (icell != null) {
            appendCell({
              line: newLine,
              icell,
              indexCol: tableau.headingLines != null ? i + 1 : i,
              indexLine: tableau.headingCols != null ? 1 + j : j,
              tag: 'td',
              className,
              suffix
            })
          }
        }
      }
    }
    // pour l’instant je retourne l’objet complet avec le HTML de la table dans sa propriété output,
    tableauMathlive.output = table.outerHTML
    tableauMathlive.container = table
    return tableauMathlive
  }

  getValue (): Record<string, string> {
    return getValue(this)
  }

  isAnswered () {
    return isAnswered(this)
  }

  setFeedback () {
    return setFeedback(this)
  }

  setFocus () {
    const firstCellToFill = this.container.querySelector('math-field')
    if (firstCellToFill != null) (firstCellToFill as MathfieldWithKeyboard).focus()
  }
}

export default TableMathliveDblEntry
