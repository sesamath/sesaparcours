import { MathfieldElement } from 'mathlive'
import type { MathfieldWithKeyboard } from 'src/lib/types'
import VirtualKeyboard from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import type { Icell, ItableDblEntry, ItableProp, TableMathlive } from './tableMathlive'

export function hasLatex (type: 'prop' | 'dblEntry', tableau: ItableProp | ItableDblEntry) {
  if (type === 'prop') {
    const content = tableau as ItableProp
    return content.ligne1.find(cell => cell.latex) || content.ligne2.find(cell => cell.latex)
  } else {
    const content = tableau as ItableDblEntry
    return content.raws.some(el => el.find(cell => cell.latex) != null) != null || content.headingCols.find(cell => cell.latex) || content.headingLines.find(cell => cell.latex)
  }
}

/**
 * Un getValue commun aux deux className
 */
export function getValue (item: TableMathlive): Record<string, string> {
  const mathfields = Array.from(item.container.querySelectorAll('math-field')) as MathfieldElement[]
  const value: Record<string, string> = {}
  const prefixLength = 2 + item.suffix.length
  for (const mf of mathfields) {
    if (!mf.readOnly) {
      const key = mf.id.substring(prefixLength, mf.id.length)
      const val = mf.value
      const newEntry = Object.fromEntries([[key, val]])
      Object.assign(value, newEntry)
    }
  }
  return value
}

export function isAnswered (item: TableMathlive): boolean {
  const mathfields = Array.from(item.container.querySelectorAll('math-field')) as MathfieldElement[]
  return mathfields.filter(mf => mf.value !== '').length === mathfields.length
}

export function setFeedback (item: TableMathlive): void {
  // @todo à implémenter
  console.warn(`méthode setFeedback des tableaux à implémenter sur ${item.constructor.name}`)
}

export function appendCell ({ line, icell, indexCol, indexLine, tag, className, suffix }: {
  line: HTMLElement,
  icell: Icell,
  indexCol: number,
  indexLine: number,
  tag: 'th' | 'td',
  className: string,
  suffix: string
}) {
  const cell = document.createElement(tag)
  if (className == null) className = ''
  if (icell.texte === '') {
    const mfe = new MathfieldElement() as MathfieldWithKeyboard
    mfe.classList.add(`tableauMathlive;${className.replaceAll(' ', ';')}`)
    mfe.id = `mf${suffix}L${indexLine}C${indexCol}`
    if (mfe instanceof MathfieldElement) mfe.menuItems = []
    cell.appendChild(mfe)
    const keyboard = new VirtualKeyboard(mfe, /[^a-z^A-Z]/, { acceptSpecialChars: false })
    mfe.virtualKeyboard = keyboard
    const divDuSmiley = document.createElement('div')
    divDuSmiley.id = `divDuSmiley${suffix}L${indexLine}C${indexCol}`
    cell.appendChild(divDuSmiley)
  } else {
    let span: HTMLSpanElement
    if (icell.latex) {
      span = document.createElement('span')
      cell.appendChild(span)
      span.outerHTML = `<span id="span${suffix}L${indexLine}C${indexCol}">$${icell.texte}$</span>`
    } else {
      span = document.createElement('span')
      cell.appendChild(span)
      span.outerHTML = `<span id="span${suffix}L${indexLine}C${indexCol}">${icell.texte}</span>`
    }
  }

  line.appendChild(cell)
}

/**
 * @param {HTMLTableRowElement} line L’élément HTML <tr> qui reçoit les cellules
 * @param {object} params
 * @param {Icell[]} [params.content] la liste des cellules au format Icell
 * @param {number} [params.index] le numéro de la ligne pour fabriquer l’identifiant de cellule
 * @param {'td'|'th'} [params.tag] le tag (td pour des cellules à l’intérieur th pour des cellules d’entête)
 * @param {string} [params.className] une liste de className dans un string séparés par des espaces c’est pour le
 *     math-field
 */
export function fillLine ({ line, content, index, tag, className, suffix }: {
  line: HTMLElement, content: Icell[], index: number, tag: 'td' | 'th', className: string, suffix: string
}): void {
  for (let i = 0; i < content.length; i++) {
    const icell = content[i]
    if (icell != null) {
      appendCell({ line, icell, indexCol: i, indexLine: index, tag, className, suffix })
    }
  }
}
