import type { Item } from 'src/lib/player/types'
import { MathfieldWithKeyboard } from 'src/lib/types'
import { appendCell, getValue, isAnswered, setFeedback } from './main'

import type { Icell, ItableProp, TableMathliveOptions } from './tableMathlive'

import './tableMathlive.scss'

class TableMathliveProp implements Item {
  id: string // ce sera nécessaire pour retrouver le tableau s’il y en a plusieurs dans la page.
  suffix: string // c’est une partie de l’id, mais j’ai pas envie de jouer avec les regex pour l’extraire chaque fois
  // que j’en ai besoin
  output!: string
  nbColonnes: number
  ligne1!: Icell[]
  ligne2!: Icell[]
  //   flecheHaut: Fleche[] // [[1, 2, '\\times 6,4', 3], [2, 3, '\\div 6']]
  //   flecheBas: Fleche[]
  //   flecheDroite: FlecheCote // à remplacer par un string
  //   flecheDroiteSens: FlecheSens
  //   flecheGauche: FlecheCote
  //   flecheGaucheSens: FlecheSens
  className: string
  container!: HTMLTableElement
  persistent: boolean

  private constructor (suffix: string, tableau: ItableProp, { className, persistent }: Required<TableMathliveOptions>) {
    this.nbColonnes = tableau.nbColonnes ?? 1
    //     this.flecheHaut = tableau.flecheHaut ?? []
    //     this.flecheBas = tableau.flecheBas ?? []
    //     this.flecheDroite = tableau.flecheDroite ?? false
    //     this.flecheDroiteSens = tableau.flecheDroiteSens ?? 'bas'
    //     this.flecheGauche = tableau.flecheGauche ?? false
    //     this.flecheGaucheSens = tableau.flecheGaucheSens ?? 'haut'
    this.id = `tabML${suffix}`
    this.suffix = suffix
    this.className = className
    this.persistent = persistent
  }

  static create (suffix: string, tableau: ItableProp, {
    className = '',
    persistent = false
  }: TableMathliveOptions = {}) {
    if (!Array.isArray(tableau.ligne1) || !Array.isArray(tableau.ligne1)) {
      throw Error(`ajouteTableauMathlive : vérifiez vos paramètres : ${JSON.stringify({
        ligne1: tableau.ligne1, ligne2: tableau.ligne2, nbColonnes: tableau.nbColonnes
      })}`)
    }
    if (className == null) className = ''
    // ça, c’est pour ne pas modifier les lignes du tableau passé en argument
    const ligne1 = [...tableau.ligne1]
    const ligne2 = [...tableau.ligne2]
    const tableauMathlive: TableMathliveProp = new TableMathliveProp(suffix, tableau, { className, persistent })
    const table = document.createElement('table')
    table.className = className !== '' ? className : 'tableauMathlive'
    table.id = tableauMathlive.id
    const firstLine = document.createElement('tr')
    const entete1 = ligne1.shift()
    if (entete1) {
      appendCell({
        line: firstLine, icell: entete1, indexCol: 0, indexLine: 0, tag: 'th', className, suffix
      })
    }
    for (let i = 0; i < ligne1.length; i++) {
      const icell = ligne1[i]
      if (icell != null) {
        appendCell({
          line: firstLine, icell, indexCol: i + 1, indexLine: 0, tag: 'td', className, suffix
        })
      }
    }
    table.appendChild(firstLine)
    // tableau de proportionnalité conforme à ceux définis dans src/lib/2d/tableau.js
    const secondLine = document.createElement('tr')
    const entete2 = ligne2.shift()
    if (entete2) {
      appendCell({
        line: secondLine, icell: entete2, indexCol: 0, indexLine: 1, tag: 'th', className, suffix
      })
    }
    for (let i = 0; i < ligne2.length; i++) {
      const icell = ligne2[i]
      if (icell != null) {
        appendCell({
          line: secondLine, icell, indexCol: i + 1, indexLine: 1, tag: 'td', className, suffix
        })
      }
    }
    table.appendChild(secondLine)
    // pour l’instant je retourne l’objet complet avec le HTML de la table dans sa propriété output,
    // mais il sera peut-être possible de ne retourner que le HTML comme pour ajouteChampTexteMathlive...
    tableauMathlive.output = table.outerHTML
    tableauMathlive.container = table
    return tableauMathlive
  }

  getValue () {
    return getValue(this)
  }

  isAnswered () {
    return isAnswered(this)
  }

  setFeedback () {
    return setFeedback(this)
  }

  setFocus () {
    const firstCellToFill = this.container.querySelector('math-field')
    if (firstCellToFill != null) (firstCellToFill as MathfieldWithKeyboard).focus()
  }
}

export default TableMathliveProp
