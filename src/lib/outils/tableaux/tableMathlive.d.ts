import type TableMathliveDblEntry from './TableMathliveDblEntry'
import type TableMathliveProp from './TableMathliveProp'

export type TableMathlive = TableMathliveDblEntry | TableMathliveProp

export interface Icell {
  texte: string
  latex: boolean
  gras: boolean
  color: string
}

/* Un projet pour ajouter des flèches d’une colonne à une autre, d’une ligne à une autre (comme pour un tableau de proportionnalité)
 export type FlecheCote = false | string
 export type FlecheSens = 'bas' | 'haut'
 export type Fleche = [number, number, string, number] | [number, number, string]
 */
export type Raws = Array<Icell[]>

// Le TabPropMathlive ne gère pas les flèches... alors, je ne sais pas pourquoi j’ai mis toutes ces propriétés
// faculatives... Pour l’avenir ? Si quelqu’un sait faire, qu’il ne se gêne pas !
export interface ItableProp {
  nbColonnes: number
  ligne1: Icell[]
  ligne2: Icell[]
}

export interface TableMathliveOptions {
  className?: string
  persistent?: boolean
}

export interface ItableDblEntry {
  raws: Array<Icell[]>
  headingCols: Icell[]
  headingLines: Icell[]
}

export interface DomEltTableValues {
  tableau: ItableDblEntry | ItableProp
  inputProps: {
    name: string, classes: string, sousType: 'prop' | 'dblEntry'
  }
  persistent?: boolean
}
