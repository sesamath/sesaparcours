import { addElement } from 'src/lib/utils/dom/main'

interface TableauCreateOptions {
  headingLines?: string[],
  headingCols?: string[],
  raws: Array<string[]>,
  options?: Record<string, string>
}

/**
 * Outil pour intégrer à un énoncé facilement une table html.
 * Cette classe n’a peut-être plus d’utilité, car on peut utiliser TableMathliveDblEntry ou TableMathliveProp,
 * prévues pour mettre des inputs mathlive dans les cellules, mais uniquement si la cellule est vide.
 * Si les cellules sont toutes remplies, alors c’est l’équivalent de ce Tableau.
 */
class Tableau {
  parent: HTMLElement
  table: HTMLTableElement

  private constructor (parent: HTMLElement, table: HTMLTableElement) {
    this.parent = parent
    this.table = table
  }

  static create (parent: HTMLElement, { headingLines, headingCols, raws, options }: TableauCreateOptions): Tableau {
    const table = addElement(parent, 'table')

    // Première ligne En-tête ou pas ?
    if (headingCols != null) {
      const firstLine = addElement(table, 'tr')
      for (let i = 0; i < headingCols.length; i++) {
        addElement(firstLine, 'th', { content: headingCols[i] })
      }
    }
    // lignes suivantes
    for (let j = 0; j < raws.length; j++) {
      const newLine = addElement(table, 'tr')
      if (headingLines != null) {
        addElement(newLine, 'th', { content: headingLines[j] })
      }
      const raw = raws[j]
      if (Array.isArray(raw) && raw.length > 0) {
        for (let i = 0; i < raw.length; i++) {
          addElement(newLine, 'td', { content: raw[i] })
        }
      }
    }
    if (options?.className != null) table.classList.add(options.className)

    parent.appendChild(table)
    return new Tableau(parent, table)
  }
}

export default Tableau
