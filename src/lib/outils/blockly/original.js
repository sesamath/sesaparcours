/** @module lib/outils/blockly/original */
import * as Blockly from 'blockly/core'
import 'blockly/blocks'
import 'blockly/javascript'
import * as Fr from 'blockly/msg/fr'

Blockly.setLocale(Fr)

/**
 * Le Blockly issu du package blockly (≠ de celui de scratch-blocks) configuré en fr, avec blockly/blocks et blockly/javascript
 */
export default Blockly
