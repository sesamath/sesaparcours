import { addElement, addTextContent, empty } from 'src/lib/utils/dom/main'
import { MathfieldElement } from 'mathlive'

import type { EltOptions } from 'src/lib/utils/dom/dom'
import type { EventListener, KeyboardEventListener } from 'src/lib/types'

// et notre css
import 'src/lib/outils/listeDeroulante/listeDeroulante.scss'

/*
L’outil ListeDeroulante créé un élément complexe remplaçant le HTMLSelectElement où on peut mettre dans chaque item du LaTex, une image, etc.
ListeDeroulante repose sur l’argument essentiel 'choices' (un array de Choice).
    Choice mélange deux types de choix :
      string ou {latex?: string, image?: string, label?: string, value: string}
    Le deuxième permet d’introduire dans la liste, du latex, une image, ou un label qui est un texte différent de value.
    On utilisera le type string si le label et la value ont la même valeur.
    On peut mixer les différentes formes, par exemple :
      choices: Choice[] = [{image: 'src/section/datasExemple/firefox.png', value: 'firefox'},
            {latex: '\\frac{\\sqrt{3}}{2}',value: 'sinusPiSur3'},
            {label: "n’est pas définie en zéro", value: 'noF0'}
            'Je ne sais pas']
 */
interface IChoice {
  latex?: string,
  image?: string,
  label?: string,
  value: string
}

export type Choice = string | IChoice

export type OnChange = (choice: Choice) => void

interface ListeDeroulanteCreateOptions {
  select?: number
  heading?: string
  onChange?: OnChange
}

interface CstOptions {
  onChange?: OnChange
}

interface ListeDeroulanteInitOptions {
  conteneur: HTMLElement
  select: number
  heading: string
}

export function setPointEventsToNone (htmlString: string) {
  return htmlString.replaceAll('pointer-events:auto', 'pointer-events:none')
}

/**
 * L’outil ListeDeroulante créé un "select" like où on peut mettre dans chaque option du LaTex, une image, etc.
 * Il faut passer par ListeDeroulante.create() et pas new ListeDeroulante(…)
 * @class
 */
class ListeDeroulante {
  heading!: string
  disabled: boolean = false
  choices: Choice[] // La liste des choix
  /** Callback éventuelle à appeler avec le choix fait à chaque changement */
  onChange?: OnChange
  changed: boolean = false // Passe à true dès qu’on a fait une sélection (ou dès le départ si le choix initial est sélectionnable)
  response: string = ''
  container!: HTMLElement
  spanSelected?: HTMLSpanElement
  ulContainer?: HTMLUListElement
  initialChoice?: Choice
  givenChoices?: Choice[]
  width?: number
  private _kbIndex: number = -1 // L’index du choix sélectionné au clavier
  private _elts: HTMLLIElement[] = [] // Liste des elts contenant les choix (les &lt;li&gt;)
  private _clickListener?: EventListener
  private _keydownListener?: KeyboardEventListener

  /**
   * Le constructeur ne peut pas être appelé de l’extérieur, il faut passer par create
   * @constructor
   */
  protected constructor (choices: Choice[], { onChange }: CstOptions = {}) {
    if (!Array.isArray(choices)) throw Error('Il faut passer une liste de choix')
    /**
     * true lorsgue la liste est désactivée
     * @type {boolean}
     */
    this.disabled = false
    /**
     * La liste des choix donnée initialement
     * @type {Choice[]}
     */
    this.givenChoices = [...choices]
    /**
     * La liste des choix
     * @type {Choice[]}
     */
    this.choices = [...choices]
    this._kbIndex = -1 // L’index du choix sélectionné au clavier
    this.changed = false // Passe à true dès qu’on a fait une sélection (ou dès le départ si le choix initial est sélectionnable)
    this.onChange = onChange
    this.response = '' // Le choix courant
    this._elts = [] // Liste des elts contenant les choix (les <li>)
  }

  /**
   * Ajoute la liste dans le dom et retourne l’objet ListeDeroulante qu’elle a créé
   * @name ListeDeroulante
   * @param {HTMLElement} conteneur
   * @param {IChoice[]} choices la liste des choix
   * @param {Object} [parametres]  Les paramètres de cette liste
   * @param {number} [parametres.select=0] index de choices à sélectionner dès le départ (ignoré si choix est précisé)
   * @return {ListeDeroulante}
   */
  static create (conteneur: HTMLElement, choices: Choice[], {
    select = 0,
    heading = '',
    onChange
  }: ListeDeroulanteCreateOptions = {}): ListeDeroulante {
    const ld = new ListeDeroulante(choices, { onChange })
    ld._init({ conteneur, select, heading })
    return ld
  }

  /**
   * La méthode qui affiche l’un des choix, que ce soit du texte, du latex ou une image.
   * @param {HTMLLIElement} li l’élément de liste qui doit contenir le choix
   * @param {Choice} choice le choix en question
   */
  static afficheChoice (li: HTMLLIElement | HTMLSpanElement, choice: Choice) {
    if (typeof choice === 'string') {
      addTextContent(li, choice)
    } else if (typeof choice === 'object') {
      if ('latex' in choice) {
        const mf: MathfieldElement = new MathfieldElement()
        mf.value = choice.latex ?? choice.value
        mf.readOnly = true
        const shadowRoot = mf.shadowRoot
        if (shadowRoot) mf.shadowRoot.innerHTML = setPointEventsToNone(shadowRoot.innerHTML)
        li.appendChild(mf)
      } else if ('image' in choice) {
        addElement(li, 'img', { src: choice.image ?? choice.value })
      } else if ('label' in choice) {
        addTextContent(li, choice.label ?? choice.value)
      } else {
        console.error('Le choix ne contient pas d’élément affichable')
      }
    } else {
      throw Error(`Type de choix inconnu : ${choice}`)
    }
  }

  /**
   * Crée les éléments dans le DOM (appelé par create) et ajoute les listeners
   * @param {object} params
   * @param {HTMLSpanElement} [params.conteneur]
   * @param {number} [params.select] // l’élément sélectionné initialement
   * @param {string} [params.heading] // Un titre non sélectionnable par exemple : 'Faire un choix'
   * @private
   */
  protected _init ({ conteneur, select, heading }: ListeDeroulanteInitOptions) {
    this.heading = heading
    /**
     * Le span qui va contenir la liste (tous les éléments que l’on crée, enfant de conteneur)
     * @type {HTMLSpanElement}
     */
    if (conteneur instanceof HTMLSpanElement && conteneur.className.includes('listeDeroulante')) {
      this.container = conteneur
    } else {
      this.container = addElement(conteneur, 'span', { className: 'listeDeroulante' })
    }
    const char = '˅'
    const spanSelectedProps: EltOptions<'span'> = {
      className: 'currentChoice',
      role: 'listbox',
      tabIndex: 0,
      content: this.heading ?? ''
    }
    /**
     * Le span de l’élément sélectionné
     * @type {HTMLSpanElement}
     */
    this.spanSelected = addElement(this.container, 'span', spanSelectedProps)

    // les listeners sur container, faut les mettre en propriété pour pouvoir les retirer dans disable()
    this._clickListener = (event) => {
      event?.stopPropagation()
      this.toggle()
    }
    this.container.onclick = this._clickListener
    /**
     * listener de keydown sur spanSelected, pour sélection au clavier
     * @param {KeyboardEvent} event
     */
    this._keydownListener = (event: KeyboardEvent): void => {
      const { code, key } = event
      if (code === 'Tab' || key === 'Tab') {
        // on sort du menu au clavier, faut le refermer
        this.hide()
      } else if (code === 'ArrowDown' || key === 'ArrowDown') {
        event.preventDefault()
        if (this._kbIndex < this.choices.length - 1) {
          this._kbIndex++
          if (!this.isVisible()) this.show()
          this._kbSelect()
        }
      } else if (code === 'ArrowUp' || key === 'ArrowUp') {
        event.preventDefault()
        if (this._kbIndex === -1) this._kbIndex = this.choices.length // 1er clic sur arrowUp, on part de la fin
        if (this._kbIndex > 0) {
          this._kbIndex--
          if (!this.isVisible()) this.show()
          this._kbSelect()
        }
      } else if (code === 'Space' || key === 'Space' || code === 'Enter' || key === 'Enter') {
        if (this.isVisible()) {
          this.select(this._kbIndex)
        } else {
          this.show()
        }
      }
    }
    const fleche = addElement(this.container, 'span', { className: 'trigger', textContent: char })
    if (this._clickListener) fleche.onclick = this._clickListener
    // la liste qui peut être masquée (il doit passer au-dessus du reste, les boutons sont à 50, les modales à 90)
    this.ulContainer = addElement(this.container, 'ul')
    // si le premier élément de la liste n’est pas sélectionnable, on le sort de la liste
    this.initialChoice = heading ? undefined : this.choices[0]

    // les choix
    const liProps: {
      role: string,
      style?: object
    } = { role: 'option' }
    for (const [index, choice] of this.choices.entries()) {
      const li = addElement(this.ulContainer, 'li', liProps)
      // si y’a du mathLive dans choice, faut que ce soit visible, sinon mathLive peut faire des bêtises dans ses calculs de dimensionnement
      ListeDeroulante.afficheChoice(li, choice)
      li.addEventListener('click', (event) => {
        event.stopPropagation() // faut pas propager à container sinon il va rouvrir la liste après select()
        this.select(index)
      })
      this._elts.push(li)
    }
    // Il faut faire ça pour récupérer this.width à l’init ce qui permet de fixer la largeur minimum du spanSelected et d’aligner la liste à droite du spanSelected
    this.show() // fixe le this.width car il est undefined.
    this.spanSelected.style.minWidth = this.width + 'px'
    this.hide()

    this.spanSelected.addEventListener('keydown', this._keydownListener)
    // pour refermer le menu si on sort au clavier,
    // on a essayé de refermer le menu au focusout sur container (avec un timeout sinon ça ferme avant de déclencher
    // le clic sur un li et on se retrouve à cliquer dessous), mais ça pose plus de pb que ça n’en résoud
    // (le clic sur la flèche referme parfois le menu aussitôt)
    // => on gère le tab sortant dans _keydownListener

    // et un listener pour refermer la liste si on clique ailleurs
    document.body.addEventListener('click', ({ target }) => {
      // si on trouve un .listeDeroulante dans un parent on ne fait rien
      /** @type {null|HTMLElement} */
      let parent: EventTarget | null = target
      while (parent && parent instanceof HTMLElement) {
        if (parent.classList.contains('listeDeroulante')) return
        parent = parent.parentElement
      }
      // sinon on cache
      this.hide()
    })
    if (select) this.select(select, { withoutCallback: true })
    this.reset({ withoutCallback: true })
    this._replace()
  }

  /**
   * met le focus sur l’élément sélectionné
   */
  focus () {
    if (this.spanSelected) this.spanSelected.focus()
  }

  /**
   * positionne le ul par rapport au spanSelected
   * @private
   */
  _replace () {
    if (this.container) {
      const height = this.container.offsetHeight
      if (this.ulContainer) {
        this.ulContainer.style.top = `${height}px`
        this.ulContainer.style.left = '0px'
      }
    }
  }

  /**
   * Remet la liste dans son état initial
   * @param {Object} [options]
   * @param {boolean} [options.withoutCallback] Passer true pour ne pas appeler la callback
   */
  reset ({ withoutCallback }: {
    withoutCallback?: boolean
  } = {}) {
    this.hide()
    if (this.spanSelected) {
      empty(this.spanSelected)
      if (this.initialChoice) {
        ListeDeroulante.afficheChoice(this.spanSelected, this.initialChoice)
      } else {
        this.spanSelected.textContent = this.heading
      }
      this.spanSelected.style.fontStyle = 'italic'
      this.spanSelected.style.color = 'Grey'
      this._kbIndex = -1
      this.response = ''
      this.changed = false
      this._replace() // au cas où la hauteur de spanSelected aurait changé
      if (!withoutCallback && this.onChange && this.initialChoice) this.onChange(this.initialChoice)
    }
  }

  /**
   * Sélectionne le choix index (dans le tableau fourni initialement)
   * @param {number} index index dans choices
   * @param {Object} [options]
   * @param {boolean} [options.withoutCallback] Passer true pour ne pas appeler la callback (seulement à l’init à priori)
   */
  select (index: number, { withoutCallback }: {
    withoutCallback?: boolean
  } = {}) {
    if (this.spanSelected != null) {
      if (this.disabled) return
      this.spanSelected.style.fontStyle = ''
      this.spanSelected.style.color = ''
      if (!Number.isInteger(index)) return Error(`index non entier : ${index}`)
      // faut décaler l’index si on a viré le 1er elt à l’init
      if (index < 0 || index >= this.choices.length) {
        return console.error(`index invalide : ${index} non compris entre ${0} et ${this.choices.length - 1}`)
      }
      empty(this.spanSelected)
      const choix = this.choices[index]
      if (choix) {
        ListeDeroulante.afficheChoice(this.spanSelected, choix)
      }
      this.response = String(typeof choix === 'string' ? choix : typeof choix === 'object' ? choix?.value : 'undefined')
      if (this.response === 'undefined') throw Error(`Un problème avec la valeur de ce choix : ${choix}`)
      this.changed = true
      if (this.onChange && !withoutCallback && choix != null) this.onChange(choix)
      this._kbIndex = index
      for (const [i, li] of this._elts.entries()) {
        if (i === index) li.classList.add('selected')
        else li.classList.remove('selected')
      }
      this._replace() // la hauteur de spanSelected peut changer
      this.focus()
      this.hide()
    }
  }

  /**
   * Marque un élément comme étant sélectionné au clavier (sera ensuite vraiment sélectionné si on appuie ensuite sur entrée)
   */
  _kbSelect () {
    for (const [i, li] of this._elts.entries()) {
      if (this._kbIndex === i) li.classList.add('selected')
      else li.classList.remove('selected')
    }
  }

  hide () {
    if (this.ulContainer) this.ulContainer.classList.remove('visible')
  }

  show () {
    if (this.disabled) return
    // il faut d’abord masquer toutes les autres listes qui pourraient être ouverte (pour éviter des chevauchements)
    for (const ul of document.querySelectorAll('.listeDeroulante ul.visible')) {
      ul.classList.remove('visible')
    }
    if (this.ulContainer) this.ulContainer.classList.add('visible')
    // Si this.width n’est pas défini (ça veut dire que c’est l’init qui l’appelle) on le renseigne et on replace la liste à droite et en dessous du spanSelected.
    if (!this.width) {
      this.width = this.ulContainer?.offsetWidth
      this._replace()
    }
    this.focus() // pour usage au clavier
  }

  /**
   * Pour changer l’état 'visible' de la liste
   */
  toggle () {
    if (this.disabled) return
    if (this.isVisible()) this.hide()
    else this.show()
  }

  isVisible () {
    if (this.ulContainer) return this.ulContainer.classList.contains('visible')
    else return false
  }
}

export default ListeDeroulante
