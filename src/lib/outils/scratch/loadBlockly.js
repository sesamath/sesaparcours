/** @module lib/outils/scratch/loadBlockly */
/**
 * Charge Blockly en async (dynamic import) et le retourne
 * @returns Promise<Object>
 */
export default function loadBlockly () {
  import(/* @vite-ignore */ 'vendors/scratch/Blockly').then(module => module.default)
}
