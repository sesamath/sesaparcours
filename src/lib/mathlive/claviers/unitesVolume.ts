import type { VirtualKeyboardLayout } from 'mathlive'
import type { Shortcuts } from 'src/lib/mathlive/loadKeyboard'

export const layout: VirtualKeyboardLayout = {
  label: 'Volumes', // Label displayed in the Virtual Keyboard Switcher
  tooltip: 'Clavier mathématique (Volumes)', // Tooltip when hovering over the label
  rows: [
    [
      { latex: '\\operatorname{cL}' },
      { latex: '\\operatorname{mL}' },
      { class: 'separator w5' },
      { latex: '\\operatorname{cm}^3' },
      { latex: '\\operatorname{mm}^3' },
      { class: 'separator w5' },
      { label: '7', key: '7' },
      { label: '8', key: '8' },
      { label: '9', key: '9' },
      { latex: '\\div' },
      { class: 'separator w5' },
      {
        class: 'tex small',
        label: '<span><i>x</i>&thinsp;²</span>',
        insert: '$$#@^{2}$$'
      },
      {
        class: 'tex small',
        label: '<span><i>x</i><sup>&thinsp;<i>3</i></sup></span>',
        insert: '$$#@^{3}$$'
      },
      {
        class: 'small',
        latex: '\\sqrt{#0}',
        insert: '$$\\sqrt{#0}$$'
      }
    ],
    [
      { latex: '\\operatorname{L}' },
      { latex: '\\operatorname{dL}' },
      { class: 'separator w5' },
      { latex: '\\operatorname{m}^3' },
      { latex: '\\operatorname{dm}^3' },
      { class: 'separator w5' },
      { label: '4', latex: '4' },
      { label: '5', key: '5' },
      { label: '6', key: '6' },
      { latex: '\\times' },
      { class: 'separator w5' },
      { class: 'small', latex: '\\frac{#0}{#0}' },
      { label: '=', key: '=' },
      { latex: 'f' }
    ],
    [
      { latex: '\\operatorname{hL}' },
      { latex: '\\operatorname{daL}' },
      { class: 'separator w5' },
      { latex: '\\operatorname{hm}^3' },
      { latex: '\\operatorname{dam}^3' },
      { class: 'separator w5' },
      { label: '1', key: '1' },
      { label: '2', key: '2' },
      { label: '3', key: '3' },
      { latex: '-' },
      { class: 'separator w5' },
      { label: ';', key: ';' },
      { label: 'oui', key: 'oui' },
      { label: 'non', key: 'non' }
    ],
    [
      { class: 'separator w15' },
      { class: 'separator w15' },
      { latex: '\\operatorname{km}^3' },
      { class: 'separator w10' },
      { label: '0', key: '0' },
      { latex: ',' },
      { latex: '\\pi' },
      { latex: '+' },
      { class: 'separator w5' },
      {
        class: 'action',
        label: "<svg><use xlink:href='#svg-arrow-left' /></svg>",
        command: ['performWithFeedback', 'moveToPreviousChar']
      },
      {
        class: 'action',
        label: "<svg><use xlink:href='#svg-arrow-right' /></svg>",
        command: ['performWithFeedback', 'moveToNextChar']
      },
      {
        class: 'action font-glyph',
        label: '&#x232b;',
        command: ['performWithFeedback', 'deleteBackward']
      }
    ]
  ]
}

export const shortcuts: Shortcuts = {
  mm3: { mode: 'math', value: '\\operatorname{mm}^3' },
  cm3: { mode: 'math', value: '\\operatorname{cm}^3' },
  dm3: { mode: 'math', value: '\\operatorname{dm}^3' },
  m3: { mode: 'math', value: '\\operatorname{m}^3' },
  dam3: { mode: 'math', value: '\\operatorname{dam}^3' },
  hm3: { mode: 'math', value: '\\operatorname{hm}^3' },
  km3: { mode: 'math', value: '\\operatorname{km}^3' },
  '*': { mode: 'math', value: '\\times' },
  '.': { mode: 'math', value: ',' }
}
