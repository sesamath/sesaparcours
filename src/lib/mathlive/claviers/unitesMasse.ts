import type { VirtualKeyboardLayout } from 'mathlive'
import type { Shortcuts } from 'src/lib/mathlive/loadKeyboard'

export const layout: VirtualKeyboardLayout = {
  label: 'Masses', // Label displayed in the Virtual Keyboard Switcher
  tooltip: 'Clavier mathématique (Masses)', // Tooltip when hovering over the label
  rows: [
    [
      { latex: '\\operatorname{dg}' },
      { latex: '\\operatorname{cg}' },
      { latex: '\\operatorname{mg}' },
      { class: 'separator w5' },
      { label: '7', key: '7' },
      { label: '8', key: '8' },
      { label: '9', key: '9' },
      { latex: '\\div' },
      { class: 'separator w5' },
      {
        class: 'tex small',
        label: '<span><i>x</i>&thinsp;²</span>',
        insert: '$$#@^{2}$$'
      },
      {
        class: 'tex small',
        label: '<span><i>x</i><sup>&thinsp;<i>3</i></sup></span>',
        insert: '$$#@^{3}$$'
      },
      {
        class: 'small',
        latex: '\\sqrt{#0}',
        insert: '$$\\sqrt{#0}$$'
      }
    ],
    [
      { latex: '\\operatorname{hg}' },
      { latex: '\\operatorname{dag}' },
      { latex: '\\operatorname{g}' },
      { class: 'separator w5' },
      { label: '4', latex: '4' },
      { label: '5', key: '5' },
      { label: '6', key: '6' },
      { latex: '\\times' },
      { class: 'separator w5' },
      { class: 'small', latex: '\\frac{#0}{#0}' },
      { label: '=', key: '=' },
      { latex: 'f' }
    ],
    [
      { class: 'separator w8' },
      { latex: '\\operatorname{kg}' },
      { class: 'separator w15' },
      { label: '1', key: '1' },
      { label: '2', key: '2' },
      { label: '3', key: '3' },
      { latex: '-' },
      { class: 'separator w5' },
      { label: ';', key: ';' },
      { label: 'oui', key: 'oui' },
      { label: 'non', key: 'non' }
    ],
    [
      { class: 'separator w10' },
      { latex: '\\operatorname{t}' },
      { latex: '\\operatorname{q}' },
      { class: 'separator w10' },
      { label: '0', key: '0' },
      { latex: ',' },
      { latex: '\\pi' },
      { latex: '+' },
      { class: 'separator w5' },
      {
        class: 'action',
        label: "<svg><use xlink:href='#svg-arrow-left' /></svg>",
        command: ['performWithFeedback', 'moveToPreviousChar']
      },
      {
        class: 'action',
        label: "<svg><use xlink:href='#svg-arrow-right' /></svg>",
        command: ['performWithFeedback', 'moveToNextChar']
      },
      {
        class: 'action font-glyph',
        label: '&#x232b;',
        command: ['performWithFeedback', 'deleteBackward']
      },
      { class: 'separator w5' }
    ]
  ]
}

export const shortcuts: Shortcuts = {
  mg: { mode: 'math', value: '\\operatorname{mg}' },
  cg: { mode: 'math', value: '\\operatorname{cg}' },
  dg: { mode: 'math', value: '\\operatorname{dg}' },
  g: { mode: 'math', value: '\\operatorname{g}' },
  dag: { mode: 'math', value: '\\operatorname{dag}' },
  hg: { mode: 'math', value: '\\operatorname{hg}' },
  kg: { mode: 'math', value: '\\operatorname{kg}' },
  '*': { mode: 'math', value: '\\times' },
  '.': { mode: 'math', value: ',' }
}
