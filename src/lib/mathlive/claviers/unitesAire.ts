import type { VirtualKeyboardLayout } from 'mathlive'
import type { Shortcuts } from 'src/lib/mathlive/loadKeyboard'

export const layout: VirtualKeyboardLayout = {
  label: 'Aires', // Label displayed in the Virtual Keyboard Switcher
  tooltip: 'Clavier mathématique (Aires)', // Tooltip when hovering over the label
  rows: [
    [
      { latex: '\\operatorname{dm}^2' },
      { latex: '\\operatorname{cm}^2' },
      { latex: '\\operatorname{mm}^2' },
      { class: 'separator w5' },
      { label: '7', key: '7' },
      { label: '8', key: '8' },
      { label: '9', key: '9' },
      { latex: '\\div' },
      { class: 'separator w5' },
      {
        class: 'tex small',
        label: '<span><i>x</i>&thinsp;²</span>',
        insert: '$$#@^{2}$$'
      },
      {
        class: 'tex small',
        label: '<span><i>x</i><sup>&thinsp;<i>3</i></sup></span>',
        insert: '$$#@^{3}$$'
      },
      {
        class: 'small',
        latex: '\\sqrt{#0}',
        insert: '$$\\sqrt{#0}$$'
      }
    ],
    [
      { latex: '\\operatorname{hm}^2' },
      { latex: '\\operatorname{dam}^2' },
      { latex: '\\operatorname{m}^2' },
      { class: 'separator w5' },
      { label: '4', latex: '4' },
      { label: '5', key: '5' },
      { label: '6', key: '6' },
      { latex: '\\times' },
      { class: 'separator w5' },
      { class: 'small', latex: '\\frac{#0}{#0}' },
      { label: '=', key: '=' },
      { latex: 'f' }
    ],
    [
      { class: 'separator w8' },
      { latex: '\\operatorname{km}^2' },
      { class: 'separator w15' },
      { label: '1', key: '1' },
      { label: '2', key: '2' },
      { label: '3', key: '3' },
      { latex: '-' },
      { class: 'separator w5' },
      { label: ';', key: ';' },
      { label: 'oui', key: 'oui' },
      { label: 'non', key: 'non' }
    ],
    [
      { latex: '\\operatorname{ha}' },
      { latex: '\\operatorname{a}' },
      { latex: '\\operatorname{ca}' },
      { class: 'separator w5' },
      { label: '0', key: '0' },
      { latex: ',' },
      { latex: '\\pi' },
      { latex: '+' },
      { class: 'separator w5' },
      {
        class: 'action',
        label: "<svg><use xlink:href='#svg-arrow-left' /></svg>",
        command: ['performWithFeedback', 'moveToPreviousChar']
      },
      {
        class: 'action',
        label: "<svg><use xlink:href='#svg-arrow-right' /></svg>",
        command: ['performWithFeedback', 'moveToNextChar']
      },
      {
        class: 'action font-glyph',
        label: '&#x232b;',
        command: ['performWithFeedback', 'deleteBackward']
      }
    ]
  ]
}

export const shortcuts: Shortcuts = {
  mm2: { mode: 'math', value: '\\operatorname{mm}^2' },
  cm2: { mode: 'math', value: '\\operatorname{cm}^2' },
  dm2: { mode: 'math', value: '\\operatorname{dm}^2' },
  m2: { mode: 'math', value: '\\operatorname{m}^2' },
  dam2: { mode: 'math', value: '\\operatorname{dam}^2' },
  hm2: { mode: 'math', value: '\\operatorname{hm}^2' },
  km2: { mode: 'math', value: '\\operatorname{km}^2' },
  a: { mode: 'math', value: '\\operatorname{a}' },
  ha: { mode: 'math', value: '\\operatorname{ha}' },
  '*': { mode: 'math', value: '\\times' },
  '.': { mode: 'math', value: ',' }
}
