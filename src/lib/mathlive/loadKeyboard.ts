import { type VirtualKeyboardLayout, VirtualKeyboardName } from 'mathlive'

interface Shortcut {
  mode: string
  value: string
}
export type Shortcuts = Record<string, Shortcut | string>

interface CustomKeyboard {
  layout: VirtualKeyboardLayout
  shortcuts: Shortcuts
}

type KeyboardLoader = () => Promise<CustomKeyboard>

const keyboardLoaders: Record<string, KeyboardLoader> = {
  '6e': () => import('./claviers/6e'),
  college: () => import('./claviers/college'),
  hms: () => import('./claviers/hms'),
  lycee: () => import('./claviers/lycee'),
  trigo: () => import('./claviers/trigo'),
  unitesAire: () => import('./claviers/unitesAire'),
  unitesLongueur: () => import('./claviers/unitesLongueur'),
  unitesMasse: () => import('./claviers/unitesMasse'),
  unitesVolume: () => import('./claviers/unitesVolume')
}

/**
 * Les noms des claviers fournis avec mathlive
 */
const virtualKeyboardName: string[] = ['default', 'numeric', 'symbols', 'alphabetic', 'greek', 'minimalist', 'compact', 'numeric-only']

export function isKeyboardName (name: string): name is VirtualKeyboardName {
  return virtualKeyboardName.includes(name)
}

export const isCustomKeyboardName = (name: string): boolean => name in keyboardLoaders

type CustomKeyboardName = keyof typeof keyboardLoaders

export type MAthliveKeyboardName = VirtualKeyboardName | CustomKeyboardName

export async function loadKeyboard (name: CustomKeyboardName): Promise<CustomKeyboard> {
  if (name in keyboardLoaders) {
    const loader = keyboardLoaders[name]
    if (loader != null) return loader()
  }
  throw Error(`${name} n’est pas un clavier connu`)
}
