# Usage de Mathlive et Compute-engine ([de cortexjs.io](https://cortexjs.io))

[Une carte mentale pour visualiser les relations entre ces deux libraires javascript]!(Mathlive_compute-engine.jpg)

## Mathlive *(Pour saisir et afficher des expressions mathématiques)*

Mathlive est développé autour d’un élément HTML :  
le MathfieldElement (tag <math-field> est interfacé via un objet javascript de type MathFieldElement qui lui procure
différentes propriétés et méthodes pour le manipuler.  
Pour plus de concision, nous nommerons 'mathfield' ou son abréviation 'mf' les instances de cet objet.  
Les personnes qui sont habituées à MathQuill verront sans doute la similitude avec les MQ.MathField() ou
MQ.StaticMath().  
Un mathfield peut contenir du latex 'statique' et des inputs dynamiques (les placeHolders).  
Le code MatQuill ci-dessous :

```
<span id="fill-in-the-blank">\sqrt{ \MathQuillMathField{x}^2 + \MathQuillMathField{y}^2 }</span>
<script>
  var fillInTheBlank = MQ.StaticMath(document.getElementById('fill-in-the-blank'));
  fillInTheBlank.innerFields[0].latex() // => 'x'
  fillInTheBlank.innerFields[1].latex() // => 'y'
</script>
```

peut être remplcé par :

```
<math-field readonly>
\sqrt{ \placeholder[x]{}^2 + \placeholder[x]{}^2 }
</mathfield>
```

ou encore :

```
const fillInTheBlank = new MathfieldElement()
fillInTheBlank.value = '\sqrt{ \placeholder[x]{}^2 + \placeholder[x]{}^2 }'
fillInTheBlank.getPromptValue('x') // => 'x'
fillInTheBlank.getPromptValue('x') // => 'y'
```

Par défaut, les mathfields disposent d’un clavier mathématique virtuel associé.  
Il est entièrement configurable (des modèles prédéfinis sont fournis par la librairies, auxquels j'ai ajouté les
claviers customisés pour Mathalea, et on peut toujours choisir de fabriquer son propre clavier).

Mathlive fournit une fonction de rendu Latex dans un élément ou dans le document : renderMathInElement()
/renderMathInDocument().  
Ces fonctions permettent d’écrire des formules mathématiques en ligne ou en mode display simplement en écrivant du latex
dans l’élément ou la page encadré par des délimiteurs standards ($ ou $$).  
Lors de l’exécution de la fonction renderMathInDocument(), ces délimiteurs sont recherchés et leur contenu est
automatiquement remplacé par des mathfield appropriés.

Mathlive permet d’activer la fonctionnalité text-to-speech pour les malvoyants par exemple. On pourra
utiliser : `mf.executeCommand('speak')`

Mathlive permet de définir des macros qui sont des commandes latex : par
exemple `mf.macros = {...mf.macros,{average: '\\operatorname{average}'}` permet d’ajouter la commande `'average'` qui
sera
instantanément remplacée une fois saisie par `'\operatorname{average}'` dans le mathfield.

Le mathfield est entièrement pilotable via une API dédiée.

Mathlive utilise le typage Typescript, donc il est aisé de l’intégrer à des projets typescript. Les fichiers de
définitions permettent l’auto-complétion lors du codage.

## Compute-engine *(Pour analyser et évaluer les expressions mathématiques)*

compute-engine désigne la librairie javascript (littéralement 'moteur de calcul').  
ComputeEngine désigne l’objet servant d’interface à la librairie.  
Dans la suite du document, 'ce' désignera une instance du ComputeEngine.

Sa première tâche consiste à lire et comprendre du Latex et à le transformer dans un format propre (BoxedExpression) qui
est une enveloppe pour le format [MathJSON](https://cortexjs.io/math-json/).  
Mais il ne s'arrête pas là. Il fournit des outils pour manipuler les objets MathJSON ainsi obtenus.

### Le parser : `ce.parse('latexString')`

Son travail consiste à analyser l’argument passé et à retourner l’expression correspondante. Un exemple tiré du site :

```
const mf = document.getElementById("input");
mf.value = "\\frac{10}{5}";
const expr = mf.expression; // équivalent à ce.parse(mf.value)
console.log(expr.evaluate().latex);
// ➔ 2
```

où l’on voit que les mathfields de Mathlive sont intimement liés à compute-engine. En effet, `mf.value` fournit le latex
dont se nourrit compute-engine. `ce.parse(mf.value)` fournit le BoxedNumber '2' car ici, le parseur tourne avec
l’option 'canonical' à true par défaut. Si l’on voulait la BoxedExpression '["Divide", "10", "5"]', alors, il aurait
fallu mettre `ce.parse(mf.value,{canonical: false})`.  
`mf.expression` est le raccourcis qui permet de lier MathfieldElement et ComputeEngine.

### Les méthodes des BoxedExpressions

### Les méthodes du ComputeEngine
