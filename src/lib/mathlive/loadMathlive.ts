import { MathfieldElement, NormalizedVirtualKeyboardLayer, VirtualKeyboardInterface, VirtualKeyboardKeycap, VirtualKeyboardLayout, VirtualKeyboardLayoutCore, VirtualKeyboardName } from 'mathlive'
import { isCustomKeyboardName, isKeyboardName, loadKeyboard, type MAthliveKeyboardName, type Shortcuts } from 'src/lib/mathlive/loadKeyboard'

/*
 ATTENTION, pour que mathlive retrouve ses polices, il faut lui indiquer le dossier.
 On les a copiées dans docroot/static/mathlive/fonts, et il faut le déclarer avant d’utiliser mathlive.
 C’est src/lib/player/index.js qui s’en charge
 */

type ExtendedVirtualKeyboard = VirtualKeyboardInterface & {
  readonly normalizedLayouts: (VirtualKeyboardLayoutCore & {
    layers: NormalizedVirtualKeyboardLayer[];
  })[];
}

export type LayoutsList = (MAthliveKeyboardName | VirtualKeyboardLayout)[]
export type LayoutsValue = LayoutsList | VirtualKeyboardLayout

const mathVirtualKeyboard: unknown = window.mathVirtualKeyboard
/**
 * une fonction pour retirer une touche sur un ensemble de layout
 * @param {string} key la touche à retirer
 * @param {Array<string|Object>}layouts
 */
const dropKey = function (key: string, layouts: (VirtualKeyboardLayoutCore & {
  layers: NormalizedVirtualKeyboardLayer[]
})[]) {
  // layouts est normalisé ici, c’est un Array
  if (Array.isArray(layouts)) { // on est dans le cas 2 ou 3
    // ici les for of ne fonctionnent pas, car on modifie l’Array...
    for (let indexOnglet = 0; indexOnglet < layouts.length; indexOnglet++) {
      if (typeof layouts[indexOnglet] !== 'string') {
        // layouts peut être constitué de plusieurs "layout" avec sa propriété rows contenant les touches par
        // ligne mais layouts peur aussi n’avoir qu’un seul "layout", lequel contient une propriété layers qui
        // contient plusieurs "layer" avec leur propriété rows Je n’ai pas encore compris la nuance qu’il y a
        // entre un modèle et l’autre Il y a des chances que pour simplifier, on n’utilise qu’un seul des deux
        // modèles mais en attendant le test ci-dessous est censé uniformiser la recherche des rows...
        if (layouts && Array.isArray(layouts)) {
          const layout = layouts[indexOnglet]?.layers != null ? layouts[indexOnglet]?.layers : layouts[indexOnglet]
          if (layout != null && Array.isArray(layout)) {
            for (let index = 0; index < layout.length; index++) {
              const rows = layout[index]?.rows
              if (rows != null) {
                for (let indexRow = 0; indexRow < rows.length; indexRow++) {
                  const newRow: Partial<VirtualKeyboardKeycap>[] = []
                  const raw = rows[indexRow]
                  if (raw != null && Array.isArray(raw)) {
                    for (const theKey of raw) {
                      if (theKey.latex) {
                        if (!theKey.latex.includes(key)) {
                          newRow.push(theKey)
                        }
                      }
                    }
                  }
                  rows[indexRow] = newRow
                }
              }
            }
          }
        }
      } else { // il s’agit d’un virtualKeyboardName, donc on doit récupérer la définition des layouts avant de la bricoler
        for (let index = 0; index < layouts.length; index++) {
          const layout = layouts[index]
          if (layout != null) {
            const layers = layout.layers
            if (layers && layers.length > 0) {
              for (let indexLayer = 0; indexLayer < layers.length; indexLayer++) {
                const layer = layers[indexLayer]
                if (layer != null) {
                  const rows = layer.rows
                  if (rows && rows.length > 0) {
                    for (let indexRow = 0; indexRow < rows.length; indexRow++) {
                      const newRow: Partial<VirtualKeyboardKeycap>[] = []
                      const row = rows[indexRow]
                      if (row != null) {
                        for (let indexKeyCap = 0; indexKeyCap < row.length; indexKeyCap++) {
                          const keycap = row[indexKeyCap]
                          if (keycap != null) {
                            // c’est normalisé, il n’y a plus de string, mais des
                            // VirtualKeyboardKeycap
                            const theKey = keycap.key // ce qui est inséré si ni insert, ni
                            // latex ne sont définis
                            const insert = keycap.insert // le latex inséré quand on clique
                            // sur la touche
                            const latex = keycap.latex // c’est le latex affiché sur la
                            // touche, pas le latex inséré
                            if (insert) {
                              if (!insert.includes(key)) {
                                newRow.push(keycap)
                              }
                            } else if (latex) {
                              if (!latex.includes(key)) {
                                newRow.push(keycap)
                              }
                            } else if (theKey) {
                              if (!theKey.includes(key)) {
                                newRow.push(keycap)
                              }
                            } else { // la touche à supprimer n’est ni dans insert, ni dans latex, ni dans key, on la conserve.
                              newRow.push(keycap)
                            }
                          }
                        }
                        rows[indexRow] = newRow
                      }
                    }
                  }
                }
              }
            }
            layouts[index] = layout
          }
        }
      }
    }
  }
}

/**
 * une fonction pour retirer les touches qu’on ne veut pas (propriété unusedKeys)
 * unusedKeys est un array de strings. Un minimum de caractère discriminant peut être utilisé. Par exemple 'fr'
 * retirera aussi bien '\\dfrac' que '\\frac' du clavier, mais risque aussi de retirer une touche si elle contient 'fr'
 * On retire la touche sur le clavier chargé dans mathVirtualKeyboard.
 * @param {string[]} keys
 */
const dropKeys = function (keys: string[]) {
  const layoutsNormalized = (mathVirtualKeyboard as ExtendedVirtualKeyboard).normalizedLayouts
  for (const key of keys) {
    dropKey(key, layoutsNormalized)
  }
}

/**
 * Charge MathLive et personnalise les réglages
 * MathLive est chargé dès qu’un tag math-field est créé
 */
export async function loadMathLive (mf: MathfieldElement, layouts: LayoutsValue, unusedKeys?: string[], shortcuts?: Shortcuts) {
  const claviers: (VirtualKeyboardName | VirtualKeyboardLayout)[] = []
  const raccourcis = shortcuts ?? {}
  const promisedKeyboards = []
  if (Array.isArray(layouts)) {
    for (const layout of layouts) {
      if (typeof layout === 'string') {
        if (isCustomKeyboardName(layout)) {
          promisedKeyboards.push(loadKeyboard(layout))
        } else if (isKeyboardName(layout)) {
          // pas un clavier personnalisé, mais un clavier Mathlive dans les VirtualKeyboardNames
          claviers.push(layout)
        } else {
          throw Error(`clavier ${layout} inconnu`)
        }
      } else {
        // un clavier au format VirtualKeyboardLayout
        claviers.push(layout)
      }
    }
  } else {
    // layouts est un VirtualKeyboardLayout
    claviers.push(layouts)
  }
  // Ce raccourcis me semble nécessaire : il remplace le point par la virgule dans la saisie en mode math.
  // Je l’ajoute donc systématiquement.
  // ici la virgule placée sera sans extra gap... l’expression parsée par le compute-engine sera bien un nombre. Mais
  // mf.value sera de la forme '3{,}14'... donc à ne pas passer à Number() comme ceci... utiliser
  // mf.expression.evaluate() qui retourne la valeur exacte ou mf.expression.N() qui retourne une approximation
  // numérique.
  Object.assign(raccourcis, { '.': { mode: 'math', value: '{,}' } })

  // On paramètre le comportement du clavier
  mf.mathVirtualKeyboardPolicy = 'manual'
  mf.menuItems = []
  mf.addEventListener('focusin', () => {
    window.mathVirtualKeyboard.layouts = claviers
    mf.inlineShortcuts = raccourcis
    if (unusedKeys != null) {
      dropKeys(unusedKeys)
    }
    window.mathVirtualKeyboard.show()
  })
  mf.addEventListener('focusout', () => window.mathVirtualKeyboard.hide())
}
