import $ from 'jquery'
import ButtonMq, { dispatchFakeKeyup } from 'src/lib/widgets/mqVirtualKeyboard/ButtonMq'

export const doBackSpaceAction = (inputMq) => {
  $(inputMq).mathquill('keystroke', 'Backspace')
  dispatchFakeKeyup(inputMq)
}

class ButtonBackSpace extends ButtonMq {
  constructor (inputMq, options = {}) {
    options.value = '↤' // essais avec 🔙 ⟻
    options.fontSize = '1.6rem' // via la css ça veut pas, on cherche pas trop à comprendre et on l’impose ici
    options.onClick = () => doBackSpaceAction(inputMq)
    super(inputMq, options)
  }
}

export default ButtonBackSpace
