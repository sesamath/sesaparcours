import ButtonMq, { dispatchFakeKeyup } from 'src/lib/widgets/mqVirtualKeyboard/ButtonMq'
import { mqCommandes } from 'src/lib/mathquill/functions'
import { j3pAddElt } from 'src/legacy/core/functions'
import { showMqCursor } from 'src/lib/widgets/mqVirtualKeyboard/MqVirtualKeyboard'

const mqFunctions = ['sin', 'cos', 'tan', 'ln', 'log']

const doCommandAction = (inputMq, command) => {
  mqCommandes[command](inputMq, true) // Deuxième pramètre à true car il faut donner le focus à l’éditeur
  // Sinon par exemple ln(abs(x)) ne fonctionne pas bien avec le clavier virtuel
  showMqCursor(inputMq, true)
  dispatchFakeKeyup(inputMq)
}

class ButtonMqCommand extends ButtonMq {
  constructor (inputMq, options = {}) {
    const { command } = options
    if (!command) throw Error('option commande manquante')
    if (!mqCommandes[command]) throw Error(`Commande mathquill ${command} inconnue`)
    options.onClick = () => {
      doCommandAction(inputMq, command)
    }
    options.className = 'mqButton' // spécifique à mqVirtualKeyboard
    const isMqFunction = mqFunctions.includes(command)
    if (isMqFunction) options.value = command // faut écrire la fct dans le bouton car elle n’a pas d’image de background associée
    super(inputMq, options)
    // on ajoute un span dans notre <button>, c’est lui qui a la classe MQ… et que l’on réduit pour que notre bouton garde la même tête que les autres
    j3pAddElt(this.element, 'span', '', {
      className: `mqBtn mqBtn${command}` // définis dans les css génériques de j3p et utilisé par j3pPaletteMathquill
    })
    if (isMqFunction) this.element.classList.add('mqFunction')
  }
}

export default ButtonMqCommand
