import $ from 'jquery'
import ButtonMq from 'src/lib/widgets/mqVirtualKeyboard/ButtonMq'

const capitalize = mot => mot[0].toUpperCase() + mot.substr(1).toLowerCase()

const arrows = {
  Left: '←',
  Right: '→',
  Up: '↑',
  Down: '↓'
}

class ButtonArrow extends ButtonMq {
  /**
   * Crée un bouton "flèche" pour déplacer le curseur dans l’input
   * @param {HTMLElement} inputMq
   * @param {string} arrow
   * @param {ButtonOptions} options
   */
  constructor (inputMq, arrow, options = {}) {
    if (typeof arrow !== 'string') throw Error('argument arrow invalide')
    arrow = capitalize(arrow)
    if (!arrows[arrow]) throw Error(`argument arrow inconnu (${arrow})`)
    options.value = arrows[arrow]
    options.fontSize = '18px' // via la css ça veut pas, on cherche pas trop à comprendre et on l’impose ici
    options.onClick = () => {
      $(inputMq).mathquill('keystroke', arrow)
    }
    super(inputMq, options)
  }
}

export default ButtonArrow
