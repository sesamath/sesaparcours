import ButtonMq, { dispatchFakeKeyup } from 'src/lib/widgets/mqVirtualKeyboard/ButtonMq'

export const doEnterAction = (inputMq) => {
  // ferme le clavier
  if (inputMq.virtualKeyboard) inputMq.virtualKeyboard.hide()
  else return console.error(Error('Aucun virtualKeybord associé à cet input mathquill'), inputMq)
  // les sections écoutent keyup et testent parfois charCode ou keyCode
  dispatchFakeKeyup(inputMq, {
    key: 'Enter',
    charCode: 13,
    keyCode: 13
  })
}

class ButtonEnter extends ButtonMq {
  constructor (inputMq, options = {}) {
    options.value = '↲' // essais avec ↩ ⤶ ↲ ↵ ✅ ✓ ✔
    options.fontSize = '1.6rem' // via la css ça veut pas, on cherche pas trop à comprendre et on l’impose ici
    options.onClick = () => doEnterAction(inputMq)
    super(inputMq, options)
  }
}

export default ButtonEnter
