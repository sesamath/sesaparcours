import VirtualKeyboard from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import ButtonChar from './ButtonChar'

class ButtonEnter extends ButtonChar {
  constructor (editor, options = {}) {
    const text = editor.tagName.toLowerCase() === 'input'
    options.value = '↲' // essais avec ↩ ⤶ ↲ ↵ ✅ ✓ ✔
    // géré dans le css... ça veut bien (J-C le 25/03/2024)
    // options.fontSize = '100%' // via la css ça veut pas, on cherche pas trop à comprendre et on l’impose ici
    if (text) {
      options.onClick = () => {
        const input = VirtualKeyboard.currentEditor
        if (input === VirtualKeyboard.currentEditor) {
          input.dispatchEvent(new KeyboardEvent('keyup', { key: 'Enter', keyCode: 13, charCode: 13, bubbles: true }))
        }
      }
    } else {
      options.onClick = () => {
        const mf = VirtualKeyboard.currentEditor
        if (mf === this.editor) {
          // mf.executeCommand(['commit'])
          mf.dispatchEvent(new KeyboardEvent('keyup', { key: 'Enter', keyCode: 13, charCode: 13, bubbles: true }))
        }
      }
    }
    super(editor, options)
  }
}

export default ButtonEnter
