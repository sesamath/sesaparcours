import { MathfieldElement } from 'mathlive'
import { createToggleButton } from 'src/lib/outils/mathlive/utils'
import type { InputWithKeyboard, KeyboardDivContainer, KeyboardMLContainer, MathfieldWithKeyboard } from 'src/lib/types'
import { addElement, empty, isHtmlElement } from 'src/lib/utils/dom/main'
import ButtonArrow from './ButtonArrow'
import ButtonBackSpace from './ButtonBackSpace'
import ButtonChar from './ButtonChar'
import ButtonCommand from './ButtonCommand'
import ButtonEnter from './ButtonEnter'

import './mlVirtualKeyboard.scss'

export interface MfVirtualKeyboardOptions {
  charset?: string // liste des touches ?
  charsetText?: boolean
  listeBoutons?: string[] // Les commandes à ajouter
  transposeMat?: boolean // mettre à true si on peut que les matrices soient transposées
  // avant le remplacement des \editable{} pas des placeholders pour compatibilité avec les anciennes sections
  // puis retransposées après pour être compatible avec l’ancienne version MathQuill (inversions des lignes et des colonnes)
  replacefracdfrac?: boolean // Si présent et true, on remplace les codes \frac par des codes \dfrac
  cleanHtml?: boolean // Pour nettoyer quoi ?
  commandes?: string[]
  acceptSpecialChars?: boolean
}

/*
@todo évolutions de VirtualKeyboard
En 2024-06 VirtualKeyboard sert d’espace de nommage pour gérer un singleton. Quand on change d’input on garde le même clavier qui change de place (et c’est ce qu’on veut), mais il faudra transformer ça en multiton.
Une instance de clavier par exo dans le dom pourrait régler le pb (avoir plusieurs exos sur la même page), mais ça interdit toujours d’avoir des claviers différents dans le même exo.
Il faudrait plutôt ajouter un id à l’instance, par défaut on en a qu’un et c’est celui-là qu’on prend, et si on en veut plusieurs on doit préciser l’id voulu.
 */

export function hideCurrentVirtualKeyboard () {
  if (VirtualKeyboard.currentEditor) {
    try {
      VirtualKeyboard.currentEditor.virtualKeyboard.hide()
    } catch (error) {
      console.error(error)
    }
  }
}

export function setKeyboardContainer (container: HTMLElement): void {
  VirtualKeyboard.setContainer(container)
}

/**
 * Retourne la position relative du touch ou du clic par rapport à sa cible (position dans event.target)
 * @param {TouchEvent|MouseEvent} event
 * @return {number[]} [x, y]
 * @private
 */
const getPosition = (event: TouchEvent | MouseEvent): [number, number] => {
  const vkContainer = VirtualKeyboard.vkContainer
  const rect = vkContainer.getBoundingClientRect()
  if (event instanceof MouseEvent) {
    return [event.clientX - rect.x, event.clientY - rect.y]
  } else if (event instanceof TouchEvent) {
    const touch = event.targetTouches.item(0)
    if (touch != null) return [touch.clientX - rect.x, touch.clientY - rect.y]
  } else {
    throw Error(`getPosition appelé avec un event non géré : ${JSON.stringify(event)}`)
  }
  throw Error(`getPosition a rencontré un problème pour déterminer la position de l’event : ${JSON.stringify(event)}`)
}

/**
 * Décale le vkContainer à l’intérieur du vkParentContainer
 * @param decalX Le décalage en abscisse
 * @param decalY Le décalage en ordonnée
 */
const translatePosition = (decalX: number, decalY: number) => {
  const vkContainer = VirtualKeyboard.vkContainer
  if (!vkContainer) return // ça peut arriver si le clavier vient d’etre détruit
  const { left, top } = getComputedStyle(vkContainer)
  vkContainer.style.left = (parseInt(left, 10) + decalX) + 'px'
  vkContainer.style.top = (parseInt(top, 10) + decalY) + 'px'
}

/**
 * fonction appelée quand on commence à faire glisser le clavier virtuel
 * @param {UIEvent} event
 */
const onDragStart = (event: TouchEvent | MouseEvent) => {
  const vkContainer = VirtualKeyboard.vkContainer
  // sur iPad faut un preventDefault sinon toute la page bouge, sauf sur les <button> sinon ça déclenche plus le click
  // => on ignore le drag sur un bouton et on fera un preventDefault dans les autres cas
  if (event.target instanceof HTMLDivElement) { // on fait rien sur les <button> ou les <span> qu’ils contiennent
    if (vkContainer.isDragging) return
    vkContainer.isDragging = true
    const [x, y] = getPosition(event)
    vkContainer._dragStartX = x
    vkContainer._dragStartY = y
    vkContainer.addEventListener('mousemove', onDragMove)
    vkContainer.addEventListener('touchmove', onDragMove)
    // pour le mousemove faut le mettre sur body, sinon la souris sort trop facilement du div dès qu’on bouge un peu trop vite
    document.body.addEventListener('mousemove', onDragMove)
    document.body.addEventListener('touchmove', onDragMove)
    if (event.cancelable) event.preventDefault()
  }
}

/**
 * Fonction appelée quand on déplace le clavier virtuel après avoir cliqué pour commencer à le capturer
 * @param {UIEvent} event
 */
const onDragMove = (event: TouchEvent | MouseEvent) => {
  const vkContainer = VirtualKeyboard.vkContainer
  if (vkContainer.isDragging) {
    const [x, y] = getPosition(event)
    const decalX = x - vkContainer._dragStartX
    const decalY = y - vkContainer._dragStartY
    translatePosition(decalX, decalY)
    vkContainer.hasBeenMoved = true
  }
}

/**
 * Fonction appelée quand on relâche le clavier virtuel après l’avoir fait glisser
 */
const onDragEnd = () => {
  const vkContainer = VirtualKeyboard.vkContainer
  if (!vkContainer.isDragging) return
  vkContainer.isDragging = false
  vkContainer.removeEventListener('touchmove', onDragMove)
  vkContainer.removeEventListener('mousemove', onDragMove)
  document.body.removeEventListener('mousemove', onDragMove)
  document.body.removeEventListener('mousemove', onDragMove)
}

/**
 * Fonction réinitialisant la position du clavier virtuel par rapport à l’éditeur auquel
 * il est associé même si l’éditeur a déjà été déplacé
 */
export function resetKeyboardPosition () {
  const dikbd = VirtualKeyboard.vkContainer
  if (!dikbd) return
  dikbd.hasBeenMoved = false
  VirtualKeyboard.setPosition()
}

/**
 * Fonction renvoyant true seulement si on est sur un appareil avec éctan tactile
 * @returns {boolean}
 */
function isTouchDevice (): boolean {
  // return 'ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0
  return ((window.PointerEvent && ('maxTouchPoints' in navigator) && (navigator.maxTouchPoints > 0)) ||
    (window.PointerEvent && ('msMaxTouchPoints' in navigator) && (Number(navigator.msMaxTouchPoints) > 0)) ||
    (window.matchMedia && window.matchMedia('(any-pointer:coarse)').matches) ||
    (window.TouchEvent && ('ontouchstart' in window)))
}

/**
 * Fonction ajoutant le listeners nécessaires à l’éditeur Mathlive mf de façon à faire apparaître
 * le clavier au focus et le faire disparaître quand il perd le focus
 * @param editor
 */
export function addEventListenersToEditor (editor: InputWithKeyboard | MathfieldWithKeyboard) {
  const tagName = editor.tagName.toLowerCase()
  const text = tagName === 'input'
  if (tagName !== 'math-field' && !text) return
  // la ligne suivante doit être présente sinon le clavier virtuel mathlive peut apparaître
  editor.mathVirtualKeyboardPolicy = 'manual'
  editor.addEventListener('focus', () => {
    if (editor.disabled) return
    // On mémorise l’éditeur MathLive auquel est associé le clavier virtuel
    VirtualKeyboard.currentEditor = editor
    if (isTouchDevice()) {
      editor.virtualKeyboard.show()
    }
    if (editor.virtualKeyboard.isOpen) editor.triggerBtn.innerHTML = '⇓'
    VirtualKeyboard.setPosition()
  })
  editor.addEventListener('focusin', () => {
    if (editor.disabled) return
    // On mémorise l’éditeur MathLive auquel est associé le clavier virtuel
    VirtualKeyboard.currentEditor = editor
    if (isTouchDevice()) {
      editor.virtualKeyboard.show()
    }
    if (editor.virtualKeyboard.isOpen) editor.triggerBtn.innerHTML = '⇓'
    VirtualKeyboard.setPosition()
  })
  editor.addEventListener('focusout', (event) => {
    if (event instanceof FocusEvent) {
      if (event.relatedTarget != null) {
        const elt = event.relatedTarget as HTMLElement
        // Si le focusOut de l’éditeur se fait parce que l’on clique sur le triggerBtn d’un éditeur, il n’y a rien à faire : c’est l’event sur le bouton qui s’occupe de tout
        if (elt.classList.contains('ML__virtual-keyboard-toggle')) {
          return
        }
      }
      // On a cliqué sur un élément qui n’a rien à voir avec un bouton de clavier ou dans le vide
      VirtualKeyboard.currentEditor = null
      editor.virtualKeyboard.hide()
      VirtualKeyboard.setPosition()
    }
  })

  if (text) {
    editor.addEventListener('keyup', () => {
      setTimeout(function () {
        VirtualKeyboard.setPosition()
      })
    })
  } else { // Editeur Mathlive ?
    editor.classList.add('mathfieldWithKeyboard')
    editor.addEventListener('input', () => {
      setTimeout(function () {
        VirtualKeyboard.setPosition()
      })
    })
  }
}

// @fixme ce commentaire date à ce qu’il semble d’une autre époque. Et le tutoriel cité n’a plus de raison d'être.
/**
 * À priori pas besoin d’instancier directement cette classe, l’appel de {@link module:mqFunctions.mqRestriction mqRestriction} devrait suffire
 * Cf également le tuto {@tutorial MqVirtualKeyboard} pour migrer de {@link module:j3pFunctions.j3pRestriction} à {@link module:lib/mathquill/functions.mqRestriction mqRestriction}
 */
class VirtualKeyboard {
  static vkParentContainer: KeyboardDivContainer // div.mainContent en v2 et div#MepMG en v1
  static vkContainer: KeyboardMLContainer // Le conteneur du clavier
  static currentEditor: InputWithKeyboard | MathfieldWithKeyboard | null
  static mouseupML?: () => void
  editor: InputWithKeyboard | MathfieldWithKeyboard
  isActive: boolean
  commandsContainer?: HTMLDivElement
  element: HTMLDivElement // l’élément qui contient le clavier de cette instance (à ne pas confondre avec VirtualKeyboard.vkContainer qui est son container
  isOpen: boolean
  restriction: RegExp

  /**
   * Instancie un clavier virtuel et ses éléments et les lie à un éditeur : ajout des listeners et du triggerBtn.
   * @param {HTMLElement} editor L’éditeur Mathlive associé ou l’éditeur input classique associé
   * @param {RegExp} restriction
   * @param {MfVirtualKeyboardOptions} [options]
   */
  constructor (editor: InputWithKeyboard | MathfieldWithKeyboard, restriction: RegExp, options: MfVirtualKeyboardOptions) {
    // On commence par créer le triggerBtn
    const divBtn = createToggleButton(editor)
    if (editor instanceof MathfieldElement) {
      if (editor.shadowRoot != null) {
        const menuToggleButton = editor.shadowRoot.querySelector('.ML__menu-toggle')
        if (menuToggleButton != null && menuToggleButton.parentNode != null) {
          menuToggleButton.parentNode.removeChild(menuToggleButton)
        }

        const toggleButton = editor.shadowRoot.querySelector('.ML__virtual-keyboard-toggle')
        if (toggleButton != null && toggleButton.parentNode != null) toggleButton.parentNode.removeChild(toggleButton)
      }
    }
    // On place le toggleBtn en dehors de l’éditeur pour harmoniser entre les inputs et les mathfield
    editor.after(divBtn)

    // Ensuite, on vérifie si c’est la première instance de VirtualKeyboard. Si oui, alors il faut initialiser le container de tous les claviers (VirtualKeyboard.vkParentContainer) ainsi que le VirtualKeyboard.vkContainer.
    if (isHtmlElement(VirtualKeyboard.vkParentContainer)) {
      VirtualKeyboard.setContainer(VirtualKeyboard.vkParentContainer)
    } else {
      VirtualKeyboard.initContainer(this)
    }
    // Si c’est la première instance, on initialise VirtualKeyboard.vkContainer avec ses listeneners
    if (VirtualKeyboard.vkContainer == null) {
      VirtualKeyboard.vkContainer = document.createElement('div') as KeyboardMLContainer
      VirtualKeyboard.vkContainer.style.display = 'none'
      VirtualKeyboard.vkContainer.style.position = 'absolute'
      VirtualKeyboard.vkContainer.hasBeenMoved = false
      // pour drag&drop au touch ou à la souris
      if (!VirtualKeyboard.vkContainer.mouseDownML) {
        VirtualKeyboard.vkContainer.mouseDownML = onDragStart
        VirtualKeyboard.vkContainer.addEventListener('mousedown', onDragStart)
      }
      // faut préciser passive: false pour indiquer au navigateur qu’on va utilise preventDefault (si on le dit pas ça marche quand même mais chrome met un warning en console)
      if (!VirtualKeyboard.vkContainer.touchStartML) {
        VirtualKeyboard.vkContainer.touchStartML = onDragStart
        VirtualKeyboard.vkContainer.addEventListener('touchstart', onDragStart, { passive: false })
      }
      const body = document.body
      if (!VirtualKeyboard.mouseupML) {
        VirtualKeyboard.mouseupML = onDragEnd
        body.addEventListener('mouseup', VirtualKeyboard.mouseupML)
      }
      if (!VirtualKeyboard.vkContainer.touchEndML) {
        VirtualKeyboard.vkContainer.touchEndML = onDragEnd
        VirtualKeyboard.vkContainer.addEventListener('touchend', onDragEnd)
      }
    }

    // On crée le div qui contient les touches, c’est la propriété this.element de l’instance. Elle viendra prendre la place de VirtualKeyboard.vkContainer lorsque cette instance aura le focus
    this.element = document.createElement('div')
    this.element.className = 'virtualKeyboard'
    this.element.style.display = 'none'

    const text = editor.tagName.toLowerCase() === 'input'
    this.editor = editor
    editor.virtualKeyboard = this
    // Parce qu’il en faut un, si c’est le premier, alors ce sera le currentEditor
    // if (VirtualKeyboard.currentEditor == null) VirtualKeyboard.currentEditor = this.editor
    // On ajoute les listeners sur l’éditeur
    addEventListenersToEditor(this.editor)
    if (!restriction) {
      // on autorise tout, ce sera limité plus loin par ce qu’on gère dans un clavier virtuel
      restriction = /./
    }
    if (!(restriction instanceof RegExp)) throw Error('restriction invalide')
    // si on nous passe une restriction qui vérifie depuis le début de la chaîne, on vire cette condition
    // (sinon on aurait que le premier caractère autorisé comme bouton, ou aucun)
    const regSrc = restriction.source
    let flags = restriction.flags
    if (restriction.global) {
      console.warn('La regexp restriction passée au clavier virtuel ne doit pas être globale (on appelle sa méthode test de nombreuses fois)')
      flags = flags.replace('g', '')
    }
    if (regSrc.startsWith('^')) {
      console.warn('La regexp restriction passée au clavier virtuel ne doit pas démarrer avec ^')
      restriction = new RegExp(regSrc.substring(1), flags)
    } else {
      // on la clone, pour que notre usage ne modifie pas son pointeur interne lastIndex, au cas où qqun s’en servirait
      restriction = new RegExp(regSrc, flags)
    }
    const acceptSpecialChars = options.acceptSpecialChars
    /**
     * Sera mis à false quand on veut le désactiver par exemple dans j3PDesactive (le bouton pour déplier devient alors inactif)
     * @type {boolean}
     */
    this.isActive = true
    /**
     * Le contenant de haut niveau
     * @type {HTMLElement}
     */
    // this.container = container
    /**
     * La liste des touches du clavier virtuel
     * @type {RegExp}
     */
    this.restriction = restriction

    // La chaîne contenant tous les caractères éventuels (qui pourraient être filtrés par la regExp restriction)
    // @todo if (restriction.flag.includes('i')) => retirer les majuscules de cette liste et ajouter un bouton Maj pour les gérer
    // on crée des listes pour passer à la ligne entre chaque liste
    const allChars = [
      '01234,56789.', '+-*/()',
      '=<>²^;[]|',
      'abcdefghijklmnopqrstuvwxyz',
      'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
      'àâçéèêëïôùû'
    ]
    if (acceptSpecialChars) {
      allChars[2] += ' :$°\''
    }
    const allowedChars = allChars
      .map(list => Array.from(list).filter(char => restriction.test(char)))
      .filter(l => l.length)

    this.isOpen = false
    // on s’ajoute aussi à cet elt (c’est lui qu’on récupère avec du querySelectorAll('.virtualKeyboard')

    // ///////////////////////////////
    // ajout du contenu du clavier
    // ///////////////////////////////

    // A gauche un bloc contenant les boutons pour les caractères
    const blockChars = addElement(this.element, 'div')
    allowedChars.forEach((chars, i) => {
      if (i) addElement(blockChars, 'br')
      chars.forEach(char => ButtonChar.addInto(blockChars, editor, { value: char }))
    })
    /**
     * Le bloc des boutons de commande
     * @type {HTMLDivElement}
     */
    if (!text) {
      this.commandsContainer = addElement(blockChars, 'div', { className: 'mlCommandes' })
      if (Array.isArray(options.commandes)) {
        options.commandes.forEach(commande => this.addCommand(commande))
      }
    }
    // un block à droite pour les boutons "meta"
    const blockMeta = addElement(this.element, 'div')
    // qui contient deux block, un pour les flèches
    const blockArrows = addElement(blockMeta, 'div')
    const arrows = text ? ['Left', 'Right'] : ['Left', 'Right', 'Up', 'Down']
    arrows.forEach(arrow => ButtonArrow.addInto(blockArrows, editor, arrow))
    // et un autre pour backspace et enter
    const blockDelEnter = addElement(blockMeta, 'div')
    ButtonBackSpace.addInto(blockDelEnter, editor, {})
    ButtonEnter.addInto(blockDelEnter, editor, {})
    const blockEdit = addElement(blockMeta, 'div')
    if (!text) {
      const edit = ['undo', 'redo']
      edit.forEach(cmd => ButtonCommand.addInto(blockEdit, editor, { command: cmd }))
    }
  } // constructor

  /**
   * Fonction plaçant automatiquement le clavier virtuel par rapport à l’éditeur auquel il est associé
   * Ne fait rien si on a déjà fat glisser manuellement le clavier virtuel (pour un éditeur pas recréé)
   */
  static setPosition () {
    if (!VirtualKeyboard.vkContainer.hasBeenMoved) {
      const editor = VirtualKeyboard.currentEditor
      const dikbd = VirtualKeyboard.vkContainer
      if (!editor || !dikbd || dikbd.hasBeenMoved) return
      const rectEdit = editor.getBoundingClientRect()
      const keyboardcontainer = VirtualKeyboard.vkParentContainer
      const { left, right, top } = keyboardcontainer.getBoundingClientRect()
      const { width: widthkbd } = editor.virtualKeyboard.element.getBoundingClientRect()
      let newleft // Nouvelle position gauche du clavier virtuel
      let newtop
      if (rectEdit.right + widthkbd > right) {
        newleft = right - widthkbd - 10
        newtop = rectEdit.bottom + 5
      } else {
        newleft = rectEdit.right + 70
        newtop = rectEdit.top - 80
        if (newtop < top) newtop = top
      }
      dikbd.style.left = String(Math.round(newleft - left)) + 'px'
      dikbd.style.top = String(Math.round(newtop - top + Number(keyboardcontainer?.scrollTop))) + 'px'
    }
  }

  private static initContainer (virtualKeyboard: VirtualKeyboard): void {
    let divWork = document.querySelector('div.divZone') as HTMLDivElement// v1, MG est le premier divZone du DOM
    if (divWork == null) {
      // on doit être en v2, mais il pourrait y avoir plusieurs playground dans le dom…
      const allMainDivs = document.querySelectorAll('div.mainContent')
      if (allMainDivs?.length) {
        if (allMainDivs.length > 1) {
          console.warn(Error('Il y a plusieurs .mainContent dans le DOM, il faut préciser celui que l’on veut avec setContainer'))
        } else {
          const divMainContent = allMainDivs[0]
          if (divMainContent != null) divWork = divMainContent.querySelector('div.zoneWork') as HTMLDivElement
          else throw Error('Il n’y a mas de zone de travail dans le div.mainContent')
        }
      }
    }
    if (divWork == null) {
      throw Error('Pas trouvé de .divZone ni de .mainContent')
    }
    VirtualKeyboard.setContainer(divWork)
    // et on lui colle notre instance
    VirtualKeyboard.vkParentContainer.virtualKeyboard = virtualKeyboard
  }

  static setContainer (container: HTMLElement): void {
    // on regarde si on a déjà un clavier
    const actualVk = VirtualKeyboard.vkParentContainer?.virtualKeyboard
    VirtualKeyboard.vkParentContainer = container as KeyboardDivContainer
    if (actualVk) VirtualKeyboard.vkParentContainer.virtualKeyboard = actualVk
  }

  /**
   * Ajoute un bouton de commande mathquill (cf commands dans src/lib/mathquill/functions.js)
   * @param {ButtonOptions} command
   */
  addCommand (command: string) {
    if (this.commandsContainer == null) {
      return // c’est moins violent que le throw Error... ça va faire tiquer celui qui appelle ça sur un input classique
      // throw Error('On ne peut pas ajouter de commande sur un input classique, uniquement sur un math-field')
    }
    ButtonCommand.addInto(this.commandsContainer, this.editor, { command })
  }

  /**
   * Montre le clavier associé à l’éditeur actif
   */
  show () {
    // if (this.isOpen) return
    if (this.editor.disabled) return
    if (VirtualKeyboard.vkContainer.parentNode !== VirtualKeyboard.vkParentContainer) {
      VirtualKeyboard.vkParentContainer.appendChild(VirtualKeyboard.vkContainer)
    }
    VirtualKeyboard.vkContainer.style.display = 'block'
    if (VirtualKeyboard.vkContainer) {
      VirtualKeyboard.currentEditor = this.editor
      empty(VirtualKeyboard.vkContainer)
      // if (contenu) VirtualKeyboard.vkContainer.removeChild(contenu)
      this.element.style.display = 'block'
      this.isOpen = true
      VirtualKeyboard.vkContainer.appendChild(this.element)

      // Il faut désactiver les boutons de TOUS les éditeurs Mathlive
      document.body.querySelectorAll('.mathfieldWithKeyboard, .inputWithKeyboard').forEach((mf) => {
        if (mf !== this.editor) {
          if ('triggerBtn' in mf && mf.triggerBtn instanceof HTMLButtonElement) mf.triggerBtn.innerHTML = '⇑'
        }
      })
      this.editor.triggerBtn.innerHTML = '⇓'
      VirtualKeyboard.setPosition()
    }
  }

  /**
   * Cache le clavier asscocié à l’éditeur actif
   */
  hide () {
    // if (!this.isOpen) return
    const vkContainer = VirtualKeyboard.vkContainer
    if (vkContainer) {
      this.element.style.display = 'none'
      if (vkContainer.contains(this.element)) vkContainer.removeChild(this.element)
      this.isOpen = false
      // Il faut activer les flèches de tous les éditeurs mathfield
      document.body.querySelectorAll('.mathfieldWithKeyboard, .inputWithKeyboard').forEach((mf) => {
        if ('triggerBtn' in mf && mf.triggerBtn instanceof HTMLButtonElement) mf.triggerBtn.innerHTML = '⇑'
      })
    }
  }

  /**
   * Fonction appelée quand on a cliqué sur un bouton ⇑ ou ⇓ d’un clavie virtuel pour le faire apparaître
   * ou disparaître
   * @param {string} char chaîne de un caractère ⇑ ou ⇓
   */
  changeKbdVisibility (char: string) {
    if (char === '⇑') {
      if (!this.isOpen) this.show()
      setTimeout(() => {
        this.editor.focus()
      })
    } else {
      if (char === '⇓' && this.isOpen) this.hide()
    }
  }

  /**
   * Fonction appelée pour rendre le clavier virtuel actif ou inactif suivant la valeur de bActive
   * @param {boolean} bActive
   * /
  setActive (bActive) {
    this.isActive = bActive
    const sel = this.inputMq.parentNode.querySelector('.triggerButton')
    if (bActive) {
      sel.style.display = 'inline-block'
    } else { // Si on désactive le clavier virtuel et qu’il est visible on le masque
      if (this.isOpen) this.hide()
      sel.style.display = 'none'
    }
  } /* */
}

export default VirtualKeyboard
