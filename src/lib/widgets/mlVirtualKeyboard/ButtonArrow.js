import VirtualKeyboard from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import ButtonChar from './ButtonChar'

const capitalize = mot => mot[0].toUpperCase() + mot.substr(1).toLowerCase()

function moveCursor (input, direction) {
  let startPos = input.selectionStart
  if (direction === 'Left' && startPos > 0) startPos--
  if (direction === 'Right' && startPos < input.value.length) startPos++
  input.setSelectionRange(startPos, startPos)
}

const arrows = {
  Left: '←',
  Right: '→',
  Up: '↑',
  Down: '↓'
}

class ButtonArrow extends ButtonChar {
  /**
   * Crée un bouton "flèche" pour déplacer le curseur dans l’input
   * @param {HTMLElement} editor L’éditeur Mathlive associé au clavier virtuel contenant le bouton
   * @param {string} arrow
   * @param {ButtonOptions} options
   */
  constructor (editor, arrow, options = {}) {
    if (typeof arrow !== 'string') throw Error('argument arrow invalide')
    const text = editor.tagName.toLowerCase() === 'input'
    arrow = capitalize(arrow)
    if (!arrows[arrow]) throw Error(`argument arrow inconnu (${arrow})`)
    options.value = arrows[arrow]
    // géré dans le css... ça veut bien (J-C le 25/03/2024)
    // options.fontSize = '18px' // via la css ça veut pas, on cherche pas trop à comprendre et on l’impose ici
    if (text) {
      options.onClick = () => {
        const input = VirtualKeyboard.currentEditor
        if (input === this.editor) {
          /*
          const key = 'Arrow' + arrow
          input.dispatchEvent(new KeyboardEvent('keyup', { key, keycode: 37, bubbles: true }))
           */
          moveCursor(input, arrow)
          if (input.demarqueErreur) input.demarqueErreur() // Si on a affecté à l’éditeur une fonction pour le démarquer comme avec erreur
        }
      }
    } else {
      options.onClick = () => {
        try {
          const mf = VirtualKeyboard.currentEditor
          if (mf === this.editor) {
            if (mf.demarqueErreur) mf.demarqueErreur() // Si on a affecté à l’éditeur une fonction pour le démarquer comme avec erreur
            const cmd = arrow === 'Left' ? 'ToPreviousChar' : (arrow === 'Right' ? 'ToNextChar' : arrow)
            mf.executeCommand('move' + cmd)
          }
        } catch (error) {
          console.error(error)
        }
      }
    }
    super(editor, options)
  }
}

export default ButtonArrow
