import VirtualKeyboard from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import ButtonChar from './ButtonChar'

function deleteBackAtCursor (input) {
  const startPos = input.selectionStart
  const endPos = input.selectionEnd
  // if (startPos === 0 && endPos === 0) return
  if (endPos > startPos) { // Il y a une sélection
    input.value = input.value.substring(0, startPos) + input.value.substring(endPos, input.value.length)
    input.selectionStart = startPos
  } else {
    input.value = input.value.substring(0, startPos - 1) + input.value.substring(endPos, input.value.length)
    input.selectionStart = startPos - 1
  }
  input.selectionEnd = input.selectionStart
}

class ButtonBackSpace extends ButtonChar {
  constructor (editor, options = {}) {
    const text = editor.tagName.toLowerCase() === 'input'
    options.value = '↤' // essais avec 🔙 ⟻
    // géré dans le css... ça veut bien (J-C le 25/03/2024)
    // options.fontSize = '100%' // via la css ça veut pas, on cherche pas trop à comprendre et on l’impose ici
    if (text) {
      options.onClick = () => {
        const input = VirtualKeyboard.currentEditor
        if (input === this.editor) {
          if (input.demarqueErreur) input.demarqueErreur() // Si on a affecté à l’éditeur une fonction pour le démarquer comme avec erreur
          deleteBackAtCursor(input)
        }
      }
    } else {
      options.onClick = () => {
        const mf = VirtualKeyboard.currentEditor
        if (mf === this.editor) {
          if (mf.demarqueErreur) mf.demarqueErreur() // Si on a affecté à l’éditeur une fonction pour le démarquer comme avec erreur
          mf.executeCommand('deleteBackward')
        }
      }
    }
    super(editor, options)
  }
}

export default ButtonBackSpace
