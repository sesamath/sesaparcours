import { j3pAddElt } from 'src/legacy/core/functions'
import { mqCommandes } from 'src/lib/mathquill/functions'
import VirtualKeyboard from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import ButtonChar from './ButtonChar'

const mqFunctions = ['sin', 'cos', 'tan', 'ln', 'log']

class ButtonCommand extends ButtonChar {
  constructor (mathfield, options = {}) {
    const { command } = options
    if (!command) throw Error('option commande manquante')
    const isMqFunction = mqFunctions.includes(command)
    const isEditButton = ['undo', 'redo'].includes(command)
    options.className = isEditButton ? 'mlButtonEdit' : 'mlButton' // spécifique à mqVirtualKeyboard
    if (isEditButton) {
      options.onClick = () => {
        const mf = VirtualKeyboard.currentEditor
        if (mf === this.editor) {
          if (mf.demarqueErreur) mf.demarqueErreur() // Si on a affecté à l’éditeur une fonction pour le démarquer comme avec erreur
          mf.executeCommand(command)
        }
      }
    } else {
      if (!mqCommandes[command]) throw Error(`Commande mathquill ${command} inconnue`)
      options.onClick = () => {
        const mf = VirtualKeyboard.currentEditor
        if (mf === this.editor) {
          if (mf.demarqueErreur) mf.demarqueErreur() // Si on a affecté à l’éditeur une fonction pour le démarquer comme avec erreur
          mqCommandes[command](mf, true)
        }
      }
      if (isMqFunction) options.value = command // faut écrire la fct dans le bouton car elle n’a pas d’image de background associée
    }
    super(mathfield, options)
    // on ajoute un span dans notre <button>, c’est lui qui a la classe MQ… et que l’on réduit pour que notre bouton garde la même tête que les autres
    const className = isEditButton ? ` mlBtn mlBtn${command}` : `mqBtn mqBtn${command}`
    j3pAddElt(this.element, 'span', '', {
      className // définis dans les css génériques de j3p et utilisé par j3pPaletteMathquill
    })
    if (isMqFunction) this.element.classList.add('mlFunction')
  }
}

export default ButtonCommand
