// Gestion des tooltips avec floating-ui
// cf https://floating-ui.com/docs/tutorial

import { computePosition, flip, offset, type Placement } from '@floating-ui/dom'

import { addElement, showError } from 'src/lib/utils/dom/main'

import './tooltip.scss'

export type AddTooltipOptions = {
  tooltip: string
  target: HTMLElement
  className?: string
  parent?: HTMLElement
  placement?: Placement
}
type TooltipOptions = {
  tooltipElt: HTMLElement
  target: HTMLElement
  parent: HTMLElement
  placement: Placement
}

/**
 * Met à jour le placement d’un tooltip
 * @param parent On a besoin de l’offset de la modale pour calculer la position relative du tooltip
 * @param target l’élément auquel le tooltip est lié
 * @param tooltip
 */
function updateTooltip ({ parent, target, tooltipElt, placement }: TooltipOptions): void {
  computePosition(target, tooltipElt, {
    placement,
    middleware: [offset(6), flip()]
  }).then(({ x, y }) => {
    Object.assign(tooltipElt.style, {
      left: `${x - parent.offsetLeft}px`,
      top: `${y - parent.offsetTop}px`
    })
  }).catch(showError)
}

/**
 * Affiche un tooltip
 * @param parent
 * @param target
 * @param tooltipElt
 */
export function showTooltip ({ parent, target, tooltipElt, placement }: TooltipOptions): void {
  tooltipElt.style.display = 'block'
  updateTooltip({ parent, target, tooltipElt, placement })
}

/**
 * Masque un tooltip
 * @param tooltipElt
 */
export function hideTooltip (tooltipElt: HTMLElement): void {
  tooltipElt.style.display = 'none'
}

/**
 * Ajoute un tooltip
 * @param tooltip Le texte à afficher dans le tooltip
 * @param target L'élément dont le survol déclenche l'affichage du tooltip
 * @param [parent=document.body] L'élément parent auquel attacher le tooltip
 * @param [placement=top]
 */
export function addTooltip ({ tooltip, target, parent = document.body, placement = 'top', className = 'tooltip' }: AddTooltipOptions) {
  // faut innerHTML car y'a du param.help avec du html en v1
  // @todo nettoyer tout ça et n'autoriser que du mmd (markdown + latex)
  const tooltipElt = addElement(parent, 'div', { innerHTML: tooltip, className })
  const showListener = showTooltip.bind(null, { parent, target, tooltipElt, placement })
  const hideListener = hideTooltip.bind(null, tooltipElt)
  target.addEventListener('mouseenter', showListener)
  target.addEventListener('mouseleave', hideListener)
  target.addEventListener('focus', showListener)
  target.addEventListener('blur', hideListener)
  return tooltipElt
}
