/** @module lib/widgets/systemeEquations/index */

import './systemeEquations.scss'

import { ResultatAffichage, j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pAddElt, j3pIsHtmlElement } from 'src/legacy/core/functions'

import { addElement } from 'src/lib/utils/dom/main'
import { enforceShape } from 'src/lib/utils/object'

interface EquationOptions {
  styleTexte?: {
    couleur?: string
    taillePolice?: number
  }
  style?: CSSStyleDeclaration
}
interface Equation {
  /** Le contenu au format j3pAffiche (avec les $ & et autres caractères particuliers gérés par j3pAffiche) */
  contenu?: string
  /** Les options à passer à j3pAffiche pour cette équation (clé inputmq1 par ex) */
  options?: EquationOptions
}

/**
 * Affiche un système d’équations
 * @param {HTMLElement} container
 * @param {Equation[]} equations
 * @returns {ResultatAffichage[]} Le tableau des retours de j3pAffiche (chacun contient parent, xxxList, …), un élément par équation fournie
 */
export function afficheSystemeEquations (container: HTMLElement, equations: Equation[]): ResultatAffichage[] {
  if (!j3pIsHtmlElement(container)) throw Error('conteneur invalide')
  if (!Array.isArray(equations)) throw Error('equations invalides')
  // vérif de forme
  equations.forEach((eqn, i) => {
    enforceShape(eqn, { contenu: 'string', options: 'object' }, { undefProps: ['options'], errorPrefix: `Problème dans l’équation d’index ${i}` })
  })
  const divSysteme = addElement(container, 'div', { className: 'systemeEquations' })
  const elts: ResultatAffichage[] = []
  for (const { contenu, options } of equations) {
    const divEquation = j3pAddElt(divSysteme, 'div')
    elts.push(j3pAffiche(divEquation, '', contenu ?? '', options))
  }
  return elts
}
