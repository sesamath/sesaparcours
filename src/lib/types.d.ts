import type { MathfieldElement } from 'mathlive'
import type Parcours from 'src/legacy/core/Parcours'
import type { editorCallback, RessourceLike } from 'src/lib/editor/spEdit'
import type { GraphJson } from 'src/lib/entities/Graph'
import type { ParamsValues } from 'src/lib/entities/GraphNode'
import type { PathwayJson, PathwayValues } from 'src/lib/entities/Pathway'
import type { SectionContext } from 'src/lib/entities/SectionContext'
import type VirtualKeyboard from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'

// on déclare ici tous les modules js qu’on importe, pour éviter

declare module '@sesatheque-plugins/arbre/lib/index'
declare module 'sesatheque-client/src/promise/fetch'

interface BugsnagClient {
  addMetadata: (name: string, prop: string | unknown, data?: unknown) => void
}

declare global {

  interface Window {
    isPlaywright?: boolean
    bugsnagClient?: BugsnagClient
    j3p?: Parcours
  }
}

// ****************************
// * Utilitaires TS pratiques *
// ****************************

/** Liste des valeurs de T */
export type ValueOf<T> = T[keyof T]

/**
 * Une sous partie de T (idem `Partial<T>` mais avec du partial sur toutes les sous-propriétés)
 * merci à https://stackoverflow.com/a/51365037
 */
export type RecursivePartial<T> = {
  [P in keyof T]?: T[P] extends (infer U)[]
    ? RecursivePartial<U>[]
    : T[P] extends object | undefined
      ? RecursivePartial<T[P]>
      : T[P]
}

export type PlainObject = Record<string, unknown>

type voidFunction = () => void
type voidPromised = Promise<void>
/**
 * Un id de setTimeout que l’on peut passer clearTimeout()
 */
export type TimeoutId = ReturnType<typeof setTimeout>

type LegacySectionParamsParametreType =
  'string'
  | 'number'
  | 'entier'
  | 'integer'
  | 'boolean'
  | 'liste'
  | 'array'
  | 'intervalle'
  | 'editor'
  | 'multiEditor'
type LegacyParamValue = string | number | boolean | (string | number)[]
/** Un array avec nom, defaultValue, type, aide?, … */
export type LegacySectionParamsParametre = [string, LegacyParamValue, LegacySectionParamsParametreType, string, (Function | (string | number)[])?]
export type LegacySectionParamsParametres = LegacySectionParamsParametre[]

/** Objet params exporté par une section v1 */
export interface LegacySectionParams {
  outils?: string[]
  parametres?: LegacySectionParamsParametres
  /** Les pe à l’ancien format, tableau d’objets compliqués */
  pe?: PlainObject[]
  /** Les pe au nouveau format, un d’objet peKey => peValue */
  phrasesEtat?: PlainObject
}

export interface LegacySectionMainFunction {
  (this: Parcours): void
  // Propriétés ajoutées par j3pLoad ou LegacySectionRunner
  upgradeParametres?: (this: Parcours, params: ParamsValues) => void
  parametres?: LegacySectionParamsParametres
  phrasesEtat?: PlainObject
}

export interface LegacySection {
  params: LegacySectionParams
  default: LegacySectionMainFunction
  upgradeParametres?: (params: ParamsValues) => void
}

export interface LegacyBilan {
  boucle?: number
  duree: number
  fin?: boolean
  id: string
  index?: number
  nextId?: string
  /** index de la prochaine section ou 'fin' (ne devrait plus servir depuis le 11/05/2020 car on utilise nextId) */
  ns?: number | string
  pe: string
  score: number | null // null pour les sections passives
}

export type LegacyPositionNode = [number, number]
export type TitreNodesV1 = Array<string | null>

/** Format de l’objet mis dans les ressources j3p v1 dans ressource.parametres.editgraphes */
export interface EditGrapheOptsV1 {
  positionNodes: LegacyPositionNode[]
  titreNodes: Array<string | null>
}

/** Les params d’un node v1 (éventuel dernier élément du 3e elt du tableau du node) */
export type LegacyNodeParams = ParamsValues

/** Les branchements sortant d’un node v1 (dans le 3e elt du tableau du node) */
export interface LegacyConnector {
  nn: string
  conclusion?: string
  // score et pe sont facultatifs, mais il faut l’un des deux
  score?: string
  pe?: string
  max?: number
  snn?: string
  sconclusion?: string
  maxParcours?: number
}

/** Les options d’un node v1 (3e elt du tableau) */
export type LegacyNodeOptions = [...LegacyConnector[], LegacyNodeParams]
/**
 * Le tableau décrivant un node v1
 * Le 1er elt est l’id du node (il doit être numérique pour l’éditeur v1, pas pour le player),
 * le 2e est le nom de la section,
 * le 3e contient les options éventuelles (branchements et params en dernier)
 */
export type LegacyNode = [string, string, LegacyNodeOptions?]
/** Un graphe v1 */
export type LegacyGraph = LegacyNode[]

/**
 * Le contenu du legacyResultat
 */
export interface LegacyContenu {
  editgraphes?: EditGrapheOptsV1
  /** La liste des ids des nœuds faits */
  noeuds: string[]
  /** Les pe retournées par ces nœuds */
  pe: (string | number)[]
  /** Les scores retournés par ces nœuds */
  scores: Array<number | null>
  /** la liste des ids des nœuds suivant, redondant avec noeuds */
  ns: (string | number)[]
  /** L’id du nœud suivant (si on reprend le graphe) */
  nextId: string
  /** à décrire… */
  boucle: (number | boolean)[]
  /** à décrire… */
  boucleGraphe: Record<string, (number | null)[]>
  /** Le graphe tel qu’il était au moment du résultat, important lors de la reprise (pour ne pas reprendre si les nœuds déjà faits ont changé depuis */
  graphe: Array<LegacyNode>
  /** Les bilans de chaque nœud */
  bilans: LegacyBilan[]
  /** Un éventuel objet pour passer des infos entre nœuds, important pour la reprise */
  donneesPersistantes: PlainObject
}

/**
 * Un Résultat (https://bibliotheque.sesamath.net/doc/modules/sesatheque-client/Resultat.html) pour sesaparcours (qui précise contenu)
 */
export interface LegacyResultat {
  oid?: string
  contenu: LegacyContenu // voir ci-dessus
  /**
   * Une date (ou sa représentation en string au format YYYY-MM-DDThh:mm:ss.sssZ, idem json)
   */
  date: Date | string
  duree: number
  fin: boolean
  score: number
  reponse: string
}

export type LegacyResultatCallback = (resultat: LegacyResultat) => void

interface LegacyFenetreJquery {
  id: string
  name: string
  title: string
  width: number
  left: number
  top: number
}

export type LegacyFenetresJquery = LegacyFenetreJquery[]

/** Objet retourné par les fonctions validate */
export interface CheckResults {
  ok: boolean
  warnings: string[]
  errors: string[]
}

/** Objet evaluations éventuellement exporté par une section v2 */
export type SectionEvaluations = Record<string, string>

/**
 * Résultat retourné par une méthode correction
 */
export interface PartialResult {
  ok: boolean
  feedBackMessage?: string
  partialScore?: number
  evaluation?: string
  /** Passer true pour provoquer le passage automatique au nœud suivant (pas de feedBackMessage dans ce cas) */
  nextAuto?: boolean
}

/**
 * Les types (js) pour chaque type de paramètre
 */
export interface SectionParameterTypes {
  boolean: boolean
  string: string
  integer: number
  number: number
}

/** Les types valides pour un paramètre de section ('string'|'number'|'integer'|'boolean') */
export type SectionParameterType = keyof SectionParameterTypes

type SectionParameterEditor<T extends SectionParameterType> = ((container: HTMLElement, value: SectionParameterTypes[T], values: ParamsValues) => Promise<SectionParameterTypes[T]>)
type SectionParameterValidate<T extends SectionParameterType, M extends boolean> = (
  value: M extends true
    ? Array<SectionParameterTypes[T]>
    : SectionParameterTypes[T],
  values: ParamsValues
) => Promise<CheckResults>

/**
 * Un paramètre de section
 */
export interface SectionParameter<T extends SectionParameterType, M extends boolean> {
  /** Le type de paramètre boolean|integer|number|string, cf SectionParameterTypes */
  type: T
  /** La valeur par défaut */
  defaultValue: M extends true ? Array<SectionParameterTypes[T]> : SectionParameterTypes[T]
  /** Le message d’aide à la saisie */
  help: string
  /** true si la valeur est multiple (Array) */
  multiple?: M
  /** Pour imposer une liste de valeurs possibles, doit inclure defaultValue */
  controlledValues?: Array<SectionParameterTypes[T]>
  /** un min imposé pour les types integer|number */
  min?: number
  /** un max imposé pour les types integer|number */
  max?: number
  /** Un nb minimal de valeurs (ignoré si !multiple) */
  nbValuesMin?: number
  /** Un nb max de valeurs (ignoré si !multiple) */
  nbValuesMax?: number
  /** Une fonction pour éditer le paramètre */
  editor?: SectionParameterEditor<T>
  /** Une fonction de validation */
  validate?: SectionParameterValidate<T, M>
}

/**
 * Objet parameters exporté par une section v2
 * La propriété title est obligatoire.
 * Les propriétés nbQuestions, limite, autoNextQuestion sont connues et gérée par le Player et
 */
export interface SectionParameters extends Record<string, SectionParameter<SectionParameterType>> {
  /** Le titre par défaut de la section, obligatoire pour toutes */
  title: SectionParameter<'string'>
}

/**
 * Si la section gère elle-même le passage d’énoncé à correction,
 * (avec une validation dans .zoneWork, par ex le drapeau Scratch)
 * alors la promesse retournée par enonce() doit résoudre avec une fonction
 * permettant de récupérer une promesse de résultat
 * (et le player n’affichera pas le bouton ok
 * mais un message disant de lire la consigne pour valider)
 */
type PartialResultGetter = () => Promise<PartialResult>

export type ValidateParametersFn = (params: ParamsValues) => Promise<Record<string, string>>
/**
 * Format d’une section v2
 */
export interface Section {
  /** Les paramètres modifiables (dans editGraphe) de la section */
  parameters: SectionParameters
  /** Une éventuelle fonction pour gérer la compatibilité ascendante (modif de param) */
  updateParameters?: (params: ParamsValues) => void
  /** Une éventuelle fonction appelé à chaque modif de paramètre lors de l’édition, pour éventuellement en masquer d’autres ou donner des indications */
  /** Une éventuelle fonction de validation globale, pour valider la cohérence de l’ensemble des paramètres (si certains choix possibles sont incompatibles entre eux), appelée par l’éditeur */
  validateParameters?: ValidateParametersFn
  /** La liste des évaluations possibles pour une section qualitative (ex phrases d’état) */
  evaluations?: SectionEvaluations
  /** La fonction appelée à l’initialisation de la section (avant l’affichage de la première question) */
  init: (ctx: SectionContext) => voidPromised
  /** Fonction facultative pour savoir si une réponse a été fournie (sinon on ne vérifie que les items connus par Playground) */
  isAnswered?: (ctx: SectionContext) => boolean | Promise<boolean>
  /**
   * La fonction appelée au début de chaque question, pour afficher l’énoncé.
   * La promesse qu’elle retourne résout avec void en général, mais ça peut être
   * une fct de type PartialResultGetter.
   * Dans ce cas c’est elle qui gère le déclenchement de la correction et le player
   * n’affichera pas de bouton ok mais un message disant de lire la consigne pour valider
   * (et il attendra que la promesse retournée par le partialResultGetter soit résolue
   * pour incrémenter le score et passer à la question suivante)
   */
  enonce: (ctx: SectionContext) => Promise<void | PartialResultGetter>
  /** la fonction appelée à la validation de l’élève (pour chaque question) */
  check: (ctx: SectionContext) => Promise<PartialResult>
  /** la fonction qui gère l’affichage de la solution (pour le dernier essai de l’étape|question) */
  solution: (ctx: SectionContext) => voidPromised
}

/** Une ressource générique de la bibliothèque */
export interface Ressource {
  rid: string
  type: string
  titre: string
  idOrigine: string
  parametres: PlainObject
}

/** Propriété parametres d’une ressource j3p de la bibliothèque (v1, en v2 c’est le format RessourceJ3pParametres) */
export interface RessourceJ3pParametresV1 {
  /** graphe au format v1 (tableau de nodeV1) */
  g: LegacyGraph
  /** infos editgraphes (v1) */
  editgraphes?: EditGrapheOptsV1
}

/** Propriété parametres d’une ressource j3p de la bibliothèque (v2, en v1 c’est RessourceJ3pParametresV1 */
export interface RessourceJ3pParametres {
  graph: GraphJson
}
/** Ressource j3p (v2 only) */
export interface RessourceJ3p extends Ressource {
  type: 'j3p'
  parametres: RessourceJ3pParametres
}
/** Ressource j3p de la bibliothèque (v1 ou v2) */
export interface RessourceJ3pBibli extends Ressource {
  type: 'j3p'
  parametres: RessourceJ3pParametresV1 | RessourceJ3pParametres
}

export interface LegacyNodeElements {
  id: string
  section: string
  branchements: LegacyConnector[]
  params: LegacyNodeParams
}

/** signature de la fct qui récupère l’objet à mettre dans ressource.parametres */
export type ressourceJ3pParametresGetter = () => RessourceJ3pParametres
export type ressourceJ3pParametresGetterV1 = () => RessourceJ3pParametresV1

/**
 * Un résultat j3p (toutes versions) pour la Sésathèque
 * @see https://bibliotheque.sesamath.net/doc/modules/sesatheque-client/Resultat.html)
 */
export interface Resultat {
  contenu: {
    pathway: PathwayJson
  }
  date: string
  duree: number
  fin: boolean
  reponse: ''
  rid?: string // facultatif car c’est la bibliothèque qui l’ajoutera (on le récupère mais on ne l’envoie pas)
  score: number
  type: 'j3p'
}

export type ResultatCallback = (resultat: Resultat) => void

export type EventListener = (event?: Event) => void

export type KeyboardEventListener = (event: KeyboardEvent) => void

export type LogLevel = 0 | 1 | 2 | 3 | 4 | 'debug' | 'notice' | 'warning' | 'error' | 'critical'

export interface J3pLoadOptions {
  graphe: LegacyGraph
  editgraphes?: EditGrapheOptsV1
  indexInitial?: number
  lastResultat?: LegacyResultat
  logLevel?: LogLevel
  resultatCallback?: LegacyResultatCallback
  /** url absolue du domaine où charger j3p (indispensable si la page courante n’y est pas) */
  baseUrl?: string
}

// les propriétés globales que l’on est susceptible d’ajouter
export type LoaderName = 'j3pLoad' | 'editGraphe' | 'showParcours' | 'spPlay' | 'spEdit' | 'spView' | 'spValidate'
export type SpLoadingState = 'preload' | 'lazy' | 'ready'
export type SpLoading = Record<LoaderName, SpLoadingState>

declare global {

  interface Window {
    j3p?: Parcours
    j3pVersion?: string
    mtgUrl?: string
    spLoading?: SpLoading
    j3pLoad?: (container: HTMLElement | string, options: J3pLoadOptions, loadCallback?: voidFunction) => typeof loadCallback extends voidFunction ? void : voidPromised
    editGraphe?: (container: HTMLElement, ressource: RessourceLike, options?, next?) => void
    showParcours?: (container: HTMLElement | string, resultat: LegacyResultat) => void
    spPlay?: (container: HTMLElement | string, options: PathwayValues) => void
    spEdit?: (container: HTMLElement | string, ressource: RessourceLike, next: editorCallback) => void
    spView?: (container: HTMLElement | string, resultat: Resultat | LegacyResultat) => void
    spValidate?: (ressource: RessourceJ3p, errors: PlainObject) => void
    vkContainer?: HTMLDivElement
    vkParentContainer?: HTMLDivElement
    currentEditor?: HTMLInputElement | MathfieldElement
  }
}

export interface InputWithKeyboard extends HTMLInputElement {
  virtualKeyboard: VirtualKeyboard
  mathVirtualKeyboardPolicy: string
  triggerBtn: HTMLButtonElement
}

export interface MathfieldWithKeyboard extends MathfieldElement {
  virtualKeyboard: VirtualKeyboard
  mathVirtualKeyboardPolicy: string
  triggerBtn: HTMLButtonElement
}

export interface KeyboardDivContainer extends HTMLDivElement {
  virtualKeyboard: VirtualKeyboard
}

export interface KeyboardMLContainer extends HTMLDivElement {
  isDragging: boolean
  _dragStartX: number
  _dragStartY: number
  hasBeenMoved: boolean
  mouseDownML: (event: TouchEvent | MouseEvent) => void
  touchStartML: (event: TouchEvent | MouseEvent) => void
  touchEndML: () => void
}
