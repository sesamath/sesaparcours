/** @module lib/core/loadJqueryDialog */

// les imports dynamiques des css plaisent pas à l’éditeur
import 'jquery-ui/themes/base/base.css'
import 'jquery-ui/themes/base/theme.css'
import 'jquery-ui/themes/base/dialog.css'

// cf https://stackoverflow.com/questions/38417328/import-jquery-ui-and-jquery-with-npm-install

// depuis qu’on a débranché le parsing tsc dans eslint (bcp trop lent), eslint signale du no-undef pour les types que tsc trouve très bien
/* global JQueryStatic */
declare global {

  interface Window {
    jQuery?: JQueryStatic
    $?: JQueryStatic
  }
}

// declare module 'jquery-ui/ui/widget'

export default async function loadJqueryDialog (): Promise<JQueryStatic> {
  const { default: $ } = await import('jquery')
  // avant le type module, on avait pas besoin de l’affecter ici, mais sans ça on a du `jQuery is not defined` dans dialog.js
  window.jQuery = $
  window.$ = $
  // ni de charger jquery-ui/ui/widget avant de charger dialog (ça donne du $.widget is not a function)
  await Promise.all([
    // une extension à nous
    import('src/legacy/core/jqueryTextWidth.js'),
    // import('jquery-ui/dist/jquery-ui.min') // avec lui seul tout fonctionne, mais on charge tous les widgets… 330ko vs ~50
    // on se limite aux dépendances de https://api.jqueryui.com/dialog/
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/widget') // lui défini $.ui, il doit être chargé et exécuté avant d’en charger d’autres
  ])
  await Promise.all([
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/unique-id'), // sans lui l’appel de dialog() plante
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/widgets/button') // doit être chargé avant focusable
  ])
  await Promise.all([
    // et les morceaux de jquery-ui dont on a besoin
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/widgets/mouse'), // indispensable pour resizable
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/plugin'), // indispensable pour resizable
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/keycode'), // sinon ça plante dans des listeners de dialog
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/data'),
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/disable-selection'), // indispensable pour resizable
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/effect'),
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/tabbable'),
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/focusable'),
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/safe-active-element'),
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/position'),
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/scroll-parent'),
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/safe-blur'),
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/widgets/dialog'),
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/widgets/resizable'),
    // @ts-ignore TS7016: Could not find a declaration file for module …
    import('jquery-ui/ui/widgets/draggable')
  ])
  // await import('jquery-ui/ui/effects/effect-transfer')
  return $
}
