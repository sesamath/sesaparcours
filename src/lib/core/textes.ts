const textesFr = {
  anger: '😣',
  bug: 'Cette ressource a rencontré un problème : prenez une capture d`écran et signalez le à votre professeur.',
  cBien: 'C’est bien !',
  cFaux: 'C’est faux !',
  confused: '😕',
  essaieEncore: 'Essaie encore !',
  maxRuns: 'exécution(s) max',
  maxTries: 'essai(s) max',
  nbQuestions: 'nombre de questions',
  nbSteps: 'nombre d’étapes',
  parcoursTermine: 'Ce parcours a déjà été terminé.\nSi tu fermes cette boite de dialogue,\nil reprendra du début\n(sinon choisis un autre exercice).',
  regardeCorrection: 'Regarde la correction !',
  reponseIncomplete: 'Réponse incomplète',
  reponseManquante: 'Il faut donner une réponse !',
  smile: '🙂',
  tempsDepasse: 'Temps dépassé !'
}

const textesEn = {
  anger: '😣',
  bug: 'This resource has encountered a problem : take a screenshot and send it to your teacher.',
  cBien: 'Good !',
  cFaux: 'It’s false !',
  confused: '😕',
  essaieEncore: 'Try again !',
  maxRuns: 'max runs',
  maxTries: 'max tries',
  nbQuestions: 'number of questions',
  nbSteps: 'number of steps',
  parcoursTermine: 'This resource is already achieved.\nClosing this dialog will launch it again from start\n(if you don’t want to, choose another exercise).',
  regardeCorrection: 'Look at the correction !',
  reponseIncomplete: 'Incomplete answer !',
  reponseManquante: 'Answer needed !',
  smile: '🙂',
  tempsDepasse: 'Time elapsed !'
}

/**
 * Les textes de l’application
 */
const textes = textesFr

export default textes

type TextKey = keyof typeof textesFr

/**
 * Retourne le texte correspondant à key (ou key s'il n'existe pas)
 * @param {string} key
 * @returns {string}
 */
export const getText = (key: TextKey | string): string => textes?.[key as TextKey] ?? key

/**
 * Défini la langue à utiliser (à appeler avant tout chargement de section)
 */
export function setLang (lang: string) {
  switch (lang) {
    case 'fr':
      Object.assign(textes, textesFr)
      break
    case 'en':
      Object.assign(textes, textesEn)
      break
    default:
      throw Error(`langue ${lang} inconnue`)
  }
}
