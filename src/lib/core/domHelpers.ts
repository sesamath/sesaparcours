/**
 * Quelques fonctions de gestion du dom spécifiques à j3p (les génériques sont dans utils/dom)
 * @module lib/core/domHelpers
 */
import { getFirstParent } from 'src/lib/utils/dom/main'

/**
 * Retourne le conteneur j3p parent de l’élément (l’élément qui a la classe css j3pContainer,
 * à priori c’est #Mepact mais l’id n’existe pas forcément alors que la classe est toujours là)
 * @param elt
 * @param [doNotThrow] passer true pour que ça retourne null sans planter si on le trouve pas
 * @throws {Error} si y’en a pas (sauf si on a précisé doNotThrow)
 */
export function getJ3pConteneur (elt: HTMLElement, doNotThrow = false): typeof doNotThrow extends false ? HTMLElement : (HTMLElement | null) {
  return getFirstParent(elt, 'j3pContainer', { strict: !doNotThrow })
}

/**
 * Retourne la zone parente (MG, MD, etc) de l’élément
 * @param elt
 * @param [doNotThrow] passer true pour que ça retourne null sans planter si on le trouve pas
 * @throws {Error} si y’en a pas (sauf si on a précisé doNotThrow)
 */
export function getZoneParente (elt: HTMLElement, doNotThrow: boolean): typeof doNotThrow extends false ? HTMLElement : (HTMLElement | null) {
  return getFirstParent(elt, 'divZone', { strict: !doNotThrow })
}
