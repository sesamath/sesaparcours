import Graph from 'src/lib/entities/Graph'

import type { PlainObject, RessourceJ3p } from 'src/lib/types'

/**
 * Valide les paramètres de la ressource (utilisé par sesatheque-plugin-j3p)
 * @param ressource qui sera éventuellement modifiée
 * @param {PlainObject} errors Objet dans lequel seront ajouté les erreurs éventuelles (clé parametres)
 */
function spValidate (ressource: RessourceJ3p, errors: PlainObject, { clean = false } = {}) {
  console.debug('spValidate call', ressource, errors)
  // check des arguments, ça throw en cas de pb
  if (!ressource) throw Error('ressource manquante')
  if (ressource.type !== 'j3p') throw Error('La ressource n’est pas de type j3p')
  if (!ressource.parametres?.graph) throw Error('Ressource j3p invalide (pas de graphe)')
  if (errors == null || typeof errors !== 'object') throw Error('il faut fournir un objet errors (qui sera éventuellement peuplé)')

  // pour la suite, faut catch une erreur éventuelle pour la mettre dans errors
  try {
    const graph = new Graph(ressource.parametres.graph)
    const { errors: graphErrors } = graph.validateSync({ clean, emptyAllowed: true })
    if (clean) {
      ressource.parametres.graph = graph.toJSON()
    }
    if (graphErrors.length) {
      errors.parametres = graphErrors.join(';\n')
    }
  } catch (error) {
    console.error(error)
    errors.parametres = error instanceof Error ? error.message : String(error)
  }
  console.debug(`fin spValidate (clean ${clean})`, ressource?.parametres?.graph, errors)
}

export default spValidate
