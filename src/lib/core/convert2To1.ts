/** @module lib/core/convert2To1 */

import { isLegacyConnector } from 'src/lib/core/checks'
import { params2byName1 } from 'src/lib/core/convert1to2'
import Connector from 'src/lib/entities/Connector'
import Graph from 'src/lib/entities/Graph'
import GraphNode, { type ParamsValues } from 'src/lib/entities/GraphNode'

import type { EditGrapheOptsV1, LegacyConnector, LegacyGraph, LegacyNode, LegacyNodeOptions, LegacyNodeParams, RessourceJ3pParametresV1 } from 'src/lib/types'

// la réciproque de params2byName1
const params1byName2: Record<string, string> = {}
for (const [name1, name2] of Object.entries(params2byName1)) {
  params1byName2[name2] = name1
}

/**
 * Convertit un GraphNode (V2) en LegacyNode (tableau de deux ou trois éléments)
 * @param graphNode
 */
function convertNodeBack (graphNode: GraphNode): LegacyNode {
  const { id, section, connectors, params } = graphNode
  if (id == null) {
    throw Error(`id manquant dans ce nœud du graphe ${JSON.stringify(graphNode)}`)
  }
  if (graphNode.isEnd()) return [id, 'fin']
  // les branchements convertis en v1
  const branchements: LegacyConnector[] = []
  // faut mémoriser le connector précédent pour éventuellement coller du max au connecteur courant dans la boucle
  let lastConnector: Connector | undefined
  // on peut boucler sur les connecteurs
  for (const connector of connectors) {
    const connector1: LegacyConnector = { nn: connector.target, conclusion: connector.feedback }
    try {
      switch (connector.typeCondition) {
        case 'none':
          connector1.score = 'sans condition'
          break
        case 'score':
          connector1.score = connector.scoreComparator + connector.scoreRef.toString()
          break
        case 'pe':
          connector1.pe = connector.peRefs.join(',')
          break
        case 'nbRuns':
          // rien pour nbRuns, il faudra ajouter un max au connecteur suivant
          lastConnector = connector
          continue
        default:
          throw Error(`Type de condition non géré ${connector.typeCondition}`)
      }
      if (lastConnector != null && lastConnector.typeCondition === 'nbRuns') {
        connector1.snn = lastConnector.target
        connector1.sconclusion = lastConnector.feedback
        if (connector.target === id) {
          // quand on boucle sur le même nœud c’est la propriété max
          connector1.max = lastConnector.nbRuns
        } else {
          // quand on boucle vers un nœud précédent du graphe c’est la propriété maxParcours
          connector1.maxParcours = lastConnector.nbRuns
        }
      }
      branchements.push(connector1)
    } catch (error) {
      console.error(error, '\navec les datas:\n', connector1, '\ndu connector1:\n', connector)
      throw error
    }
    lastConnector = connector
  }
  if (lastConnector != null && lastConnector.typeCondition === 'nbRuns') {
    throw Error(`GraphNode ${graphNode.label} (${graphNode.id}) invalide avec un dernier branchement de type nbRuns`)
  }
  const options: LegacyNodeOptions = [...branchements, convertGraphParamsBack(params)]
  return [id, section, options]
}

/**
 * Permet au legacySectionRunner de récupérer la correspondance entre les id du graphe V2 t les id converties pour Parcours.
 * @param graph
 */
export function idsV1FromIdsV2 (graph: Graph): Record<string, string> {
  const idsV1: Record<string, string> = {}
  let i = 1
  for (const node of Object.values(graph.nodes)) {
    idsV1[node.id] = String(i++)
  }
  return idsV1
}

/**
 * Converti un graphe v2 (objet Graph) en graphe v1 (tableau de tableaux) + objet editgraphes (avec positionNodes et titreNodes)
 * @param {Graph} graph
 * @throws {Error} au 1er pb rencontré
 */
export async function convertGraphBack (graph: Graph): Promise<RessourceJ3pParametresV1> {
  // on s’assure d’avoir un graphe correct avant de convertir
  const { warnings, errors } = await graph.validate({ clean: true })
  if (errors.length) throw Error(errors.join('\n'))
  if (warnings.length) console.warn('warnings du graph.validate', ...warnings)
  const g: LegacyGraph = []
  const editgraphes: EditGrapheOptsV1 = { positionNodes: [], titreNodes: [] }
  // Important, il faut mettre le node startingId en premier dans le graphe (en v1 ça démarre par le premier)

  // l’éditeur de graphe v1 déconne complètement quand les index de node ne sont pas numériques…
  // une map de translation des ids (cela intéresse aussi le legacySectionRunner donc je factorise)
  const newIds: Record<string, string> = idsV1FromIdsV2(graph)
  // newIds est complet, notre fct de modif du node pour corriger les ids
  const translateConnectors = (node: LegacyNode) => {
    const opts = node[2]
    if (!Array.isArray(opts)) return
    for (const opt of opts) {
      if (isLegacyConnector(opt)) {
        opt.nn = newIds[opt.nn] ?? ''
        if (opt.snn != null) opt.snn = newIds[opt.snn] ?? ''
      }
    }
  }

  // faut mettre le startingId en premier dans le tableau de nodes v1
  const firstNode = graph.getNode(graph.startingId)
  if (firstNode == null) throw Error(`Aucune nœud d’id ${graph.startingId} dans le graphe (c’était startingId)`)
  const firstNodeV1 = convertNodeBack(firstNode)
  const newStartingId = newIds[firstNode.id]
  if (newStartingId == null) throw Error(`pas trouvé l’id v1 de ${firstNode.id}`)
  firstNodeV1[0] = newStartingId
  if (Array.isArray(firstNodeV1[2])) {
    translateConnectors(firstNodeV1)
  }
  g.push(firstNodeV1)
  const { x, y } = firstNode.position
  editgraphes.positionNodes.push([x, y])
  editgraphes.titreNodes.push(firstNode.label)

  for (const node of Object.values(graph.nodes)) {
    if (node.id === graph.startingId) continue // on l’a déjà pris
    const node1 = convertNodeBack(node)
    const { x, y } = node.position
    editgraphes.positionNodes.push([x, y])
    editgraphes.titreNodes.push(node.label)
    node1[0] = newIds[node.id] ?? ''
    translateConnectors(node1)
    g.push(node1)
  }
  return { g, editgraphes }
}

/**
 * Convertit les noms de paramètres qui ont changé entre v1 et v2 (nbrepetitions => nbQuestions, etc.)
 * @param params les params du GraphNode (v2)
 */
function convertGraphParamsBack (params: ParamsValues): LegacyNodeParams {
  const paramsV1: LegacyNodeParams = {}
  for (const [name, value] of Object.entries(params)) {
    const key = params1byName2[name] ?? name
    paramsV1[key] = value
  }
  return paramsV1
}
