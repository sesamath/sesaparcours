/**
 * Url absolue du site j3p courant utilisé (même si j3p est chargé sur un autre domaine, depuis une sesathèque par exemple)
 * Avec le slash de fin, par ex https://j3p.sesamath.net/ ou http://localhost:8081/
 */
export const baseUrl = import.meta.url.replace(/^(https?:\/\/[^/]+\/).*/, '$1')
export const j3pBaseUrl = baseUrl

/** Valeur max admise pour maxRun */
export const maxRunCeil = 10
