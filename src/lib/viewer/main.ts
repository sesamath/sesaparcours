import Viewer from './Viewer'

import type { IPathwayValues, PathwayJson } from 'src/lib/entities/Pathway'

async function showPathway (container: HTMLElement, pathway: IPathwayValues | PathwayJson): Promise<void> {
  const viewer = new Viewer(pathway)
  await viewer.display(container)
}

export default showPathway
