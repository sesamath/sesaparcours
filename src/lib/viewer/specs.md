# ShowParcours

Il s'agit d’une "webapp" qui affiche un parcours fait par un élève dans un graphe (à partir d’un résultat), le parcours
n’étant pas forcément terminé.

Elle est appelée par labomep au clic sur le lien "détails" des bilans (pour le prof et l’élève, que ce soit un bilan de
séquence ou d’élève individuel), mais pourrait aussi l'être dans mathenpoche ou n’importe quel site qui affiche du j3p
et stocke des résultats.

Cf src/lib/types.d.ts pour le format générique d’un
objet [Resultat](https://bibliotheque.sesamath.net/doc/modules/sesatheque-client/Resultat.html) et LegacyResultat, la
méthode _sendResult de src/legacy/core/Parcours.js et

## TODO préalable

- finir d’implémenter convertResultat (src/lib/core/convert1to2.ts)
- le player (pour la reprise de parcours si on fourni un lastResultat) et showParcours doivent utiliser convertResultat
  si besoin (faire un import dynamique pour ne pas mettre d’office ce code v1 dans les chunks js v2)
- le player doit envoyer un objet Resultat, en mettant un ResultGraph dans resultat.contenu

## Spécifications

showParcours doit afficher un parcours comme editor le fait pour un graphe, avec

- pour les nœuds "faits"  
  [x] sous le titre on ajoute `Score: x%` ou `Scores: x% | y% | z%` si y’en a plusieurs.  
  [x] S'il y a une phrase d’état (
  pe en v1 et evaluation en v2) on l’affiche aussi (avec `Évaluation : xx` ou `Évaluations : xx | yy | zz`)  
  [x] bordure grasse dont la couleur dépend du score (en v1 c'est toujours rouge, ce qui laisse supposer un échec), on
  pourra utiliser par ex `hsl(${90 * score} 100% 50% )` pour varier de 0 (rouge) à 90° (vert).  
  [x] Si le nœud a été fait
  plusieurs fois on prend le meilleur score (on pourrait lui mettre plusieurs bordures plus fines de couleurs
  différentes pour chaque score, mais c'est plus compliqué à gérer)  
  [x] les autres nœuds du graphe sont présents mais grisés (on pourra ajouter ensuite un bouton pour les
  afficher/masquer)
  [x] les branchements utilisés par l’élève sont en gras, et on indique dedans un numéro permettant de suivre le
  parcours.
  [x] les autres branchements sont grisés  
  [x] pour les infos rang et condition, à voir si on les affiche au survol ou directement dans la case du branchement (
  ça
  peut faire bcp et être impossible à afficher sans chevauchement)  
  [x]  on utilise les infos du résultat pour positionner les nœuds (en v1 c'est
  resultat.contenu.editgraphe.positionsNodes)
  et les nommer (resultat.contenu.editgraphe.titresNodes), mais ils restent déplaçables (au cas où l’utilisateur veut
  les bouger pour y voir plus clair)

~~Il faudra donc voir comment mutualiser tout ça avec editor, car y’a bcp de choses en commun, et pas mal de différence.
Je sais pas si on peut avoir une classe SceneUIeditor qui hérite d’un SceneUIshowParcours (à priori plus pauvre), ou
bien ces deux là qui hériteraient d’une SceneUIbase pour le code commun, ou bien un param showParcours à true qui serait
passé à l’éditeur (à voir combien de `if (isShowParcours)` ça ajouterait au code, le risque étant de le rendre
illisible)~~
