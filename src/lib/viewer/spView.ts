import { isLegacyResultat } from 'src/lib/core/checks'
import showPathway from './main'
import { addElement, normalizeContainer } from 'src/lib/utils/dom/main'

import type { LegacyResultat, Resultat } from 'src/lib/types'

/**
 * Lance l’afficheur de parcours.
 * C’est la fonction qui se retrouvera mise en global (par loader puis lazyLoader), elle est sync et gère les erreurs (et appelle la fct async de main)
 * @module
 */

/**
 * Lance l’afficheur de parcours v2 en gérant les éventuelles erreurs
 * @throws {Error} si le container ou le résultat est invalide
 */
function spView (container: HTMLElement | string, resultat: Resultat | LegacyResultat): void {
  const { mainContainer, errorsContainer } = normalizeContainer(container)
  // on gère ici la chaîne de promesses et les erreurs
  Promise.resolve()
    .then(async () => {
      if (!isLegacyResultat(resultat)) return resultat
      const { convertResultat } = await import('src/lib/core/convert1to2')
      return convertResultat(resultat)
    })
    .then(resultat => {
      showPathway(mainContainer, resultat.contenu.pathway)
    })
    .catch(error => {
      console.error(error)
      const content = error instanceof Error ? error.message : String(error)
      addElement(errorsContainer, 'p', { className: 'error', content })
    })
}

export default spView
