import { BrowserJsPlumbInstance, newInstance } from '@jsplumb/browser-ui'
import { BezierConnector } from '@jsplumb/connector-bezier'
import { Connection, CustomOverlay } from '@jsplumb/core'

import Connector from 'src/lib/entities/Connector'
import Modal from 'src/lib/entities/dom/Modal'
import Graph from 'src/lib/entities/Graph'
import type { GraphNodeSerialized } from 'src/lib/entities/GraphNode'
import GraphNode from 'src/lib/entities/GraphNode'
import type { IPathwayValues, PathwayJson } from 'src/lib/entities/Pathway'
import Result from 'src/lib/entities/Result'
import { addElement } from 'src/lib/utils/dom/main'

import './viewer.scss'

interface CustomOverlay2 extends CustomOverlay {
  canvas: HTMLElement
}

/**
 * Gère l’affichage d’un pathway (un cheminement dans le graphe)
 */
class Viewer {
  graph: Graph
  results: Result[]
  modal?: Modal
  jsPlumb!: BrowserJsPlumbInstance
  nodesElements: Record<string, { elt: HTMLDivElement, scores: string, evals: string, isEnd: boolean }>
  container!: HTMLElement
  nextFreePosition: { x: number, y: number }
  toggleUndone!: HTMLButtonElement
  branchements: Record<string, { connection: Connection, label: string, condition: string }>

  constructor (pathway: IPathwayValues | PathwayJson) {
    this.graph = pathway.graph instanceof Graph ? pathway.graph : new Graph(pathway.graph)
    this.results = pathway.results.map(result => result instanceof Result ? result : new Result(result))
    this.nextFreePosition = { x: 70, y: 50 }
    this.nodesElements = {}
    this.branchements = {}
  }

  /**
   * La méthode qu’on appelle de l’extérieur pour afficher la modale.
   */
  async display (container?: HTMLElement) {
    if (container) {
      // on affiche dans le container qu’on nous a passé
      this.container = container
    } else {
      // on affiche dans une modale
      this.modal = new Modal({ fullScreen: true })
      this.modal.hide()
      this.container = this.modal.addElement('div', {
        className: 'pathwayScene'
      })
    }
    const showAllLabel = 'Montrer tous les nœuds'
    const hideUndoneLabel = 'Masquer les nœuds non parcourus'
    this.toggleUndone = addElement(this.container, 'button', {
      className: 'toggleBtn',
      textContent: hideUndoneLabel,
      style: { position: 'absolute' }
    })
    this.toggleUndone.addEventListener('click', () => {
      if (this.toggleUndone.textContent === showAllLabel) {
        this.toggleUndone.textContent = hideUndoneLabel
      } else {
        this.toggleUndone.textContent = showAllLabel
      }
      for (const node of Object.values(this.nodesElements)) {
        // on touche pas aux nœuds faits
        if (node.elt.classList.contains('done')) continue
        if (!node.isEnd && node.elt.style.display === 'block') {
          node.elt.style.display = 'none'
          this.jsPlumb.select({ target: node.elt }).each(
            (connect) => connect.setVisible(false)
          )
        } else {
          node.elt.style.display = 'block'
          this.jsPlumb.select({ target: node.elt }).each(
            (connect) => connect.setVisible(true)
          )
        }
      }
    })

    // on crée l’instance de jsPlumb
    this.jsPlumb = newInstance({
      container: this.container,
      // cf https://docs.jsplumbtoolkit.com/community/lib/defaults
      connectionsDetachable: false,
      connector: {
        type: BezierConnector.type,
        options: {
          curviness: 5,
          margin: 0,
          proximityLimit: 50,
          anchor: 'Autodefault', // cf https://docs.jsplumbtoolkit.com/community/lib/anchors#default-dynamic-anchor
          detachable: false,
          reattach: false
        }
      },
      connectionOverlays: [
        {
          type: 'Custom',
          options: {
            /**
             * méthode pour créer l’overlay Custom (on définit les listeners qui vont modifier le contenu)
             */
            create: () => {
              const d = addElement(this.container, 'div', { className: 'jtk-default-label' })
              addElement(d, 'span', { content: '', className: 'connectorLabelDefault' })
              addElement(d, 'span', { content: '', className: 'connectorLabelHover' })
              return d
            },
            location: 0.5
          }
        }
      ]
    })

    await this.drawAll()
    // this.colorParcours()
    this.complete()
    this.modal?.show()
  }

  /**
   * Initialise les listeners, renseigne les scores et les évaluations
   */
  complete () {
    for (const node of Object.values(this.nodesElements)) {
      if (node.isEnd) continue
      const nodeScore = node.elt.querySelector('.scores')
      const nodeEval = node.elt.querySelector('.evaluations')
      if (nodeEval == null || nodeScore == null) {
        throw Error(`Problème avec les divs scores et/ou evaluations du noeud ${node.elt.id}`)
      }
      if (node.scores !== '') nodeScore.textContent = `Score${node.scores.includes('|') ? 's' : ''} : ${node.scores}`
      if (node.evals !== '') nodeEval.textContent = `Évaluations : ${node.evals}`
    }
    let indexResult = 0
    while (indexResult < this.results.length - 1) {
      const sourceId = this.results[indexResult]?.id
      const targetId = this.results[indexResult + 1]?.id
      if (!sourceId || !targetId) break
      const id = `${sourceId}-${targetId}`
      const branchement = this.branchements[id]
      if (branchement != null) {
        if (branchement.label === '') branchement.label += String(indexResult + 1)
        else branchement.label += `|${indexResult + 1}`
      }
      indexResult++
    }
    for (const branchement of Object.values(this.branchements)) {
      const connexion = branchement.connection
      if (branchement.connection.target.className.includes('done')) {
        branchement.connection.paintStyle.strokeWidth = 3
        branchement.connection.paintStyle.stroke = 'blue'
      } else {
        branchement.connection.paintStyle.strokeWidth = 1
        branchement.connection.paintStyle.stroke = 'gray'
      }

      if (branchement.label !== '') {
        const overlay = Object.values(connexion.overlays).find(el => el.type === 'Custom') as CustomOverlay2
        if (overlay.canvas != null) {
          const contentLabelDefault = overlay.canvas.querySelector('.connectorLabelDefault')
          if (contentLabelDefault) {
            contentLabelDefault.innerHTML = `${branchement.label}`
          }
          const contentLabelHover = overlay.canvas.querySelector('.connectorLabelHover')
          if (contentLabelHover) {
            contentLabelHover.innerHTML = `${branchement.condition}`
          }
        }
      }
    }
  }

  /**
   * Met en couleur les branchements à partir des résultats
   */
  colorParcours () {

  }

  /**
   * Place les éléments (nodes) dans la scène, crée les connexions, les colorie
   */
  private async drawAll () {
    /**
     * La fonction qui ajoute un connecteur entre deux nœuds
     * @param source
     * @param target
     */
    const addConnection = async (source: string, target: string) => {
      if (this.nodesElements != null) {
        const src = this.nodesElements[source]
        const tgt = this.nodesElements[target]
        const srcNode = this.graph.getNode(source)
        if (srcNode == null) throw Error(`Le noeud ${source} n’a pas été trouvé dans le graphe`)
        const connectors = srcNode.connectors
        const section = srcNode.section
        const connector = connectors.find(el => el.target === target)
        if (connector == null) throw Error(`Le noeud ${source} n’a pas de connecteur vers le noeud ${target} dans le graphe`)
        if (section == null) throw Error(`Le noeud ${source} n’a pas de connecteurs ou de section dans le graphe`)
        const condition = await Connector.getCondition(connector, srcNode.section)
        if (src != null && tgt != null) {
          const connection = this.jsPlumb.connect({
            source: src.elt,
            target: tgt.elt,
            anchor: 'Continuous',
            endpoint: 'Blank',
            overlays: [
              { type: 'Arrow', options: { location: 1, width: 10, length: 15 } }
            ]
          })
          this.branchements[`${source}-${target}`] = { connection, label: '', condition }
        }
      }
    }

    const nodes: GraphNode[] = Array.from(Object.values(this.graph.nodes))
    for (const node of nodes) {
      this.drawNode(node)
    }
    // Les noeuds sont en place, on peut créer les connexions du graphe (qui sont grises au départ).
    for (const node of nodes) {
      const connectors = node.connectors
      for (const connector of connectors) {
        await addConnection(node.id, connector.target)
      }
    }
  }

  /**
   * Ajoute le node sur la scène
   * @param {GraphNode} graphNode
   * @return {HTMLElement}
   */
  private drawNode (graphNode: GraphNodeSerialized): void {
    const classList: string[] = []
    const nodeId = graphNode.id
    const x = graphNode.position != null ? graphNode.position.x : this.nextFreePosition.x
    const y = graphNode.position != null ? graphNode.position.y : this.nextFreePosition.y
    const node = addElement(this.container, 'div', {
      style: { display: 'block' },
      attrs: { 'data-id': nodeId }
    })
    classList.push(graphNode.section === '' ? 'graphNodeEnd' : 'graphNode')
    node.style.top = `${y}px`
    node.style.left = `${x}px`
    const scores = this.results.filter((el: Result) => Boolean(el.id === nodeId)).map(el => el.score)
    if (scores.length > 0) {
      classList.push('done')
    }
    const scoreMax = scores.length > 0 ? Math.max(...scores) : 0
    const borderColor: [number, number, number] = [120 * Math.round(scoreMax * 10) / 10, 50, 50]
    // const backgroundColor: [number, number, number] = [120 * Math.round(scoreMax * 10) / 10, 80, 80]
    // Pour convertir les composante teinte saturation luminosité dans le système RGB à des fins de transformation en gris
    const HSLToRGB = (h: number, s: number, l: number): [number, number, number] => {
      s /= 100
      l /= 100
      const k = (n: number) => (n + h / 30) % 12
      const a = s * Math.min(l, 1 - l)
      const f = (n: number) =>
        l - a * Math.max(-1, Math.min(k(n) - 3, Math.min(9 - k(n), 1)))
      return [255 * f(0), 255 * f(8), 255 * f(4)]
    }
    // Pour convertir la couleur RGB en un équivalent gris.
    const rgb2gray = ([r, g, b]: [number, number, number]) => {
      const gray = 0.2126 * r + 0.7152 * g + 0.0722 * b
      return [gray, gray, gray]
    }
    const grayBorder = `rgb(${rgb2gray(HSLToRGB(...borderColor)).map(el => `${Math.round(el)}`).join(' ')})`
    // const grayBackground = `rgb(${rgb2gray(HSLToRGB(...backgroundColor)).map(el => `${Math.round(el)}`).join(' ')})`
    if (!classList.includes('graphNodeEnd')) {
      node.style.borderColor = scores.length > 0 ? `hsl(${borderColor[0]} ${borderColor[1]}% ${borderColor[2]}%)` : grayBorder
      //  node.style.backgroundColor = scores.length > 0 ? `hsl(${backgroundColor[0]} ${backgroundColor[1]}% ${backgroundColor[2]}%)` : grayBackground
      node.style.color = scores.length > 0 ? 'hsl(0 0 0)' : 'rgb(40 40 40)'
    }
    node.style.borderWidth = '3px'
    node.classList.add(...classList)
    if (graphNode.section === '') {
      addElement(node, 'div', { textContent: 'Noeud final' })
    }
    addElement(node, 'div', { className: 'title', textContent: graphNode.label })
    if (graphNode.section !== '' && graphNode.section !== 'fin') {
      addElement(node, 'div', { className: 'scores', textContent: '' })
      addElement(node, 'div', { className: 'evaluations', textContent: '' })
    }

    this.nextFreePosition.y = this.nextFreePosition.y + 120
    this.nextFreePosition.x = this.nextFreePosition.x + 100
    if (this.nextFreePosition.y > 600) {
      this.nextFreePosition.x = this.nextFreePosition.x + 100
      this.nextFreePosition.y = 50
    }
    const scoresString = scores.map(el => `${(el * 100).toFixed(0)}%`).join('|')
    const evals = this.results.filter((el: Result) => Boolean(el.id === nodeId) && el.evaluation !== '').map(el => el.evaluation).join('|')
    // on ajoute l’élément html à notre liste interne
    this.nodesElements[nodeId] = { elt: node, scores: scoresString, evals, isEnd: graphNode.section === '' }
  }
}

export default Viewer
