import { addElement, normalizeContainer } from 'src/lib/utils/dom/main'
import { normalizeOptions } from 'src/lib/core/checks'
import play from './main'

import { MathfieldElement } from 'mathlive'

import type { LegacyGraph, LegacyResultat, Resultat } from 'src/lib/types'
import type { GraphValues } from 'src/lib/entities/Graph'

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// dans loadMathlive c’est bon avec pnpm start mais trop tard dans un build)
MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
MathfieldElement.soundsDirectory = '/static/mathlive/sounds'

export interface SpPlayOptions {
  baseUrl?: string
  /** Le graphe à jouer */
  graph?: GraphValues | LegacyGraph
  /** Autre prop possible pour le graphe à jouer */
  graphe?: GraphValues | LegacyGraph
  /** un résultat (v1|v2) contenant le graphe à reprendre */
  lastResultat?: Resultat | LegacyResultat
  isDebug?: boolean
  mtgUrl?: string
  resultatCallback?: (resultat: Resultat) => void
}

/**
 * Lance le player v2 (avec un graphe v1 ou v2) en gérant les éventuelles erreurs
 * @param container
 * @param options
 * @param [loadCb] Pour compatibilité ascendante (avec j3pLoad), une éventuelle callback de chargement (idem attendre que la promesse de chargement retournée soit résolue)
 * @throws {Error} si le container est invalide (si y’a un pb dans les options ce sera mis en console et à l’écran)
 */
function spPlay (container: HTMLElement | string, options: SpPlayOptions, loadCb ?: (error?: Error) => void): Promise<void> {
  const { mainContainer, errorsContainer } = normalizeContainer(container)
  // if (mainContainer.parentElement != null) mainContainer.parentElement.style.height = '100%'
  // on gère ici la chaîne de promesses et les erreurs
  // on normalise les options en v2 only
  return normalizeOptions(options)
    .then(opts => {
      if (opts == null) throw Error('Arguments invalides')
      const { graph, lastResultat } = opts
      return play(mainContainer, { graph, lastResultat, resultatCallback: options.resultatCallback, isDebug: Boolean(options.isDebug) })
    })
    .then(() => {
      if (loadCb) {
        try {
          loadCb()
        } catch (error) {
          console.error('Erreur lancée par la callback de chargement', error)
        }
      }
    })
    .catch(error => {
      console.error('plantage dans play', error)
      if (loadCb) {
        loadCb(error)
      } else {
        const content = error instanceof Error ? error.message : String(error)
        addElement(errorsContainer, 'p', { className: 'error', content })
      }
    })
}

export default spPlay
