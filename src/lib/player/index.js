// même pb que pour editor, depuis un js un import('src/lib/player/main.js') ne marche pas, mais en l’important en statique ici sans extension ça fonctionne
// depuis un .ts un import dynamique de ts sans extension fonctionne
import play from './main'
import { MathfieldElement } from 'mathlive'

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// dans loadMathlive c’est bon avec pnpm start mais trop tard dans un build)
MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
export default play
