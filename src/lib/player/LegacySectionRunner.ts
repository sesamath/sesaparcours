import { getPhrasesEtat } from 'src/j3pLoad'
import Parcours, { type ParcoursOptions } from 'src/legacy/core/Parcours'
import Calculatrice from 'src/legacy/outils/calculatrice/Calculatrice'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import { convertGraphBack, idsV1FromIdsV2 } from 'src/lib/core/convert2To1'
import loadJqueryDialog from 'src/lib/core/loadJqueryDialog'
import Result, { ResultValues } from 'src/lib/entities/Result'
import { getDelaySec } from 'src/lib/utils/date'
import { addElement, empty, loadJs } from 'src/lib/utils/dom/main'

import type Pathway from 'src/lib/entities/Pathway'
import type { LegacyResultat, LegacyResultatCallback, LegacySection, LegacySectionMainFunction } from 'src/lib/types.d'

/**
 * Un wrapper pour lancer une section v1 au milieu d’un graphe v2
 * Équivalent du player.runOneNode() dans le cas d’une section v1 (appelé par lui)
 */
class LegacySectionRunner {
  /** Le nom de la section v1 */
  private readonly sectionName: string
  /** La fct principale de la section v1 (son export par défaut) */
  private readonly mainFn: LegacySectionMainFunction
  /** Les outils exportés par la section */
  private readonly outils: string[]
  /** Objet phrasesEtat construit à partir de params.pe de la section v1 */
  private readonly phrasesEtat

  constructor (sectionName: string, module: LegacySection) {
    if (typeof module.default !== 'function') throw Error(`module invalide pour la section ${sectionName}`)
    this.sectionName = sectionName
    this.mainFn = module.default
    // il faut coller ça comme propriété de mainFn pour que Parcours l'utilise
    if (module.upgradeParametres) this.mainFn.upgradeParametres = module.upgradeParametres
    const params = module.params
    this.outils = params.outils ?? []
    this.mainFn.parametres = params.parametres ?? []
    // idem j3pLoad v1 qui fait ça pour toutes les sections
    this.phrasesEtat = this.mainFn.phrasesEtat = getPhrasesEtat(params)
    // c’est init qui fera le reste
  }

  /**
   * Vérifie la cohérence de la pe (si c'est une valeur on retourne la clé)
   */
  checkPe (pe: string): string {
    if (typeof pe !== 'string') {
      console.error(Error(`pe invalide : ${typeof pe} pe`))
      return ''
    }
    if (pe in this.phrasesEtat) return pe
    for (const [k, v] of Object.entries(this.phrasesEtat)) {
      if (v === pe) return k
    }
    console.error(Error(`pe invalide : ${pe}, pas dans la liste ${this.phrasesEtat.join(' | ')}`))
    return ''
  }

  /**
   * Lance la section v1 passée au constructeur dans le conteneur du playground
   * @param {HTMLElement} container
   * @param {Pathway} pathway
   * @return {Promise<Result>}
   */
  async start (container: HTMLElement, pathway: Pathway): Promise<Result> {
    // faut charger d’office la même chose que j3pLoad
    const promises: Promise<unknown>[] = [loadJqueryDialog()]
    const addLoading = (url: string) => promises.push(loadJs(url))
    let mepcalculatrice: Calculatrice
    for (const outil of this.outils) {
      switch (outil) {
        case 'calculatrice': {
          const p = import('src/legacy/outils/calculatrice/Calculatrice')
            .then(({ default: Calculatrice }) => {
              // sera ajouté en propriété de l’objet global j3p quand il aura été créé
              mepcalculatrice = new Calculatrice({ nbdecimales: 5 })
            })
          promises.push(p)
          break
        }
        case 'iep':
          addLoading('https://instrumenpoche.sesamath.net/iep/js/iepLoad.min.js')
          break
        case 'mathquill':
          console.warn(`La section ${this.sectionName} utilise encore mathquill, il faut la migrer vers mathlive`)
          promises.push(import('src/lib/mathquill/loadMathquill.js').then(({ default: loadMathquill }) => loadMathquill()))
          break
        case 'three':
          addLoading(`${j3pBaseUrl}externals/three/three.min.js`)
          break
        default:
          console.error(`Outil ${outil} ignoré, inutile de le déclarer dans la section ${this.sectionName}`)
      }
    }
    await Promise.all(promises)

    empty(container)
    container.style.position = 'relative'
    const divErrors = addElement(container, 'div', { className: 'j3pErrors' })
    const v1Container = addElement(container, 'div', { className: 'j3pContainer', id: 'Mepact' })

    const { g: graphe } = await convertGraphBack(pathway.graph)
    const idsV1: Record<string, string> = idsV1FromIdsV2(pathway.graph)
    // il faudra résoudre la promesse retournée lors de l’appel de resultatCallback
    // => il faut du new Promise() pour appeler son resolve dans la callback
    return new Promise((resolve, reject) => {
      /**
       * La fct passée en options à Parcours (qui sera wrappée dans parcours.resultatCallback),
       * qui va résoudre ou rejeter la promesse.
       */
      const resultatCallback: LegacyResultatCallback = (legacyResultat: LegacyResultat) => {
        const bilan = legacyResultat.contenu.bilans.at(-1)
        if (bilan == null) return reject(Error('bilan manquant'))
        let evaluation = ''
        if (bilan.pe) {
          evaluation = this.checkPe(bilan.pe)
          // on râle pas si ça a changé car c'est trop fréquent…
        }
        const { id, score, duree } = bilan
        const result = new Result({ id, evaluation, score: score ?? 1, duree })
        if (pathway.isDebug) {
          console.debug(`end sectionV1 ${this.sectionName} avec le result`, result, 'issu de', legacyResultat)
        }
        isResultatSent = true
        resolve(result)
      }

      let isResultatSent = false
      let parcours: Parcours
      const parcoursOptions: ParcoursOptions = {
        graphe,
        isDebug: pathway.isDebug,
        // FIXME en l’état ça semble impossible de reconvertir un Resultat en LastResultat (on perd boucle et boucleGraphe qui n’ont plus lieu d'être)
        // voir si on peut le déduire du parcours fait, ou alors modifier Parcours pour qu'il digère un Resultat
        // lastResultat
        resultatCallback,
        skipAutoInit: true
      }
      try {
        // console.debug('wrapper v1 avec', parcoursOptions)
        parcours = new Parcours(v1Container, 'Mep', parcoursOptions)
        // Parce que ce parcours est tout neuf alors qu’il s’agit peut-être de la nième section du parcours, on met à jour cette valeur
        // (c'est pas indexInitial, juste la nième exécution dans le graphe courant,
        // de sections différentes ou pas)
        parcours.indexProgression = pathway.results?.length ?? 1
        if (pathway.persistentStorage != null) parcours.donneesPersistantes = pathway.persistentStorage
        if (mepcalculatrice) parcours.mepcalculatrice = mepcalculatrice
        // il faut lui ajouter les sections, on veut pas le faire dans son prototype, seulement sur cette instance
        const propSup = { [`Section${this.sectionName}`]: this.mainFn }
        Object.assign(parcours, propSup)
        // faut le faire pour toutes les autres sections du graphe
        const dummyFn = () => undefined
        dummyFn.parametres = {}
        for (const node of Object.values(pathway.graph.nodes)) {
          if (node.section && node.section !== this.sectionName) {
            Object.assign(parcours, { [`Section${node.section}`]: dummyFn })
          }
        }

        // et il faut aussi surcharger ça car y’a des sections qui l’appelle directement (la section Choix par ex)
        parcours.sectionSuivante = () => {
          // Si Parcours a appelé resultatCallback dans finNavigation, la promesse est déjà résolue
          // => rien à faire
          if (isResultatSent) {
            return
          }

          // …mais c'est pas toujours le cas, d'où le code qui suit
          const resultValue: ResultValues = {
            id: pathway.currentNodeId,
            score: (parcours.score != null && Number.isFinite(parcours.score)) ? parcours.score / (parcours.donneesSection?.nbitems ?? 1) : -1,
            duree: getDelaySec(startedAtMs)
          }
          if (parcours.parcours.pe) resultValue.evaluation = this.checkPe(parcours.parcours.pe)
          resultValue.nextAuto = true
          resolve(new Result(resultValue))
        }

        window.j3p = parcours

        // il faut convertir notre nodeId en index
        const initialId = idsV1[`${pathway.currentNodeId}`]
        if (initialId == null) throw Error(`pas trouvé l’id v1 de ${pathway.currentNodeId}`)
        const indexInitial = parcours.getIndexFromId(initialId)
        if (indexInitial < 0) throw Error(`Aucun nœud d’index ${initialId}`)
        // et on peut le lancer
        parcours.init(indexInitial)
        const startedAtMs = Date.now()
      } catch (error) {
        console.error(error)
        const content = (error instanceof Error) ? error.message : 'Erreur interne'
        addElement(divErrors, 'p', { content })
      }
    })
  }
}

export default LegacySectionRunner
