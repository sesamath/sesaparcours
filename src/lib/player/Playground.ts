import { MathfieldElement, renderMathInElement } from 'mathlive'

import textesGeneriques from 'src/lib/core/textes'
import DomEltCheckboxes, { type DomEltCheckboxesValues } from 'src/lib/entities/dom/DomEltCheckboxes'
import DomEltInput, { type DomEltInputValues } from 'src/lib/entities/dom/DomEltInput'
import DomEltMathlive, { type IDomEltMathliveValues } from 'src/lib/entities/dom/DomEltMathlive'
import DomEltRadios, { type DomEltRadiosValues } from 'src/lib/entities/dom/DomEltRadios'
import DomEltSelect, { type DomEltSelectValues } from 'src/lib/entities/dom/DomEltSelect'
import DomEltSpan from 'src/lib/entities/dom/DomEltSpan'
import InputMtg, { type IDomEltMtgValues } from 'src/lib/entities/dom/InputMtg'
import Modal, { type ModalOptions } from 'src/lib/entities/dom/Modal'
import SectionContext from 'src/lib/entities/SectionContext'
import { getMtgAppLecteurApi } from 'src/lib/outils/mathgraph'
import { hasLatex } from 'src/lib/outils/tableaux/main'

import type { DomEltTableValues, ItableDblEntry, ItableProp } from 'src/lib/outils/tableaux/tableMathlive'

import TableMathliveDblEntry from 'src/lib/outils/tableaux/TableMathliveDblEntry'
import TableMathliveProp from 'src/lib/outils/tableaux/TableMathliveProp'
import type { Item, ItemValue } from 'src/lib/player/types'

import { InputWithKeyboard } from 'src/lib/types'
import { freeze } from 'src/lib/utils/dom/freeze'
import { addElement, empty } from 'src/lib/utils/dom/main'
import { stringify } from 'src/lib/utils/object'
// On gère du mathlive pour les inputs et le rendu des feedback
import VirtualKeyboard from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'

import './playground.scss'

const timeoutMsg = textesGeneriques.tempsDepasse

const renderMath = (elt: HTMLElement): void => renderMathInElement(elt, {
  TeX: {
    delimiters: {
      display: [['$$', '$$']], inline: [['$', '$']]
    }
  }
})

export const btnLabels = {
  validation: 'OK',
  nextQuestion: 'Question suivante',
  nextSection: 'Section suivante'
}

// on définit ces types d’après l’objet précédent
// type ValueOf<T> = T[keyof T]
type BtnLabels = typeof btnLabels
type BtnLabel = keyof BtnLabels

type ZoneName = 'work' | 'solution' | 'feedback' | 'status'

interface DialogOptions extends ModalOptions {
  /** Passer true pour que la fermeture de la boîte de dialogue actionne également le bouton courant du playground (ok ou next) */
  autoClickOnClose?: boolean
}

interface IDisplayBilan {
  title?: string
}

// idem DomEltXxxValues avec type fixé en plus
interface IDisplayParamCheckboxes extends DomEltCheckboxesValues {
  type: 'checkboxes'
}

interface IDisplayParamInput extends DomEltInputValues {
  type: 'input'
}

interface IDisplayParamRadios extends DomEltRadiosValues {
  type: 'radios'
}

interface IDisplayParamSelect extends DomEltSelectValues {
  type: 'select'
}

// @todo lui n’est pas un elt de base, il ne devrait donc pas être géré par displayXxx (et donc plus besoin de ce type IDisplayParamTable)
interface IDisplayParamTable extends DomEltTableValues {
  type: 'table'
}

/** Options à passer à reset() */
interface IResetOptions {
  butPersistent?: boolean
  feedback?: boolean
  solution?: boolean
  status?: boolean
  work?: boolean
}

interface IDisplayParamMathlive extends IDomEltMathliveValues {
  type: 'mathlive'
}

interface IDisplayParamMtg extends IDomEltMtgValues {
  type: 'mathgraph'
}

// Les différents formats de params possibles pour tous nos types de base (lib/entities)
// Pour les autres (tout ce qui est dans lib/outils), c’est la section qui doit importer
// l’outil et utiliser ses méthodes pour ajouter des trucs dans le DOM sans passer par
// les méthodes génériques displayXxx
type IDisplayParam =
  IDisplayParamCheckboxes
  | IDisplayParamInput
  | IDisplayParamMathlive
  | IDisplayParamMtg
  | IDisplayParamRadios
  | IDisplayParamSelect
  | IDisplayParamTable
  | number
  | string

type IDisplayParams = Record<string, IDisplayParam>

interface IDisplayOptions {
  persistent?: boolean
  className?: string
  asBlock?: boolean
}

// type BtnLabelKeys = keyof BtnLabels
// Une interface pour Playground.elements
interface IElement {
  parent: HTMLElement
  element: HTMLElement
  persistent: boolean
}

class Playground {
  private readonly container: HTMLElement
  /** Les éléments que l’élève utilise pour répondre (inputs, figure mathgraph, etc.) */
  private items!: Record<string, Item>
  /** Les éléments à surveiller, pour les supprimer ou pas au reset, les geler, etc. */
  private elements!: Record<string, IElement>
  /** div du titre */
  private divTitle!: HTMLDivElement
  /** div pour l’avancement (score et nb de question) */
  private divStatus!: HTMLDivElement
  /** div avec l’énoncé et la ou les zones de réponse */
  private divWork!: HTMLDivElement
  /** div de commentaire global sur la réponse de l’élève */
  private divFeedback!: HTMLDivElement
  /** div pour afficher la solution */
  private divSolution!: HTMLDivElement
  /** div des boutons d’action (suite & co) */
  private divActions!: HTMLDivElement
  /** Pour l’affichage du temps restant */
  private divLimit!: HTMLDivElement | null
  /** span d’attente de chargement */
  private spanWaiting!: HTMLSpanElement
  /** span précisant qu’il faut valider autrement qu’avec un clic sur OK */
  private spanAlternateValidation!: HTMLSpanElement
  /** le bouton d’action (dont le texte change suivant le contexte) */
  private button!: HTMLButtonElement
  /** Les observers des éléments gelés */
  private freezers!: MutationObserver[]

  private constructor (container: HTMLElement) {
    if (!document.body.contains(container)) throw Error('Conteneur invalide')
    empty(container)
    this.container = container
    this.init()
  }

  /**
   * C’est par cette méthode qu’on instancie un playground
   * @param {HTMLElement} container
   */
  static async create (container: HTMLElement/* , options: Record<string, unknown> */): Promise<Playground> {
    return new Playground(container)
  }

  /**
   * Affiche un contenu dans la zone de travail
   * Par exemple, pour afficher un input ce serait
   * playground.displayWork('Un texte avec un input de nombres pairs %{q1}', { q1: { type: 'input', props: {type:
   * number, min: 2, max: 8, step: 2 } } })
   * @private
   * @param content Le contenu, où %{xxx} représente une variable xxx décrite dans les params
   * @param params La liste des déclarations des variables mises dans content
   * @param [options]
   * @param isBlock permet d'écrire le contenu dans un nouveau bloc (et donc forcer un retour à la ligne)
   * @param [options.persistent] Passer true pour que content soit conservé entre deux questions
   */
  async displayWork (content: string, params?: IDisplayParams, options?: IDisplayOptions, isBlock?: Boolean) {
    const container = (isBlock) ? addElement(this.divWork, 'div') : this.divWork
    await this.display(container, content, params, options)
  }

  /**
   * Affiche un contenu dans la zone solution
   * Par exemple, pour afficher une expression latex ce serait
   * playground.displaySolution('Un texte avec une expression latex %{corrige}', { corrige: { type: 'latex', props:
   * {type: number, min: 2, max: 8, step: 2 } } })
   * @private
   * @param content Le contenu, où %{xxx} représente une variable xxx décrite dans les params
   * @param params La liste des déclarations des variables mises dans content
   * @param displayOptions essentiellement le boolean persistent, mais on aura peut-être d’autres propriétés à passer
   *     comme frozen
   */
  async displaySolution (content: string, params: IDisplayParams, displayOptions: IDisplayOptions) {
    this.show(this.divSolution)
    await this.display(this.divSolution, content, params, displayOptions)
  }

  /**
   * Affiche le Bilan dans une modale
   * @param {string} content
   * @param {IDisplayBilan} params
   * @return {Promise<unknown>}
   */
  async displayBilan (content: string, params: IDisplayBilan): Promise<void> {
    return new Promise(resolve => {
      const modal = new Modal({
        closeWithOk: true, onClose: () => resolve()
      })
      if (content !== '') {
        if (params.title !== '') {
          modal.addElement('h3', { textContent: params.title })
        }
        // Pour pouvoir mettre en page le bilan via dialog, je modifie ce textContent en innerHTML. (J-C le 7/04/2024)
        modal.addElement('span', { innerHTML: content })
        modal.show()
      }
    })
  }

  /**
   * Retourne le conteneur demandé (attention, ce qui sera ajouté dedans ne sera pas persistant)
   * @param zone
   */
  getContainer (zone?: ZoneName): HTMLElement {
    if (zone) {
      switch (zone) {
        case 'feedback':
          return this.divFeedback
        case 'solution':
          return this.divSolution
        case 'work':
          return this.divWork
        case 'status':
          return this.divStatus
        default:
          throw Error(`${zone} n’est pas une zone modifiable`)
      }
    }
    return this.container
  }

  /**
   * retourne la valeur d’un input
   * @param {string} name
   */
  async getItemValue (name: string): Promise<ItemValue> {
    const item = this.items[name]
    if (item == null) throw Error(`Le nom ${name} ne correspond à aucun item`)
    return item.getValue()
  }

  /**
   * Retourne true si tous les champs ont été complétés (qq chose dans chaque input, les radios/checkboxes cochés,
   * etc.)
   */
  async isAnswered (): Promise<boolean> {
    for (const item of Object.values(this.items)) {
      const itemAnswered = item.isAnswered()
      if (itemAnswered instanceof Promise) {
        if (!await itemAnswered) return false
      } else if (!itemAnswered) {
        return false
      }
    }
    return true
  }

  /**
   * Affiche une boîte de dialogue
   * @param message
   * @param title
   * @param [options]
   */
  async dialog (message: string, title: string = '', {
    inset, maxWidth, maxHeight, zIndex, fullScreen, delay,
    autoClickOnClose
  }: DialogOptions = {}): Promise<void> {
    return new Promise(resolve => {
      const onClose = () => {
        if (autoClickOnClose) this.button.click()
        resolve()
      }
      const modal = new Modal({ title, inset, maxWidth, maxHeight, zIndex, fullScreen, delay, onClose })
      if (title !== '') {
        modal.addElement('h3', { textContent: title })
      }
      if (message !== '') {
        // Pour pouvoir mettre en page le bilan via dialog, je modifie ce textContent en innerHTML. (J-C le 7/04/2024)
        // @todo remettre ça en texte simple et faire le innerHTML chez l’appelant (qui récupère le retour d’un addElement pour y faire ce qu’il veut avec)
        modal.addElement('span', { innerHTML: message })
        modal.show()
      }
    })
  }

  /**
   * Reconstruit le DOM (nettoie les zones sélectionnées, sans toucher aux éléments persistants
   * supprime des éléments en fonction des options passées
   * si options n’est pas précisé on vide tout
   * si options.butPersistent = true => on n’éfface pas les contenus persistants
   * work: true => on efface dans le divWork
   * solution: true => on efface dans le divSolution ...
   * avec `{work: true, solution: true, butPersistent: true}` on efface tout ce qui n’est pas persistent dans divWork et divSolution
   * @param {IResetOptions} options
   */
  reset (options: IResetOptions = {}): void {
    // on vérifie d’abord que notre dom est correct (après un passage en v1 ça peut être tout cassé)
    const divs = [this.divActions, this.divFeedback, this.divSolution, this.divStatus, this.divTitle, this.divWork]
    const isInCt = (div: HTMLElement) => this.container.contains(div)
    if (!divs.every(isInCt)) this.init()
    // ok, on continue
    this.unFreeze()
    if (!Object.keys(options).length) {
      empty(this.divFeedback)
      empty(this.divStatus)
      empty(this.divSolution)
      empty(this.divWork)
      this.items = {}
      this.elements = {}
      // et on affiche le message d’attente
      this.showWaiting()
    } else {
      // faut regarder les options
      const butPersistent = Boolean(options.butPersistent)
      const elements = Object.entries(this.elements)
      for (const [name, elementRecord] of elements) {
        const parent = elementRecord.parent
        const element = elementRecord.element
        const persistent = elementRecord.persistent
        if ((parent === this.divWork && options.work) || (parent === this.divSolution && options.solution) || (parent === this.divStatus && options.status) || (parent === this.divFeedback && options.feedback)) {
          if (butPersistent && persistent) continue // faut pas l’effacer
          if (butPersistent) {
            // on ne retire que cet élément non persistant
            const checkContent = parent.contains(element)
            if (checkContent) parent.removeChild(element)
            // éléments.
          } else {
            // On nettoie tout dans le parent, ça permet de supprimer les <p> et autres
            empty(parent)
            // on vient de potentiellement virer d’autres items du DOM
            // mais c’est pas gênant (ça fera juste un empty() d’un parent déjà vide pour ces items)
          }
          // on supprime la propriété qui nous a amené ici pour nettoyer this.elements
          if (this.items[name] != null) delete this.items[name]
          delete this.elements[name]
        }
      }
    }
    // on masque feedback et solution au départ
    this.hide(this.divFeedback)
    this.hide(this.divSolution)
  }

  setFocus (itemName: string) {
    const item = this.items[itemName]
    if (item != null) item.setFocus()
  }

  /**
   * Affiche le titre de l’exo (texte simple pour le moment, LaTeX à venir)
   * @param title
   */
  setTitle (title: string) {
    empty(this.divTitle)
    // Pour avoir du latex il suffit de l’encadrer par deux $ pour du 'inline' et par deux $$ pour du display
    addElement(this.divTitle, 'h1', { content: title })
    if (title.includes('$')) renderMath(this.divTitle)
  }

  /**
   * Affiche un feedback
   * @param feedback
   * @param options
   * @param [options.ok] passer true pour styler le feedback ok, et false pour ko (laisser undefined pour garder la couleur "normale")
   */
  async showFeedback (feedback: string, { ok, withoutListener }: {
    ok?: boolean,
    withoutListener?: boolean
  } = {}): Promise<void> {
    // on a peut-être été lancé par un event click, qui va se propager ensuite au container
    // on rend donc la main aussitôt et exécute ensuite notre code dans la promesse retournée
    // faut mettre ça dans un setTimeout, sinon on est viré d’office par
    // l’event click qui nous a lancé (et qui remonte ensuite au container)
    return new Promise((resolve) => {
      const className = ok == null ? '' : ok ? 'ok' : 'ko'
      empty(this.divFeedback)
      this.show(this.divFeedback)
      this.display(this.divFeedback, feedback, {}, { className })
        .then(() => {
          if (feedback.includes('$')) renderMath(this.divFeedback)
          if (withoutListener) {
            resolve()
          } else {
            setTimeout(() => {
              this.container.addEventListener('click', () => {
                this.hide(this.divFeedback)
                resolve()
              }, { once: true })
            }, 0)
          }
        })
    })
  }

  /**
   * La méthode pour mettre un feedback sur une partie d’item (une case à cocher ou un bouton radio)
   * Si l’index n’est pas fourni, c’est le feedback général qui est utilisé (à la fin).
   * @param item
   * @param isOk
   * @param feedback
   * @param index L’index de la case à cochée concernée
   */
  async setFeedbackItem (item: string, isOk: boolean, feedback: string, index?: number) {
    const elt = this.items[item]
    if (elt != null) {
      if (typeof index === 'number' && (elt instanceof DomEltCheckboxes || elt instanceof DomEltRadios)) {
        elt.setFeedback(isOk, feedback, index)
      } else {
        elt.setFeedback(isOk, feedback)
      }
    }
  }

  /**
   * Affiche l’état courant (question N sur T, score…)
   */
  async showStatus (sectionContext: SectionContext): Promise<void> {
    empty(this.divStatus)

    const { question, step, score, nbSteps, nbQuestions, pathway, params } = sectionContext
    const sectionCounter = Object.values(pathway.nbRuns).reduce((nb: number, accumulator) => nb + accumulator, 0) + 1
    let status = ''
    status += `Question ${question}`
    // ajout du suffixe éventuel pour l’étape
    if (nbSteps > 1) status += String.fromCharCode(96 + step) + (step < 27 ? '' : String(step - 26)) // à partir de 27 on passe à z2, z3…
    // le nb de questions
    status += ` / ${nbQuestions}`
    if (sectionCounter > 1) status += ` (section ${sectionCounter})`
    addElement(this.divStatus, 'p', { content: status })
    // Le timer éventuel
    if (params?.limite) {
      if (this.divLimit) {
        // y’en avait déjà un qu’on a viré du dom avec empty, faut le remettre
        this.divStatus.appendChild(this.divLimit)
      } else {
        // init du timer
        this.divLimit = addElement(this.divStatus, 'div')
        this.updateLimitBar(params.limite, 0)
      }
    } else {
      this.divLimit = null
    }
    // puis le score, mais certaines sections ne gèrent pas de score et retourne -1
    if (score >= 0) {
      addElement(this.divStatus, 'p', { content: `Score : ${score} / ${nbQuestions}` })
    }
  }

  unFreeze () {
    for (const freezer of this.freezers) freezer.disconnect()
    this.freezers = []
    // et faut remettre ces opacités à 1
    this.divWork.style.opacity = '1'
    this.divSolution.style.opacity = '1'
  }

  /**
   * Met à jour la zone de status sur le temps limité restant (texte + rectangle coloré)
   * @param {number} limit
   * @param {number} elapsed
   */
  updateLimitBar (limit: number, elapsed: number): void {
    if (this.divLimit == null) throw Error('La barre de temps limité n’existe pas, impossible de la mettre à jour')
    empty(this.divLimit)
    const rest = Math.round(limit - elapsed)
    const isTimeout = rest <= 0
    const textContent = isTimeout
      ? timeoutMsg
      : `Il reste ${rest} seconde${rest > 1 ? 's' : ''}`
    const className = isTimeout ? 'ko' : ''
    const progressPc = Math.round(rest / limit * 100)
    // on veut passer de 120° (vert) à 0° (rouge) entre 100% et 20%
    const backgroundColor = `hsl(${Math.round((progressPc - 20) * 1.25)} 90 40)`
    addElement(this.divLimit, 'p', { textContent, className })
    addElement(this.divLimit, 'p', { style: { backgroundColor, height: '2px', width: `${progressPc}%` } })
  }

  /**
   * Masque l’attente, modifie le contenu du bouton d’action et attends le click dessus
   * (ça ré-affichera l’attente en attendant qu’on demande un autre bouton)
   * @param labelName
   */
  async waitForClick (labelName: BtnLabel): Promise<void> {
    return new Promise((resolve, reject) => {
      // le bouton prend le label ok et résoudra la promesse quand il sera cliqué
      if (!this.button) return reject(Error('Le bouton n’existe pas'))
      const label = btnLabels[labelName]
      if (label == null) return reject(Error(`Nom de label ${labelName} inconnu`))
      this.button.innerText = label
      this.button.className = labelName
      this.button.onclick = () => {
        this.showWaiting()
        resolve()
      }
      // et on peut l’afficher (seul)
      this.hide(this.spanAlternateValidation)
      this.hide(this.spanWaiting)
      this.show(this.button)
    })
  }

  showAlternateValidation () {
    this.hide(this.spanWaiting)
    this.hide(this.button)
    this.show(this.spanAlternateValidation)
  }

  /**
   * Gèle divWork et divSolution
   */
  freeze () {
    this.freezers.push(freeze(this.divWork))
    this.freezers.push(freeze(this.divSolution))
  }

  /**
   * Affiche le spinner d’attente, appeler waitForClick() ou showAlternateValidation() ou hideWaiting() simplement pour le cacher
   * pour le remplacer par autre chose
   */
  showWaiting (): void {
    this.hide(this.button)
    this.hide(this.spanAlternateValidation)
    this.show(this.spanWaiting)
  }

  hideWaiting (): void {
    this.hide(this.spanWaiting)
  }

  /**
   * Retourne un nom unique pour un item
   * @param prefix
   * @protected
   */
  protected getItemName (prefix: string) {
    let i = 1
    while (`${prefix}${i}` in this.items) i++
    return `${prefix}${i}`
  }

  /**
   * Ajoute des checkboxes dans la zone de travail (énoncé / réponses)
   * @param name
   * @param choices
   */
  private addCheckboxes ({
    container,
    choices,
    inputProps,
    labelProps,
    verticalBlock = false,
    persistent
  }: DomEltCheckboxesValues) {
    if (!container) container = this.divWork
    // si y’a pas de name on en génère un
    if (!inputProps) inputProps = {}
    if (!inputProps.name) inputProps.name = this.getItemName('checkboxes')
    const item = DomEltCheckboxes.create(container, { choices, inputProps, labelProps, verticalBlock })
    // faut laisser this.divWork en parent (pas container)
    this.elements[inputProps.name] = { parent: this.divWork, element: item.container, persistent: persistent ?? false }
    this.addItem(inputProps.name, item)
  }

  /**
   * Ajoute un input simple dans la zone de travail et y attache le VirtualKeyboard
   */
  private addInput ({
    container,
    label,
    inputProps,
    labelProps,
    persistent = false,
    restriction,
    kbRestriction
  }: DomEltInputValues): void {
    // si y’a pas de name on en génère un
    if (!inputProps) inputProps = {}
    if (!inputProps.name) inputProps.name = this.getItemName('input')
    if (!container) container = this.divWork
    const input: DomEltInput = DomEltInput.create(container, { label, inputProps, labelProps, restriction })
    const elt = input.element as InputWithKeyboard
    elt.addEventListener('keydown', (e: KeyboardEvent) => {
      if (e.key === 'Enter') {
        const elt = e.target
        if (elt != null) {
          this.button.click()
        }
      }
    })
    // les restrictions sur un input peuvent être des regexp, des noms de restriction connues ou des fonctions,
    // mais VirtualKeyboard veut une RegExp
    let restric = kbRestriction
    if (!restric) {
      if (restriction instanceof RegExp) {
        restric = restriction
      } else if (typeof restriction === 'string') {
        restric = DomEltInput.getRestriction(restriction)
      }
    }
    if (!restric) {
      console.warn(Error('addInput sans RegExp de restriction à passer à VirtualKeyboard (si restriction est une fonction il faut préciser kbRestriction'))
      restric = /./
    }

    elt.virtualKeyboard = new VirtualKeyboard(elt, restric, {
      acceptSpecialChars: false
    })
    this.elements[inputProps.name] = { parent: this.divWork, element: input.container, persistent }
    return this.addItem(inputProps.name, input)
  }

  /**
   * Ajoute un DomElt à notre liste d’items (à priori des zones de saisie)
   * @param name
   * @param item
   */
  private addItem (name: string, item: Item) {
    if (this.items[name]) throw Error(`Il y a déjà un item ${name}`)
    this.items[name] = item
  }

  /**
   * Ajoute un ou plusieurs input Mathlive, et y attache un VirtualKeyboard
   * Exemple 1 : On veut un simple input de type MathfieldElement, vide, avec son clavier virtuel ->
   * await playground.displayWork('Écrire la formule après les deux points : %{formule}', {formule: {value: ''}},{})
   * Exemple 2 : On veut un input de type FillInTheBlank avec 2 (ou plus) placeholders ->
   * await playground.displayWork('Donne une écriture fractionnaire de 0.25 : %{fraction}', {fraction: {value: '\\frac{%{num}}{%{den}}'}}, {})
   * %{variable} contenu dans value.
   */
  private async addMathlive ({ inputProps, options }: { inputProps: IDisplayParamMathlive, options: IDisplayOptions }) {
    const container = inputProps.container ?? this.divWork
    const name = inputProps.name ?? this.getItemName('mfe')
    // Les deux propriétés qui suivent sont pour choisir les touches du clavier. Voir src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard.js
    const restriction: RegExp = inputProps.restriction ?? /./
    const commands: string[] = inputProps.commands ?? []
    const persistent = options.persistent ?? false
    const value = inputProps.value
    const inputMl = DomEltMathlive.create(container, {
      persistent,
      value,
      restriction,
      commands,
      name
    })
    inputMl.element.addEventListener('change', () => {
      // Les mathfieldElement interceptent la touche 'Enter' du clavier, et dispatchent un event 'change' ce qui équivaut à valider
      this.button.click()
    })
    this.elements[name] = { parent: this.divWork, element: inputMl.element, persistent } // avant, j'avais fait pointer element sur le container et ça posait un soucis pour le focus()
    this.addItem(name, inputMl)
  }

  /**
   * Ajoute une figure MtgAppLecteurApi et crée un item pour chaque calcul identifié par %{variable}
   * @param {HTMLElement} parent
   * @param  props
   */
  private async addMtg (parent: HTMLElement, { props }: { props: IDisplayParamMtg }) {
    const persistent = Boolean(props.persistent)
    if (!props.name) props.name = this.getItemName('mtg')
    const svgOptions = props.svgOptions ?? {}
    const mtgOptions = props.mtgOptions ?? {}
    Object.assign(svgOptions, 'isSvg' in svgOptions ? {} : { idSvg: props.name })
    const divMtg = addElement(parent, 'div')
    const name = props.name as string
    this.elements[name] = { parent, element: divMtg, persistent }
    const mtgAppLecteurApi = await getMtgAppLecteurApi(divMtg, svgOptions, mtgOptions)
    const content = props.value ?? ''
    const variables = content.match(/%\{([^}]+)}/mg)
    if (!variables) {
      throw Error(`addMtg : on n’a pas de variable, donc la figure ne possède pas de calcul ? l’objet fourni est ${stringify(props)}`)
    }
    let spanFeedbackIsCreated = false
    for (const variable of variables) {
      const name = variable.substring(2, variable.length - 1)
      if (name == null) throw Error(`Définition de ${name} manquante`)
      if (name in props.inputProps) {
        const initialValue = props.inputProps[name]?.initialValue ?? 0
        const input = InputMtg.create({ mtgAppLecteurApi, name, initialValue, divMtg, persistent })
        if (input != null && !spanFeedbackIsCreated) {
          const spanFeedback = DomEltSpan.create(parent, { content: '', persistent: false, className: 'spanFeedback' })
          this.addItem(`${props.name}.spanFeedback`, spanFeedback)
          this.elements[`${props.name}.spanFeedback`] = {
            parent: this.divWork, element: spanFeedback.container, persistent: false
          }
          spanFeedback.container.className = 'feedback'
          input.spanFeedback = spanFeedback.container
          parent.insertBefore(input.spanFeedback, divMtg)
          spanFeedbackIsCreated = true
        }
        this.addItem(name, input)
      } else throw Error(`addMtg : J’ai une variable du nom de ${name} mais pas de propriété correspondante dans inputProps.`)
    }
    parent.appendChild(divMtg)
  }

  /**
   * Ajoute une liste de boutons radio avec leur label respectif à la liste d’items
   */
  private addRadios ({ container, choices, inputProps, labelProps, verticalBlock, persistent = false }: DomEltRadiosValues) {
    if (!container) container = this.divWork
    // si y’a pas de name on en génère un
    if (!inputProps?.name) {
      const name = this.getItemName('radios')
      inputProps = Object.assign({}, inputProps, { name })
    }
    const radios = DomEltRadios.create(container, { choices, inputProps, labelProps, verticalBlock })
    this.elements[inputProps.name] = { parent: this.divWork, element: radios.container, persistent }
    this.addItem(inputProps.name, radios)
  }

  /**
   * Ajoute une liste de sélection avec leur label respectif à la liste d’items
   */
  private addSelect ({ container, choices, inputProps, persistent = false }: DomEltSelectValues) {
    if (!container) container = this.divWork
    if (!inputProps?.name) {
      const name = this.getItemName('select')
      inputProps = Object.assign({}, inputProps, { name })
    }
    const select = DomEltSelect.create(container, { choices, inputProps })
    this.elements[inputProps.name] = { parent: this.divWork, element: select.container, persistent }
    this.addItem(inputProps.name, select)
  }

  private addTable (parent: HTMLElement, { tableau, inputProps, persistent = false }: DomEltTableValues) {
    if (!inputProps?.name) {
      const name = this.getItemName('table')
      inputProps = Object.assign({}, inputProps, { name })
    }
    const table = inputProps.sousType === 'prop'
      ? TableMathliveProp.create(inputProps.name, tableau as ItableProp, { className: inputProps.classes ?? '' }) // tableau
      : TableMathliveDblEntry.create(inputProps.name, tableau as ItableDblEntry, { className: inputProps.classes ?? '' })
    // style
    // double
    // entrée
    parent.appendChild(table.container)
    const mfes = table.container.querySelectorAll('math-field')
    for (const mfe of mfes) {
      mfe.addEventListener('keydown', (e) => {
        const event = e as KeyboardEvent
        if (event.key === 'Enter') this.button.click()
      }
      )
    }

    this.elements[inputProps.name] = { parent: this.divWork, element: table.container, persistent }
    this.addItem(inputProps.name, table)
  }

  /**
   * Ajoute un <span>texte</span> au div parent et retourne un pointeur sur l’élément <span>
   * @param {HTMLElement} parent
   * @param args
   * @param {string} args.texte
   * @param {boolean} args.persistent
   */
  private addTextElement (parent: HTMLElement, { texte = '', persistent = false, className = '' }: {
    texte: string,
    persistent: boolean,
    className?: string
  }) {
    const name = this.getItemName('texte')
    const textSpan = DomEltSpan.create(parent, { content: texte, persistent, className })
    this.elements[name] = { parent, element: textSpan.container, persistent }
    this.addItem(name, textSpan)
  }

  /**
   * Reset complet du playground (reconstruit tout le html)
   * @protected
   */
  private init () {
    if (!document.body.contains(this.container)) throw Error('Plus de conteneur html, impossible de continuer')
    empty(this.container)
    // reset css complet du container, au cas où qqun lui aurait appliqué des trucs directement
    this.container.setAttribute('style', '')
    for (const styleProp of Object.keys(this.container.style)) {
      // ces deux là sont readonly, au cas où
      if (styleProp === 'length' || styleProp === 'parentRule') continue
      try {
        // ça pourrait planter…
        this.container.style.removeProperty(styleProp)
      } catch (error) {
        console.error(error)
      }
    }
    // et on met notre classe (à l’exclusion de toute autre)
    this.container.className = 'playground'
    // on peut ajouter les enfants
    const header = addElement(this.container, 'div', { className: 'header' })
    this.divTitle = addElement(header, 'div', { className: 'zoneTitle' })
    this.divStatus = addElement(header, 'div', { className: 'zoneStatus' })
    const mainContent = addElement(this.container, 'div', { className: 'mainContent' })
    this.divWork = addElement(mainContent, 'div', { className: 'zoneWork' })
    this.divSolution = addElement(mainContent, 'div', { className: 'zoneSolution' })
    // le bloc flottant actions & feedback
    const divActionFeedback = addElement(mainContent, 'div', { className: 'actionFeedback' })
    this.divFeedback = addElement(divActionFeedback, 'div', { className: 'zoneFeedback' })
    this.divActions = addElement(divActionFeedback, 'div', { className: 'zoneActions' })
    this.spanWaiting = addElement(this.divActions, 'span', { className: 'zoneWaiting', textContent: '…' })
    this.spanAlternateValidation = addElement(this.divActions, 'span', {
      className: 'masked',
      textContent: 'Lis la consigne pour valider cette question'
    })
    this.items = {}
    this.elements = {}
    this.button = addElement(this.divActions, 'button')
    this.showWaiting()
    // clue (ex indication en BG, à priori ce sera un picto qui va démasquer un rectangle au dessus ou sous
    // l’énoncé, pour pas prendre tout le temps un grand rectangle à l’écran, si y’a pas d’indication le picto
    // reste caché)
    this.freezers = []
  }

  /**
   * Méthode générique pour afficher un contenu (appelée par displayWork() ou displaySolution() par exemple)
   * Par exemple, pour afficher un input ce serait
   * display('Un texte avec un input de nombres pairs %{q1}', { q1: { type: 'input', props: {type: number, min: 2,
   * max: 8, step: 2 } } })
   * @private
   * @param parent
   * @param content Le contenu, où %{xxx} représente une variable xxx décrite dans les params
   * @param params La liste des déclarations des variables mises dans content
   * @param [options]
   * @param [options.persistent] Passer true pour que content soit conservé entre deux questions
   */
  private async display (parent: HTMLElement, content: string, params: IDisplayParams = {}, options: IDisplayOptions = {}) {
    // Si on voit passer du Latex (un $ dans content), on basculera à true
    let latexContent = false
    const enforceWork = (type: string): void => {
      if (parent !== this.divWork) throw Error(`On ne peut pas ajouter d’élement de type ${type} ailleurs que dans la zone de travail`)
    }
    // Ensuite on applique le découpage avec les marqueurs %{} pour chaque morceau
    const persistent = options.persistent ?? false
    const className = options.className
    // on ajoute un div dans parent si on veut du mode block
    const container = (options.asBlock) ? addElement(parent, 'div') : parent

    while (content) {
      if (content.includes('$')) {
        latexContent = true
      }
      // dans `.*?` le `?` sert à rendre "non gourmand" (non greedy) le * qui précède
      // (pour s’arrêter à la première occurrence de ce qui suit, et pas la dernière)
      const chunks = /^([^%]*?)%\{([^}]+)}(.*)$/s.exec(content)
      if (chunks) {
        const [, start, n, end] = chunks
        const name = n as string
        const def = params[name]
        if (def == null) {
          console.error(Error(`Définition de ${name} manquante`), params)
          await this.showFeedback(textesGeneriques.bug, { ok: false })
          // on laisse le contenu tel quel et on arrête là
          this.addTextElement(container, { texte: content, persistent })
          return
        }
        this.addTextElement(container, { texte: start as string, persistent })
        // Si le paramètre est de type string ou number, on l’ajoute directement au texte.
        if (typeof def === 'number') {
          this.addTextElement(container, { texte: String(def), persistent })
        } else if (typeof def === 'string') {
          if (def.includes('$')) {
            // possibilité offerte d’avoir du latex dans les variables...
            // mais ça donne un mathfieldElement du plus mauvais goût
            // en plein milieu du texte... Je pense que le renderMathInElement()
            // est mieux intégré et plus souhaitable.
            let propsContent = def
            while (propsContent) {
              const texChunks = /^([^$]*?)\$([^$]*?)\$(.*)$/s.exec(propsContent)
              if (texChunks) {
                const [, start, tex, end] = texChunks
                this.addTextElement(container, { texte: start as string, persistent })
                if (tex != null) {
                  const texString = tex
                  const mfe = new MathfieldElement()
                  mfe.setValue(texString)
                  container.appendChild(mfe)
                }
                propsContent = end ?? ''
              }
            }
          } else {
            this.addTextElement(container, { texte: def, persistent })
          }
        } else {
          switch (def.type) {
            case 'checkboxes':
              enforceWork(def.type)
              if (def?.choices == null) {
                throw Error('propriété choices manquante pour le type checkboxes')
              }
              this.addCheckboxes({
                container,
                choices: def.choices,
                verticalBlock: def.verticalBlock,
                inputProps: { ...def.inputProps, name },
                persistent
              })
              break
            case 'input': {
              enforceWork(def.type)
              this.addInput({
                container,
                label: def.label,
                labelProps: def.labelProps,
                inputProps: { ...def.inputProps, name },
                persistent,
                restriction: def.restriction,
                kbRestriction: def.kbRestriction
              })
              break
            }
            case 'mathlive':
              // à utiliser pour mettre du latex avec une ou plusieurs box de saisie pour du contenu mathématique ou autre.
              // Tout est regroupé au sein d’un input Mathlive dont on peut configurer le clavier comme
              // on veut pour du latex sans saisie, mettre directement le latex dans l’énoncé avec comme
              // délimiteurs : '$' et '$' pour du inline et '$$' et '$$' pour le mode display
              await this.addMathlive({ inputProps: { ...def, name }, options: { persistent: persistent ?? false } })
              break
            case 'mathgraph':
              await this.addMtg(container, { props: { ...def, name, persistent } })
              break
            case 'radios': // ajoute un input liste avec des boutons radios
              if (def?.choices == null) {
                throw Error('Zone.display() pas de propriété choices pour le type radios')
              }
              this.addRadios({ choices: def.choices, inputProps: { name }, persistent })
              break

            case 'select': // ajoute un input de type liste de sélection
              if (def?.choices == null) {
                throw Error('Zone.display() pas de propriété choices pour le type select')
              }
              this.addSelect({
                choices: def.choices, inputProps: { ...def.inputProps, name }, persistent
              })
              break
            case 'table': {
              if (!def.tableau) {
                throw Error('Vous n’avez pas fourni de tableau pour addTable()')
              }
              const sousType = def.inputProps?.sousType
              if (sousType !== 'prop' && sousType !== 'dblEntry') {
                throw Error('Vous n’avez pas donné un sousType de tableau correct !')
              }
              if (hasLatex(def.inputProps.sousType, def.tableau)) latexContent = true
              this.addTable(container, {
                tableau: def.tableau, inputProps: { ...def.inputProps, name }, persistent
              })
            }
              break
            default:
              console.error(Error(`type ${params.type} à implémenter`))
          }
        }
        content = end ?? ''
      } else {
        // pas de %{} dans content => du texte (avec éventuellement du latex dedans)
        this.addTextElement(container, { texte: content, persistent, className })
        content = ''
      }
    } // while content

    // et si on a vu passer du latex on le rend
    if (latexContent) renderMath(container)
  }

  private show (ct: HTMLElement) {
    ct.classList.remove('masked')
  }

  private hide (ct: HTMLElement) {
    ct.classList.add('masked')
  }
}

export default Playground
