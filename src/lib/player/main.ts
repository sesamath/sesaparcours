import textesGeneriques from 'src/lib/core/textes'
import type { PathwayValues } from 'src/lib/entities/Pathway'
import Pathway from 'src/lib/entities/Pathway'
import Player from 'src/lib/player/Player'
import Playground from 'src/lib/player/Playground'

const { parcoursTermine } = textesGeneriques

/**
 * Lance l’exécution d’un graphe
 */
async function play (container: HTMLElement, options: PathwayValues): Promise<void> {
  if (options?.graph == null) {
    throw Error('Il faut fournir un graphe')
  }
  const playground = await Playground.create(container)
  let pathway = new Pathway(options)

  // Si le graphe est déjà terminé, on affiche une boîte de dialogue pour informer l’élève
  // qu’il va recommencer (il peut fermer s’il ne veut pas recommencer)
  if (pathway.getCurrentNode().isEnd()) {
    await playground.dialog(parcoursTermine)
    // on repart sur un pathway vierge de tout historique
    const optsWithoutLastResultat = { ...options }
    delete optsWithoutLastResultat.lastResultat
    pathway = new Pathway(optsWithoutLastResultat)
  }

  // on peut lancer
  const player = new Player({ playground, pathway })
  await player.run()
}

export default play
