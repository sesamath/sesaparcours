import loadSection, { isLegacySection } from 'src/lib/core/loadSection'

import textesGeneriques from 'src/lib/core/textes'
import Modal from 'src/lib/entities/dom/Modal'
import Pathway from 'src/lib/entities/Pathway'
import Result from 'src/lib/entities/Result'
import SectionContext from 'src/lib/entities/SectionContext'
import type { PartialResult, Resultat, Section } from 'src/lib/types'
import { getDelaySec } from 'src/lib/utils/date'
import { getElement } from 'src/lib/utils/dom/main'
import { getMinutesSecondesLabel } from 'src/lib/utils/number'
import { getNonEmptyString } from 'src/lib/utils/string'
import Playground from './Playground'

const defaultFeedbackMissing = textesGeneriques.reponseIncomplete
const defaultFeedbackOk = textesGeneriques.cBien
const defaultFeedbackKo = textesGeneriques.cFaux
const timeoutMsg = textesGeneriques.tempsDepasse
const bug = textesGeneriques.bug

interface PlayerValues {
  pathway: Pathway
  playground: Playground
}

class Player {
  private readonly pathway: Pathway
  private readonly playground: Playground
  /** Le nom de la section courante */
  private currentSectionName?: string
  /**
   * Les exports de la section courante
   * @type {Section}
   * @private
   */
  private currentSection?: Section
  /** Le contexte de la section pour ce nœud (initialisé au démarrage du nœud) */
  private sectionContext?: SectionContext

  constructor ({ pathway, playground }: PlayerValues) {
    this.pathway = pathway
    this.playground = playground
  }

  /**
   * Retourne le résultat courant (doit être sync, on peut être appelé à la fermeture du navigateur
   * pour récupérer l’état courant (pour le refiler lors du prochain run de la même ressource)
   */
  getCurrentResultat (): Resultat {
    return this.pathway.getResultat()
  }

  /**
   * Pour écrire le bilan en html (à améliorer)
   * @return {string}
   */
  getAssessment (): string {
    const resultat = this.getCurrentResultat()
    const results = resultat.contenu.pathway.results
    let assessment: string = ''
    for (const result of results) {
      const duree = result.duree
      assessment += `<span class="nodeInAssessment">Noeud ${result.id}</span> -> Temps passé : ${getMinutesSecondesLabel(duree)}, score : ${result.score} ${result.evaluation !== '' ? '(' + result.evaluation + ')' : ''}<br>`
    }
    return assessment
  }

  /**
   * Joue le graphe complet
   */
  async run (): Promise<void> {
    try {
      const currentNode = this.pathway.getCurrentNode()
      console.debug(`run avec currentNode.id ${currentNode.id}, section ${currentNode.section}`)
      if (currentNode.isEnd()) {
        // affiche le bilan du parcours.
        const bilan = this.getAssessment()

        await this.playground.displayBilan(`Parcours terminé<br><span>${bilan}</span>`, {
          title: 'Bilan'
        })
        this.playground.reset()
        this.playground.hideWaiting()
        return
      }

      // pas un nœud fin
      const { section: sectionName } = this.pathway.getCurrentNode()
      this.currentSectionName = sectionName
      // on charge la section (ça throw si ça existe pas)
      const section = await loadSection(sectionName)

      let isLegacy = false
      let result: Result
      // pour les section v1 on délègue tout à LegacySectionRunner
      if (isLegacySection(section)) {
        // c’est une section v1
        isLegacy = true
        this.currentSection = undefined
        const { default: LegacySectionRunner } = await import('./LegacySectionRunner')
        const runner = new LegacySectionRunner(sectionName, section)
        result = await runner.start(this.playground.getContainer(), this.pathway)
      } else {
        result = await this.runOneNode(section)
      }

      // le feedback mis dans le connecteur, message à afficher en passant au nœud suivant
      // const debugMsg = `result du ${this.pathway.currentNodeId} donne le nextNode `
      const connectionFeedback = this.pathway.setNextNode(result)
      // console.log(debugMsg + this.pathway.currentNodeId, result)
      if (!result.nextAuto) {
        if (isLegacy) {
          // on cherche le bouton section suivante, qui devrait être là
          const nextSectionButton = getElement('Mepsectioncontinuer')
          if (nextSectionButton) {
            await (new Promise(resolve => {
              nextSectionButton.addEventListener('click', resolve, { once: true })
            }))
          } else {
            console.error(Error('Pas trouvé le bouton section suivante'))
          }
          if (connectionFeedback) {
            await (new Promise<void>((resolve) => {
              const modal = new Modal({ title: connectionFeedback, onClose: resolve })
              modal.show()
            }))
          }
        } else {
          if (connectionFeedback) {
            await this.playground.showFeedback(connectionFeedback, { withoutListener: true })
          }
          await this.playground.waitForClick('nextSection')
        }
      }

      // currentNode a changé, on recommence (dans un setTimeout pour limiter la callStack)
      setTimeout(this.run.bind(this), 0)
    } catch (error) {
      console.error(error)
      await this.playground.showFeedback(bug, { ok: false })
    }
  }

  /**
   * Lance la section courante et résout avec le résultat du nœud dès qu’on a répondu à la dernière question
   * @protected
   */
  protected async runOneNode (section: Section): Promise<Result> {
    const { id, params, section: sectionName } = this.pathway.getCurrentNode()
    this.sectionContext = new SectionContext({
      params,
      pathway: this.pathway,
      playground: this.playground,
      section
    })
    const ctx = this.sectionContext
    this.currentSection = section
    this.playground.reset()
    const fallbackTitle = 'Pas de titre'
    const title = getNonEmptyString(ctx.params.title, fallbackTitle)
    if (title === fallbackTitle) console.error(Error(`La section ${sectionName} n’a pas de paramètre titre`))
    this.playground.setTitle(title)

    // et on peut appeler la section
    await this.currentSection.init(ctx)
    const startedAtMs = Date.now()

    // boucle sur le nombre de questions
    let hasNextQuestion = ctx.question <= ctx.nbQuestions
    while (hasNextQuestion) {
      // lance les étapes (boucle sur le nb d’essais)
      await this.runSteps()
      // On prépare pour la question suivante
      hasNextQuestion = ctx.question <= ctx.nbQuestions
      let resetOptions
      if (hasNextQuestion) {
        await this.playground.waitForClick('nextQuestion')
        resetOptions = { work: true, feedback: true, solution: true, butPersistent: true }
        this.playground.reset(resetOptions)
      }
      // sinon c’est section suivante, et c’est run qui gérera après avoir envoyé le résultat
    }
    const nbItems = ctx.nbQuestions * ctx.nbSteps
    return new Result({ id, score: ctx.score / nbItems, evaluation: ctx.evaluation, duree: getDelaySec(startedAtMs) })
  } // runOneNode

  /**
   * Lance une étape (boucle sur le nb d’essais pour cette étape)
   * @protected
   */
  protected async runSteps (): Promise<void> {
    if (this.currentSection == null || this.sectionContext == null) throw Error('Pas de section courante valide')
    const sec = this.currentSection
    const ctx = this.sectionContext
    // on démarre un nouvel essai
    ctx.nbTries++
    // d’une nouvelle étape
    ctx.step++
    await this.playground.showStatus(ctx)
    this.playground.unFreeze()
    // On efface l’éventuelle solution précédente.
    this.playground.reset({ solution: true })
    // et on peut afficher l’énoncé
    this.playground.showWaiting()
    const partialResultGetter = await this.currentSection.enonce(ctx)
    // l’énoncé est affiché, on regarde s’il faut démarrer un timer pour du temps limité
    /** Promesse résolue en cas de timeout */
    let timeoutPromise
    const limite = Number(ctx.params.limite)
    if (Number.isFinite(limite) && limite > 0) {
      timeoutPromise = new Promise<void>((resolve) => {
        const startedAtMs = Date.now()
        const timer = setInterval(() => {
          const elapsed = getDelaySec(startedAtMs)
          this.playground.updateLimitBar(limite, elapsed)
          if (elapsed >= limite) {
            clearInterval(timer)
            ctx.isTimeout = true
            resolve()
          }
        }, 1000)
      })
    }
    let partialResult: PartialResult
    if (partialResultGetter == null) {
      // boucle tant que l’élève n’a pas répondu
      let isAnswered = false
      // parce qu’il y a un setInterval qui s’en occupe.

      while (!isAnswered) {
        const clickPromise = this.playground.waitForClick('validation')
        if (timeoutPromise != null) {
          await Promise.race([timeoutPromise, clickPromise])
          if (ctx.isTimeout) {
            // pas besoin de feedback, updateLimitBar s’en est chargé
            break
          }
        } else {
          await clickPromise
        }
        this.playground.reset({ solution: true, feedback: true })
        // on prend par défaut le isAnswered de la section, et celui du playground si elle n’en précise pas
        isAnswered = sec.isAnswered ? await sec.isAnswered(ctx) : await this.playground.isAnswered()
        if (!isAnswered) {
          await this.playground.dialog(defaultFeedbackMissing, 'Attention !')
          // await this.playground.showFeedback(defaultFeedbackMissing, { ko: true })
        }
      }
      // y’a une réponse, on évalue avec check()
      partialResult = await sec.check(ctx)
    } else {
      // C’est la section qui gère le passage énoncé => correction,
      // Y’a pas de bouton ok et on a récupéré la promesse de résultat.
      // (pour les exo scratch par ex ou la validation se fait en cliquant sur le drapeau)
      this.playground.showAlternateValidation()
      partialResult = await partialResultGetter()
    }
    const { ok, feedBackMessage, evaluation, partialScore } = partialResult
    if (evaluation == null) {
      ctx.evaluation = ''
    } else {
      // y’a plein de section en js sans type checking, webstorm râle sur le typeof inutile mais faut le laisser
      if (typeof evaluation !== 'string') {
        throw Error('La correction retourne une évaluation qualitative invalide')
      }
      if (this.currentSection.evaluations == null) {
        throw Error(`La correction retourne une évaluation qualitative mais la section ${this.currentSectionName} n’en exporte aucune`)
      }
      // on vérifie que c'est bien une évaluation valide
      if (evaluation in this.currentSection.evaluations) {
        ctx.evaluation = evaluation
      } else {
        // y’a un pb, on regarde si c'est une valeur connue (pour récupérer le nom de la propriété)
        let evalId
        Object.entries(this.currentSection.evaluations).some(([k, v]) => {
          if (v === evaluation) {
            evalId = k
            return true
          }
          return false
        })
        if (evalId) {
          console.error(Error(`La section ${this.currentSectionName} retourne la valeur d’une évaluation qualitative plutôt que son identifiant (« ${evaluation} » alors qu’elle aurait dû retourner ${evalId})`))
          ctx.evaluation = evalId
        } else {
          throw Error(`Évaluation qualitative ${evaluation} invalide (dans un résultat de la section ${this.currentSectionName})`)
        }
      }
    }
    ctx.score += partialScore ?? (ok ? 1 : 0)
    // affiche l’avancement
    await this.playground.showStatus(ctx)
    // et le feedback
    const message = ctx.isTimeout
      ? timeoutMsg
      : feedBackMessage || (ok ? defaultFeedbackOk : defaultFeedbackKo)
    await this.playground.showFeedback(message, { ok })

    // et on incrémente dans sectionContext
    const rest = ctx.maxTries - ctx.nbTries
    if (!ctx.isTimeout && rest > 0 && !ok) {
      const message = `Il reste ${rest} essai${rest > 1 ? 's' : ''}`
      await this.playground.showFeedback(message)
      return this.runSteps()
    }

    // y’a plus d’essais disponibles, on affiche la solution
    await sec.solution(ctx)

    if (!ctx.isTimeout && ctx.step < ctx.nbSteps) {
      // il reste des étapes
      // en cas de très grand nombre d’étapes, on passe par un setTimeout pour éviter d’atteindre le maxCallStack
      if (ctx.nbSteps > 50 && ctx.step % 10 === 0) {
        setTimeout(this.runSteps.bind(this), 0)
      }
      return this.runSteps()
    }

    // c'était la dernière étape de cette question
    // => on peut geler divWork et divSolution
    this.playground.freeze()

    ctx.question++
    ctx.step = 0
    ctx.nbTries = 0
  } // runSteps
}

export default Player
