/** Valeurs possibles pour un item */
export type ItemValue = string | number | string[] | number[] | Record<string, string>

/** Un item du playground */
export interface Item {
  container: HTMLElement
  getValue: () => ItemValue | Promise<ItemValue>
  isAnswered: () => boolean | Promise<boolean>
  persistent: boolean
  setFeedback: (isOk: boolean, feedback: string) => void
  setFocus: () => void
}

/** Propriété parcours de la classe Parcours (v1 only, pour les données de contexte) */
export interface LegacyParcoursProp {
  pe: string
  boucle?: number
  boucleGraphe?: number[]
  /** index du nœud courant dans le graphe */
  noeud: number
}
