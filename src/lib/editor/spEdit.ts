import { isLegacyRessourceJ3pParametres } from 'src/lib/core/checks'
import Graph from 'src/lib/entities/Graph'
import { normalizeContainer } from 'src/lib/utils/dom/main'
// on ne peut pas importer directement main.ts depuis src/j3pGlobalLoader.js
// ça marche pas en import dynamique, mais semble fonctionner ici en import classique, tant qu’on ne précise pas d’extension…
import editor from './main'

import type { RessourceJ3pParametres, ressourceJ3pParametresGetter, RessourceJ3pParametresV1 } from 'src/lib/types'

export type RessourceJ3pParametresLax = RessourceJ3pParametresV1 | RessourceJ3pParametres

export interface RessourceLike {
  parametres: RessourceJ3pParametresLax
}
export type editorCallback = (error: Error | null, getter: ressourceJ3pParametresGetter) => void

async function getGraphFromParams (parametres: RessourceJ3pParametresLax): Promise<Graph> {
  if (isLegacyRessourceJ3pParametres(parametres)) {
    // c’est du graphe v1
    if (!Array.isArray(parametres.g)) throw Error('Les paramètres de la ressource ont une propriété g mais ce n’est pas un tableau')

    const { convertGraph } = await import('src/lib/core/convert1to2.js')
    return convertGraph(parametres.g, parametres.editgraphes)
  }
  if (parametres?.graph != null) {
    return new Graph(parametres.graph)
  }
  return new Graph()
}

/**
 * Édite un graphe
 */
export default async function spEdit (container: HTMLElement | string, { parametres }: RessourceLike, { sourceTreeRid }: {
  sourceTreeRid: string
}, next?: editorCallback): Promise<ressourceJ3pParametresGetter> {
  try {
    const { mainContainer } = normalizeContainer(container)
    // faut ajouter ça pour spStart qui peut cumuler les css player/editor
    mainContainer.classList.add('spEditor')
    // une fct pour garantir le type Graph

    const graph = await getGraphFromParams(parametres)
    const getParametres = await editor(mainContainer, graph, sourceTreeRid)
    mainContainer.style.height = '100vh' // une hauteur de viewport
    mainContainer.scrollIntoView(false)
    if (next) {
      try {
        next(null, getParametres)
      } catch (error) {
        console.error(error)
      }
    }
    return getParametres
  } catch (error) {
    console.error(error)
    const errorToForward = Error('Le graphe à éditer était invalide, impossible de récupérer ses paramètres')
    if (next) {
      next(error as Error, () => {
        throw errorToForward
      })
    }
    throw errorToForward
  }
}
