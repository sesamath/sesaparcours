import { revert } from 'src/lib/utils/string'

// Pour des questions de perfs, il faut mieux avoir des cases pas trop petites (ne pas descendre en dessous de 10)
/** Largeur d'une case de la grille */
export const gridEltX = 50
/** Hauteur d'une case de la grille */
export const gridEltY = 25

// Pour une question d'ergonomie, il vaut mieux avoir une aimantation au même pas (pour éviter que le nœud ne "saute" quand on le lâche)
// on pourrait baisser l'aimantation si on n'arrondissait pas les x,y retournés
/** pas horizontal d'aimantation */
export const magnetPitchX = gridEltX
/** pas vertical d'aimantation */
export const magnetPitchY = gridEltY

// les marges ajoutées par SceneUi.placeInGrid, mises ici pour avoir toutes les constantes au même endroit)
/** Marge horizontale */
export const marginX = 80
/** Marge verticale */
export const marginY = 40

export interface BlocPx {
  x: number
  y: number
  width: number
  height: number
}

interface BlocCoord {
  ligMin: number
  ligMax: number
  colMin: number
  colMax: number
}
interface BlocMask {
  ligMin: number
  ligMax: number
  mask: bigint
}

interface BlocCoordMask extends BlocCoord {
  mask: bigint
}

/*
On passe à un array de binaires, pour utiliser les opérations booléennes sur les binaires et gagner en perf
  Cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer
  https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/TypedArray
  https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/DataView
  https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/BigInt

Ex avec une grille
  0001110000
  0001110000
  0001110000
où on veut faire rentrer
  000011
  000011
ça devrait le positionner juste à droite pour donner
  0001111100
  0001111100
  0001110000

Pour la collision on utilise le & binaire (qui doit retourner 0 si ça collisionne pas, pas de 1 en commun)
https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Bitwise_AND
Pour le décalage vers la droite on utilisera <<
https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Left_shift
car nos unités sont à gauche (pour ne pas devoir tout décaler quand la grille augmente)
*/

/**
 * Gère la grille d’occupation de la scène par les nœuds
 */
class Grid {
  /** Le tableau de BigInt */
  values: bigint[]

  /** initialise la grille vide */
  constructor () {
    this.values = [0n]
  }

  /**
   * Occupe une ou des position(s)
   * @param x
   * @param y
   * @param width
   * @param height
   */
  occupy ({ x, y, width, height }: BlocPx): void {
    const { ligMin, ligMax, mask } = this.convertPxToMask({ x, y, width, height })
    // reste à ajouter ce masque à la grille sur les lignes concernées
    for (let lig = ligMin; lig <= ligMax; lig++) {
      this.values[lig] = (this.values[lig] ?? 0n) | mask
    }
    // console.debug('fin occupy avec', JSON.stringify(arguments[0]), '\n' + this.toString())
  }

  free ({ x, y, width, height }: BlocPx): void {
    const { ligMin, ligMax, mask } = this.convertPxToMask({ x, y, width, height })
    for (let lig = ligMin; lig <= ligMax; lig++) {
      // on veut mettre des 0 dans this.values[lig] là où on a des 1 dans mask
      // => on passe en négatif pour cumuler les places vides
      this.values[lig] = ~(~(this.values[lig] ?? 0n) | mask)
    }
    // console.debug('fin free ' + JSON.stringify(arguments[0]), this.toString())
  }

  /**
   * Retourne la première position non occupée capable d’accueillir le rectangle
   * (on cherche seulement à droite et dessous)
   */
  firstFreePosition ({ x, y, width, height }: BlocPx): { x: number, y: number } {
    try {
      // console.debug('firstFreePosition ' + JSON.stringify(arguments[0]), `avec la grille ${this}`)
      const { ligMin, ligMax, colMin, mask } = this.convertPxToMask({ x, y, width, height })
      // on regarde si c’est libre d’office
      if (this.isFreePositionMask({ ligMin, ligMax, mask })) {
        return {
          // faut arrondir au coin d'une case
          x: x - x % gridEltX,
          y: y - y % gridEltY
        }
      }
      // sinon, on boucle en agrandissant notre rayon de recherche
      let radius = 1
      // les mask déjà calculés pour chaque décalage de bits, l'index 0 correspond à colMin
      const masks = [mask]

      // on cherche dans un carré vers la droite et le bas
      while (radius < 10) {
        // on commence par le bord droit des lignes déjà testées
        // le nouveau mask à tester une colonne plus loin (colMin+radius)
        // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Left_shift
        const m = masks[radius - 1] as bigint << 1n
        masks[radius] = m

        // boucle sur chaque ligne
        for (let iL = 0; iL < radius; iL++) {
          const l = ligMin + iL
          if (this.isFreePositionMask({ ligMin: l, ligMax: ligMax + iL, mask: m })) {
            // console.debug(`trouvé après décalage à droite de ${radius} col sur la ligne ${l}`)
            return {
              x: (colMin + radius) * gridEltX,
              y: l * gridEltY
            }
          } // else console.debug(`pas trouvé en (${colMin + radius}, ${l}) (jusqu’à ${colMax + radius} ${ligMax + iL})`)
        }

        // on teste une ligne de plus (ligMin + radius) sur toutes les colonnes (colMin + radius incluse)
        const newL = ligMin + radius
        for (let iC = 0; iC <= radius; iC++) {
          const mask = masks[iC] as bigint
          if (this.isFreePositionMask({ ligMin: newL, ligMax: ligMax + radius, mask })) {
            // console.debug(`trouvé après décalage vers le bas de ${radius} et vers la droite de ${iC}`)
            return {
              x: (colMin + iC) * gridEltX,
              y: newL * gridEltY
            }
          } // else console.debug(`pas trouvé après décalage vers le bas de ${radius} et vers la droite de ${iC}`)
        }

        radius++
      }
      // pas trouvé, on retourne tout en bas à gauche…
      return { x: 0, y: this.values.length * gridEltY }
    } catch (error) {
      // soit la grille est devenue vraiment énorme, soit on a un bug dans notre algo
      console.error(error)
      return { x, y }
    }
  }

  /**
   * nettoie la grille (jamais utilisé)
   */
  clear (): void {
    for (let i = 0; i < this.values.length; i++) {
      this.values[i] = 0n
    }
  }

  /**
   * Converti la grille en string (et permet de faire du `String(grid)` pour l'afficher)
   * @param [withoutLegend=false] passer true pour ne pas avoir les n° de ligne et de colonne
   */
  toString (withoutLegend = false): string {
    let dump = ''
    let max = 0
    for (let i = 0; i < this.values.length; i++) {
      // faut l'inverser pour mettre les unités à gauche
      const line = revert((this.values[i] ?? 0n).toString(2))
      if (line.length > max) max = line.length
      dump += withoutLegend
        ? `${line}\n`
        : `${String(i).padStart(3, ' ')} ${line}\n`
    }
    if (!withoutLegend) {
      const firstLine = '0123456789'.repeat(Math.ceil(max / 10))
      dump = '    ' + firstLine + '\n\n' + dump
    }
    return dump
  }

  /**
   * retourne true si ça empiète sur une place occupée
   * @param x
   * @param y
   * @param width
   * @param height
   */
  hasCollision ({ x, y, width, height }: BlocPx): boolean {
    return !this.isFreePositionPx({ x, y, width, height })
  }

  /**
   * Retourne les cases occupées par ce rectangle (lignes et colonnes sont incluses)
   * @param x
   * @param y
   * @param width
   * @param height
   * @private
   */
  private convertPxToCoord ({ x, y, width, height }: BlocPx): BlocCoord {
    return {
      // on décale d'un pixel pour que floor ou ceil ne nous décale pas d'une unité en cas d'arrondis antérieurs (on veut 2 pour le floor de 1.9999999 ou le ceil de 2.00000001)
      ligMin: Math.max(0, Math.floor((y + 1) / gridEltY)),
      ligMax: Math.max(0, Math.ceil((y - 1 + height) / gridEltY) - 1),
      colMin: Math.max(0, Math.floor((x + 1) / gridEltX)),
      colMax: Math.max(0, Math.ceil((x - 1 + width) / gridEltX) - 1)
    }
  }

  private convertPxToMask ({ x, y, width, height }: BlocPx): BlocCoordMask {
    const { ligMin, ligMax, colMin, colMax } = this.convertPxToCoord({ x, y, width, height })
    let mask = 0n
    let col = BigInt(colMin)
    while (col < colMax + 1) {
      mask += 1n << col // ajoute un 1 dans la colonne col
      col++
    }
    return { ligMin, ligMax, colMin, colMax, mask }
  }

  /**
   * Retourne true si la place est libre
   * @param x
   * @param y
   * @param width
   * @param height
   */
  private isFreePositionPx ({ x, y, width, height }: BlocPx): boolean {
    const { ligMin, ligMax, mask } = this.convertPxToMask({ x, y, width, height })
    return this.isFreePositionMask({ ligMin, ligMax, mask })
  }

  /**
   * Retourne true si la place est libre
   * @param ligMin
   * @param ligMax
   * @param mask
   * @private
   */
  private isFreePositionMask ({ ligMin, ligMax, mask }: BlocMask): boolean {
    // console.log('isFreePositionMask', ligMin, ligMax, revert(mask.toString(2)), `avec la grille\n${this}`)
    for (let lig = ligMin; lig <= ligMax; lig++) {
      const curLig = this.values[lig]
      if (curLig == null) continue
      // c'est bien l'opérateur bitwise qu'on veut, et pas &&, ça vaudra 0 s'il n'y a aucun 1 en commun
      if ((curLig & mask) !== 0n) return false // collision sur cette ligne
    }
    return true
  }
}

export default Grid
