// @ts-ignore TS7016: Could not find a declaration file for module …
import { addNode as addNodeToTree, build, getAsRef } from '@sesatheque-plugins/arbre/lib/index'
import $ from 'jquery'
// @ts-ignore TS7016: Could not find a declaration file for module …
import { fetchPersoOnCurrent, fetchPrivateRessource, fetchPublicRessource, fetchRef } from 'sesatheque-client/src/promise/fetch'
// @ts-ignore TS7016: Could not find a declaration file for module …
import { getBaseId } from 'sesatheque-client/src/sesatheques'
import { isLegacyRessourceJ3pParametres } from 'src/lib/core/checks'
import SceneUI from 'src/lib/editor/SceneUI'
import Modal from 'src/lib/entities/dom/Modal'
import Graph from 'src/lib/entities/Graph'
import GraphNode from 'src/lib/entities/GraphNode'

import type { Ressource, RessourceJ3pParametres, RessourceJ3pParametresV1 } from 'src/lib/types'

// le css pour les ressources jstree
import '@sesatheque-plugins/arbre/public/arbre.css' // pour le .jstree-themeicon-custom et #display #apercu
import { empty, showError } from 'src/lib/utils/dom/main'

import { typeBibliConnus } from './config'
import { addGraph, addNode } from './redux/actions'
import { dispatch } from './redux/store'
import '@sesatheque-plugins/arbre/public/icons.css'

interface itemType {
  type: string,
  aliasOf: string,
  public: boolean
}

// depuis qu’on a débranché le parsing tsc dans eslint (bcp trop lent), eslint signale du no-undef pour les types que tsc trouve très bien
/* global JQuery */
interface JqObj {
  event: JQuery.TriggeredEvent<HTMLElement>
  data: any
}

// on créé une variable pour l’élément jqueryfié
// (pour éviter de relancer la construction de $('#edgMenu') à chaque fois qu’on en aura besoin
let $menuElt: JQuery

class EditorSrcTree {
  sceneUI: SceneUI

  constructor (sceneUI: SceneUI) {
    this.sceneUI = sceneUI
  }

  /**
   * Ajoute le listener pour dnd_stop
   * @private
   */
  private addDropListener (): void {
    // pb car l’événement dnd_stop.vakata a une target sur le document entier,
    // on peut donc pas filtrer sur la target, sauf à remonter jqObj.data.obj.0.parentNode.parentNode…
    // cf l’exemple http://jsfiddle.net/38vbrbpn/ pour jstree 3.3.3
    $(document).on('dnd_stop.vakata', (domEvent: number, { event, data }: JqObj) => {
      if (domEvent === 42) {
        domEvent++ // c’est juste pour pas faire râler TS
      }
      // domEvent est un event du dom
      // le 2e param est un objet construit par jQuery, avec
      // event est un jQuery.Event, c’est lui qui a les infos clientX et offsetX
      // event.target un élément du dom passé par jQuery
      // event.target.id l’id html de l’élément sur lequel on drop
      // data est un objet avec des éléments de jsTree
      // data.nodes un tableau des id jsTree (les id jsTree des items 'dropped')
      try {
        // bizarre, parfois jqObj.event.target n’existe pas
        const classList: DOMTokenList = event?.target.classList
        if (classList == null || !classList.contains('editorScene')) return
        // on est sur la scene, la liste des nodes du drop est là
        for (const nodeId of data.nodes) {
          const item: itemType = getAsRef($menuElt, nodeId)
          if (item.type === 'arbre') {
            // eslint-disable-next-line no-console
            console.log('drop d’un arbre ignoré')
          } else if (item.type != null && item.type !== 'error' && item.aliasOf !== undefined) {
            this.addItemToScene(item, event)
              .catch(showError)
          }
        }
      } catch (error) {
        console.error(error)
      }
    })
  }

  /**
   * Ajoute un item (droppé depuis l’arbre de gauche) sur la scène passée en argument pour pouvoir utiliser ses checkPositions
   * @param item
   * @param event
   */
  private async addItemToScene (item: itemType, event: JQuery.TriggeredEvent): Promise<void> {
    // appel ajax pour récupérer toutes les infos qu’on n’a pas dans une Ref
    const ressource: Ressource = item.public !== undefined
      ? await fetchPublicRessource(null, item.aliasOf)
      : await fetchPrivateRessource(null, item.aliasOf)
    if (ressource == null) throw Error('Impossible de récupérer cette ressource.')
    // console.debug('on a récupéré la ressource', ressource)
    const { offsetX = 0, offsetY = 0 } = event
    const nodeOrGraph = await this.getNodeOrGraph(ressource)
    if (nodeOrGraph instanceof Graph) {
      this.sceneUI.checkGraphPositions(nodeOrGraph)
      dispatch(addGraph(nodeOrGraph.serialize()))
    } else {
      // on affecte les positions voulues au node
      nodeOrGraph.position.x = offsetX
      nodeOrGraph.position.y = offsetY
      // et on vérifie que ça rentre
      this.sceneUI.checkNodePosition(nodeOrGraph)
      dispatch(addNode(nodeOrGraph.serialize()))
    }
  }

  /**
   * Ajoute les ressources persos à l’arbre (s’il y en a)
   */
  private async addPerso (): Promise<void> {
    // faut pas appeler cette fonction si la page courante n’est pas sur une sesathèque (il faut envoyer les cookies pour récupérer ses ressources perso)
    if (getBaseId(document.location.origin + '/', null) == null) return
    const listeComplete = await fetchPersoOnCurrent({ limit: 100 }) // 100 max
    const liste = listeComplete.filter((ref: Ressource) => typeBibliConnus.includes(ref.type))
    if (liste.length === 0) return
    const branche = {
      type: 'arbre',
      titre: 'Mes ressources',
      enfants: liste
    }
    addNodeToTree($menuElt, branche, '#', function (error: Error /*, node: Node */) {
      if (error) console.error(error)
      // console.debug('node des ressources persos ajouté', node)
    })
  }

  /**
   * Retourne le GraphNode correspondant à la ressource (ou le graphe complet si c’est une ressource j3p à plusieurs nodes)
   * @param {Ressource} ressource
   */
  private async getNodeOrGraph ({ idOrigine, parametres, titre, type }: Ressource): Promise<GraphNode | Graph> {
    const label = titre
    if (type === 'j3p') {
      const graphValues = parametres.graph
      let graph: Graph
      if (graphValues) {
        graph = new Graph()
      } else {
        const p = parametres as unknown
        if (isLegacyRessourceJ3pParametres(p as RessourceJ3pParametres | RessourceJ3pParametresV1)) {
          const p1 = p as RessourceJ3pParametresV1
          const { convertGraph } = await import('src/lib/core/convert1to2.js')
          graph = convertGraph(p1.g, p1.editgraphes)
        } else {
          throw Error('Aucun graphe dans cette ressource')
        }
      }
      // si c’est un graphe avec seulement un nœud "normal" et un nœud fin, on ne prend que le nœud "normal"
      // Et on n’oublie pas de virer le connecteur qui pointe vers le noeud fin qui n’existe plus...
      if (graph.getLength() === 1) {
        const graphNode = graph.getNode(graph.startingId) // le GraphNode de départ, c’est le seul
        if (graphNode instanceof GraphNode) {
          graphNode.connectors = []
          return graphNode
        }
        throw Error('Graphe invalide')
      }
      // sinon, on retourne le graph complet
      return graph
    }
    if (type === 'ecjs') {
      return new GraphNode({ label, section: 'calculatice', params: { exo: idOrigine } })
    }
    if (type === 'iep') {
      // cas particulier : on dispose parfois d’un XML iep, parfois non, auquel cas on a l’URL et on utilise alors la section lecteuriepparurl avec le param urlpn
      if (typeof parametres?.xml === 'string' && parametres.xml !== '') {
        return new GraphNode({ label, section: 'lecteuriep', params: { script: parametres.xml } })
      }
      if (typeof parametres?.url === 'string' && parametres.url !== '') {
        return new GraphNode({ label, section: 'lecteuriepparurl', params: { url: parametres.url } })
      }
      throw Error('Ressource instrumenpoche invalide (pas de script pour l’animation)')
    }
    if (type === 'ato') {
      return new GraphNode({ label, section: 'squeletteatome', params: { atome: idOrigine } })
    }
    if (type === 'em') { // Exercice mathenpoche
      const modeleMep = String(parametres.mep_modele) // ça pourrait être un number
      const section = (modeleMep === '1' || modeleMep === 'mep1') ? 'ancienexo_mep' : 'exo_mep'
      return new GraphNode({ section, label, params: { exo: idOrigine } })
    }
    if (type === 'am') {
      return new GraphNode({ section: 'aide_mep', label, params: { aide: idOrigine } })
    }
    // on a fait tous les cas connus, si on est encore là y’a un souci…
    throw Error(`type de ressource ${type} non géré`)
  }

  /**
   * Appelé au clic droit "tester la ressource" (sur une ressource du menu)
   * Ouvre la ressource dans une modale
   * @param node Le node jstree
   */
  private testRessource (node: any): void {
    const type = node.a_attr['data-type']
    if (type === 'arbre') return
    const url = node.a_attr.href
    if (url === undefined) {
      console.error(new Error('node sans href'))
      return
    }
    // ce n’est pas un arbre, on affiche
    let longueur = 890 // au pif pour l’instant
    let hauteur = 710
    if (type === 'j3p') {
      longueur = 890
      hauteur = 710
    }
    if (type === 'em' || type === 'am') {
      longueur = 820
      hauteur = 600
    }
    // une modale
    const modaleElt = new Modal({ inset: '2%' })
    const iframeOpts = {
      id: 'iframe_affiche_ressource',
      src: url,
      width: String(longueur),
      height: String(hauteur)
    }
    modaleElt.addElement('iframe', { attrs: iframeOpts })
    modaleElt.show()
  }

  /**
   * Initialise l’arbre (source des éléments à dropper)
   * @param container
   * @param sourceTreeRid
   */
  async init (container: HTMLElement, sourceTreeRid: string): Promise<void> {
    empty(container)
    container.classList.add('editorMenu')
    // on construit le menu
    const arbre = await fetchRef(sourceTreeRid)

    // on a notre arbre, on peut continuer
    const testRessource = this.testRessource.bind(this)
    const options = {
      check_callback: (operation: string, node: any): boolean => {
        // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
        // node est le node manipulé
        // parent est le node parent du node manipulé dans l’arbre source
        // position est l’index de node parmis les enfants de parent (démarre à 1)
        // infos peut être undefined, sinon il contient
        // - dnd {boolean} (true)
        // - is_foreign {boolean} devrait valoir true si on est hors de l’arbre, mais vaut tout le temps false, pénible
        // - is_multi {boolean}
        // - pos {string} ?
        // - origin un truc interne jstree
        // - ref Le node
        // mais on a rien pour savoir où on est, pas de target

        // on autorise la création de la branche 'Mes ressources'
        if (operation === 'create_node' && node.text === 'Mes ressources') return true
        // et on refuse tout le reste, pour empêcher l’utilisateur de modifier l’arbre source
        return false
      },
      plugins: ['dnd', 'contextmenu', 'search'],
      // dnd: {
      // cf https://www.jstree.com/api/#/?q=$.jstree.defaults.dnd&f=$.jstree.defaults.dnd.always_copy
      // a boolean indicating if nodes from this tree should only be copied with dnd (as opposed to moved), default is false
      // always_copy: true
      // },
      contextmenu: {
        items: function (node: any) {
          // on veut pas de menu contextuel sur les arbres
          if (node.a_attr['data-type'] === 'arbre') return
          return {
            // nom de propriété arbitraire
            testRessource: {
              separator_before: false,
              separator_after: false,
              label: 'Tester la ressource',
              action: () => testRessource(node)
            }
          }
        }
      }
    } // options
    // ça c’est indispensable pour écouter les événements ici (sinon on entend rien
    // car on aura pas la même instance de jquery que jstree)
    // options.jQuery = $ // à priori inutile avec notre configuration de vite (qui dédoublonne (dedupe) le module jquery pour que tout le monde ait la même instance)
    const $tree = await build(container, arbre, options)
    if ($tree == null) throw new Error('La construction de l’arbre des ressources j3p a échoué')
    // on affecte notre variable globale utilisée partout dans ce module
    $menuElt = $tree
    // le listener pour réagir au drop sur la scene
    this.addDropListener()
    // ici on ajoute nos ressources perso à l’arbre de gauche
    await this.addPerso()
  }
}

export default EditorSrcTree
