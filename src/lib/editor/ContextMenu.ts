import SceneUI from 'src/lib/editor/SceneUI'
import { addElement, addTextContent, empty, showError } from 'src/lib/utils/dom/main'

import { addTooltip, hideTooltip } from 'src/lib/widgets/tooltip'

interface Entry {
  content: string
  tooltip?: string
  icon?: string
  clickListener: (event: MouseEvent) => void | Promise<void>
}

// Une marge pour éviter de sortir
const offsetMin = 15

/**
 * Code commun de gestion d’un menu contextuel
 */
class ContextMenu {
  /** le div qui masque la scène */
  mask: HTMLElement
  /** le div positionné contenant le menu */
  divMenu: HTMLElement
  sceneUiContainer: HTMLElement
  sceneUI: SceneUI
  /** Le tag ul parent dans divMenu (mis par open) */
  protected ulParent!: HTMLUListElement

  constructor (sceneUiContainer: HTMLElement, sceneUI: SceneUI) {
    this.sceneUiContainer = sceneUiContainer
    this.sceneUI = sceneUI
    this.mask = addElement(this.sceneUiContainer, 'div', { className: 'menuMask', style: { display: 'none' } })
    this.divMenu = addElement(this.mask, 'div', { className: 'context-menu' })
    this.ulParent = addElement(this.divMenu, 'ul', { className: 'menu' })
    this.ulParent.addEventListener('mouseleave', (event: MouseEvent) => {
      // on ferme que si on sort de l’ul (et pas si on sort d’un de ses enfants)
      if (event.target === event.currentTarget) this.close()
    }, { passive: true })
  }

  /**
   * Ouvre le menu contextuel (avec ses listeners & actions éventuelles)
   */
  open (event: MouseEvent): void {
    empty(this.ulParent)
    this.build()
    // on replace le menu si besoin
    // mémorisation taille actuelle
    // mémorisation taille actuelle
    const wDispo = this.sceneUiContainer.offsetWidth
    const hDispo = this.sceneUiContainer.offsetHeight
    // on affiche (pour que le menu prenne sa place)
    this.mask.style.display = 'block'
    const target = event.target as Element
    let x: number
    let y: number
    if (target == null) return
    if (target === this.sceneUiContainer) {
      x = event.clientX - this.sceneUiContainer.offsetLeft - 10
      y = event.clientY - 10
    } else {
      const { top, left } = target.getBoundingClientRect()
      // console.debug('position', this.sceneUiContainer.offsetLeft, this.sceneUiContainer.offsetTop)
      x = left - this.sceneUiContainer.offsetLeft + 10
      y = top - 10 + window.scrollY
    }
    if (x < offsetMin) x = offsetMin
    const xMax = wDispo - this.divMenu.offsetWidth - offsetMin
    if (x > xMax) x = Math.max(offsetMin, xMax)
    if (y < offsetMin) y = offsetMin
    const yMax = hDispo - this.divMenu.offsetHeight - offsetMin
    if (y > yMax) y = Math.max(offsetMin, yMax)

    this.divMenu.style.left = `${x}px`
    this.divMenu.style.top = `${y}px`
  }

  protected build (): void {
    // à implémenter dans les classes filles si besoin
  }

  protected addEntry ({ content, icon, tooltip, clickListener }: Entry) {
    const li = addElement(this.ulParent, 'li', { className: 'menu-item' })
    if (icon) addElement(li, 'img', { attrs: { src: icon } })
    addTextContent(li, content)
    let tooltipElt: HTMLElement
    if (tooltip) {
      // faut-il vraiment préciser `parent: this.ulParent` ?
      tooltipElt = addTooltip({ tooltip, target: li, placement: 'right' })
    }
    li.addEventListener('click', (event: MouseEvent) => {
      try {
        this.close()
        if (tooltipElt) hideTooltip(tooltipElt)
        const result = clickListener(event)
        if (result instanceof Promise) result.catch(showError)
      } catch (error) {
        showError(error)
      }
    })
  }

  protected hide (): void {
    this.mask.style.display = 'none'
  }

  protected show (): void {
    this.mask.style.display = 'block'
  }

  /**
   * vide le menu, mais conserve le div
   * @private
   */
  protected close (): void {
    this.hide()
    empty(this.ulParent)
  }
} // ContextMenu

export default ContextMenu
