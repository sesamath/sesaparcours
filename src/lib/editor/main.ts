import Graph, { type GraphJson, type GraphValues } from 'src/lib/entities/Graph'
import { showError } from 'src/lib/utils/dom/main'
import SceneManager from './SceneManager'

import type { ressourceJ3pParametresGetter } from 'src/lib/types'

import 'roboto-fontface/css/roboto/sass/roboto-fontface-regular.scss'
import './editor.scss'

/**
 * Lance l’édition du graphe et retourne une fonction pour récupérer le nouveau graphe (v2)
 */
export default async function editor (container: HTMLDivElement, graph: GraphValues, sourceTreeRid: string): Promise<ressourceJ3pParametresGetter> {
  try {
    // construction de la scene, via l’instanciation de son manager
    const sceneManager = await SceneManager.create(container, new Graph(graph), sourceTreeRid)

    // et on retourne une fct permettant de récupérer le graphe en cours d’édition
    // (c’est l’appelant qui va le réclamer, faut lui donner un moyen de le faire)
    return (): { graph: GraphJson } => {
      const graph = sceneManager.getGraph({ complete: true, lax: true })
      return { graph: graph.toJSON() }
    }
  } catch (error) {
    showError(error)
    throw error
  }
}
