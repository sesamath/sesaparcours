import { createReducer, Reducer } from '@reduxjs/toolkit'
import { Action } from 'redux'

import Connector from 'src/lib/entities/Connector'

import type { GraphSerialized } from 'src/lib/entities/Graph'
import type { GraphNodeSerialized } from 'src/lib/entities/GraphNode'
import { isEndNode } from 'src/lib/entities/GraphNode'
import { arrayMove } from 'src/lib/utils/array'

import { addConnector, addGraph, addNode, changeConnector, ChangeConnectorPayload, changeNode, changeNodesPositions, changeNodesPositionsGhosted, ChangeNodesPositionsPayload, removeConnector, RemoveConnectorPayload, removeNode, renameNode, setStartingNode } from './actions'

/** @module lib/editor/redux/reducer */
// cf https://redux-toolkit.js.org/usage/usage-with-typescript#building-type-safe-reducer-argument-objects
// pour avoir des reducers "type safe"
/**
 * Format d’un état du state (le présent ou un élément de past|future)
 */
interface stateRecord {
  // on stocke la liste des nodes sous la forme d’un array, plus facile à manipuler dans nos reducers (très facile à cloner)
  // et dans cet array on a que du plain object (vivement conseillé avec redux, et sinon il râle avec `A non-serializable value was detected in an action`
  // cf https://redux.js.org/faq/actions#why-should-type-be-a-string-or-at-least-serializable-why-should-my-action-types-be-constants
  nodesList: GraphNodeSerialized[]
  startingId: string
}

/**
 * Format complet de notre state
 */
export interface EditorState {
  past: stateRecord[]
  present: stateRecord
  future: stateRecord[]
}

/**
 * Notre state vierge
 */
export const emptyState: EditorState = {
  past: [],
  present: {
    nodesList: [],
    startingId: ''
  },
  future: []
}

/**
 * fonction utilitaire
 * Retourne un id de nœud qui n’existe pas dans nodeList
 * @param nodeIdList
 */
function getNewId (nodeIdList: string[]): string {
  let i = 1
  while (nodeIdList.includes(`node${i}`)) i++
  return `node${i}`
}

/**
 * Wrap un reducer pour gérer l’historique undo/redo
 * @param reducer
 */
function undoable (reducer: Reducer<EditorState, Action>): Reducer<EditorState, Action> {
  // Call the reducer with empty action to populate the initial state
  const initialState: EditorState = emptyState

  // Return a reducer that handles undo and redo
  return function (state = initialState, action: Action): EditorState {
    const { past, present, future } = state

    // les actions qui ne sont pas annulables
    const suffix = '_GHOSTED'
    if (action.type.endsWith(suffix)) {
      action.type = action.type.replace(suffix, '')
      return reducer(state, action)
    }

    switch (action.type) {
      case 'INIT':
        return emptyState
      case 'FLUSH_PAST':
        return { past: [], present, future }
      case 'UNDO': {
        const previous = past[past.length - 1]
        if (previous == null) {
          console.warn('Rien à annuler')
          return state
        }
        const newPast = past.slice(0, past.length - 1)
        return {
          past: newPast,
          present: previous,
          future: [present, ...future]
        }
      }
      case 'REDO': {
        const next = future[0]
        if (next == null) {
          console.warn('rien à refaire')
          return state
        }
        const newFuture = future.slice(1)
        return {
          past: [...past, present],
          present: next,
          future: newFuture
        }
      }
      default: {
        // Delegate handling the action to the passed reducer
        const newPresent = reducer(state, action).present
        if (present === newPresent) {
          return state
        }
        return {
          past: [...past, present],
          present: newPresent,
          future: []
        }
      }
    }
  }
}

/**
 * Le réducer qui gère les actions sur le présent (undoable se chargera de la translation passé / futur)
 */
const reducer: Reducer<EditorState, Action> = createReducer(emptyState, (builder) => {
  builder
    .addCase(addNode, (state, action) => {
      const { nodesList } = state.present
      const idsUsed = nodesList.map(n => n.id)
      const newNode = action.payload
      const idCandidate = newNode.id
      if (!idCandidate) {
        // pas d’id, on en génère un
        newNode.id = getNewId(idsUsed)
      } else if (idsUsed.includes(idCandidate)) {
        // On doit changer l’id du nœud et modifier tous les connecteurs qui en dépendent.
        newNode.id = getNewId(idsUsed)
        if (newNode.label.startsWith('Nœud ')) {
          // si Le label commence par Nœud, c’est sans doute parce qu’il a été fixé automatiquement par convertGraph, donc on modifie aussi son N°
          newNode.label = `Nœud ${newNode.id.substring(4)}`
        }
        if ('connectors' in newNode) { // ça existe pas sur les nœuds fin
          for (const connector of newNode.connectors) {
            connector.source = newNode.id
            if (connector.target === idCandidate) connector.target = newNode.id
          }
        }
      }
      if (!newNode.label) {
        const nbRealNodes = nodesList.filter(n => n.section).length
        newNode.label = `Nœud ${nbRealNodes + 1}`
      }
      let startingIdNew = state.present.startingId
      // Le graph n'a peut-être pas de startingId, on ajoute un noeud, si c'est pas un noeud 'fin', on l'utilise comme startingId
      if (startingIdNew === '') {
        if (!isEndNode(newNode)) {
          startingIdNew = newNode.id
        }
      }
      nodesList.push(newNode)
      state.present.startingId = startingIdNew
    })
    .addCase(removeNode, (state, action) => {
      const idRemoved = action.payload
      const startingId = state.present.startingId
      state.present.nodesList = state.present.nodesList.filter(node => node.id !== idRemoved)
      for (const nodeSource of state.present.nodesList) {
        if (isEndNode(nodeSource)) continue
        nodeSource.connectors = nodeSource.connectors.filter(connector => connector.target !== idRemoved)
      }
      // Si le startingId pointait sur le noeud qu'on vient de supprimer, il faut le faire pointer sur le premier dispo ou mettre un startingId vide s'il n'y a pas de candidat.
      if (startingId === idRemoved) {
        const candidate = state.present.nodesList.find(el => !isEndNode(el))
        if (candidate != null) {
          state.present.startingId = candidate.id
        } else {
          state.present.startingId = ''
        }
      }
    })
    .addCase(setStartingNode, (state, action) => {
      const { node } = action.payload
      state.present.startingId = node.id
    })
    .addCase(renameNode, (state, action) => {
      const { node, newLabel } = action.payload
      const nodeToChange = state.present.nodesList.find(el => el.id === node.id)
      if (nodeToChange != null) {
        nodeToChange.label = newLabel
      }
    })
    .addCase(changeNode, (state, action) => {
      const newNode = action.payload
      state.present.nodesList = state.present.nodesList.map(node => node.id === newNode.id ? newNode : node)
    })
    .addCase(changeNodesPositions, (state, action: { type: string, payload: ChangeNodesPositionsPayload }) => {
      for (const { id, x, y } of action.payload.movedNodes) {
        state.present.nodesList = state.present.nodesList.map(node => node.id === id
          ? {
              ...node,
              position: { x, y }
            }
          : node)
      }
    }) // C'est le même qu'au dessus. Le premier est dispatché par SceneUI après un drag d'un seul noeud
    // Celui-ci est dispatché por onStateChange en cas de replacement de noeuds ajoutés/dessinés.
    // @todo on pourrait n'en utiliser qu'un, c'est totalement redondant, ou alors on fait un changeNodePositions qui ne prends pas un array mais un seul {id,x,y}
    .addCase(changeNodesPositionsGhosted, (state, action: { type: string, payload: ChangeNodesPositionsPayload }) => {
      for (const { id, x, y } of action.payload.movedNodes) {
        state.present.nodesList = state.present.nodesList.map(node => node.id === id
          ? {
              ...node,
              position: { x, y }
            }
          : node)
      }
    })
    .addCase(addConnector, (state, action) => {
      const { id, connector } = action.payload
      const node = state.present.nodesList.find(n => n.id === id)
      if (node == null) throw Error(`Aucun nœud d’id ${id}`)
      if (isEndNode(node)) throw Error(`Impossible d’ajouter un connecteur sur un nœud fin (${id})`)
      if (node.connectors == null) node.connectors = []
      const connectorSerialized = (new Connector(connector)).serialize()
      if (connectorSerialized.source == null) connectorSerialized.source = node.id
      node.connectors.push(connectorSerialized)
    })
    .addCase(changeConnector, (state, action: { type: string, payload: ChangeConnectorPayload }) => {
      const { oldRang, newRang, connector } = action.payload
      // On récupère le node source pour manipuler sa liste de connectors
      const node = state.present.nodesList.find(n => n.id === connector.source)

      if (node != null && !isEndNode(node)) {
        // on remplace le connector par celui du payload (avant déplacement)
        node.connectors[oldRang - 1] = connector
        if (newRang > 0) {
          // on regarde s’if faut modifier l’ordre
          if (oldRang !== newRang) arrayMove(node.connectors, oldRang - 1, newRang - 1)
        } else {
          // newRang est négatif => on supprime le connector
          node.connectors.splice(oldRang, 1)
        }
      }
    })
    .addCase(removeConnector, (state, action: { type: string, payload: RemoveConnectorPayload }) => {
      const { oldRang, connector } = action.payload
      // On récupère le node source pour manipuler sa liste de connectors
      const node = state.present.nodesList.find(n => n.id === connector.source)
      // on modifie l’ordre des connectors
      if (node != null && !isEndNode(node)) {
        arrayMove(node.connectors, oldRang - 1, 0)
        node.connectors.splice(0, 1)
      }
    })
    .addCase(addGraph, (state: EditorState, action: { type: string, payload: GraphSerialized }) => {
      const present = state.present
      const { startingId, nodesList } = present
      const { startingId: startingIdNew, nodes: nodesNew } = action.payload
      const nodeListNew = Object.values(nodesNew)
      // on ne tient compte du startingId fourni que si l’on en avait pas
      if (startingId === '') state.present.startingId = startingIdNew
      if (nodesList.length === 0) {
        // il n’y avait rien dans le 'state'
        state.present.nodesList = nodeListNew
        return // inutile de retourner le 'state' car on l’a modifié
      }
      const nodesToAdd = nodeListNew.length === 2
        ? nodeListNew.filter(node => !isEndNode(node))
        : nodeListNew
      // pour chaque newNode de nodeToAdd, regarder si son id (const idCandidate = newNode.id) est déjà dans le state
      // si oui, alors getNewId, puis remplacer idCandidate dans newNode et dans tous les branchements de newList
      // quand c’est fini, concaténer newList dans nodeList
      const idsUsed = state.present.nodesList.map(n => n.id)
      for (const newNode of nodesToAdd) {
        const idCandidate = newNode.id
        if (idsUsed.includes(idCandidate)) {
          // On doit changer l’id du nœud et modifier tous les connecteurs qui en dépendent.
          newNode.id = getNewId(idsUsed) // newNode.id est de la forme nodeN
          if (!isEndNode(newNode)) {
            for (const connector of newNode.connectors) {
              connector.source = newNode.id
            }
          }
          if (newNode.label.substring(0, 5) === 'Nœud ') { // si Le label commence par Nœud, c’est sans doute parce qu’il a été fixé automatiquement par convertGraph, donc on modifie aussi son N°
            newNode.label = `Nœud ${newNode.id.substring(4)}`
          }
          idsUsed.push(newNode.id)
          for (const node of nodesToAdd) {
            if (isEndNode(node)) continue
            for (const connector of node.connectors) {
              if (connector.target === idCandidate) connector.target = newNode.id
            }
          }
        }
      }
      nodesList.push(...nodesToAdd)
    })
    .addDefaultCase((state, action) => {
      if (action.type === '@@INIT') return emptyState // pourquoi createReducer gère pas ça tout seul ???
      if (!/^@@redux/.test(action.type)) console.error(Error(`action ${action.type as string} non gérée`))
      return state
    })
})

/**
 * Le reducer global du state (avec gestion passé/futur)
 */
const undoableReducer = undoable(reducer)

export default undoableReducer
