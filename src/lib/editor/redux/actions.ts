/**
 * La liste des fonctions (action creators) qui retournent nos actions pour redux
 * On utilise createAction qui nous simplifie beaucoup la vie avec typescript
 * @module lib/editor/redux/actions
 */
import { createAction } from '@reduxjs/toolkit'
import { ConnectorSerialized } from 'src/lib/entities/Connector'
import { GraphSerialized } from 'src/lib/entities/Graph'
import { GraphNodeSerialized } from 'src/lib/entities/GraphNode'

export interface AddConnectorPayload {
  id: string
  connector: ConnectorSerialized
}

export interface ChangeConnectorPayload {
  connector: ConnectorSerialized
  oldRang: number
  newRang: number
}

export interface RemoveConnectorPayload {
  connector: ConnectorSerialized
  oldRang: number
}

export interface RenameNodePayload {
  node: GraphNodeSerialized
  newLabel: string
}

export interface ChangeNodesPositionsPayload {
  movedNodes: Array<{
    id: string,
    x: number,
    y: number
  }>
}

export interface SetStartingNodePayload {
  node: GraphNodeSerialized
}

// les actions _GHOSTED sont non annulables

export const addNode = createAction<GraphNodeSerialized>('ADD_NODE')
export const removeNode = createAction<string>('REMOVE_NODE')
export const renameNode = createAction<RenameNodePayload>('RENAME_NODE')
export const addConnector = createAction<AddConnectorPayload>('ADD_CONNECTOR')
export const addGraph = createAction<GraphSerialized>('ADD_GRAPH')
export const addGraphGhosted = createAction<GraphSerialized>('ADD_GRAPH_GHOSTED')
export const changeConnector = createAction<ChangeConnectorPayload>('CHANGE_CONNECTOR')
export const removeConnector = createAction<RemoveConnectorPayload>('REMOVE_CONNECTOR')
export const changeNode = createAction<GraphNodeSerialized>('CHANGE_NODE')
export const changeNodesPositions = createAction<ChangeNodesPositionsPayload>('CHANGE_NODES_POSITIONS')
export const changeNodesPositionsGhosted = createAction<ChangeNodesPositionsPayload>('CHANGE_NODES_POSITIONS_GHOSTED')
export const setStartingNode = createAction<SetStartingNodePayload>('SET_STARTINGNODE')
export const init = createAction('INIT')
export const redo = createAction('REDO')
export const undo = createAction('UNDO')
