# Redux

[redux](https://redux.js.org/) est un outil pour gérer l’état d’une application, en donnant des moyens simples et fiables d’accéder à cet état et de le modifier. 

On utilisera [redux toolkit](https://redux-toolkit.js.org/) qui simplifie l’écriture du code redux.

Le principe est d’avoir un "state" composé de plusieurs "slices" (à voir comme des propriétés d’un objet), avec des "actions" et des "reducers" (qui retournent un nouvel état du state suite à une action, éventuellement le même).

C'est important de retourner un nouvel état sans jamais modifier l’état courant directement (immutable state), mais l’usage de [createReducer](https://redux-toolkit.js.org/usage/usage-guide#simplifying-reducers-with-createreducer) permet de modifier directement l’objet passé en argument (c'est lui qui se charge de transformer ça pour conserver un state immutable).

Afin de se simplifier la vie, on n’a pas encore d’instance d’un objet Graph dans le state de l’éditeur, on ne garde qu'une liste de GraphNode dans un tableau (et ce sera au moment de la sauvegarde / validation que l’on créera l’objet Graph).
