/** @module lib/editor/redux/store */
import { configureStore } from '@reduxjs/toolkit'
import undoableReducer from './reducer'

const store = configureStore({
  reducer: undoableReducer
})
// et on exporte ces 3 fonctions
export const dispatch = store.dispatch.bind(store)
export const getState = store.getState.bind(store)
export const subscribe = store.subscribe.bind(store)
