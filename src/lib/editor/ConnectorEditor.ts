import * as notie from 'notie'
import { fetchEvaluations } from 'src/lib/core/loadSection'
import type { AddConnectorPayload, ChangeConnectorPayload } from 'src/lib/editor/redux/actions'
import { addConnector, changeConnector, removeConnector } from 'src/lib/editor/redux/actions'
import { dispatch } from 'src/lib/editor/redux/store'

import type { ConnectorValues } from 'src/lib/entities/Connector'
import Connector, { scoreComparators, TypeCondition } from 'src/lib/entities/Connector'
import { addElement, destroy, empty, showError } from 'src/lib/utils/dom/main'

/**
 * Format des paramètres de la méthode ConnectorEditor.edit
 */
interface EditParams {
  connectorValues: ConnectorValues
  rang: number
  rangMax: number
  isNew: boolean
  section: string
  srcLabel: string
  targetLabel: string
}

interface inputsConnector extends HTMLFormControlsCollection {
  scoreComparator: HTMLSelectElement
  scoreRef: HTMLInputElement
  typeCondition: HTMLSelectElement
  nbRuns: HTMLInputElement
  feedback: HTMLInputElement
}

class ConnectorEditor {
  sceneUIContainer: HTMLElement

  /**
   * Éditeur d’un branchement
   * @param sceneUIContainer
   */
  constructor (sceneUIContainer: HTMLDivElement) {
    this.sceneUIContainer = sceneUIContainer
  }

  /**
   * Ouvre la modale d’édition d’un branchement
   * @param connectorValues
   * @param rang
   * @param rangMax
   * @param isNew
   * @param section
   */
  async edit ({ connectorValues, rang, rangMax, isNew, section, srcLabel, targetLabel }: EditParams): Promise<void> {
    // une fonction pour effectuer la bascule rapidement entre les différents types de condition
    const toggleTypeCondition = (condition: TypeCondition | null): void => {
      // on cache tout
      scoreCondition.style.display = 'none'
      nbRunsCondition.style.display = 'none'
      peCondition.style.display = 'none'
      // et on affiche ce qui doit l'être
      switch (condition) {
        case 'pe':
          peCondition.style.display = 'block'
          break
        case 'score':
          scoreCondition.style.display = 'block'
          break
        case 'nbRuns':
          nbRunsCondition.style.display = 'block'
          break
        // dans tous les autres cas (sans condition ou rien de défini) on laisse tout masqué
      }
    }
    const connector = new Connector(connectorValues) // il y a le minimum dans connectorValues lorsqu’on crée une nouvelle connexion notamment. Connector va être complété au submit
    const form = addElement(this.sceneUIContainer, 'form', { className: 'modale connectorEditor', method: 'dialog' })
    const oldRang = rang // le rang est fourni à l’appel dans sceneUI.ts
    addElement(form, 'h2', { content: `Configuration de ce branchement (${srcLabel} => ${targetLabel})` })
    const content = addElement(form, 'div', { className: 'contenuModale' })
    addElement(content, 'span', { content: 'Rang : ' })
    let rangInput: HTMLInputElement
    if (rangMax > 1) {
      rangInput = addElement(content, 'input', {
        type: 'number',
        min: '1',
        step: '1',
        max: String(rangMax),
        value: String(rang)
      })
    } else {
      // rien à modifier, on met un span
      addElement(content, 'span', { textContent: String(rang) })
    }
    addElement(content, 'span', { content: '  (ordre d’évaluation des conditions, les conditions de rang inférieur ont été évaluées avant, la première condition remplie impose le nœud suivant)' })

    const conditionDiv = addElement(content, 'div', {
      content: 'type de branchement : '
    })
    addElement(conditionDiv, 'label', { className: 'condition', content: 'sans condition' })
    addElement(conditionDiv, 'input', {
      className: 'checkbox',
      type: 'radio',
      value: 'none',
      attrs: { name: 'typeCondition' }
    })
    addElement(conditionDiv, 'label', { className: 'condition', content: 'score' })
    addElement(conditionDiv, 'input', {
      className: 'checkbox',
      type: 'radio',
      value: 'score',
      attrs: { name: 'typeCondition' }
    })
    const evaluations = await fetchEvaluations(section)
    const evals = Object.entries(evaluations)

    if (evals?.length != null && evals.length !== 0) {
      addElement(conditionDiv, 'label', { className: 'condition', content: 'évaluation qualitative' })
      addElement(conditionDiv, 'input', {
        className: 'checkbox',
        type: 'radio',
        value: 'pe',
        attrs: { name: 'typeCondition' }
      })
    }
    addElement(conditionDiv, 'label', { className: 'condition', content: 'nombre d’éxécution' })
    addElement(conditionDiv, 'input', {
      className: 'checkbox',
      type: 'radio',
      value: 'nbRuns',
      attrs: { name: 'typeCondition' }
    })
    const scoreCondition = addElement(content, 'div', {
      content: 'Score de l’élève réel (compris entre 0 et 1) :',
      style: { display: 'none' }
    })
    const peCondition = addElement(content, 'div', { content: 'évaluation qualitative', style: { display: 'none' } })
    const nbRunsCondition = addElement(content, 'div', {
      content: 'Nombre de passage dans le nœud source ≥ ',
      style: { display: 'none' }
    })
    const addPesToEditor = async (): Promise<void> => {
      if (section == null) return showError(Error('section non définie pour le noeud source du connecteur'))
      empty(peCondition)
      for (const [ref, phrase] of evals) {
        const item = addElement(peCondition, 'span', { content: phrase, className: 'peItem' })
        addElement(item, 'input', {
          className: 'checkbox',
          type: 'checkbox',
          checked: connector?.peRefs?.includes(ref),
          value: ref
        })
      }
      // et on vérifie que toutes les peRefs initiales existent bien
      if (connector?.peRefs != null) {
        for (const ref of connector.peRefs) {
          if (evaluations[ref] == null) {
            notie.alert({
              type: 'warning',
              text: `L’évaluation qualitative ${ref} du connecteur est inconnue pour la section ${section ?? ''}`,
              stay: false,
              position: 'top',
              time: 3
            })
          }
        }
      }
    }

    // les radios pour le type de condition
    const oldTypeCondition: TypeCondition = connector.typeCondition
    // on sélectionne le type
    for (const radio of conditionDiv.querySelectorAll('input')) {
      radio.checked = radio.value === oldTypeCondition
    }

    await addPesToEditor()
    // édition du score
    const scoreComparator = addElement(scoreCondition, 'select', {
      className: 'comparatorSelect',
      value: connector.scoreComparator,
      attrs: { name: 'scoreComparator' }
    })
    addElement(scoreCondition, 'input', {
      className: 'saisieScore',
      value: String(connector.scoreRef),
      attrs: {
        name: 'scoreRef'
      }
    })
    for (const value of scoreComparators) {
      const option = addElement(scoreComparator, 'option', {
        value,
        content: value,
        attrs: { name: 'scoreComparator' }
      })
      if (value === connector.scoreComparator) option.selected = true
    }
    // édition des pe générée dans edit
    // édition du nbRuns
    addElement(nbRunsCondition, 'input', {
      type: 'number',
      value: String(connector.nbRuns),
      attrs: {
        name: 'nbRuns'
      }
    })

    toggleTypeCondition(connector.typeCondition)
    conditionDiv.addEventListener('change', () => {
      const inputs = conditionDiv.querySelectorAll('input')
      for (const input of inputs) {
        if (input.checked) {
          toggleTypeCondition(input.value as TypeCondition)
          return
        }
      }
      // aucun sélectionné
      toggleTypeCondition(null)
    })
    const feedbackZone = addElement(content, 'div', {
      textContent: 'Feedback '
    })
    addElement(feedbackZone, 'input', {
      type: 'text',
      value: connector.feedback ?? '',
      attrs: {
        name: 'feedback'
      }
    })
    addElement(content, 'button', {
      content: 'Valider',
      id: 'boutonValider',
      className: 'button',
      type: 'submit'
    })
    const boutonSupprimer = addElement(content, 'button', {
      content: 'Supprimer ce branchement',
      id: 'boutonSupprimer',
      className: 'button'
    })
    addElement(content, 'button', {
      content: 'Annuler',
      className: 'button',
      type: 'reset'
    })

    const confirmationZone = addElement(content, 'div')
    const boutonsConfirmer = addElement(confirmationZone, 'form', { content: 'Vous êtes certain de vouloir supprimer ce branchement ?' })
    boutonsConfirmer.style.display = 'none'
    addElement(boutonsConfirmer, 'button', {
      content: 'Oui',
      className: 'style.display.padding = "10px"',
      type: 'submit'
    })
    addElement(boutonsConfirmer, 'button', {
      content: 'Non',
      className: 'style.display.padding = "10px"',
      type: 'reset'
    })
    // définition des gestionnaires
    // Celui qui gère la validation du ConnectorEditor.edit()
    const submitHandler = (event: SubmitEvent): void => {
      event.preventDefault()
      try {
        if (connector == null) throw Error('Le connector à éditer n’est pas défini')
        const connectorValues: ConnectorValues = {
          target: connector.target, // ne change pas quand on édite le connecteur
          source: connector.source,
          id: connector.id
        }
        const inputs = form.elements as inputsConnector
        switch (inputs.typeCondition.value) {
          case 'none':
            connectorValues.typeCondition = 'none'
            break
          case 'pe': {
            const peRefs: string[] = []
            const peItems = peCondition.querySelectorAll('.peItem')
            for (const peItem of peItems) {
              const peCheckBox = peItem.querySelector('input')
              if (peCheckBox == null) throw Error(`Case à cocher manquante pour ${peItem.textContent ?? ''}`)
              if (peCheckBox.checked) peRefs.push(peCheckBox.value)
            }
            connectorValues.peRefs = peRefs
            connectorValues.typeCondition = 'pe'
          }
            break
          case 'score':
            connectorValues.scoreComparator = inputs.scoreComparator.value
            connectorValues.scoreRef = Number(inputs.scoreRef.value.replace(',', '.'))
            connectorValues.typeCondition = 'score'
            break
          case 'nbRuns':
            connectorValues.nbRuns = Number(inputs.nbRuns.value)
            connectorValues.typeCondition = 'nbRuns'
            break
        }
        if (rangInput) {
          const rang = Number(rangInput.value)
          if (!Number.isInteger(rang) || rang < 1 || rang > rangMax) throw Error(`Rang ${rang} invalide (doit être entre 1 et ${rangMax})`)
        }
        connectorValues.feedback = inputs.feedback.value
        const newConnector = new Connector(connectorValues)
        const errors = newConnector.getErrors()
        if (errors.length) throw Error(errors.join('\n'))
        // c’est tout bon, on masque
        destroy(form)
        // et on sauvegarde (ça va faire du dispatch)
        if (isNew) {
          const values: AddConnectorPayload = { id: newConnector.source, connector: newConnector.serialize() }
          dispatch(addConnector(values))
        } else {
          const values: ChangeConnectorPayload = { connector: newConnector.serialize(), newRang: rang, oldRang }
          dispatch(changeConnector(values))
        }
      } catch (error: any) {
        notie.alert({
          type: 'error',
          text: error instanceof Error ? error.message : error.toString(),
          stay: false,
          time: 3,
          position: 'top'
        })
        console.error(error)
      }
    }

    // celui qui gère l’annulation du ConnectorEditor.edit()
    const resetHandler = (event: Event): void => { // on ne dispatch rien du tout, le state est inchangé
      event.preventDefault()
      destroy(form)
    }

    // celui qui gère la confirmation de suppression
    const confirmHandler = (event: SubmitEvent): void => {
      event.stopPropagation()
      event.preventDefault()
      if (connector == null) throw Error('Le connector n’est pas défini, ce qui n’est pas normal dans le connectorEditor')
      if (!isNew) {
        const values = { connector: connector.serialize(), oldRang }
        dispatch(removeConnector(values))
      }
      destroy(form)
    }
    // Si on décide d’annuler la suppression
    const nonConfirmHandler = (event: Event): void => {
      boutonsConfirmer.style.display = 'none'
      boutonSupprimer.style.display = 'block'
      // on n’arrête pas les listener, on reste dans l’éditeur
      event.preventDefault()
    }
    // On veut supprimer la connexion, on gère une demande de confirmation
    const openConfirmHandler = (event: Event): void => {
      boutonsConfirmer.style.display = 'block'
      boutonSupprimer.style.display = 'none'
      event.preventDefault()
    }
    // fin de définition des gestionnaires
    // on met en place les listeners
    form.addEventListener('submit', submitHandler)
    form.addEventListener('reset', resetHandler)
    boutonSupprimer.addEventListener('click', openConfirmHandler)
    boutonsConfirmer.addEventListener('submit', confirmHandler)
    boutonsConfirmer.addEventListener('reset', nonConfirmHandler)
  }
}

export default ConnectorEditor
