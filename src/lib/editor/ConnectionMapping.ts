import { Connection } from '@jsplumb/core'

import type { ConnectorValues } from 'src/lib/entities/Connector'

/**
 * Conserve les relations entre les connectors du 'state' et les liens JsPlumb.
 * On utilise des Map par commodité.
 */
class ConnectionMapping {
  // associe le connector.id aux connexions jsp correspondantes
  jspByConnector: Map<string, Connection>
  // associe le jsp.id au connector correspondant
  connectorByJsp: Map<string, ConnectorValues>

  constructor () {
    this.jspByConnector = new Map()
    this.connectorByJsp = new Map()
  }
}

export default ConnectionMapping
