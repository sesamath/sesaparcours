charger le menu (v) 
placer les nodes sur la scene (v)
dessiner les connecteurs avec jsPlumb
    - ajouter les connecteurs par drop sur la scène (v)
    - création de connecteurs à la souris (v)
        + renseigner les paramètres du connecteur (ouverture de la modale)(v)
    - renseigner l’inventaire (connectionMapping) (v)
ajouter l’édition d’un node (picto menu contextuel)(v)
    - gérer les actions du menu (v)
    - gestion des tooltips d’aide sur les paramètres (v)
    - gestions des paramètres de type liste (à voir pour liste de booléens)
    - gérer la validation (v)
ajouter l’édition d’un branchement (clic sur son label)
    - gestion de l’événement (v)
    - création de la modale (v)
    - édition des paramètres (v)
    - gérer la validation : (à faire)
au clic sur Valider lancer le validate et afficher le retour à l’utilisateur (v)
suivant les erreurs, proposer des solutions (v)

ajouter la gestion du clic droit sur la scène (que fait-on pour les tablettes)
    - ajouter un nœud fin
    - ajouter une ressource d’après son identifiant (sesabibli/xxx)
    - ajouter une nœud d’après une section

Revoir le code du connectorEditor sur le modèle du nodeContextMenu.
