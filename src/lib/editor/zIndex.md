z-index des différents éléments

jtk-overlay:hover => 10

graphNode => 50 (120 lorsqu'il y a une demande de confirmation de suppression)

endGraphNode => 50 (120 lorsqu'il y a une demande de confirmation de suppression)

jtk-overlay.jtk-default-label => 20

jtk-overlay.jtk-default-label:hover => 60

modale => 200

menuMask => 250

context-menu => 260

editorTooltip => 300

confirmSupprDiv => 65
