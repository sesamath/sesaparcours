// @ts-ignore TS7016: Could not find a declaration file for module …
import svgCopy from 'feather-icons/dist/icons/clipboard.svg'
// @ts-ignore TS7016: Could not find a declaration file for module …
import svgPaste from 'feather-icons/dist/icons/download.svg'
// @ts-ignore TS7016: Could not find a declaration file for module …
import svgFilePlus from 'feather-icons/dist/icons/file-plus.svg'
// @ts-ignore TS7016: Could not find a declaration file for module …
import svgFolderPlus from 'feather-icons/dist/icons/folder-plus.svg'
// @ts-ignore TS7016: Could not find a declaration file for module …
import svgEnd from 'feather-icons/dist/icons/plus-square.svg'
import * as notie from 'notie'

import { loadBibli } from 'src/lib/core/bibli'
import { getSectionsList } from 'src/lib/core/loadSection'
import editNode, { addInput } from 'src/lib/editor/editNode'
import { addGraph, addNode } from 'src/lib/editor/redux/actions'
import { dispatch, getState } from 'src/lib/editor/redux/store'
import Modal from 'src/lib/entities/dom/Modal'
import Graph from 'src/lib/entities/Graph'
import GraphNode, { GraphNodeActiveSerialized, type GraphNodeSerialized } from 'src/lib/entities/GraphNode'
import { addElement, empty, showError } from 'src/lib/utils/dom/main'
import { isLegacyRessourceJ3pParametres } from '../core/checks'

import ContextMenu from './ContextMenu'

/**
 * Gère le menu contextuel de la scène pour les opérations génériques (qui ne portent pas sur un nœud ou un branchement).
 * Une seule instance est créée dans sceneUI.
 * La méthode open() de l’instance va provoquer l’ouverture du menu et tout ce qui en découle jusqu’aux traitements éventuels (dispatch)
 * Les items sont :
 * - annuler
 * - refaire
 * - ajouter un nœud d’après une section
 * - ajouter un graphe d’après un identifiant de ressource (rid)
 * - ajouter un noeud fin
 * - copier le graphe dans le presse-papier
 * - ajouter le graphe contenu dans le presse papier
 */
class SceneContextMenu extends ContextMenu {
  /**
   * Crée le menu (dans mask), avec ses listeners
   */
  protected build () {
    // construction du menu (ul et li)
    // Ajouter un nœud d’après une section
    this.addEntry({
      content: 'Ajouter un noeud',
      icon: svgFilePlus,
      tooltip: 'Ajoute un noeud à partir d’un nom de section',
      clickListener: this.addNode.bind(this)
    })

    // ajouter un graphe d’après un rid
    this.addEntry({
      content: 'Ajouter une ressource',
      icon: svgFolderPlus,
      tooltip: 'Ajoute un graphe d’après l’identifiant de la ressource (par ex sesabibli/xxx)',
      clickListener: () => {
        // on vide le menu et réutilise mask pour notre form
        this.close()
        this.show()
        const form = addElement(this.divMenu, 'form')
        const label = addElement(form, 'label', { content: 'Saisir l’identifiant de la ressource (ex sesabibli/xxx)' })
        const input = addElement(label, 'input') as HTMLInputElement
        addElement(form, 'input', { type: 'submit', value: 'OK' })
        const btnCancel = addElement(form, 'input', { type: 'button', value: 'Annuler' })
        btnCancel.addEventListener('click', () => this.close())
        form.addEventListener('submit', (event) => {
          event.preventDefault()
          this.addGraphByRid(input.value).catch(showError)
        })
      }
    })

    // ajouter un nœud fin
    this.addEntry({
      content: 'Ajouter un noeud fin',
      icon: svgEnd,
      clickListener: () => {
        this.addEndNode(parseInt(this.divMenu.style.left), parseInt(this.divMenu.style.top))
      }
    })

    // copier le graphe dans le presse-papier
    this.addEntry({
      content: 'Copier le graphe (dans le presse-papier)',
      icon: svgCopy,
      clickListener: this.copyGraph.bind(this)
    })

    // ajouter le graphe contenu dans le presse papier
    this.addEntry({
      content: 'Coller le graphe (depuis le presse-papier)',
      icon: svgPaste,
      clickListener: this.pasteGraph.bind(this)
    })
  }

  /**
   * ajout d’un nœud fin
   * @private
   */
  private addEndNode (x: number, y: number): void {
    this.close()
    // on passe par l’instanciation d’un graphNode pour qu’il affecte ses valeurs par défaut
    const node = new GraphNode({ section: '', label: 'Nœud fin', position: { x, y } })
    this.sceneUI.checkNodePosition(node)
    dispatch(addNode(node.serialize()))
  }

  private addNode (evt: MouseEvent) {
    this.close()
    // on réutilise divMenu pour notre formulaire
    const modal = new Modal({ inset: '5%', fullScreen: true, title: 'Ajouter un nœud' })
    const sectionsList = getSectionsList()
    const sectionsDataList = modal.addElement('datalist', {
      id: 'sectionsList',
      className: 'datalist'
    })
    for (const sectionName of sectionsList) {
      addElement(sectionsDataList as HTMLElement, 'option', { value: sectionName })
    }
    const container = modal.addElement('div')
    const form = addElement(container, 'form')
    const fieldSet = addElement(form, 'fieldset', { className: 'nodeEditor' })
    const inputSection = addInput(fieldSet, {
      name: 'section',
      label: 'Section',
      inputAttrs: { list: 'sectionsList' },
      tooltip: 'Choisissez la section qui va gérer ce nœud (l’exercice interactif voulu).<br>Tapez quelques caractères pour limiter les choix.',
      tooltipParent: form
    })
    const inputLabel = addInput(fieldSet, {
      name: 'label',
      label: 'Label',
      tooltip: 'Label du nœud (qui sera affiché dans ce graphe)',
      tooltipParent: form
    })

    addElement(fieldSet, 'button', { type: 'submit', content: 'valider' })
    addElement(fieldSet, 'button', { type: 'reset', content: 'annuler' })

    form.addEventListener('submit', (event) => {
      try {
        event.preventDefault()
        empty(container)
        modal.setTitle('Paramétrage du nœud ajouté')
        const section = inputSection.value
        const label = inputLabel.value || `Nœud ${section}`
        const { x: xCt, y: yCt } = this.sceneUI.container.getBoundingClientRect()
        const x = evt.clientX ? evt.clientX - xCt : 0
        const y = evt.clientY ? evt.clientY - yCt : 0
        const node = new GraphNode({ label, section, position: { x, y } })
        this.sceneUI.checkNodePosition(node)
        // on passe à l'édition
        const onClose = () => modal.destroy()
        editNode(container, node.serialize() as GraphNodeActiveSerialized, onClose)
          .catch(error => console.error(error))
      } catch (error) {
        console.error(error)
      }
    })
    form.addEventListener('reset', () => {
      modal.destroy()
    })
    modal.show()
  }

  private async addGraphByRid (rid: string): Promise<void> {
    this.close()
    const { ressource } = await loadBibli(rid)
    let graph: Graph
    if (isLegacyRessourceJ3pParametres(ressource.parametres)) {
      const { getGraphFromResource } = await import('src/lib/core/convert1to2.js')
      graph = getGraphFromResource(ressource)
    } else {
      graph = new Graph(ressource.parametres.graph)
    }
    this.sceneUI.checkGraphPositions(graph)
    dispatch(addGraph(graph.serialize()))
  }

  /**
   * Copie le graphe dans le presse papier
   * @private
   */
  private async copyGraph (): Promise<void> {
    try {
      this.close()
      const { present: { nodesList, startingId } } = getState()
      const nodes: Record<string, GraphNodeSerialized> = {}
      for (const node of nodesList) {
        nodes[node.id] = node
      }
      const graph = new Graph({ nodes, startingId })
      this.sceneUI.checkGraphPositions(graph)
      await window.navigator.clipboard.writeText(JSON.stringify(graph))
      notie.alert({
        type: 'info',
        text: 'Le graphe est copié dans le presse-papier',
        time: 3,
        position: 'top'
      })
    } catch (error) {
      console.error(error)
      notie.alert({
        type: 'error',
        text: 'Le graphe n’a pas pu être copié dans le presse-papier à cause d’une erreur',
        time: 8,
        position: 'top'
      })
    }
  }

  /**
   * Colle le graphe contenu dans le presse papier
   * @private
   */
  private async pasteGraph (): Promise<void> {
    try {
      this.close()
      const navigator = window.navigator
      const text = await navigator.clipboard.readText()
      try {
        const graphJson = JSON.parse(text)
        // à priori on colle du GraphJson, mais addGraph veut du serialized
        const graph = new Graph(graphJson)
        this.sceneUI.checkGraphPositions(graph)
        dispatch(addGraph(graph.serialize()))
        notie.alert({
          type: 'info',
          text: 'Le graphe contenu dans le presse-papier a été ajouté à la scène',
          time: 3,
          position: 'top'
        })
      } catch (error) {
        console.error(error)
        notie.alert({
          type: 'error',
          text: 'Le json contenu dans le presse-papier ne correspondait pas à un graphe',
          time: 3,
          position: 'top'
        })
      }
    } catch (error) {
      console.error(error)
      notie.alert({
        type: 'error',
        text: error instanceof Error ? error.message : String(error),
        time: 3,
        position: 'top'
      })
    }
  }
} // SceneContextMenu

export default SceneContextMenu
