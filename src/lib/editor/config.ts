// @ts-ignore TS7016: Could not find a declaration file for module …
import { addSesatheque } from 'sesatheque-client/src/sesatheques'

/** @module lib/editor/config */

// variables qui dépendent de là où on est
const isDev = /(\.local|sesamath\.dev)$/.test(window.location.hostname) || window.location.hostname === 'localhost'

/**
 * Le rid de l’arbre présenté dans l’éditeur (qui contient les ressources gérables dans un graphe)
 */
export const defaultSourceTreeRid = isDev ? 'sesabidev/50045' : 'sesabibli/50045'

if (isDev) {
  addSesatheque('sesabidev', 'https://bibliotheque.sesamath.dev/')
  addSesatheque('biblilocal3001', 'http://bibliotheque.local:3001/')
}

/**
 * Les types de ressources gérées par un graphe
 */
export const typeBibliConnus = ['am', 'em', 'j3p', 'ecjs', 'iep', 'mathgraph']
