import { Connection } from '@jsplumb/core'

import { defaultSourceTreeRid } from 'src/lib/editor/config'
import EditorTree from 'src/lib/editor/EditorTree'
import { addGraphGhosted, init } from 'src/lib/editor/redux/actions'
import type { EditorState } from 'src/lib/editor/redux/reducer'
import { emptyState } from 'src/lib/editor/redux/reducer'
import { dispatch, getState, subscribe } from 'src/lib/editor/redux/store'
import SceneUI from 'src/lib/editor/SceneUI'
import type { ConnectorValues } from 'src/lib/entities/Connector'
import Connector from 'src/lib/entities/Connector'
import Graph from 'src/lib/entities/Graph'
import { type GraphNodeActiveSerialized, type GraphNodeSerialized, isEndNode } from 'src/lib/entities/GraphNode'
import { areSameValues } from 'src/lib/utils/comparators'
import { addElement, empty, showError } from 'src/lib/utils/dom/main'

import 'notie/dist/notie.css'

export interface MappingParams {
  connector: ConnectorValues
  jspConnection?: Connection
  isNew: boolean
}

/**
 * le SceneManager est instancié dans graphEditor (main.ts)
 * c’est le SceneManager qui instancie l’interface SceneUI, l’inventaire connectionMapping
 * c’est aussi ici qu’on définit la modale cachée qui contiendra les différents formulaires d’édition
 * enfin et surtout, c’est le SceneManager qui lance les actions qui vont modifier le state,
 * et qui met à jour l’UI à chaque changement de state
 */
class SceneManager {
  sceneUI: SceneUI
  lastState: EditorState
  /** Les ids à bouger */
  lastIdsToMove: string[]

  deferredConnectors: GraphNodeActiveSerialized[]

  protected constructor (sceneUI: SceneUI) {
    this.sceneUI = sceneUI
    this.lastState = emptyState
    this.lastIdsToMove = []
    this.deferredConnectors = []
  }

  /**
   * construit la scene d’édition du graphe dans container
   * @param sourceTreeRid
   * @param container
   * @param [graph]
   */
  static async create (container: HTMLDivElement, graph?: Graph, sourceTreeRid: string = defaultSourceTreeRid): Promise<SceneManager> {
    empty(container)
    // un div pour menu et scène
    const divMain = addElement(container, 'div', { className: 'graphEditor' })
    // on ajoute notre structure avec 3 <div>
    const divMenu = addElement(divMain, 'div')
    const divScene = addElement(divMain, 'div')
    divScene.classList.add('editorScene')
    addElement(container, 'div', { className: 'editorFooter' })
    const sceneUI = new SceneUI(divScene, divMenu)
    // l’objet qui gère l’interface (et utilise jsPlumb)
    const sceneManager = new SceneManager(sceneUI)

    try {
      // construction du menu (avant la scène, elle le récupère pour changer sa taille)
      const editorTree = new EditorTree(sceneUI)
      await editorTree.init(divMenu, sourceTreeRid)
    } catch (error) {
      console.error('erreur à la construction du menu', error)
      empty(divMenu)
      addElement(divMenu, 'p', {
        content: 'Impossible de charger l’arbre des ressources\n(probablement un problème de réseau)',
        className: 'error',
        style: { width: '12rem', padding: '0.6rem', textAlign: 'center' }
      })
      divMenu.style.width = '12rem'
    }

    // on se connecte aux modifs du store
    subscribe(sceneManager.onStateChange.bind(sceneManager))

    // il faut initialiser le store
    dispatch(init())

    // si on nous passe des nœud initiaux on les met dans le state (ça mettra à jour la scene avec le subscribe précédent)
    // S’il n’y a pas de graphe, ça appellera quand même onStateChange et initialisera la scene avec l’état courant
    // du state, vide dans ce cas.
    if (!(graph instanceof Graph)) graph = new Graph()
    sceneUI.checkGraphPositions(graph)
    dispatch(addGraphGhosted(graph.serialize()))

    return sceneManager
  }

  /**
   * Retourne le graphe courant
   */
  getGraph ({ complete = false, lax = false } = {}): Graph {
    const { nodesList, startingId } = getState().present
    const nodes: Record<string, GraphNodeSerialized> = {}
    for (const node of nodesList) nodes[node.id] = node
    const graph = new Graph({ nodes, startingId })
    if (complete) {
      try {
        const msg = graph.complete({ emptyAllowed: true })
        if (msg) this.sceneUI.notif(msg)
      } catch (error) {
        if (lax) console.error(error)
        else throw error
      }
    }
    return graph
  }

  /**
   * applique les changements du state à la scène
   */
  onStateChange (): void {
    // on vide le nodeMapping pour le reconstruire à la fin
    this.sceneUI.nodeMapping.clear()
    this.deferredConnectors = []
    const newState = getState()
    // on actualise les boutons
    this.sceneUI.updateUndoRedo(newState)

    const { nodesList } = newState.present
    const { nodesList: oldNodesList } = this.lastState.present
    // on regarde déjà s’il faut supprimer des nodes
    for (const oldNode of oldNodesList) {
      // note : on n’a pas besoin de libérer les places dans la grille : à ce stade elle est nettoyée
      const newNode = nodesList.find(n => n.id === oldNode.id)
      if (newNode == null) { // le node n’existe plus dans le state, on le supprime de la scène avec toutes ses connexions
        this.sceneUI.delNode(oldNode.id)
        if (!isEndNode(oldNode)) this.sceneUI.removeAllConnections(oldNode)
      }
    }

    // en ajouter ou modifier
    for (const newNode of nodesList) {
      // Il se peut que la oldNodesList soit vide (ou ne contienne pas newNode) et que pourtant les nœuds soient déjà présent sur la scène.
      // Ça arrive par ex lorsqu’un noeud a été dessiné puis qu’un dispatch(changeNodesPositions() a eu lieu.
      // Dans ce cas, on doit avoir oldNode = newNode sinon, ça va créer un doublon dans le DOM
      const oldNode = oldNodesList
        .find(n => n.id === newNode.id) ??
        (this.sceneUI.nodesElements[newNode.id] == null ? null : newNode)
      if (oldNode == null) {
        // nouveau => on l’ajoute à la liste des connexions à réaliser (deferredConnectors)
        // on dessine le nœud. Il sera placé à la place libre la plus proche de sa position souhaitée
        this.sceneUI.drawNode(newNode)
        // on ne peut pas dessiner les connexions tant que tous les nodes n’ont pas été ajoutés, donc on diffère
        if (!isEndNode(newNode)) {
          this.deferredConnectors.push(newNode) // on ne le fait que pour les nouveaux nodes
        }
      }

      if (oldNode != null) {
        // pas nouveau donc on regarde si on doit mettre à jour quelque chose (un connecteur en plus par exemple)
        if (newNode.label !== oldNode.label) {
          this.sceneUI.majLabel(newNode)
        }

        if (!isEndNode(newNode) && !isEndNode(oldNode)) {
          // on a peut-être ajouté ou supprimé un connector ?
          // mais il faut aussi se poser la question de l’existence de la cible (car elle a pu être enlevée et pas encore remise)
          if (newNode.connectors.length !== oldNode.connectors.length) {
            for (const connector of newNode.connectors) {
              if (Object.keys(this.sceneUI.nodesElements).includes(connector.target) && Object.keys(this.sceneUI.nodesElements).includes(connector.source ?? '')) {
                if (this.sceneUI.connexionMapping.jspByConnector.get(connector.id ?? '') == null) { // le connector est nouveau, on dessine la connexion
                  const jspConnection = this.sceneUI.addConnection(connector)
                  if (jspConnection?.id == null) {
                    throw Error(`la connexion n’a pas pu être réalisée : ${JSON.stringify(connector)}`)
                  }
                  const rang = newNode.connectors.findIndex(c => areSameValues(c, connector)) + 1
                  if (rang === 0) {
                    console.error('Problème avec le connector : ', JSON.stringify(connector))
                  }

                  // cette màj est async (faut aller chercher l’export des pe de la section)
                  Connector.getCondition(connector, newNode.section)
                    .then(condition => {
                      this.sceneUI.setOverlayLabels(jspConnection, `Rang ${rang}`, condition, rang)
                    })
                    .catch(showError)

                  this.sceneUI.majConnectionMapping({ connector, jspConnection, isNew: true })
                }
              } else {
                this.deferredConnectors.push(newNode)
                break // c’est pas la peine de continuer, on s’occupera des connectors de ce nœud plus tard
              }
            }

            for (const connector of oldNode.connectors) {
              if (newNode.connectors.find((elt: ConnectorValues) => elt.id === connector.id) == null) {
                // Le connector n’existe plus, on l’enlève
                if (connector.id !== undefined) {
                  const jsp = this.sceneUI.connexionMapping.jspByConnector.get(connector.id)
                  if (jsp != null) {
                    this.sceneUI.removeConnection(jsp)
                    this.sceneUI.majConnectionMapping({ connector, isNew: false })
                  }
                }
              }
            }
          }
          // on met à jour les rangs systématiquement et le connexionMapping aussi !
          for (const connector of newNode.connectors) {
            const newRang = newNode.connectors.findIndex((elt) => elt.id === connector.id) + 1
            if (connector.id !== undefined) {
              // on mets à jour le label de l’overlay
              const jsp = this.sceneUI.connexionMapping.jspByConnector.get(connector.id)
              if (jsp != null) {
                Connector.getCondition(connector, newNode.section)
                  .then(condition => {
                    this.sceneUI.setOverlayLabels(jsp, `Rang ${newRang}`, condition, newRang)
                  })
                  .catch(showError)
                this.sceneUI.majConnectionMapping({ connector, jspConnection: jsp, isNew: false })
              }
            }
          }
        } // if !endNode
      } // oldNode != null

      if (newNode.position?.x != null && newNode.position?.y != null) {
        this.sceneUI.majPosition(newNode, newNode.position.x, newNode.position.y)
      }
      const nodeElement = this.sceneUI.nodesElements[newNode.id]
      if (nodeElement != null) {
        // ici on refait le nodeMapping complet
        this.sceneUI.nodeMapping.set(nodeElement, newNode)
      }
    }
    const oldStartingId = newState.past[newState.past.length - 1]?.startingId
    const newStartingId = newState.present.startingId
    if (oldStartingId !== newStartingId) {
      this.sceneUI.setStartingNode(newStartingId)
    }
    for (const node of this.deferredConnectors) {
      this.sceneUI.drawConnections(node)
    }

    this.sceneUI.jsPlumb.repaintEverything() // redessine toutes les connexions car dans certains cas, elles peuvent avoir été détachées (lors d’un undo changeNodesPositions par exemple)
    this.sceneUI.autosize()
    this.lastState = newState
  }
}

export default SceneManager
