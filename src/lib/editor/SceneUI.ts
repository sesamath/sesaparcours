// jsPlumb est assez pénible à utiliser, mais pas trouvé de concurrent sérieux
// y’a https://snyk.io/advisor/npm-package/DrawFlow mais visiblement on ne peut pas mettre d’étiquette sur les connexions
// la doc 'jsplumb' est succincte (pas d’exemple) et pas toujours à jour (types des bouts de code de la doc incompatibles avec la source)
// pas trouvé de projet github avec jsplumb 5.x (seulement 3 modules exotiques qui utilisent @jsplumb/browser-ui : https://www.npmjs.com/browse/depended/@jsplumb/browser-ui
// La doc dont je me suis servi est ici : https://docs.jsplumbtoolkit.com/community/5.x/

import { BrowserJsPlumbInstance, ContainmentType, DragStopEventParams, EVENT_DRAG_STOP, newInstance } from '@jsplumb/browser-ui'
import { BezierConnector } from '@jsplumb/connector-bezier'
import { ArrowOverlay, BeforeDropParams, BlankEndpoint, Connection, CustomOverlay, DotEndpoint, INTERCEPT_BEFORE_DROP } from '@jsplumb/core'

// @ts-ignore TS7016: Could not find a declaration file for module …
import svgMove from 'feather-icons/dist/icons/move.svg'

import * as notie from 'notie'
import 'notie/dist/notie.css'

import ConnectionMapping from 'src/lib/editor/ConnectionMapping'
import Grid, { type BlocPx, gridEltX, gridEltY, magnetPitchX, magnetPitchY, marginX, marginY } from 'src/lib/editor/Grid'
import NodeContextMenu from 'src/lib/editor/NodeContextMenu'
import { changeNodesPositions, redo, undo } from 'src/lib/editor/redux/actions'
import { EditorState } from 'src/lib/editor/redux/reducer'
import { dispatch } from 'src/lib/editor/redux/store'
import SceneContextMenu from 'src/lib/editor/SceneContextMenu'

import type { MappingParams } from 'src/lib/editor/SceneManager'
import { ConnectorValues, noConditionLabel } from 'src/lib/entities/Connector'
import Modal from 'src/lib/entities/dom/Modal'
import Graph from 'src/lib/entities/Graph'
import GraphNode, { type GraphNodeActiveSerialized, type GraphNodeSerialized, isEndNode } from 'src/lib/entities/GraphNode'
import type { PositionSerialized } from 'src/lib/entities/Position'

import { addElement, getFirstParent, showError } from 'src/lib/utils/dom/main'
import { stringify } from 'src/lib/utils/object'
import { v4 as uuid } from 'uuid'
import ConnectorEditor from './ConnectorEditor'

interface CustomOverlay2 extends CustomOverlay {
  canvas: HTMLElement
}

const coordX = (x: number): string => (x / gridEltX).toFixed(1)
const coordY = (y: number): string => (y / gridEltY).toFixed(1)

/**
 * Ajoute le node sur la scène
 * @param container
 * @param node
 * @param showNodeContextMenu
 */
function addNode (container: HTMLElement, node: GraphNodeSerialized, showNodeContextMenu?:(nodeElt: HTMLElement, event: MouseEvent) => void): HTMLElement {
  const nodeId = node.id
  const x = (node.position?.x ?? 0) // + gridOffsetX
  const y = (node.position?.y ?? 0) // + gridOffsetY
  // on ajoute l’élément html à notre liste interne
  const elt = addElement(container, 'div', {
    className: node.section === '' ? 'graphNodeEnd' : 'graphNode',
    style: { left: `${x}px`, top: `${y}px` }, // ce sera mis à jour par majPosition()
    attrs: { 'data-id': nodeId }
  })
  const divContent = addElement(elt, 'div', { className: 'nodeContent' })
  addElement(divContent, 'img', { className: 'movingTool', src: svgMove, title: 'Déplacer' })
  const spanId = addElement(divContent, 'span', { className: 'spanId' })
  spanId.textContent = `Id : ${node.id}`
  addElement(divContent, 'div', { content: node.label })
  const burgerMenu = addElement(elt, 'div', { className: 'nodeContextMenu', textContent: '☰', title: 'Actions sur ce nœud' })
  if (showNodeContextMenu) {
    divContent.addEventListener('mousedown', (event) => {
      if (event.button === 2) {
        showNodeContextMenu(elt, event)
      }
    })
    burgerMenu.addEventListener('mousedown', (event) => showNodeContextMenu(elt, event))
  }
  return elt
}

// cf https://docs.jsplumbtoolkit.com/community/ Le lien redirige vers la dernière version (6.x) or nous utilisons la 5.13.17
// à partir de la version 6.0, il semble qu'il n'y ait plus qu'une seule librairie qui fait tout... à voir.
// cf https://docs.jsplumbtoolkit.com/toolkit/6.x/lib/changelog

/**
 * Gère les éléments graphiques affichés sur la scène (d’apres le state) et l’interactivité (dispatch d’actions)
 */
class SceneUI {
  container: HTMLDivElement
  jsPlumb: BrowserJsPlumbInstance
  nodesElements: Record<string, HTMLElement>
  // modaleConnector: HTMLFormElement
  connectorEditor: ConnectorEditor // On peut initialiser différents formulaires d’édition qui prendront place dans la modale plutôt que de les définir à la volée
  nodeContextMenu: NodeContextMenu // menu contextuel d’édition des noeuds
  sceneContextMenu: SceneContextMenu
  /** inventaire qui permettra de faire le lien entre les connectors des nodes et les connexions JsPlumb */
  connexionMapping: ConnectionMapping
  nodeMapping: Map<HTMLElement, GraphNodeSerialized>
  grid: Grid // une grille de booléens pour le placement des nœuds sur la scène. Chaque emplacement correspond à un rectangle de gridEltX x gridEltY
  /** Le div du menu de gauche (avec l’arbre pour dropper des éléments) */
  divMenu: HTMLDivElement
  undoButton: HTMLDivElement
  redoButton: HTMLDivElement

  constructor (container: HTMLDivElement, divMenu: HTMLDivElement) {
    /**
     * Le conteneur de la scène
     */
    this.container = container
    this.divMenu = divMenu

    // undo
    this.undoButton = addElement(this.container, 'div', {
      className: 'picto undo',
      textContent: '⇦',
      title: 'Annuler la dernière action'
    })
    this.undoButton.addEventListener('click', () => {
      dispatch(undo())
    })
    // redo
    this.redoButton = addElement(this.container, 'div', {
      className: 'picto redo',
      textContent: '⇨',
      title: 'Refaire l’action annulée'
    })
    this.redoButton.addEventListener('click', () => {
      dispatch(redo())
    })
    // fullscreen
    const main = getFirstParent(this.container, 'spEditor')
    if (main == null) {
      console.error(Error('Pas trouvé de parent avec .spEditor => pas de fullscreen'))
    } else {
      addElement(this.container, 'div', { className: 'picto fullscreen', title: 'Plein écran' })
        .addEventListener('click', () => {
          if (!document.fullscreenElement) {
            main.requestFullscreen()
              .then(() => { main.classList.add('maximized') })
              .catch(error => console.error(error))
          } else {
            document.exitFullscreen()
              .then(() => { main.classList.remove('maximized') })
              .catch(error => console.error(error))
          }
        })
    }
    // picto menu contextuel de la scène
    const onContextMenu = this.showSceneContextMenu.bind(this)
    addElement(this.container, 'div', {
      className: 'picto more-vertical',
      textContent: '⁝',
      title: 'Actions possibles'
    })
      .addEventListener('click', onContextMenu)
    // idem au clic droit
    this.container.addEventListener('contextmenu', onContextMenu)

    // picto resize en bas à droite
    addElement(this.container, 'div', { className: 'picto upSize', textContent: '↧', title: 'Agrandir' })
      .addEventListener('click', () => {
        let { height } = this.container.getBoundingClientRect()
        height += 75
        this.container.style.height = `${height}px`
        // faut harmoniser avec le menu
        this.divMenu.style.height = `${height}px`
        // et retirer le 100vh sur le parent
        const j3pContainer = this.container.parentElement?.parentElement
        if (j3pContainer?.style.height) j3pContainer.style.removeProperty('height')
      })
    addElement(this.container, 'div', { className: 'picto downSize', textContent: '↥', title: 'Réduire' })
      .addEventListener('click', () => {
        let { height } = this.container.getBoundingClientRect()
        height -= 75
        this.container.style.height = `${height}px`
        // faut harmoniser avec le menu
        this.divMenu.style.height = `${height}px`
        // et retirer le 100vh sur le parent
        const j3pContainer = this.container.parentElement?.parentElement
        if (j3pContainer?.style.height) j3pContainer.style.removeProperty('height')
      })
    // et faut initialiser à la taille de départ
    const { height } = this.container.getBoundingClientRect()
    this.divMenu.style.height = `${height}px`

    this.connexionMapping = new ConnectionMapping()
    this.nodeMapping = new Map<HTMLElement, GraphNodeSerialized>()

    /**
     * Le state courant (donné par redux).
     */
    // on initialise le contenu des modales à travers une
    this.connectorEditor = new ConnectorEditor(this.container)
    this.nodeContextMenu = new NodeContextMenu(this.container, this)
    this.sceneContextMenu = new SceneContextMenu(this.container, this)
    /**
     * Notre liste de nodes (les div)
     */
    this.nodesElements = {}
    /**
     * La grille de placement des noeuds
     */
    this.grid = new Grid()

    // le type de connecteur par défaut (si on le colle directement dans l’objet passé à newInstance tsc n’est pas content, il veut pas de anchor)
    const connector = {
      type: BezierConnector.type, // cf https://docs.jsplumbtoolkit.com/toolkit/6.x/lib/connectors
      anchor: 'Continuous', // cf https://docs.jsplumbtoolkit.com/toolkit/6.x/lib/anchors
      detachable: false,
      reattach: false,
      options: {
        curviness: 50
      }
    }

    // on crée l’instance
    this.jsPlumb = newInstance({
      container,
      connectionsDetachable: false,
      dragOptions: {
        // pour empêcher de sortir un nœud de la scène
        containment: ContainmentType.parentEnclosed, // cf https://docs.jsplumbtoolkit.com/community/lib/dragging#via-the-containment-property
        grid: { w: magnetPitchX, h: magnetPitchY }
      },
      maxConnections: -1,
      endpoints: [{
        type: DotEndpoint.type,
        // on peut mettre en option `cssClass: 'connectorEndpoint'` mais c'est pas utile (on le chope avec  .jtk-endpoint
        // l'option `hoverClass: 'connectorEndpointOver'` marche pas (pourtant la doc indique que ça devrait)
        // on peut le choper avec `.jtk-source-hover rect`
        options: { radius: 4 }
      }, {
        type: BlankEndpoint.type, options: {}
      }],
      paintStyle: { stroke: 'currentColor', strokeWidth: 1 },
      hoverPaintStyle: { strokeWidth: 2 },
      connectionOverlays: [
        {
          type: 'Custom',
          options: {
            /**
             * méthode pour créer l’overlay Custom (on définit les listeners qui vont modifier le contenu)
             * @param component la connexion jsPlumb
             */
            create: (component: HTMLElement) => {
              const d = addElement(this.container, 'div', { className: 'jtk-default-label' })
              addElement(d, 'p', { content: 'Rang ?', className: 'connectorLabelDefault' })
              addElement(d, 'p', { content: 'score>=0', className: 'connectorLabelHover' })
              d.addEventListener('click', (event) => {
                event.preventDefault()
                this.onClickConnection(component.id)
              })
              return d
            },
            location: 0.9
          }
        },
        {
          type: ArrowOverlay.type, // cf https://docs.jsplumbtoolkit.com/toolkit/6.x/lib/overlays
          options: {
            location: 1,
            width: 7,
            length: 7
          }
        }
      ],
      connector
    })
    // permet d'enregistrer les graphNodes comme cible de connexions
    this.jsPlumb.addTargetSelector('.graphNode', {
      canAcceptNewConnection: (/* elt, event */) => true,
      target: true
    })
    // Un endNode ne peut pas être source d'une connexion, mais cible, oui.
    this.jsPlumb.addTargetSelector('.graphNodeEnd', {
      canAcceptNewConnection: (/* elt, event */) => true,
      source: false,
      target: true
    })
    // enregistre les grapheNodes comme source de connexion
    this.jsPlumb.addSourceSelector('.graphNode', {
      canAcceptNewConnection: (/* elt, event */) => true,
      source: true
    })
    // voir si les interceptors sont encore supportés en version >5.6 cf https://docs.jsplumbtoolkit.com/toolkit/6.x/lib/changelog#561
    this.jsPlumb.bind(INTERCEPT_BEFORE_DROP, (param: BeforeDropParams): boolean => {
      const sourceNode = this.nodeMapping.get(param.connection.source) as GraphNodeActiveSerialized // peut pas être un nœud fin
      const targetNode = this.nodeMapping.get(param.connection.target)
      if (sourceNode == null || targetNode == null) throw Error('Intercept before drop : Je n’ai pas récupéré la source et la destination')
      // on a nos deux nodes
      for (const c of sourceNode.connectors) {
        if (c.target === targetNode.id) {
          if (c.target !== sourceNode.id) {
            notie.alert({
              type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
              text: 'Le connecteur existe déjà, vous pouvez cliquer dessus pour en éditer les paramètres',
              stay: false, // optional, default = false
              time: 3, // optional, default = 3, minimum = 1,
              position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
            })
          }
          // on doit supprimer la connection
          return false
        }
      }
      try {
        this.connectorEditor.edit({
          connectorValues: {
            source: sourceNode.id,
            target: targetNode.id,
            id: uuid()
          },
          rang: sourceNode.connectors.length + 1,
          rangMax: sourceNode.connectors.length + 1,
          isNew: true,
          section: sourceNode.section,
          srcLabel: sourceNode.label,
          targetLabel: targetNode.label
        }).catch(showError)
      } catch (error) {
        showError(error)
      }
      return false // On empêche jsPlumb de réaliser la connexion à la souris, c’est le onStateChange() va s’en charger.
    })

    // au drop d'un nœud ça fera un dispatch pour mettre à jour la position
    // ça c'est le listener du drag&drop d'un noeud existant vers une nouvelle position.
    this.jsPlumb.bind(EVENT_DRAG_STOP, (param: DragStopEventParams): void => {
      const element = param.el as HTMLElement
      if (!element.classList.contains('graphNode') && !element.classList.contains('graphNodeEnd')) {
        // c'est un drag pas pour nous (si c'est une connexion qui est lâchée et pas un noeud, c'est géré par le interceptBeforeDrop)
        return
      }

      const node = this.nodeMapping.get(element)
      if (node?.position?.x == null || node?.position?.y == null) {
        return console.error(Error('Le noeud déplacé n´a pas de position initiale'))
      }
      const newX = element.offsetLeft
      const newY = element.offsetTop
      const width = element.clientWidth
      const height = element.clientHeight
      console.debug(`Avant déplacement la grille est\n${this.grid}`)
      this.removeInGrid({ x: node.position.x, y: node.position.y, width, height })
      console.debug(`On veut placer un noeud x: ${coordX(newX)}-${coordX(newX + element.clientWidth)}), y: ${coordY(newY)}-${coordY(newY + element.clientHeight)}) dans\n${this.grid}`)
      const { x, y } = this.placeInGrid({
        x: newX,
        y: newY,
        width,
        height
      })
      console.debug(`Et il a trouvé sa place en (${x},${y}), coords (${Math.round(x / gridEltX)}, ${Math.round(y / gridEltY)})`)
      // @todo si x ou y a bougé de plus de 2 cases appliquer un petit effet de glow pendant 2~3s (pour voir où s'est barré le node)
      dispatch(changeNodesPositions({ movedNodes: [{ id: node.id, x, y }] }))
    })

    // Au début du drag d'un node, on pourrait libérer sa position dans la grille
    // mais vu qu'on ne peut faire qu'un seul drag à la fois, on fait libération + ré-occupation + dispatch dans le drop ci-dessus
    // (listener EVENT_DRAG_START viré le 2024-07-10)
  }

  /**
   * Appelée par le 'onStateChange' pour ajouter une connexion.
   * @param connector
   */
  addConnection (connector: ConnectorValues): Connection | null {
    const { target, source } = connector
    const nodeId = source
    if (nodeId == null) {
      throw Error('addConnection : Il y a un problème avec ce connector qui n’a pas de source')
    }
    const srcNode = this.nodesElements[nodeId]
    const dstNode = this.nodesElements[target]
    if (srcNode != null && dstNode != null) {
      // visiblement ça sert à rien de passer par addEndpoint si on n’a pas d’options à filer
      // const source = this.jsPlumb.addEndpoint(srcNode)
      // const target = this.jsPlumb.addEndpoint(dstNode)
      return this.jsPlumb.connect({
        source: srcNode,
        target: dstNode,
        anchor: 'Continuous'
      })
    } else {
      return null
    }
  }

  /**
   * Affecte minWidth et minHeight au container d’après la position des nodes
   */
  autosize (): void {
    let maxX = 0
    let maxY = 0
    const nodes = [...this.container.querySelectorAll('.graphNode'), ...this.container.querySelectorAll('.graphNodeEnd')] as HTMLElement[]
    for (const node of nodes) {
      const { bottom, right } = node.getBoundingClientRect()
      if (right > maxX) maxX = right
      if (bottom > maxY) maxY = bottom
    }
    const { top, left, height } = this.container.getBoundingClientRect()
    this.container.style.minWidth = `${maxX - left + 10}px`
    const minHeight = maxY - top + 10
    this.container.style.minHeight = `${minHeight}px`
    // faut harmoniser avec le menu
    if (this.divMenu != null) this.divMenu.style.height = `${Math.max(minHeight, height)}px`
  }

  /**
   * Listener du click sur l'overlay
   * @param {string} jspId
   */
  onClickConnection (jspId: string): void {
    try {
      const connectorValues = this.connexionMapping.connectorByJsp.get(jspId)
      if (connectorValues == null) throw Error(`Le connector associé à la connexion jsp ${jspId} n’est pas présent dans l’inventaire`)
      if (connectorValues.source == null) throw Error(`Le connector associé à la connexion jsp ${jspId} n’a pas de noeud source identifié`)
      const srcElt = this.nodesElements[connectorValues.source]
      if (srcElt == null) throw Error(`impossible de retrouver le nœud source ( ${connectorValues.source ?? ''}) dans les éléments de la scène`)
      const srcNode = this.nodeMapping.get(srcElt)
      if (srcNode == null) throw Error('Le nœud source a disparu')
      if (isEndNode(srcNode)) throw Error('Le nœud source de ce connecteur est un nœud fin')
      const targetElt = this.nodesElements[connectorValues.target]
      if (targetElt == null) throw Error(`Impossible de retrouver le nœud destination (${connectorValues.target ?? ''}) dans les éléments de la scène`)
      const targetNode = this.nodeMapping.get(targetElt)
      if (targetNode == null) throw Error('Le nœud destination a disparu')
      const rangMax = srcNode.connectors.length
      const rang = srcNode.connectors.findIndex(c => c.id === connectorValues.id) + 1
      this.connectorEditor.edit({
        connectorValues,
        rang,
        rangMax,
        isNew: false,
        section: srcNode.section,
        srcLabel: srcNode.label,
        targetLabel: targetNode.label
      })
        .catch(showError)
    } catch (error) {
      showError(error)
    }
  }

  // En cas de suppression d’un node de la scène, il faut nettoyer l’inventaire des connexions qui en partent
  removeAllConnections (node: GraphNodeActiveSerialized): void {
    for (const connector of node.connectors) {
      const jsp: Connection | undefined = this.connexionMapping.jspByConnector.get(connector.id ?? '')
      if (jsp != null) this.connexionMapping.connectorByJsp.delete(jsp.id)
      this.connexionMapping.jspByConnector.delete(connector.id ?? '')
    }
  }

  /**
   * callback passée en paramètre de SceneUI.drawConnexions() pour mettre à jour l’inventaire
   * drawConnexion fournit le connector avec source et l’id de la connexion JsPlumb
   */
  majConnectionMapping ({ connector, jspConnection, isNew }: MappingParams): boolean {
    if (isNew && this.connexionMapping.jspByConnector.get(connector.id ?? '') != null) {
      return false
    }
    if (jspConnection == null) { // dans ce cas on enlève la connexion du mapping
      const oldJsp = this.connexionMapping.jspByConnector.get(connector.id ?? '')
      if (oldJsp != null) {
        this.connexionMapping.connectorByJsp.delete(oldJsp.id)
      }
      if (connector.id != null) {
        this.connexionMapping.jspByConnector.delete(connector.id)
      }
      return true
    }
    if (connector.id != null) {
      this.connexionMapping.jspByConnector.set(connector.id, jspConnection)
      this.connexionMapping.connectorByJsp.set(jspConnection.id, connector)
      return true
    }
    return false
  }

  /**
   * Ajoute le node sur la scène
   */
  drawNode (node: GraphNodeSerialized): void {
    if (node.id != null && this.nodesElements[node.id] != null) {
      throw Error(`Le nœud ${node.id} est déjà affiché`)
    }
    const elt = addNode(this.container, node, this.showNodeContextMenu.bind(this))
    // le node a déjà été mis dans la grille avant le dispatch, faut pas refaire de placeInGrid ici
    this.nodesElements[node.id] = elt
    this.jsPlumb.addEndpoint(elt, { endpoint: 'Blank' })
    this.nodeMapping.set(elt, node)
  }

  /**
   * Affiche les connexions JsPlumb qui partent de ce node
   */
  drawConnections (node: GraphNodeActiveSerialized): void {
    for (const [index, connector] of node.connectors.entries()) {
      const jspConnection = this.addConnection(connector)
      if (!jspConnection) {
        console.error(Error(`Pas trouvé l’overlay du label pour le connector d’index ${index} du node ${node.id} : ${stringify(connector)}`))
        return
      }
      if (jspConnection.id == null) {
        console.error(Error('jspConnection sans id'), jspConnection)
      } else {
        this.setOverlayLabels(jspConnection, `Rang ${index + 1}`, noConditionLabel, index + 1)
      }
      // màj mapping
      if (!this.majConnectionMapping({
        connector,
        jspConnection,
        isNew: true
      })) {
        // la connexion est déjà dans l’inventaire donc on supprime la nouvelle
        this.jsPlumb.deleteConnection(jspConnection)
      }
    }
  }

  /**
   * Enlève un node de la scène
   */
  delNode (id: string): void {
    const nodeElt = this.nodesElements[id]
    if (nodeElt == null) {
      // vu qu’on voulait le virer on ne plante pas, mais c’est quand même pas très normal…
      console.error(Error(`Le node ${id} n’existe plus, rien à supprimer`))
    } else {
      // faut libérer sa place dans la grille
      const x = nodeElt.offsetLeft
      const y = nodeElt.offsetTop
      const width = nodeElt.clientWidth
      const height = nodeElt.clientHeight
      this.removeInGrid({ x, y, width, height })
      // on supprime tous ses connecteurs
      this.jsPlumb.select({ source: this.nodesElements[id] }).deleteAll()
      // et le node lui-même
      nodeElt.parentNode?.removeChild(nodeElt)
      delete this.nodesElements[id]
      this.nodeMapping.delete(nodeElt)
    }
  }

  /**
   * appelé par le onStateChange lorsqu'un connecteur est supprimé
   * @param {Connection} jsp
   */
  removeConnection (jsp: Connection): void {
    this.jsPlumb.deleteConnection(jsp)
  }

  /**
   * Met à jour le label d’un node déjà sur la scène
   * @throws {Error} si on ne trouve pas l’élément html à modifier
   */
  majLabel ({ id, label }: GraphNodeSerialized): void {
    const nodeElement = this.nodesElements[id]
    if (nodeElement == null) {
      throw Error(`Aucun nœud d’id ${id}`)
    }
    const divLabel = nodeElement.querySelector('.nodeContent > div')
    if (divLabel == null) throw Error('Aucun élément à mettre à jour')
    divLabel.textContent = label
  }

  /**
   * change le label sur le connecteur
   */
  setOverlayLabels (connexion: Connection, labelDefault: string, labelHover: string, index: number): void {
    const overlay = Object.values(connexion.overlays).find(el => el.type === 'Custom') as CustomOverlay2
    if (overlay.canvas == null) {
      throw Error(`Pas trouvé l’overlay du label pour la connexion ${connexion}`)
    }
    const contentLabelDefault = overlay.canvas.querySelector('.connectorLabelDefault')
    if (contentLabelDefault) {
      contentLabelDefault.innerHTML = `${labelDefault}`
    }
    const contentLabelHover = overlay.canvas.querySelector('.connectorLabelHover')
    if (contentLabelHover) {
      contentLabelHover.innerHTML = labelHover
      const parent = contentLabelHover.parentElement
      if (parent) {
        if (labelHover === noConditionLabel) {
          parent.classList.add('noCondition')
        } else {
          parent.classList.remove('noCondition')
        }
      }
    }
    overlay.location = 0.3 + (index % 10) / 20
    this.jsPlumb.repaintEverything()
  }

  /**
   * Déplace un nœud sur la scene.
   */
  majPosition (node: GraphNodeSerialized, x: number, y: number): void {
    const { id } = node
    const nodeElement = this.nodesElements[id]
    if (nodeElement == null) return console.error(Error(`Aucun nœud d’id ${id}`))
    nodeElement.style.left = `${String(x)}px`
    nodeElement.style.top = `${String(y)}px`
  }

  notif (message: string, { delay = 0 } = {}): void {
    const modal = new Modal({ title: message, delay })
    modal.show()
  }

  /**
   * Modifie éventuellement la position du node s'il collisionne, et réserve sa place dans la grille
   * @param {GraphNode} node
   */
  checkNodePosition (node: GraphNode): void {
    const { x = 0, y = 0 } = node.position
    const elt = addNode(this.container, node)
    // On ne connait pas sa taille => faut le dessiner
    const width = elt.clientWidth
    const height = elt.clientHeight
    // on a ses dimensions, on peut le virer
    this.container.removeChild(elt)
    // on vérifie le rectangle
    const p = this.placeInGrid({ x, y, width, height })
    // màj dans le node
    node.position.x = p.x
    node.position.y = p.y
    // @todo si x ou y a bougé de plus de 2 cases appliquer un petit effet de glow pendant 2~3s (pour voir où s'est barré le node)
  }

  /**
   * Modifie les positions des nodes de graph qui collisionneraient sur la scène (et réserve leur place dans la grille)
   * @param graph
   */
  checkGraphPositions (graph: Graph): void {
    for (const node of Object.values(graph.nodes)) {
      this.checkNodePosition(node)
    }
  }

  /**
   * Actualise les boutons de l'interface en fonction de l'état du state (avant, c'était fait dans le onStateChange, mais on l'a déplacé ici parce qu'il s'occupe de l'UI) appelé par le onStateChange en début de traitement
   * @param {EditorState} state
   */
  updateUndoRedo (state: EditorState) {
    // On masque les boutons Undo/Redo selon l’historique
    const undoButton = this.container.querySelector('.undo') as HTMLButtonElement
    if (undoButton != null) {
      if (state.past.length === 0) {
        undoButton.style.display = 'none'
      } else {
        undoButton.style.display = 'block'
      }
    }
    const redoButton = this.container.querySelector('.redo') as HTMLButtonElement
    if (redoButton != null) {
      if (state.future.length === 0) {
        redoButton.style.display = 'none'
      } else {
        redoButton.style.display = 'block'
      }
    }
  }

  /**
   * Met à jour les classList des noeuds présents sur la scène pour identifier le startingNode (appelé par le onStateChange en cas de suppression du startingNode ou de la mise à jour du startingId)
   * @param {string} id
   * @return {boolean}
   */
  setStartingNode (id: string): boolean {
    const elt = this.nodesElements[id]
    if (elt == null) return false
    const startingNode = Object.values(this.nodesElements).find(el => el.classList.contains('startingNode'))
    if (startingNode != null) {
      if (id !== startingNode.id) {
        // on doit changer le startingId qui passe de l'ancien au nouveau
        startingNode.classList.remove('startingNode')
        elt.classList.add('startingNode')
        return true
      }
      return true // c'est le même que précédemment, on ne fait rien mais normalement on ne devrait pas avoir de setStartingNode si c'est le cas
    }
    // ici il n'y avait pas de startingNode avant
    elt.classList.add('startingNode')
    return true
  }

  /**
   * Case le rectangle dans la grille et retourne la position où c'est fait
   * @param x
   * @param  y
   * @param  width
   * @param  height
   */
  protected placeInGrid ({ x, y, width, height }: BlocPx): PositionSerialized {
    // on décale un peu vers le haut et la gauche, pour éviter qu'un décalage de qq pixels fasse un bond (et pour que ça revienne où c'était si c'était pile poil dans un trou)
    let pos = {
      x: Math.max(0, x - marginX / 2),
      y: Math.max(0, y - marginY / 2)
    }
    // ça va arrondir à un nombre entier de cases
    pos = this.grid.firstFreePosition({ x, y, width, height })
    // on occupe un peu plus grand
    width += marginX
    height += marginY
    this.grid.occupy({ ...pos, width, height })
    return pos
  }

  /**
   * Retire le rectangle de la grille
   * @param x
   * @param y
   * @param width
   * @param height
   */
  removeInGrid ({ x, y, width, height }: BlocPx): void {
    // x,y devraient être ceux retournés par placeInGrid, faut pas décaler mais faut mettre la même marge que lui
    width += marginX
    height += marginY
    this.grid.free({ x, y, width, height })
  }

  /**
   * Affiche le menu contextuel générique
   * @param event
   */
  protected showSceneContextMenu (event: MouseEvent): void {
    event.preventDefault() // pour empêcher jsPlumb de récupérer ce click
    if (event.target !== event.currentTarget && event.button === 2) return
    this.sceneContextMenu.open(event)
  }

  /**
   * Affiche le menu contextuel d’un nœud (clic droit sur le node ou clic sur l’icône 'more-vertical')
   * @param {HTMLElement} nodeElt
   * @param {MouseEvent} event
   */
  protected showNodeContextMenu (nodeElt: HTMLElement, event: MouseEvent): void {
    // on est un listener, faut du try/catch
    try {
      event.preventDefault()
      this.nodeContextMenu.openNode(nodeElt, event)
    } catch (error) {
      showError(error)
    }
  }
}

export default SceneUI
