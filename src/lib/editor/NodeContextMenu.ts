// @ts-ignore TS7016: Could not find a declaration file for module …
import svgEye from 'feather-icons/dist/icons/eye.svg'
// @ts-ignore TS7016: Could not find a declaration file for module …
import svgSetting from 'feather-icons/dist/icons/settings.svg'
// @ts-ignore TS7016: Could not find a declaration file for module …
import svgTrash from 'feather-icons/dist/icons/trash.svg'
// @ts-ignore TS7016: Could not find a declaration file for module …
import svgRename from 'feather-icons/dist/icons/type.svg'

import * as notie from 'notie'

import editNode from 'src/lib/editor/editNode'
import ContextMenu from 'src/lib/editor/ContextMenu'
import { removeNode, renameNode, setStartingNode } from 'src/lib/editor/redux/actions'
import { dispatch } from 'src/lib/editor/redux/store'
import Modal from 'src/lib/entities/dom/Modal'
import Graph from 'src/lib/entities/Graph'
import { type GraphNodeActiveSerialized, type GraphNodeSerialized, isEndNode } from 'src/lib/entities/GraphNode'
import play from 'src/lib/player/index'

import { addElement, showError } from 'src/lib/utils/dom/main'

/**
 * Gère le menu contextuel d’édition des nœud
 * Une seule instance est créée dans sceneUI
 * c’est la méthode open() de l’instance qui va provoquer l’ouverture du menu et tout ce qui va en découler jusqu’aux traitements éventuels (dispatch)
 *
 */
class NodeContextMenu extends ContextMenu {
  currentNode!: GraphNodeSerialized | null

  /**
   * Crée le menu contextuel du node (dans mask), avec ses listeners
   */
  openNode (nodeElt: HTMLElement, event: MouseEvent): void {
    this.currentNode = this.sceneUI.nodeMapping.get(nodeElt) ?? null
    super.open(event)
  }

  protected build () {
    const node = this.currentNode
    if (node == null) throw Error('Aucun nœud pour construire son menu contextuel')
    this.addEntry({
      content: 'Renommer',
      icon: svgRename,
      tooltip: 'Renommer ce nœud (pour cet éditeur et le visualiseur de parcours)',
      clickListener: this.renameNode.bind(this, node)
    })
    if (!isEndNode(node)) {
      this.addEntry({
        content: 'Paramétrer',
        icon: svgSetting,
        tooltip: 'Paramétrer cet exercice',
        clickListener: this.editNode.bind(this, node)
      })
      this.addEntry({
        content: 'Tester',
        icon: svgEye,
        tooltip: 'Tester cet exercice',
        clickListener: this.testRessource.bind(this, node)
      })
      this.addEntry({
        content: 'Définir le noeud départ',
        icon: svgSetting,
        tooltip: 'Définir ce noeud comme noeud de départ du graphe',
        clickListener: this.setStartingNode.bind(this, node)
      })
    }
    this.addEntry({
      content: 'Supprimer',
      icon: svgTrash,
      tooltip: 'Supprimer le nœud et les connecteurs associés',
      clickListener: this.deleteNode.bind(this, node)
    })
  }

  private async editNode (node: GraphNodeActiveSerialized): Promise<void> {
    try {
      this.close()
      // la modale de notre form
      const modal = new Modal({ inset: '5%', fullScreen: true, title: `Configuration du nœud : ${node.label}` })
      const container = modal.addElement('div')
      await editNode(container, node, () => {
        modal.destroy()
      })
      // reste à afficher tout ça
      modal.show()
    } catch (error) {
      console.error()
    }
  }

  /**
   * Gère l’option Supprimer
   */
  private deleteNode (node: GraphNodeSerialized): void {
    this.close()
    this.mask.style.zIndex = '120'
    const confirmSuppr = addElement(this.ulParent, 'div', {
      className: 'confirmSupprDiv',
      content: 'Vous êtes certain de vouloir supprimer ce nœud, tous les branchements qui en partent, et tous ceux qui y arrivent ?'
    })
    confirmSuppr.style.color = 'red'
    const yes = addElement(confirmSuppr, 'button', { content: 'Oui' })
    const no = addElement(confirmSuppr, 'button', { content: 'Non' })
    yes.style.margin = '10px'
    this.show()
    yes.addEventListener('click', () => {
      this.ulParent.removeChild(confirmSuppr)
      this.close()
      if (node != null) {
        const elt = this.sceneUI.nodesElements[node.id]
        if (elt == null) {
          console.error(Error('demande de suppression d’un node qui a déjà disparu de la scène'))
        } else {
          this.sceneUI.removeInGrid({
            // le ?? 0 est là pour calmer TS mais ça ne devrait jamais arriver
            // (le node sort de redux et il est sur la scene)
            x: node.position?.x ?? 0,
            y: node.position?.y ?? 0,
            width: elt.clientWidth,
            height: elt.clientHeight
          })
        }
        dispatch(removeNode(node.id))
      }
    })
    no.addEventListener('click', () => {
      this.ulParent.removeChild(confirmSuppr)
      this.close()
    })
  }

  /**
   * Gère l’option Renommer
   * @todo si on renomme avec un nom plus court ou plus long, la taille de l'élément sur la scène change. Il faudrait mettre à jour la grille en conséquence (mais ça risque d'être compliqué s'il est au milieu de plein d'autres noeuds... surtout si le nom augmente la taille.
   */
  private renameNode (node: GraphNodeSerialized): void {
    this.close()
    const renameInput = addElement(this.ulParent, 'form', { className: 'renameNodeDiv' })
    const textInput = addElement(renameInput, 'input', {
      type: 'text',
      attrs: { name: 'label' },
      required: true,
      value: node?.label ?? ''
    }) as HTMLInputElement
    addElement(renameInput, 'button', { type: 'submit', content: 'Valider' })
    addElement(renameInput, 'button', { type: 'reset', content: 'Annuler' })
    this.show()
    renameInput.addEventListener('submit', (event) => {
      try {
        event.preventDefault()
        if (node != null) {
          dispatch(renameNode({ node, newLabel: textInput.value }))
        }
        this.close()
      } catch (error) {
        showError(error)
      }
    })
    renameInput.addEventListener('reset', (event) => {
      try {
        event.preventDefault()
        this.close()
      } catch (error) {
        showError(error)
      }
    })
  }

  private setStartingNode (node: GraphNodeSerialized): void {
    dispatch(setStartingNode({ node }))
  }

  /**
   * Gère l’option Tester
   * @param node
   * @private
   */
  private async testRessource (node: GraphNodeSerialized): Promise<void> {
    if (node.section != null) {
      const modal = new Modal({ fullScreen: true, inset: '2%' })
      const playingDiv = modal.addElement('div', { className: 'playground' })
      modal.show()
      const graph = new Graph({
        nodes: {
          n1: {
            section: node.section,
            maxRuns: 1,
            connectors: [{
              target: 'fin',
              typeCondition: 'none',
              feedback: ''
            }]
          },
          fin: {
            section: ''
          }
        },
        startingId: 'n1'
      })
      await play(playingDiv, { graph })
      modal.destroy()
    } else {
      notie.alert({
        type: 'warning',
        text: 'Ce nœud ne présente pas de section à tester',
        stay: false,
        time: 5,
        position: 'top'
      })
    }
    this.close()
  }
} // NodeContextMenu

export default NodeContextMenu
