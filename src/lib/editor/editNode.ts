import type { Placement } from '@floating-ui/dom'
import { formatCheckResult } from 'src/lib/core/checks'
import { maxRunCeil } from 'src/lib/core/constantes'
import { fetchParameters } from 'src/lib/core/loadSection'
import { getText } from 'src/lib/core/textes'
import { addNode, changeNode } from 'src/lib/editor/redux/actions'
import { dispatch } from 'src/lib/editor/redux/store'
import Modal from 'src/lib/entities/dom/Modal'
import type { GraphNodeActiveSerialized, GraphNodeSerialized, ParamsValues, ParamValue } from 'src/lib/entities/GraphNode'
import type { CheckResults, PlainObject, SectionParameter, SectionParameters, SectionParameterType, SectionParameterTypes, ValidateParametersFn } from 'src/lib/types'
import { addElement, destroy, getFirstParentByTag, showError } from 'src/lib/utils/dom/main'
import { addTooltip, AddTooltipOptions } from 'src/lib/widgets/tooltip'

type AddInputOptions = {
  label?: string,
  name: string,
  inputAttrs?: PlainObject,
  inputOpts?: PlainObject,
  tooltip?: string
  tooltipParent?: HTMLElement
  tooltipPlacement?: Placement
}

/**
 * Ouvre la modale d'édition du node (avec tous les champs et la gestion de la validation)
 * @param container
 * @param node
 * @param onClose
 */
async function editNode (container: HTMLElement, node: GraphNodeActiveSerialized, onClose: () => void): Promise<void> {
  // On va avoir besoin des paramètres modifiables de la section et de son titre par défaut.
  const { parameters, updateParameters, validateParameters } = await fetchParameters(node.section)
  // faut cloner car node.params est readOnly (il sort du store redux)
  // et ça planterait dans updateParameters
  const nodeParams = { ...node.params }
  if (updateParameters) {
    updateParameters(nodeParams)
  }
  // console.log('nodeParams', nodeParams)

  const form = addElement(container, 'form')
  const fieldSet = addElement(form, 'fieldset', { className: 'nodeEditor' })

  /** Ajoute un champ input dans notre form */
  const addInputF = ({ label, name, inputOpts, tooltip }: AddInputOptions) => addInput(fieldSet, { label, name, inputOpts, tooltip, tooltipParent: form })

  // label
  addInputF({
    label: 'label',
    name: 'label',
    inputOpts: { type: 'text', value: node.label },
    tooltip: 'Label du noeud, dans l’éditeur et l’afficheur de bilans'
  })

  // titre
  addInputF({
    label: 'titre',
    name: 'title',
    inputOpts: { type: 'text', value: node.title || parameters.title.defaultValue },
    tooltip: 'Titre affiché à l’exécution de l’exercice'
  })

  // maxRuns
  addInputF({
    label: getText('maxRuns'),
    name: 'maxRuns',
    inputOpts: {
      type: 'number',
      min: 1,
      max: String(maxRunCeil),
      step: 1,
      value: String(node.maxRuns ?? 1)
    },
    tooltip: 'Nombre maximum d’exécution(s) de cet exercice'
  })

  // on boucle sur les params
  for (const [name, param] of Object.entries(parameters)) {
    if (!['number', 'integer', 'string', 'boolean'].includes(param.type)) {
      throw Error(`Type de paramètre ${param.type} incorrect pour le paramètre ${name})`)
    }
    // title déjà édité ci-dessus
    if (name === 'title') continue

    const label = getText(name)
    const labelElt = addElement(fieldSet, 'label', { className: 'param' })
    addElement(labelElt, 'span', { content: label, className: 'label' })
    if (param.help) {
      addTooltip({ tooltip: param.help, parent: form, target: labelElt })
    }
    let actualValue: ParamValue<typeof param.multiple> = nodeParams[name] ?? param.defaultValue

    if (param.multiple === true) {
      if (!Array.isArray(actualValue)) {
        if (!Array.isArray(param.defaultValue)) {
          throw Error(`Valeur du paramètre multiple ${name} incorrecte (pas un array), idem pour sa valeur par défaut, impossible de continuer`)
        }
        console.error(Error(`valeur du paramètre ${name} incorrecte (pas un array)`), actualValue)
        actualValue = param.defaultValue
      }
      addMultipleField(name, labelElt, param, actualValue)
      continue
    } // fin cas multiple

    // cas non multiple, il n’y a qu’une valeur à saisir
    if (param.editor != null) {
      addEditedField(name, labelElt, param, actualValue, form, parameters)
      continue
    }

    // reste le cas unique sans editor
    if (param.controlledValues != null) {
      // On fournit une liste des valeurs possibles pour n’en choisir qu’une
      // (et on teste même pas boolean car ce serait complètement débile)
      const selectElt = addElement(labelElt, 'select', {
        attrs: { name },
        value: String(actualValue)
      })
      for (const controlledValue of param.controlledValues) {
        addElement(selectElt, 'option', { value: String(controlledValue), content: String(controlledValue) })
      }
      // lui doit être affecté après ajout des options
      selectElt.selectedIndex = param.controlledValues.findIndex((el: unknown) => el === actualValue) ?? -1
      if (param.validate) {
        selectElt.addEventListener('input', async () => {
          try {
            resetAvert(form)
            const value = getInputValue(param.type, selectElt)
            const results = await param.validate(value, getValues(form, parameters))
            processFieldMessage(labelElt, results)
          } catch (error) {
            console.error(error)
          }
        })
      }
      continue
    }

    // Une seule valeur est attendue et seul le type demandé sera vérifié au submit
    if (param.type === 'boolean') {
      const checkBoxElt = addElement(labelElt, 'input', {
        attrs: { name },
        type: 'checkbox',
        checked: actualValue as boolean
      })
      let currentValue = checkBoxElt.checked
      if (param.validate) {
        // ici faut pas l'event input car il n'est pas appelé sur un checkbox
        // https://developer.mozilla.org/fr/docs/Web/API/Element/input_event
        checkBoxElt.addEventListener('change', async () => {
          try {
            const newValue = checkBoxElt.checked
            if (newValue === currentValue) return
            currentValue = newValue
            const results = await param.validate(currentValue, getValues(form, parameters))
            processFieldMessage(labelElt, results)
          } catch (error) {
            console.error(error)
          }
        })
      }
      continue
    }

    // string ou number
    const inputElt = addElement(labelElt, 'input', {
      attrs: { name },
      type: param.type === 'integer' ? 'number' : 'text',
      value: String(actualValue)
    })
    if (param.validate) {
      // ici faut pas l'event input car il n'est pas appelé sur un checkbox
      // https://developer.mozilla.org/fr/docs/Web/API/Element/input_event
      inputElt.addEventListener('input', async () => {
        try {
          const results = await param.validate(getInputValue(param.type, inputElt), getValues(form, parameters))
          processFieldMessage(labelElt, results)
        } catch (error) {
          console.error(error)
        }
      })
    }
  } // boucle sur les params

  addElement(fieldSet, 'button', {
    type: 'submit',
    content: 'Valider'
  })
  addElement(fieldSet, 'button', {
    type: 'reset',
    content: 'Annuler'
  })

  // gestionnaire de validation
  form.addEventListener('submit', (event: SubmitEvent): void => {
    event.preventDefault() // on veut pas poster les valeurs
    validateGlobal(node, form, parameters, validateParameters)
      .then(result => {
        if (result) onClose()
      })
      .catch(showError)
  }) // gestionnaire de validation
  // gestionnaire d’annulation
  form.addEventListener('reset', onClose)
} // editNode

/**
 * Ajoute un input et son label
 * @param container
 * @param options
 * @param [options.label]
 * @param [options.name]
 * @param [options.inputAttrs]
 * @param [options.inputOpts]
 * @param [options.tooltip]
 * @param [options.tooltipPlacement]
 * @param [options.tooltipParent]
 */
export function addInput (container: HTMLElement, {
  label = '',
  name,
  inputAttrs = {},
  inputOpts = {},
  tooltip = '',
  tooltipPlacement = 'top',
  tooltipParent
}: AddInputOptions): HTMLInputElement {
  const labelElt = addElement(container, 'label', { className: 'param' })
  addElement(labelElt, 'span', { content: `${label} : ` })
  const input = addElement(labelElt, 'input', { attrs: { name, ...inputAttrs }, ...inputOpts })
  if (tooltip) {
    const tooltipOpts: AddTooltipOptions = { tooltip, target: labelElt, placement: tooltipPlacement }
    if (tooltipParent) tooltipOpts.parent = tooltipParent
    addTooltip(tooltipOpts)
  }
  return input
}

/**
 * pour insérer un nouveau champ de saisie avec les boutons nécessaires pour supprimer et ajouter si besoin
 * @private
 * @param parentNode le conteneur
 * @param element le bouton ajouter sur lequel on vient de cliquer qui va servir de référence pour insérer et déplacer les éléments
 * @param name le nom du paramètre qui permet d’associer tous les champs dans un array
 * @param type le type du champ que l’on clone
 * @param nbBooleans pour indexer les cases à cocher des booléens et savoir s’il ne reste plus
 */
function addBloc (parentNode: HTMLElement, element: HTMLElement, name: string, type: string, nbBooleans: number): number {
  // On clone le bouton d’ajout
  const supBeforeAdd = element.previousSibling as HTMLInputElement
  if (supBeforeAdd.value === 'Retirer cet élément') {
    //  modale.removeChild(inputBeforeAjout)
    const brBeforeSup = supBeforeAdd.previousSibling as HTMLElement
    if (brBeforeSup != null && brBeforeSup.tagName !== 'BR') {
      parentNode.insertBefore(document.createElement('br'), element)
    }
  }
  const champ = document.createElement('input')
  champ.type = type === 'boolean' ? 'checkbox' : 'text'
  champ.setAttribute('name', name)
  parentNode.insertBefore(supBeforeAdd, element)
  if (type === 'boolean') {
    const label = document.createElement('label')
    label.textContent = String(nbBooleans++)
    label.appendChild(champ)
    parentNode.insertBefore(label, supBeforeAdd)
  } else {
    parentNode.insertBefore(champ, supBeforeAdd)
    nbBooleans++
  }
  if (nbBooleans != null && nbBooleans > 1) {
    supBeforeAdd.style.display = 'inline'
  }
  return nbBooleans
}

/**
 * Ajoute les inputs pour l'édition de ce champ multiple
 * @private
 * @param labelElt
 * @param param
 */
export function addEditedField (name: string, labelElt: HTMLElement, param: SectionParameter<SectionParameterType, boolean>, actualValue: ParamValue<true>, form: HTMLFormElement, parameters: SectionParameters): void {
  // Le paramètre a un éditeur qui lui est propre
  const editor = param.editor
  if (!editor) throw Error('Erreur interne')
  // on traite ici le cas des paramètres ayant un éditeur
  const inputElt = addElement(labelElt, 'input', {
    attrs: { name },
    type: 'text',
    value: String(actualValue),
    style: { display: 'none', width: '100%' }
  })
  const buttonsCt = addElement(labelElt, 'div', {
    className: 'value',
    style: { display: 'flex', justifyContent: 'space-evenly' }
  })
  addElement(buttonsCt, 'button', {
    name: 'Editer',
    textContent: 'Éditer',
    type: 'button'
  })
    .addEventListener('click', async () => {
      // au cas où on aurait cliqué sur la saisie brute auparavant
      inputElt.style.display = 'none'
      const modaleEditor = new Modal({
        inset: '7%',
        fullScreen: true,
        zIndex: 160 // faut passer au dessus de la modale d'édition du nœud
      })
      const divModale = modaleEditor.addElement('div')
      modaleEditor.show()
      const value = await editor(divModale, inputElt.value, getValues(form, parameters))
      inputElt.value = String(value)

      // on ferme la modale de l'éditeur
      modaleEditor.empty()
      modaleEditor.hide()
      modaleEditor.destroy()
    }) // listener

  addElement(buttonsCt, 'button', { textContent: 'Saisie brute', type: 'button' })
    .addEventListener('click', () => {
      inputElt.style.display = 'block'
    })

  const { validate } = param
  if (validate) {
    inputElt.addEventListener('input', async () => {
      try {
        validateParams(form, parameters)
        const results = await validate(inputElt.value, getValues(form, parameters))
        processFieldMessage(labelElt, results)
      } catch (error) {
        console.error(error)
      }
    })
  }
} // addEditedField
/**
 * Ajoute un message d'erreur|warning sur un champ
 * @private
 * @param labelElt
 * @param message
 * @param isWarning
 */
function addFieldMessage (labelElt: HTMLElement, message: string, isWarning = false): void {
  const className = isWarning ? 'warning' : 'error'
  labelElt.classList.add(className)
  addElement(labelElt, 'p', { className, content: message })
}

/**
 * Ajoute les inputs pour l'édition de ce champ multiple
 * @private
 * @param labelElt
 * @param param
 */
export function addMultipleField (name: string, labelElt: HTMLElement, param: SectionParameter<SectionParameterType, true>, actualValue: ParamValue<true>): void {
  // @todo implémenter validate dans ce cas

  // c'est forcément un array de params mais faut aider TS
  const values = actualValue as (string | number | boolean)[]

  if (param.controlledValues == null) {
    // Une liste de valeurs est attendue, on fournit autant de zones de saisie que voulu et on vérifie le type de chaque valeur saisie
    // on doit aussi initialiser avec les valeurs déjà contenues dans params et fournir les boutons pour supprimer les valeurs.
    let nbBooleans = 1 // on initialise le numéro de checkbox (il faut bien mettre quelque chose devant) et j’utilise un array pour pouvoir modifier sa valeur dans addBloc()

    // le span pour toutes nos saisies dans la colonne de droite
    const span = addElement(labelElt, 'span', { className: 'value' })

    // le bouton supprimer
    const btnSuppr = addElement(span, 'input', {
      type: 'button',
      value: 'Retirer cet élément'
    })
    const btnAjout = addElement(span, 'input', {
      type: 'button',
      value: 'Ajouter un élément'
    })
    const refreshButtons = () => {
      // faut remettre les boutons à la fin du span, ajout en dernier
      span.appendChild(btnSuppr)
      span.appendChild(btnAjout)
      btnSuppr.style.display = nbBooleans > 1 ? 'inline' : 'none'
    }
    btnSuppr.addEventListener('click', (event) => {
      event.preventDefault()
      nbBooleans = removeLastField(span, btnSuppr, nbBooleans)
      refreshButtons()
    })
    btnAjout.addEventListener('click', (event) => {
      event.preventDefault()
      nbBooleans = addBloc(span, btnAjout, name, param.type, nbBooleans)
      refreshButtons()
    })

    if (values.length === 0) {
      // faut quand même mettre un input
      if (param.type === 'boolean') {
        const labelBool = addElement(span, 'label', { content: String(nbBooleans++) })
        addElement(labelBool, 'input', {
          type: param.type === 'boolean' ? 'checkbox' : 'text',
          attrs: { name }
        })
      } else {
        nbBooleans++
        addElement(span, 'input', {
          type: 'text',
          attrs: { name }
        })
      }
      addElement(span, 'br')
    } else {
      // y'a des valeurs
      for (const value of values) {
        if (param.type === 'boolean') {
          const subLabelElt = addElement(span, 'label', { content: String(nbBooleans++) })
          addElement(subLabelElt, 'input', {
            attrs: { name },
            type: 'checkbox',
            checked: value as boolean
          })
        } else {
          nbBooleans++ // on s’en sert aussi pour savoir si c’est le dernier champ et ajouter le bouton 'Retirer cet élément'
          addElement(span, 'input', {
            attrs: { name },
            type: 'text',
            value: String(value)
          })
        }
        if (nbBooleans !== values.length + 1) {
          addElement(span, 'br')
        }
      }
    }
    refreshButtons()
    return
  } // fin liste non contrôlée

  // reste le cas multiple contrôlé
  if (param.type === 'boolean') throw Error('On ne devrait pas avoir de liste de valeurs booléennes pour un paramètre multiple, chaque booléen devrait être associé à un paramètre clairement défini')
  // liste des valeurs possibles fournie
  const div = addElement(labelElt, 'div')
  for (const value of param.controlledValues.map(String)) {
    const label = addElement(div, 'label', { content: value })
    addElement(label, 'input', {
      type: 'checkbox',
      checked: values.includes(value),
      value: String(value),
      style: {
        marginRight: '12px',
        marginLeft: '2px'
      },
      attrs: { name }
    })
  }
} // addMultipleField

/**
 * Supprimer le dernier champ et déplacer/supprimer les boutons associés
 * @param parentNode le conteneur
 * @param element le bouton supprimer
 * @param nbBooleans la valeur du compteur de champ passée dans un array afin d'être modifiée
 */
function removeLastField (parentNode: HTMLElement, element: HTMLElement, nbBooleans: number): number {
  if (nbBooleans === 1) return 1
  if (element.previousSibling != null) {
    parentNode.removeChild(element.previousSibling)
  }
  // supprime le retour à la ligne si présent faisant ainsi remonter le bouton 'Retirer cet élément' et le bouton 'Ajouter un élément' d’un étage.
  const previousElement = element.previousSibling as HTMLElement
  if (previousElement != null && previousElement.tagName === 'BR') {
    parentNode.removeChild(previousElement)
  }
  nbBooleans--
  return nbBooleans
}

export function resetAvert (form: HTMLFormElement): void {
  // on commence par vider un éventuel message d’erreur précédent
  for (const pErr of form.querySelectorAll('p.error')) {
    destroy(pErr)
  }
  // et virer la classe error sur les labels params
  for (const errElt of form.querySelectorAll('.error')) {
    errElt.classList.remove('error')
  }
}

/**
 * Retourne les valeurs (typées) du form
 * @param form
 * @param parameters
 */
export function getValues (form: HTMLFormElement, parameters: SectionParameters): ParamsValues {
  const values: ParamsValues = {}

  for (const [param, { multiple, type }] of Object.entries(parameters)) {
    // le ou les inputs de param
    const inputs = form.elements.namedItem(param)
    const getValue = getInputValue.bind(null, type)

    if (inputs == null) {
      if (multiple !== true) throw Error(`On ne retrouve pas les élément du paramètre ${param}`)
      // si il n’y a pas de valeurs, il n’y a pas besoin de les valider
      values[param] = []
    } else {
      // inputs est RadioNodeList | Element
      try {
        // et on traite chaque champ
        if (multiple === true) {
          // inputs devrait être un RadioNodeList, sauf si y’a qu’une seule case de cochée (c’est un Element)
          if ('length' in inputs && inputs.length > 0) {
            // RadioNodeList
            values[param] = Array.from(inputs)
              .map(inputElt => getValue(inputElt as HTMLInputElement))
              // on vire les null, donnera un tableau vide si rien n’a été défini
              .filter(el => el != null) as ParamValue<true>
          } else {
            // inputs est un Element (la seule checkbox cochée)
            const value = getValue(inputs as HTMLInputElement)
            values[param] = value == null ? [] : [value]
          }
        } else {
          // single, inputs est un Element
          values[param] = getValue(inputs as HTMLInputElement) as string
        }
      } catch (error) {
        console.error(error)
        delete values[param]
      }
    }
  } // parameters loop

  return values
}

/**
 * Affiche les warning|error si y'en a
 * @param labelElt
 * @param results
 */
export function processFieldMessage (labelElt: HTMLElement, results: Partial<CheckResults>) {
  const { ok, errors, warnings } = formatCheckResult(results)
  if (!ok) {
    const isWarning = !errors.length
    const msg = isWarning ? warnings.join('\n') : errors.join('\n')
    addFieldMessage(labelElt, msg, isWarning)
  }
}

/**
 * Retourne la valeur typée d’un input (null si rien n’a été saisi)
 * @private
 */
export function getInputValue (type: SectionParameterType, inputElt: HTMLInputElement | HTMLSelectElement): SectionParameterTypes[typeof type] | null {
  if (inputElt == null) throw Error('Aucun input, impossible de récupérer une valeur')
  if (type === 'boolean') {
    if (inputElt.type !== 'checkbox') throw Error('Paramètre booléen non représenté par une case à cocher')
    return inputElt.checked
  }
  // on cast et on vérifie en même temps
  if (['number', 'integer'].includes(type)) {
    if (inputElt.value === '') return null
    const value = Number(inputElt.value.replace(',', '.'))
    if (type === 'integer' && !Number.isInteger(value)) throw Error(`Saisie invalide, ${inputElt.value} n’est pas un entier`)
    else if (!Number.isFinite(value)) throw Error(`Saisie invalide, ${inputElt.value} n’est pas un nombre`)
    return value
  }
  // on laisse string
  return inputElt.value === '' ? null : inputElt.value
}

/**
 * Retourne l'élément label du param dans le form
 * @param param
 * @param form
 */
function getLabelElt (param: string, form: HTMLFormElement): HTMLElement | null {
  const inputElement = form.elements.namedItem(param)
  if (inputElement == null) {
    console.error(Error(`Aucun input ${param} dans`), form)
    return null
  }
  if ('length' in inputElement) {
    // RadioNodeList
    const firstElt = inputElement[0]
    if (firstElt) return getFirstParentByTag(firstElt as HTMLElement, 'LABEL')
  }
  // input ou select
  return getFirstParentByTag(inputElement as HTMLElement, 'LABEL')
}

/**
 * Relance la validation individuelle de tous les params
 * (on doit commencer par effacer les avertissements déjà affichés
 * donc faut tous les relancer, même si un seul a changé)
 * @param form
 * @param parameters
 */
async function validateParams (form: HTMLFormElement, parameters: SectionParameters): Promise<boolean> {
  resetAvert(form)
  const values = getValues(form, parameters)
  let hasError = false
  for (const [param, { validate }] of Object.entries(parameters)) {
    if (validate) {
      const rawResults = await validate(values[param], values)
      const results = formatCheckResult(rawResults)
      if (!results.ok) {
        // faut retrouver l'élément label
        const labelElt = getLabelElt(param, form)
        if (labelElt == null) throw Error(`Pas trouvé de saisie pour le paramètre ${param}`)
        processFieldMessage(labelElt, results)
      }
      if (results.errors.length > 0) hasError = true
    }
  }
  return !hasError
}

/**
 * Valide les paramètres du node, en cas de pb les affiche dans le form (et si ok retourne true et dispatch les modifs
 * @param node
 * @param form
 * @param parameters
 * @param [validateParameters]
 * @returns true si params ok (et dispatch lancé)
 */
export async function validateGlobal (node: GraphNodeActiveSerialized, form: HTMLFormElement, parameters: SectionParameters, validateParameters?: ValidateParametersFn): Promise<boolean> {
  // valeurs courantes, par param
  const values = getValues(form, parameters)

  // la validation individuelle des params
  let isOk = await validateParams(form, parameters)

  // maxRuns est une propriété du node, pas géré par validateParams
  const maxRunInput = form.elements.namedItem('maxRuns')
  // si on a pas trouvé l’input ça plantera ligne suivante avec le 0 mis dans ce cas
  const saisieMaxRuns = (maxRunInput && 'value' in maxRunInput) ? Number(maxRunInput.value) : 0
  if (!Number.isInteger(saisieMaxRuns) || saisieMaxRuns < 1 || saisieMaxRuns > maxRunCeil) {
    isOk = false
    const labelMaxRunsElt = getLabelElt('maxRuns', form)
    if (labelMaxRunsElt == null) throw Error('Aucune saisie pour maxRuns')
    processFieldMessage(labelMaxRunsElt, { errors: [`Saisie invalide, maxRuns doit être un entier compris entre 1 et ${maxRunCeil}`] })
  }
  // en cas de pb, c'est déjà affiché, on arrête là
  if (!isOk) return false

  // la validation générale (ignorée si y'a déjà des pbs à la validation individuelle)
  if (validateParameters) {
    const sectionPbs = await validateParameters(values)
    for (const [param, pb] of Object.entries(sectionPbs)) {
      isOk = false
      const labelElt = getLabelElt(param, form)
      if (labelElt != null) processFieldMessage(labelElt, { errors: [pb] })
    }
  }
  if (!isOk) return false

  // saisie valide, on peut envoyer
  const newNode: GraphNodeSerialized = { ...node }

  const eltTitle = form.elements.namedItem('title')
  const inputTitle = (eltTitle && 'value' in eltTitle) ? eltTitle.value : ''
  if (inputTitle != null && inputTitle.length > 0) newNode.title = inputTitle
  const eltLabel = form.elements.namedItem('label')
  const inputLabel = (eltLabel && 'value' in eltLabel) ? eltLabel.value : ''
  if (inputLabel != null && inputLabel.length > 0) newNode.label = inputLabel

  newNode.maxRuns = saisieMaxRuns
  newNode.params = values
  if (newNode.id) dispatch(changeNode(newNode))
  // on a pas accès ici à SceneUI, mais c'est pas grave car l'action ajouter a déjà
  // appelé checkNodePosition dès qu'on connaissait son label (dans SceneContextMenu)
  else dispatch(addNode(newNode))

  return true
} // validateParams

export default editNode
