import { isLegacyConnector } from 'src/lib/core/checks'
import { isStringNotEmpty } from 'src/lib/utils/string'
import { isEmptyArray } from 'src/lib/utils/array'
import { stringify } from 'src/lib/utils/object'
import { areSameObject } from 'src/lib/utils/comparators'
import { fetchEvaluations, sectionExists } from 'src/lib/core/loadSection'

import type { LegacyConnector, CheckResults, LegacyGraph, LegacyNode, LegacyNodeElements, LegacyNodeParams, LegacyPositionNode, TitreNodesV1 } from 'src/lib/types'

export interface GraphV1Values {
  graphe: LegacyGraph
  positionNodes?: LegacyPositionNode[]
  titreNodes?: string[]
}
/**
 * Objet représentant le graphe dans sa première version (tableau de tableaux), avec ses propriétés positionNodes et titreNodes mises par editgraphes (v1)
 */
class GraphV1 {
  // en ts il vaut mieux avoir des objets dont les propriétés ne changent pas, si y’a besoin de propriété dynamique pour une collection, Map est plus indiqué
  /** Les nœuds du graphe, indexés par leur id */
  graphe: LegacyGraph
  positionNodes: LegacyPositionNode[]
  titreNodes: TitreNodesV1
  warnings: string[]

  constructor ({ graphe, positionNodes, titreNodes }: GraphV1Values) {
    if (!Array.isArray(graphe)) throw Error('graphe invalide')
    positionNodes = (Array.isArray(positionNodes)) ? [...positionNodes] : []
    titreNodes = (Array.isArray(titreNodes)) ? [...titreNodes] : []
    this.graphe = []
    this.warnings = []
    let firstEndNode: LegacyNode | null = null
    for (const [indexNode, node] of graphe.entries()) {
      if (indexNode === 0 && (node == null || isEmptyArray(node))) {
        // un tableau vide en premier, on le zappe, mais faut regarder aussi les positions et titres pour virer le premier
        if (graphe.length === positionNodes.length) positionNodes.shift()
        if (graphe.length === titreNodes.length) titreNodes.shift()
        continue
      }
      let [id, section] = node
      // id en string
      if (typeof id === 'number') id = String(id)
      if (typeof id !== 'string' || id.length === 0) throw Error(`identifiant invalide pour le nœud d’index ${indexNode} (${stringify(node)})`)
      // section en string
      if (typeof section !== 'string') throw Error(`section invalide pour le nœud d’id ${id} (${stringify(node)})`)
      if (section.length === 0) throw Error(`section vide pour le nœud ${id} (${stringify(node)})`)

      // cas du nœud fin
      if (section.toLowerCase() === 'fin') {
        if (section !== 'fin') section = 'fin' // on règle la casse (bizarrement TS indique string pour la var dans la condition et any celle qui se fait affecter)
        // on le note comme premier nœud fin trouvé et on arrête là
        const endNode: LegacyNode = [id, section]
        if (firstEndNode == null) firstEndNode = endNode
        this.graphe.push(endNode)
        continue
      }
      // pas un nœud fin

      // la section doit exister
      if (!sectionExists(section)) throw Error(`La section ${section} n’existe pas`)

      // id/section ok, on passe aux branchements
      const { branchements, params } = GraphV1.splitNode(node)
      if (branchements.length < 1) throw Error(`Le nœud d’index ${indexNode} n’a pas de branchement`)
      // on construit nos branchements nettoyés
      const connectors: LegacyConnector[] = []
      for (const [indexBranchement, br] of branchements.entries()) {
        // on passe en revue les propriétés d’un branchement

        // nn
        let { nn } = br
        if (typeof nn === 'number') nn = String(nn)
        if (typeof nn !== 'string') throw Error(`nn invalide dans le branchement d’index ${indexBranchement} du nœud d’index ${indexNode} (${stringify(br)})`)
        if (nn === '') throw Error(`Le branchement d’index ${indexBranchement} du nœud d’index ${indexNode} n’a pas de destination (${stringify(br)}`)
        const connector: LegacyConnector = {
          nn,
          conclusion: typeof br.conclusion === 'string' ? br.conclusion : ''
        }
        // rectif si nn = fin
        if (connector.nn.toLowerCase() === 'fin') {
          connector.nn = (firstEndNode == null) ? 'fin' : firstEndNode[0]
        }

        // snn
        if (Number.isInteger(br.snn)) connector.snn = String(br.snn)
        else if (typeof br.snn === 'string' && br.snn) connector.snn = br.snn

        // max
        if (typeof br.max === 'string') br.max = Number(br.max) // on fait le cast aussi dans l’objet d’origine
        // le test != null est stupide mais ts l’exige ici
        if (br.max != null && Number.isInteger(br.max) && br.max > 0) connector.max = br.max

        // maxParcours
        if (typeof br.maxParcours === 'string') br.maxParcours = Number(br.maxParcours)
        if (typeof br.maxParcours === 'number' && Number.isInteger(br.maxParcours) && br.maxParcours > 0) connector.maxParcours = br.maxParcours

        // sconclusion
        if (br.sconclusion) connector.sconclusion = br.sconclusion

        // pe
        if (br.pe) connector.pe = br.pe

        // on passe à la validation des branchements
        if (connector.pe == null) {
          // si y’a pas de pe faut un score
          if (typeof br.score === 'string' && br.score.length > 0) {
            if (/^sans.condition$/.test(br.score)) {
              connector.score = 'sans condition'
            } else if (/^[<>]=[0-9.]+$/.test(br.score)) { // en v1 y’a pas de comparateur < ou >, seulement <= ou >=
              connector.score = br.score
            } else {
              throw Error(`Le branchement d’index ${indexBranchement} du nœud d’index ${indexNode} a une condition de score invalide ${br.score}`)
            }
          }
          if (connector.score == null) {
            throw Error(`Le branchement d’index ${indexBranchement} du nœud d’index ${indexNode} n’a pas de condition (ni pe ni score) ${stringify(br)}`)
          }
          // reste à normaliser les conditions sur le score
          if (connector?.score?.length != null && connector.score.length > 0) {
            if (connector.score === '>=0' || connector.score === '<=1' || /sans[ +]condition/i.test(connector?.score ?? '')) {
              connector.score = 'sans condition'
            }
          }
        }

        connectors.push(connector)
        // Il restera à vérifier que les pe existent dans la section, c’est validate qui fait ça
      } // boucle connectors

      // et on ajoute le nœud
      this.graphe.push([id, section, [...connectors, params]])
    } // boucle nœuds

    if (this.graphe.length === 0) throw Error('graphe vide')

    // si y’avait aucun nœud fin on l’ajoute (on a mis des nn|snn à fin, c’est leur destination)
    if (firstEndNode == null) {
      // en v1 un nœud fin se reconnait à sa section nommée 'fin', en v2 c’est une chaîne vide
      this.graphe.push(['fin', 'fin']) // tant pis si un idiot a déjà donné l’id 'fin' à un nœud non fin
    }

    // on passe en revue tous les branchements pour vérifier que la destination existe
    // une première passe pour les récupérer tous
    const destinations = new Set()
    for (const node of this.graphe) {
      if (node[2] == null) continue
      const connectors = [...node[2]]
      connectors.pop()
      for (const connector of connectors) {
        if (connector.nn != null) destinations.add(connector.nn) // pas de nn sur le dernier (params)
        if (connector.snn != null) destinations.add(connector.snn)
      }
    }
    // et une 2e pour vérifier
    for (const node of this.graphe) {
      const opts = node[2]
      if (opts == null) continue
      const connectors = [...opts]
      connectors.pop()
      for (const connector of connectors) {
        if (!destinations.has(connector.nn)) throw Error(`Aucun nœud ${connector.nn as string}`)
        if (connector.snn != null && !destinations.has(connector.snn)) throw Error(`Aucun nœud ${connector.snn as string}`)
      }
    }

    // ok pour graphe, on passe à positionNodes
    this.positionNodes = []
    let lastX = 0
    let lastY = 0
    for (const position of positionNodes) {
      if (this.positionNodes.length >= this.graphe.length) {
        this.warnings.push(`${String(positionNodes.length)} positions alors qu’on a ${this.graphe.length} nœuds !`)
        break
      }
      if (Array.isArray(position) && position.length === 2) {
        const [x, y] = position
        if (typeof x === 'number' && typeof y === 'number') {
          this.positionNodes.push([x, y])
          lastX = x
          lastY = y
          continue
        }
      }
      lastX += 50
      lastY += 50
      this.positionNodes.push([lastX, lastY])
    }

    // pour titreNodes on est plus laxiste
    this.titreNodes = titreNodes
  }

  getNode (nodeId: string): LegacyNode {
    const node = this.graphe.find(([id]) => nodeId === id)
    if (node == null) throw Error(`Le node ${nodeId} n’existe pas dans ce graphe : ${stringify(this.graphe)}`)
    return node
  }

  /**
   * Retourne le tableau de nodes normalisés (tous avec des propriétés id, section, branchements, params)
   */
  getNormalizedNodes (): LegacyNodeElements[] {
    return this.graphe.map(GraphV1.splitNode)
  }

  /**
   * Compare le graphe courant à un autre
   */
  isSame (graph2: GraphV1, { ignoreLastNone = false } = {}): CheckResults {
    const { ok, warnings, errors } = isSameNodesV1(this.graphe, graph2.graphe, { ignoreLastNone })
    const result = isSameEditGraphes(this, graph2) // à priori, cette fonction ne retournera que des warnings
    if (result.ok) {
      warnings.push(...result.warnings)
    } else {
      errors.push(...result.errors)
    }
    return { ok: ok && result.ok, warnings, errors }
  }

  async validate (): Promise<CheckResults> {
    const _validateNodeConnectors = async (node: LegacyNode): Promise<void> => {
      const [id, section, opts = []] = node
      const evaluations = await fetchEvaluations(section)
      const existingPes = Object.keys(evaluations)
      const connectors = [...opts]
      connectors.pop()
      for (const connector of connectors) {
        if (isStringNotEmpty(connector.pe)) {
          for (const pe of connector.pe.split(',')) {
            if (!existingPes.includes(pe)) errors.push(`Le node ${id} a un branchement avec la pe ${String(pe)} mais elle n’existe pas dans la section ${section}`)
          }
        }
        if (connector.nn == null) throw Error(`branchement invalide ${stringify(connector)}`)
        targets.add(String(connector.nn))
        if (connector.snn != null) targets.add(String(connector.snn))
      }
      validatedNodes.add(id)
    } // _validateNodeConnectors

    // pour mémoriser les nœuds traités
    const validatedNodes: Set<string> = new Set()
    // et ceux qui sont des cibles
    const targets: Set<string> = new Set()

    // ce qu’on va retourner
    const errors: string[] = []
    const warnings: string[] = []
    try {
      // on vérifie qu’on a au moins un nœud de départ
      const firstNode = this.graphe.find(node => node.length === 3 && node[1] !== 'fin')
      if (firstNode == null) {
        errors.push('Aucun nœud de départ')
      } else {
        targets.add(firstNode[0])
        await _validateNodeConnectors(firstNode)
      }
      while (targets.size > validatedNodes.size) {
        for (const target of targets) {
          if (validatedNodes.has(target)) continue
          const node = this.getNode(target)
          if (node[1] === 'fin') {
            validatedNodes.add(target)
            continue
          }
          await _validateNodeConnectors(node)
        }
      }
    } catch (error) {
      // errors.push(`${error.message}\n${error.stack}`)
      errors.push(String(error))
    }
    return { ok: errors.length === 0, errors, warnings }
  }

  /**
   * Retourne les éléments du node (toujours fournis, mais éventuellement vides)
   * @param node
   */
  static splitNode (node: LegacyNode): LegacyNodeElements {
    const [id, section, opts] = [...node]
    const branchements: LegacyConnector[] = []
    const params: LegacyNodeParams = {}
    if (Array.isArray(opts)) {
      for (const [index, opt] of opts.entries()) {
        if (isLegacyConnector(opt)) {
          branchements.push(opt)
        } else {
          if (index !== opts.length - 1) {
            throw Error(`branchement invalide à l’index ${index}`)
          }
          Object.assign(params, opt)
        }
      }
    }
    return { id, section, branchements, params }
  }
}

/**
 * Compare deux tableaux de noeuds V1
 */
function isSameNodesV1 (nodes1: LegacyGraph, nodes2: LegacyGraph, { ignoreLastNone = false } = {}): CheckResults {
  if (nodes1.length !== nodes2.length) {
    return { ok: false, errors: ['Les listes de noeuds n’ont pas le même nombre de noeuds'], warnings: [] }
  }
  // faut boucler sur les nodes
  const errors: string[] = []
  const warnings: string[] = []
  for (const node1 of nodes1) {
    const [id1, section1, options1] = node1
    const node2 = nodes2.find(node => node[0] === id1)
    if (node2 == null) {
      errors.push(`Aucun nœud d’id ${node1[0]} dans la deuxième liste`)
      continue
    }
    // id ok
    const [, section2, options2] = node2
    // on vérifie la section
    if (section1 !== section2) {
      errors.push(`les nœuds d’id ${id1} n’ont pas la même section ${section1} ≠ ${section2}`)
      // pas la peine de continuer ça doit déconner pas mal ensuite
      continue
    }
    if (section1 === 'fin') continue
    if (options1 == null) {
      errors.push(`Le nœud d’id ${id1} n’a pas de branchement (1er graphe)`)
      continue
    }
    if (options2 == null) {
      errors.push(`Le nœud d’id ${id1} n’a pas de branchement (2e graphe)`)
      continue
    }
    // on enlève les params des options pour récupérer les branchements
    const brs1 = options1.filter(isLegacyConnector) // ça clone pour que le pop plus loin ne modifie pas options1 (donc node1)
    const brs2 = options2.filter(isLegacyConnector)
    const params1 = options1.find(opt => !isLegacyConnector(opt)) ?? {}
    const params2 = options2.find(opt => !isLegacyConnector(opt)) ?? {}
    const nbErrors = errors.length
    let pbNb = false
    if (brs1.length !== brs2.length) {
      if (ignoreLastNone) {
        // on peut avoir un connecteur sans condition en plus d’un coté
        // (ajouté automatiquement par la conversion v1=>v2)
        if (brs1.length === brs2.length + 1 && brs1?.at(-1)?.score === 'sans condition') {
          // dernier br1 sans condition qu’on vire
          brs1.pop()
        } else if (brs1.length + 1 === brs2.length && brs2?.at(-1)?.score === 'sans condition') {
          // dernier br2 sans condition qu’on vire
          brs2.pop()
        } else {
          pbNb = true
        }
      } else {
        pbNb = true
      }
      if (pbNb) {
        errors.push(`Pas le même nombre de branchements pour le nœud d’id ${id1} : ${brs1.length} ≠ ${brs2.length}`)
      }
    }
    if (!pbNb) {
      // on a le même nb, on peut boucler sur les branchements 1
      for (const [i, br1] of brs1.entries()) {
        const br2: LegacyConnector = brs2[i] ?? {} as LegacyConnector
        // on vérifie qu’on récupère bien toutes nos propriétés, mais on peut pas utiliser areSameXxx
        // car on peut avoir des modif max <=> maxParcours (bugs de l’éditeur v1, normalement c’est max quand ça boucle sur le même nœud et maxParcours quand ça boucle vers un nœud précédent du graphe)
        for (const [k, v1] of Object.entries(br1)) {
          const v2 = br2[k as keyof LegacyConnector]
          if (v2 !== v1) {
            if ((k === 'maxParcours' && br2.max === v1) || (k === 'max' && br2.maxParcours === v1)) {
              warnings.push(`le branchement d’index ${String(i)} du nœud d’id ${id1} a une propriété max d’un coté et maxParcours de l’autre (qui valent ${v1}), c’est quasi similaire.`)
            } else {
              errors.push(`le branchement d’index ${String(i)} du nœud d’id ${id1} est différent pour sa propriété ${k} : ${String(v1)} ≠ ${String(v2)}`)
            }
          }
        }
      }
      // on boucle sur les branchements 2 pour vérifier qu’ils n’ont pas de propriétés en plus
      for (const [i, br2] of brs2.entries()) {
        const br1 = brs1[i] ?? {} as LegacyConnector
        for (const [k, v2] of Object.entries(br2)) {
          if (!(k in br1)) {
            if ((k === 'max' && br1.maxParcours === v2) || (k === 'maxParcours' && br1.max === v2)) continue
            errors.push(`le branchement d’index ${String(i)} du nœud d’id ${id1} a une propriété ${k} : ${String(v2)} qui n’existe pas dans un des graphes`)
          }
        }
      }
    }
    if (errors.length > nbErrors) {
      errors.push(`les nœuds d’id ${id1} n’ont pas les mêmes branchements :\n${JSON.stringify(brs1)}\net\n${JSON.stringify(brs2)}`)
    }
    if (!areSameObject(params1, params2)) {
      errors.push(`les nœuds d’id ${id1} n’ont pas le même paramétrage :\n${JSON.stringify(params1)}\net\n${JSON.stringify(params2)}`)
    }
  }
  return { ok: errors.length < 1, warnings, errors }
}

/**
 * Compare deux objets editgraphes, l’un issu des ressources, l’autre issu de la double conversion
 * @param {GraphV1} graphe1
 * @param {GraphV1} graphe2
 * @return {{ok: true, warnings: string[], errors: string[]}}
 */
function isSameEditGraphes (graphe1: GraphV1, graphe2: GraphV1): CheckResults {
  const warnings: string[] = []
  // On compare les listes de positions
  if (graphe1.positionNodes.length === graphe2.positionNodes.length) {
    const samePos = graphe1.positionNodes.every(([x, y], i) => {
      const [x2, y2] = graphe2.positionNodes[i] ?? []
      return x === x2 && y === y2
    })
    if (!samePos) {
      warnings.push(`Les listes de positions ${JSON.stringify(graphe1.positionNodes)} et ${JSON.stringify(graphe2.positionNodes)} n’ont pas les mêmes éléments`)
    }
  } else {
    warnings.push('Les listes de positions n’ont pas le même nombre d’éléments')
  }
  // On compare les listes de titres
  if (graphe1.titreNodes.length !== graphe2.titreNodes.length) {
    warnings.push('Les titres n’ont pas le même nombre d’éléments')
  } else if (graphe1.titreNodes.join('\n') !== graphe2.titreNodes.join('\n')) {
    warnings.push('Les titres sont différents')
  }
  return { ok: true, warnings, errors: [] }
}

export default GraphV1
