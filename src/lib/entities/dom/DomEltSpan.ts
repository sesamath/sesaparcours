import DomElt from './DomElt'

export interface DomEltSpanValues {
  content: string
  persistent: boolean
  className: string
}

/**
 * DomElt pour mettre un span dans une zone grâce à display()
 * ça permet d’avoir des portions d’énoncé liés à des items du playground qui peuvent être persistants du coup.
 */
class DomEltSpan extends DomElt {
  protected constructor (parent: HTMLElement, { content, persistent, className }: DomEltSpanValues) {
    super(parent, { tag: 'span', props: { textContent: content, persistent, className } })
    //  this.initFeedback() Pas de feedback sur du texte, seulement sur des inputs
  }

  /**
   * Crée un DomElt de type span pour contenir du texte que l’on peut enlever d’un parent.removeChild()
   * @param parent
   * @param content
   * @param persistent
   */
  static create (parent: HTMLElement, { content, persistent = false, className = '' }: DomEltSpanValues) {
    return new DomEltSpan(parent, { content, persistent, className })
  }

  getValue (): string {
    return this.container.innerText
  }

  isAnswered (): boolean {
    return true
  }

  reset () {
    this.container.innerText = ''
  }
}

export default DomEltSpan
