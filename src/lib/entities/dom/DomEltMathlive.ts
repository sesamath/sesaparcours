import { MathfieldElement } from 'mathlive'
import DomElt from 'src/lib/entities/dom/DomElt'
import type { MathfieldWithKeyboard } from 'src/lib/types'
import VirtualKeyboard from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'

export interface IDomEltMathliveValues {
  container?: HTMLElement
  name?: string
  value: string
  restriction?: RegExp
  commands?: string[]
  persistent?: boolean
}

/**
 * Un élément spécial pour gérer les placeholders contenus dans un mathfieldElement readonly.
 * Chaque placeholder correspond à un item dans playground.items
 * Le clavier virtuel est mutualisé au niveau du mathfieldElement qui contient les placeholders.
 */
class DomEltMathlive extends DomElt {
  protected constructor (parent: HTMLElement, {
    persistent,
    value,
    restriction,
    commands
  }: IDomEltMathliveValues) {
    super(parent, { tag: 'span', props: { persistent } })
    const mfe = new MathfieldElement() as MathfieldWithKeyboard
    this.container.appendChild(mfe)
    if (value !== '') {
      mfe.menuItems = []
      mfe.readOnly = true
      let mfeValue = ''
      while (value) {
        const chunks = /^(.*?)%\{([^}]+)}(.*)$/.exec(value)
        if (chunks) {
          const [, start, n, end] = chunks
          const fieldName = n as string
          if (fieldName == null) throw Error(`Définition de ${fieldName} manquante`)
          mfeValue += start as string
          mfeValue += `\\placeholder[${fieldName}]{}`
          value = end ?? ''
        } else {
          mfeValue += value
          value = ''
        }
      }
      mfe.value = mfeValue
    } else { // ne serait-ce déjà pas le cas ?
      mfe.value = ''
      mfe.readOnly = false
      mfe.menuItems = []
    }
    const keyboard = new VirtualKeyboard(mfe, restriction ?? /./, {
      acceptSpecialChars: true,
      commandes: commands
    })
    mfe.virtualKeyboard = keyboard
    this.element = mfe
    this.initFeedback()
  }

  static create (parent: HTMLElement, props: IDomEltMathliveValues) {
    return new DomEltMathlive(parent, props)
  }

  /**
   * retourne la valeur de l’input associé à this.name
   */
  getValue (): string | Record<string, string> {
    const prompts = (this.element as MathfieldWithKeyboard).getPrompts()
    if (prompts.length === 0) {
      return (this.element as MathfieldWithKeyboard).value
    }
    const values: Array<[string, string]> = []
    for (const prompt of prompts) {
      values.push([prompt, (this.element as MathfieldWithKeyboard).getPromptValue(prompt)])
    }
    return Object.fromEntries(values)
  }

  /**
   * Retourne true si le placeholder a été modifié
   */
  isAnswered (): boolean {
    const prompts = (this.element as MathfieldWithKeyboard).getPrompts()
    return prompts == null
      ? !((this.element as MathfieldWithKeyboard).value == null || (this.element as MathfieldWithKeyboard).value === '')
      : prompts.filter((el: string) => (this.element as MathfieldWithKeyboard).getPromptValue(el) !== '' || (this.element as MathfieldWithKeyboard).getPromptValue(el) != null).length === prompts.length
  }

  setFocus () {
    const prompts = (this.element as MathfieldWithKeyboard).getPrompts()
    if (prompts.length === 0) this.element.focus()
    else {
      (this.element as MathfieldWithKeyboard).selection = 0
      this.element.focus()
    }
  }
}

export default DomEltMathlive
