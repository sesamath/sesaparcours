import { MathfieldElement } from 'mathlive'
import { Choice, setPointEventsToNone } from 'src/lib/outils/listeDeroulante/ListeDeroulante'
import { addElement, addTextContent, setOk } from 'src/lib/utils/dom/main'

import DomElt from './DomElt'

// une interface pour les DomEltCheckboxes ci-dessous
export interface DomEltCheckboxesValues {
  container?: HTMLElement
  choices: Choice[]
  verticalBlock?: boolean
  labelProps?: Partial<HTMLLabelElement>
  inputProps?: Partial<HTMLInputElement>
  persistent?: boolean
}

/**
 * DomElt pour une liste de cases à cocher (précédées de leur label respectif)
 */
class DomEltCheckboxes extends DomElt {
  private labels: HTMLLabelElement[]
  private inputs: HTMLInputElement[]

  protected constructor (parent: HTMLElement, {
    choices,
    labelProps,
    inputProps,
    verticalBlock,
    persistent
  }: DomEltCheckboxesValues) {
    super(parent, { tag: verticalBlock ? 'div' : 'span', props: { persistent } })
    if (verticalBlock) this.container.classList.add('verticalCb')
    this.labels = []
    this.inputs = []

    for (const choice of choices) {
      let label: string | MathfieldElement | HTMLImageElement
      let value: string
      if (typeof choice === 'string') {
        label = choice
        value = choice
      } else if (typeof choice === 'object') {
        if ('latex' in choice) {
          label = new MathfieldElement()
          label.readOnly = true
          label.style.backgroundColor = 'transparent'
          label.style.border = 'none'
          label.value = choice.latex ?? ''
          const shadowRoot = label.shadowRoot
          if (shadowRoot) shadowRoot.innerHTML = setPointEventsToNone(shadowRoot.innerHTML)
        } else if ('image' in choice) {
          label = document.createElement('img')
          label.src = choice.image ?? ''
        } else if ('label' in choice) {
          label = choice.label ?? ''
        } else {
          throw Error(`type de choix non reconnu ${choice}`)
        }

        value = choice.value
      } else {
        throw Error(`type de choix non reconnu ${choice}`)
      }
      const labelElt = addElement(this.container, 'label', labelProps)
      if (typeof label === 'string') {
        addTextContent(labelElt, label)
      } else {
        labelElt.appendChild(label)
      }
      const iProps = Object.assign({}, inputProps, { type: 'checkbox', value })
      const input = addElement(labelElt, 'input', iProps)
      input.addEventListener('click', this.resetFeedbackListener)
      this.labels.push(labelElt)
      this.inputs.push(input)
    }
    if (this.inputs[0] != null) this.element = this.inputs[0]
    this.initFeedback()
  }

  /**
   * Ajoute des checkboxes dans parent
   * @param parent
   * @param name
   * @param choices
   * @param verticalBlock
   * @param labelProps
   * @param inputProps
   */
  static create (parent: HTMLElement, {
    choices,
    verticalBlock = false,
    labelProps,
    inputProps,
    persistent = false
  }: DomEltCheckboxesValues) {
    return new DomEltCheckboxes(parent, { choices, verticalBlock, labelProps, inputProps, persistent })
  }

  /**
   * Ne pas préciser l’index permet de mettre globalement 'ok' ou 'ko'
   * Avec un index pour configurer individuellement le feedback sur la checkbox concernée
   * @param isOk
   * @param feedback
   * @param index
   */
  setFeedback (isOk: boolean, feedback: string, index?: number): void {
    if (typeof index !== 'number') return super.setFeedback(isOk, feedback)

    // c’est un feedback local à une case à cocher
    const label = this.labels[index]
    if (label == null) {
      console.error(Error(`index ${index} invalide`))
      return super.setFeedback(isOk, feedback)
    }
    // On ajoute un span.feedback dans le label s’il n’y est pas déjà
    const feedbackElt: HTMLSpanElement = label.querySelector('span.feedback') ?? addElement(label, 'span', { className: 'feedback' })
    feedbackElt.innerText = feedback
    if (feedback) {
      setOk(feedbackElt, isOk)
      feedbackElt.style.display = 'inline'
    } else {
      feedbackElt.style.display = 'none'
    }
  }

  /**
   * retourne la liste des valeurs cochées
   */
  getValue (): string[] {
    const responses: string[] = []
    for (const input of this.inputs) {
      if (input.checked) responses.push(input.value)
    }
    return responses
  }

  /**
   * Retourne true si au moins une case est cochée
   */
  isAnswered (): boolean {
    return this.inputs.some(input => input.checked)
  }

  /**
   * Décoche toutes les cases
   */
  reset () {
    for (const input of this.inputs) {
      input.checked = false
    }
  }
}

export default DomEltCheckboxes
