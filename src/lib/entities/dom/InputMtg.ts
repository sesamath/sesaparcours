// types
import type { Item } from 'src/lib/player/types'
import type { MtgAppLecteurApi } from 'src/types/mathgraph'

export interface IDomEltMtgValues {
  value: string
  svgOptions: object
  mtgOptions: object
  inputProps: Record<string, { initialValue: number }>
  name: string
  persistent: boolean
}

/**
 * Ceci n’est pas un élément du Dom, c’est la figure affichée par l’AppMtg qui est dans le Dom. On utilise l’API pour accéder aux valeurs choisies
 * Cette classe permet de créer les items correspondant aux calculs inclus dans la figure
 */
class InputMtg implements Item {
  /** Le nom du calcul dans la figure mathgraph */
  name: string
  initialValue: number
  app: MtgAppLecteurApi
  container: HTMLElement
  spanFeedback!: HTMLSpanElement

  protected constructor ({ mtgAppLecteurApi, name, initialValue, divMtg, persistent = false }: {
    mtgAppLecteurApi: MtgAppLecteurApi,
    name: string,
    initialValue: number,
    divMtg: HTMLElement,
    persistent: boolean
  }) {
    this.app = mtgAppLecteurApi
    this.name = name
    this.initialValue = initialValue // une tentative pour donner du sens à isAnswered pour les calculs Mtg
    this._persistent = persistent
    this.container = divMtg
  }

  private _persistent: boolean

  public get persistent () {
    return this._persistent
  }

  static create ({ mtgAppLecteurApi, name, initialValue, divMtg, persistent = false }: {
    mtgAppLecteurApi: MtgAppLecteurApi,
    name: string,
    initialValue: number,
    divMtg: HTMLDivElement,
    persistent: boolean
  }) {
    return new InputMtg({ mtgAppLecteurApi, name, initialValue, divMtg, persistent })
  }

  /**
   * retourne la valeur de l’input associé à l’item (this.name est le nom du calcul dans la figure Mtg)
   */
  getValue (): string {
    return String(this.app.getValue({ x: this.name }))
  }

  /**
   * Retourne true si la valeur du calcul a changé
   */
  isAnswered (): boolean {
    return this.app.getValue({ x: this.name }) !== this.initialValue
  }

  setFeedback (isOk: boolean, feedback: string) {
    if (isOk) {
      this.spanFeedback.classList.add('ok')
      this.spanFeedback.classList.remove('ko')
    } else {
      this.spanFeedback.classList.add('ko')
      this.spanFeedback.classList.remove('ok')
    }
    // on met le feedback en title, pour le tooltip au rollover (à améliorer, pas terrible sur tablette)
    this.spanFeedback.textContent = feedback
  }

  setFocus () {
    this.container.focus()
  }
}

export default InputMtg
