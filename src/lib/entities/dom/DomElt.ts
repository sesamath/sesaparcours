import type { Item, ItemValue } from 'src/lib/player/types'
import { InputWithKeyboard, MathfieldWithKeyboard } from 'src/lib/types'

import type { NodeListOf, TagNameCustom, TagNameHtml } from 'src/lib/utils/dom/dom'
import { addElement, setOk } from 'src/lib/utils/dom/main'

interface IProps extends Partial<HTMLElement> {
  // @todo revoir la façon dont on gère le côté persistent : utiliser une classe css.
  persistent?: boolean
}

interface IDomEltOptions {
  tag: TagNameHtml | TagNameCustom
  props?: IProps
}

/**
 * Classe parente de tous nos DomElt, ne peut pas être instanciée directement
 */
class DomElt implements Item {
  container: HTMLElement
  element!: MathfieldWithKeyboard | InputWithKeyboard | HTMLSelectElement | HTMLInputElement | HTMLElement
  protected resetFeedbackListener: () => void
  private spanFeedback!: HTMLSpanElement

  protected constructor (parent: HTMLElement, { tag, props }: IDomEltOptions) {
    this._persistent = Boolean(props?.persistent)
    this.container = addElement(parent, tag, props)
    // listener pour masquer le feedback quand on modifie les cases cochées
    this.resetFeedbackListener = () => {
      for (const elt of this.container.querySelectorAll('.feedback') as NodeListOf<HTMLSpanElement>) {
        elt.style.display = 'none'
      }
    }
  }

  private _persistent: boolean

  public get persistent () {
    return this._persistent
  }

  /**
   * Retourne la valeur de l’élément (string vide si rien n’a été saisi)
   */
  getValue (): ItemValue {
    console.error(Error('à implémenter dans la classe fille'))
    return ''
  }

  /**
   * Retourne true si l’item a été modifié
   */
  isAnswered (): boolean {
    console.error(Error('à implémenter dans la classe fille'))
    return true
  }

  /**
   * Affecte la valeur de feedback (masque l’élément si string vide)
   * @param {boolean} isOk
   * @param {string} feedback
   */
  setFeedback (isOk: boolean, feedback: string) {
    if (!this.spanFeedback) throw Error('Impossible d’ajouter un feedback, initFeedback() n’a pas été appelé')
    this.spanFeedback.textContent = feedback
    if (feedback) {
      setOk(this.spanFeedback, isOk)
      this.spanFeedback.style.display = 'inline'
    } else {
      this.spanFeedback.style.display = 'none'
    }
  }

  setFocus () {
    this.element.focus()
  }

  protected initFeedback () {
    this.spanFeedback = addElement(this.container, 'span', { className: 'feedback', style: { display: 'none' } })
  }
}

export default DomElt
