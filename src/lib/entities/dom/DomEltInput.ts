import type { InputWithKeyboard } from 'src/lib/types'
import { addElement, addTextContent } from 'src/lib/utils/dom/main'
import DomElt from './DomElt'

type Replacer = (src: string) => string

// règles classiques de restrictions qu’on identifie par un 'mot-clé'
const knownRestrictions: Record<string, RegExp | Replacer> = {
  decimal: (inputString: string) => {
    // si c’est déjà numérique avec virgule on ne fait rien
    if (/^-?\d+,?\d*$/.test(inputString)) return inputString
    const src = inputString
      // on remplace . par ,
      .replace(/\./g, ',')
      // on vire ce qui non numérique
      .replace(/[^\d,-]/g, '')
    // reste à vérifier que le - est forcément en premier et la virgule pas au début
    // (faut autoriser qu’elle soit à la fin pour pouvoir la saisir)
    const firstCipherIndex = inputString.startsWith('-') ? 1 : 0
    let hasSep = false
    return Array.from(src).reduce((acc: string, char: string, index: number): string => {
      if (char === '-' && index !== 0) return acc
      if (char === ',') {
        if (index <= firstCipherIndex || hasSep) return acc
        hasSep = true
      }
      return acc + char
    }, '')
  },
  minuscules: /[a-z]/g,
  majuscules: /[A-Z]/g,
  lettres: /[a-zA-Z]/g,
  lettresAccents: /[a-zA-ZáäâàéëêèíïîìóöôòúüûùÿÁÄÂÀÉËÊÈÍÏÎÌÓÖÔÚÜÛÙŸæÆœŒçÇñÑ]/g,
  alpha: /[a-zA-Z0-9]/g
}
// Une restriction de remplacement (voir 'decimal' pour l’exemple).
export type RestrictionCallback = (inputString: string) => string
// la liste des mots-clés admis.
export type RestrictionList = keyof typeof knownRestrictions

export interface DomEltInputValues {
  container?: HTMLElement
  element?: InputWithKeyboard
  label?: string
  labelProps?: Partial<HTMLLabelElement>
  inputProps?: Partial<HTMLInputElement>
  restriction: RegExp | RestrictionList | RestrictionCallback
  kbRestriction?: RegExp
  persistent?: boolean
}

/**
 * DomElt pour un tag input classique (zone de saisie texte ou number ou date)
 */
class DomEltInput extends DomElt {
  protected constructor (parent: HTMLElement, {
    label,
    inputProps,
    labelProps,
    persistent,
    restriction
  }: DomEltInputValues) {
    super(parent, { tag: 'label', props: Object.assign({}, labelProps, persistent) })
    // this.container est le tag <label> que le constructeur de DomElt a créé
    if (label) addTextContent(this.container, label)
    this.element = addElement(this.container, 'input', inputProps) as InputWithKeyboard
    this.element.className = 'inputWithKeyboard'
    this.element.addEventListener('input', () => {
      this.element.setAttribute('value', (this.element as InputWithKeyboard).value)
    })

    // Application de la restriction si besoin => mise en place d’un listener sur l’inputElt.
    if (restriction) {
      let rule
      if (typeof restriction === 'string') {
        // c’est une propriété de knownRestrictions
        rule = knownRestrictions[restriction]
        if (rule == null) throw Error(`La restriction ${restriction} est inconnue !`)
      } else if (restriction instanceof RegExp) {
        // il faut surtout pas de flag g ici
        if (restriction.global) {
          rule = new RegExp(restriction.source, restriction.flags.replace('g', ''))
        } else {
          rule = restriction
        }
      } else if (typeof restriction === 'function') {
        rule = restriction
      } else {
        console.error(Error('La restriction lié à cet input n’est pas d’un type connu'), restriction)
      }
      if (rule) {
        this.element.addEventListener('input', (event) => {
          const target = event.currentTarget as HTMLInputElement
          if (target == null) return
          // masque le feedback quand on modifie le contenu
          this.setFeedback(true, '')
          const content = target.value
          if (typeof rule === 'function') {
            target.value = rule(content)
          } else {
            let cleaned = ''
            const pbs: string[] = []
            for (const c of content) {
              if (pbs.includes(c)) continue
              if (rule.test(c)) {
                cleaned += c
              } else {
                pbs.push(c)
              }
            }
            target.value = cleaned
            if (pbs.length) {
              const feedback = pbs.length === 1
                ? `Le caractère « ${pbs[0]} » n’est pas autorisé ici`
                : `Ces caractères ne sont pas autorisés ici : ${pbs.join(' ')}`
              this.setFeedback(false, feedback)
            }
          }
        })
      }
    }
    this.initFeedback()
  }

  /**
   * Ajoute un input dans parent
   * @param parent
   * @param values.name
   * @param values.inputProps
   * @param values.labelProps
   */
  static create (parent: HTMLElement, {
    label,
    inputProps,
    labelProps,
    persistent = false,
    restriction
  }: DomEltInputValues): DomEltInput {
    return new DomEltInput(parent, { label, inputProps, labelProps, persistent, restriction })
  }

  /**
   * Retourne la restriction sous forme de RexExp (pour VirtualKeyboard)
   * @param {string} restrictionName
   * @returns {RegExp}
   */
  static getRestriction (restrictionName: string): RegExp {
    const restriction = knownRestrictions[restrictionName]
    if (restriction instanceof RegExp) return restriction
    if (restrictionName === 'decimal') return /^-?\d+,?\d*$/
    throw Error(`${restrictionName} est pas une restriction connue mais non gérée (il faut corriger getRestriction)`)
  }

  getValue (): string {
    return (this.element as InputWithKeyboard).value
  }

  isAnswered (): boolean {
    return this.getValue() !== ''
  }

  reset () {
    (this.element as InputWithKeyboard).value = ''
  }
}

export default DomEltInput
