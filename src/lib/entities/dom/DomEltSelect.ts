import ListeDeroulante, { Choice } from 'src/lib/outils/listeDeroulante/ListeDeroulante'
import DomElt from './DomElt'

// choices obligatoire pour ce DomElt... autres propriétés facultatives
export interface DomEltSelectValues {
  container?: HTMLElement
  choices: Choice[]
  inputProps?: Partial<HTMLSelectElement> & {
    name: string,
    heading?: string,
    onChange?: (choice: Choice) => void,
    select?: number,
  }
  persistent?: boolean
}

/**
 * DomElt pour un liste déroulante
 */
class DomEltSelect extends DomElt {
  options: HTMLOptionElement[]
  listeDeroulante: ListeDeroulante

  protected constructor (parent: HTMLElement, { choices, inputProps, persistent }: DomEltSelectValues) {
    super(parent, { tag: 'span', props: { persistent, className: 'listeDeroulante' } })
    // on doit surcharger this.container avec une ListeDeroulante si les choices contiennent autre chose que du texte.
    // Si un seul des choices est défini avec autre chose que text (latex, image...) alors, on crée une ListeDeroulante à la place
    this.options = []
    this.listeDeroulante = ListeDeroulante.create(this.container, choices, { ...inputProps })
    if (this.listeDeroulante?.container) {
      this.container = this.listeDeroulante.container
    }
    this.element = this.listeDeroulante.container // @todo en attendant mieux car le spanSelected peut être undefined.
    this.initFeedback()
  }

  static create (parent: HTMLElement, { choices, inputProps, persistent = false }: DomEltSelectValues) {
    return new DomEltSelect(parent, { choices, inputProps, persistent })
  }

  /**
   * retourne la liste des valeurs cochées
   */
  getValue (): string {
    const reponse = this.listeDeroulante.response
    if (reponse == null || reponse !== '') {
      return reponse
    } else {
      throw Error('aucune option choisie : il faut utiliser isAnswered() avant ?')
    }
  }

  /**
   * Retourne true si au moins une case est cochée
   */
  isAnswered (): boolean {
    return this.listeDeroulante.response !== ''
  }

  /**
   * Décoche toutes les cases
   */
  reset () {
    for (const input of this.options) {
      input.selected = false
    }
  }
}

export default DomEltSelect
