import { MathfieldElement } from 'mathlive'
import { addElement, setOk } from 'src/lib/utils/dom/main'

import DomElt from './DomElt'

import type { DomEltCheckboxesValues } from './DomEltCheckboxes'

// idem checkbox, sauf que inputProps.name est obligatoire
export interface DomEltRadiosValues extends DomEltCheckboxesValues {
  container?: HTMLElement
  inputProps?: Partial<HTMLInputElement> & { name: string }
  persistent?: boolean
}

/**
 * DomElt pour une liste de boutons radios (précédées de leur label respectif)
 */
class DomEltRadios extends DomElt {
  private labels: HTMLLabelElement[]
  private inputs: HTMLInputElement[]

  protected constructor (container: HTMLElement, {
    choices,
    inputProps,
    labelProps,
    verticalBlock,
    persistent
  }: DomEltRadiosValues) {
    if (inputProps?.name == null) throw Error('inputProps.name est obligatoire pour des input de type radio')
    super(container, { tag: verticalBlock ? 'div' : 'span', props: { persistent } })
    if (verticalBlock) {
      this.container.classList.add('verticalRadios')
    } else {
      this.container.classList.add('horizontalRadios')
    }
    this.labels = []
    this.inputs = []

    for (const choice of choices) {
      let label
      let value
      if (typeof choice === 'string') {
        label = choice
        value = choice
      } else if (typeof choice === 'object') {
        // forcément un IChoiceValue
        if (typeof choice.latex === 'string') {
          label = new MathfieldElement()
          label.readOnly = true
          label.style.backgroundColor = 'transparent'
          label.style.border = 'none'
          label.value = choice.latex
        } else if (typeof choice.image === 'string') {
          label = document.createElement('img')
          label.src = choice.image
        } else if (typeof choice.label === 'string') {
          label = choice.label
        } else {
          throw Error(`type de choix non reconnu ${choice}`)
        }
        value = choice.value
      } else {
        throw Error(`type de choix non reconnu ${choice}`)
      }
      const labelElt = addElement(this.container, 'label', { ...labelProps })
      const input = addElement(labelElt, 'input', { ...inputProps, type: 'radio', value })
      if (typeof label === 'string') {
        labelElt.textContent = label
      } else {
        labelElt.appendChild(label)
      }
      input.addEventListener('click', this.resetFeedbackListener)
      this.labels.push(labelElt)
      this.inputs.push(input)
    }
    if (this.inputs[0] != null) this.element = this.inputs[0]
    this.initFeedback()
  }

  static create (parent: HTMLElement, {
    choices,
    inputProps,
    labelProps,
    verticalBlock = false,
    persistent = false
  }: DomEltRadiosValues) {
    return new DomEltRadios(parent, { choices, inputProps, labelProps, verticalBlock, persistent })
  }

  /**
   * retourne la liste des valeurs cochées
   */
  getValue (): string {
    const checkedInput = this.inputs.find(el => el.checked)
    if (checkedInput == null) throw Error('aucun bouton radio coché : il faut utiliser isAnswered() avant ?')
    return checkedInput.value
  }

  /**
   * Retourne true si au moins une case est cochée
   */
  isAnswered (): boolean {
    return this.inputs.some(input => input.checked)
  }

  /**
   * Décoche toutes les cases
   */
  reset () {
    for (const input of this.inputs) {
      input.checked = false
    }
  }

  /**
   * appelé san préciser l’index permet de mettre globalement 'ok' ou 'ko'
   * appelé avec un index pour configurer individuellement le feedback sur la checkbox concernée
   * @param isOk
   * @param feedback
   * @param index
   */
  setFeedback (isOk: boolean, feedback: string, index?: number): void {
    if (typeof index !== 'number') return super.setFeedback(isOk, feedback)

    // c’est un feedback local à un bouton radio
    const label = this.labels[index]
    if (label == null) {
      console.error(Error(`index ${index} invalide`))
      return super.setFeedback(isOk, feedback)
    }
    // On ajoute un span.feedback dans le label s’il n’y est pas déjà
    const feedbackElt: HTMLSpanElement = label.querySelector('span.feedback') ?? addElement(label, 'span', { className: 'feedback' })
    feedbackElt.innerText = feedback
    if (feedback) {
      setOk(feedbackElt, isOk)
      feedbackElt.style.display = 'inline'
    } else {
      feedbackElt.style.display = 'none'
    }
  }
}

export default DomEltRadios
