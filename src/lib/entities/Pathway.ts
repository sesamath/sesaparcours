import Graph, { type GraphJson } from 'src/lib/entities/Graph'

import type { CheckResults, Resultat, ResultatCallback } from 'src/lib/types'
import { getDelaySec } from 'src/lib/utils/date'

import GraphNode from './GraphNode'
import Result, { type ResultValues } from './Result'

export interface IPathwayValues {
  graph: Graph
  currentNodeId: string
  results: Result[]
  nbRuns: Record<string, number>
  persistentStorage: Record<string, unknown>
}

export interface PathwayJson {
  graph: GraphJson
  currentNodeId: string
  results: ResultValues[]
  nbRuns: Record<string, number>
  persistentStorage: Record<string, unknown>
  startedAt: string
}

export interface PathwayValues {
  graph?: Graph | GraphJson
  isDebug?: boolean
  lastResultat?: Resultat
  resultatCallback?: ResultatCallback
  startedAt?: Date | string
}

const defaultResultatListener: ResultatCallback = (resultat: Resultat) => {
  console.info('Le resultat (le currentNodeId reflète le prochain nœud à effectuer) qui aurait été envoyé si on l’avait réclamé', resultat)
}

function logIfDebug (isDebug: boolean, ...args: unknown[]): void {
  if (!isDebug) return
  console.debug(...args)
}

/**
 * Le cheminement suivi dans le graphe (reprend une partie de l’ancien Parcours de j3p),
 * avec les scores et évaluations.
 * C’est le playground qui gère l’affichage, et le player qui lie les deux.
 * Un cheminement a toujours un graphe non vide et valide.
 * @param graph
 * @constructor
 */
class Pathway {
  /** Le graphe de ce cheminement */
  graph: Graph
  isDebug: boolean
  /** Le résultat du cheminement précédent (même élève et même graphe) */
  lastResultat?: Resultat
  /** La date de démarrage de ce cheminement (en cas de reprise, il faut remonter tous les lastResultat pour retrouver la date initiale) */
  startedAt: Date
  /** Le nœud courant */
  currentNodeId: string
  /**
   * Affiche les arguments en console si on est en mode debug (sinon ne fait rien)
   */
  logIfDebug: (...args: unknown[]) => void
  /** Les résultats des nœuds précédents */
  results: Result[]
  /** Le nb d’exécution de chaque nœud */
  nbRuns: Record<string, number>
  /** Un objet pour passer des infos d’un node à l’autre (une fct définie dans un node réutilisée dans un autre) */
  persistentStorage: Record<string, unknown>
  /** fct appelée à chaque fin de nœud */
  private readonly resultatCallback: ResultatCallback

  constructor (options: PathwayValues) {
    if (options.graph == null) throw TypeError('Il faut fournir un graphe')
    this.isDebug = Boolean(options.isDebug)
    this.logIfDebug = logIfDebug.bind(null, this.isDebug)

    this.startedAt = options.startedAt instanceof Date
      ? options.startedAt
      : typeof options.startedAt === 'string'
        ? new Date(options.startedAt)
        : new Date()
    if (options.lastResultat) {
      // on reprend le pathway là où on l’avait laissé

      // @todo comparer graph avec celui passé dans le résultat
      // il faut que la partie du graphe déjà exécutée soit identique dans les 2
      // - si oui on peut faire une reprise de graphe
      // - sinon, il faut afficher un message expliquant que le graphe a changé et qu’on peut pas reprendre où on en était, obligé de repartir du début (cf v1 pour la comparaison)
      // pour les comparer, il faut
      // - startingId identique
      // - partir de ce node startingId (forcément le premier du résultat) et suivre les nodes du résultat :
      //   - pour chacun, on doit avoir même section et mêmes paramètres (utiliser isSameObject, si on retombe sur un nœud déjà comparé inutile de recommencer)
      // si on est arrivé au dernier node du résultat sans différence, c’est tout bon, on peut faire une reprise de graphe.

      const pathwaySerialized = options.lastResultat.contenu.pathway
      this.graph = new Graph(pathwaySerialized.graph)
      this.lastResultat = options.lastResultat
      this.currentNodeId = pathwaySerialized.currentNodeId
      this.results = pathwaySerialized.results.map(r => new Result(r))
      // on pourrait prendre nbRuns dans pathwaySerialized.nbRuns mais on préfère le reconstruire
      this.nbRuns = {}
      for (const { id } of this.results) {
        if (!this.nbRuns[id]) this.nbRuns[id] = 0
        this.nbRuns[id]++
      }
      // @todo ici il faut utiliser un dateReviver si besoin
      this.persistentStorage = pathwaySerialized.persistentStorage ?? {}
    } else {
      // on démarre au début
      this.graph = options.graph instanceof Graph
        ? options.graph
        : new Graph(options.graph)
      this.persistentStorage = {}
      this.currentNodeId = this.graph.startingId
      this.results = []
      this.nbRuns = {}
    }
    this.resultatCallback = options.resultatCallback ?? defaultResultatListener
    // pas de validate() dans le constructeur, on tolère un graphe incomplet à ce stade
  }

  /**
   * Ajoute un résultat au parcours et change le currentNode d’après le résultat
   * et retourne le feedback à afficher à l’utilisateur
   * @return Le feedback de fin de nœud à afficher (en attendant le clic sur section suivante)
   */
  setNextNode (result: Result): string {
    this.results.push(result)
    const { id, score, evaluation } = result
    if (this.nbRuns[id] == null) {
      this.nbRuns[id] = 1
    } else {
      this.nbRuns[id] += 1
    }

    // On renseigne le prochain currentNodeId dans le pathway
    const nbRuns = this.nbRuns[id] ?? 0
    for (const connector of this.getCurrentNode().connectors) {
      const transition = connector.getTransition(score, nbRuns, evaluation)
      if (transition === null) continue
      // on a trouvé une transition valide
      const { nextId, feedback } = transition
      if (this.graph.getNode(nextId) == null) throw Error(`Aucun nœud d’id ${nextId}, impossible de passer au nœud suivant`)
      this.currentNodeId = nextId
      this.resultatCallback(this.getResultat())
      return feedback
    }
    throw Error(`Aucun branchement du nœud ${id} ne permet de déterminer le nœud suivant, fin du parcours`)
  }

  /**
   * Retourne le résultat du graphe (à partir des Result de chaque nœud)
   * @return {Resultat}
   */
  getResultat (): Resultat {
    if (!this.results.length) throw Error('propriété results manquante')
    // calcul du score (moyenne des scores des Result qui en ont, ou 1 si y’en a aucun)
    const resultsWithScore = this.results.filter(r => (r.score !== -1))
    const score = resultsWithScore.length ? resultsWithScore.reduce((acc, r) => r.score + acc, 0) / resultsWithScore.length : 1
    // durée en s
    let duree = getDelaySec(this.startedAt.valueOf())
    // que l’on ajoute à la durée précédente si y’en avait une
    if (this.lastResultat) duree += this.lastResultat.duree
    return {
      contenu: {
        pathway: this.toJSON()
      },
      date: JSON.stringify(this.startedAt),
      duree,
      fin: this.isFinished(),
      score,
      // valeurs fixes pour tout résultat sesaparcours
      reponse: '',
      type: 'j3p'
    }
  }

  /**
   * Retourne true si le parcours est terminé
   */
  isFinished (): boolean {
    return this.getCurrentNode().isEnd()
  }

  /**
   * Retourne le GraphNode courant
   */
  getCurrentNode (): GraphNode {
    return this.graph.getNode(this.currentNodeId)
  }

  /**
   * Retourne un objet qui peut être transmis à qui on veut (mis dans du json par ex)
   * Les valeurs du pathway courant sans aucune référence vers ses objets internes.
   */
  toJSON (): PathwayJson {
    return {
      graph: this.graph.toJSON(),
      currentNodeId: this.currentNodeId,
      results: this.results.map(result => ({ ...result })),
      nbRuns: this.nbRuns,
      startedAt: JSON.stringify(this.startedAt),
      // clonage du pauvre, faudra utiliser du dateReviver à la récup au cas où il y aurait des types autres que Object ou types natifs
      persistentStorage: JSON.parse(JSON.stringify(this.persistentStorage))
    }
  }

  /**
   * Valide le pathway
   * @return {Promise<CheckResults>}
   */
  validate (): Promise<CheckResults> {
    // @todo vérifier le reste
    return this.graph.validate()
  }
}

export default Pathway
