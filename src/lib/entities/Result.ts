/*
Voir aussi dans src/lib/types.d.ts les types PartialResult, Resultat, LegacyResultat
 */

export interface ResultValues {
  id: string
  score: number
  evaluation?: string
  date?: string
  duree: number
  nextAuto?: boolean
}

/** Résultat d’un nœud */
class Result {
  /** l’id du node qui a envoyé ce résultat */
  id: string
  /** Toutes les sections doivent retourner un score (-1 si pour une section sans score, qui ne fait qu’afficher des infos) */
  score: number
  /** la evaluation associé à ce résultat, éventuellement vide */
  evaluation: string
  /** La date au format json */
  date: string
  /** La durée pour effectuer le node en s */
  duree: number
  nextAuto?: boolean

  constructor ({ id, score, evaluation = '', date, duree, nextAuto }: ResultValues) {
    // id
    if (id.length < 1) throw Error('id vide')
    this.id = id
    // score
    if (score == null) {
      console.warn(`Pas de score pour le nœud ${id}, il faudrait retourner -1 si la section ne retourne pas de score`)
      this.score = -1
    } else {
      // on garde ce typeof (score est toujours number pour ts) car on peut être appelé par du js
      if (typeof score !== 'number') throw Error(`score invalide : ${typeof score}`)
      if (score !== -1 && (score < 0 || score > 1)) throw Error(`score invalide : ${String(score)}`)
      this.score = score
    }
    // evaluation
    this.evaluation = evaluation
    // date
    this.date = date == null ? (new Date()).toJSON() : date
    this.duree = duree
    if (nextAuto === true) this.nextAuto = true
  }
}

export default Result
