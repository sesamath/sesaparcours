Structure des données
=====================

## Ressources

Dans la bibliothèque (sesabibli), on a des ressources de type j3p (aka sesaparcours), qui sont au format :

```js
const ressource = {
    oid: '…',
    rid: '{bibliBaseId}/{oid}',
    titre: '…',
    // autres propriétés communes à tous les types de ressources
    // la propriété dont le format dépend du type de ressource, pour j3p c'est
    parametres: {
        /* le graphe, utilisé par le player + editGraphe + showParcours */
        g: [
            [], // parfois un premier élément vide, pas toujours (c'était pour démarrer à l’index 1…)
            // chaque élément est un nœud
            [
                '1', // 1er elt, l’id, en général un entier en string
                'xxx', // la section, il faut aller voir adresses.js pour connaître son dossier et retrouver le legacy/sections/…/sectionxxx.js
                // les options du nœud, encore un tableau
                [
                    // tous les premiers éléments sont des branchements
                    {
                        nn: 'idNoeudSuivant',
                        conclusion: 'le feedback affiché si la condition est remplie',
                        score: 'une string avec une condition sur le score', // ça peut être '>=0.5' ou 'sans+condition' ou 'sans condition' ou …
                        pe: 'une pe qui match', // ça peut être une liste avec séparateur virgule, ou 'sans condition', ou parfois une condition sur le score, cf convertConnector dans convert1to2.js
                        // ces props sont facultatives
                        max: n, // le nb max de passages dans ce branchement
                        snn: 'idSuivant si max',
                        sconclusion: 'feedback si max'
                    },
                    // branchements suivants éventuels
                    {
                        // … 
                    },
                    // le dernier élément peut contenir le paramétrage de la section (c'est le plus fréquent, mais pas systématique)
                    {
                        // la liste des props dépend de la section, ce sont les mêmes que la propriété 'parametres' de l’objet params exporté par la section
                    }
                ]
            ]
        ],
        /* params sup utilisés par editGraphe et showParcours */
        editgraphes: {
            // les posisions des nœud sur la scène
            positionNodes: [
                [xNode1, yNode1],
                /* … */
            ],
            titresNodes: [
                'titre node1',
                '…'
            ]
        }
    }
}
```

On transforme tout ça en objets

* [Graph](./Graph.html) : le graphe
* [GraphNode](./GraphNode.html) : un nœud dans le graphe
* [Position](./Position.html) : un objet avec les propriétés x et y et une méthode translate()
* [Connector](./Connector.html) : un branchement entre deux nœuds
* [Pathway](./Pathway.html) : le cheminement dans le graphe pour une exécution donnée (contient donc les résultats
  obtenu à chaque nœud parcouru)
* [Result](./Result.html) : un résultat obtenu après l’exécution d’un nœud

## Résultat
Cf la définition de `LegacyResultat` pour le détail du format de données en v1 et `Resultat` pour la v2.
