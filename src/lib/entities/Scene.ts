import { addElement } from 'src/lib/utils/dom/main'

interface SceneValues {
  parent: HTMLElement
  theme: string
}
/**
 * Un objet pour s’interfacer avec le DOM, pas forcément très utile
 * on verra (le player gèrera peut-être directement son DOM)
 * @param graph
 * @constructor
 */
class Scene {
  /** Le conteneur */
  container!: HTMLElement
  /** Le thème css choisi (va imposer la structure de la page) */
  theme!: string

  private constructor () {
    console.warn('Scene constructor à implémenter')
  }

  static create ({ parent, theme }: SceneValues): Scene {
    const scene = new Scene()
    scene.container = addElement(parent, 'div', { className: theme })
    scene.theme = theme
    return scene
  }
}

export default Scene
