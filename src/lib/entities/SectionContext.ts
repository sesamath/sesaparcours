import { isLegacySection } from 'src/lib/core/loadSection'
import { ParamsValues } from 'src/lib/entities/GraphNode'
import Playground from 'src/lib/player/Playground'
import { ensureInteger } from 'src/lib/utils/number'
import Pathway from './Pathway'

import type { PlainObject, Section } from 'src/lib/types'

interface SectionContextValues {
  playground: Playground
  pathway: Pathway
  params: ParamsValues
  section: Section
}

const defaultNbQuestions = 5

/**
 * Le contexte du player passé aux sections (en argument des méthodes enonce/check/solution)
 */
class SectionContext {
  /** Voir Playground.ts : l’objet qui permet d’interagir avec la zone de travail */
  playground: Playground
  /** Voir Pathway.ts : l’objet qui permet de gérer le parcours de l’élève */
  pathway: Pathway
  /** Les valeurs de tous les paramètres (imposés par le graphe ou valeurs par défaut de la section) */
  params: ParamsValues
  /** Sert à passer des données d’une fonction à l’autre de la section */
  storage: PlainObject
  /** Le numéro de la question courante */
  question: number
  /** Le numéro de l’étape courante */
  step: number
  /** Le score à la question courante */
  score: number
  /** Un id d’évaluation (clé de l’export evaluations de la section qui donne la phrase d’évaluation) */
  evaluation: string
  /* Nb de questions de ce node */
  nbQuestions: number
  /** Nb d’étapes de la question */
  nbSteps: number
  /** Nb d’essais déjà effectués */
  nbTries: number
  /** Le nb max d’essai (pour chaque question) */
  maxTries: number
  /** Passe à true si le timeout de la question est atteint */
  isTimeout: boolean

  constructor ({ params, pathway, playground, section }: SectionContextValues) {
    if (isLegacySection(section)) throw Error('SectionContext ne gère pas de section legacy')
    this.pathway = pathway
    this.playground = playground
    this.isTimeout = false

    // les params, si la section propose une fct de màj on l’utilise
    if (section.updateParameters) section.updateParameters(params)

    // on complète avec les valeurs par défaut des params qui ne sont pas précisés dans le graphe
    this.params = {}
    // pour les sections v1 on ne fait rien, les params resteront vide dans le contexte,
    // mais c’est pas grave car c’est alors Parcours qui initialise tout ça et le player v2 ne s’occupe pas des params

    const nodeParams = { ...params }
    if (section.updateParameters) section.updateParameters(nodeParams)

    // on ne s’occupe que des paramètres exportés par la section
    for (const [paramName, { defaultValue, type, multiple }] of Object.entries(section.parameters)) {
      let paramValue
      if (paramName in nodeParams) {
        // le graphe fourni une valeur, on vérifie son type
        const value = nodeParams[paramName]
        // on vérifie le type
        if (multiple && Array.isArray(value)) {
          if (
            (type === 'boolean' && value.every(v => typeof v === 'boolean')) ||
              (type === 'integer' && value.every(v => Number.isInteger(v))) ||
              (type === 'number' && value.every(v => Number.isFinite(v))) ||
              (type === 'string' && value.every(v => typeof v === 'string'))
          ) {
            paramValue = value
          }
        } else if (
          (type === 'boolean' && typeof value === 'boolean') ||
            (type === 'integer' && Number.isInteger(value)) ||
            (type === 'number' && Number.isFinite(value)) ||
            (type === 'string' && typeof value === 'string')
        ) {
          paramValue = value
        }
        if (paramValue == null) {
          console.error(Error(`La valeur de ${paramName} passée dans les paramètres du graphe n’est pas de type ${type} (${typeof value} ${value}) => ${defaultValue} imposé`))
        }
      }
      this.params[paramName] = paramValue ?? defaultValue
    }

    this.nbQuestions = ensureInteger(this.params.nbQuestions, defaultNbQuestions)
    this.nbSteps = ensureInteger(this.params.nbSteps, 1)
    this.maxTries = ensureInteger(this.params.maxTries, 1)
    // tout ça est toujours imposé au démarrage d’une section
    this.evaluation = ''
    this.nbTries = 0
    this.question = 1
    this.score = 0
    this.storage = {}
    this.step = 0
  }
}

export default SectionContext
