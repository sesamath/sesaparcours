Chargement
==========

Tout commence par loader.js, qui est en es5 et déclare nos fonctions de chargement en global
* window.j3pLoad
* window.editGraphe
* window.showPathway
* window.spEdit
* window.spPlay
* window.spView
* window.spValidate
* window.spStart

Au premier appel de chaque fonction, ça charge le code qui le gère puis l’exécute (c'est lazyLoader qui fait ça).

## j3pLoad

Charge un graphe et l’exécute.

En 2023-02 j3pLoad.js ne gère que les graphes v1, c'est la méthode play de src/lib/player/main.ts qui utilise le moteur v2

`window.j3pLoad(container, options, loadCallback)` va charger les outils déclaré par toutes les sections du graphe, toutes les sections, les ajouter dans `Parcours.prototype.SectionXxx` puis lancer le parcours (avec un `new Parcours()`) 

## editGraphe

Édite un graphe v1

Appelé par la page modification de la bibliothèque (et le form spStart)

## showParcours

Affiche un parcours d’élève dans un graphe (v1 seulement).

## spEdit (editeur v2)

Il accepte des graphe v1 ou v2

## spPlay (player v2)

Il accepte des graphe ou resultats v1 ou v2

`window.spPlay(container, { graph, lastResultat })` va 
* créer un Playground dans container
* convertir le graphe s'il est en v1
* créer un Pathway à partir du graphe ou lastResultat
* demander confirmation si lastResultat correspond à un parcours terminé (si confirmé on reprend au début)
* instancier un player
* appeler sa méthode run()

## spView

Affiche un parcours d’élève dans un graphe (prend des résultats v1|v2).

Appelé lors du clic sur "Détails" dans les bilans Labomep (ou ailleurs, c'est une méthode de sesatheque-client qui fait ça).

## spValidate
Valide une ressource (ses paramètres). Appelé par sesatheque-plugin-j3p

## spStart

Affiche le form de test de la page d’accueil (https://j3p.sesamath.net/)
