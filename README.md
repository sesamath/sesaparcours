# SesaParcours

SesaParcours est l’évolution de modulaire de j3p (au sens module javascript, appelé aussi `es module` ou `esm`, avec import/export, ≠ de commonJs ou cjs qui sont des modules à base de require/module.exports).

Remarque jQuery : On reste en 2.x car mathquill passe pas en 3.x, pb notamment avec $().andSelf()

## Prérequis

Installer [NodeJs](https://wiki.sesamath.net/doku.php?id=public:dev:js:nodejs:installation) et [git](https://wiki.sesamath.net/doku.php?id=public:dev:git:start#installation)

Dans toute la doc on utilise `pnpm` à la place de `npm` (plus économe en espace disque mais surtout beaucoup plus rapide), mais vous pouvez retirer le "p" pour revenir au npm classique (installé d’office avec nodeJs).

Pour installer `pnpm` c'est `npm install -g pnpm` (suivant votre config il faudra peut-être lancer cette commande en
 admin)
 
## Installation

La première fois
* Récupération du dépôt : `git clone git@forge.apps.education.fr:sesamath/sesaparcours.git` (ATTENTION à bien prendre cette commande en cliquant sur le bouton cloner sur gitlab, et pas l’url en https, sinon vous devrez saisir login/pass à chaque push)
* installation des dépendances `pnpm i`

Ensuite, pour mettre à jour le dépôt `git pull`, et si le package.json a changé faut refaire un `pnpm i` (dans le
 doute vous pouvez le faire à chaque fois, c'est très rapide si rien n’a changé).
 
## Utilisation
 
Pour tester le code en local, lancer `pnpm start` puis aller consulter `http://localhost:8081/`, qui propose la même chose que l’ancien j3p.html avec le test de
* une section avec son paramétrage par défaut `http://localhost:8081/#section=cm2exN4_22` pour sections/primaire/cahiercm2/sectioncm2exN4_22.js
* un rid, par ex `http://localhost:8081/#rid=sesabibli/32476`, les baseId possibles sont sesabibli ou sesacommun ou sesabiblidev ou sesacommundev
* un graphe passé dans l’url `http://localhost:8081/#graphe=[[1,%22751%22,[{%22pe%22:%22sans%20condition%22,%22nn%22:%22fin%22,%22conclusion%22:%22fin%22}]],[2,%22fin%22,[]]]`

Par exemple, pour reproduire localement ce que l’on a sur https://bibliotheque.sesamath.(dev|net)/public/voir/xxx :
* lancer `pnpm start`
* indiquer xxx en ressource (préciser l’origine si c'est pas sesabibli) et valider pour aller sur http://localhost:8081/#rid=sesabibli/xxx
  
Vous pouvez aussi faire un `pnpm run build:mod` puis aller tester docroot/index.html, par ex depuis webstorm (faire un run sur ce html). Le build:mod ne build que la version module, inutile de compiler la version es5 pour du test en local.

## Développement

Le code est dans un dépôt git, cf https://wiki.sesamath.net/doku.php?id=public:dev:git:scenarios#cas_courantresolution_d_un_bug_ou_ajout_de_fonctionnalite_avec_framagit_gitlab_github
pour la marche à suivre (et package.json pour l'adresse du dépôt).

# Modifications de code, workflow git à suivre

Cf [wiki](https://wiki.sesamath.net/doku.php?id=public:dev:git:scenarios:base)

# consulter la doc

Elle est sur https://j3p.sesamath.net/doc/ ou https://j3p.sesamath.net/tsdoc/ pour la partie concernant le moteur v2 en typescript (pas encore pu fusionner les deux) 

Pour la générer c'est `pnpm run doc`, et vous pouvez ensuite la consulter localement (après un pnpm start) sur http://localhost:8081/doc/index.html ou http://localhost:8081/tsdoc/index.html

# Tester avec une autre ip pour que ce soit accessible avec un autre terminal (tablette ou téléphone par ex)
Vous devez retrouver votre ip sur le réseau local, souvent de la forme 192.168.1.x (pour les box courantes des FAI), et ensuite lancer `HOST=192.168.1.x pnpm start`, ça vous permettra de tester depuis une tablette connectée sur le même wifi que votre PC avec du `http://192.168.1.x/#section=…` ou `http://192.168.1.x/#rid=…`

Vous pouvez également utuiliser la variable d’environnement PORT=xxx pour mettre un autre port que le 8081

# Problèmes connus

On se retrouve parfois avec une erreur `Failed to fetch dynamically imported module:…` assez incompréhensible (le module existe bien, et ça fonctionnait 2 min plus tôt), stopper `vite`, vider `node_modules/.vite` et relancer vite règle le pb…
