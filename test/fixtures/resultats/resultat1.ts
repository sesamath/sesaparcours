import type { LegacyGraph, LegacyPositionNode, LegacyResultat } from 'src/lib/types'

const graphe: LegacyGraph = [
  [
    '1',
    'natureSuite',
    [
      {
        nn: '2',
        pe: 'sans condition',
        conclusion: 'Maintenant déterminons le terme général de la suite'
      },
      {
        nbrepetitions: 1,
        typeSuite: 'géométrique'
      }
    ]
  ],
  [
    '2',
    'SuiteTermeGeneral',
    [
      {
        nn: '3',
        pe: 'sans condition',
        conclusion: 'Et maintenant calculons un terme particulier de la suite'
      },
      {
        nbrepetitions: 1,
        donneesPrecedentes: 'j3p.parcours.donnees[1]',
        indication: '',
        limite: 0,
        nbchances: 2,
        a: '[-1.1;2.1]',
        b: '[-0.5;1]',
        termeInitial: [
          '0|[0.4;5]',
          '0|[-4;-1]',
          '1|[1;6]'
        ],
        UnDirect: true,
        afficherPhrase: true,
        niveau: 'TS'
      }
    ]
  ],
  [
    '3',
    'calculTerme',
    [
      {
        nn: '1',
        score: 'sans+condition',
        conclusion: 'Nouvel exemple',
        sconclusion: 'Dernier exemple',
        snn: '4',
        maxParcours: 2
      },
      {
        nbrepetitions: 1,
        donneesPrecedentes: 'j3p.parcours.donnees[2]',
        boitedialogue: 'oui',
        indication: '',
        limite: 0,
        nbchances: 2,
        a: '[-1.1;2.1]',
        b: '[-0.5;1]',
        termeInitial: [
          '0|[0.4;5]',
          '0|[-4;-1]',
          '1|[1;6]'
        ],
        niveau: 'TS'
      }
    ]
  ],
  [
    '4',
    'natureSuite',
    [
      {
        nn: '5',
        pe: 'sans condition',
        conclusion: 'Maintenant déterminons le terme général de la suite'
      },
      {
        nbrepetitions: 1,
        typeSuite: 'géométrique',
        termeInitial: 1
      }
    ]
  ],
  [
    '5',
    'SuiteTermeGeneral',
    [
      {
        nn: '6',
        pe: 'sans condition',
        conclusion: 'Et maintenant calculons un terme particulier de la suite'
      },
      {
        nbrepetitions: 1,
        donneesPrecedentes: 'j3p.parcours.donnees[4]'
      }
    ]
  ],
  [
    '6',
    'calculTerme',
    [
      {
        nn: '7',
        pe: 'sans condition',
        conclusion: 'Fin'
      },
      {
        nbrepetitions: 1,
        donneesPrecedentes: 'j3p.parcours.donnees[5]'
      }
    ]
  ],
  [
    '7',
    'fin'
  ]
]

export const legacyContenu1 = {
  noeuds: [
    '1',
    '2',
    '3',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6'
  ],
  pe: [
    '',
    '1',
    '1',
    '1',
    '1',
    '1',
    '1',
    '1',
    '1'
  ],
  scores: [
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1
  ],
  ns: [
    2,
    3,
    1,
    2,
    3,
    4,
    5,
    6,
    'fin'
  ],
  boucle: [
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1
  ],
  nextId: '7',
  boucleGraphe: {
    1: [
      null,
      null,
      null,
      2
    ],
    2: [
      null,
      null,
      null,
      2
    ],
    3: [
      null,
      null,
      null,
      2
    ],
    4: [
      null,
      null,
      null,
      2
    ],
    5: [
      null,
      null,
      null,
      2
    ],
    6: [
      null,
      null,
      null,
      2
    ]
  },
  editgraphes: {
    positionNodes: [
      [
        70,
        70
      ],
      [
        345,
        63
      ],
      [
        190,
        203
      ],
      [
        478,
        188
      ],
      [
        270,
        270
      ],
      [
        568,
        289
      ],
      [
        370,
        370
      ]
    ] as LegacyPositionNode[],
    titreNodes: [
      'nature d’une suite',
      'terme général de la suite',
      'calcul d’un terme',
      'nature d’une suite',
      'terme général de la suite',
      'calcul d’un terme'
    ]
  },
  graphe,
  bilans: [
    {
      index: 0,
      id: '1',
      pe: '',
      duree: 14,
      score: 0.5,
      fin: false,
      nextId: '2',
      ns: '2',
      boucle: 1
    },
    {
      index: 1,
      id: '2',
      pe: '1',
      duree: 69,
      score: 0.2,
      fin: false,
      nextId: '3',
      ns: '3',
      boucle: 1
    },
    {
      index: 2,
      id: '3',
      pe: '1',
      duree: 24,
      score: 0.2,
      fin: false,
      nextId: '1',
      ns: '1',
      boucle: 1
    },
    {
      index: 3,
      id: '1',
      pe: '1',
      duree: 19,
      score: 0.8,
      fin: false,
      nextId: '2',
      ns: '2',
      boucle: 1
    },
    {
      index: 4,
      id: '2',
      pe: '1',
      duree: 14,
      score: 0.7,
      fin: false,
      nextId: '3',
      ns: '3',
      boucle: 1
    },
    {
      index: 5,
      id: '3',
      pe: '1',
      duree: 15,
      score: 0.6,
      fin: false,
      nextId: '4',
      ns: '4',
      boucle: 1
    },
    {
      index: 6,
      id: '4',
      pe: '1',
      duree: 13,
      score: 0.9,
      fin: false,
      nextId: '5',
      ns: '5',
      boucle: 1
    },
    {
      index: 7,
      id: '5',
      pe: '1',
      duree: 20,
      score: 0.5,
      fin: false,
      nextId: '6',
      ns: '6',
      boucle: 1
    },
    {
      index: 8,
      id: '6',
      pe: '1',
      duree: 34,
      score: 0.2,
      fin: true,
      nextId: '7',
      ns: 'fin',
      boucle: 1
    }
  ],
  donneesPersistantes: {
    suites: {
      niveau: 'TS',
      coefA: [
        1.7
      ],
      coefB: [
        0.9
      ],
      termeInit: [
        [
          0,
          2.1
        ]
      ],
      typeSuite: 'arithmeticoGeometrique',
      u0: 2.1,
      a: 1.7,
      b: 0.9,
      c: '\\frac{9}{7}',
      nomU: 'v',
      nomV: 'x'
    }
  }
}

/**
 * Un exemple de resultat v1
 */
export const legacyResultat1: LegacyResultat = {
  date: '2024-01-26T08:17:57.139Z',
  duree: 254,
  fin: true,
  score: 1,
  reponse: '',
  contenu: legacyContenu1
}
