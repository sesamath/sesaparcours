import { describe, expect, it, beforeEach, afterEach } from 'vitest'

import { unLatexify } from 'src/lib/mathquill/functions'

describe('matquill unlatexify fonctionne comme attendu avec :', () => {
  // une série de strings, avec la chaîne d’origine et ce qu’on s’attend à trouver en sortie
  Object.entries({
    fractions: [
      ['42\\frac{3}{4}', '42(3)/(4)'],
      ['\\frac{sqrt{2}}{2}', '(sqrt(2))/(2)']
    ],
    pi: [
      ['2\\frac{pi}{4}', '2(pi)/(4)'],
      ['3PI r', '3pir']
    ],
    intervalles: [
      ['\\bracket{3;4}', '\\(3;4)'] // @todo mettre la bonne chaîne en entrée, cet exemple n’existe probablement pas
    ],
    // Ligne suivante pour vérifier que le nouveau MathQuill fonctionne mieux que l’ancien pour les puissances
    puissances: [
      ['2^{12}\\times3^4', '2^(12)*3^4'] // est-ce bien ce qu’on veut ?
    ]
  }).forEach(([label, tests]) => {
    it(label, () => {
      tests.forEach(([orig, wanted]) => expect(unLatexify(orig)).to.equals(wanted))
    })
  })
})
