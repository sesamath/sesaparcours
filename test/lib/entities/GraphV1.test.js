import { describe, it, expect } from 'vitest'

import GraphV1 from 'src/lib/entities/GraphV1'
import { areSameObject } from 'src/lib/utils/comparators'
import { stringify } from 'src/lib/utils/object'
import ressource from 'test/fixtures/ressources/5c5d7085830f3437fdd1119f.json'

describe('GraphV1', () => {
  it('Passe sur des graphes basiques', async () => {
    const { parametres: { g, editgraphes } } = ressource
    const { positionNodes, titreNodes } = editgraphes
    const g1 = new GraphV1({ graphe: g, positionNodes, titreNodes })
    // boucle sur les nœud d’origine normalisés
    const nodes = g.map(GraphV1.splitNode)
    for (const { id, section, branchements, params } of nodes) {
      const { section: node1Section, branchements: node1Branchements, params: node1Params } = GraphV1.splitNode(g1.getNode(id))
      if (node1Section !== 'fin') {
        expect(node1Section).to.equals(section, `Le nœud ${id} n’a plus la même section après conversion\n${section}\ndevenu\n${node1Section}`)
        if (branchements.length > 0) {
          expect(Array.isArray(node1Branchements)).to.equals(true, `Le nœud ${id} n’a plus de branchement après conversion\n${stringify(branchements)}\ndevenu\n${stringify(node1Branchements)}`)
          expect(node1Branchements.length).to.equals(branchements.length, `Le nœud ${id} n’a plus le même nombre de branchements après conversion\n${stringify(branchements)}\ndevenu\n${stringify(node1Branchements)}`)
          for (const [i, br] of branchements.entries()) {
            expect(areSameObject(br, node1Branchements[i])).to.equals(true, `Pb avec l’option ${i} du nœud ${id}\n${stringify(br)}\net\n${stringify(node1Branchements[i])}`)
          }
          expect(areSameObject(params, node1Params)).to.equals(true, `Pb avec les params du nœud ${id}\n${stringify(params)}\net\n${stringify(node1Params)}`)
        }
      }
    } // boucle node
  })

  it('plante sur des données incohérentes', () => {
    const fnErrors = [
      () => new GraphV1({ graphe: { foo: 'bar' } }),
      () => new GraphV1([1, 2, 3]),
      () => new GraphV1([1, 'deux', [3, 4, 5]]),
      // ressemble à un graphe, mais pas de branchement valide
      () => new GraphV1([1, 'deux', [{ snn: 1 }]]),
      () => new GraphV1(['1', 'deux', [{ nn: '2', conclusion: 'y’a pas de nœud 2' }]])
    ]
    for (const fn of fnErrors) {
      expect(fn).to.throw
    }
  })

  it('validate plante sur des branchements avec des pe qui n’existent pas', async () => {
    const graphe = [
      ['1', '501', [{ pe: 'pe_4', nn: '2' }]],
      ['2', 'fin']
    ]
    const g1 = new GraphV1({ graphe })
    const { ok, errors, warnings } = await g1.validate()
    expect(ok).to.equals(false, 'Le validate répond ok alors que pe_4 n’existe pas')
    expect(warnings).to.have.length(0)
    expect(errors).to.have.length(1)
    expect(errors[0]).to.match(/pe_4.*existe pas dans la section/)
  })
  it('validate ok si toutes les pe existent', async () => {
    const graphe = [
      ['1', 'choix', [{ pe: 'pe_1,pe_3', nn: '2' }]],
      ['2', 'fin']
    ]
    const g1 = new GraphV1({ graphe })
    const { ok, errors, warnings } = await g1.validate({ graphe })
    expect(ok).to.equals(true, `Pb avec ${stringify(graphe)}`)
    expect(warnings).to.have.length(0)
    expect(errors).to.have.length(0)
  })
})
