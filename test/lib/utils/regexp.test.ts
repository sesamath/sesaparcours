import {
  captureIntervalleFermeDecimaux,
  captureIntervalleFermeDecimauxPoint,
  captureIntervalleFermeDecimauxVirgule,
  captureIntervalleFermeEntiers,
  captureIntervalleFermeEntiersPositifs,
  convertRestriction,
  testIntervalleDecimaux,
  testIntervalleEntiers,
  testIntervalleFermeDecimaux,
  testIntervalleFermeDecimauxPoint,
  testIntervalleFermeDecimauxVirgule,
  testIntervalleFermeEntiers,
  testIntervalleFermeEntiersPositifs
} from 'src/lib/utils/regexp'
import { describe, expect, it } from 'vitest'

const testsEntiersOk = new Map()
// les premiers marchent pour les positifs
testsEntiersOk.set('[0;2]', ['0', '2'])
testsEntiersOk.set('[1;2]', ['1', '2'])
testsEntiersOk.set('[1;0]', ['1', '0'])
testsEntiersOk.set('[10;23456789]', ['10', '23456789'])
testsEntiersOk.set('[123456789;2]', ['123456789', '2'])
const nbEntiersPositifs = testsEntiersOk.size
// ensuite que des négatifs
testsEntiersOk.set('[-1;-2]', ['-1', '-2'])
testsEntiersOk.set('[-0;1]', ['-0', '1'])
testsEntiersOk.set('[-1;-0]', ['-1', '-0'])
testsEntiersOk.set('[-1;-2345678]', ['-1', '-2345678'])
testsEntiersOk.set('[-12345678;2345678]', ['-12345678', '2345678'])
testsEntiersOk.set('[12345678;-2345678]', ['12345678', '-2345678'])

const testsEntiersKo = [
  '[-O.42;1]',
  '[-O,42;1]',
  '[01;2]',
  '[1;2.1]',
  '[1;2,1]'
]

const testsDecimauxOk = new Map()
testsDecimauxOk.set('[1,2;3.1]', ['1,2', '3.1'])

const testsDecimauxPointOk = new Map()
testsDecimauxPointOk.set('[1;2.42]', ['1', '2.42'])
testsDecimauxPointOk.set('[0;-2.42]', ['0', '-2.42'])
testsDecimauxPointOk.set('[0.123;-2.42]', ['0.123', '-2.42'])
testsDecimauxPointOk.set('[-0.123;-2.42]', ['-0.123', '-2.42'])
testsDecimauxPointOk.set('[1.23456789;-2.42]', ['1.23456789', '-2.42'])
testsDecimauxPointOk.set('[123456789.8765432;2.42]', ['123456789.8765432', '2.42'])

const testsDecimauxVirguleOk = new Map()
testsDecimauxVirguleOk.set('[1;2,42]', ['1', '2,42'])
testsDecimauxVirguleOk.set('[0;-2,42]', ['0', '-2,42'])
testsDecimauxVirguleOk.set('[0,123;-2,42]', ['0,123', '-2,42'])
testsDecimauxVirguleOk.set('[-0,123;-2,42]', ['-0,123', '-2,42'])
testsDecimauxVirguleOk.set('[1,23456789;-2,42]', ['1,23456789', '-2,42'])
testsDecimauxVirguleOk.set('[123456789,8765432;2,42]', ['123456789,8765432', '2,42'])

const testsDecimauxKo = testsEntiersKo.filter(t => !/[.,]/.test(t)).concat([
  '[1.2.3;3]',
  '[1.2,3;3]',
  '[1,2,3;3]',
  '[012;3.1]'
])

const getVariantes = (intFerme: string): string[] => [
  intFerme,
  intFerme.replace(/^\[/, ']'), // ouvert à gauche
  intFerme.replace(/]$/, '['), // ouvert à droite
  intFerme.replace(/^\[/, ']').replace(/]$/, '[') // les deux
]

describe('Tests des regexp', () => {
  it('testIntervalleEntiers', () => {
    for (const intervalle of testsEntiersOk.keys()) {
      for (const inter of getVariantes(intervalle)) {
        expect(testIntervalleEntiers.test(inter)).to.equals(true, `pb avec ${inter}`)
      }
    }
    for (const intervalle of testsEntiersKo) {
      for (const inter of getVariantes(intervalle)) {
        expect(testIntervalleEntiers.test(inter)).to.equals(false, `pb avec ${inter}`)
      }
    }
  })
  it('testIntervalleFermeEntiers', () => {
    for (const intervalle of testsEntiersOk.keys()) {
      expect(testIntervalleFermeEntiers.test(intervalle)).to.equals(true, `pb avec ${String(intervalle)}`)
    }
    for (const intervalle of testsEntiersKo) {
      expect(testIntervalleFermeEntiers.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
    }
  })
  it('captureIntervalleFermeEntiers', () => {
    for (const [intervalle, expected] of testsEntiersOk.entries()) {
      expect(captureIntervalleFermeEntiers.test(intervalle)).to.equals(true, `pb avec ${String(intervalle)}`)
      expect(captureIntervalleFermeEntiers.exec(intervalle)).to.deep.equals([intervalle, ...expected], `pb avec ${String(intervalle)}`)
    }
    for (const intervalle of testsEntiersKo) {
      expect(captureIntervalleFermeEntiers.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
    }
  })

  it('testIntervalleFermeEntiersPositifs', () => {
    for (const [i, intervalle] of Array.from(testsEntiersOk.keys()).entries()) {
      expect(testIntervalleFermeEntiersPositifs.test(intervalle)).to.equals(i < nbEntiersPositifs, `pb avec ${String(intervalle)}`)
    }
    for (const intervalle of testsEntiersKo) {
      expect(testIntervalleFermeEntiersPositifs.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
    }
  })
  it('captureIntervalleFermeEntiersPositifs', () => {
    for (const [i, [intervalle, expected]] of Array.from(testsEntiersOk.entries()).entries()) {
      if (i < nbEntiersPositifs) {
        expect(captureIntervalleFermeEntiersPositifs.test(intervalle)).to.equals(true, `pb avec ${String(intervalle)}`)
        expect(captureIntervalleFermeEntiersPositifs.exec(intervalle)).to.deep.equals([intervalle, ...expected], `pb avec ${String(intervalle)}`)
      } else {
        expect(captureIntervalleFermeEntiersPositifs.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
        expect(captureIntervalleFermeEntiersPositifs.exec(intervalle)).to.equals(null, `pb avec ${String(intervalle)}`)
      }
    }
    for (const intervalle of testsDecimauxKo) {
      expect(testIntervalleFermeEntiersPositifs.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
    }
  })

  it('testIntervalleFermeDecimaux', () => {
    const intervalles = Array.from(testsDecimauxOk.keys())
      .concat(Array.from(testsDecimauxPointOk.keys()))
      .concat(Array.from(testsDecimauxVirguleOk.keys()))
      .concat(Array.from(testsEntiersOk.keys()))
    for (const intervalle of intervalles) {
      expect(testIntervalleFermeDecimaux.test(intervalle)).to.equals(true, `pb avec ${String(intervalle)}`)
    }
    for (const intervalle of testsDecimauxKo) {
      expect(testIntervalleFermeDecimaux.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
    }
  })
  it('testIntervalleDecimaux', () => {
    const intervalles = Array.from(testsDecimauxOk.keys())
      .concat(Array.from(testsDecimauxPointOk.keys()))
      .concat(Array.from(testsDecimauxVirguleOk.keys()))
      .concat(Array.from(testsEntiersOk.keys()))
    for (const intervalle of intervalles) {
      for (const inter of getVariantes(intervalle)) {
        expect(testIntervalleDecimaux.test(inter)).to.equals(true, `pb avec ${inter}`)
      }
    }
    for (const intervalle of testsDecimauxKo) {
      for (const inter of getVariantes(intervalle)) {
        expect(testIntervalleDecimaux.test(inter)).to.equals(false, `pb avec ${inter}`)
      }
    }
  })
  it('captureIntervalleFermeDecimaux', () => {
    const entries = Array.from(testsDecimauxPointOk.entries())
      .concat(Array.from(testsDecimauxVirguleOk.entries()))
      .concat(Array.from(testsEntiersOk.entries()))
    for (const [intervalle, expected] of entries) {
      expect(captureIntervalleFermeDecimaux.test(intervalle)).to.equals(true, `pb avec ${String(intervalle)}`)
      expect(captureIntervalleFermeDecimaux.exec(intervalle)).to.deep.equals([intervalle, ...expected], `pb avec ${String(intervalle)}`)
    }
    for (const intervalle of testsDecimauxKo) {
      expect(captureIntervalleFermeDecimaux.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
    }
  })

  it('testIntervalleFermeDecimauxPoint', () => {
    const intervalles = Array.from(testsDecimauxPointOk.keys())
      .concat(Array.from(testsEntiersOk.keys()))
    for (const intervalle of intervalles) {
      expect(testIntervalleFermeDecimauxPoint.test(intervalle)).to.equals(true, `pb avec ${String(intervalle)}`)
    }
    for (const intervalle of testsDecimauxKo) {
      expect(testIntervalleFermeDecimauxPoint.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
    }
  })
  it('captureIntervalleFermeDecimauxPoint', () => {
    const intervalles = Array.from(testsDecimauxPointOk.entries())
      .concat(Array.from(testsEntiersOk.entries()))
    for (const [intervalle, expected] of intervalles) {
      expect(captureIntervalleFermeDecimauxPoint.test(intervalle)).to.equals(true, `pb avec ${String(intervalle)}`)
      expect(captureIntervalleFermeDecimauxPoint.exec(intervalle)).to.deep.equals([intervalle, ...expected], `pb avec ${String(intervalle)}`)
    }
    for (const intervalle of testsDecimauxKo) {
      expect(captureIntervalleFermeDecimauxPoint.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
    }
  })

  it('testIntervalleFermeDecimauxVirgule', () => {
    const intervalles = Array.from(testsDecimauxVirguleOk.keys())
      .concat(Array.from(testsEntiersOk.keys()))
    for (const intervalle of intervalles) {
      expect(testIntervalleFermeDecimauxVirgule.test(intervalle)).to.equals(true, `pb avec ${String(intervalle)}`)
    }
    for (const intervalle of testsDecimauxKo) {
      expect(testIntervalleFermeDecimauxVirgule.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
    }
  })
  it('captureIntervalleFermeDecimauxVirgule', () => {
    const intervalles = Array.from(testsDecimauxVirguleOk.entries())
      .concat(Array.from(testsEntiersOk.entries()))
    for (const [intervalle, expected] of intervalles) {
      expect(captureIntervalleFermeDecimauxVirgule.test(intervalle)).to.equals(true, `pb avec ${String(intervalle)}`)
      expect(captureIntervalleFermeDecimauxVirgule.exec(intervalle)).to.deep.equals([intervalle, ...expected], `pb avec ${String(intervalle)}`)
    }
    for (const intervalle of testsDecimauxKo) {
      expect(captureIntervalleFermeDecimauxVirgule.test(intervalle)).to.equals(false, `pb avec ${String(intervalle)}`)
    }
  })
  it('convertRestriction', () => {
    // une série de string => RegExp attendues (les ranges sont toujours en premier dans le résultat)
    const expected = {
      abC2: '[abC2]',
      'a-z[': '[a-z[]',
      '2-Z': '[2\\-Z]', // range invalide, ça échappe le -
      '\\da-z': '[a-z\\d]',
      'a-z\\d': '[a-z\\d]',
      'a-z A-Z0-9': '[a-zA-Z0-9 ]',
      'a-zC-M0-Z': '[a-zC-M0\\-Z]'
    }
    for (const [chars, restrictionExpected] of Object.entries(expected)) {
      expect(convertRestriction(chars).source).to.equals(restrictionExpected, `Pb avec ${chars}`)
    }
    for (const noRestriction of [null, undefined, '']) {
      // @ts-expect-error c’est volontaire pour vérifier que ça retourne le pb
      expect(convertRestriction(noRestriction).source).to.equals('.', `Pb avec "${String(noRestriction)}"`)
    }
    for (const weird of [true, false, 0, 1]) {
      // @ts-expect-error c’est volontaire pour vérifier que ça plante
      expect(() => convertRestriction(weird)).to.throw(/invalide/)
    }
    for (const invalidRange of ['3-1', '0-9c-a']) {
      expect(() => convertRestriction(invalidRange)).to.throw(/Invalid regular expression/)
    }
  })
})
