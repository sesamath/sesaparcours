import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

import Queue from 'src/lib/utils/Queue'

use(sinonChai)

const logError = (error) => console.error(error)

describe('Queue', function () {
  // pour le stub de la console
  let consoleErrorStub
  // pour les erreurs passées à errorCallback
  let errors
  const errorCallback = (error) => errors.push(error.message)

  const checkFakeError = () => {
    expect(consoleErrorStub).to.not.have.been.called
    expect(errors).to.have.length(1)
    expect(errors[0]).to.match(/fake Error/)
    errors = []
  }
  const checkNoError = () => {
    expect(consoleErrorStub).to.not.have.been.called
    expect(errors).to.have.length(0)
  }
  const checkRejectedPromise = async (promise) => {
    expect(promise).is.a('Promise')
    try {
      await promise
      // on doit pas passer là, ici c’est à vitest qu’on retourne une promesse rejetée
      throw Error('La promesse n’est pas rejetée comme on s’y attendait')
    } catch (error) {
      // le rejet
      expect(error.message).to.match(/fake Error/)
      checkFakeError()
    }
  }
  const checkResolvedPromise = async (promise, expectedValue) => {
    expect(promise).is.a('Promise')
    const value = await promise
    expect(value).to.equals(expectedValue)
    checkNoError()
  }

  const syncFakeError = () => { throw Error('fake Error') }
  const asyncFakeError = () => new Promise((resolve, reject) => {
    setTimeout(() => reject(Error('fake Error')), 1)
  })

  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
    errors = []
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  it('add retourne le résultat d’une fct sync dans une promesse', async () => {
    let q = new Queue()
    let retour = q.add(() => 42, logError)
    await checkResolvedPromise(retour, 42)
    // idem avec un fonction "normale" (non fléchée)
    q = new Queue() // test unitaire => on recommence sur une nouvelle queue, le test d’empilement est plus loin
    retour = q.add(function () { return 'foo' }, logError)
    await checkResolvedPromise(retour, 'foo')
  })

  it('add retourne le résultat d’une fct async dans une promesse', async () => {
    const q = new Queue()
    const onSuccess = () => new Promise((resolve) => {
      setTimeout(() => resolve(42), 1)
    })
    const retour = q.add(onSuccess, logError)
    checkResolvedPromise(retour, 42)
  })

  it('add retourne une promesse échouée si on lui passe une fct sync qui plante, et l’erreur est passée à errorCallback', async () => {
    const q = new Queue()
    const retour = q.add(() => { throw Error('fake Error') }, errorCallback)
    await checkRejectedPromise(retour)
  })

  it('add retourne une promesse échouée si on lui passe une fct async qui plante, et l’erreur est passée à errorCallback', async () => {
    const q = new Queue()
    const retour = q.add(asyncFakeError, errorCallback)
    await checkRejectedPromise(retour)
  })

  it('tout ça fonctionne aussi si on les empile', async () => {
    const q = new Queue()
    let retour = q.add(() => 42, errorCallback)
    await checkResolvedPromise(retour, 42)

    retour = q.add(syncFakeError, errorCallback)
    await checkRejectedPromise(retour)

    retour = q.add(function () { return 'foo' }, errorCallback)
    await checkResolvedPromise(retour, 'foo')

    retour = q.add(asyncFakeError, errorCallback)
    await checkRejectedPromise(retour)
  })

  it('les fonctions sont bien exécutées l’une après l’autre, même si une sync suit une async, mais après le code sync des appels', async () => {
    const q = new Queue()
    const results = []
    q.add(() => results.push(3), errorCallback) // première promesse à être lancée, mais après les appels sync fait ici
    results.push(0) // doit être effectué d’abord
    q.add(() => new Promise((resolve) => {
      setTimeout(() => { results.push(4); resolve() }, 10)
    }), errorCallback)
    results.push(1)
    const last = q.add(() => results.push(5), errorCallback) // devra être effectué en dernier
    results.push(2) // ici aucune des deux fonctions dans la pile ne devrait être lancée
    await last
    expect(results.every((value, index) => value === index)).to.be.true
    checkNoError()
  })

  it('add nous renvoie la promesse rejetée, et on peut continuer à empiler ensuite', async () => {
    const q = new Queue()
    const retour1 = q.add(syncFakeError, errorCallback)
    const retour2 = q.add(() => 1, errorCallback)
    await checkRejectedPromise(retour1)
    await checkResolvedPromise(retour2, 1)
  })

  it('les fonctions sont bien exécutées l’une après l’autre, même si une sync suit une async, mais après le code sync des appels', async () => {
    const q = new Queue()
    const results = []
    q.add(() => results.push(3), errorCallback) // première promesse à être lancée, mais après les appels sync fait ici
    results.push(0) // doit être effectué d’abord
    q.add(() => new Promise((resolve) => {
      setTimeout(() => { results.push(4); resolve() }, 10)
    }), errorCallback)
    results.push(1)
    const last = q.add(() => results.push(5), errorCallback) // devra être effectué en dernier
    results.push(2) // ici aucune des deux fonctions dans la pile ne devrait être lancée
    await last
    expect(results.every((value, index) => value === index)).to.be.true
    checkNoError()
  })
})
