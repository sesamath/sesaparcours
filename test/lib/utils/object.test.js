import { describe, expect, it } from 'vitest'

import { safeEval } from 'src/lib/utils/object'

describe('safeEval', () => {
  it('retourne l’expression pour object|number|boolean|null|undefined', () => {
    for (const expr of [-1, 0, 1, true, false, null, undefined, { foo: 'bar' }, [1, 'deux']]) {
      expect(safeEval(expr)).to.equals(expr)
    }
  })
  it('retourne la valeur de l’expression pour du plain Object (ou un Array)', () => {
    const obj = safeEval('{foo: "bar", baz: 42, \'nul\': null, pi: Math.PI}')
    expect(Object.keys(obj)).to.have.length(4)
    expect(obj.foo).to.equals('bar')
    expect(obj.baz).to.equals(42)
    expect(obj.nul).to.equals(null)
    expect(obj.pi).to.equals(Math.PI)
    const a = safeEval('[1, "deux"]')
    expect(Array.isArray(a)).to.be.true
    expect(a).to.have.length(2)
    expect(a[0]).to.equals(1)
    expect(a[1]).to.equals('deux')
  })
  it('plante sur tout appel de variable globale (window), eval et Function)', () => {
    expect(() => safeEval('(new Function("return 42"))()')).to.throw(Error)
    expect(() => safeEval('new Date()')).to.throw(Error)
    expect(() => safeEval('eval(42)')).to.throw(Error)
    expect(() => safeEval('Object.keys({foo: 42})')).to.throw(Error)
    expect(() => safeEval('console.log(42)')).to.throw(Error)
    expect(() => safeEval('parseInt("42foo")')).to.throw(Error)
    expect(() => safeEval('setTimeout(() => null, 0)')).to.throw(Error)
  })
  it('accepte du js basique (avec Math et Number dispo)', () => {
    expect(safeEval('(function () { var a = 2; return a+40 })()')).to.equals(42)
    expect(safeEval('/foo/.test("afoobar")')).to.equals(true)
  })
})
