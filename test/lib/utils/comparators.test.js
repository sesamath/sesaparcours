import { describe, it, expect } from 'vitest'
import { areSameArray, areSameObject, areSameValues } from 'src/lib/utils/comparators'
import { stringify } from 'src/lib/utils/object'

describe('comparators', () => {
  it('areSameArray', () => {
    const a1 = []
    const a2 = []
    expect(areSameArray(a1, a2)).to.equals(true, `Pb avec ces deux tableaux qui devraient être similaires\n${stringify(a1, 2)}\net\n${stringify(a2, 2)}`)
    a1.push(null)
    expect(areSameArray(a1, a2)).to.equals(false, `Pb avec ces deux tableaux qui devraient être différents\n${stringify(a1, 2)}\net\n${stringify(a2, 2)}`)
    a1.push(42)
    a2.push(null)
    a2.push(42)
    expect(areSameArray(a1, a2)).to.equals(true, `Pb avec ces deux tableaux qui devraient être similaires\n${stringify(a1, 2)}\net\n${stringify(a2, 2)}`)
    a1.push({ a: 'foo', b: { c: 'bar' } })
    a2.push({ b: { c: 'bar' }, a: 'foo' })
    expect(areSameArray(a1, a2)).to.equals(true, `Pb avec ces deux tableaux qui devraient être similaires\n${stringify(a1, 2)}\net\n${stringify(a2, 2)}`)
    expect(areSameValues(a1, a2)).to.equals(true, `Pb avec ces deux tableaux qui devraient être similaires\n${stringify(a1, 2)}\net\n${stringify(a2, 2)}`)
  })
  it('areSameObject', () => {
    const o1 = {
      a: 42,
      b: 'foo',
      c: {
        d: 'bar'
      }
    }
    const o2 = {
      c: {
        d: 'bar'
      },
      b: 'foo',
      a: 42
    }
    expect(areSameObject(o1, o2)).to.equals(true, `Pb avec ces deux objets qui devraient être similaires\n${stringify(o1, 2)}\net\n${stringify(o2, 2)}`)
    o2.e = undefined
    expect(areSameObject(o1, o2)).to.equals(false, `Pb avec ces deux objets qui devraient être différents\n${stringify(o1, 2)}\net\n${stringify(o2, 2)}`)
    expect(areSameValues(o1, o2)).to.equals(false, `Pb avec ces deux objets qui devraient être différents\n${stringify(o1, 2)}\net\n${stringify(o2, 2)}`)
  })
  it('areSameValues', () => {
    for (const v of [1, false, true, null, undefined, NaN, Number.POSITIVE_INFINITY, 0, '', 'foo']) {
      expect(areSameValues(v, v)).to.equals(true, `Pb avec cette valeur qui devraient être identique à elle-même\n${String(v)}`)
    }
  })
})
