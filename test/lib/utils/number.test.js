import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

import { areEqual, fixArrondi, formatNumber, pgcd, pgcdMulti, puissance10, simplifieFraction } from 'src/lib/utils/number'

use(sinonChai)

const arrondis = [
  [4059999.9999999995, 4060000],
  [0.00042999999999999994, 0.00043],
  // le classique 0.1+0.2 qui vaut 0.30000000000000004
  [0.1 + 0.2, 0.3],
  [0.30000000000000004, 0.3],
  [0, 0],
  [1, 1],
  [2, 2],
  [1.23456789, 1.23456789],
  [12345.67899999999, 12345.679],
  [12345.670000001, 12345.670000001],
  [12345.67000000001, 12345.67]
]

describe('areEqual(nb1, nb2)', () => {
  it('retourne true pour des nombres égaux sur les 15 premiers chiffres significatifs', () => {
    for (const [nb, nbFixed] of arrondis) {
      expect(areEqual(nb, nbFixed)).to.equals(true, `Pb avec ${nb} & ${nbFixed}`)
      expect(areEqual(-nb, -nbFixed)).to.equals(true, `Pb avec ${-nb} & ${-nbFixed}`)
    }
  })
  it('throw si on fourni autre chose qu’un number', () => {
    for (const weakNb of [true, false, undefined, null, 'foo', {}, new Date()]) {
      expect(() => areEqual(weakNb, 0)).to.throw(/Il faut passer un nombre/)
      expect(() => areEqual(0, weakNb)).to.throw(/Il faut passer un nombre/)
    }
  })
  it('retourne false pour NaN et true pour Infinity|EPSILON', () => {
    expect(areEqual(Number.NaN, Number.NaN)).to.be.false
    for (const nb of [Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY, Number.EPSILON]) {
      expect(areEqual(nb, nb)).to.equals(true, `Pb avec ${nb}`)
    }
  })
})

describe('fixArrondi(n)', () => {
  it('arrondi correctement les nombres', () => {
    for (const [nb, nbFixed] of arrondis) {
      expect(fixArrondi(nb)).to.equals(nbFixed)
      expect(fixArrondi(-nb)).to.equals(-nbFixed)
    }
  })
  it('throw si on fourni autre chose qu’un number', () => {
    for (const weakNb of [true, false, undefined, null, 'foo', {}, new Date()]) {
      expect(() => fixArrondi(weakNb)).to.throw(/Il faut passer un nombre/)
    }
  })
  it('retourne la valeur fournie si NaN ou infini', () => {
    expect(Number.isNaN(fixArrondi(Number.NaN))).to.be.true
    for (const inf of [Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY]) expect(fixArrondi(inf)).to.equals(inf)
  })
})

describe('formatNumber(n)', () => {
  const tests = [
    [0, '0'],
    [1, '1'],
    [10, '10'],
    [100, '100'],
    [1000, '1 000'],
    [10000, '10 000'],
    [100000, '100 000'],
    [1000000, '1 000 000'],
    [10000000, '10 000 000'],
    [100000000, '100 000 000'],
    [1000000000, '1 000 000 000'],
    [10000000000, '10 000 000 000'],
    [100000000000, '100 000 000 000'],
    [0.1, '0,1'],
    [0.01, '0,01'],
    [0.001, '0,001'],
    [0.0001, '0,000 1'],
    [0.00001, '0,000 01'],
    [0.000001, '0,000 001'],
    [0.0000001, '0,000 000 1'],
    [0.00000001, '0,000 000 01'],
    [0.000000001, '0,000 000 001'],
    [0.0000000001, '0,000 000 000 1'],
    [0.00000000001, '0,000 000 000 01'],
    [0.000000000001, '0,000 000 000 001'],
    [12345, '12 345'],
    [123456, '123 456'],
    [1234567, '1 234 567'],
    [12345.6789012, '12 345,678 901 2'],
    [12345.006789012, '12 345,006 789 012'],
    [0.12345678901, '0,123 456 789 01'],
    ['023.234500', '23,234 5'],
    ['52023.234500', '52 023,234 5'],
    [1e12, '1 000 000 000 000'],
    ['1e12', '1 000 000 000 000'],
    [1e+24, '1 000 000 000 000 000 000 000 000'],
    [1.0234e+24, '1 023 400 000 000 000 000 000 000'],
    ['1e+24', '1 000 000 000 000 000 000 000 000'],
    [1e-9, '0,000 000 001'],
    [12.34e-18, '0,000 000 000 000 000 012 34'],
    [0.1e-8, '0,000 000 001'],
    ['0,1e-8', '0,000 000 001'],
    ['0023.2340500', '23,234 05'],
    ['0023.2340500', '23,234 05', { maxDecimales: 6 }],
    [23.4506, '23,450 6', { maxDecimales: 6 }],
    [12345.006789012, '12 345,007', { maxDecimales: 3 }]
  ]

  it('formate des nombres comme attendu', () => {
    for (const [nb, expected, opts] of tests) {
      expect(formatNumber(nb, opts)).to.equals(expected)
      const isNbString = typeof nb === 'string'
      // idem en négatif
      const nbNeg = isNbString ? '-' + nb : -nb
      if (nb !== 0) expect(formatNumber(nbNeg, opts)).to.equals('-' + expected)
      let nbStr
      if (!isNbString) {
        // même résultat si on lui passe le nb en string, séparateur virgule ou point
        nbStr = expected.replace(/ /g, '')
        expect(formatNumber(nbStr, opts)).to.equals(expected)
        expect(formatNumber(nbStr.replace(/,/, '.'), opts)).to.equals(expected)
      }
    }
  })

  it('plante si on lui passe autre chose qu’un nombre fini', () => {
    for (const v of ['', null, undefined, false, true, new Date(), /foo/, Number.POSITIVE_INFINITY, Number.NaN]) {
      expect(() => formatNumber(v)).to.throw(Error, /faut passer un nombre/, `Pb avec ${v}`)
    }
  })
})

describe('pgcd(n, m)', () => {
  // pour le stub de la console
  let consoleErrorStub
  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  it('retourne le pgcd de deux entiers strictement positifs', () => {
    expect(pgcd(2, 4)).to.equals(2)
    expect(pgcd(0, 4)).to.equals(4)
    expect(pgcd(6, 3)).to.equals(3)
    expect(pgcd(12, 18)).to.equals(6)
    expect(pgcd(18, 12)).to.equals(6)
    expect(pgcd(1, 1)).to.equals(1)
    expect(pgcd(1, 3)).to.equals(1)
    expect(pgcd(3, 1)).to.equals(1)
    expect(pgcd(11, 3)).to.equals(1)
    expect(pgcd(3, 11)).to.equals(1)
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('accepte des presque entiers (à 1e-10', () => {
    expect(pgcd(6 + 1e-11, 3)).to.equals(3)
    expect(pgcd(60 + 1e-11, 90 - 1e-12)).to.equals(30)
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('plante si on fourni autre chose', () => {
    [
      [0, 0],
      [1, 'a'],
      [1],
      [-1, 1e12],
      [-1e8, 1]
    ].forEach(args => {
      expect(pgcd.bind(null, ...args)).to.throw(Error, /pas possible/)
    })
  })
  it('accepte (0, 0) avec l’option acceptOnlyZeros', () => {
    expect(pgcd(0, 0, { acceptOnlyZeros: true })).to.equals(0)
    expect(pgcd(1e-11, -1e-11, { acceptOnlyZeros: true })).to.equals(0)
    expect(consoleErrorStub).to.not.have.been.called
  })
})

describe('pgcdMulti([n, m, …])', () => {
  // pour le stub de la console
  let consoleErrorStub
  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  it('retourne le pgcd de 2 à 10 entiers strictements positifs', () => {
    expect(pgcdMulti([2, 4])).to.equals(2)
    expect(pgcdMulti([6, 3, 9])).to.equals(3)
    expect(pgcdMulti([12, 18, 30, 36])).to.equals(6)
    expect(pgcdMulti([1, 1])).to.equals(1)
    expect(pgcdMulti([1, 3, 6])).to.equals(1)
    expect(pgcdMulti([11, 3, 22, 9])).to.equals(1)
    expect(pgcdMulti([0, 5])).to.equals(5)
    expect(pgcdMulti([120, 24, 48, 36, 144, 12 * 11, 12 * 7, 12 * 8, 12 * 9, 1200000000])).to.equals(12)
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('accepte des presque entiers (à 1e-10', () => {
    expect(pgcdMulti([6 + 1e-11, 3])).to.equals(3)
    expect(pgcdMulti([60 + 1e-11, 90 - 1e-12])).to.equals(30)
    expect(pgcdMulti([60 + 1e-11, 90 - 1e-12, 120, 150])).to.equals(30)
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('accepte [0,0,0…] avec l’option acceptOnlyZeros', () => {
    expect(pgcdMulti([0, 0], { acceptOnlyZeros: true })).to.equals(0)
    expect(pgcdMulti([0, 0, 0, 0], { acceptOnlyZeros: true })).to.equals(0)
    expect(pgcdMulti([0, 0, 0, 5], { acceptOnlyZeros: true })).to.equals(5)
    expect(pgcdMulti([0, 0, 0, 5])).to.equals(5)
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('plante si on fourni autre chose', () => {
    [
      [1, 'a'],
      [0],
      [3],
      [-1],
      [-1, 1],
      [-1, 1],
      [null],
      [undefined, 2, 4]
    ].forEach(args => {
      expect(pgcdMulti.bind(null, args)).to.throw(Error, /(pas possible|pas entier|invalide)/)
    })
  })
})

describe('puissance10', () => {
  it('retourne la puissance de 10 du nombre (BigInt pas trop grand acceptés)', () => {
    const checks = [
      [42, 1],
      [4, 0],
      [-6, 0],
      [1, 0],
      [-1, 0],
      [0, 0],
      [0.1, -1],
      [100, 2],
      [999.9999999, 2],
      [1e2, 2],
      [1e24, 24],
      [-1e244, 244],
      [1e-3, -3],
      [0.24, -2],
      [-0.009, -3],
      [1e-33, -33],
      [-1e-242, -242],
      [-1n, 0],
      [42n, 1],
      [123456789n * BigInt(1e7), 15],
      [-12345678n * BigInt(1e7), 14]
    ]
    for (const [nb, puiss] of checks) {
      expect(puissance10(nb)).to.equals(puiss, `Pb avec ${nb}`)
    }
  })
  it('throw si on lui passe des trucs louches (ou des nombres trop grands)', () => {
    const freaks = ['foo', null, undefined, true, false, '', /foo/, new Date(), {}, BigInt(1e306)]
    for (const freak of freaks) {
      expect(() => puissance10(freak)).to.throw
    }
  })
})

describe('simplifieFraction', () => {
  it('retourne une fraction d’entiers simplifiée (dénominateur positif)', () => {
    const checks = [
      [6, 4, 3, 2],
      [-8, 4, -2, 1],
      [48, -12, -4, 1],
      [12, -21, -4, 7],
      [1, 1, 1, 1],
      [0, -4, 0, 1],
      [17 ** 3 * 7 ** 5, 17 * 49 * 3, 17 * 17 * 7 * 7 * 7, 3]
    ]
    for (const [num, den, n, d] of checks) {
      const [nu, de] = simplifieFraction(num, den)
      expect(nu).to.equals(n, `numérateur ${nu} (attendu ${n}) pour ${num}/${den}`)
      expect(de).to.equals(d, `dénominateur ${de} (attendu ${d}) pour ${num}/${den}`)
    }
  })
  it('plante si on lui file autre chose qu’une fraction, ou un dénominateur nul', () => {
    const freaks = [
      [1n, 2],
      [null, 2],
      [true, 2],
      [2, 0]
    ]
    for (const [num, den] of freaks) {
      expect(() => simplifieFraction(num, den)).to.throw
    }
  })
})
