import { describe, expect, it } from 'vitest'

import { addMaj, getFirstIndexParExcluded } from 'src/lib/utils/string'

// pourquoi il prend pas ça dans nos règles de .eslintrc.cjs ???
/* eslint-disable no-unused-expressions */

describe('addMaj(liste)', () => {
  const tabMaj = Array.from('ABCDEFGHIJKLMNOPQRSTUVWXYZ')

  it('ajoute une majuscule à la liste et la retourne', () => {
    let liste = ['A', 'B', 'C']
    const listeInitiale = [...liste]
    let result = addMaj(liste)
    expect(listeInitiale.includes(result)).to.equals(false, `Pb avec ${liste.join('')} qui retourne ${result}`)
    expect(liste.includes(result)).to.equals(true, `Pb avec ${liste.join('')} qui retourne ${result}`)
    expect(result).to.match(/^[A-Z]$/)
    // idem avec une liste vide
    liste = []
    result = addMaj(liste)
    expect(liste.includes(result)).to.equals(true, `Pb avec ${liste.join('')} qui retourne ${result}`)
    expect(result).to.match(/^[A-Z]$/)
  })
  it('ajoute un suffixe numérique si toutes les lettres sont prises', () => {
    let liste = [...tabMaj]
    let listeInitiale = [...liste]
    let result = addMaj(liste)
    expect(listeInitiale.includes(result)).to.equals(false, `Pb avec ${liste.join('')} qui retourne ${result}`)
    expect(liste.includes(result)).to.equals(true, `Pb avec ${liste.join('')} qui retourne ${result}`)
    expect(result).to.match(/^[A-Z]2$/)
    // on recommence en mettant tous les suffixes 2
    liste = [...tabMaj, ...tabMaj.map(l => `${l}2`)]
    listeInitiale = [...liste]
    result = addMaj(liste)
    expect(listeInitiale.includes(result)).to.equals(false, `Pb avec ${liste.join('')} qui retourne ${result}`)
    expect(liste.includes(result)).to.equals(true, `Pb avec ${liste.join('')} qui retourne ${result}`)
    expect(result).to.match(/^[A-Z]3$/)
  })
})

describe('getFirstIndexParExcluded(st, searched, startIndex)', () => {
  it('trouve l’index de searched quand il est présent', () => {
    const samples:[string, string, number, number][] = [
      ['foo bar (baz foo) baz bar', 'foo', 0, 0],
      ['foo bar (baz foo) baz bar', 'oo', 0, 1],
      ['foo bar (baz foo) baz bar', 'oo', 2, -1],
      ['foo bar (baz foo) baz bar', 'bar', 0, 4],
      ['foo bar (baz fubar) baz bar', 'fubar', 0, -1],
      ['foo bar (baz foo) baz bar', 'bar', 5, 22],
      ['foo bar (baz foo) baz bar', 'bar', 22, 22],
      ['foo bar (baz foo) baz bar', 'bar', 23, -1],
      ['foo bar (baz foo) baz bar', 'bar', 42, -1]
    ]
    for (const [st, searched, startIndex, expected] of samples) {
      expect(getFirstIndexParExcluded(st, searched, startIndex)).to.equals(expected, `Pb avec '${searched}' dans '${st}'`)
    }
  })
})
