// getComparator fait du mtgLoad et il lui faut un environnement browser, on précise ça avec ce commentaire
/* @vitest-environment happy-dom */

import { beforeAll, describe, expect, it } from 'vitest'

import { getComparator } from 'src/lib/outils/mathgraph/index'

const equivs = [
  ['1*x', 'x', ''],
  ['x+z', 'z+x', ''],
  ['3*x', 'x*3', ''],
  ['6*x+2', '2+x*6', ''],
  ['(2*x+1)/(1*x+3)', '(1+x*2)/(3+x)', ''],
  ['2*u*v+w', 'w+u*v*2', 'uvw']
]
const noEquivs = [
  ['x', '2*x'],
  ['x', 'x+0', ''],
  ['2*x', 'x+x']
]
const badSyntax = [
  '2++3x'
]

describe('compareFormula', () => {
  let compareFormula

  beforeAll(async () => {
    compareFormula = await getComparator()
  })

  it('Retourne -2 en cas d’erreur de syntaxe de la solution', () => {
    for (const badExpr of badSyntax) {
      expect(compareFormula(badExpr, 'x')).to.equals(-2, `Pb avec ${badExpr} et x`)
      expect(compareFormula(badExpr, 'a', { varNames: 'ab' })).to.equals(-2, `Pb avec ${badExpr} et a`)
    }
  })
  it('Retourne -1 en cas d’erreur de syntaxe de la réponse', () => {
    for (const badExpr of badSyntax) {
      expect(compareFormula('x', badExpr)).to.equals(-1, `Pb avec ${badExpr} et x`)
      expect(compareFormula('a', badExpr, { varNames: 'ab' })).to.equals(-1, `Pb avec ${badExpr} et a`)
    }
  })
  it('Retourne 1 en cas d’équivalence', () => {
    for (const [expr1, expr2, nomsvar] of equivs) {
      expect(compareFormula(expr1, expr2, { varNames: nomsvar })).to.equals(1, `Pb avec "${expr1}" et ${expr2}`)
    }
  })
  it('Retourne 0 en cas de non équivalence', () => {
    for (const [expr1, expr2] of noEquivs) {
      expect(compareFormula(expr1, expr2)).to.equals(0, `Pb avec "${expr1}" et ${expr2}`)
    }
  })
})
