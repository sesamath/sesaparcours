import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinonChai from 'sinon-chai'
import sinon from 'sinon'

import { j3pMathquillXcas } from 'src/legacy/core/functions'
import { mqNormalise } from 'src/lib/outils/conversion/casFormat'

use(sinonChai)

// Ajout de ce test le 2021-11-10, pour réécrire j3pMathquillXcas en mqNormalise
// et garantir que cela ne modifie pas les comportements existants (sinon pour corriger des bugs).

// la liste des expression à tester, chaque elt du tableau étant au choix
// ['expression à tester', 'expression attendue (retournée par les deux fonctions)']
// ['expression à tester', 'expression attendue par j3pMathquillXcas', 'expression attendue par mqNormalise']
const expressions = [
  ['3\\times2', '3*2'],
  ['\\frac{3}{2}', '(3)/(2)'],
  ['\\frac{3\\times \\PI}{2}', '(3*\\PI)/(2)'],
  ['ln(x)', 'ln(x)'],
  ['sin(42)', 's*i*n (42)', 'sin(42)'],
  ['sin 42', 's*i*n 42', 'sin 42'],
  ['sin x', 's*i*n x', 'sin x'],
  ['2x', '2x'],
  ['ax+b', 'ax+b'],
  ['a x + b', 'ax+b'],
  ['42x+b', '42x+b'],
  ['42 x + b', '42x+b'],
  ['2i', '2*i'],
  ['2i+3', '2*i+3'],
  ['2x i+3', '2x*i+3'],
  ['2xi+3', '2x*i+3'],
  ['2 i + 3', '2*i+3'],
  ['x + 2 i + 3', 'x+2*i+3'],
  ['cos(a \\times b)', 'cos (a*b)', 'cos(a*b)'],
  ['atan(0)', 'atan (0)', 'atan(0)'],
  ['e^42', 'e^4*2'],
  ['3^2', '3^2'],
  ['1e-3', '1e-3'],
  ['0.001', '0.001'],
  ['0,001', '0.001'],
  ['1e-10', '1e-10'],
  ['\\widehat{ABC}', '\\w*i*dehat(ABC)'],
  ['\\infty', '\\i*nfty'],
  [true, 'true'],
  [null, 'null'],
  [undefined, 'undef*i*ned'],
  [42, '42'],
  // ['\\sin\\left(\\pi+\\frac{\\pi}{3}\\right)', '\\s*i*n ((PI)+((PI))/(3))', '\\sin((PI)+((PI))/(3))'],
  // bug j3pMathquillXcas qui devrait retourner pi/3 en ((PI)/(3))    ^-- parenthèse fermante mal placée
  // --> Réponse de Rémi
  // Non, non, il écrit juste pi/3 sous la forme ((PI))/(3), la dernière parenthèse est celle qui ferme le sinus
  // C’est un peu lourd mais c’est parce que pi est renvoyé comme (PI) et pour toute fraction, des parenthèses sont ajoutées
  // autour du numérateur et autour du dénominateur, même quand ils sont simples
  // en attendant que mqNormalise corrige le bug on lui met la même réponse erronée
  ['\\sin\\left(\\pi+\\frac{\\pi}{3}\\right)', '\\s*i*n ((PI)+((PI))/(3))', '\\sin((PI)+((PI))/(3))'],
  ['\\frac{2}{x^2}+\\frac{-1}{4n}', '(2)/(x^2)+(-1)/(4n)'],
  ['\\frac{1-2\\sqrt{2}}{3}', '(1-2*sqrt(2))/(3)'],
  ['3-\\mathrm{e}^{2x}', '3-e^(2x)'],
  ['\\frac{\\sqrt{2}}{3}\\mathrm{e}^{\\frac{2i\\pi}{3}}', '(sqrt(2))/(3)*e^((2*i*(PI))/(3))']
]

describe('j3pMathquillXcas et mqNormalise', () => {
  /* pour vérifier le comportement de qq expressions * /
  let expr = '2x i+3'
  console.log(expr, '=>', j3pMathquillXcas(expr))
  expr = Number.POSITIVE_INFINITY
  console.log(expr, '=>', j3pMathquillXcas(expr))
  return /* */

  // pour le stub de la console
  let consoleErrorStub
  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })
  // helper
  const checkNoConsoleCall = () => expect(consoleErrorStub).to.not.have.been.called
  const checkOneConsoleCall = () => {
    expect(consoleErrorStub).have.been.calledOnce
    consoleErrorStub.resetHistory()
  }

  // et les tests
  it('formatent correctement une string sortie de mathquill (en laissant d’éventuelles expressions latex inchangées)', () => {
    expressions.forEach(([expr, expectedOld, expectedNew]) => {
      if (expectedNew === undefined) expectedNew = expectedOld
      expect(j3pMathquillXcas(expr)).to.equals(expectedOld, `Pb j3pMathquillXcas avec l’expression ${expr}`)
      expect(mqNormalise(expr)).to.equals(expectedNew, `Pb mqNormalise avec l’expression ${expr}`)
      ;['number', 'string'].includes(typeof expr) ? checkNoConsoleCall() : checkOneConsoleCall()
    })
  })
  it('cast des numbers en string', () => {
    [0, 1, -1, 2.345678, Math.PI].forEach(n => {
      expect(j3pMathquillXcas(n)).to.equals(String(n), `Pb avec j3pMathquillXcas sur le number ${n}`)
      expect(mqNormalise(n)).to.equals(String(n), `Pb avec mqNormalise sur le number ${n}`)
    })
    checkNoConsoleCall()
  })
})
