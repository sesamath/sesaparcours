import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

import { entierEnMot, nombreEnMots } from 'src/lib/outils/conversion/nombreEnMots'

use(sinonChai)

// pour les règles d’orthographe de 1990
// https://www.dictionnaire-academie.fr/article/QDL057
// https://www.academie-francaise.fr/sites/academie-francaise.fr/files/rectifications_1990.pdf
// y’a parfois des contradictions, notamment autour des guillemets autour de millions et milliards, mais d’après
// http://dpernoux.free.fr/ecriture.htm
// qui cite notamment
// https://www.academie-francaise.fr/veronique-p-sable-sur-sarthe
// il en faut aussi autour de million et milliards (qui s’accordent quand même)
const entiers = {
  0: 'zéro',
  1: 'un',
  2: 'deux',
  9: 'neuf',
  11: 'onze',
  20: 'vingt',
  21: 'vingt-et-un',
  33: 'trente-trois',
  42: 'quarante-deux',
  54: 'cinquante-quatre',
  61: 'soixante-et-un',
  65: 'soixante-cinq',
  70: 'soixante-dix',
  71: 'soixante-et-onze',
  76: 'soixante-seize',
  80: 'quatre-vingts', // faut pas de s si c’est un adjectif ordinal :-/ ("page quatre-vingt" vs "quatre-vingts pages")
  81: 'quatre-vingt-un',
  87: 'quatre-vingt-sept',
  90: 'quatre-vingt-dix',
  91: 'quatre-vingt-onze',
  98: 'quatre-vingt-dix-huit',
  100: 'cent',
  101: 'cent-un',
  110: 'cent-dix',
  130: 'cent-trente',
  194: 'cent-quatre-vingt-quatorze',
  200: 'deux-cents',
  201: 'deux-cent-un',
  202: 'deux-cent-deux',
  212: 'deux-cent-douze',
  680: 'six-cent-quatre-vingts',
  700: 'sept-cents',
  999: 'neuf-cent-quatre-vingt-dix-neuf',
  1000: 'mille',
  1001: 'mille-un',
  10000: 'dix-mille',
  10001: 'dix-mille-un',
  10013: 'dix-mille-treize',
  10061: 'dix-mille-soixante-et-un',
  100000: 'cent-mille',
  140000: 'cent-quarante-mille',
  105000: 'cent-cinq-mille',
  100600: 'cent-mille-six-cents',
  100070: 'cent-mille-soixante-dix',
  100008: 'cent-mille-huit',
  1000000: 'un-million',
  1000001: 'un-million-un',
  1000020: 'un-million-vingt',
  1000300: 'un-million-trois-cents',
  1004000: 'un-million-quatre-mille',
  2050000: 'deux-millions-cinquante-mille',
  3600000: 'trois-millions-six-cent-mille',
  11000000: 'onze-millions',
  172000000: 'cent-soixante-douze-millions',
  1000000000: 'un-milliard',
  1000000001: 'un-milliard-un',
  1000000015: 'un-milliard-quinze',
  1000000117: 'un-milliard-cent-dix-sept',
  2000000080: 'deux-milliards-quatre-vingts',
  2000003000: 'deux-milliards-trois-mille',
  3004000000: 'trois-milliards-quatre-millions',
  3004005000: 'trois-milliards-quatre-millions-cinq-mille',
  4050060007: 'quatre-milliards-cinquante-millions-soixante-mille-sept',
  51000234005: 'cinquante-et-un-milliards-deux-cent-trente-quatre-mille-cinq',
  600000000200: 'six-cents-milliards-deux-cents',
  998277831673: 'neuf-cent-quatre-vingt-dix-huit-milliards-deux-cent-soixante-dix-sept-millions-huit-cent-trente-et-un-mille-six-cent-soixante-treize'
}
const decimaux = {
  0.1: 'un dixième',
  0.01: 'un centième',
  0.001: 'un millième',
  0.0001: 'un dix-millième',
  0.00001: 'un cent-millième',
  0.000001: 'un millionième',
  0.0000001: 'un dix-millionième',
  0.00000001: 'un cent-millionième',
  0.000000001: 'un milliardième',
  0.0000000001: 'un dix-milliardième',
  0.00000000001: 'un cent-milliardième',
  1.1: 'un et un dixième',
  1.01: 'un et un centième',
  1.001: 'un et un millième',
  1.0001: 'un et un dix-millième',
  1.00001: 'un et un cent-millième',
  1.000001: 'un et un millionième',
  1.0000001: 'un et un dix-millionième',
  1.00000001: 'un et un cent-millionième',
  1.000000001: 'un et un milliardième',
  1.0000000001: 'un et un dix-milliardième',
  1.00000000001: 'un et un cent-milliardième',
  1.10000000001: 'un et dix-milliards-un cent-milliardièmes',
  200080.1000000001: 'deux-cent-mille-quatre-vingts et un-milliard-un dix-milliardièmes',
  // lui passe pas, js l’arrondi à 200080.1 avant d’appeler la fct
  // 200080.10000000001: 'deux-cents-mille-quatre-vingts et dix milliards un cent-milliardièmes',
  300000080.1000001: 'trois-cents-millions-quatre-vingts et un-million-un dix-millionièmes',
  0.2: 'deux dixièmes',
  0.11: 'onze centièmes',
  0.111: 'cent-onze millièmes',
  0.212: 'deux-cent-douze millièmes',
  0.1111: 'mille-cent-onze dix-millièmes',
  0.12345: 'douze-mille-trois-cent-quarante-cinq cent-millièmes',
  0.080481: 'quatre-vingt-mille-quatre-cent-quatre-vingt-un millionièmes',
  1.234567891: 'un et deux-cent-trente-quatre-millions-cinq-cent-soixante-sept-mille-huit-cent-quatre-vingt-onze milliardièmes',
  // on ne peut pas dépasser 16 caractères (longueur de Number.MAX_SAFE_INTEGER en 64 bits) en tout (sinon js tronque avant d’appeler la fct)
  298765432191.1001: 'deux-cent-quatre-vingt-dix-huit-milliards-sept-cent-soixante-cinq-millions-quatre-cent-trente-deux-mille-cent-quatre-vingt-onze et mille-un dix-millièmes',
  32171.51000234005: 'trente-deux-mille-cent-soixante-et-onze et cinquante-et-un-milliards-deux-cent-trente-quatre-mille-cinq cent-milliardièmes'
}

describe('Conversion de nombres en mots', function () {
  // pour le stub de la console
  let consoleErrorStub
  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })
  it('entierEnMot et nombreEnMots retournent le(s) mot(s) attendu(s) pour les entiers entre 0 et 999 999 999 999', () => {
    Object.entries(entiers).forEach(([snb, mot]) => {
      expect(entierEnMot(Number(snb))).to.equals(mot)
      expect(nombreEnMots(Number(snb))).to.equals(mot)
    })
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('entierEnMot retourne le mot attendu avec vingt et cent invariables pour les ordinaux (option ordinal), mais seulement à la fin (deux-cents-millions-trois-cent)', () => {
    const options = { ordinal: true }
    Object.entries(entiers).forEach(([snb, mot]) => {
      expect(entierEnMot(Number(snb), options)).to.equals(mot.replace(/(vingt|cent)s$/, '$1'))
    })
    // on en rajoute deux explicites au cas où la regex ci-dessus deviendrait foireuse
    expect(entierEnMot(200000300, options)).to.equals('deux-cents-millions-trois-cent')
    expect(entierEnMot(80000080, options)).to.equals('quatre-vingts-millions-quatre-vingt')
    expect(entierEnMot(280000080, options)).to.equals('deux-cent-quatre-vingts-millions-quatre-vingt')
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('nombreEnMots préfixe avec moins pour les entiers négatifs', () => {
    Object.entries(entiers).forEach(([snb, mot]) => {
      if (snb === '0') return expect(nombreEnMots(-0)).to.equals('zéro')
      expect(nombreEnMots(-1 * Number(snb))).to.equals('moins ' + mot)
    })
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('nombreEnMots ajoute "unités" si on le lui demande (pour les entiers)', () => {
    const options = { ajouterUnites: true }
    Object.entries(entiers).forEach(([snb, mot]) => {
      const nb = Number(snb)
      const result = nombreEnMots(nb, options)
      const resultNeg = nombreEnMots(-nb, options)
      if (nb === 0) {
        expect(result).to.equals('zéro unité')
        expect(resultNeg).to.equals('zéro unité')
      } else if (nb === 1) {
        expect(result).to.equals('une unité')
        expect(resultNeg).to.equals('moins une unité')
      } else {
        expect(result).to.equals(mot + ' unités')
        expect(resultNeg).to.equals('moins ' + mot + ' unités')
      }
    })
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('nombreEnMots convertit les décimaux (max 11 décimales, 16 chars en tout)', () => {
    Object.entries(decimaux).forEach(([snb, mots]) => {
      expect(nombreEnMots(Number(snb))).to.equals(mots)
    })
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('nombreEnMots tronque avant la conversion si on impose maxDecimales, et garde les 0 non significatifs si on le demande, avec ou sans ajouterUnites', () => {
    [
      [3.45678, 3, 'trois et quatre-cent-cinquante-sept millièmes', false],
      [3.45678, 3, 'trois et quatre-cent-cinquante-sept millièmes', true],
      [-4 / 3, 2, 'moins un et trente-trois centièmes', false],
      [-4 / 3, 2, 'moins un et trente-trois centièmes', true],
      [200 + 2 / 3, 3, 'deux-cents et six-cent-soixante-sept millièmes', false],
      [200 + 2 / 3, 3, 'deux-cents et six-cent-soixante-sept millièmes', true],
      [80.2, 3, 'quatre-vingts et deux dixièmes', false],
      [80.2, 2, 'quatre-vingts et vingt centièmes', true],
      [80.8, 2, 'quatre-vingts et quatre-vingts centièmes', true],
      [80.21, 2, 'quatre-vingts et vingt-et-un centièmes', true],
      [80.21, 2, 'quatre-vingts et vingt-et-un centièmes', false],
      [80.2, 3, 'quatre-vingts et deux-cents millièmes', true],
      [80.2, 6, 'quatre-vingts et deux-cent-mille millionièmes', true],
      [80.2, 9, 'quatre-vingts et deux-cents-millions milliardièmes', true]
    ].forEach(([nb, maxDecimales, mots, garderZerosNonSignificatifs]) => {
      expect(nombreEnMots(nb, { maxDecimales, garderZerosNonSignificatifs })).to.equals(mots)
      const expected = mots.replace(/ et/, ' unité' + (Math.abs(nb) >= 2 ? 's' : '') + ' et').replace('un unité', 'une unité')
      expect(nombreEnMots(nb, { maxDecimales, garderZerosNonSignificatifs, ajouterUnites: true })).to.equals(expected)
    })
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('nombreEnMots gère aussi les cas limites autour de zéro, ou sans partie décimale mais avec zéro non significatifs réclamés (zéro centièmes ou zéro unité et zéro centièmes)', () => {
    [
      [0, 3, false, false, 'zéro'],
      [0, 3, true, false, 'zéro millième'],
      [0, 3, true, true, 'zéro unité et zéro millième'],
      [200, 3, true, false, 'deux-cents et zéro millième'],
      [200, 3, true, true, 'deux-cents unités et zéro millième'],
      [-998277831673, 3, true, true, 'moins neuf-cent-quatre-vingt-dix-huit-milliards-deux-cent-soixante-dix-sept-millions-huit-cent-trente-et-un-mille-six-cent-soixante-treize unités et zéro millième'],
      [998277831673, 3, true, true, 'neuf-cent-quatre-vingt-dix-huit-milliards-deux-cent-soixante-dix-sept-millions-huit-cent-trente-et-un-mille-six-cent-soixante-treize unités et zéro millième'],
      [998277831673, 3, true, false, 'neuf-cent-quatre-vingt-dix-huit-milliards-deux-cent-soixante-dix-sept-millions-huit-cent-trente-et-un-mille-six-cent-soixante-treize et zéro millième']
    ].forEach(([nb, maxDecimales, garderZerosNonSignificatifs, ajouterUnites, mots]) => {
      expect(nombreEnMots(nb, { maxDecimales, garderZerosNonSignificatifs, ajouterUnites })).to.equals(mots)
    })
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('plante (ou râle en console pour maxDecimales) si on lui passe n’importe quoi', () => {
    // les plantages qui doivent arriver
    [Number.POSITIVE_INFINITY, 'portnawak', '', null, undefined, true, false, {}].forEach(wrongNb => {
      expect(() => entierEnMot(wrongNb)).to.throw(TypeError, 'entierEnMot ne peut convertir que des entiers')
      expect(() => nombreEnMots(wrongNb)).to.throw(TypeError, 'nombre invalide')
    })
    expect(() => entierEnMot(2.3)).to.throw(TypeError, /entierEnMot ne peut convertir que des entiers/)
    expect(() => entierEnMot(-3)).to.throw(RangeError, /entierEnMot ne peut convertir que des entiers entre/)
    expect(() => entierEnMot(Number.MAX_SAFE_INTEGER)).to.throw(RangeError, /entierEnMot ne peut convertir que des entiers entre/)
    expect(() => nombreEnMots(Number.MAX_SAFE_INTEGER)).to.throw(RangeError, /nombreEnMots ne peut convertir que des nombres inférieurs/)
    expect(consoleErrorStub).to.not.have.been.called

    // et les maxDecimales foireux qui râlent en console
    ;[Number.POSITIVE_INFINITY, 'portnawak', '', null, true, false, {}, -2, 12, 3.1].forEach((maxDecimales, i) => {
      nombreEnMots(1, { maxDecimales })
      expect(consoleErrorStub).to.have.been.calledOnce
      consoleErrorStub.resetHistory()
    })
  })
})
