import { describe, it, expect } from 'vitest'

import Grid, { gridEltX, gridEltY, type BlocPx } from 'src/lib/editor/Grid'

type Rect = {x: number, y: number, w: number, h: number}

// pour convertir du mask --xxx--- en bigint
// const toBigInt = (strMask: string): bigint => {
//   let n = 0n
//   let i = 0
//   while (i < strMask.length) {
//     const c = strMask[i]
//     if (c === '1') n += BigInt(1 << i)
//     i++
//   }
//   return n
// }

const coordToPx = (values: Rect): BlocPx => {
  const { x, y, w, h } = values
  return { x: x * gridEltX, y: y * gridEltY, width: w * gridEltX, height: h * gridEltY }
}
const strToPx = (strMask: string, x: number, y: number): BlocPx => {
  const lines = strMask.trim().split('\n')
  return coordToPx({ x, y, w: lines[0]?.length ?? 0, h: lines.length })
}
// converti en BlocPx une string au format grille (il faut passer un carré de 1 sur la grille, sans 0 à droite et en bas)
const gridToPx = (strGrid: string): BlocPx => {
  const lines = strGrid.trim().split('\n')
  const firstLineIndex = lines.findIndex((line) => line.includes('1'))
  const firstLine = lines[firstLineIndex]
  if (firstLine == null) throw Error('grille invalide (elle doit contenir au moins un 1')
  const firstColIndex = firstLine.indexOf('1')
  if (firstColIndex < 0) throw Error('erreur interne')
  const y = firstLineIndex
  const x = firstColIndex
  const h = lines.length - firstLineIndex
  const w = firstLine.length - firstColIndex
  return coordToPx({ x, y, w, h })
}

type RoundFn =(x: number) => number
const roundX:RoundFn = x => Math.floor(x / gridEltX) * gridEltX
const roundY:RoundFn = y => Math.floor(y / gridEltY) * gridEltY

const bloc1 = strToPx('xxx\nxxx', 1.2, 2.3)
const bloc1Decal = strToPx('xxx\nxxx', 2.2, 3.3)
const bloc2 = strToPx('xx\nxx\nxx', 2, 7)
const bloc3 = strToPx('xx\nxx', 4, 7)

const expectEmpty = (grid: unknown, nbLig?:number) => {
  const str = grid instanceof Grid ? grid.toString(true) : String(grid)
  const lines = str.split('\n')
  // la dernière ligne du split doit être vide
  expect(lines.pop()).to.equals('')
  expect(lines.every(l => l === '0')).to.be.true
  if (nbLig) expect(lines).to.have.lengthOf(nbLig)
}

describe('editor/Grid', () => {
  it('est vide au départ', () => {
    const g = new Grid()
    expect(g.toString(true)).to.equals('0\n')
  })
  describe('clear', () => {
    it('vide la grille, même vierge', () => {
      const g = new Grid()
      g.clear()
      expectEmpty(g)
    })
    it('en laissant les lignes créées avant (vides)', () => {
      const g = new Grid()
      g.occupy(bloc1)
      g.clear()
      expect(g.toString(true)).to.equals('0\n'.repeat(5))
    })
  })
  describe('occupy', () => {
    const g = new Grid()
    it('ajoute correctement sur la grille', () => {
      g.occupy(bloc1)
      // => ça doit donner 4×3 après 2 lignes vides et une colonne vide
      expect(g.toString(true)).to.equals('0\n0\n01111\n01111\n01111\n')
      g.occupy(bloc2)
      // on doit tomber pile, un rect de 2×3 qui démarre à gauche à partir de la 8e ligne
      // expect(g.toString(true)).to.equals('0\n0\n011111\n011111\n011111\n011111\n0\n0011\n0011\n0011\n')
      expect(g.toString(true)).to.equals('0\n0\n01111\n01111\n01111\n0\n0\n0011\n0011\n0011\n')
    })
    it('même si ça se chevauche', () => {
      g.occupy(bloc1Decal)
      expect(g.toString(true)).to.equals('0\n0\n01111\n011111\n011111\n001111\n0\n0011\n0011\n0011\n')
    })
  })
  describe('free', () => {
    it('libère la grille', () => {
      const g = new Grid()
      g.occupy(bloc1)
      g.free(bloc1)
      expect(g.toString(true)).to.equals('0\n'.repeat(5))
    })
    it('même déjà vide', () => {
      const g = new Grid()
      g.free(bloc1)
      expect(g.toString(true)).to.equals('0\n'.repeat(5))
    })
  })

  it('enchaînement occupy / free', () => {
    const g = new Grid()
    g.occupy(bloc1)
    g.occupy(bloc1Decal)
    g.free(bloc1)
    expect(g.toString(true)).to.equals('0\n0\n0\n000001\n000001\n001111\n')
    g.occupy(bloc1)
    expect(g.toString(true)).to.equals('0\n0\n01111\n011111\n011111\n001111\n')
  })

  it('hasCollision détecte les collisions', () => {
    const g = new Grid()
    g.occupy(bloc1)
    expect(g.hasCollision(bloc1Decal)).to.be.true
    expect(g.hasCollision(bloc2)).to.be.false
    expect(g.hasCollision(bloc3)).to.be.false
    g.occupy(bloc2)
    expect(g.hasCollision(bloc2)).to.be.true
    expect(g.hasCollision(bloc3)).to.be.false
  })

  describe('firstFreePosition trouve la bonne place', () => {
    it('retourne la position demandée arrondie si c’est libre', () => {
      const g = new Grid()
      const { x, y } = bloc1
      const result = g.firstFreePosition(bloc1)
      expect(result.x).to.equal(roundX(x))
      expect(result.y).to.equal(roundY(y))
    })
    it('décale vers le bas si y’a moins de chemin que vers la droite', () => {
      const blocPos = gridToPx('0000\n0111\n0111')
      // le même décalé d'un cran vers le bas et à droite
      const blocWanted = gridToPx('0\n0\n0011\n0011')
      const g = new Grid()
      g.occupy(blocPos)
      const result = g.firstFreePosition(blocWanted)
      // => faudrait décaler de 2 à droite ou 1 en bas => ça doit décaler y de l'offset avec x identique (arrondi)
      expect(result.x).to.equal(roundX(blocWanted.x))
      expect(result.y).to.equal(roundY(blocPos.y + blocPos.height + 1))
    })
    it('décale vers la droite si y’a moins de chemin que vers le bas', () => {
      const g = new Grid()
      // on ajoute un bloc plus haut que large
      const blocPos = gridToPx('0\n011\n011\n011')
      g.occupy(blocPos)
      const blocWanted = gridToPx('0\n0\n0011\n0011')
      const result = g.firstFreePosition(blocWanted)
      // => ça doit le décaler d'un cran vers la droite
      expect(result.x).to.equal(roundX(blocWanted.x + gridEltX + 1))
      expect(result.y).to.equal(roundY(blocWanted.y + 1))
    })
    it('trouve un trou dans la grille', () => {
      /* avec une grille
1111000
1111000
1100111
1100111
  il doit caser un carré de 2×2 en x:3, y:2 (pour remplir les 0) si on le demande en 0,0 ou 3,1
  et en 5,0 si on le demande avec y=0 et x > 2
      */
      const g = new Grid()
      g.occupy(gridToPx('1111\n1111'))
      g.occupy(gridToPx('0\n0\n11\n11'))
      g.occupy(gridToPx('0\n0\n0000111\n0000111'))
      let result = g.firstFreePosition(gridToPx('11\n11'))
      expect(result.x).to.equal(2 * gridEltX)
      expect(result.y).to.equal(2 * gridEltY)
      // au même endroit si on le demande sur la 2e ligne
      result = g.firstFreePosition(gridToPx('0\n11\n11'))
      expect(result.x).to.equal(2 * gridEltX)
      expect(result.y).to.equal(2 * gridEltY)
      // en haut à droite si on le veut avec y=0 et x=3
      result = g.firstFreePosition(gridToPx('00011\n00011'))
      // console.log('res', result)
      expect(result.x).to.equal(4 * gridEltX)
      expect(result.y).to.equal(0)
      // et en bas sur le x demandé si on le veut trop loin pour rentrer dans le trou
      result = g.firstFreePosition(gridToPx('0\n0\n00011\n00011'))
      expect(result.x).to.equal(3 * gridEltX)
      expect(result.y).to.equal(4 * gridEltY)
      result = g.firstFreePosition(gridToPx('0\n0\n\n11\n11'))
      expect(result.x).to.equal(0)
      expect(result.y).to.equal(4 * gridEltY)
    })
  })
})
