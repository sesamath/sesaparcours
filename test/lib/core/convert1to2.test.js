import { afterAll, afterEach, beforeAll, describe, it } from 'vitest'
import { expect, use } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

import { convertGraph } from 'src/lib/core/convert1to2'
import GraphNode from 'src/lib/entities/GraphNode'

use(sinonChai)

const exemple1 = [
  [1, 'Exemple1', [{ nn: 2, score: '>=0.6' }]],

  ['2', 'Exemple2', [{ nn: '3', score: 'sans condition' }]],
  [3, 'fin']
]

const checkGraph = (graph, realNodesExpected, endNodesExpected) => {
  const nodesIds = Array.from(Object.keys(graph.nodes))
  const allNodesExpected = realNodesExpected.concat(endNodesExpected)
  // le même nb de nodes
  expect(graph.getLength()).to.equals(realNodesExpected.length)
  expect(Object.entries(graph.nodes).length).to.equals(allNodesExpected.length)
  // ceux voulus
  expect(nodesIds.every(nodeId => allNodesExpected.includes(nodeId)))
  // tous des GraphNode
  // la ligne suivante marche pas (cf https://github.com/chaijs/type-detect)
  // expect(graph.nodes['1']).to.be.a('GraphNode')
  // on utilise instanceof
  for (const nodeId of allNodesExpected) {
    const node = graph.getNode(nodeId)
    if (node == null) throw Error(`Le node ${nodeId} n’existe pas`)
    expect(graph.getNode(nodeId) instanceof GraphNode).to.be.true
  }
  // y’a tous les nœuds fin voulus
  for (const id of endNodesExpected) {
    expect(graph.getNode(id).section).to.equals('', 'Nœud fin avec une section')
  }
  // et les autres
  for (const id of realNodesExpected) {
    const node = graph.getNode(id)
    expect(node.section).not.to.equals('', 'Nœud sans section')
    // avec un titre
    expect(node.label).to.be.a('string', 'nœud sans titre')
    expect(node.label).to.not.have.lengthOf(0, 'nœud avec titre vide')
    // et une position
    expect(node.position.x).to.be.a('number', 'nœud sans position x')
    expect(node.position.y).to.be.a('number', 'nœud sans position y')
  }
  // le graph a un startingId qui existe
  expect(realNodesExpected.includes(graph.startingId)).to.be.true
}

describe('convert1to2', () => {
  let consoleWarnStub, consoleErrorStub

  beforeAll(() => {
    consoleWarnStub = sinon.stub(console, 'warn')
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleWarnStub.resetHistory()
    consoleErrorStub.resetHistory()
  })
  afterAll(() => {
    consoleWarnStub.restore()
    consoleErrorStub.restore()
  })

  it('Converti un graphe à un nœud sans nœud fin ni paramètres', () => {
    const grapheV1 = [
      [1, 'Exemple1', [{ nn: 'fin', score: 'sans+condition' }]]
    ]
    const graph = convertGraph(grapheV1)
    checkGraph(graph, ['1'], ['fin'])
    const node1 = graph.getNode('1')
    expect(node1.section).to.equals('Exemple1')
    expect(node1.connectors).to.have.lengthOf(1)
    expect(node1.connectors[0]?.typeCondition).to.equals('none')
    expect(node1.connectors[0]?.target).to.equals('fin')
    expect(consoleWarnStub).to.not.have.been.called
    expect(consoleErrorStub).to.not.have.been.called
  })

  it('Converti un graphe à un nœud avec nœud fin (sans paramètres, mais avec branchement vers fin)', () => {
    const grapheV1 = [
      [1, 'Exemple1', [{ nn: 2, score: 'sans+condition' }]],
      [2, 'fin']
    ]
    const graph = convertGraph(grapheV1)
    checkGraph(graph, ['1'], ['2'])
    const node1 = graph.getNode('1')
    expect(node1.section).to.equals('Exemple1')
    expect(node1.connectors).to.have.lengthOf(1)
    expect(node1.connectors[0]?.typeCondition).to.equals('none')
    expect(node1.connectors[0]?.target).to.equals('2')
    const node2 = graph.getNode('2')
    expect(node2.section).to.equals('')
    expect(node2.connectors).to.have.lengthOf(0)
    expect(consoleWarnStub).to.not.have.been.called
    expect(consoleErrorStub).to.not.have.been.called
  })

  it('converti un graphe avec condition sur le score (et ajoute un connecteur si le dernier n’est pas sans condition', () => {
    const graph = convertGraph(exemple1)
    checkGraph(graph, ['1', '2'], ['3'])
    const node1 = graph.getNode('1')
    expect(node1.section).to.equals('Exemple1')
    expect(node1.connectors).to.have.lengthOf(2)
    expect(node1.connectors[0]?.typeCondition).to.equals('score')
    expect(node1.connectors[0]?.target).to.equals('2')
    // le connecteur sans condition ajouté
    expect(node1.connectors[1]?.typeCondition).to.equals('none')
    expect(node1.connectors[1]?.target).to.equals('3')
    const node2 = graph.getNode('2')
    expect(node2.section).to.equals('Exemple2')
    expect(node2.connectors).to.have.lengthOf(1)
    expect(node2.connectors[0]?.typeCondition).to.equals('none')
    expect(node2.connectors[0]?.target).to.equals('3')
    expect(consoleWarnStub).to.have.been.calledOnce
    expect(consoleErrorStub).to.not.have.been.called
  })

  it('Ajoute les positions et titres manquants', () => {
    const grapheV1 = [
      [1, 'Exemple1', [{ nn: 2, score: '>=0.6' }]],
      [2, 'Exemple2', [{ nn: '3', score: 'sans condition' }]],
      ['3', 'Exemple2', [{ nn: '4', score: 'sans condition' }]],
      ['4', 'fin']
    ]
    const titreNodes = ['nœud1', null, 'Nœud2']
    const graph = convertGraph(grapheV1, { titreNodes, positionNodes: [null, [5, 5]] })
    checkGraph(graph, ['1', '2', '3'], ['4'])
    const newTitres = []
    for (const node of Object.values(graph.nodes)) {
      newTitres.push(node.label)
      expect
    }
    expect(newTitres.length).to.equals(4, 'pas le bon nb de titres')
    for (const titre of newTitres) {
      expect(typeof titre).to.equals('string')
      expect(titre).to.not.have.lengthOf(0)
    }
    expect(consoleWarnStub).to.have.been.calledOnce
    expect(consoleErrorStub).to.not.have.been.called
  })
})
