import { describe, it, expect } from 'vitest'
import { checkRessource } from 'src/lib/core/checks'
import ressource from 'test/fixtures/ressources/5c5d7085830f3437fdd1119f.json'

/**
 *
 * @param {LegacyGraph} nodes
 * @param {EditGrapheOptsV1} editgraphe
 * @returns {{parametres: {g, editgraphes: {positionNodes: PositionNodesV1, titreNodes: string[]}}, type: string}}
 */
function getRessource (nodes, { titreNodes, positionNodes } = {}) {
  let i = 0
  return {
    type: 'j3p',
    parametres: {
      g: nodes,
      editgraphes: {
        titreNodes: titreNodes ?? nodes.map(([id]) => `Nœud ${id}`),
        positionNodes: positionNodes ?? nodes.map(() => [i++ * 10, i * 10])
      }
    }
  }
}

async function check (nodes, warningMatchers, errorsMatchers) {
  const ressource = getRessource(nodes)
  const { ok, warnings, errors } = await checkRessource(ressource)
  expect(warnings).to.have.lengthOf(warningMatchers.length)
  for (const [i, matcher] of warningMatchers.entries()) {
    expect(warnings[i]).to.match(matcher)
  }
  expect(errors).to.have.lengthOf(errorsMatchers.length)
  for (const [i, matcher] of errorsMatchers.entries()) {
    expect(errors[i]).to.match(matcher)
  }
  // lui en dernier car on préfère voir les pbs plus parlants avant
  expect(ok).to.equals(errorsMatchers.length === 0)
}

describe('checkRessource', () => {
  it('ne signale pas de pb sur une ressource intègre', async () => {
    const { ok, warnings, errors } = await checkRessource(ressource)
    expect(ok).to.be.true
    expect(warnings).to.have.lengthOf(0, `On a les warnings :\n${warnings.join('\n')}`)
    expect(errors).to.have.lengthOf(0, `On a les erreurs :\n${errors.join('\n')}`)
  })

  it('sort un warning pour un branchement vers un nœud inexistant', async () => {
    const nodes = [['1', 'choix', [{ nn: '3', conclusion: 'blabla', score: 'sans condition' }, {}]]]
    return await check(nodes, [], [/Le node .+ n’existe pas dans ce graphe/])
  })
  it('sort une erreur pour une section inexistante', async () => {
    const nodes = [['1', 'foo', [{ nn: '2', conclusion: 'blabla', score: 'sans condition' }, {}]], ['2', 'fin']]
    return await check(nodes, [], [/La section foo n’existe pas/])
  })
  it('sort une erreur pour un branchement avec une condition invalide', async () => {
    const nodes = [['1', 'choix', [{ nn: '2', conclusion: 'blabla', score: '>0.5' }, {}]], ['2', 'fin']]
    return await check(nodes, [], [/condition de score/])
  })
  it('sort une erreur pour un branchement sans condition score ni pe', async () => {
    const nodes = [['1', 'choix', [{ nn: '2', conclusion: 'blabla' }, {}]], ['2', 'fin']]
    return await check(nodes, [], [/pas de condition \(ni pe ni score\)/])
  })
  it('sort une erreur pour un node sans branchement', async () => {
    const nodes = [['1', 'choix', [{}]], ['2', 'fin']]
    return await check(nodes, [], [/pas de branchement/])
  })
  it('sort une erreur pour un branchement avec condition sur une pe qui n’existe pas dans la section', async () => {
    const branchement = { nn: '2', conclusion: 'foo', pe: 'pe_4' }
    const nodes = [['1', 'choix', [branchement]], ['2', 'fin']]
    // on vérifie que ça passe avec un branchement ok
    return await check(nodes, [], [])
      .then(async () => {
        // puis plante avec une pe foireuse
        branchement.pe = 'pe_9'
        return await check(nodes, [], [/branchement avec la pe .+ mais elle n’existe pas dans la section/])
      })
  })
})
