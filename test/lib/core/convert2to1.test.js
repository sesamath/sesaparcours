// import fs from 'node:fs'
import { describe, it, beforeAll, afterEach, afterAll } from 'vitest'
import { expect, use } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

import { areSameObject } from 'src/lib/utils/comparators'
import { stringify } from 'src/lib/utils/object'
import { convertGraph } from 'src/lib/core/convert1to2'
import { convertGraphBack } from 'src/lib/core/convert2To1'
import GraphV1 from 'src/lib/entities/GraphV1'
import Graph from 'src/lib/entities/Graph'

import ressource from 'test/fixtures/ressources/5c5d7085830f3437fdd1119f.json'

use(sinonChai)
/*
const checkGraph = (graph, realNodesExpected, endNodesExpected) => {
  const nodesIds = Array.from(graph.nodes.keys())
  const allNodesExpected = realNodesExpected.concat(endNodesExpected)
  // le même nb de nodes
  expect(graph.getLength()).to.equals(realNodesExpected.length)
  expect(graph.nodes.size).to.equals(allNodesExpected.length)
  expect(nodesIds.every(nodeId => allNodesExpected.includes(nodeId)))
  // la ligne suivante marche pas (cf https://github.com/chaijs/type-detect)
  // expect(graph.nodes['1']).to.be.a('GraphNode')
  // on utilise instanceof
  allNodesExpected.forEach(nodeId => {
    const node = graph.nodes.get(nodeId)
    if (node == null) throw Error(`Le node ${nodeId} n’existe pas`)
    expect(graph.nodes.get(nodeId) instanceof GraphNode).to.be.true
  })
  endNodesExpected.forEach(id => expect(graph.nodes.get(id).section).to.equals(''))
  realNodesExpected.forEach(id => expect(graph.nodes.get(id).section).not.to.equals(''))
  expect(graph.startingId).to.equals(realNodesExpected[0])
}
*/

// un graph de test
const getGraph = () => {
  const graph = new Graph()
  graph.addNode({ id: '1', section: 'Exemple1', connectors: [{ target: '2', typeCondition: 'none' }] })
  graph.addNode({ id: '2', section: 'Exemple2', connectors: [{ target: '1', scoreComparator: '<', scoreRef: 0.5 }, { target: '3', typeCondition: 'none' }] })
  graph.addNode({ id: '3', section: '' })
  graph.setStartingId('1')
  graph.complete()
  return graph
}

describe('convert2to1', () => {
  let consoleWarnStub, consoleErrorStub

  beforeAll(() => {
    consoleWarnStub = sinon.stub(console, 'warn')
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleWarnStub.resetHistory()
    consoleErrorStub.resetHistory()
  })
  afterAll(() => {
    consoleWarnStub.restore()
    consoleErrorStub.restore()
  })

  it('récupère le bon nb de nœuds', async () => {
    const { g } = await convertGraphBack(getGraph())
    expect(Array.isArray(g)).to.be.true
    expect(g).to.have.lengthOf(3)
    const sections = ['Exemple1', 'Exemple2', 'fin']
    for (const [i, node] of g.entries()) {
      expect(Array.isArray(node)).to.be.true
      expect(node).to.have.lengthOf(i === 2 ? 2 : 3)
      const [id, section, opts] = node
      expect(id).to.equals(String(i + 1))
      expect(section).to.equals(sections[i])
      if (i < 2) {
        const branchements = [...opts]
        const params = branchements.pop()
        expect(branchements).to.have.lengthOf(i === 2 ? 0 : i + 1)
        expect(params).to.be.a('object')
        expect(Object.keys(params)).to.have.lengthOf(0)
      }
    }
    expect(consoleWarnStub).to.not.have.been.called
    expect(consoleErrorStub).to.not.have.been.called
  })

  it('Récupère le  graphe d’origine après une conversion en v2 (aux branchements sans conditions ajoutés en dernier près, avec warnings en console)', async () => {
    const { parametres: { g, editgraphes } } = ressource
    const graph = convertGraph(g, editgraphes)
    const nbNodeBefore = g.reduce((nb, node) => {
      if (node[1].toLowerCase() === 'fin') return nb
      return nb + 1
    }, 0)
    expect(graph.getLength()).to.equals(nbNodeBefore, 'Le graphe v2 n’a pas le même nombre de nœuds non fin')
    const { g: gBack, editgraphes: { positionNodes: positionNodesBack, titreNodes: titreNodesBack } } = await convertGraphBack(graph)
    const { positionNodes, titreNodes } = editgraphes
    expect(titreNodesBack).to.have.lengthOf(g.length, 'Le graphe transformé n’a pas le bon nombre de titres')
    expect(positionNodesBack).to.have.lengthOf(g.length, 'Le graphe transformé n’a pas le bon nombre de positions')
    // on normalise via le constructeur GraphV1
    const g1 = new GraphV1({ graphe: g, positionNodes, titreNodes })
    const g2 = new GraphV1({ graphe: gBack, positionNodes: positionNodesBack, titreNodes: titreNodesBack })
    // et on compare
    const { warnings, errors } = g1.isSame(g2, { ignoreSnn: false, ignoreLastNone: true })
    if (warnings.length > 0) {
      console.warn(`Il y a des warnings : \n${warnings.join('\n')}`)
    }
    // DEBUG pour comparer la conversion
    // fs.writeFileSync('gOrig.json', stringify(g1, 2))
    // fs.writeFileSync('gConv.json', stringify(g2, 2))
    // fs.writeFileSync('gConv2.json', stringify(graph.toJSON(), 2))
    if (errors.length > 0) {
      console.error('On a des erreurs entre ce graphe d’origine', stringify(g1, 2), '\n\net celui après transformation 1 => 2 => 1', stringify(g2, 2), '\n\navec le graphe v2 intermédiaire', stringify(graph.toJSON(), 2))
      throw Error(errors.join('\n'))
    }
    expect(consoleWarnStub).to.have.callCount(6)
    expect(consoleErrorStub).to.not.have.been.called
  })

  it('Après une première conversion v1=>v2 (qui ajoute éventuellement des branchements sans condition en dernier), les conversions suivantes donnent des objets strictement identiques', async () => {
    // on converti en v2 une première fois
    const { parametres: { g, editgraphes } } = ressource
    const graphA = convertGraph(g, editgraphes)
    // on oublie les warnings
    consoleWarnStub.resetHistory()
    const { g: grapheB, editgraphes: { positionNodes, titreNodes } } = await convertGraphBack(graphA)
    const graphB = convertGraph(grapheB, { positionNodes, titreNodes })
    // c’est la sérialisation json qui doit être identique (les ids sont générés à chaque fois)
    expect(areSameObject(graphA.toJSON(), graphB.toJSON())).equals(true, 'la 2e conversion vers v2 ne donne pas le même résultat')
    const { g: grapheC } = await convertGraphBack(graphB)
    expect(areSameObject(grapheB, grapheC)).equals(true, 'la 2e conversion vers v1 ne donne pas le même résultat')
    expect(consoleWarnStub).to.not.have.been.called
    expect(consoleErrorStub).to.not.have.been.called
  })
})
