import { describe, it } from 'vitest'
import { checkSectionExports } from 'src/lib/core/checks'
import { getSectionsList } from 'src/lib/core/loadSection'

/* @vitest-environment happy-dom */

const sections = getSectionsList()
// const sections = ['Exemple']

describe(`Les ${sections.length} sections ont des exports corrects`, () => {
  for (const section of sections) {
    it(`section ${section} a des paramètres corrects`, async () => {
      const { warnings, errors } = await checkSectionExports(section)
      for (const warning of warnings) {
        console.warn(`warning sur la section ${section} : ${warning}`)
      }
      if (errors.length) {
        for (const error of errors) console.error(error)
        throw Error(`Erreurs sur la section ${section} : ` + errors.map(e => e.message ?? e).join('\n'))
      }
    })
  }
})
