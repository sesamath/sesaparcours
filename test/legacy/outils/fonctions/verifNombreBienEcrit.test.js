import { describe, it } from 'vitest'
import { expect } from 'chai'

import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'

function check (nbStr, errorExpected, opts) {
  const { erreur } = verifNombreBienEcrit(nbStr, opts)
  expect(erreur).to.equals(errorExpected, `Pb avec "${nbStr}"`)
}

describe('verifNombreBienEcrit', () => {
  it('plante sur autre chose qu’une string', () => {
    for (const weakNb of [true, false, undefined, null, {}, new Date()]) {
      expect(verifNombreBienEcrit.bind(null, weakNb)).to.throw(/nombre invalide/)
    }
  })
  it('plante sur une écriture scientifique', () => {
    for (const nb of ['1e42', '1e+42', '4e-42']) {
      expect(verifNombreBienEcrit.bind(null, nb)).to.throw(/hors du champ d’analyse/, `pb avec ${nb}`)
    }
  })
  it('signale espaceDebut (sauf laxStartingSpace ou sans checkSpace)', () => {
    for (const nb of [' 123', ' +42', ' -42', '- 42', '+ 42', ' + 42', ' 123']) {
      check(nb, 'espaceDebut')
      check(nb, '', { checkSpace: false })
      check(nb, '', { laxStartingSpace: true })
    }
  })
  it('signale pbSigne', () => {
    for (const nb of ['+-42', '-+42', '42+', '42-', '-42-', '+42+', '123 45-', '-123,+4', '4-2']) {
      check(nb, 'pbSigne')
    }
  })
  it('signale espaceDouble (sauf sans checkSpace)', () => {
    for (const nb of ['1  000', '12  3', '+123  456', '-12,4  2']) {
      check(nb, 'espaceDouble')
      check(nb, '', { checkSpace: false })
    }
  })
  it('signale espaceFin (sauf laxEndingSpace ou sans checkSpace)', () => {
    for (const nb of ['123 ', '+42 ', '-123 ', '123,456 ', '123, ']) {
      check(nb, 'espaceFin')
      if (/, +$/.test(nb)) {
        // ça doit signaler virguleInutile
        check(nb, 'virguleInutile', { laxEndingSpace: true })
        check(nb, 'virguleInutile', { checkSpace: false })
      } else {
        check(nb, '', { laxEndingSpace: true })
        check(nb, '', { checkSpace: false })
      }
    }
  })
  it('signale "intrus"', () => {
    for (const nb of ['12a3', '+12,34abc', '-3@4,56']) {
      check(nb, 'intrus')
    }
  })
  it('signale "virguleMultiple"', () => {
    for (const nb of ['1,23,4', '+12,,34', '123,456,']) {
      check(nb, 'virguleMultiple')
    }
  })
  it('signale "virguleDebut"', () => {
    for (const nb of [',123', '+,42', '-,123']) {
      check(nb, 'virguleDebut')
    }
  })
  it('signale virguleInutile (sauf ignoreVirguleInutile)', () => {
    for (const nb of ['123,', '-42,', '+12,']) {
      check(nb, 'virguleInutile')
      check(nb, '', { ignoreVirguleInutile: true })
    }
  })
  it('signale zeroInutile (sauf sans checkUselessZero)', () => {
    for (const nb of ['0123', '00,012', '+00,123', '-123,00']) {
      check(nb, 'zeroInutile')
      if (/,0+$/.test(nb)) {
        // ça change d'erreur
        check(nb, 'virguleInutile', { checkUselessZero: false })
        // sauf si on l'ignore aussi
        check(nb, '', { checkUselessZero: false, ignoreVirguleInutile: true })
      } else {
        check(nb, '', { checkUselessZero: false })
      }
    }
  })
  it('signale espaceFinEnt (sauf sans checkSpace)', () => {
    for (const nb of ['123 ,4', '+123 ,4', '-42 ,567']) {
      check(nb, 'espaceFinEnt')
      check(nb, '', { checkSpace: false })
    }
  })
  it('signale espaceDebDec (sauf sans checkSpace)', () => {
    for (const nb of ['123, 456', '+0, 001', '-42, 1']) {
      check(nb, 'espaceDebDec')
      check(nb, '', { checkSpace: false })
    }
  })
  it('signale espaceEnt (sauf sans checkSpace)', () => {
    for (const nb of ['12 34', '1 2345', '+1 23 456']) {
      check(nb, 'espaceEnt')
      check(nb, '', { checkSpace: false })
    }
  })
  it('signale espaceDec (sauf sans checkSpace)', () => {
    for (const nb of ['123,45 678', '+12,3 456', '-0,12 3 456']) {
      check(nb, 'espaceDec')
      check(nb, '', { checkSpace: false })
    }
  })
  // Cas valides
  it('ne signale pas d’erreur sur des nombres bien écrits', () => {
    for (const nb of ['123', '+123', '-123', '0', '0,1', '0,123 4', '123,45', '-123,456', '1 234', '1 234,567', '+1 234,567 89', '-1 234,567 89']) {
      check(nb, '')
      check(nb, '', { checkSpace: false })
      check(nb, '', { checkUselessZero: false })
      check(nb, '', { ignoreVirguleInutile: true })
      check(nb, '', { laxStartingSpace: true })
      check(nb, '', { laxEndingSpace: true })
    }
  })
})
