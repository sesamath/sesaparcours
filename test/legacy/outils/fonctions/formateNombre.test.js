import { afterEach, beforeEach, describe, it } from 'vitest'
import { expect, use } from 'chai'

import sinon from 'sinon'
import sinonChai from 'sinon-chai'

import { formateNombre } from 'src/legacy/outils/fonctions/ecritureNombre'

use(sinonChai)

describe('formateNombre', () => {
  // pour le stub de la console
  let consoleErrorStub
  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  it('râle en console et retourne une chaîne vide pour les nombres non finis', () => {
    for (const nb of [null, undefined, true, new Date(), {}, [], NaN, Infinity, -Infinity]) {
      expect(formateNombre(nb)).to.equals('')
      expect(consoleErrorStub).to.have.been.calledOnce
      consoleErrorStub.resetHistory()
    }
  })
  it('râle sur les nombres trop grand ou trop petit et les retourne tel quel en écriture scientifique', () => {
    expect(formateNombre(1e42)).to.equals('1e+42')
    expect(consoleErrorStub).to.have.been.calledOnce
    consoleErrorStub.resetHistory()
    expect(formateNombre(1e-42)).to.equals('1e-42')
    expect(consoleErrorStub).to.have.been.calledOnce
  })
  it('formate le nombre à l’identique pour les entiers simples', () => {
    expect(formateNombre(0)).to.equals('0')
    expect(formateNombre(42)).to.equals('42')
    expect(formateNombre(-42)).to.equals('-42')
    expect(formateNombre(123)).to.equals('123')
    expect(formateNombre(0)).to.equals('0')
    expect(formateNombre(-0)).to.equals('0') // JavaScript retourne "0" dans les deux cas
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('formate correctement les entiers', () => {
    expect(formateNombre(1000)).to.equals('1 000')
    expect(formateNombre(123456)).to.equals('123 456')
    expect(formateNombre(123456789)).to.equals('123 456 789')
    expect(formateNombre(-1)).to.equals('-1')
    expect(formateNombre(-1234)).to.equals('-1 234')
    expect(formateNombre(-1234567)).to.equals('-1 234 567')
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('formate correctement les nombres décimaux', () => {
    expect(formateNombre(0.123)).to.equals('0,123')
    expect(formateNombre(42.56)).to.equals('42,56')
    expect(formateNombre(1234.56789)).to.equals('1 234,567 89')
    expect(formateNombre(-1234.56789)).to.equals('-1 234,567 89')
    expect(formateNombre(0.1234567)).to.equals('0,123 456 7')
    expect(formateNombre(12345.6789012)).to.equals('12 345,678 901 2')
    expect(formateNombre(123456789.98765)).to.equals('123 456 789,987 65')
    expect(formateNombre(-987654321.12345)).to.equals('-987 654 321,123 45')
    expect(formateNombre(0.000001)).to.equals('0,000 001')
    expect(formateNombre(-0.000001)).to.equals('-0,000 001')
    expect(formateNombre(1000000000000)).to.equals('1 000 000 000 000')
    expect(formateNombre(-1000000000000.1234)).to.equals('-1 000 000 000 000,123 4')
  })
})
