import { describe, it, beforeAll, beforeEach, afterAll } from 'vitest'
import { expect, use } from 'chai'
import sinonChai from 'sinon-chai'
import sinon from 'sinon'

import { j3pNombre } from 'src/legacy/core/functions'
import { stringify } from 'src/lib/utils/object'

use(sinonChai)

const testsPoint = [
  ['0.2', 0.2],
  ['-1.34', -1.34],
  ['3.141516171819', 3.141516171819],
  ['-123456789.01234', -123456789.01234]
]
const testsVirgule = [
  ['0,2', 0.2],
  ['-1,34', -1.34],
  ['3,141516171819', 3.141516171819],
  ['-123456789,01234', -123456789.01234]
]
const testsEntiersString = [
  ['1', 1],
  ['0', 0],
  ['-3', -3],
  ['12345678901', 12345678901],
  ['-12345678901', -12345678901]
]
const testsIdem = [
  0.2,
  -1.34,
  3.141516171819,
  -123456789.01234
]

describe('j3pVirgule', () => {
  // pour le stub de la console
  let consoleErrorStub
  beforeAll(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  beforeEach(() => {
    consoleErrorStub.resetHistory()
  })
  afterAll(() => {
    consoleErrorStub.restore()
  })
  const checkConsoleCall = () => {
    expect(consoleErrorStub).to.have.been.calledOnce
    consoleErrorStub.resetHistory()
  }
  const checkNoConsoleCall = () => {
    // on pourrait faire simplement ça
    // expect(consoleErrorStub).to.not.have.been.called
    // mais ça ne donne aucune info sur l’appel qui n’aurait pas dû avoir lieu
    const firstCall = consoleErrorStub.getCall(0)
    if (firstCall) {
      if (firstCall.args[0] instanceof Error) throw firstCall.args[0]
      throw Error('console.error a été appelé avec ' + stringify(firstCall.args))
    }
  }

  it('traite les décimaux à virgule', () => {
    for (const [test, expected] of testsVirgule) {
      expect(j3pNombre(test)).to.equals(expected)
      checkNoConsoleCall()
    }
  })
  it('traite les décimaux avec point (en string)', () => {
    for (const [test, expected] of testsPoint) {
      expect(j3pNombre(test)).to.equals(expected)
      checkNoConsoleCall()
    }
  })
  it('converti les entiers string => number', () => {
    for (const [test, expected] of testsEntiersString) {
      expect(j3pNombre(test)).to.equals(expected)
      checkNoConsoleCall()
    }
  })
  it('conserve les number à l’identique', () => {
    for (const test of testsIdem) {
      expect(j3pNombre(test)).to.equals(test)
      checkNoConsoleCall()
    }
  })
  it('Retourne NaN si on lui file une string qui n’est pas un nombre (sans râler)', () => {
    expect(Number.isNaN(j3pNombre('foo'))).to.be.true
    checkNoConsoleCall()
    expect(Number.isNaN(j3pNombre('foo.bar'))).to.be.true
    checkNoConsoleCall()
    expect(Number.isNaN(j3pNombre('foo,bar'))).to.be.true
    checkNoConsoleCall()
  })
  it('râle si on lui file autre chose qu’une string ou un number et retourne NaN', () => {
    expect(Number.isNaN(j3pNombre(new Date()))).to.be.true
    checkConsoleCall()
    expect(Number.isNaN(j3pNombre(Error('fake error')))).to.be.true
    checkConsoleCall()
    expect(Number.isNaN(j3pNombre(/foo/))).to.be.true
    checkConsoleCall()
  })
})
