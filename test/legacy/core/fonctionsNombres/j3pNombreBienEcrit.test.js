import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinonChai from 'sinon-chai'
import sinon from 'sinon'

import { j3pNombreBienEcrit } from 'src/legacy/core/functions'

use(sinonChai)

const tests = [
  [0, '0'],
  [1, '1'],
  [10, '10'],
  [100, '100'],
  [1000, '1 000'],
  [10000, '10 000'],
  [100000, '100 000'],
  [1000000, '1 000 000'],
  [10000000, '10 000 000'],
  [100000000, '100 000 000'],
  [1000000000, '1 000 000 000'],
  [10000000000, '10 000 000 000'],
  [100000000000, '100 000 000 000'],
  [0.1, '0,1'],
  [0.01, '0,01'],
  [0.001, '0,001'],
  [0.0001, '0,000 1'],
  [0.00001, '0,000 01'],
  [0.000001, '0,000 001'],
  [0.0000001, '0,000 000 1'],
  [0.00000001, '0,000 000 01'],
  [0.000000001, '0,000 000 001'],
  [0.0000000001, '0,000 000 000 1'],
  [0.00000000001, '0,000 000 000 01'],
  [0.000000000001, '0,000 000 000 001'],
  [12345, '12 345'],
  [123456, '123 456'],
  [1234567, '1 234 567'],
  [12345.6789012, '12 345,678 901 2'],
  [12345.006789012, '12 345,006 789 012'],
  [0.12345678901, '0,123 456 789 01'],
  ['023.234500', '023,234 500', { garderZeroNonSignificatif: true }],
  ['0023.2340500', '0 023,234 050 0', { garderZeroNonSignificatif: true }],
  ['0023.2340500', '0 023,234 050', { garderZeroNonSignificatif: true, maxDecimales: 6 }],
  [23.4506, '23,450 600', { garderZeroNonSignificatif: true, maxDecimales: 6 }],
  [12345.006789012, '12 345,007', { maxDecimales: 3 }]
]
describe('j3pNombreBienEcrit', () => {
  // pour le stub de la console
  let consoleErrorStub
  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })
  const checkConsoleCall = () => {
    expect(consoleErrorStub).to.have.been.calledOnce
    consoleErrorStub.resetHistory()
  }
  const withZeroOpts = { garderZeroNonSignificatif: true }

  it('formate des nombres comme attendu', () => {
    tests.forEach(([nb, expected, opts]) => {
      expect(j3pNombreBienEcrit(nb, opts)).to.equals(expected)
      const isNbString = typeof nb === 'string'
      // idem en négatif
      const nbNeg = isNbString ? '-' + nb : -nb
      if (nb !== 0) expect(j3pNombreBienEcrit(nbNeg, opts)).to.equals('-' + expected)
      let nbStr
      if (!isNbString) {
        // même résultat si on lui passe le nb en string, séparateur virgule ou point
        nbStr = expected.replace(/ /g, '')
        expect(j3pNombreBienEcrit(nbStr, opts)).to.equals(expected)
        expect(j3pNombreBienEcrit(nbStr.replace(/,/, '.'), opts)).to.equals(expected)
      }
      // si le test ne précise rien on s’attend au même résultat avec garderZeroNonSignificatif
      if (!opts) {
        opts = withZeroOpts
        expect(j3pNombreBienEcrit(nb, opts)).to.equals(expected)
        if (nb !== 0) expect(j3pNombreBienEcrit(-nb, opts)).to.equals('-' + expected)
        if (nbStr) {
          expect(j3pNombreBienEcrit(nbStr, opts)).to.equals(expected)
          expect(j3pNombreBienEcrit(nbStr.replace(/,/, '.'), opts)).to.equals(expected)
        }
      }
      expect(consoleErrorStub).to.not.have.been.called
    })
  })

  it('retourne une chaîne vide si on lui passe une chaîne vide, sans râler (mode non strict)', () => {
    ;[{ garderZeroNonSignificatif: true }, { maxDecimales: 4 }].forEach(opts => {
      expect(j3pNombreBienEcrit('', opts)).to.equals('')
    })
    expect(consoleErrorStub).to.not.have.been.called
  })

  it('râle en console et retourne une chaîne vide si on lui passe des trucs louches (mode non strict)', () => {
    ['un', null, undefined, true, false, Number.POSITIVE_INFINITY].forEach((nb, i) => {
      expect(j3pNombreBienEcrit(nb)).to.equals('')
      checkConsoleCall()
      expect(j3pNombreBienEcrit(nb, withZeroOpts)).to.equals('')
      checkConsoleCall()
      expect(j3pNombreBienEcrit(nb, { maxDecimales: i })).to.equals('')
      checkConsoleCall()
    })
  })

  it('avec l’option strict, throw une erreur si on lui passe autre chose qu’un number fini', () => {
    ;['un', '', null, undefined, true, false, '', '42', '0', Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY, Number.NaN].forEach((nb, i) => {
      ;[{ strict: true }, { strict: true, garderZeroNonSignificatif: true }, { strict: true, maxDecimales: i }].forEach(opts => {
        expect(() => j3pNombreBienEcrit(nb, opts)).to.throw(/Nombre invalide/)
      })
    })
  })
})
