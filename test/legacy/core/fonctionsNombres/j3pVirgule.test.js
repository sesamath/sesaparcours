import { describe, it, beforeAll, beforeEach, afterAll } from 'vitest'
import { expect, use } from 'chai'
import sinonChai from 'sinon-chai'
import sinon from 'sinon'

import { j3pVirgule } from 'src/legacy/core/functions'
import { stringify } from 'src/lib/utils/object'

use(sinonChai)

const testPointString = [
  ['0.2', '0,2'],
  ['-1.34', '-1,34'],
  ['3.141516171819', '3,141516171819'],
  ['-123456789.01234', '-123456789,01234']
]
const testPointNumber = [
  [0.2, '0,2'],
  [-1.34, '-1,34'],
  [3.141516171819, '3,141516171819'],
  [-123456789.01234, '-123456789,01234']
]
const testsIdem = [
  '1,2',
  '3,141516171819',
  '-123456789,01234',
  '42',
  '2000'
]
const testsArrondis = [
  ['3', '3', 3],
  [-3, '-3', 3],
  ['3.14159', '3,1416', 4],
  [3.1415926, '3,1416', 4]
]
const testsZerosDropped = [
  [3, '3', 3],
  [300, '300', 3],
  [300, '300'],
  ['300', '300'],
  ['300', '300', 3],
  // avec 15 chiffres et des 0 à la fin
  ['123456789012340', '123456789012340'],
  // avec 16 chiffres et des 0 à la fin
  ['1234567890123400', '1,2345678901234e+15'],
  ['300.00', '300'],
  ['300.00', '300', 3],
  ['300,00', '300'],
  ['300,00', '300', 3],
  ['300.10', '300,1', 3],
  ['300.10', '300,1'],
  ['3.20e1', '32'],
  ['3.20e1', '32', 3],
  [3.1415, '3,142', 3],
  ['3.14150', '3,1415']
]

describe('j3pVirgule', () => {
  // pour le stub de la console
  let consoleErrorStub
  beforeAll(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  beforeEach(() => {
    consoleErrorStub.resetHistory()
  })
  afterAll(() => {
    consoleErrorStub.restore()
  })
  const checkConsoleCall = () => {
    expect(consoleErrorStub).to.have.been.calledOnce
    consoleErrorStub.resetHistory()
  }
  const checkNoConsoleCall = () => {
    // on pourrait faire simplement ça
    // expect(consoleErrorStub).to.not.have.been.called
    // mais ça ne donne aucune info sur l’appel qui n’aurait pas dû avoir lieu
    const firstCall = consoleErrorStub.getCall(0)
    if (firstCall) {
      if (firstCall.args[0] instanceof Error) throw firstCall.args[0]
      throw Error('console.error a été appelé avec ' + stringify(firstCall.args))
    }
  }

  it('remplace le point par une virgule dans une string', () => {
    for (const [test, expected] of testPointString) {
      expect(j3pVirgule(test)).to.equals(expected)
      checkNoConsoleCall()
    }
  })
  it('remplace le point par une virgule dans un number', () => {
    for (const [test, expected] of testPointNumber) {
      expect(j3pVirgule(test)).to.equals(expected)
      checkNoConsoleCall()
    }
  })
  it('laisse inchangé le décimal à virgule ou l’entier', () => {
    for (const test of testsIdem) {
      expect(j3pVirgule(test)).to.equals(test, `Pb avec ${test}`)
      checkNoConsoleCall()
    }
  })
  it('arrondi si besoin au nb max de décimales demandées', () => {
    for (const [test, expected, maxDec] of testsArrondis) {
      expect(j3pVirgule(test, maxDec)).to.equals(expected)
      checkNoConsoleCall()
    }
  })
  it('râle si on lui file une string qui n’est pas un nombre fini (et le retourne en remplaçant le . par une virgule (pour compatibilité ascendante :-/)', () => {
    expect(j3pVirgule('foo')).to.equals('foo')
    checkConsoleCall()
    expect(j3pVirgule('foo.bar')).to.equals('foo,bar')
    checkConsoleCall()
  })
  it('vire les 0 non significatifs', () => {
    for (const [test, expected, maxDec] of testsZerosDropped) {
      expect(j3pVirgule(test, maxDec)).to.equals(expected)
      checkNoConsoleCall()
    }
  })
})
