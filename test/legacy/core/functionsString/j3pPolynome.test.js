import { describe, expect, it } from 'vitest'
import { j3pPolynome } from 'src/legacy/core/functions'

const exemples = {
  '3x^2-2x+1': [3, -2, 1],
  'x^2+1': [1, 0, 1],
  '-x^3+x-1': [-1, 0, 1, -1],
  'x^3': [1, 0, 0, 0],
  '242x^{12}-2x+1': [242, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 1]
}
const coefsFoireux = [
  null,
  undefined,
  true,
  false,
  42,
  'foo',
  ['4'],
  [1, 2, '3']
]
describe('j3pPolynome', () => {
  it('retourne la string attendue', () => {
    for (const [poly, coefs] of Object.entries(exemples)) {
      expect(j3pPolynome(coefs)).to.equals(poly)
      expect(j3pPolynome(coefs, 'y')).to.equals(poly.replace(/x/g, 'y'))
    }
  })
  it('plante si coefs n’est pas un tableau de nombre', () => {
    for (const coefs of coefsFoireux) {
      expect(() => j3pPolynome(coefs)).to.throw
    }
  })
})
