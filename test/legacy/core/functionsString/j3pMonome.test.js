import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinonChai from 'sinon-chai'
import sinon from 'sinon'

import { j3pMonome } from 'src/legacy/core/functions'

use(sinonChai)

describe('j3pMonome', () => {
  let consoleErrorStub
  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  it('retourne la string attendue', () => {
    // degré 0
    expect(j3pMonome(1, 0, 1)).to.equals('1')
    expect(j3pMonome(2, 0, 1)).to.equals('+1')
    expect(j3pMonome(1, 0, -1)).to.equals('-1')
    expect(j3pMonome(2, 0, -1)).to.equals('-1')
    expect(j3pMonome(1, 0, -2.8)).to.equals('-2,8')
    expect(j3pMonome(1, 0, 4.2, 'y')).to.equals('4,2')
    expect(j3pMonome(1, 0, '4,2', 'y')).to.equals('4,2')
    expect(j3pMonome(2, 0, '4,2', 'y')).to.equals('+4,2')
    expect(j3pMonome(1, 0, '-4,2', 'y')).to.equals('-4,2')
    // degré 1
    expect(j3pMonome(1, 1, 1)).to.equals('x')
    expect(j3pMonome(1, 1, -1)).to.equals('-x')
    expect(j3pMonome(2, 1, 1, 't')).to.equals('+t')
    // degré 2
    expect(j3pMonome(1, 2, 1)).to.equals('x^2')
    expect(j3pMonome(1, 2, 1, 'y')).to.equals('y^2')
    expect(j3pMonome(1, 2, -1)).to.equals('-x^2')
    expect(j3pMonome(2, 2, 1)).to.equals('+x^2')
    expect(j3pMonome(2, 2, '-5,3')).to.equals('-5,3x^2')
    expect(j3pMonome(2, 2, 1, '(x-3)')).to.equals('+(x-3)^2')
    // degré -2
    expect(j3pMonome(2, -2, -3)).to.equals('-3x^{-2}')
    // degré -42
    expect(j3pMonome(2, -42, '-5,3')).to.equals('-5,3x^{-42}')
    // degré 56
    expect(j3pMonome(1, 56, '5,3', 't')).to.equals('5,3t^{56}')
    expect(consoleErrorStub).to.not.have.been.called
  })

  it('râle si coef est foireux (et retourne une string vide', () => {
    expect(j3pMonome(2, 1, 'a')).to.equals('')
    expect(j3pMonome(2, 1, '4/3')).to.equals('')
    expect(consoleErrorStub).to.have.been.calledTwice
  })
  it('râle si degré est foireux (mais le laisse tel quel)', () => {
    // degré 4/3 qui passe mais qui râle
    expect(j3pMonome(2, '4/3', 42, 'z')).to.equals('+42z^{4/3}')
    expect(consoleErrorStub).to.have.been.calledOnce
  })
})
