import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

import { getStyleFromCssString } from 'src/lib/utils/css'

use(sinonChai)

describe('getStyleFromCssString', () => {
  // pour le stub de la console
  let consoleErrorStub

  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  const check = (style) => {
    expect(style.zIndex).to.equal('3')
    expect(style.color).to.equal('#fff')
    expect(style.fontSize).to.equal('12pt')
    expect(Object.keys(style)).to.have.length(3)
  }

  it('transforme bien les attributs avec - en camelCase', () => {
    const css = 'z-index: 3 ; color: #fff; font-size: 12pt'
    check(getStyleFromCssString(css))
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('vire les vides « ; ; », ignore les espaces non significatifs, dernier ; facultatif', () => {
    let css = 'z-index: 3 ;; color: #fff; ; font-size:12pt'
    check(getStyleFromCssString(css))
    css += ';'
    check(getStyleFromCssString(css))
    css = ' z-index  : 3 ; ; color:   #fff  ; font-size : 12pt'
    check(getStyleFromCssString(css))
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('râle s’il manque une valeur, ou s’il y a une espace dans le nom d’attribut (et ignore dans ce cas)', () => {
    [
      'z-index: 3; color: #fff; background: ; font-size:12pt',
      'z-index: 3; color: #fff; back ground: #fff; font-size:12pt'
    ].forEach((css, i) => {
      check(getStyleFromCssString(css))
      expect(consoleErrorStub).to.have.been.callCount(i + 1)
    })
  })
})
