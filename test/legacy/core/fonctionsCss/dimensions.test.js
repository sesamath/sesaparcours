import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinonChai from 'sinon-chai'
import sinon from 'sinon'

import { _cssToNb } from 'src/legacy/core/functions'
import { getCssDimension } from 'src/lib/utils/css'

use(sinonChai)

describe('Fonctions css', () => {
  // pour le stub de la console
  let consoleErrorStub

  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  describe('_cssToNb', () => {
    const nbs = [-42, -1.234, 0, 0.123, 1, 42, 99999999]
    it('retourne le number inchangé s’il est valide (not NaN ou infini)', () => {
      nbs.forEach(n => {
        expect(_cssToNb(n)).to.equals(n, `Pb avec le number ${n}`)
        expect(consoleErrorStub).to.not.have.been.called
      })
    })
    it('retourne le number casté si on passe une string avec px ou pas, ; ou pas', () => {
      nbs.forEach(n => {
        const str = String(n)
        // on teste les 4 suffixes pour chaque nb
        ;[str, `${str};`, `${str}px`, `${str}px;`].forEach(s => {
          expect(_cssToNb(s)).to.equals(n, `Pb avec la string ${s}`)
          expect(consoleErrorStub).to.not.have.been.called
        })
      })
    })
    it('retourne 0 et râle dans console.error sinon', () => {
      [
        null,
        undefined,
        true,
        false,
        NaN,
        Number.POSITIVE_INFINITY,
        [],
        {},
        'foo',
        '42em',
        '3%',
        '-42rem',
        '-1%'
      ].forEach(value => {
        expect(_cssToNb(value)).to.equals(0, `Pb avec ${value}`)
        expect(consoleErrorStub).to.have.been.calledOnce
        consoleErrorStub.resetHistory()
      })
    })
  })

  describe('getCssDimension', () => {
    it('Ajoute px si besoin et retourne une string, sans râler', () => {
      // la liste des trucs à tester
      ;[
        [0, '0'],
        ['0', '0'],
        [1, '1px'],
        [-1, '-1px'],
        [0.2, '0.2px'],
        ['.2', '.2px'],
        [+0.2, '0.2px'],
        ['+0.2', '+0.2px'],
        [-0.2, '-0.2px'],
        ['-.2', '-.2px'],
        [-0.2, '-0.2px'],
        ['-0.2', '-0.2px'],
        ['3em', '3em']
      ].forEach(([src, expected]) => {
        expect(getCssDimension(src)).to.equal(expected, `pb avec ${src} (≠ ${expected}, type ${typeof src})`)
      })
      ;['px', 'em', 'rem', 'ex', 'ch', 'vw', 'vh', 'vmin', 'vmax', '%', 'pt', 'pc', 'in', 'Q', 'mm', 'cm'].forEach(unit => {
        [-2, -0.2, 0, '.2', 0.2, 2].forEach(v => {
          expect(getCssDimension(v + unit)).to.equal(v + unit, `pb avec ${v} (type ${typeof v})`)
        })
      })
      expect(consoleErrorStub).to.not.have.been.called
    })
    it('retourne une chaîne vide et râle en console sur une valeur ou une unité invalide', () => {
      [undefined, null, '4foo', 'foo4', '4pix', Number.POSITIVE_INFINITY].forEach((foireux, i) => {
        expect(getCssDimension(foireux)).to.equal('')
        expect(consoleErrorStub).to.have.been.callCount(i + 1)
      })
    })
  })
})
