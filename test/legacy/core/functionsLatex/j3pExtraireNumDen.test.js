import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

import { j3pExtraireNumDen } from 'src/legacy/core/functionsLatex'

use(sinonChai)

describe('j3pExtraireNumDen', () => {
  // pour le stub de la console
  let consoleErrorStub
  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  it('récupère numérateur et dénominateur comme attendu', () => {
    for (const [frac, [isFrac, num, den, signe]] of [
      ['0', [false, '0', '1', '+']],
      [1e-14, [false, '1e-14', '1', '+']], // normal ???
      ['1', [false, '1', '1', '+']],
      ['-1', [false, '-1', '1', '-']],
      [2, [false, '2', '1', '+']],
      ['\\frac{3}{2}', [true, '3', '2', '+']],
      [' \\frac{3.14}{2,5}', [true, '3.14', '2,5', '+']],
      [' - \\frac{3.14}{2,5}', [true, '-3.14', '2,5', '-']],
      ['-\\frac{-3.14}{2,5}', [true, '3.14', '2,5', '+']],
      ['-\\frac{3.14}{-2,5}', [true, '-3.14', '-2,5', '+']],
      ['\\frac{42.5}{-2,5}', [true, '42.5', '-2,5', '-']]
    ]) {
      const [resIsFrac, resNum, resDen, resSigne] = j3pExtraireNumDen(frac)
      expect(resIsFrac).to.equals(isFrac, `Pb isFrac avec ${frac}`)
      expect(resNum).to.equals(num, `Pb num avec ${frac}`)
      expect(resDen).to.equals(den, `Pb den avec ${frac}`)
      expect(resSigne).to.equals(signe, `Pb signe avec ${frac}`)
      expect(consoleErrorStub).to.not.have.been.called
    }
  })

  it('retourne le numérateur identique à la string passée, avec signe vide et isFrac false, si ce qu’on passe n’est ni un nombre ni une fraction de nombre', () => {
    for (const entree of ['\\frac{\\sqrt{2}}{2}', '\\frac{2^3}{5}']) {
      const [isFrac, num, den, signe] = j3pExtraireNumDen(entree)
      const errMsg = `Pb avec ${entree}`
      expect(isFrac).to.equals(false, errMsg)
      expect(num).to.equals(String(entree), errMsg)
      expect(den).to.equals('1', errMsg)
      expect(signe).to.equals('', errMsg)
      expect(consoleErrorStub).to.have.been.calledOnce
      consoleErrorStub.resetHistory()
    }
  })

  it('plante avec une chaîne vide ou autre chose que string|number', () => {
    for (const entree of [Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY, '', null, undefined, true, {}]) {
      expect(() => j3pExtraireNumDen(entree)).to.throw
    }
  })
})
