import { describe, expect, it } from 'vitest'

import { j3pSimplificationRacineTrinome } from 'src/legacy/core/functionsLatex'

// on avait du @ vitest-environment jsdom et parfois un pb de
// `something prevents Vite server from exiting` sous windows
// => on le vire, et on remettra plutôt un environnement happy-dom si la fct testée ici en a besoin

describe.skip('j3pSimplificationRacineTrinome', () => {
  // @todo à implémenter
  it('retourne la string attendue', () => {
    const listes = [
      // b, plusmoins, delta, a
      ['b', 'plusmoins', 'delta', 'a', 'attendu']
      // etc.
    ]
    for (const [b, plusmoins, delta, a, expected] of listes) {
      expect(j3pSimplificationRacineTrinome(b, plusmoins, delta, a)).to.equals(expected, `Pb avec (${[b, plusmoins, delta, a].join(', ')})`)
    }
  })

  // @todo à implémenter
  it('plante avec des arguments bizarres', () => {
    const liste = [
      [1, Number.POSITIVE_INFINITY, 'etc']
    ]
    for (const args of liste) {
      expect(() => j3pSimplificationRacineTrinome.apply(null, args)).to.throw
    }
  })
})
