import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'

use(sinonChai)

describe('j3pGetLatexMonome', () => {
  let consoleErrorStub
  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  it('retourne la string attendue', () => {
    for (const [args, expected] of [
      // rang, puiss, nb, variable
      [[1, 1, 1], 'x'],
      [[1, 1, 1, 'a'], 'a'],
      [[1, 1, 0], ''],
      [[1, 1, 0, 'a'], ''],
      [[1, 2, 3], '3x^2'],
      [[2, 2, 3], '+3x^2'],
      [[3, 2, 3], '+3x^2'],
      [[2, 1, '-\\frac{1}{3}'], '-\\frac{1}{3}x'],
      [[2, 3, '\\frac{-1}{3}', 'y'], '-\\frac{1}{3}y^3'],
      [[2, 0, '\\frac{1}{3}'], '+\\frac{1}{3}'],
      [[1, 2, '+\\frac{1}{3}'], '\\frac{1}{3}x^2'],
      [[1, 12, '\\frac{7}{3}', 'y'], '\\frac{7}{3}y^{12}'],
      [[1, -3, '-\\frac{1}{2}'], '-\\frac{1}{2}x^{-3}'], // cela peut tout aussi bien marcher avec des puissances négatives
      [[2, 0, 0.2], '+0,2'],
      [[2, 3, 0.2], '+0,2x^3']
      // @todo à étoffer
    ]) {
      expect(j3pGetLatexMonome.apply(null, args)).to.equals(expected, `Pb avec (${args.join(', ')})`)
      expect(consoleErrorStub).to.not.have.been.called
    }
    // ça marche aussi avec une puissance non entière, mais ça râle en console
    const [args, expected] = [[1, Math.PI, 3], '3x^{' + String(Math.PI) + '}']
    expect(j3pGetLatexMonome.apply(null, args)).to.equals(expected, `Pb avec (${args.join(', ')})`)
    expect(consoleErrorStub).to.have.been.calledOnce
  })

  // @todo à implémenter
  it.skip('plante avec des arguments bizarres', () => {
    ;[
      [1, Number.POSITIVE_INFINITY],
      [Number.NEGATIVE_INFINITY, 2],
      ['', 1],
      [null, -1],
      [undefined, true],
      [42, {}]
    ].forEach(args => {
      expect(() => j3pGetLatexMonome.apply(null, args)).to.throw
    })
  })
})
