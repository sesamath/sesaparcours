import { describe, expect, it } from 'vitest'

import { j3pGetLatexFrac } from 'src/legacy/core/functionsLatex'

describe('j3pGetLatexFrac', () => {
  it('retourne la string attendue (éventuellement un nombre entier)', () => {
    ;[
      [1, 1, '1'],
      [0, 42, '0'],
      [1e-14, 1, '0'],
      [42, 1, '42'],
      [-42, 1, '-42'],
      [4, 2, '2'],
      [3, 2, '\\frac{3}{2}'],
      [3, -2, '\\frac{-3}{2}'],
      [3.11, -2.5, '\\frac{-311}{250}'],
      [3.4, -2.2, '\\frac{-17}{11}'],
      [-3.4, -0.17, '20'],
      [3.14, -2.5, '\\frac{-157}{125}'],
      [1e-9, 2, '\\frac{1}{2000000000}'],
      [1e-14, 2, '0'],
      [1e-3, 1e10, '0'], // donne un entier trop grand
      [8e12, 2, '4000000000000'],
      [8e10, 2e-2, '4000000000000'],
      [Math.PI, 2, '\\frac{314159265359}{200000000000}'] // c’est j3pMeilleurArrondi qui tronque à 12 chiffres
    ].forEach(([num, den, expected]) => {
      expect(j3pGetLatexFrac(num, den)).to.equals(expected, `Pb avec ${num} et ${den}`)
    })
  })

  it('plante avec autre chose que des number finis < 1e13, ou avec un dénominateur nul (ou presque), ou si le résultat du quotient est un entier trop grand (1e13)', () => {
    ;[
      [1, Number.POSITIVE_INFINITY],
      [Number.NEGATIVE_INFINITY, 2],
      ['', 1],
      [null, -1],
      [undefined, true],
      [42, {}],
      [2, 1e13],
      [2, 1e-12], // dénominateur trop petit => division par 0
      [-2e13, -3],
      [1e10, 1e-3], // donne un entier trop grand
      [1e-3, 1e10], // donne un entier trop grand
      [4e12, 2e-9] // donne un entier trop grand
    ].forEach((num, den) => {
      expect(() => j3pGetLatexFrac(num, den)).to.throw
    })
  })
})
