import { describe, expect, it } from 'vitest'

import { j3pGetLatexOppose, j3pGetLatexProduit, j3pGetLatexSomme, j3pGetLatexQuotient, j3pGetLatexPuissance } from 'src/legacy/core/functionsLatex'

const zarbList = [Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY, '', null, undefined, true, {}, null]

describe('j3pGetLatexOppose', () => {
  it('retourne la string attendue', () => {
    ;[
      [0, '0'],
      [1, '-1'],
      [-1, '1'],
      [1 + 1e-14, '-1'],
      ['-3', '3'],
      ['42', '-42'],
      ['-\\frac{2}{-3}', '\\frac{-2}{3}'],
      ['-\\frac{-6}{-9}', '\\frac{2}{3}'],
      ['-\\frac{-6}{-2}', '3'],
      ['\\frac{4}{2}', '-2']
    ].forEach(([nb, expected]) => {
      expect(j3pGetLatexOppose(nb)).to.equals(expected, `Pb avec (${nb})`)
    })
  })

  it('plante avec des arguments bizarres', () => {
    zarbList.forEach(nb => {
      expect(() => j3pGetLatexOppose(nb)).to.throw
    })
  })
})

describe('j3pGetLatexSomme', () => {
  it('retourne la string attendue', () => {
    ;[
      [0, 1, '1'],
      [1, '-1', '0'],
      [-1, 1, '0'],
      [1 + 1e-14, 2, '3'],
      ['\\frac{6}{2}', -5, '-2'],
      ['2', '-\\frac{6}{3}', '0'],
      ['-\\frac{2}{-3}', '\\frac{3}{2}', '\\frac{13}{6}']
      // @todo à étoffer
    ].forEach(([n1, n2, expected]) => {
      expect(j3pGetLatexSomme(n1, n2)).to.equals(expected, `Pb avec (${n1} et ${n2})`)
    })
  })

  it('plante avec des arguments bizarres', () => {
    zarbList.forEach(nb => {
      expect(() => j3pGetLatexSomme(nb, 1)).to.throw
    })
  })
})

describe('j3pGetLatexProduit', () => {
  it('retourne la string attendue', () => {
    ;[
      [0, 1, '0'],
      [1, '-1', '-1'],
      [-1, 1, '-1'],
      [1 + 1e-14, 2, '2'],
      ['\\frac{6}{2}', -5, '-15'],
      ['2', '-\\frac{6}{3}', '-4'],
      ['-\\frac{2}{-3}', '\\frac{3}{2}', '1']
      // @todo à étoffer
    ].forEach(([n1, n2, expected]) => {
      expect(j3pGetLatexProduit(n1, n2)).to.equals(expected, `Pb avec (${n1} et ${n2})`)
    })
  })

  it('plante avec des arguments bizarres', () => {
    zarbList.forEach(nb => {
      expect(() => j3pGetLatexProduit(nb, 1)).to.throw
      expect(() => j3pGetLatexProduit(1, nb)).to.throw
    })
  })
})

describe('j3pGetLatexQuotient', () => {
  it('retourne la string attendue', () => {
    ;[
      [0, 1, '0'],
      [1, '-1', '-1'],
      [-1, 1, '-1'],
      [1 + 1e-14, 2, '\\frac{1}{2}'],
      ['\\frac{6}{2}', -5, '\\frac{-3}{5}'],
      ['2', '-\\frac{6}{3}', '-1'],
      ['-\\frac{2}{-3}', '\\frac{4}{5}', '\\frac{5}{6}']
      // @todo à étoffer
    ].forEach(([n1, n2, expected]) => {
      expect(j3pGetLatexQuotient(n1, n2)).to.equals(expected, `Pb avec (${n1} et ${n2})`)
    })
  })

  it('plante avec des arguments bizarres', () => {
    zarbList.forEach(nb => {
      expect(() => j3pGetLatexQuotient(nb, 1)).to.throw
      expect(() => j3pGetLatexQuotient(1, nb)).to.throw
    })
  })
})

describe('j3pGetLatexPuissance', () => {
  it('retourne la string attendue', () => {
    ;[
      [0, 1, '0'],
      ['0', '1', '0'],
      [1, 1, '1'],
      [1, '1', '1'],
      [-1, 1, '-1'],
      [1 + 1e-14, 1, String(1 + 1e-14)], // pas génial mais c’est comme ça que ça fonctionne
      ['\\frac{6}{2}', 2, '9'],
      ['2', '3', '8'],
      ['-4', '2', '16']
      // @todo à étoffer
    ].forEach(([nb, puiss, expected]) => {
      expect(j3pGetLatexPuissance(nb, puiss)).to.equals(expected, `Pb avec (${nb} et ${puiss})`)
    })
  })

  it('plante avec des arguments bizarres', () => {
    zarbList.forEach(nb => {
      expect(() => j3pGetLatexPuissance(nb, 1)).to.throw
    })
  })
})
