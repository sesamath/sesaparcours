import { describe, expect, it } from 'vitest'

import { j3pGetLatexRacine } from 'src/legacy/core/functionsLatex'

describe('j3pGetLatexRacine', () => {
  it('retourne la string attendue', () => {
    ;[
      [0, '0'],
      ['1', '1'],
      ['4', '2'],
      [8, '2\\sqrt{2}'],
      ['\\frac{8}{2}', '2'],
      ['\\frac{3}{2}', '\\frac{\\sqrt{6}}{2}']
      // etc.
    ].forEach(([nb, expected]) => {
      expect(j3pGetLatexRacine(nb)).to.equals(expected, `Pb avec (${nb})`)
    })
  })

  it('plante avec autre chose que des nombres positifs', () => {
    ; [-1, Number.POSITIVE_INFINITY, '', null, true, undefined, {}, [], '\\frac{-2}{3}', '-\\frac{3}{2}'].forEach(nb => {
      expect(() => j3pGetLatexRacine(nb)).to.throw
    })
  })
})
