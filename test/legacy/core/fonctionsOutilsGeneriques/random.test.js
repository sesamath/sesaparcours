import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinonChai from 'sinon-chai'
import sinon from 'sinon'

import { j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomFixed, j3pGetRandomFloat, j3pGetRandomInt } from 'src/legacy/core/functions'

use(sinonChai)

const nbTirages = 500
const tolerance = 0.5 // ± 50% par rapport à la fréquence attendue
const essaisMax = 10 // en cas d’un tirage "mal distribué" on recommence, avec 3 le test plantait de tps en tps, avec 10 on est plus tranquille…
const contexte = `${nbTirages} tirages et tolérance de ±${tolerance * 100}% sur la fréquence attendue`

describe(`j3pGetRandomBool (${contexte})`, () => {
  function batch () {
    let i = 0
    let nbTrue = 0
    let result
    while (i < nbTirages) {
      result = j3pGetRandomBool()
      if (typeof result !== 'boolean') {
        throw Error(`j3pGetRandomBool retourne autre chose qu’un booléen : ${typeof result} ${result}`)
      }
      if (result) nbTrue++
      i++
    }
    nbEssais++
    if (Math.abs(nbTrue / nbTirages - 0.5) > (0.5 * tolerance)) {
      if (nbEssais < essaisMax) return batch()
      throw Error(`j3pGetRandomBool a renvoyé ${nbTrue} true sur ${nbTirages} tirages (écart ${nbTrue / nbTirages - 0.5} > ${tolerance}), 3e essai`)
    }
  }

  let nbEssais = 0
  it('retourne un booléen à ~50/50', batch)
})

describe(`j3pGetRandomElt (${contexte})`, () => {
  function batch () {
    const fixture = ['foo', 42, 'bar', null, undefined, false, 'baz']
    const results = fixture.map(() => 0)
    const nb = fixture.length
    let i = 0
    while (i < nbTirages) {
      const result = j3pGetRandomElt(fixture)
      const indice = fixture.indexOf(result)
      if (indice === -1) {
        throw Error(`j3pGetRandomElt retourne autre chose qu’un élément du tableau passé : ${typeof result} ${result}`)
      }
      results[indice]++
      i++
    }
    nbEssais++

    const frequences = results.map(nb => nb / nbTirages)
    frequences.forEach((f, i) => {
      if (f === 0) {
        if (nbEssais < essaisMax) return batch()
        throw Error(`${fixture[i]} n’est jamais sorti sur ${nbTirages} tirages (3e essai)`)
      }
      if (Math.abs(f - 1 / nb) > (1 / nb * tolerance)) {
        if (nbEssais < essaisMax) return batch()
        throw Error(`j3pGetRandomElt a renvoyé ${fixture[i]} ${results[i]} fois sur ${nbTirages} (fréquence ${f} ≠ 1/${nb} ± ${tolerance}), 3e essai`)
      }
    })
  }

  let nbEssais = 0

  it('retourne un des élément passé avec une proba ~1/n', batch)
})

describe('j3pGetRandomInt', () => {
  const getContext = (nbTirages, borneInf, borneSup) => `${nbTirages} tirages dans [${borneInf} ; ${borneSup}], tolérance de ±${tolerance * 100}% sur la fréquence attendue pour la tranche, ${nbPas} tranches`

  function check (result, borneInf, borneSup) {
    expect(typeof result).to.equal('number', `j3pGetRandomInt retourne autre chose qu’un number (${typeof result})`)
    // above (alias greatThan) est strict, least pour ≥
    // cf https://www.chaijs.com/api/bdd/#method_above
    expect(result).to.equal(Math.round(result), `j3pGetRandomInt retourne autre chose qu’un entier (${typeof result})`)
    expect(result).to.be.at.least(borneInf, `j3pGetRandomInt a retourné ${result} qui est < à la borne inférieure ${borneInf}`)
    // idem pour lessThan et most
    expect(result).to.be.most(borneSup, `j3pGetRandomInt a retourné ${result} qui est > à la borne supérieure ${borneSup}`)
    // et la console
    expect(consoleErrorStub).to.not.have.been.called
  }

  // on lance une série de random, et si on a des probas bizarres on se relance, jusqu'à essaisMax fois
  function batch (borneInf, borneSup) {
    let pas = (borneSup - borneInf) / 10
    if (pas < 1) pas = 1
    else pas = Math.round(pas)
    const nbPas = Math.ceil((borneSup - borneInf) / pas)
    const nbResults = (new Array(nbPas)).fill(0)
    // on lance les tirages
    let i = 0
    while (i < nbTirages) {
      // on teste le console.warn sur le 0 pour un seul appel
      const result = j3pGetRandomInt(borneInf, borneSup)
      check(result, borneInf, borneSup)
      const indice = Math.floor((result - borneInf) / pas)
      nbResults[indice]++
      i++
    }
    nbEssais++

    // on regarde les fréquences récupérées
    const freqExpected = 1 / nbPas
    nbResults.forEach((nb, i) => {
      const inter = `[${borneInf + i} ; ${borneInf + i + 1}[`
      if (nb === 0) {
        if (nbEssais < essaisMax) return batch(borneInf, borneSup)
        throw Error(`aucun résultat dans ${inter} avec ${contexte} (après ${essaisMax} relances)`)
      }
      const freq = nb / nbTirages
      if (Math.abs(freq - freqExpected) > (freqExpected * tolerance)) {
        if (nbEssais < essaisMax) return batch(borneInf, borneSup)
        throw Error(`j3pGetRandomInt a renvoyé ${nb} nombres dans ${inter} avec ${contexte} tirages (fréquence ${freq.toFixed(3)} ≠ ${freqExpected.toFixed(3)} ± ${tolerance * 100}%, on a eu [${nbResults.join(', ')}])`)
      }
    })
  }

  // pour le stub de la console
  let consoleErrorStub
  // pour le nb d’essais (si on a des résultats improbables, on recommence
  let nbEssais

  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  const nbTirages = 500
  const nbPas = 10
  const tolerance = 0.5
  for (const [borneInf, borneSup] of [
    [2, 5],
    [-4, -1.5],
    [4, 1000000]
  ]) {
    const contexte = getContext(nbTirages, borneInf, borneSup)
    nbEssais = 0
    it(`retourne un nombre dans l’intervalle demandé avec la bonne proba (${contexte})`, () => {
      batch(borneInf, borneSup)
    })
  }

  it('accepte des string', () => {
    for (const [borneInf, borneSup] of [['1', 2], [1, '2'], ['-1', 2]]) {
      const result = j3pGetRandomInt(borneInf, borneSup)
      check(result, Number(borneInf), Number(borneSup))
    }
  })

  it('accepte des bornes inversées', () => {
    let result = j3pGetRandomInt(2, 1)
    check(result, 1, 2)
    result = j3pGetRandomInt(2, -5)
    check(result, -5, 2)
  })

  it('retourne un des entiers si l’autre est NaN, en râlant en console', () => {
    for (const [borneInf, borneSup] of [['deux', 3], [1, '3x']]) {
      const result = j3pGetRandomInt(borneInf, borneSup)
      expect(result).to.equals(Number.isInteger(borneInf) ? borneInf : borneSup)
      expect(consoleErrorStub).to.have.been.callCount(1, `console.error n’a pas été appelé avec [${borneInf} ; ${borneSup}]`)
      consoleErrorStub.resetHistory()
    }
  })
  it('retourne NaN si on lui envoie deux Nan', () => {
    function checkNaN (result, borneInf, borneSup) {
      expect(Number.isNaN(result)).to.equal(true, `Ne retourne pas NaN avec [${borneInf} ; ${borneSup}] : ${result}`)
      // on utilise callCount plutôt que calledOnce pour le msg d’erreur (calledOnce n’est pas une fct)
      expect(consoleErrorStub).to.have.been.callCount(1, `console.error n’a pas été appelé avec [${borneInf} ; ${borneSup}]`)
      consoleErrorStub.resetHistory()
    }
    for (const [borneInf, borneSup] of [['deux', 'trois'], ['un', '3x']]) {
      const result = j3pGetRandomInt(borneInf, borneSup)
      checkNaN(result, borneInf, borneSup)
    }
  })
})

// pour ce test, on utilise sinon (pour choper la console) et l’écriture au format chai expect
describe('j3pGetRandomFixed', () => {
  // pas de fct fléchée car on fait du apply dessus
  function getContext (nbTirages, borneInf, borneSup, nbDecimales) {
    return `${nbTirages} tirages dans [${borneInf} ; ${borneSup}] avec ${nbDecimales} décimales, tolérance de ±${tolerance * 100}% sur la fréquence attendue pour chaque tranche, ${nbPas} tranches`
  }
  function check (result, borneInf, borneSup, nbDecimales) {
    expect(typeof result).to.equal('string', `j3pGetRandomFixed retourne autre chose qu’une string (${typeof result})`)
    // et ceux là en string
    const [partieEntiere, decimales] = result.split('.')
    // pour les comparaison on veut du number
    const nResult = Number(result)
    // above (alias greaterThan) est strict, least pour ≥
    // cf https://www.chaijs.com/api/bdd/#method_above
    expect(nResult).to.be.at.least(borneInf, `j3pGetRandomFixed a retourné ${result} qui est < à la borne inférieure ${borneInf}`)
    expect(borneSup).to.be.at.least(nResult, `j3pGetRandomFixed a retourné ${result} qui est > à la borne supérieure ${borneSup}`)
    expect(partieEntiere).to.have.lengthOf.at.least(1, `j3pGetRandomFixed retourne une partie entière invalide : ${nResult}`)
    if (nbDecimales === 0) {
      expect(decimales).to.equals(undefined, `j3pGetRandomFixed retourne une partie décimale avec nbDecimales=0 : ${result}`)
    } else {
      expect(decimales).to.have.lengthOf(nbDecimales, `j3pGetRandomFixed retourne une partie décimale qui n’est pas de longueur ${nbDecimales} : ${result}`)
    }

    // et la console
    expect(consoleErrorStub).to.not.have.been.called
    if (nbDecimales === 0) {
      expect(consoleWarnStub).to.have.been.callCount(1, 'console.warn n’a pas été appelé avec 0 décimales')
      expect(consoleWarnStub).to.have.been.calledWithMatch(/plutôt que.*pour récupérer des entiers/)
      consoleWarnStub.resetHistory()
    } else {
      expect(consoleWarnStub).to.not.have.been.called
    }
  }

  function batch (fixture) {
    const [borneInf, borneSup] = fixture
    const pas = (borneSup - borneInf) / nbPas
    // un tableau de 10 éléments rempli de 0
    const nbResults = (new Array(nbPas)).fill(0)
    // on lance les tirages
    let i = 0
    while (i < nbTirages) {
      const result = j3pGetRandomFixed.apply(null, fixture)
      check(result, ...fixture)
      // la ligne suivante marche pas, car le Number(result) peut donner un décalage, 2.3 => 2.300…001 ou 2.299…999, et on peut avoir un indice hors tableau
      // const indice = Math.floor((Number(result) - borneInf) / pas)
      // on passe à du round avec décalage d’une 1/2 tranche
      const indice = Math.round((Number(result) - pas / 2 - borneInf) / pas)
      nbResults[indice]++
      i++
    }
    nbEssais++

    const freqExpected = 1 / nbPas
    for (const [i, nb] of nbResults.entries()) {
      const inter = `[${borneInf + pas * i} ; ${borneInf + pas * (i + 1)}[`
      if (nb === 0) {
        if (nbEssais < essaisMax) return batch(fixture)
        throw Error(`aucun résultat dans ${inter} avec ${contexte}, avec ${essaisMax} essais`)
      }
      const freq = nb / nbTirages
      if (Math.abs(freq - freqExpected) > (freqExpected * tolerance)) {
        if (nbEssais < essaisMax) return batch(fixture)
        throw Error(`j3pGetRandomFixed a renvoyé ${nb} nombres dans ${inter} (fréquence obtenue ${freq.toFixed(3)} ≠ ${freqExpected.toFixed(3)} (attendue) ± ${tolerance * 100}%, sur ${essaisMax} essais on a eu les fréquences [${nbResults.map(n => (n / nbTirages).toFixed(3)).join(', ')}])`)
      }
    }
  }

  // var globales à ce describe
  const nbPas = 7
  let nbEssais = 0
  let contexte
  // pour le stub de la console
  let consoleErrorStub, consoleWarnStub

  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
    consoleWarnStub = sinon.stub(console, 'warn')
  })
  afterEach(() => {
    consoleErrorStub.reset()
    consoleErrorStub.restore()
    consoleWarnStub.reset()
    consoleWarnStub.restore()
  })

  for (const fixture of [
    [2, 3, 1]
    // [-2, 1, 0],
    // [4, 1000000, 2]
  ]) {
    contexte = getContext(nbTirages, ...fixture)
    it(`retourne un nombre dans l’intervalle demandé avec la bonne proba (${contexte})`, () => batch(fixture))
  }

  it('accepte des string', () => {
    let result = j3pGetRandomFixed('1', 2, 3)
    check(result, 1, 2, 3)
    result = j3pGetRandomFixed(1, '2', 3)
    check(result, 1, 2, 3)
    result = j3pGetRandomFixed(1, 2, '3')
    check(result, 1, 2, 3)
    result = j3pGetRandomFixed('-1', '2', '3')
    check(result, -1, 2, 3)
  })

  it('accepte des bornes inversées', () => {
    let result = j3pGetRandomFixed(2, 1, 3)
    check(result, 1, 2, 3)
    result = j3pGetRandomFixed(2, -5, 2)
    check(result, -5, 2, 2)
  })

  it('retourne NaN si on lui envoie n’importe quoi', () => {
    function checkNaN (result, borneInf, borneSup, nbDecimales) {
      expect(result).to.equal('NaN', `Ne retourne pas NaN avec [${borneInf} ; ${borneSup}] et ${nbDecimales} décimales`)
      expect(consoleWarnStub).to.not.have.been.called
      // on utilise callCount plutôt que calledOnce pour le msg d’erreur (calledOnce n’est pas une fct)
      expect(consoleErrorStub).to.have.been.callCount(1, `console.error n’a pas été appelé avec [${borneInf} ; ${borneSup}] et ${nbDecimales} décimales`)
      consoleErrorStub.resetHistory()
    }

    for (const fixture of [
      [1, 2, -3],
      [1, 2, 200],
      ['deux', 1, 3],
      [1, 2, 3.14]
    ]) {
      const result = j3pGetRandomFixed.apply(null, fixture)
      checkNaN(result, ...fixture)
    }
  })
})

describe('j3pGetRandomFloat', () => {
  // pas de fct fléchée car on fait du apply dessus
  function getContext (nbTirages, borneInf, borneSup) {
    return `${nbTirages} tirages dans [${borneInf} ; ${borneSup}], tolérance de ±${tolerance * 100}% sur la fréquence attendue pour la tranche, ${nbPas} tranches`
  }
  function check (result, borneInf, borneSup) {
    expect(typeof result).to.equal('number', `j3pGetRandomFloat retourne autre chose qu’un number (${typeof result})`)
    // above (alias greatThan) est strict, least pour ≥
    // cf https://www.chaijs.com/api/bdd/#method_above
    expect(result).to.be.at.least(borneInf, `j3pGetRandomFloat a retourné ${result} qui est < à la borne inférieure ${borneInf}`)
    expect(result).to.be.lessThan(borneSup, `j3pGetRandomFloat a retourné ${result} qui est ≥ à la borne supérieure ${borneSup}`)
    // et la console
    expect(consoleErrorStub).to.not.have.been.called
  }

  // on lance une série de random, et si on a des probas bizarres on se relance, jusqu'à essaisMax fois
  function batch (borneInf, borneSup) {
    const pas = (borneSup - borneInf) / nbPas
    // un tableau de 10 éléments rempli de 0
    const nbResults = (new Array(nbPas)).fill(0)
    // on lance les tirages
    let i = 0
    while (i < nbTirages) {
      // on teste le console.warn sur le 0 pour un seul appel
      const result = j3pGetRandomFloat(borneInf, borneSup)
      check(result, borneInf, borneSup)
      const indice = Math.floor((result - borneInf) / pas)
      nbResults[indice]++
      i++
    }
    nbEssais++

    // on regarde les fréquences récupérées
    const freqExpected = 1 / nbPas
    for (const [i, nb] of nbResults.entries()) {
      const inter = `[${borneInf + pas * i} ; ${borneInf + pas * (i + 1)}[`
      if (nb === 0) {
        if (nbEssais < essaisMax) return batch(borneInf, borneSup)
        throw Error(`aucun résultat dans ${inter} avec ${contexte} (après ${essaisMax} relances)`)
      }
      const freq = nb / nbTirages
      if (Math.abs(freq - freqExpected) > (freqExpected * tolerance)) {
        if (nbEssais < essaisMax) return batch(borneInf, borneSup)
        throw Error(`j3pGetRandomFloat a renvoyé ${nb} nombres dans ${inter} avec ${contexte} tirages (fréquence ${freq.toFixed(3)} ≠ ${freqExpected.toFixed(3)} ± ${tolerance * 100}%, on a eu [${nbResults.join(', ')}])`)
      }
    }
  }

  // pour le stub de la console
  let consoleErrorStub
  // pour le nb d’essais (si on a des résultats improbables, on recommence
  let nbEssais

  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  const nbTirages = 500
  const nbPas = 10
  const tolerance = 0.5
  for (const [borneInf, borneSup] of [
    [2, 5],
    [-1, -0.5],
    [4, 1000000]
  ]) {
    const contexte = getContext(nbTirages, borneInf, borneSup)
    nbEssais = 0
    it(`retourne un nombre dans l’intervalle demandé avec la bonne proba (${contexte})`, () => {
      batch(borneInf, borneSup)
    })
  }

  it('accepte des string', () => {
    for (const [borneInf, borneSup] of [['1', 2], [1, 2], [1, '2'], [1.5, 2.5], [-1, 2]]) {
      const result = j3pGetRandomFloat(borneInf, borneSup)
      check(result, Number(borneInf), Number(borneSup))
    }
  })

  it('accepte des bornes inversées', () => {
    let result = j3pGetRandomFloat(2, 1)
    check(result, 1, 2)
    result = j3pGetRandomFloat(2, -5)
    check(result, -5, 2)
  })

  it('retourne NaN si on lui envoie n’importe quoi', () => {
    function checkNaN (result, borneInf, borneSup) {
      expect(Number.isNaN(result)).to.equal(true, `Ne retourne pas NaN avec [${borneInf} ; ${borneSup}] : ${result}`)
      // on utilise callCount plutôt que calledOnce pour le msg d’erreur (calledOnce n’est pas une fct)
      expect(consoleErrorStub).to.have.been.callCount(1, `console.error n’a pas été appelé avec [${borneInf} ; ${borneSup}]`)
      consoleErrorStub.resetHistory()
    }

    for (const [borneInf, borneSup] of [
      ['deux', 3],
      [1, '3x']
    ]) {
      const result = j3pGetRandomFloat(borneInf, borneSup)
      checkNaN(result, borneInf, borneSup)
    }
  })
})
