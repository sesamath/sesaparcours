import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinonChai from 'sinon-chai'
import sinon from 'sinon'

import { j3pShuffle, j3pShuffleMulti } from 'src/legacy/core/functions'

use(sinonChai)

const nbTirages = 100
const contexte = `${nbTirages} tirages et fréquence attendue entre 10% et 90% pour chaque position finale`

describe(`Avec ${contexte})`, () => {
  // pour le stub de la console
  let consoleErrorStub
  let consoleLogStub

  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
    consoleLogStub = sinon.stub(console, 'log')
  })
  afterEach(() => {
    consoleErrorStub.restore()
    consoleLogStub.restore()
  })

  it('j3pShuffle mélange un tableau de manière aléatoire', () => {
    const tabsTest = [
      ['zero', 'un', 'deux', 'trois', 'quatre'],
      ['un', false],
      [null, undefined, {}]
    ]
    tabsTest.forEach(tab => {
      const zeros = tab.map(() => 0) // un tableau de 0 de même longueur
      // pour chaque élément on regarde son nb d’apparition dans chaque position
      const cumuls = new Map() // idem un objet, dont les propriétés peuvent être n’importe quoi, y compris un objet ou null
      tab.forEach(elt => cumuls.set(elt, zeros))
      let i = 0
      while (i < nbTirages) {
        const result = j3pShuffle(tab)
        expect(Array.isArray(result)).to.be.true
        expect(result).to.have.length(tab.length)
        // on veut avoir tous les éléments (et pas un en double ou des null|undefined)
        expect(result.every(elt => tab.includes(elt)))
        // on note les positions de chacun
        result.forEach((elt, index) => {
          cumuls.get(elt)[index]++
        })
        i++
      }
      cumuls.entries(([elt, cumul]) => {
        // on devrait avoir en moyenne nbTirages / tab.length occurrences, on vérifie juste qu’on est entre 0.1 et 0.9 pour chacun
        cumul.forEach((nbOcc, pos) => {
          expect(nbOcc).to.be.above(nbTirages / 10, `${elt} est sorti ${nbOcc} fois en position ${pos} sur ${nbTirages}, ça parait peu`)
          expect(nbOcc).to.be.below(nbTirages / 10 * 9, `${elt} est sorti ${nbOcc} fois en position ${pos} sur ${nbTirages}, ça parait beaucoup`)
        })
      })
    })
    // tout ça sans rien dire en console
    expect(consoleErrorStub).to.not.have.been.called
    expect(consoleLogStub).to.not.have.been.called
  })

  it('j3pShuffle throw une erreur si on lui passe autre chose qu’un tableau', () => {
    expect(j3pShuffle).to.throw(Error, /argument invalide/i)
    const badFunctions = ['foo', null, new Date(), new Set(), 42].map(arg => j3pShuffle.bind(null, arg))
    badFunctions.forEach(badFunction => {
      expect(badFunction).to.throw(Error, /argument invalide/i)
    })
  })
  it('j3pShuffle retourne un tableau vide sans râler si on lui passe un tableau vide', () => {
    const result = j3pShuffle([])
    expect(Array.isArray(result)).to.be.true
    expect(result).to.have.length(0)
    // tout ça sans rien dire en console
    expect(consoleErrorStub).to.not.have.been.called
    expect(consoleLogStub).to.not.have.been.called
  })

  it('j3pShuffleMulti mélange deux tableaux de manière aléatoire mais identique', () => {
    const tabsTest = [
      // premier test avec deux tableaux
      [
        ['zero', 'un', 'deux', 'trois', 'quatre'],
        [0, 1, 2, 3, 4]
      ],
      // 2e test avec 4 tableaux
      [
        ['un', false, 2],
        [null, undefined, {}],
        [0, 1, 2],
        ['foo', 'bar', 'baz']
      ]
    ]
    tabsTest.forEach(tabs => {
      const tabRef = tabs[0]
      const tabRefSize = tabRef.length
      // un tableau de tableaux, cumuls[1][2] compte le nb de fois où l’item 1 de tabRef est sorti en position 2
      // (on ne compte que pour tabs[0] vu que ça doit être pareil pour les autres, ce qu’on vérifie par ailleurs)
      // retourne cumuls[eltIndex], un tableau de zéros
      const cumuls = tabRef.map(() => tabRef.map(() => 0))
      // on lance les tirages
      for (let tirage = 0; tirage < nbTirages; tirage++) {
        const resultTabs = j3pShuffleMulti(...tabs)
        // pour noter les positions de ce tirage
        const positions = []
        // autant de tableaux en sortie
        expect(resultTabs).to.have.length(tabs.length)
        // chacun avec les éléments du tableau d’entrée
        resultTabs.forEach((resultTab, indexTab) => {
          // même taille que l’entrée
          expect(resultTab).to.have.length(tabRefSize)
          // on parcours l’entrée
          const inputTab = tabs[indexTab]
          inputTab.forEach((inputElt, eltInputIndex) => {
            const resultIndex = resultTab.indexOf(inputElt)
            // tous les éléments en entrée se retrouvent en sortie
            expect(resultIndex).to.be.least(0) // ≥
            // on vérifie que toutes les positions sont identiques pour tous les tableaux de sortie
            if (indexTab === 0) {
              // init des positions de sortie de ce tirage
              positions[eltInputIndex] = resultIndex
              // on note la position de sortie de cet élément de tabRef pour nos probas
              cumuls[eltInputIndex][resultIndex]++
            } else {
              // on vérifie que c’est la même chose pour les tableaux suivants (tous mélangés pareil)
              expect(resultIndex).to.equal(positions[eltInputIndex])
            }
          })
        })
      } // fin des tirages

      // on vérifie que chaque cumul est entre 1 et nbTirages -1
      cumuls.forEach((cumul, eltIndex) => {
        cumul.forEach((nbOccurence, positionSortie) => {
          expect(nbOccurence).to.be.above(1, `${tabRef[eltIndex]} est sorti ${nbOccurence} fois en position ${positionSortie} sur ${nbTirages}, ça parait peu`)
          expect(nbOccurence).to.be.below(nbTirages - 1, `${tabRef[eltIndex]} est sorti ${nbOccurence} fois en position ${positionSortie} sur ${nbTirages}, ça parait beaucoup`)
        })
      }) // forEach cumul

      // c’est tout bon…
    }) // forEach tabs

    // tout ça sans rien dire en console
    expect(consoleErrorStub).to.not.have.been.called
    expect(consoleLogStub).to.not.have.been.called
  })

  it('j3pShuffleMulti throw une erreur si on lui passe autre chose qu’une liste de tableaux de même longueur', () => {
    const badCalls = [
      j3pShuffleMulti,
      j3pShuffleMulti.bind(null, 42),
      // j3pShuffleMulti.bind(null, 'foo', 'bar'),
      j3pShuffleMulti.bind(null, ['un', 'seul', 'tableau'])
      // j3pShuffleMulti.bind(null, ['deux', 'tableaux'], ['de', 'longueur', 'différente'])
    ]
    badCalls.forEach(badCall => {
      expect(badCall).to.throw(Error, /arguments invalides/i)
      expect(consoleErrorStub).to.have.been.calledOnce
      consoleErrorStub.resetHistory()
      expect(consoleLogStub).to.not.have.been.called
    })
  })
})
