import { describe, it, beforeEach, afterEach } from 'vitest'
import { expect, use } from 'chai'
import sinonChai from 'sinon-chai'
import sinon from 'sinon'

import { j3pClone } from 'src/legacy/core/functions'

use(sinonChai)

const sameValues = (src, dst) => {
  if (Array.isArray(src)) {
    return Array.isArray(dst) &&
      src.length === dst.length &&
      src.every((elt, i) => sameValues(elt, dst[i]))
  }
  if (typeof src === 'object') {
    if (src === null) return dst === null
    // il faut le même type
    return typeof dst === 'object' &&
      // on doit avoir les mêmes propriétés (mais pas forcément dans le même ordre,
      // donc on peut pas comparer les Object.keys, mais vu qu’on boucle ensuite
      // sur les valeurs il suffit de vérifier qu’il y a autant de propriétés)
      Object.keys(src).length === Object.keys(dst).length &&
      // pour chaque propriété c’est la même valeur
      Object.entries(src).every(([prop, value]) => sameValues(value, dst[prop]))
  }
  if (Number.isNaN(src)) {
    return Number.isNaN(dst)
  }
  return src === dst
}

describe('j3pClone', () => {
  // qq elts pour les tests
  function fct1 () {
    console.error(Error('ne devrait pas être appelé'))
  }
  const fct2 = () => console.error(Error('ne devrait pas être appelé non plus'))

  const getArray = () => ['foo', { a: 'foo', b: { c: 42 } }, fct1, fct2, '', null, undefined, true, false, 0, 42]
  const getObj = () => ({
    str: 'foo',
    strVide: '',
    boolTrue: true,
    boolFalse: false,
    undef: undefined,
    null: null,
    re: /foo/g,
    date: new Date(),
    nan: NaN,
    ar1: getArray(),
    obj1: {
      a: 'foo',
      infini: Number.POSITIVE_INFINITY,
      obj2: {
        a: 'bar',
        obj3: {

        }
      }
    },
    fct1,
    fct2
  })
  // pour le stub de la console
  let consoleErrorStub

  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    consoleErrorStub.restore()
  })

  it('retourne string|number|boolean|undefined|null inchangé', () => {
    ['', 'foo', 0, 1, -1, 42, Number.POSITIVE_INFINITY, null, undefined, true, false].forEach(v => {
      expect(j3pClone(v)).to.equals(v, `Pb avec ${v} (type ${typeof v})`)
      expect(consoleErrorStub).to.not.have.been.called
    })
  })
  it('NaN => NaN', () => {
    expect(Number.isNaN(j3pClone(NaN))).to.be.true
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('clone un Array (en profondeur)', () => {
    const src = getArray()
    const clone = j3pClone(src)
    expect(Array.isArray(clone)).to.be.true
    expect(clone.length).to.equal(src.length)
    src.forEach((srcElt, i) => {
      expect(sameValues(srcElt, clone[i])).to.be.true
    })
    // et on vérifie que c’est un vrai clone
    clone[1].a = 'bar'
    expect(src[1].a).to.equal('foo')
    // profond
    clone[1].b.c = 'bar'
    expect(src[1].b.c).to.equal(42)
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('Array noDeep => shallow copy', () => {
    const src = getArray()
    const clone = j3pClone(src, true)
    expect(Array.isArray(clone)).to.be.true
    expect(clone.length).to.equal(src.length)
    src.forEach((srcElt, i) => {
      expect(sameValues(srcElt, clone[i])).to.be.true
    })
    // ça clone au 1er niveau
    clone[0] = 'bar'
    expect(src[0]).to.equal('foo')
    // mais pas profond
    clone[1].a = 'bar'
    expect(src[1].a).to.equal('bar')
    clone[1].b.c = 'bar'
    expect(src[1].b.c).to.equal('bar')
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('Object deep clone', () => {
    const obj = getObj()
    const clone = j3pClone(obj)
    expect(sameValues(obj, clone)).to.be.true
    // ça clone
    clone.str = 'bar'
    expect(obj.str).to.equal('foo')
    expect(obj.str).to.equal('foo')
    // en profondeur
    clone.obj1.a = {}
    expect(obj.obj1.a).to.equal('foo')
    clone.obj1.obj2.a = 42
    expect(obj.obj1.obj2.a).to.equal('bar')
    expect(consoleErrorStub).to.not.have.been.called
    clone.fct1()
    expect(consoleErrorStub).to.have.been.calledOnce
    clone.fct2()
    expect(consoleErrorStub).to.have.been.calledTwice
  })
  it('Object noDeep => shallow copy', () => {
    const obj = getObj()
    const clone = j3pClone(obj, true)
    expect(sameValues(obj, clone)).to.be.true
    // clonage au 1er niveau
    clone.str = 'bar'
    expect(obj.str).to.equal('foo')
    // mais pas en profondeur
    clone.obj1.obj2.a = 42
    expect(obj.obj1.obj2.a).to.equal(42)
    clone.ar1[3] = 24
    expect(obj.ar1[3]).to.equal(24)
    clone.obj1.a = 'baz'
    expect(obj.obj1.a).to.equal('baz')
    clone.obj1 = {}
    expect(obj.obj1.a).to.equal('baz')
    expect(consoleErrorStub).to.not.have.been.called
  })
  it('Object deep clone retourne l’objet initial (et râle) en cas de ref circulaire', () => {
    const obj = {
      ssObj: {
        prop: 42
      }
    }
    // on crée une référence circulaire
    obj.ssObj.circ = obj
    expect(j3pClone(obj) === obj).to.be.true
    expect(consoleErrorStub).to.have.been.calledOnce
  })
})
