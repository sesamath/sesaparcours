Tests unitaires
===============

Cf également les tests fonctionnels dans testBrowser

Après le passage de webpack à vite, il y a eu pas mal de pbs avec mocha (imports avec ou sans extension, ts-node ou node avec --loader ts-node/esm), le passage à vitest a réglé les pbs
(Cf le merge d6880f227 du 2022-10-07)

Les tests unitaires plus longs ont le suffixe `.longtest.js` et sont joués à part (`pnpm test:long`)
