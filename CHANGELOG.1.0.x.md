# Journal des changements de sesaparcours (versions 1.0.x)

## version 1.0.70

`* `[a43839bd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a43839bd0) fix pb de disabled() => disable() dans le constructeur BulleAide dupliqué localement ici (2021-12-15 15:44) <Daniel Caillibaud>  
`*   `[4d6cb2f05](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d6cb2f05) Merge branch 'blindageRecursionMathquill' into 'main' (2021-12-15 15:33) <Yves Biton>  
`|\  `  
`| * `[e6be73f68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e6be73f68) ajout d'une croix sur les messages d'erreurs, avec aussi une option vanishAfter pour un délai d'autodestruction (2021-12-15 15:18) <Daniel Caillibaud>  
`| * `[096d40eaf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/096d40eaf) aj commentaires (2021-12-15 14:51) <Daniel Caillibaud>  
`| * `[594b3e4dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/594b3e4dd) blindage j3pValeurde (qui retourne une chaîne vide si la commande latex plante) (2021-12-15 14:46) <Daniel Caillibaud>  
`| * `[3cd94b8c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3cd94b8c0) simplification (2021-12-15 14:44) <Daniel Caillibaud>  
`| * `[11807a6ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11807a6ad) on compte plutôt le nb de childs (avec querySelectorAll, plus ligth qu'un appel de la commande latex) (2021-12-15 14:37) <Daniel Caillibaud>  
`| * `[880a38c6c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/880a38c6c) on vérifie le nb de commande et refuse d'en ajouter si y'en a déjà plus de 100 (2021-12-15 14:30) <Daniel Caillibaud>  
`* | `[d6926fbc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d6926fbc4) retag 1.0.70 (2021-12-15 12:51) <j3pOnDev>  
`* |   `[5c4a2ff4d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c4a2ff4d) Merge branch 'fixPbDialogWidth' into 'main' (2021-12-15 12:50) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[eec900f45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eec900f45) fix eslint et nettoyage (2021-12-15 11:45) <Daniel Caillibaud>  
`| * | `[9fd89a4bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9fd89a4bc) fix largeur du Dialog avec le tableau de conversion (2021-12-15 11:18) <Daniel Caillibaud>  
`| |/  `  
`* |   `[f439ec3eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f439ec3eb) Merge branch 'Correction_MathQuill' into 'main' (2021-12-15 12:50) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[cea240575](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cea240575) Rectification (et throw dans tous les cas à pb, comme avant) (2021-12-15 11:58) <Daniel Caillibaud>  
`| * | `[72374bd19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72374bd19) on continue à throw une erreur pour continuer à recevoir les rapports bugsnag (avec davantage d'info cette fois (2021-12-15 11:53) <Daniel Caillibaud>  
`| * | `[732685c49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/732685c49) Modification de mathquill suite à des rapports  BusNag arrivant tous sous Safari ou chrome OS remanié (2021-12-15 11:17) <Yves Biton>  
`| * | `[3f8e472f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f8e472f5) Modification de mathquill suite à des rapports  BusNag arrivant tous sous Safari ou chrome OS (2021-12-15 10:22) <Yves Biton>  
`| |/  `  
`* |   `[b6e09af9e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6e09af9e) Merge branch 'Correction_Squelette_Yves' into 'main' (2021-12-15 12:29) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[9bd4c846e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9bd4c846e) Correction d'appels incorrects de afficheTitre (2021-12-15 12:27) <Yves Biton>  
`|/  `  
`*   `[25b771275](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25b771275) Merge branch 'eslintForm22' into 'main' (2021-12-15 08:22) <Tommy Barroy>  
`|\  `  
`| * `[0bccf0207](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0bccf0207) bugsnag (2021-12-15 08:21) <Tommy>  
`* |   `[aaf8cd4ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aaf8cd4ff) Merge branch 'Correction_ex_Validation_Interne' into 'main' (2021-12-14 19:56) <Yves Biton>  
`|\ \  `  
`| * | `[b6b4189a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6b4189a4) Correction : pas d'éditeur mathquill (2021-12-14 19:53) <Yves Biton>  
`|/ /  `  
`* |   `[e71cac761](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e71cac761) Merge branch 'signeAffine' into 'main' (2021-12-14 18:13) <Rémi Deniaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[15c9cd14b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15c9cd14b) correction erreur paramètre signe_affine (2021-12-14 18:13) <Rémi Deniaud>  
`| * `[71156196e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/71156196e) nettoyage limites_exp2 (2021-12-13 23:02) <Rémi Deniaud>  
`| * `[ffd92a8e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffd92a8e1) correction focus sur une zone qui n'existait pas + gros nettoyage de signe_affine (2021-12-13 22:40) <Rémi Deniaud>  
`* |   `[8fe372db7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8fe372db7) Merge branch 'bugsnag02' into 'main' (2021-12-14 17:47) <Tommy Barroy>  
`|\ \  `  
`| * | `[1860e315f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1860e315f) bugsnag (2021-12-14 17:45) <Tommy>  
`* | |   `[55d01a76b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55d01a76b) Merge branch 'multiFixes' (2021-12-14 17:45) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[9a75e0c3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a75e0c3f) On remet le remplacement des <br> par des \n dans j3pAddContent, mais à la bonne place (2021-12-14 17:39) <Daniel Caillibaud>  
`| * | `[1ce7c575a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ce7c575a) fix bugsnag 61a9d86ae640de000727396d (2021-12-14 17:23) <Daniel Caillibaud>  
`* | |   `[7cdbe84eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7cdbe84eb) Merge branch 'bugsnag01' into 'main' (2021-12-14 17:30) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[230d8dfa5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/230d8dfa5) bugsnag (2021-12-14 17:29) <Tommy>  
`* | | |   `[40733935f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40733935f) Merge branch 'Correction_Squelette_Calc_Vect_Multi_Edit' into 'main' (2021-12-14 16:46) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[233c2f1ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/233c2f1ff) Correction du squelette : Apr-ès mes modifs la répoinse vecteur nul était refusée comme incorrecte... (2021-12-14 16:44) <Yves Biton>  
`* | | | | `[5aaec4ffb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5aaec4ffb) retag 1.0.69 (2021-12-14 16:40) <j3pOnDev>  
`|/ / / /  `  
`* | | |   `[1ff3e0fe5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ff3e0fe5) Merge branch 'cleanPoseOperation' into 'main' (2021-12-14 16:32) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[6c86e789c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c86e789c) aj de FIXME en commentaires (2021-12-14 09:32) <Daniel Caillibaud>  
`| * | | `[ea259e3cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea259e3cd) simplification (2021-12-14 09:30) <Daniel Caillibaud>  
`* | | |   `[0af5e86f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0af5e86f9) Merge branch 'multiFixes' into 'main' (2021-12-14 15:23) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| | |/ /  `  
`| |/| |   `  
`| * | | `[dd76d4f65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd76d4f65) amélioration génération du html (2021-12-14 15:21) <Daniel Caillibaud>  
`| * | | `[066aeace2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/066aeace2) amélioration afficheTitre qui génère son html proprement quand le titre est sur 2 lignes (et râle si y'en a plus de deux car seules les 2 premières sont visibles) (2021-12-14 15:16) <Daniel Caillibaud>  
`| * | | `[f5effd023](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5effd023) j3pGetPositionSourisZone virée car plus utilisée (2021-12-14 14:12) <Daniel Caillibaud>  
`| * | | `[b200b77f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b200b77f9) simplification du déplacement de la droite à la souris (plus besoin de j3pGetPositionSourisZone (2021-12-14 14:12) <Daniel Caillibaud>  
`| * | | `[a48722b55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a48722b55) j3pGetMousePosition viré (2021-12-14 13:40) <Daniel Caillibaud>  
`| * | | `[522ebe223](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/522ebe223) fix appels foireux de barre_espace dans les sections de videoprojection (bugsnag 61a745affc337600071829a0) (2021-12-14 13:25) <Daniel Caillibaud>  
`| * | | `[e8d0493a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8d0493a3) aj d'info pour les notifs bugsnag (cf https://app.bugsnag.com/sesamath/j3p/errors/61b0e8116b6afa000781e0f6) (2021-12-14 12:33) <Daniel Caillibaud>  
`| * | | `[bba071d36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bba071d36) simplification usage structure (bugsnag 61b1d67047f4d8000704e4cb) (2021-12-14 12:14) <Daniel Caillibaud>  
`| * | | `[77c7c753c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77c7c753c) fix https://app.bugsnag.com/sesamath/j3p/errors/61b3d8e54e4e1000071fed53 (2021-12-14 12:06) <Daniel Caillibaud>  
`| * | | `[b47edae0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b47edae0e) fix zIndex pour pouvoir mettre des ZoneStyleMathquill et des BulleAide dans des Dialog (2021-12-14 11:50) <Daniel Caillibaud>  
`| * | | `[06a58313c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06a58313c) nettoyage, mais trop de taf pour utiliser le BulleAide générique, on verra plus tard (2021-12-14 11:42) <Daniel Caillibaud>  
`* | | |   `[135375bda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/135375bda) Merge branch 'zzzzzz' into 'main' (2021-12-14 11:37) <Tommy Barroy>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[58831499d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58831499d) eslint (2021-12-14 11:35) <Tommy>  
`| * | | `[039222a04](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/039222a04) eslint (2021-12-14 11:34) <Tommy>  
`| * | | `[711a73295](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/711a73295) sauv (2021-12-13 20:55) <Tommy>  
`* | | | `[9ac576427](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ac576427) fix nom qui restait en 3e param dans new BulleAide (2021-12-14 10:46) <Daniel Caillibaud>  
`* | | | `[8bec73cde](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8bec73cde) retag 1.0.68 (2021-12-14 09:41) <j3pOnDev>  
`| |/ /  `  
`|/| |   `  
`* | |   `[a30c3885f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a30c3885f) Merge branch 'rere' into 'main' (2021-12-13 21:56) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[f8025b0ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8025b0ea) rustine, ca devrait plus bloquer, mais faudra que je repasse (2021-12-13 21:56) <Tommy>  
`|/ / /  `  
`* | |   `[7baddcf62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7baddcf62) Merge branch 'supportLAboPose' into 'main' (2021-12-13 21:43) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[f26483a0d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f26483a0d) rustine, ca devrait plus bloquer, mais faudra que je repasse (2021-12-13 21:43) <Tommy>  
`|/ / /  `  
`* | |   `[f49551253](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f49551253) Merge branch 'replaxceVirguleBien' into 'main' (2021-12-13 21:31) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[08444a588](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/08444a588) bugVirgSupport (2021-12-13 21:31) <Tommy>  
`| * | | `[0de23fdc3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0de23fdc3) bugVirgSupport (2021-12-13 21:30) <Tommy>  
`|/ / /  `  
`* | | `[4ac7729d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ac7729d1) hotfix point-virgule (2021-12-13 19:51) <Daniel Caillibaud>  
`* | |   `[49964161c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49964161c) Merge branch 'cleanBulleAide' into 'main' (2021-12-13 19:48) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[97ad698f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97ad698f6) rectif de forme (2021-12-13 19:48) <Daniel Caillibaud>  
`| * | | `[2389f9dbe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2389f9dbe) bulleAide/index.js => bulleAide/BulleAide.js pour harmoniser (2021-12-13 19:37) <Daniel Caillibaud>  
`| * | | `[0cb473b54](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0cb473b54) on vire le 3e param (nom) qui n'est pas utilisé (2021-12-13 19:37) <Daniel Caillibaud>  
`|/ / /  `  
`* | | `[a334e6fee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a334e6fee) retag 1.0.67 (2021-12-13 18:54) <j3pOnDev>  
`* | |   `[e3bb2390c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e3bb2390c) Merge branch 'fixLoadError' into 'main' (2021-12-13 18:54) <Daniel Caillibaud>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[4902e1baf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4902e1baf) fix props mises sur l'instance courante du parcours (2021-12-13 18:53) <Daniel Caillibaud>  
`| * | `[13cc7abfb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13cc7abfb) fix props mises sur l'instance courante du parcours (2021-12-13 18:51) <Daniel Caillibaud>  
`| * | `[bd6fad75d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd6fad75d) fix props mises sur l'instance courante du parcours (2021-12-13 18:48) <Daniel Caillibaud>  
`| * | `[93e70ed1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93e70ed1a) fix props mises sur l'instance courante du parcours (2021-12-13 18:46) <Daniel Caillibaud>  
`| * | `[a90a4951a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a90a4951a) fix props mises sur l'instance courante du parcours (2021-12-13 18:43) <Daniel Caillibaud>  
`| * | `[188a0fdfc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/188a0fdfc) fix props mises sur l'instance courante du parcours (2021-12-13 18:40) <Daniel Caillibaud>  
`| * | `[9464ee78e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9464ee78e) fix props mises sur l'instance courante du parcours (2021-12-13 18:31) <Daniel Caillibaud>  
`| * | `[f5758dd6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5758dd6e) fix props mises sur l'instance courante du parcours (2021-12-13 18:29) <Daniel Caillibaud>  
`| * | `[605be564d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/605be564d) fix props mises sur l'instance courante du parcours (2021-12-13 18:25) <Daniel Caillibaud>  
`| * | `[4cf81319e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4cf81319e) fix props mises sur l'instance courante du parcours (2021-12-13 18:22) <Daniel Caillibaud>  
`| * | `[cc9016151](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc9016151) fix dernier warning eslint (2021-12-13 18:20) <Daniel Caillibaud>  
`| * | `[cd0061299](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd0061299) fix props mises sur l'instance courante du parcours (2021-12-13 18:20) <Daniel Caillibaud>  
`| * | `[a626f665c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a626f665c) fix props mises sur l'instance courante du parcours (2021-12-13 18:17) <Daniel Caillibaud>  
`| * | `[4e3ed7754](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e3ed7754) fix props mises sur l'instance courante du parcours (2021-12-13 18:14) <Daniel Caillibaud>  
`| * | `[4d8f8b594](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d8f8b594) fix props mises sur l'instance courante du parcours (2021-12-13 18:14) <Daniel Caillibaud>  
`| * | `[b0f0272a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0f0272a9) fix props mises sur l'instance courante du parcours (2021-12-13 18:06) <Daniel Caillibaud>  
`| * | `[5689d487d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5689d487d) fix props mises sur l'instance courante du parcours (2021-12-13 18:05) <Daniel Caillibaud>  
`|/ /  `  
`* | `[bd1b22d46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd1b22d46) amélioration message erreur dans notif lorsqu'on trouve pas si la condition est réalisée (2021-12-13 18:01) <Daniel Caillibaud>  
`* | `[8ca330057](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8ca330057) fix jsdoc ({Array.<*>} pour remplacer {*[]} que jsdoc ne comprends pas) (2021-12-13 18:01) <Daniel Caillibaud>  
`* |   `[249ea7dc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/249ea7dc7) Merge branch 'Correction_Exercice_Produit_Scalaire' into 'main' (2021-12-13 16:33) <Yves Biton>  
`|\ \  `  
`| * | `[88d2397fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88d2397fc) Correction du squelette : Les boutons pour les éditeurs de formules n'aparaissaient pas quand l'éditeur prenait le focus. (2021-12-13 16:32) <Yves Biton>  
`| * | `[67c9256f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67c9256f7) Correction du squelette : Quand on faisait une faute de syntaxe dans un éditeur de calcul, le rouge ne disparaissait pas quand on continuait à taper au clavier. Quand on ne rentre qu'un poitn sous un vecteur, erreur de syntaxe incorrecte. Fichier annexe : Ajout de la touche puissance. (2021-12-13 16:10) <Yves Biton>  
`| |/  `  
`* | `[6ca4d1843](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ca4d1843) retag 1.0.66 (2021-12-13 15:47) <j3pOnDev>  
`* |   `[1b42b06cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b42b06cc) Merge branch 'Amelioration_Clavier_Virtuel' into 'main' (2021-12-13 15:47) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[4297479c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4297479c4) every not => not some (2021-12-13 15:46) <Daniel Caillibaud>  
`| * | `[d932a1892](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d932a1892) startWith => startsWith + aj commentaires (2021-12-13 15:24) <Daniel Caillibaud>  
`| * | `[b4a8f4317](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4a8f4317) rectif conditions du keydown listener du clavier virtuel (2021-12-13 15:06) <Daniel Caillibaud>  
`| * | `[baa18f05b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/baa18f05b) Améliorations du clavier virtuel via le keydownListener affecté au body : Il continue à réagir si c'est un bouton qui a le focus. Il ne réagit pas si l'éditeur mathquill est caché ou est dans u n élément du dom caché (on teste si offsetParet qui est null s'il est caché). (2021-12-13 13:58) <Yves Biton>  
`| |/  `  
`* |   `[38158be6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38158be6f) Merge branch 'Correction_Fichier_Annexe' into 'main' (2021-12-13 14:33) <Yves Biton>  
`|\ \  `  
`| * | `[27dd032f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27dd032f8) Suppression du bouton puissance pour ces exos (2021-12-13 14:32) <Yves Biton>  
`| |/  `  
`* / `[8d2432e6b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d2432e6b) hotfix disabled: function qui restait (2021-12-13 14:26) <Daniel Caillibaud>  
`|/  `  
`*   `[d82fea2e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d82fea2e3) Merge branch 'disabledBug' into 'main' (2021-12-13 10:32) <Tommy Barroy>  
`|\  `  
`| * `[ac07d52c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac07d52c7) bugsnag sur Disabled qui trainent (2021-12-13 10:32) <Tommy>  
`|/  `  
`*   `[f9181f689](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9181f689) Merge branch 'repareQuad02' into 'main' (2021-12-13 09:46) <Tommy Barroy>  
`|\  `  
`| * `[10bd5b42d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10bd5b42d) bugsnag (2021-12-13 09:45) <Tommy>  
`* |   `[14fc1abdf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14fc1abdf) Merge branch 'limites' into 'main' (2021-12-12 23:02) <Rémi Deniaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[513be94d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/513be94d0) nettoyage limites_exp1 (2021-12-12 23:01) <Rémi Deniaud>  
`| * `[0948e4af9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0948e4af9) nettoyage limites_ln_exp (2021-12-12 22:43) <Rémi Deniaud>  
`| * `[7421ac5b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7421ac5b4) nettoyage limites_ln2 (2021-12-12 22:37) <Rémi Deniaud>  
`| * `[c715d313c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c715d313c) nettoyage limites_ln1 (2021-12-12 22:20) <Rémi Deniaud>  
`* |   `[cbdf62921](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbdf62921) Merge branch 'eslintIneg' into 'main' (2021-12-12 22:42) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[8628b70be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8628b70be) eslint (2021-12-12 22:42) <Tommy>  
`|/  `  
`*   `[c0c167dca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0c167dca) Merge branch 'eslintRemp' into 'main' (2021-12-12 21:40) <Tommy Barroy>  
`|\  `  
`| * `[323224f42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/323224f42) eslint (2021-12-12 21:39) <Tommy>  
`* | `[f0361deb6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0361deb6) retag 1.0.65 (2021-12-12 18:16) <j3pOnDev>  
`* |   `[0c7b47525](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c7b47525) Merge branch 'produitscalaire' into 'main' (2021-12-12 18:16) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[bcb18b413](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bcb18b413) il restait un pb si la norme n'était pas simplifiée (2021-12-12 15:44) <Rémi Deniaud>  
`| * | `[e016af18e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e016af18e) j'avais oublié un ds.debug = true (2021-12-12 15:30) <Rémi Deniaud>  
`| * | `[d41ec58a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d41ec58a0) correct me.parcours dans produitscalaire + ajout clavier virtuel (2021-12-12 15:22) <Rémi Deniaud>  
`* | | `[d83476ddb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d83476ddb) l'héritage css fonctionne pas pour les input dans j3pAffiche, on remet comme avant, on impose l'héritage d'après les propriétés des parents (2021-12-12 16:56) <Daniel Caillibaud>  
`| |/  `  
`|/|   `  
`* |   `[005db221b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/005db221b) Merge branch 'eslintProg0101' into 'main' (2021-12-12 12:57) <Tommy Barroy>  
`|\ \  `  
`| * | `[9730188a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9730188a2) eslint (2021-12-12 12:56) <Tommy>  
`|/ /  `  
`* |   `[48122dbcf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48122dbcf) Merge branch 'bugsnagEcriChif' into 'main' (2021-12-11 22:04) <Tommy Barroy>  
`|\ \  `  
`| * | `[f42e41e09](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f42e41e09) bug (2021-12-11 22:04) <Tommy>  
`|/ /  `  
`* |   `[52da0dafd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52da0dafd) Merge branch 'fixBugDevelop' into 'main' (2021-12-11 21:29) <Tommy Barroy>  
`|\ \  `  
`| * | `[95f267f4f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95f267f4f) bug (2021-12-11 21:27) <Tommy>  
`|/ /  `  
`* |   `[ba3615517](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba3615517) Merge branch 'repereZoneCLik' into 'main' (2021-12-11 19:41) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[95335c72c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95335c72c) bug (2021-12-11 19:41) <Tommy>  
`* |   `[56eb9e00b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56eb9e00b) Merge branch 'sectionsComplexes' into 'main' (2021-12-11 18:25) <Rémi Deniaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[7778dcaa0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7778dcaa0) j'ai enlevé videLesZones dans 606Rb (2021-12-11 17:46) <Rémi Deniaud>  
`* |   `[1e2a74c5f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e2a74c5f) Merge branch 'eslintFacto' into 'main' (2021-12-11 15:38) <Tommy Barroy>  
`|\ \  `  
`| * | `[dc0ad2a28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc0ad2a28) eslint (2021-12-11 15:37) <Tommy>  
`|/ /  `  
`* |   `[a0629e6ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0629e6ef) Merge branch 'Correction_Squelette_Yves' into 'main' (2021-12-11 13:19) <Yves Biton>  
`|\ \  `  
`| * | `[d0dcb2141](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0dcb2141) Correction : Le nombre d'essais restants affiché au départ contenait des Nan (2021-12-11 13:18) <Yves Biton>  
`|/ /  `  
`* |   `[c4b3fca2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4b3fca2d) Merge branch 'repareEtik' into 'main' (2021-12-11 00:54) <Tommy Barroy>  
`|\ \  `  
`| * | `[c132ffbd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c132ffbd7) eslint (2021-12-11 00:53) <Tommy>  
`|/ /  `  
`* |   `[5c7db2ba9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c7db2ba9) Merge branch 'eslintGroPuis03' into 'main' (2021-12-10 22:03) <Tommy Barroy>  
`|\ \  `  
`| * \   `[890f75128](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/890f75128) Merge branch 'main' into eslintGroPuis03 (2021-12-10 20:18) <Daniel Caillibaud>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | | `[16f254ede](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16f254ede) hotfix droites_paralleles (un this.tab_fact_colinearite qui restait à la place du stor.tabFactColinearite) (2021-12-10 19:42) <Daniel Caillibaud>  
`* | |   `[087773caf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/087773caf) Merge branch 'sectionsComplexes' into 'main' (2021-12-10 19:18) <Rémi Deniaud>  
`|\ \ \  `  
`| | |/  `  
`| |/|   `  
`| * | `[4a5bea21a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a5bea21a) petit nettoyage de 630 (2021-12-10 19:15) <Rémi Deniaud>  
`| * | `[3311d25ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3311d25ff) petit nettoyage de 611 (2021-12-10 19:00) <Rémi Deniaud>  
`| * | `[b96726ee7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b96726ee7) petit nettoyage de 610 (2021-12-10 18:51) <Rémi Deniaud>  
`| * | `[4013ddb8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4013ddb8c) petit nettoyage de 600R (2021-12-10 18:45) <Rémi Deniaud>  
`| * | `[70a3a1738](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70a3a1738) petit nettoyage de 600 (2021-12-10 18:35) <Rémi Deniaud>  
`| * | `[49dc4abc1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49dc4abc1) gros nettoyage de 606Rb (2021-12-10 17:29) <Rémi Deniaud>  
`| * | `[9262d6d2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9262d6d2b) petit nettoyage de 606R (2021-12-10 17:00) <Rémi Deniaud>  
`| * | `[fa6696fbf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa6696fbf) petit nettoyage de 605R (2021-12-10 16:55) <Rémi Deniaud>  
`| * | `[f72045fd9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f72045fd9) petit nettoyage de 604 (2021-12-10 16:50) <Rémi Deniaud>  
`| * | `[482804b0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/482804b0f) petit nettoyage de 603 (2021-12-10 16:44) <Rémi Deniaud>  
`| * | `[870ba308e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/870ba308e) petit nettoyage de 602ter (2021-12-10 16:40) <Rémi Deniaud>  
`| * | `[987b7d7b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/987b7d7b7) petit nettoyage de 602somme (2021-12-10 16:34) <Rémi Deniaud>  
`| * | `[d3564bc49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3564bc49) petit nettoyage de 602quot (2021-12-10 16:22) <Rémi Deniaud>  
`| * | `[2ccfd79cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ccfd79cc) petit nettoyage de 602prod (2021-12-10 16:11) <Rémi Deniaud>  
`| * | `[e057ae740](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e057ae740) petit nettoyage de 602bis + clavier dans 602 (2021-12-10 16:04) <Rémi Deniaud>  
`| * | `[eabfe7581](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eabfe7581) petit nettoyage de 602 (2021-12-10 15:52) <Rémi Deniaud>  
`* | | `[a912cb76d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a912cb76d) hotfix j3pIsHtmlElement qui marchait pas pour les HTMLAnchorElement (2021-12-10 18:47) <Daniel Caillibaud>  
`* | |   `[2d6290a67](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d6290a67) Merge branch 'eslintDevel' into 'main' (2021-12-10 18:33) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[3de63c12b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3de63c12b) eslint (2021-12-10 18:28) <Tommy>  
`* | |   `[ca054fe06](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca054fe06) Merge branch 'clavierTaux' into 'main' (2021-12-10 15:32) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[95cf87364](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95cf87364) nettoyage de forme_adaptee terminé (2021-12-10 15:30) <Rémi Deniaud>  
`| * | | `[bb5e87849](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb5e87849) gros nettoyage forme_adaptee (2021-12-10 14:57) <Rémi Deniaud>  
`| * | | `[213f88e7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/213f88e7a) clavier virtuel dans tauxSuccessifs (2021-12-10 13:44) <Rémi Deniaud>  
`| |/ /  `  
`* | |   `[925ec1c62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/925ec1c62) Merge branch 'menuesModifs' into 'main' (2021-12-10 14:27) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[d3f18ae3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3f18ae3f) blindages divers (2021-12-10 13:44) <Daniel Caillibaud>  
`| * | `[6d303d20e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d303d20e) simplification (2021-12-10 13:38) <Daniel Caillibaud>  
`| * | `[c663dd84f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c663dd84f) fonction j3pUnselect virée car inutilisée (2021-12-10 13:28) <Daniel Caillibaud>  
`| * | `[6ca41429c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ca41429c) jsdoc (2021-12-10 12:05) <Daniel Caillibaud>  
`| * | `[bafef88f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bafef88f7) console.error manquant dans un catch (2021-12-10 12:04) <Daniel Caillibaud>  
`| | * `[688a9c4c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/688a9c4c6) rectif (pbs introduits au merge précédent ?) (2021-12-10 18:12) <Daniel Caillibaud>  
`| | * `[df03b0c51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df03b0c51) simplification du code (2021-12-10 18:12) <Daniel Caillibaud>  
`| | *   `[47711e95a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47711e95a) Merge branch 'main' into eslintGroPuis03 (2021-12-10 14:02) <Daniel Caillibaud>  
`| | |\  `  
`| |_|/  `  
`|/| |   `  
`* | |   `[fe9371af1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe9371af1) Merge branch 'corrigeMiseENeq' into 'main' (2021-12-10 12:54) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[ffeb0671a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffeb0671a) bug debut (2021-12-10 12:54) <Tommy>  
`|/ / /  `  
`* | | `[1094b2e25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1094b2e25) retag 1.0.64 (2021-12-10 10:18) <j3pOnDev>  
`* | |   `[09ac8d684](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09ac8d684) Merge branch 'eslintQuad03' into 'main' (2021-12-10 08:05) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[9b4db8cc9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b4db8cc9) eslint (2021-12-10 08:04) <Tommy>  
`|/ / /  `  
`* | |   `[fb70f2f62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb70f2f62) Merge branch 'eslintQuad02' into 'main' (2021-12-09 22:20) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[ad1d3e0ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad1d3e0ed) eslint (2021-12-09 22:19) <Tommy>  
`| * | | `[33de96b5b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33de96b5b) eslint (2021-12-09 22:19) <Tommy>  
`|/ / /  `  
`* | |   `[05782b584](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05782b584) Merge branch 'supportProg01' into 'main' (2021-12-09 21:23) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[2358701d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2358701d4) bug clavier + bug correction réduction avec ² (2021-12-09 21:23) <Tommy>  
`| * | | `[28ba83431](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28ba83431) bug clavier + bug correction réduction avec ² (2021-12-09 21:23) <Tommy>  
`|/ / /  `  
`* | |   `[82e5e001e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/82e5e001e) Merge branch 'suppportParaper' into 'main' (2021-12-09 18:49) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[bd7ef7038](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd7ef7038) notifiyencore (2021-12-09 18:46) <Tommy>  
`|/ / /  `  
`* | |   `[d394dcabc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d394dcabc) Merge branch 'notifyplus' into 'main' (2021-12-09 15:29) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[99ca0ad64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99ca0ad64) notifiyencore (2021-12-09 15:29) <Tommy>  
`|/ / /  `  
`* | |   `[b135517e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b135517e0) Merge branch 'AAA1' into 'main' (2021-12-09 15:09) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[6db6cf249](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6db6cf249) quad1eslint (2021-12-09 15:08) <Tommy>  
`| * | | `[659210c23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/659210c23) eslint deb (2021-12-08 21:24) <Tommy>  
`|/ / /  `  
`* | |   `[8df063ec7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8df063ec7) Merge branch 'eslintRefactSommeAlgebriqk' into 'main' (2021-12-08 15:22) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[d68c0969b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d68c0969b) eslint (2021-12-08 15:22) <Tommy>  
`|/ / /  `  
`* | |   `[2348e67a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2348e67a5) Merge branch 'eslintRelatifUneOp' into 'main' (2021-12-08 14:23) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[6bc4c6cb2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6bc4c6cb2) eslint (2021-12-08 14:18) <Tommy>  
`|/ / /  `  
`* | |   `[eea62b1bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eea62b1bf) Merge branch 'EslintSuite' into 'main' (2021-12-08 08:16) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[3a386eb94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a386eb94) eslint (2021-12-08 08:13) <Tommy>  
`|/ / /  `  
`* | |   `[a4dd112ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a4dd112ce) Merge branch 'eslintParent' into 'main' (2021-12-07 20:59) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[0027815a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0027815a2) eslint (2021-12-07 20:58) <Tommy>  
`|/ / /  `  
`* | |   `[db1188173](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db1188173) Merge branch 'rreEslintDist' into 'main' (2021-12-07 18:48) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[bb01fa83a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb01fa83a) eslint (2021-12-07 18:47) <Tommy>  
`|/ / /  `  
`* | |   `[fe188e246](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe188e246) Merge branch 'eslintDistet_reCompare' into 'main' (2021-12-07 18:21) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[ea00c0dd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea00c0dd0) eslint (2021-12-07 18:20) <Tommy>  
`|/ / /  `  
`* | |   `[c07837cef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c07837cef) Merge branch 'eslintCmpare' into 'main' (2021-12-07 16:08) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[e9747b4de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9747b4de) eslint (2021-12-07 16:04) <Tommy>  
`* | | |   `[ae7dea2a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae7dea2a6) Merge branch 'Correction_Fichiers_Annexes' into 'main' (2021-12-07 16:00) <Yves Biton>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[e93304e4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e93304e4c) Amélioration correction exercices sur les puissances. (2021-12-07 15:57) <Yves Biton>  
`|/ / /  `  
`* | |   `[33fc56941](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33fc56941) Merge branch 'eslintSmmeAngle' into 'main' (2021-12-07 12:29) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[8a9415ed8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a9415ed8) eslint (2021-12-07 12:27) <Tommy>  
`|/ / /  `  
`* | |   `[31021b4aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31021b4aa) Merge branch 'eslintStat02' into 'main' (2021-12-07 00:11) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[03fe012f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03fe012f2) eslint (2021-12-07 00:11) <Tommy>  
`|/ / /  `  
`* | |   `[043a89afe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/043a89afe) Merge branch 'AAAAAAAAAAAAAAAAAAAA' into 'main' (2021-12-06 23:57) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[d2d53efb7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2d53efb7) eslint (2021-12-06 23:56) <Tommy>  
`| * | `[9c538e850](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c538e850) replace contents par inline-table (2021-12-06 22:51) <Tommy>  
`| * | `[a6ff6370d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6ff6370d) eslint (2021-12-06 10:56) <Tommy>  
`| | *   `[4f64b94e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f64b94e2) Merge branch 'main' into eslintGroPuis03 (2021-12-06 18:35) <Daniel Caillibaud>  
`| | |\  `  
`| |_|/  `  
`|/| |   `  
`* | |   `[5b33ad71a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b33ad71a) Merge branch 'fixThPythagore' into 'main' (2021-12-06 09:34) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[9b0891932](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b0891932) correction du NaN qui venait de j3p.Repere mais qu'on ne voyait que sous Chrome, pas Firefox (2021-12-05 17:09) <Rémi Deniaud>  
`| * | `[2d4eec213](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d4eec213) fix erreurs au chargement sur la palette de bouton, reste un pb à la construction de la figure avec du NaN (qui ne semble pas bloquer l'affichage) (2021-12-05 15:17) <Daniel Caillibaud>  
`* | |   `[002d7678b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/002d7678b) Merge branch 'eslintPAtron01' into 'main' (2021-12-06 01:08) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[cd669c39c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd669c39c) eslint (2021-12-06 01:07) <Tommy>  
`|/ / /  `  
`* | |   `[2995ac8e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2995ac8e1) Merge branch 'eslintPrefixe' into 'main' (2021-12-05 23:56) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[e69164667](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e69164667) eslint (2021-12-05 23:55) <Tommy>  
`|/ / /  `  
`* | |   `[a8684ab92](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8684ab92) Merge branch 'eslintPropoGraoh' into 'main' (2021-12-05 23:22) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[3290d6373](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3290d6373) eslint (2021-12-05 23:21) <Tommy>  
`|/ / /  `  
`* | |   `[98f6cdfd3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98f6cdfd3) Merge branch 'suiteRepMalEcrite' into 'main' (2021-12-05 21:32) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[86c69f3d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86c69f3d9) contournement du pb de gestion de réponses d'élèves non prévues (2021-12-05 21:26) <Rémi Deniaud>  
`|/ / /  `  
`* | |   `[ac06f72be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac06f72be) Merge branch 'sectionsTaux' into 'main' (2021-12-05 17:46) <Rémi Deniaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[54b07907e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54b07907e) Je pense que c'était juste dû à un me.finEnonce. C'est modifié (2021-12-05 17:45) <Rémi Deniaud>  
`|/ /  `  
`* | `[302055e01](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/302055e01) getUsageRids vire les sections exo_mep et ancienexo_mep (2021-12-05 14:32) <Daniel Caillibaud>  
`* |   `[deead1ee6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/deead1ee6) Merge branch 'fixLoadingErrors' into 'main' (2021-12-05 14:20) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[cfb59afb3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfb59afb3) un peu de nettoyage + proprités mises sur le parcours courant viré (2021-12-05 14:19) <Daniel Caillibaud>  
`| * | `[a7b583728](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7b583728) un peu de nettoyage + proprités mises sur le parcours courant viré (2021-12-05 14:15) <Daniel Caillibaud>  
`| * | `[f8e7d846a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8e7d846a) un peu de nettoyage + proprités mises sur le parcours courant viré (2021-12-05 14:06) <Daniel Caillibaud>  
`| * | `[fce3f381c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fce3f381c) un peu de nettoyage + proprités mises sur le parcours courant viré (2021-12-05 14:03) <Daniel Caillibaud>  
`| * | `[1718fb9e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1718fb9e0) un peu de nettoyage + proprités mises sur le parcours courant viré (2021-12-05 13:58) <Daniel Caillibaud>  
`| * | `[33765b920](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33765b920) rectif du me.questionCourante (2021-12-05 13:53) <Daniel Caillibaud>  
`| * | `[197438dd4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/197438dd4) un peu de nettoyage + proprités mises sur le parcours courant viré (2021-12-05 13:51) <Daniel Caillibaud>  
`| * | `[a9561a9bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9561a9bf) jsdoc (2021-12-05 13:51) <Daniel Caillibaud>  
`| * | `[e569402b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e569402b0) un peu de nettoyage + proprités mises sur le parcours courant viré (2021-12-05 11:49) <Daniel Caillibaud>  
`| * | `[c9867fdf9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9867fdf9) un peu de nettoyage + proprités mises sur le parcours courant viré (2021-12-05 11:48) <Daniel Caillibaud>  
`| * | `[40cb33e0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40cb33e0c) fix ajout de vidéo pour la section 751 (il reste à remplacer tous les j3pAjouteVideo qui ne fonctionnent probablement plus) (2021-12-05 11:45) <Daniel Caillibaud>  
`| * | `[f0234a35f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0234a35f) un peu de nettoyage + proprités mises sur le parcours courant viré (2021-12-05 11:11) <Daniel Caillibaud>  
`| * | `[6a70a5b1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a70a5b1b) proprietés mises sur le parcours courant viré (2021-12-05 10:59) <Daniel Caillibaud>  
`| * | `[32740ffba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32740ffba) un peu de nettoyage + proprités mises sur le parcours courant viré (2021-12-05 10:42) <Daniel Caillibaud>  
`| * | `[90b409736](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90b409736) fix propriétés ajouté au parcours courant (2021-12-05 10:30) <Daniel Caillibaud>  
`| * | `[4edfc70fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4edfc70fe) mtgAppLecteur plus en propriété du parcours courant (2021-12-05 10:24) <Daniel Caillibaud>  
`| * | `[aa8da162c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa8da162c) fix stor.zoneConsX undefined + me.conteneur du modèle écrasé par la section + propriété zoneexplicationX ajouté par la section au parcours courant (2021-12-05 10:21) <Daniel Caillibaud>  
`| * | `[b919da575](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b919da575) fix stor.zoneConsX undefined (2021-12-05 10:16) <Daniel Caillibaud>  
`| * | `[3f49834ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f49834ab) fix xE non déclaré + tab_proba_secantes ajouté au parcours courant (2021-12-05 10:11) <Daniel Caillibaud>  
`| | * `[2e53f5324](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e53f5324) replace contents par inline-table (2021-12-06 15:51) <Tommy>  
`| | * `[4462287ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4462287ce) replace contents par inline-table (2021-12-06 15:49) <Tommy>  
`| | * `[785a35b78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/785a35b78) simplification et suppression des derniers warnings eslint (2021-12-06 13:54) <Daniel Caillibaud>  
`| | * `[5ae7b17a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ae7b17a1) spancontenant => spanContenant (2021-12-06 13:43) <Daniel Caillibaud>  
`| | * `[dda7a3723](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dda7a3723) simplification (id du span du contenu viré, propriété spanContenu ajoutée) (2021-12-06 13:42) <Daniel Caillibaud>  
`| | * `[fb9ba8496](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb9ba8496) simplification du rappel de la section dans le case correction (2021-12-06 13:20) <Daniel Caillibaud>  
`| | * `[1cf27cc3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1cf27cc3f) une inversion (2021-12-06 13:09) <Tommy>  
`| | * `[f524205e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f524205e2) remp desactive par disable (2021-12-06 13:07) <Tommy>  
`| | * `[da02eeaf9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da02eeaf9) console.log commenté (2021-12-06 12:52) <Daniel Caillibaud>  
`| | * `[777b1003d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/777b1003d) refacto de ListeDeroulante.prototype.creeComposant (simplification et factorisation de code, génération propre du html sans warning) (2021-12-06 12:51) <Daniel Caillibaud>  
`| | * `[cbad0edef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbad0edef) un disabled() qui restait (2021-12-06 12:19) <Daniel Caillibaud>  
`| | * `[ee942ccb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee942ccb8) fix import css (2021-12-06 12:14) <Daniel Caillibaud>  
`| | * `[3689b2faf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3689b2faf) on limite les modifs css mathquill au conteneur zoneStyleAffiche (2021-12-06 12:10) <Daniel Caillibaud>  
`| | * `[aa58aac0d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa58aac0d) modif de forme (2021-12-06 12:06) <Daniel Caillibaud>  
`| | * `[f82d1f63f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f82d1f63f) après avoir renomé disabled() en disable() on peut renommer disa en disabled (2021-12-06 11:59) <Daniel Caillibaud>  
`| | * `[4de9cf0ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4de9cf0ea) aj de commentaires sur les diplay: contents (2021-12-06 11:55) <Daniel Caillibaud>  
`| | * `[bfd5d0efc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bfd5d0efc) jsdoc et harmonisations des noms de méthode et propriétés (2021-12-06 11:46) <Daniel Caillibaud>  
`| | * `[d1c161793](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1c161793) ZoneStyleAffiche est l'export par défaut (pour harmoniser, un fichier js d'une classe l'exporte par défaut) (2021-12-06 10:39) <Daniel Caillibaud>  
`| | * `[cf851846e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf851846e) ZoneStyllAffiche => ZoneStyleAffiche (2021-12-06 10:37) <Daniel Caillibaud>  
`| | * `[475e8703f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/475e8703f) findPos marqué deprecated (il vaut mieux utiliser directement la méthode native getBoundingClientRect) (2021-12-06 10:29) <Daniel Caillibaud>  
`| | * `[2c218ad2c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c218ad2c) nettoyage et simplification, findPos viré, jsdoc ajouté partout (2021-12-06 10:28) <Daniel Caillibaud>  
`| | * `[82a3c50b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/82a3c50b1) harmonisation listeners (2021-12-06 09:58) <Daniel Caillibaud>  
`| | * `[bb69de5a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb69de5a7) eslint (2021-12-05 22:37) <Tommy>  
`| | * `[772d734bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/772d734bd) eslint (2021-12-05 22:36) <Tommy>  
`| |/  `  
`|/|   `  
`* |   `[12f4b1ac3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12f4b1ac3) Merge branch 'eslintPuis02' into 'main' (2021-12-05 13:37) <Tommy Barroy>  
`|\ \  `  
`| * | `[cf4c06095](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf4c06095) eslint (2021-12-05 13:36) <Tommy>  
`|/ /  `  
`* |   `[a3750eb9c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3750eb9c) Merge branch 'eslintPuis01' into 'main' (2021-12-05 10:11) <Tommy Barroy>  
`|\ \  `  
`| * | `[b72a8d8fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b72a8d8fa) eslint (2021-12-05 10:10) <Tommy>  
`* | |   `[90568367f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90568367f) Merge branch 'fixLoadingErrors' into 'main' (2021-12-04 16:27) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| /   `  
`| |/    `  
`| * `[b74bc3b3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b74bc3b3f) fix de propriétés ajoutées à l'instance courante de parcours (qui provoquent des console.error au chargement) + fixes eslint (2021-12-04 16:26) <Daniel Caillibaud>  
`| * `[4710a99e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4710a99e4) fix message de réponse manquante qui n'était pas affiché (2021-12-04 16:13) <Daniel Caillibaud>  
`* |   `[c22cabcfe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c22cabcfe) Merge branch 'eslintRecipPyth' into 'main' (2021-12-04 15:39) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[f6e1de236](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6e1de236) eslint (2021-12-04 15:36) <Tommy>  
`|/  `  
`*   `[25a5efd5c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25a5efd5c) Merge branch 'revertForDonneesParcours' into 'main' (2021-12-04 14:02) <Daniel Caillibaud>  
`|\  `  
`| * `[765795133](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/765795133) On remet le sec = this['Section' + nomSection] pour les section qui s'en servent pour exporter donnees_parcours (2021-12-04 14:01) <Daniel Caillibaud>  
`|/  `  
`*   `[b42138c9b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b42138c9b) Merge branch 'eslintThales01' into 'main' (2021-12-04 12:38) <Tommy Barroy>  
`|\  `  
`| * `[14ddcef8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14ddcef8d) eslint (2021-12-04 12:35) <Tommy>  
`|/  `  
`*   `[62687fa11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62687fa11) Merge branch 'cleanParcoursProps' into 'main' (2021-12-04 11:55) <Daniel Caillibaud>  
`|\  `  
`| * `[8767d4775](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8767d4775) autres usages de j3p['Section' + nomSection] virés (avec qq fixes eslint au passage) (2021-12-04 11:49) <Daniel Caillibaud>  
`| * `[656fb698d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/656fb698d) me['Section' + nomSection] virés (2021-12-04 11:29) <Daniel Caillibaud>  
`| * `[8e637e868](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e637e868) fix eslint errors (litteral regexp, usage du testIntervalleFermeEntiers générique) (2021-12-04 11:29) <Daniel Caillibaud>  
`| * `[f728b7be8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f728b7be8) fix eslint errors (litteral regexp & j3p) (2021-12-04 11:29) <Daniel Caillibaud>  
`| * `[16a7d85c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16a7d85c2) fix eslint errors (litteral regexp) (2021-12-04 11:29) <Daniel Caillibaud>  
`| * `[1f2dc2817](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f2dc2817) me.repere virés (2021-12-04 11:29) <Daniel Caillibaud>  
`| * `[39f8df0e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39f8df0e4) utilise le testIntervalleFermeEntiers générique (et supprime une erreur eslint au passage) (2021-12-04 11:29) <Daniel Caillibaud>  
`|/  `  
`*   `[5c339d579](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c339d579) Merge branch 'eslintTriEg01' into 'main' (2021-12-03 22:12) <Tommy Barroy>  
`|\  `  
`| * `[6611dc98f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6611dc98f) eslint (2021-12-03 22:11) <Tommy>  
`|/  `  
`*   `[846b1f639](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/846b1f639) Merge branch 'refactoBrouillonCalculs' into 'main' (2021-12-03 22:06) <Tommy Barroy>  
`|\  `  
`| * `[a2d0b4bf3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2d0b4bf3) on voit un peu mieux quand on a cliqué, mais ca reste difficile (2021-12-03 22:06) <Tommy>  
`| * `[49f4e3842](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49f4e3842) on voit un peu mieux quand on a cliqué, mais ca reste difficile (2021-12-03 17:23) <Tommy>  
`| * `[97decd535](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97decd535) css pour TableauConversionMobile sortis de zoneStyleMathquill et mis dans un tableauConversionMobile.scss (2021-12-03 14:23) <Daniel Caillibaud>  
`| * `[9b9396257](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b9396257) nettoyage TableauConversionMobile (on remet this à sa place) et addDefaultTableVerbose => addDefaultTableDetailed avec harmonisation des noms des propriétés de l'objet retourné (2021-12-03 14:18) <Daniel Caillibaud>  
`| * `[eec2888b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eec2888b3) styles de brouillonCalculs mis dans leur propre feuille de style (et plus dans zoneStyleMathquill.scss) (2021-12-03 13:32) <Daniel Caillibaud>  
`| * `[14a3f1a36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14a3f1a36) ClavSpecial => hasClavierSpecial (2021-12-03 13:23) <Daniel Caillibaud>  
`| * `[ab2e39433](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab2e39433) listeitem => items (2021-12-03 13:11) <Daniel Caillibaud>  
`| * `[f97f8fb86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f97f8fb86) refacto BrouillonCalculs, avec 2 arguments elts et params + vraie classe avec méthodes (et plus des fonctions ajoutées à chaque instance par le constructeur) (2021-12-03 13:10) <Daniel Caillibaud>  
`* |   `[31ad476cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31ad476cd) Merge branch 'supportNomme02' into 'main' (2021-12-03 16:58) <Tommy Barroy>  
`|\ \  `  
`| * | `[4cfc9bebf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4cfc9bebf) on voit un peu mieux quand on a cliqué, mais ca reste difficile (2021-12-03 16:58) <Tommy>  
`| * | `[8613493a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8613493a4) on voit un peu mieux quand on a cliqué, mais ca reste difficile (2021-12-03 16:57) <Tommy>  
`* | | `[a5832860e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5832860e) retag 1.0.63 (2021-12-03 16:22) <j3pOnDev>  
`|/ /  `  
`* |   `[8efb7fd5b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8efb7fd5b) Merge branch 'eslint' into 'main' (2021-12-03 16:15) <Tommy Barroy>  
`|\ \  `  
`| * | `[c3be07aa1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c3be07aa1) eslint (2021-12-03 16:15) <Tommy>  
`* | | `[7020d3065](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7020d3065) Merge branch 'eslint' into 'main' (2021-12-03 16:14) <Tommy Barroy>  
`|\| | `  
`| * | `[7b577af3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b577af3a) eslint (2021-12-03 16:12) <Tommy>  
`|/ /  `  
`* |   `[df0b89c60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df0b89c60) Merge branch 'supportAngle02' into 'main' (2021-12-03 15:31) <Tommy Barroy>  
`|\ \  `  
`| * | `[34341b7fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34341b7fc) pour support sens liste (2021-12-03 15:31) <Tommy>  
`|/ /  `  
`* |   `[390ce6d68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/390ce6d68) Merge branch 'eslintEgTri03' into 'main' (2021-12-03 13:11) <Tommy Barroy>  
`|\ \  `  
`| * | `[e3890641f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e3890641f) eslint (2021-12-03 13:11) <Tommy>  
`|/ /  `  
`* |   `[3437ee05f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3437ee05f) Merge branch 'eslintTriEg04' into 'main' (2021-12-03 12:53) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[5891e6b78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5891e6b78) eslint (2021-12-03 12:52) <Tommy>  
`* | `[1d4f473ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d4f473ac) normalisation crlf => lf (2021-12-03 11:54) <Daniel Caillibaud>  
`* | `[738505b95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/738505b95) extension passée en minuscules (2021-12-03 11:05) <Daniel Caillibaud>  
`* | `[bee0b4572](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bee0b4572) fichiers php virés (2021-12-03 11:01) <Daniel Caillibaud>  
`* | `[6b47a5362](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b47a5362) renommage des fichiers *.PNG en .png, mais ils ne semblent pas utilisés (2021-12-03 11:00) <Daniel Caillibaud>  
`* | `[782d28440](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/782d28440) aj d'extensions binaires (2021-12-03 10:58) <Daniel Caillibaud>  
`* | `[9d5538936](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d5538936) aj .gitattributes pour normaliser les fins de ligne en LF (plus de CRLF) (2021-12-03 10:45) <Daniel Caillibaud>  
`* | `[0fc3c8a7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0fc3c8a7f) harmonisation nommage ccorrec et colorCorrec => colorCorrection + BulleAide passé en LF (et plus CRLF) (2021-12-03 10:36) <Daniel Caillibaud>  
`* |   `[5ed88494b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ed88494b) Merge branch 'master' (2021-12-03 10:27) <Daniel Caillibaud>  
`|\ \  `  
`| * \   `[1a9cd1327](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a9cd1327) Merge branch 'Zindex_Legacy_zmq' into 'master' (2021-12-02 09:45) <Daniel Caillibaud>  
`| |\ \  `  
`* | \ \   `[5a414dcc5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a414dcc5) Merge branch 'Zindex_Legacy_zmq' into 'master' (2021-12-03 09:37) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| | |/ /  `  
`| |/| |   `  
`| * | | `[7a561559b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a561559b) eslint fixes (2021-11-29 18:35) <Daniel Caillibaud>  
`| * | |   `[e5f00d121](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5f00d121) Merge branch 'main' into Zindex_Legacy_zmq (2021-11-29 18:31) <Daniel Caillibaud>  
`| |\ \ \  `  
`| * | | | `[027e4cc08](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/027e4cc08) aj rmq FIXME (2021-11-27 10:04) <Daniel Caillibaud>  
`| * | | | `[89ab46717](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89ab46717) zIndex doit rester entre 50 et 80 (2021-11-27 09:59) <Daniel Caillibaud>  
`| * | | | `[5d5399812](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d5399812) eslint (2021-11-27 00:59) <Tommy>  
`|  / / /  `  
`* | | | `[b1165e640](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1165e640) retag 1.0.62 (2021-12-03 08:49) <j3pOnDev>  
`* | | |   `[4c4d6e9cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c4d6e9cb) Merge branch 'eslintTriEg04' into 'main' (2021-12-03 08:39) <Tommy Barroy>  
`|\ \ \ \  `  
`| | |_|/  `  
`| |/| |   `  
`| * | | `[9132f1d62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9132f1d62) eslint (2021-12-03 08:38) <Tommy>  
`* | | |   `[12944b043](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12944b043) Merge branch 'bugsnagPoint' into 'main' (2021-12-02 21:34) <Tommy Barroy>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[9a0c88c84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a0c88c84) bugsnag (2021-12-02 21:34) <Tommy>  
`|/ / /  `  
`* | |   `[3c560c80a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c560c80a) Merge branch 'bugsnagNommeAngle' into 'main' (2021-12-02 21:32) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[29b18c30b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/29b18c30b) bugsnag (2021-12-02 21:32) <Tommy>  
`|/ / /  `  
`* | |   `[d727ebcdd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d727ebcdd) Merge branch 'eslintSemb02' into 'main' (2021-12-02 21:28) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[053b8aec5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/053b8aec5) id du canvas viré (2021-12-02 19:02) <Daniel Caillibaud>  
`| * | | `[032accd74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/032accd74) eslint (2021-12-02 12:12) <Tommy>  
`* | | |   `[da9f6aa28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da9f6aa28) Merge branch 'labmepPropo' into 'main' (2021-12-02 21:26) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[8d6de2f5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d6de2f5a) simplification du code, style comme objet et plus une grande string (2021-12-02 19:46) <Daniel Caillibaud>  
`* | | | |   `[bd5a1c1c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd5a1c1c9) Merge branch 'supportLaboFractions' into 'main' (2021-12-02 21:21) <Tommy Barroy>  
`|\ \ \ \ \  `  
`| * | | | | `[840f5de80](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/840f5de80) supportLAbo (2021-12-02 21:20) <Tommy>  
`|/ / / / /  `  
`* | | | |   `[190bae906](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/190bae906) Merge branch 'eslintsemB01' into 'main' (2021-12-02 19:55) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[d724bfd15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d724bfd15) eslint (2021-12-02 15:12) <Tommy>  
`| |/ / / /  `  
`| * / / / `[c4aaac837](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4aaac837) labomep - demande (2021-12-02 12:27) <Tommy>  
`| |/ / /  `  
`* | | |   `[49cfac028](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49cfac028) Merge branch 'bugsnagDem01' into 'main' (2021-12-02 18:37) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[113000962](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/113000962) un bugsnag (2021-12-01 22:21) <Tommy>  
`| |/ / /  `  
`* | | |   `[f869a333e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f869a333e) Merge branch 'bugnsagSembl04' into 'main' (2021-12-02 18:36) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[70a5e8c95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70a5e8c95) un bugsnag a cause d un import (2021-12-01 21:42) <Tommy>  
`| |/ / /  `  
`* | | |   `[e5270510c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5270510c) Merge branch 'BuGsNaGpyth01' into 'main' (2021-12-02 18:35) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[29c3419d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/29c3419d5) un bugsnag (2021-12-01 21:33) <Tommy>  
`| |/ / /  `  
`* | | |   `[b4e01f8da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4e01f8da) Merge branch 'bugSangCons' into 'main' (2021-12-02 18:30) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[6c76a3f40](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c76a3f40) un bugsnag (2021-12-01 21:22) <Tommy>  
`|/ / /  `  
`* | |   `[107ed4127](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/107ed4127) Merge branch 'master' (2021-12-01 15:06) <Daniel Caillibaud>  
`|\ \ \  `  
`| | |/  `  
`| |/|   `  
`| * |   `[86affe458](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86affe458) Merge branch 'nettoyageGestionParametres' into 'master' (2021-12-01 10:34) <Daniel Caillibaud>  
`| |\ \  `  
`| | * | `[3c695526c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c695526c) les regex acceptent aussi -0 pour les entiers (2021-12-01 11:31) <Daniel Caillibaud>  
`| | * | `[9ebbf5ae4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ebbf5ae4) aj dans les tests des cas qui ne fonctionnaient pas et que Rémi a réglé précédemment (les -0.xx) (2021-11-29 19:07) <Daniel Caillibaud>  
`| | * | `[c78f4b89c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c78f4b89c) modif d'un test sur les regExp des intervalles (2021-11-27 14:30) <Rémi Deniaud>  
`| | * | `[88c28e555](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88c28e555) modif des regExp pour les décimaux dans les intervalles (2021-11-27 14:09) <Rémi Deniaud>  
`| | * | `[f52caacb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f52caacb8) nettoyage genereAlea, utilisation des regexp génériques testées (2021-11-26 19:58) <Daniel Caillibaud>  
`* | | |   `[4386eea1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4386eea1a) Merge branch 'Correction_Squlettes_Yves' into 'main' (2021-12-01 15:05) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[5e607e465](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e607e465) Correction squelettes et sections Yves suite aux tests en dev (2021-12-01 12:09) <Yves Biton>  
`| * | | |   `[13c958ec4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13c958ec4) Merge branch 'widehatSvg' into 'master' (2021-12-01 10:11) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`* | \ \ \ \   `[bee8ae924](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bee8ae924) Merge branch 'master' (2021-12-01 11:10) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | / / /   `  
`| | |/ / /    `  
`| |/| | |     `  
`| * | | | `[a713bb938](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a713bb938) un peu de nettoyage (2021-12-01 11:09) <Daniel Caillibaud>  
`| * | | | `[89befec92](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89befec92) retag 1.0.61 (2021-12-01 11:09) <j3pOnDev>  
`| * | | | `[5fa32e4fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5fa32e4fb) Merge branch 'widehatSvg' into 'master' (2021-12-01 08:24) <Daniel Caillibaud>  
`| |\| | | `  
`| | * | | `[d2ad63adb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2ad63adb) plus besoin du svg à part car il est intégré dans le dom en js (2021-11-30 20:38) <Daniel Caillibaud>  
`| | * | | `[13b6de1c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13b6de1c1) on ajoute le svg dans le dom (plutôt qu'en background) pour qu'il hérite de la couleur courante (2021-11-30 20:36) <Daniel Caillibaud>  
`| | * | | `[b8342f5cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8342f5cf) deux traits remplacé par un path pour avoir deux parallélogrammes (bords exterieurs bien verticaux) (2021-11-30 18:49) <Daniel Caillibaud>  
`| | * | | `[d016774c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d016774c2) manquait le svg dans le commit précédent (2021-11-30 17:22) <Daniel Caillibaud>  
`| | * | | `[46ea7ab19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46ea7ab19) on simplifie le widehat avec un svg à 100% de largeur et 0.4 caractère de hauteur (ça suivra fontSize, sans avoir de calcul en js) (2021-11-30 17:15) <Daniel Caillibaud>  
`* | | | | `[c59d6ad16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c59d6ad16) retag 1.0.61 (2021-12-01 09:22) <j3pOnDev>  
`|/ / / /  `  
`* | | |   `[cb1146a47](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb1146a47) Merge branch 'fixImportFoireux' (2021-11-30 23:33) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[ed71bf8c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed71bf8c1) fix imports foireux (2021-11-30 23:33) <Daniel Caillibaud>  
`* | | | | `[99ce84425](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99ce84425) Merge branch 'master' into externalisationCss (2021-11-30 23:27) <Daniel Caillibaud>  
`|\| | | | `  
`| * | | |   `[4a2c0d9ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a2c0d9ef) Merge branch 'externalisationCss' into 'master' (2021-11-30 19:48) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`| * \ \ \ \   `[c50a6c24c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c50a6c24c) Merge branch 'main' (2021-11-30 19:16) <Daniel Caillibaud>  
`| |\ \ \ \ \  `  
`| * \ \ \ \ \   `[25fd4faac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25fd4faac) Merge branch 'modifcss_affichefrac' into 'master' (2021-11-30 15:30) <barroy tommy>  
`| |\ \ \ \ \ \  `  
`| | * | | | | | `[f94e194f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f94e194f7) pour centrer dans affichefrac (2021-11-30 16:29) <Tommy>  
`| |/ / / / / /  `  
`| * | | | | |   `[70e7443db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70e7443db) Merge branch 'eslintPyth' into 'master' (2021-11-30 15:23) <barroy tommy>  
`| |\ \ \ \ \ \  `  
`| | |_|_|/ / /  `  
`| |/| | | | |   `  
`| | * | | | | `[e321894c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e321894c3) eslint (2021-11-30 16:22) <Tommy>  
`| |/ / / / /  `  
`* | | | | |   `[a1db42b4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1db42b4b) Merge branch 'main' into externalisationCss (2021-11-30 20:50) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| | |/ / / /  `  
`| |/| / / /   `  
`| |_|/ / /    `  
`|/| | | |     `  
`| * | | | `[b05a78214](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b05a78214) fix repere mis en propriété du parcours courant (alors qu'on le réutilise pas) (2021-11-30 19:15) <Daniel Caillibaud>  
`| * | | | `[6034b8f5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6034b8f5a) fix méthodes de Repere qui n'avaient pas suivi lors de l'externalisation (2021-11-30 19:14) <Daniel Caillibaud>  
`| |/ / /  `  
`| * | |   `[2b343224d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b343224d) Merge branch 'eslintRepere01' into 'master' (2021-11-30 12:58) <barroy tommy>  
`| |\ \ \  `  
`| | * | | `[b433aa23b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b433aa23b) eslint (2021-11-30 13:57) <Tommy>  
`| |/ / /  `  
`| * | |   `[d4583bb79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4583bb79) Merge branch 'eslintConserve' into 'master' (2021-11-30 11:40) <barroy tommy>  
`| |\ \ \  `  
`| | * | | `[a6342b127](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6342b127) eslint (2021-11-30 12:39) <Tommy>  
`| |/ / /  `  
`| * | |   `[5902f301c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5902f301c) Merge branch 'InitDom_Apres_Chargement_mtg32' into 'master' (2021-11-30 10:38) <Yves Biton>  
`| |\ \ \  `  
`| | * | | `[5f8b11b78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f8b11b78) Appel de InitDom dans initMtg quand c'est possible. (2021-11-30 10:38) <Yves Biton>  
`| |/ / /  `  
`| * | |   `[812a244b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/812a244b7) Merge branch 'elsintSembl03' into 'master' (2021-11-29 21:59) <barroy tommy>  
`| |\ \ \  `  
`| | * | | `[60ce33fa0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/60ce33fa0) eslint (2021-11-29 22:57) <Tommy>  
`| | * | |   `[d8a8a45ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8a8a45ae) Merge branch 'master' into 'elsintSembl03' (2021-11-29 21:52) <barroy tommy>  
`| | |\ \ \  `  
`| | |/ / /  `  
`| |/| | |   `  
`| | * | | `[00ec0419c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00ec0419c) eslint (2021-11-29 22:49) <Tommy>  
`| | * | | `[b043a9b08](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b043a9b08) eslint (2021-11-29 12:57) <Tommy>  
`* | | | | `[2e68df54a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e68df54a) Merge branch 'main' into externalisationCss (2021-11-29 20:09) <Daniel Caillibaud>  
`|\| | | | `  
`| * | | |   `[8225ed0bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8225ed0bd) Merge branch 'harmonisationClasseExporteeParDefaut' into 'master' (2021-11-29 17:51) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`| | |_|_|/  `  
`| |/| | |   `  
`| | * | | `[3c22aefda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c22aefda) fix imports de TableauConversionMobile (pas faits par défaut) (2021-11-29 18:50) <Daniel Caillibaud>  
`| | * | | `[c791ba1b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c791ba1b7) espace en trop viré (eslint --fix, bizarre qu'il n'ait pas été passé automatiquement lors du dernier commit de ce fichier) (2021-11-29 18:47) <Daniel Caillibaud>  
`| | * | | `[0e0476d86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e0476d86) imports inutiles virés (2021-11-29 18:46) <Daniel Caillibaud>  
`| | * | | `[44b267f51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44b267f51) aj imports manquants, fix eslint errors (reste des warnings) (2021-11-29 18:46) <Daniel Caillibaud>  
`| | * | |   `[5d6d9354b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d6d9354b) Merge branch 'main' into harmonisationClasseExporteeParDefaut (2021-11-29 18:42) <Daniel Caillibaud>  
`| | |\ \ \  `  
`| | |/ / /  `  
`| |/| | /   `  
`| | | |/    `  
`| | |/|     `  
`| | * | `[312c58bce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/312c58bce) GenereAlea => genereAlea car ce n’est pas un constructeur (2021-11-26 19:11) <Daniel Caillibaud>  
`| | * | `[6d8fb32d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d8fb32d4) harmonisation export par défaut pour Zoneclick (2021-11-26 19:05) <Daniel Caillibaud>  
`| | * | `[90296e912](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90296e912) harmonisation export par défaut pour ListeDeroulante (2021-11-26 19:02) <Daniel Caillibaud>  
`| | * | `[12352619d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12352619d) harmonisation export par défaut pour BrouillonCalculs Repere TableauConversionMobile ZoneFormuleStyleMathquill ZoneFormuleStyleMathquill2 ZoneFormuleStyleMathquill3 (2021-11-26 19:00) <Daniel Caillibaud>  
`* | | | `[e58fdc2f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e58fdc2f3) Merge branch 'main' into externalisationCss (2021-11-29 20:04) <Daniel Caillibaud>  
`|\| | | `  
`| * | |   `[af26668f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af26668f6) Merge branch 'eslintSembl04' into 'master' (2021-11-29 11:58) <barroy tommy>  
`| |\ \ \  `  
`| | |_|/  `  
`| |/| |   `  
`| | * | `[88fe3f0cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88fe3f0cf) bugsnag (2021-11-29 10:34) <Tommy>  
`| |/ /  `  
`| * |   `[5de74a145](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5de74a145) Merge branch 'bugsnagEtik' into 'master' (2021-11-28 22:44) <barroy tommy>  
`| |\ \  `  
`| | * | `[62d5533f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62d5533f4) bugsnag (2021-11-28 23:43) <Tommy>  
`| |/ /  `  
`| * |   `[c6158380f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6158380f) Merge branch 'bugsnagStata' into 'master' (2021-11-28 22:29) <barroy tommy>  
`| |\ \  `  
`| | * | `[69cfebaf0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69cfebaf0) bugsnag2 (2021-11-28 23:28) <Tommy>  
`| |/ /  `  
`| * |   `[d204565b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d204565b3) Merge branch 'bugsnagBienEc' into 'master' (2021-11-28 22:16) <barroy tommy>  
`| |\ \  `  
`| | * | `[ff2c70ca1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff2c70ca1) bugsnag2 (2021-11-28 23:16) <Tommy>  
`| | * | `[53d2badc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53d2badc4) bugsnag (2021-11-28 23:15) <Tommy>  
`| |/ /  `  
`| * |   `[ab2fbafb2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab2fbafb2) Merge branch 'bigsnagMiseenEq' into 'master' (2021-11-28 21:59) <barroy tommy>  
`| |\ \  `  
`| | * | `[4e7a1eb81](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e7a1eb81) bugsnag (2021-11-28 22:58) <Tommy>  
`| |/ /  `  
`| * |   `[4fef76683](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fef76683) Merge branch 'eslintConBase' into 'master' (2021-11-28 21:18) <barroy tommy>  
`| |\ \  `  
`| | * | `[608cb7f17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/608cb7f17) eslint remplace tabconfix par tabconvmobile modif taconvmobil pour cette section (2021-11-28 22:17) <Tommy>  
`| |/ /  `  
`| * |   `[1307cb37f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1307cb37f) Merge branch 'conv_et_valap' into 'master' (2021-11-27 21:22) <barroy tommy>  
`| |\ \  `  
`| | * | `[d690fa40b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d690fa40b) dialog dans conversion et tabconvmob dans val correc dialog destroy et correc tablo à touche suivant , ou section suivant (2021-11-27 22:21) <Tommy>  
`| |/ /  `  
`| * |   `[bb391f915](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb391f915) Merge branch 'pourtestconflictcss' into 'master' (2021-11-27 20:25) <barroy tommy>  
`| |\ \  `  
`| | * \   `[47d661c52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47d661c52) Merge branch 'master' into pourtestconflictcss (2021-11-27 10:18) <Daniel Caillibaud>  
`| | |\ \  `  
`| | * | | `[1ec2b9105](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ec2b9105) eslint (2021-11-27 00:34) <Tommy>  
`| | | |/  `  
`| | |/|   `  
`| | * | `[ad823642d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad823642d) BrouillonCalculs externalisé en 3 exemplaires (qu’il reste à fusionner) (2021-11-26 18:42) <Daniel Caillibaud>  
`| | * | `[e353b9afd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e353b9afd) TableauConversionMobile déclacé dans outils/tableauconversion (2021-11-26 18:42) <Daniel Caillibaud>  
`| | * | `[5b7ba4486](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b7ba4486) méthode caseClavier virée (syntax error dedans, heureusement elle était jamais appelée) (2021-11-26 18:42) <Daniel Caillibaud>  
`| | * | `[b4a9eaa7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4a9eaa7a) externalisation TabConvMobil (2021-11-26 18:42) <Daniel Caillibaud>  
`| | * | `[d78a6b408](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d78a6b408) externalisation BrouillonCalculs (on renomme BroullionCalculs au passage) (2021-11-26 18:42) <Daniel Caillibaud>  
`| | * | `[5675cdbe3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5675cdbe3) retag 1.0.59 (2021-11-26 18:42) <j3pOnDev>  
`| | * | `[6aa4f49bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6aa4f49bf) hotfix this.conclusion undefined (rare mais apparemment ça peut arriver) (2021-11-26 18:42) <Daniel Caillibaud>  
`| | * | `[072c80e75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/072c80e75) en cours (2021-11-25 22:58) <Tommy>  
`| | * | `[000ac53e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/000ac53e5) en cours (2021-11-25 19:24) <Tommy>  
`| |  /  `  
`| * |   `[500c2c1ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/500c2c1ee) Merge branch 'EslintFenetre' into 'master' (2021-11-27 20:20) <barroy tommy>  
`| |\ \  `  
`| | * | `[d29000a60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d29000a60) rien (2021-11-27 21:19) <Tommy>  
`| | * | `[24aa705c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24aa705c3) aj d'un objet Dialog générique (2021-11-24 10:26) <Daniel Caillibaud>  
`| | * | `[d2f3f6cf3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2f3f6cf3) eslintFenetre (2021-11-23 23:15) <Tommy>  
`| |  /  `  
`| * |   `[d39ac054b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d39ac054b) Merge branch 'affine' into 'master' (2021-11-27 13:45) <Rémi Deniaud>  
`| |\ \  `  
`| | * | `[1cbc2f8e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1cbc2f8e6) gestion d'un pgcd nul (2021-11-27 14:45) <Rémi Deniaud>  
`| |/ /  `  
`| * |   `[50dfeb61c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50dfeb61c) Merge branch 'fixEndDialog' into 'master' (2021-11-27 09:35) <Daniel Caillibaud>  
`| |\ \  `  
`| | |/  `  
`| |/|   `  
`| | * `[5aa3de715](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5aa3de715) on met le dialog enfant du conteneur plutôt que MG, pour voir si ça règle l'erreur bugsnag 619e6c8d1fe8c30008488a44 (2021-11-25 10:24) <Daniel Caillibaud>  
`| * `[8a66c435c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a66c435c) blindage j3pStyle (2021-11-27 09:56) <Daniel Caillibaud>  
`* `[0cf4497b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0cf4497b3) manquait 2 lignes (2021-11-17 19:58) <Tommy>  
`* `[b9892df1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9892df1a) bugfix et simplification (2021-11-17 14:00) <Daniel Caillibaud>  
`* `[1cde441f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1cde441f0) normalisation (stor & co) (2021-11-17 13:42) <Daniel Caillibaud>  
`* `[609925459](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/609925459) fontSize désormais inutile sur les parenthèses viré (2021-11-17 13:42) <Daniel Caillibaud>  
`* `[1c7d1153f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c7d1153f) import de la css fraction, fontSize désormais inutile sur les parenthèses viré (2021-11-17 13:42) <Daniel Caillibaud>  
`* `[800aa6c08](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/800aa6c08) aj des css et images manquantes (2021-11-17 13:42) <Daniel Caillibaud>  
`* `[3a152d652](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a152d652) eslint et normalisation (2021-11-17 13:42) <Daniel Caillibaud>  
`* `[8fac7b7cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8fac7b7cd) simplification (2021-11-17 13:42) <Daniel Caillibaud>  
`* `[8c1392f77](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c1392f77) aj commentaires (2021-11-17 13:42) <Daniel Caillibaud>  
`* `[01c4314fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01c4314fe) eslint var (2021-11-17 13:42) <Daniel Caillibaud>  
`* `[fb0799d0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb0799d0c) Externalisation des css pour charger les images via une feuille de styles (sans j3pGetBaseUrl), importée dans la section (d'après commit 3d53ea0a) (2021-11-17 13:42) <Daniel Caillibaud>  

## version 1.0.60

`* `[7eacad23f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7eacad23f) retag 1.0.60 (2021-11-26 20:30) <j3pOnDev>  
`*   `[af296fbb7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af296fbb7) Merge branch 'RepareMajuscule' into 'master' (2021-11-26 15:37) <barroy tommy>  
`|\  `  
`| * `[175d096cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/175d096cc) clavier majusculr (2021-11-26 16:36) <Tommy>  
`|/  `  
`*   `[434810020](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/434810020) Merge branch 'BugsnagPropor' into 'master' (2021-11-26 15:28) <barroy tommy>  
`|\  `  
`| * `[1990fad17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1990fad17) bug sur correction (2021-11-26 16:28) <Tommy>  
`* |   `[6ddf03d60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ddf03d60) Merge branch 'lesSuitesNext' into 'master' (2021-11-26 14:40) <Rémi Deniaud>  
`|\ \  `  
`| * | `[3218bf0b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3218bf0b6) Rien à voir avec les suites, c'est sur inequationdomaine, juste une petite modif (2021-11-26 15:31) <Rémi Deniaud>  
`| * | `[c0dce2b6c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0dce2b6c) correction pb quand on supprime tout dans la version avec fonction (2021-11-26 15:12) <Rémi Deniaud>  
`| * | `[f5bbce116](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5bbce116) contournement mauvaise reprise de parcours sur calculSuite (2021-11-26 15:06) <Rémi Deniaud>  
`|/ /  `  
`* |   `[68079fbdd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68079fbdd) Merge branch 'getLatexFracNaN' into 'master' (2021-11-26 13:19) <Rémi Deniaud>  
`|\ \  `  
`| * | `[186d40a41](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/186d40a41) gestion d'une mauvaise écriture d'une réponse par un élève dans SuiteTermeGeneral (2021-11-26 14:17) <Rémi Deniaud>  
`|/ /  `  
`* |   `[61ef52d6d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61ef52d6d) Merge branch 'bugfixDtePlanCart' into 'master' (2021-11-26 11:29) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[c1794b14e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1794b14e) fix erreurs d'ajout de propriétés à l'instance courante de Parcours (2021-11-26 12:28) <Daniel Caillibaud>  
`| * | `[6dbcb3a7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6dbcb3a7e) fix bugsnag 617057f016b60d0007053143 (2021-11-26 12:14) <Daniel Caillibaud>  
`| * | `[ba86cf677](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba86cf677) fix bugsnag 6190cfb34b791100072fe6b3 (2021-11-26 12:01) <Daniel Caillibaud>  
`|/ /  `  
`* | `[20e74190d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20e74190d) retag 1.0.59 (2021-11-26 10:30) <j3pOnDev>  
`* | `[824373a37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/824373a37) hotfix this.conclusion undefined (rare mais apparemment ça peut arriver) (2021-11-25 10:14) <Daniel Caillibaud>  
`|/  `  
`*   `[19d9d293b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19d9d293b) Merge branch 'mnibug' into 'master' (2021-11-24 23:52) <barroy tommy>  
`|\  `  
`| * `[098a39112](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/098a39112) testé (2021-11-25 00:51) <Tommy>  
`|/  `  
`*   `[0506db62c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0506db62c) Merge branch 'eslintappartenance' into 'master' (2021-11-24 21:49) <barroy tommy>  
`|\  `  
`| * `[4ecfe3a0d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ecfe3a0d) testé (2021-11-24 22:48) <Tommy>  
`|/  `  
`*   `[c23e3a068](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c23e3a068) Merge branch 'bugsnagGrave' into 'master' (2021-11-24 19:49) <barroy tommy>  
`|\  `  
`| * `[054300a41](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/054300a41) celui là je l'avais testé (2021-11-24 20:48) <Tommy>  
`|/  `  
`*   `[42fb53d1e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42fb53d1e) Merge branch 'bugsnagdslmafaute' into 'master' (2021-11-24 19:19) <barroy tommy>  
`|\  `  
`| * `[ad55b7df4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad55b7df4) bugsnag bete javais pas testé ...ca corrigeait un autre bugsnag (2021-11-24 20:19) <Tommy>  
`|/  `  
`* `[6ec57c159](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ec57c159) retour à la version ae049bdd pour la section conversion (2021-11-24 16:36) <Daniel Caillibaud>  
`*   `[824794233](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/824794233) Merge branch 'bienexrt' into 'master' (2021-11-24 14:49) <barroy tommy>  
`|\  `  
`| * `[05d9d607e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05d9d607e) eslint (2021-11-24 15:48) <Tommy>  
`| * `[b63e470e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b63e470e3) bs (2021-11-24 12:22) <Tommy>  
`* |   `[8dc845519](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8dc845519) Merge branch 'EtudeFctParcours' into 'master' (2021-11-24 14:22) <Rémi Deniaud>  
`|\ \  `  
`| * | `[85feebe5d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85feebe5d) encore un pb dans j3pVirgule + gestion de modele par défaut (2021-11-24 15:20) <Rémi Deniaud>  
`* | | `[5b52108ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b52108ca) Merge branch 'EtudeFctParcours' into 'master' (2021-11-24 14:04) <Rémi Deniaud>  
`|\| | `  
`| * | `[fdb941d71](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fdb941d71) contournement mauvaise reprise de parcours d'étude de fonctions (2021-11-24 15:03) <Rémi Deniaud>  
`* | | `[bc578d3ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc578d3ab) Merge branch 'EtudeFctParcours' into 'master' (2021-11-24 13:56) <Rémi Deniaud>  
`|\| | `  
`| * | `[14f349527](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14f349527) contournement mauvaise reprise de parcours d'étude de fonctions (2021-11-24 14:45) <Rémi Deniaud>  
`* | |   `[5c2bf1688](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c2bf1688) Merge branch 'pour_labomepdemandetableur' into 'master' (2021-11-24 11:38) <barroy tommy>  
`|\ \ \  `  
`| * | | `[6d19c06f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d19c06f0) labomep (2021-11-24 12:37) <Tommy>  
`|/ / /  `  
`* / / `[806898775](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/806898775) encore un hotfix sur conversion, pour éviter de planter le chargement quand #dialogAide n'existe pas (2021-11-24 12:07) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[86f0c6943](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86f0c6943) Merge branch 'j3pVirguleProbaCondSuite' into 'master' (2021-11-24 10:49) <Rémi Deniaud>  
`|\ \  `  
`| * | `[a8fa2f5ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8fa2f5ac) correction usage j3pExtraireNumDen (2021-11-24 11:48) <Rémi Deniaud>  
`* | | `[a0db52303](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0db52303) hotfix init de stor.lesdiv (2021-11-24 11:43) <Daniel Caillibaud>  
`* | | `[3a8640dc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a8640dc4) hotfix syntax error (2021-11-24 11:35) <Daniel Caillibaud>  
`* | | `[52ec8f80a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52ec8f80a) console.log virés (2021-11-24 11:33) <Daniel Caillibaud>  
`|/ /  `  
`* | `[64d052c6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64d052c6f) on zappe la fin du scenario ex_Calc_Multi_Edit-reprise en attendant de le réécrire (2021-11-24 11:20) <Daniel Caillibaud>  
`* | `[8e71be86e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e71be86e) rectif scenario ex_Calc_Multi_Edit-reprise mais il reste un pb sur runOne (la regex qui check l'énoncé couvre plus tous les cas) (2021-11-24 11:19) <Daniel Caillibaud>  
`* | `[fc5bb40e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc5bb40e9) retag 1.0.58 (2021-11-24 10:29) <j3pOnDev>  
`* |   `[dd55e0dae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd55e0dae) Merge branch 'bugsnagDecoupe' into 'master' (2021-11-24 08:35) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[f71a4036c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f71a4036c) bugsnag (2021-11-23 21:20) <Tommy>  
`* | |   `[fa59bdea1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa59bdea1) Merge branch 'j3pVirgule' into 'master' (2021-11-24 08:12) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[ac45510ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac45510ba) nettoyage paramStat_tableur (2021-11-22 23:01) <Rémi Deniaud>  
`| * | | `[4a1ce971d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a1ce971d) nettoyage medianeQuartiles (2021-11-22 22:45) <Rémi Deniaud>  
`| * | | `[77c19ef0b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77c19ef0b) nettoyage medianePetitsEffectifs (2021-11-22 22:34) <Rémi Deniaud>  
`| * | | `[8bfe805f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8bfe805f6) modif j3pVirgule nonEquiProba (2021-11-22 22:22) <Rémi Deniaud>  
`| * | | `[ee3875ba9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee3875ba9) modif j3pVirgule evolutionTableur (2021-11-22 22:17) <Rémi Deniaud>  
`| * | | `[8f6955735](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f6955735) nettoyage frequences (2021-11-22 22:05) <Rémi Deniaud>  
`* | | |   `[7a4192c7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a4192c7d) Merge branch 'Correction_Parcours_Pour_Eviter_Que_Rale_En_Cosnole_Dialogue_Final' into 'master' (2021-11-24 08:11) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[c996f24f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c996f24f4) faut virer le html pas seulement en mode debug (2021-11-23 11:57) <Daniel Caillibaud>  
`| * | | `[5292b5e44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5292b5e44) On génère proprement le html des bilans (2021-11-22 20:13) <Daniel Caillibaud>  
`| * | | `[1151b185f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1151b185f) modification de Parcours faite par Yves pour que ça rale plus en console pour ajout de balises html dans la boîte de dialogue finale. (2021-11-22 17:38) <Yves Biton>  
`* | | |   `[41a5720de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41a5720de) Merge branch 'bugdnagSta01Encore' into 'master' (2021-11-23 20:44) <barroy tommy>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[0c2d7e9c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c2d7e9c1) bugsnag (2021-11-23 21:42) <Tommy>  
`|/ / /  `  
`* | |   `[064b7fd7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/064b7fd7f) Merge branch 'notify_pour_bugsnag' into 'master' (2021-11-23 19:53) <barroy tommy>  
`|\ \ \  `  
`| * | | `[0aa1702c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0aa1702c7) notify pour bugsnag (2021-11-23 20:53) <Tommy>  
`|/ / /  `  
`* | |   `[dd3b948e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd3b948e7) Merge branch 'pourLAbomepDemande' into 'master' (2021-11-23 11:17) <barroy tommy>  
`|\ \ \  `  
`| * | | `[2d72bdb7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d72bdb7d) labomepRequest (2021-11-23 12:16) <Tommy>  
`|/ / /  `  
`* | |   `[c8d07586c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8d07586c) Merge branch 'eslintsss' into 'master' (2021-11-23 11:07) <barroy tommy>  
`|\ \ \  `  
`| * | | `[522893a50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/522893a50) labomepRequest (2021-11-23 12:05) <Tommy>  
`| * | | `[3f8084a00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f8084a00) bugsbg (2021-11-22 20:18) <Tommy>  
`* | | | `[0036a0767](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0036a0767) retag 1.0.57 (2021-11-23 08:03) <j3pOnDev>  
`* | | |   `[9aba82861](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9aba82861) Merge branch 'Annulation_Remplacement_Balises_BR' into 'master' (2021-11-23 07:02) <Yves Biton>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[7c1f2fa29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c1f2fa29) Le remplacement dans j3pAddContent des balises <br> par des \n ne fonctionne pas bien et certaines ressources de Yannick ne fonctionnent plus comme avant. Donc j'annule en attendant mieux. Il faudra revoir ça ... (2021-11-23 07:59) <Yves Biton>  
`|/ / /  `  
`* | |   `[f45d3db1e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f45d3db1e) Merge branch 'coAssLAbo' into 'master' (2021-11-22 20:23) <barroy tommy>  
`|\ \ \  `  
`| * | | `[60373b687](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/60373b687) labomepRequest (2021-11-22 21:21) <Tommy>  
`|/ / /  `  
`* | |   `[8b47b4097](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b47b4097) Merge branch 'simplifieNomme' into 'master' (2021-11-22 19:57) <barroy tommy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[f93812155](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f93812155) simplification du code, plus d'ids pour les zones principales, console qui râle plus pour canvas via j3pDiv et innerHTML= (2021-11-22 18:43) <Daniel Caillibaud>  
`* | | `[9aca5edae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9aca5edae) Merge branch 'simplifications' into 'master' (2021-11-22 17:40) <Daniel Caillibaud>  
`|\| | `  
`| |/  `  
`|/|   `  
`| * `[7bb19e7c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7bb19e7c7) pas mal de simplifications, ça râlera plus en console pour couleurRemplissage et opaciteRemplissage undefined (2021-11-22 18:39) <Daniel Caillibaud>  
`| * `[720a58049](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/720a58049) aj d'options à scripts/compareLoadAll.sh (2021-11-22 18:36) <Daniel Caillibaud>  
`|/  `  
`*   `[3fa456bb1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3fa456bb1) Merge branch 'proprieteMe' into 'master' (2021-11-22 14:17) <Daniel Caillibaud>  
`|\  `  
`| * `[c42fe4f80](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c42fe4f80) nettoyage compositionSchema (2021-11-20 22:17) <Rémi Deniaud>  
`| * `[3806db1a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3806db1a3) petit correctif moyenneEcartType (2021-11-20 15:42) <Rémi Deniaud>  
`| * `[23bce2cec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23bce2cec) nettoyage moyenneEcartType (2021-11-20 15:38) <Rémi Deniaud>  
`| * `[6872df963](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6872df963) nettoyage limiteGraphique (2021-11-20 14:41) <Rémi Deniaud>  
`| * `[7e32fd877](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e32fd877) nettoyage equationContinuite + petite corr dans l'outil tableau variations (2021-11-20 14:30) <Rémi Deniaud>  
`| * `[a5baf2afc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5baf2afc) nettoyage proportion (2021-11-20 14:12) <Rémi Deniaud>  
`| * `[bd55df2f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd55df2f2) nettoyage statsDeuxVariables (2021-11-20 13:38) <Rémi Deniaud>  
`* | `[39325fab4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39325fab4) ajout d'options à scripts/compareLoadAll.sh (2021-11-22 15:14) <Daniel Caillibaud>  
`* |   `[09f6c2ef2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09f6c2ef2) Merge branch 'Correction_Squelette_Calc_Vect_Multi_Edit' into 'master' (2021-11-22 08:46) <Yves Biton>  
`|\ \  `  
`| * | `[43cb8de62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43cb8de62) Correction de problèmes de focus et de clavier virtuel sur squelette Calc_Vect_Multi_Edit (2021-11-22 09:44) <Yves Biton>  
`|/ /  `  
`* |   `[6d7e85967](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d7e85967) Merge branch 'supportLab32' into 'master' (2021-11-22 08:18) <barroy tommy>  
`|\ \  `  
`| * | `[ba85aa598](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba85aa598) bugsbg (2021-11-22 09:17) <Tommy>  
`|/ /  `  
`* |   `[116802854](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/116802854) Merge branch 'Correction_Suppression_Balises_BR_Dans_AddContent' into 'master' (2021-11-21 10:55) <Yves Biton>  
`|\ \  `  
`| * | `[a8c36eeb0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8c36eeb0) Le replace n'était pas réaffecté à la chaîne donc ne faisait rien. Ca évitera que ça râle en console pour des balises <br> dans addContent... (2021-11-21 11:53) <Yves Biton>  
`|/ /  `  
`* |   `[52a2ad460](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52a2ad460) Merge branch 'suitesTjs' into 'master' (2021-11-21 08:11) <Rémi Deniaud>  
`|\ \  `  
`| * | `[e14db1840](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e14db1840) oubli de la gestion de termeInit avec suite arithmeticogeometreique dans SuiteTermeGeneral (2021-11-21 09:09) <Rémi Deniaud>  
`| * | `[67380a903](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67380a903) contournement d'un pb de reprise de parcours de calculTerme (2021-11-21 08:52) <Rémi Deniaud>  
`| * | `[7cf83e520](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7cf83e520) contournement d'un pb de reprise de parcours de limiteArithGeo (2021-11-21 08:48) <Rémi Deniaud>  
`|/ /  `  
`* | `[f3964bf72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3964bf72) hotfix j3pVirgule, on remet comme avant, ça retourne la chaîne passée si c'est n'importe quoi (2021-11-20 17:38) <Daniel Caillibaud>  
`* | `[e44e54306](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e44e54306) clean en sortant (2021-11-20 16:50) <Daniel Caillibaud>  
`* | `[577c38758](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/577c38758) aj scripts/compareLoadAll.sh pour l'analyse des loadAll.log (que l'on va ajouter aux rapports) (2021-11-20 16:44) <Daniel Caillibaud>  
`|/  `  
`*   `[926d0a4a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/926d0a4a3) Merge branch 'virguleMonome' into 'master' (2021-11-20 12:26) <Daniel Caillibaud>  
`|\  `  
`| * `[cc62f11ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc62f11ea) fix check de coef dans j3pMonome (2021-11-20 12:26) <Daniel Caillibaud>  
`| * `[48dbacf55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48dbacf55) une erreur avec isFinite (2021-11-20 12:33) <Rémi Deniaud>  
`| * `[7f4a677db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f4a677db) dernières petits modifs (2021-11-20 12:31) <Rémi Deniaud>  
`| * `[6e205bf47](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e205bf47) gestion d'un test qui ne passait pas (2021-11-20 11:21) <Rémi Deniaud>  
`| * `[10d4d32e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10d4d32e4) modif j3pMonome pour mettre des virgules sur les décimaux + ajout de tests (2021-11-20 11:15) <Rémi Deniaud>  
`* |   `[97eefddff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97eefddff) Merge branch 'hotFixJ3pVirgule' into 'master' (2021-11-20 12:07) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[a477a82a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a477a82a1) hotfix j3pVirgule (avec un peu de blindage au passage) (2021-11-20 13:05) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[c0793b8f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0793b8f0) Merge branch 'fixTests' into 'master' (2021-11-20 10:57) <Daniel Caillibaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[12bf3b3e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12bf3b3e2) harmonisation testsBrowser => testBrowser (2021-11-20 11:41) <Daniel Caillibaud>  
`| * `[0bc0416ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0bc0416ac) le selecteur mathquill a changé (2021-11-20 11:34) <Daniel Caillibaud>  
`| * `[71959c5bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/71959c5bd) dropLatex vire aussi les espaces bizarre (mathquill ajoute maintenant du zeroWidthSpace qui met le bazar, et on en profite pour virer aussi les insécables NBSP et THSP) (2021-11-20 11:34) <Daniel Caillibaud>  
`| * `[27c1404ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27c1404ca) adaptations des sélecteurs suite à des modifs de la section (2021-11-20 11:08) <Daniel Caillibaud>  
`| * `[8500d043c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8500d043c) le sélecteur du bouton de fin de section a changé… (2021-11-20 11:08) <Daniel Caillibaud>  
`| * `[36d13639a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/36d13639a) ListeDeroulante a changé (les div.choix sont devenus absolute et donc plus enfants du #idListe), faut s'adapter (2021-11-20 11:07) <Daniel Caillibaud>  
`| * `[940a81cd4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/940a81cd4) rectif fct globale à appeler (2021-11-20 11:07) <Daniel Caillibaud>  
`|/  `  
`* `[fa4cb909c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa4cb909c) retag 1.0.56 (2021-11-19 21:18) <j3pOnDev>  
`*   `[cfb1695d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfb1695d3) Merge branch 'Correction_Squelette_Param_2_Edit' into 'master' (2021-11-19 17:54) <Yves Biton>  
`|\  `  
`| * `[979e9421e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/979e9421e) Déplacement de l'initialisation de sq.marked suite à rapport BugSnag. (2021-11-19 17:54) <Yves Biton>  
`* | `[a30852d76](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a30852d76) on arrête silencieusement si on trouve pas le parent .mq-editable-field (peut arriver s'il vient d'être détruit et que le blink async est appelé après) (2021-11-19 18:45) <Daniel Caillibaud>  
`* |   `[35e99d0a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35e99d0a7) Merge branch 'Correction_Squelette_Calc_Param_MathQuill' into 'master' (2021-11-19 17:36) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[637a02861](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/637a02861) Correction suite à rapport BusNag (pas d'élémnt d'id 'correction') Plus de j3pDiv dabs le code. (2021-11-19 16:39) <Yves Biton>  
`* | |   `[aafed2d5f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aafed2d5f) Merge branch 'bugsnagEncore' into 'master' (2021-11-19 17:34) <Daniel Caillibaud>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[5a392bc1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a392bc1b) bugsbg (2021-11-19 16:57) <Tommy>  
`* | |   `[f55688d1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f55688d1d) Merge branch 'fctLatex' into 'master' (2021-11-19 17:19) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[e1a5c4bcb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1a5c4bcb) modif de forme (2021-11-19 18:17) <Daniel Caillibaud>  
`| * | | `[faae2b12a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/faae2b12a) blindage j3pSimplifieQuotient (et en conséquence j3pGetLatexFrac) avec aj de tests unitaires (2021-11-19 18:01) <Daniel Caillibaud>  
`| * | | `[98a535933](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98a535933) modif j3pGetLatexMonome + tests (2021-11-19 16:55) <Rémi Deniaud>  
`| * | | `[42cf9140b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42cf9140b) modif de j3pSimplifieQuotient (2021-11-19 16:25) <Rémi Deniaud>  
`| * | |   `[9e6692750](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e6692750) Merge branch 'master' into fctLatex (2021-11-19 14:50) <Rémi Deniaud>  
`| |\ \ \  `  
`* | \ \ \   `[cece7d36a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cece7d36a) Merge branch 'recomp06' into 'master' (2021-11-19 16:43) <barroy tommy>  
`|\ \ \ \ \  `  
`| * | | | | `[0f4e6694c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f4e6694c) bugsbg (2021-11-19 17:42) <Tommy>  
`|/ / / / /  `  
`* | | | |   `[e948cc2b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e948cc2b4) Merge branch 'bubug' into 'master' (2021-11-19 16:24) <barroy tommy>  
`|\ \ \ \ \  `  
`| |_|_|_|/  `  
`|/| | | |   `  
`| * | | | `[f2a942478](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2a942478) bugsbg (2021-11-19 17:23) <Tommy>  
`|/ / / /  `  
`* | | |   `[489d5ce10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/489d5ce10) Merge branch 'Correction_Squelettes' into 'master' (2021-11-19 14:53) <Yves Biton>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[072ab9051](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/072ab9051) Correction du squelette de somme de fractions (ou différences). L'élève pour vait se servir de la calculette et : entrer une solution approchée non excate qui était acceptée comme exacte car la vérification ne se faisait qu'à 10^-9 près. Voir accepté une réponse du type 0.835 (2021-11-19 15:50) <Yves Biton>  
`| | |/  `  
`| |/|   `  
`* | |   `[18bbbfeed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/18bbbfeed) Merge branch 'bugsnagggfefe' into 'master' (2021-11-19 14:18) <barroy tommy>  
`|\ \ \  `  
`| * | | `[7ee1ae254](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ee1ae254) bugsbg (2021-11-19 15:18) <Tommy>  
`|/ / /  `  
`* | |   `[18fe2fa3e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/18fe2fa3e) Merge branch 'bugsnanng' into 'master' (2021-11-19 14:06) <barroy tommy>  
`|\ \ \  `  
`| * | | `[12b43699f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12b43699f) bugsbg (2021-11-19 15:05) <Tommy>  
`* | | |   `[39eab3812](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39eab3812) Merge branch 'fctLatex' into 'master' (2021-11-19 14:06) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | /   `  
`| | |/    `  
`| |/|     `  
`| * | `[514ad7d86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/514ad7d86) aj d'un console.error si on passe autre chose qu'un nb ou fraction de nb à j3pExtraireNumDen (valeur de retour inchangée) (2021-11-19 10:36) <Daniel Caillibaud>  
`| * | `[fd481c532](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd481c532) aj des tests unitaires (2021-11-18 21:15) <Daniel Caillibaud>  
`| * | `[1386528a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1386528a4) renomages pour harmoniser (2021-11-18 19:04) <Daniel Caillibaud>  
`| * | `[bcb88c8db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bcb88c8db) simplification, renommage de simplifieRacine en j3pRacineLatex que l'on exporte (ça pourrait servir à d'autres) (2021-11-18 19:03) <Daniel Caillibaud>  
`| * | `[aeb97f8a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aeb97f8a6) simplification mineure (2021-11-18 19:03) <Daniel Caillibaud>  
`| * | `[ef8752577](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef8752577) blindage et factorisation de code avec ajout de j3pSimplifieQuotient et j3pSimplifieQuotientLatex (2021-11-18 19:03) <Daniel Caillibaud>  
`| * | `[14c9bde95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14c9bde95) modifs de forme (2021-11-18 19:03) <Daniel Caillibaud>  
`| * | `[4ca75d559](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ca75d559) blindage j3pPuissanceNb (2021-11-18 19:03) <Daniel Caillibaud>  
`| * | `[c21796e26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c21796e26) blindage j3pDivisionNbs (2021-11-18 19:03) <Daniel Caillibaud>  
`| * | `[85cdf8854](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85cdf8854) blindage j3pExtraireNumDen (2021-11-18 19:03) <Daniel Caillibaud>  
`| * | `[5902c458f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5902c458f) fix retour de j3pExtraireNumDen pour les nb non fractionnaires (2021-11-18 19:03) <Daniel Caillibaud>  
`| * | `[37ae5e9c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37ae5e9c2) externalisation des fcts latex (2021-11-18 19:03) <Daniel Caillibaud>  
`* | |   `[9d58f1cf6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d58f1cf6) Merge branch 'cobbbbug' into 'master' (2021-11-19 13:49) <barroy tommy>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[4dcd7ddf5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4dcd7ddf5) bug affichage (2021-11-19 14:47) <Tommy>  
`|/ /  `  
`* |   `[9c8d8996d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c8d8996d) Merge branch 'Amelioration_Squelettes' into 'master' (2021-11-19 13:01) <Yves Biton>  
`|\ \  `  
`| * | `[a57116f8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a57116f8b) Amalioration du code du squelette d'ériture scientifique et correction du code et des fichiers annexes car certaines réponses fausses pouvaient être considérées comme juste alors que fausses par exemple 1,21000000001*10^3 au lieu de 1,21*10^3 (2021-11-19 13:59) <Yves Biton>  
`* | |   `[346c93aaa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/346c93aaa) Merge branch 'CoBugAff' into 'master' (2021-11-19 12:04) <barroy tommy>  
`|\ \ \  `  
`| * | | `[1c3da3217](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c3da3217) bug affichage (2021-11-19 13:03) <Tommy>  
`|/ / /  `  
`* | | `[b9751952a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9751952a) aj regex pour remplacement de j3pDiv dans la doc (2021-11-19 12:23) <Daniel Caillibaud>  
`* | | `[660c3099b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/660c3099b) aj regex pour remplacement de j3pDiv dans la doc (2021-11-19 12:12) <Daniel Caillibaud>  
`|/ /  `  
`* | `[8c70238b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c70238b4) retag 1.0.55 (2021-11-19 09:47) <j3pOnDev>  
`* |   `[9913495ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9913495ac) Merge branch 'Correction_Clignotement_Curseur_MathQuill' into 'master' (2021-11-19 08:21) <Yves Biton>  
`|\ \  `  
`| * | `[e9a6bfeb3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9a6bfeb3) Correction du clignotement du curseur MathQuill qui ne fonctionnait plus (2021-11-19 09:19) <Yves Biton>  
`|/ /  `  
`* |   `[0a301a638](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a301a638) Merge branch 'repBArre' into 'master' (2021-11-18 21:57) <barroy tommy>  
`|\ \  `  
`| * | `[23e0bd407](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23e0bd407) rep barre zsm (2021-11-18 22:57) <Tommy>  
`|/ /  `  
`* |   `[3289b2357](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3289b2357) Merge branch 'eslintbis' into 'master' (2021-11-18 21:43) <barroy tommy>  
`|\ \  `  
`| * | `[18565e094](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/18565e094) eslint (2021-11-18 22:42) <Tommy>  
`|/ /  `  
`* |   `[aba85b911](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aba85b911) Merge branch 'eslintDroit' into 'master' (2021-11-18 20:48) <barroy tommy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[b65ae6664](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b65ae6664) eslint (2021-11-18 21:46) <Tommy>  
`* | `[3a1b6d4f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a1b6d4f2) retag 1.0.54 (2021-11-18 18:53) <j3pOnDev>  
`* |   `[268139a8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/268139a8b) Merge branch 'suitesAgain' into 'master' (2021-11-18 17:53) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[fd77580ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd77580ba) simplification (et changement de la date en commentaire) (2021-11-18 18:48) <Daniel Caillibaud>  
`| * | `[6127e9768](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6127e9768) ce n'était pas donnees_parcours mais donnees[donnees.length-1] (2021-11-18 15:53) <Rémi Deniaud>  
`* | |   `[4116d7fb2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4116d7fb2) Merge branch 'signalLAbo' into 'master' (2021-11-18 17:20) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[35036163e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35036163e) classe css simple virée, y'avait déjà un .noMargin qui fait la même chose (déplacé dans la feuille de style générique au passage) (2021-11-18 18:17) <Daniel Caillibaud>  
`| * | `[c9e793574](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9e793574) Signalement labomep (2021-11-17 22:39) <Tommy>  
`* | |   `[327557823](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/327557823) Merge branch 'fixAnalyseFrac' into 'master' (2021-11-18 14:18) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[169d4bd52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/169d4bd52) modifs dans homographique_domaine (2021-11-18 15:16) <Rémi Deniaud>  
`| * | | `[a78164b0b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a78164b0b) fix parsing d'une fraction avec regexp (2021-11-18 09:48) <Daniel Caillibaud>  
`* | | |   `[f6d24abed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6d24abed) Merge branch 'Correction_Squelette_Reduire_Eq_Droite' into 'master' (2021-11-18 13:52) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[6774b7e10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6774b7e10) Correction suite à rapport BusNag où l'éléement d'id 'correction' n'existait pas. (2021-11-18 13:52) <Yves Biton>  
`| | |_|/  `  
`| |/| |   `  
`* | | |   `[709505d84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/709505d84) Merge branch 'Correction_Squelette_Ex_Const' into 'master' (2021-11-18 13:52) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[c8e094c11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8e094c11) Correction suite à rapport BusNag où l'éléement d'id 'correction' n'existait pas. (2021-11-18 13:52) <Yves Biton>  
`| |/ / /  `  
`* | | |   `[21b2800c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21b2800c0) Merge branch 'proportions' into 'master' (2021-11-18 13:47) <Rémi Deniaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[98e93f866](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98e93f866) nettoyage propDeProportion (2021-11-17 23:08) <Rémi Deniaud>  
`* | | |   `[edefa9e57](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edefa9e57) Merge branch 'eslintpara' into 'master' (2021-11-18 12:03) <barroy tommy>  
`|\ \ \ \  `  
`| * | | | `[aa669ab8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa669ab8e) eslint (2021-11-18 13:01) <Tommy>  
`| * | | | `[a58193fb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a58193fb8) Signalement labomep (2021-11-18 00:49) <Tommy>  
`| * | | | `[67bf3b33f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67bf3b33f) Signalement labomep (2021-11-18 00:48) <Tommy>  
`| | |_|/  `  
`| |/| |   `  
`* | | |   `[7889a149a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7889a149a) Merge branch 'Amélioration_Squelette_Ecriture_Scientifique' into 'master' (2021-11-18 11:58) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[40ffce924](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40ffce924) Amélioration du squelette d'écriture scientifique pour déléguer la correction à MathQuill et avoir les virgules à la place des poinst décimaux. Adaptation des 2 fichiers annexes utilisés. (2021-11-18 12:57) <Yves Biton>  
`* | | | | `[ff42f41ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff42f41ef) aj j3pVirguleOnElt (2021-11-18 12:48) <Daniel Caillibaud>  
`|/ / / /  `  
`* | / / `[93f70a9a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93f70a9a1) retag 1.0.53 (2021-11-18 09:10) <j3pOnDev>  
`| |/ /  `  
`|/| |   `  
`* | |   `[688416845](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/688416845) Merge branch 'signalementSeb' into 'master' (2021-11-17 21:53) <Rémi Deniaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[316203283](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/316203283) console.log en trop (2021-11-17 22:36) <Rémi Deniaud>  
`| * | `[9e7e3eff3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e7e3eff3) dernière petite erreur dans un sujet de tauxEvolutionSuite (2021-11-17 22:36) <Rémi Deniaud>  
`| * | `[86c3883e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86c3883e2) j'ai remis donnees_parcours dans les exos sur les suites pour les ressources déjà débutées avant les modifs du week-end (2021-11-17 22:19) <Rémi Deniaud>  
`| * | `[47b325fc1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47b325fc1) correction de 2 petites erreurs (2021-11-17 21:56) <Rémi Deniaud>  
`* | |   `[37d54a8b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37d54a8b6) Merge branch 'esl87' into 'master' (2021-11-17 20:46) <barroy tommy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[74ace5945](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/74ace5945) eslint (2021-11-17 21:46) <Tommy>  
`|/ /  `  
`* |   `[eb3bdebcb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb3bdebcb) Merge branch 'ptitruc' into 'master' (2021-11-17 19:58) <barroy tommy>  
`|\ \  `  
`| * | `[756e35a19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/756e35a19) stor pour me (2021-11-17 20:56) <Tommy>  
`| * | `[8346266e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8346266e0) stor pour me (2021-11-17 20:32) <Tommy>  
`|/ /  `  
`* / `[b53e4ec4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b53e4ec4b) retag 1.0.52 (2021-11-17 19:06) <j3pOnDev>  
`|/  `  
`*   `[aa9446488](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa9446488) Merge branch 'gfds' into 'master' (2021-11-17 14:52) <barroy tommy>  
`|\  `  
`| * `[956378ff2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/956378ff2) finish (2021-11-17 15:51) <Tommy>  
`* |   `[a20e567e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a20e567e5) Merge branch 'fixShowError' into 'master' (2021-11-17 12:42) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[0ae0f167f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ae0f167f) fix affichage des erreurs, souvent masquée sous le titre (2021-11-17 13:41) <Daniel Caillibaud>  
`| |/  `  
`* |   `[a6886ed13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6886ed13) Merge branch 'modifFacteursDerivee' into 'master' (2021-11-17 10:12) <Rémi Deniaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[de5cef614](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de5cef614) reprise de modifs non mergées de EtudeFonction_facteursderivee (2021-11-17 11:00) <Rémi Deniaud>  
`|/  `  
`*   `[15b3527dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15b3527dd) Merge branch 'consolekl' into 'master' (2021-11-16 22:27) <barroy tommy>  
`|\  `  
`| * `[ee74f1653](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee74f1653) finish (2021-11-16 23:26) <Tommy>  
`|/  `  
`*   `[55c19406b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55c19406b) Merge branch 'paramFoireu' into 'master' (2021-11-16 22:23) <barroy tommy>  
`|\  `  
`| * `[2a15d294e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a15d294e) finish (2021-11-16 23:22) <Tommy>  
`|/  `  
`*   `[43af6586b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43af6586b) Merge branch 'eslinstss' into 'master' (2021-11-16 22:11) <barroy tommy>  
`|\  `  
`| * `[7691ccc1c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7691ccc1c) finish (2021-11-16 23:10) <Tommy>  
`|/  `  
`* `[b2bc0e72c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2bc0e72c) retag 1.0.51 (2021-11-16 19:55) <j3pOnDev>  
`*   `[176f67ef2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/176f67ef2) Merge branch 'fixBugsnagSomALg' into 'master' (2021-11-16 18:27) <Daniel Caillibaud>  
`|\  `  
`| * `[6eb2c4a1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6eb2c4a1d) eslint fixes (2021-11-16 18:10) <Daniel Caillibaud>  
`| * `[54a16c1ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54a16c1ff) suite du merge, avec répercussion d'autres modifs faites dans master depuis la création de cette branche (2021-11-16 17:55) <Daniel Caillibaud>  
`* | `[a2bee70b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2bee70b0) Merge branch 'fixBugsnagSomALg' into 'master' (2021-11-16 16:54) <barroy tommy>  
`|\| `  
`| *   `[ac64b9ce8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac64b9ce8) Merge branch 'master' into fixBugsnagSomALg (2021-11-16 17:45) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[cc94734a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc94734a8) Merge branch 'point06ETik' into 'master' (2021-11-16 15:56) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[fe446c59c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe446c59c) finish (2021-11-16 12:04) <Tommy>  
`| * | `[92b1d84c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92b1d84c6) tibug (2021-11-16 00:36) <Tommy>  
`| * | `[9cf829c96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9cf829c96) tibug (2021-11-16 00:35) <Tommy>  
`| * | `[0141f1e74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0141f1e74) tibug (2021-11-16 00:35) <Tommy>  
`| * |   `[c4bf893c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4bf893c8) Merge branch 'master' into point06ETik (2021-11-15 21:38) <Tommy>  
`| |\ \  `  
`| * | | `[c26f61bdd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c26f61bdd) tibug (2021-11-15 19:58) <Tommy>  
`| * | | `[23316c20b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23316c20b) tibug (2021-11-15 15:59) <Tommy>  
`| * | | `[3fe43ecc5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3fe43ecc5) tibug (2021-11-15 15:58) <Tommy>  
`|  / /  `  
`* | |   `[13800f48b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13800f48b) Merge branch 'eslnt74398' into 'master' (2021-11-16 15:56) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[c61077b1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c61077b1d) finish (2021-11-16 14:06) <Tommy>  
`* | | |   `[25bd5402b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25bd5402b) Merge branch 'fixReducFrac' into 'master' (2021-11-16 15:52) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[0d6476e23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d6476e23) on prend en compte le param option boutonRecopie (2021-11-16 16:47) <Daniel Caillibaud>  
`| * | | | `[89255c282](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89255c282) typofix boutonsMathquill (2021-11-16 16:45) <Daniel Caillibaud>  
`| * | | | `[57cb7a32e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57cb7a32e) fix nb d'essais (2021-11-16 15:56) <Daniel Caillibaud>  
`| * | | | `[d2a9ca4de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2a9ca4de) fix erreur bugsnag 61684ba1f740b80007cb7bb2 avec refacto du code au passage (2021-11-16 14:08) <Daniel Caillibaud>  
`| |/ / /  `  
`* | | |   `[f0f12c710](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0f12c710) Merge branch 'hotfix' into 'master' (2021-11-16 14:51) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[0aa3dcf86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0aa3dcf86) les 3 ZoneFormuleStyleMathquill plantent désormais avec un message explicite en cas de conteneur foireux (2021-11-16 14:42) <Daniel Caillibaud>  
`| * | | `[5717abce5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5717abce5) fix erreur bugsnag 6193aa24862d120008f9066e (faut gérer le cas currentVirtualKeyboard null dans copyListener et pasteListener (2021-11-16 14:37) <Daniel Caillibaud>  
`| * | | `[fd2b8832d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd2b8832d) </br> => <br> (2021-11-16 14:12) <Daniel Caillibaud>  
`| * | | `[d8d5bf626](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8d5bf626) commentaires et modif de forme (2021-11-16 14:10) <Daniel Caillibaud>  
`|/ / /  `  
`* | |   `[8a15f2fea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a15f2fea) Merge branch 'bugsnag327484' into 'master' (2021-11-15 23:44) <barroy tommy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[b4dead6d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4dead6d3) tibug (2021-11-16 00:43) <Tommy>  
`|/ /  `  
`* |   `[9807aa47d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9807aa47d) Merge branch 'bugsnag234' into 'master' (2021-11-15 20:16) <barroy tommy>  
`|\ \  `  
`| * | `[d4d8f3eaa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4d8f3eaa) tibug (2021-11-15 21:15) <Tommy>  
`|/ /  `  
`* |   `[f7fb985f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7fb985f6) Merge branch 'newPat' into 'master' (2021-11-15 20:04) <barroy tommy>  
`|\ \  `  
`| * | `[e70eb45c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e70eb45c5) tibug (2021-11-15 21:03) <Tommy>  
`|/ /  `  
`* |   `[02de41c7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02de41c7d) Merge branch 'bugsnag22' into 'master' (2021-11-15 19:12) <barroy tommy>  
`|\ \  `  
`| * | `[96795a414](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96795a414) tibug (2021-11-15 20:11) <Tommy>  
`|/ /  `  
`* |   `[a338ffa0d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a338ffa0d) Merge branch 'donneesParcours' into 'master' (2021-11-15 18:03) <Rémi Deniaud>  
`|\ \  `  
`| * | `[39fc66448](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39fc66448) mise à jour de la doc sur la reprise de données (2021-11-15 19:02) <Rémi Deniaud>  
`* | |   `[5445afc5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5445afc5a) Merge branch 'parcoursEtudefcts' into 'master' (2021-11-15 17:57) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[09a616f1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09a616f1b) études de fonctions : infos sur le paramètre imposer_fct + utilisation uniquement de 'parcours' et non plus 'j3p.parcours.donnees' (2021-11-12 16:24) <Rémi Deniaud>  
`| * | |   `[e55e1bbf0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e55e1bbf0) Merge remote-tracking branch 'origin/parcoursEtudefcts' into parcoursEtudefcts (2021-11-12 15:50) <Rémi Deniaud>  
`| |\ \ \  `  
`| | * | | `[7ec471181](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ec471181) quelques modifications apportées quand on impose une fonction au départ (2021-11-11 09:12) <Rémi Deniaud>  
`| | * | | `[24ce7e630](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24ce7e630) console.log qui traine (2021-11-10 23:53) <Rémi Deniaud>  
`| | * | | `[353f143ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/353f143ba) utilisation de me.parcours.donneesEtudeFcts pour envoyer les infos d'un noeud à l'autre dans les études de fonctions (2021-11-10 23:51) <Rémi Deniaud>  
`| |  / /  `  
`| * | | `[589e778f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/589e778f6) quelques modifications apportées quand on impose une fonction au départ (2021-11-12 15:49) <Rémi Deniaud>  
`| * | | `[3b75a7827](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b75a7827) console.log qui traine (2021-11-12 15:49) <Rémi Deniaud>  
`| * | | `[2f4a634cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f4a634cd) utilisation de me.parcours.donneesEtudeFcts pour envoyer les infos d'un noeud à l'autre dans les études de fonctions (2021-11-12 15:49) <Rémi Deniaud>  
`|  / /  `  
`* | | `[0da9e489e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0da9e489e) Merge branch 'donneesParcours' into 'master' (2021-11-15 17:56) <Rémi Deniaud>  
`|\| | `  
`| * | `[761238e15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/761238e15) donneesPrecedentes devient un booleén (2021-11-15 18:52) <Rémi Deniaud>  
`| * | `[843d26314](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/843d26314) actualisation du tutoriel sur le chainage des noeuds (2021-11-12 15:29) <Rémi Deniaud>  
`| * | `[91f968d9b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91f968d9b) suppression de J3PDonneesParcours et utilisation de me.parcours.donneesSuites pour récupérer les valeurs d'un noeud à l'autre - fonctionne pour les ressources existantes (2021-11-12 15:12) <Rémi Deniaud>  
`| * | `[3a45efe45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a45efe45) mise à jour de la doc sur le chaînage de sections (2021-11-11 14:47) <Rémi Deniaud>  
`| * | `[a977655b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a977655b0) ajout d'informations sur le chaîne de noeuds pour les suites (2021-11-10 22:24) <Rémi Deniaud>  
`| * | `[65922abd6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65922abd6) dernière petite modif dans calculTerme (2021-11-10 21:50) <Rémi Deniaud>  
`| * | `[23b677cb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23b677cb8) parcours.donnees dans sommeTermesSuites (2021-11-10 17:28) <Rémi Deniaud>  
`| * | `[f84da2ead](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f84da2ead) parcours.donnees dans natureSuite (2021-11-10 17:25) <Rémi Deniaud>  
`| * | `[dc547ec29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc547ec29) parcours.donnees dans determinerSeuil (2021-11-10 17:22) <Rémi Deniaud>  
`| * | `[c5477b13b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5477b13b) parcours.donnees dans calculTerme et conjectureLimite (2021-11-10 17:03) <Rémi Deniaud>  
`| * |   `[3f15a2f2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f15a2f2b) Merge remote-tracking branch 'origin/donneesParcours' into donneesParcours (2021-11-10 16:36) <Rémi Deniaud>  
`| |\ \  `  
`| | * | `[fc428fb1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc428fb1f) modif dans tauxEvolutionSuite pour éviter des réponses qui font planter (2021-11-09 18:29) <Rémi Deniaud>  
`| | * | `[1431c609f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1431c609f) modif du parcours 5e00c407bcbd3d668003f025 sur les suites (2021-11-09 18:06) <Rémi Deniaud>  
`| |  /  `  
`| * | `[197cd0658](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/197cd0658) modif dans tauxEvolutionSuite pour éviter des réponses qui font planter (2021-11-10 16:34) <Rémi Deniaud>  
`| * | `[b95e6b287](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b95e6b287) modif du parcours 5e00c407bcbd3d668003f025 sur les suites (2021-11-10 16:34) <Rémi Deniaud>  
`|  /  `  
`* |   `[ef1fdc605](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef1fdc605) Merge branch 'Correction_Fichier_Annexe_Image_Fraction_Par_Trinome' into 'master' (2021-11-15 16:26) <Yves Biton>  
`|\ \  `  
`| * | `[c6bac1365](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6bac1365) Correction de fichier annexe (2021-11-15 17:24) <Yves Biton>  
`|/ /  `  
`* |   `[8a9d84332](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a9d84332) Merge branch 'Correction_Fichier_Annexe_Exo' into 'master' (2021-11-15 16:18) <Yves Biton>  
`|\ \  `  
`| * | `[fa3a1be01](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa3a1be01) Correction de fichier annexe (2021-11-15 17:15) <Yves Biton>  
`|  /  `  
`* | `[29b6cb6f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/29b6cb6f1) Merge branch 'rectifCursor' into 'master' (2021-11-15 15:27) <Yves Biton>  
`* | `[b813230fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b813230fc) ajout de commentaire et rectif de la classe à virer sur le parent du curseur (mq-hascursor avait été remplacé par mq-cursor et c'était mq-hasCursor qu'il fallait virer) (2021-11-15 16:17) <Daniel Caillibaud>  
` /  `  
`* `[7378d4796](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7378d4796) eslint (2021-09-24 16:00) <Tommy>  
`* `[15a1186cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15a1186cf) correction du bug (2021-09-24 15:53) <Tommy>  

## version 1.0.50

`* `[c7ba3741f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7ba3741f) retag 1.0.50 (2021-11-15 12:06) <j3pOnDev>  
`*   `[6d7d8bc28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d7d8bc28) Merge branch 'fixZindex' into 'master' (2021-11-15 11:05) <Daniel Caillibaud>  
`|\  `  
`| * `[d554de033](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d554de033) harmonisation des zIndex, on fixe les boutons à 50 (ils ne l'étaient que dans MG) (2021-11-15 12:04) <Daniel Caillibaud>  
`|/  `  
`* `[3de0430dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3de0430dd) hotfix, il manquait une image => build KO (2021-11-15 11:29) <Daniel Caillibaud>  
`*   `[24bd4c685](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24bd4c685) Merge branch 'Gestion_Copier_Coller_Clavier_Virtuel' into 'master' (2021-11-15 10:15) <Daniel Caillibaud>  
`|\  `  
`| * `[40e677722](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40e677722) ancien outilsexternes/mathquill viré (remplacé depuis longtemps par lib/mathquill) (2021-11-15 11:14) <Daniel Caillibaud>  
`| * `[2a078f53e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a078f53e) modif commentaires, code mort viré, fix .mq-hascursor => .mq-cursor (2021-11-15 11:11) <Daniel Caillibaud>  
`| * `[edc12d920](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edc12d920) Plus besoin d'éliminer les curseurs fantômes dans doCharAction (2021-11-13 17:25) <Yves Biton>  
`| * `[5a0abd780](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a0abd780) Finalement le try catch dans blink de mathquill.js ne semble pas servir sur iPad mais comme j'ai un doute je le laisse mais sans le console.log qui ne sert à rien car on ne peut pas le voir sur iPad. (2021-11-13 09:15) <Yves Biton>  
`| * `[5db8f68ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5db8f68ed) Version où le problème des curseurs fixes fantômes semble réglée (de façon hélas pas idéale). Dans la fonction blink de clignotement de Mathquill on recherche les curseurs parasites fixes et on les supprime. Un try catch est nécessaire pour iPad pour une raison que j'ignore. Cette version permet de copier le contenu d'un éditeur pour le coller dans un autre. La sélection  dans un éditeur MathQuill provoquait des erreurs et a été désactivée. (2021-11-12 19:08) <Yves Biton>  
`| * `[536d65dc5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/536d65dc5) Pas réussi à réparer le problème des curseurs fantômes. (2021-11-10 23:02) <Yves Biton>  
`| * `[4756e2f94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4756e2f94) Tjrs des pbs de anticursor undefined (2021-11-10 21:41) <Yves Biton>  
`| * `[f4f7af006](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4f7af006) Ajout de la gestion de copier coller depuis et vers champ mathquill Tentative de correction des appels en cascade de fakefocus sur PC avec écran tactile (2021-11-10 11:25) <Yves Biton>  
`* |   `[e3c99a5d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e3c99a5d1) Merge branch 'reslin' into 'master' (2021-11-14 20:12) <barroy tommy>  
`|\ \  `  
`| * | `[3a2f2cf82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a2f2cf82) tibug (2021-11-14 21:08) <Tommy>  
`|/ /  `  
`* |   `[7c4f1932a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c4f1932a) Merge branch 'newb' into 'master' (2021-11-13 23:53) <barroy tommy>  
`|\ \  `  
`| * | `[7cf1176a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7cf1176a1) tibug (2021-11-14 00:53) <Tommy>  
`* | |   `[c743257a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c743257a0) Merge branch 'netParcours' into 'master' (2021-11-13 21:34) <Rémi Deniaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[37d04257d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37d04257d) nettoyage resteDivEucl (2021-11-13 22:27) <Rémi Deniaud>  
`| * | `[3f3720315](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f3720315) nettoyage simpleFracAvecDecomposition (2021-11-13 22:03) <Rémi Deniaud>  
`| * | `[b07de21f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b07de21f1) nettoyage pgcdAvecDecomposition (2021-11-13 21:49) <Rémi Deniaud>  
`| * | `[cae972291](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cae972291) nettoyage critereDivisibilite (2021-11-13 21:36) <Rémi Deniaud>  
`| * | `[d807247bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d807247bf) nettoyage asymptote (2021-11-13 21:28) <Rémi Deniaud>  
`| * | `[75fe8c38f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75fe8c38f) nettoyage lectureEquationDroite (2021-11-13 19:43) <Rémi Deniaud>  
`| * | `[d9a2d2495](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9a2d2495) nettoyage colinearite (2021-11-13 19:18) <Rémi Deniaud>  
`| * | `[4f206151f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f206151f) nettoyage coefVectColineaires (2021-11-13 19:07) <Rémi Deniaud>  
`| * | `[d0678a8fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0678a8fc) nettoyage appliColinearite (2021-11-13 19:00) <Rémi Deniaud>  
`|/ /  `  
`* |   `[608d055e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/608d055e5) Merge branch 'esl04' into 'master' (2021-11-13 15:10) <barroy tommy>  
`|\ \  `  
`| * | `[22109dadd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22109dadd) tibug (2021-11-13 16:10) <Tommy>  
`|/ /  `  
`* |   `[04dc5b7c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04dc5b7c2) Merge branch 'tibbug' into 'master' (2021-11-13 08:31) <barroy tommy>  
`|\ \  `  
`| * | `[fce48c19e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fce48c19e) tibug (2021-11-13 09:31) <Tommy>  
`|/ /  `  
`* |   `[629a82efb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/629a82efb) Merge branch 'tibug2' into 'master' (2021-11-12 23:37) <barroy tommy>  
`|\ \  `  
`| * | `[0025e7655](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0025e7655) tib (2021-11-13 00:37) <Tommy>  
`|/ /  `  
`* |   `[3d84ec00e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d84ec00e) Merge branch 'tipbug' into 'master' (2021-11-12 23:29) <barroy tommy>  
`|\ \  `  
`| * | `[df694dc7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df694dc7d) tib (2021-11-13 00:28) <Tommy>  
`|/ /  `  
`* |   `[b209afdaf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b209afdaf) Merge branch 'eslintFraction05' into 'master' (2021-11-12 23:18) <barroy tommy>  
`|\ \  `  
`| * | `[cf32f2927](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf32f2927) esl (2021-11-13 00:17) <Tommy>  
`|/ /  `  
`* |   `[bac9e593e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bac9e593e) Merge branch 'esl06' into 'master' (2021-11-12 07:06) <barroy tommy>  
`|\ \  `  
`| * | `[b30e95b92](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b30e95b92) esl (2021-11-12 08:05) <Tommy>  
`|/ /  `  
`* |   `[5a7bfd78e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a7bfd78e) Merge branch 'esl7' into 'master' (2021-11-12 06:56) <barroy tommy>  
`|\ \  `  
`| * | `[b96294029](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b96294029) esl (2021-11-12 07:54) <Tommy>  
`|/ /  `  
`* |   `[81956af0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81956af0a) Merge branch 'eslint_88' into 'master' (2021-11-11 21:34) <barroy tommy>  
`|\ \  `  
`| * | `[f3d0075a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3d0075a3) esl (2021-11-11 22:33) <Tommy>  
`|/ /  `  
`* |   `[3358be9f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3358be9f7) Merge branch 'eslintybd' into 'master' (2021-11-11 20:49) <barroy tommy>  
`|\ \  `  
`| * | `[5ec0a7fec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ec0a7fec) esl (2021-11-11 21:48) <Tommy>  
`|/ /  `  
`* |   `[13cd1628d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13cd1628d) Merge branch 'reHito' into 'master' (2021-11-11 20:24) <barroy tommy>  
`|\ \  `  
`| * | `[ab5464f78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab5464f78) esl (2021-11-11 21:23) <Tommy>  
`|/ /  `  
`* |   `[93eb39ee8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93eb39ee8) Merge branch 'dhqkjcdk' into 'master' (2021-11-11 18:24) <barroy tommy>  
`|\ \  `  
`| * | `[e31a6751d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e31a6751d) esl (2021-11-11 19:23) <Tommy>  
`|/ /  `  
`* |   `[ab4f9dd5c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab4f9dd5c) Merge branch 'eslingh' into 'master' (2021-11-11 18:02) <barroy tommy>  
`|\ \  `  
`| * | `[610d3e42e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/610d3e42e) esl (2021-11-11 19:01) <Tommy>  
`|/ /  `  
`* |   `[f777b1345](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f777b1345) Merge branch 'zoneClikCCC' into 'master' (2021-11-11 14:40) <barroy tommy>  
`|\ \  `  
`| * \   `[e3061f492](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e3061f492) Merge branch 'master' into 'zoneClikCCC' (2021-11-11 14:40) <barroy tommy>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[dbd45e68d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dbd45e68d) Merge branch 'fixJ3pMathquillXcas' into 'master' (2021-11-11 08:35) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[4f95cb1e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f95cb1e6) ajout commentaire faux bug (2021-11-11 09:34) <Rémi Deniaud>  
`| * | | `[b4f4e8d34](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4f4e8d34) on remet l'ancien j3pMathquillXcas à sa place et déplace le mqNormalise dans src/lib/outils/conversion/casFormat.js pour pouvoir tester de nouvelles expressions sur les deux en parallèle (2021-11-10 19:51) <Daniel Caillibaud>  
`| * | | `[b1cd4eb72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1cd4eb72) ajout de 5 tests dans j3pMathquillXcas.test (2021-11-10 16:05) <Rémi Deniaud>  
`| * | | `[13a572d3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13a572d3d) ajout de 5 tests dans j3pMathquillXcas.test (2021-11-10 16:03) <Rémi Deniaud>  
`| * | | `[c6c008b3c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6c008b3c) aj de l'id du commit avec le test mais avant modif j3pMathquillXcas (2021-11-10 11:44) <Daniel Caillibaud>  
`| * | | `[a9a3723b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9a3723b8) WIP début de refacto de j3pMathquillNormalise (ex j3pMathquillXcas) mais il manque des exemples de ce que ça doit faire (2021-11-10 11:12) <Daniel Caillibaud>  
`| * | | `[4582f2c77](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4582f2c77) Aj de tests unitaires de j3pMathquillXcas avant de le modifier (2021-11-10 10:42) <Daniel Caillibaud>  
`* | | |   `[0088c8244](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0088c8244) Merge branch 'pyth2' into 'master' (2021-11-10 20:30) <barroy tommy>  
`|\ \ \ \  `  
`| * | | | `[f3988a5e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3988a5e4) eslint et un peu de css (2021-11-10 21:29) <Tommy>  
`|/ / / /  `  
`* | | |   `[70eaaa807](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70eaaa807) Merge branch 'pythCo' into 'master' (2021-11-10 19:53) <barroy tommy>  
`|\ \ \ \  `  
`| * | | | `[3d1043616](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d1043616) eslint et un peu de css (2021-11-10 20:51) <Tommy>  
`|/ / / /  `  
`* | | |   `[ee0419c04](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee0419c04) Merge branch 'outilBouton' into 'master' (2021-11-10 19:29) <barroy tommy>  
`|\ \ \ \  `  
`| * \ \ \   `[6af76b6d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6af76b6d4) Merge remote-tracking branch 'origin/outilBouton' into outilBouton (2021-11-10 00:27) <Tommy>  
`| |\ \ \ \  `  
`| | * | | | `[ce26c2256](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce26c2256) css (2021-11-09 20:28) <Tommy>  
`| | * | | | `[851424957](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/851424957) bugsnag (2021-11-09 12:51) <Tommy>  
`| | * | | | `[958582ba2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/958582ba2) bugsnag (2021-11-09 12:51) <Tommy>  
`| | | |_|/  `  
`| | |/| |   `  
`| * | | | `[ae049bdda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae049bdda) bouton (2021-11-10 00:23) <Tommy>  
`| * | | | `[a64fcbbd3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a64fcbbd3) css (2021-11-09 21:44) <Tommy>  
`| * | | | `[122bf7e13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/122bf7e13) css (2021-11-09 21:44) <Tommy>  
`| * | | | `[7ad2084da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ad2084da) bugsnag (2021-11-09 21:42) <Tommy>  
`| * | | | `[0baf12165](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0baf12165) bugsnag (2021-11-09 21:42) <Tommy>  
`* | | | |   `[c2a0049d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2a0049d1) Merge branch 'eslintdEfrac12' into 'master' (2021-11-10 19:27) <barroy tommy>  
`|\ \ \ \ \  `  
`| * | | | | `[2c2b8451b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c2b8451b) eslint et un peu de css (2021-11-10 15:26) <Tommy>  
`| * | | | | `[1f875bc7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f875bc7f) eslint et un peu de css (2021-11-10 15:23) <Tommy>  
`| * | | | | `[580066ec7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/580066ec7) eslint (2021-11-10 13:58) <Tommy>  
`| |/ / / /  `  
`* | | | |   `[25a894ed8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25a894ed8) Merge branch 'eslintMultiple' into 'master' (2021-11-10 19:26) <barroy tommy>  
`|\ \ \ \ \  `  
`| * | | | | `[bab6c88b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bab6c88b1) eslint (2021-11-10 08:14) <Tommy>  
`| |/ / / /  `  
`| | | | * `[d475e36db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d475e36db) eslint et un peu de css (2021-11-11 15:38) <Tommy>  
`| | | | * `[86745644f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86745644f) eslint et un peu de css (2021-11-11 15:35) <Tommy>  
`| | | | * `[4b81c4665](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b81c4665) eslint et un peu de css (2021-11-10 20:23) <Tommy>  
`| |_|_|/  `  
`|/| | |   `  
`* | | |   `[04d4d9709](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04d4d9709) Merge branch 'zarb' into 'master' (2021-11-10 13:50) <barroy tommy>  
`|\ \ \ \  `  
`| * | | | `[ca0a83b7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca0a83b7d) bugsnag (2021-11-10 14:48) <Tommy>  
`* | | | | `[8c404587e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c404587e) Merge branch 'jauraijamaisduremplacercevarparlet' into 'master' (2021-11-10 13:47) <barroy tommy>  
`|\| | | | `  
`| * | | | `[2fe6f024c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2fe6f024c) bugsnag (2021-11-10 14:46) <Tommy>  
`|/ / / /  `  
`* | | |   `[e90276e82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e90276e82) Merge branch 'bugsnProg' into 'master' (2021-11-10 13:10) <barroy tommy>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[aa6aee3c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa6aee3c4) bugsnag (2021-11-10 14:08) <Tommy>  
`|/ / /  `  
`* / / `[4b311bf18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b311bf18) fix s en trop à warningss dans le rapport de loadAll (2021-11-10 09:21) <Daniel Caillibaud>  
`|/ /  `  
`* | `[41ee2f8f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41ee2f8f5) retag 1.0.49 (2021-11-09 20:32) <j3pOnDev>  
`* |   `[9f173a2eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f173a2eb) Merge branch 'fixEspaceRepere7' (2021-11-09 19:17) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[027513bae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/027513bae) fonctions j3pXx qui utilisent jQuery.dialog externalisée dans leur fichier js (2021-11-09 19:16) <Daniel Caillibaud>  
`| * | `[70c823206](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70c823206) fix erreur bugsnag 61899d818b752f0007d1a9b7 (et notif avec context si ça devait se reproduire quand même) (2021-11-09 18:57) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[7f27608cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f27608cd) Merge branch 'csspg' into 'master' (2021-11-09 17:21) <barroy tommy>  
`|\ \  `  
`| * | `[b33a542c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b33a542c3) css (2021-11-09 18:20) <Tommy>  
`|/ /  `  
`* |   `[f2b94bb67](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2b94bb67) Merge branch 'fixParallelesFaisceau' into 'master' (2021-11-09 16:33) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[8ad786db0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8ad786db0) fix pb une seule droite affichée (2021-11-09 17:33) <Daniel Caillibaud>  
`* | |   `[69913e848](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69913e848) Merge branch 'bugsNNagTab' into 'master' (2021-11-09 16:05) <barroy tommy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[5e13d8efa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e13d8efa) bugsnag (2021-11-09 17:04) <Tommy>  
`|/ /  `  
`* | `[8678af099](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8678af099) retag 1.0.48 (2021-11-09 13:55) <j3pOnDev>  
`* | `[9e9a4e8c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e9a4e8c3) hotfix zsmBtn en double, le 2e renommé en zsmChapeau (2021-11-09 13:46) <Daniel Caillibaud>  
`* |   `[ca783caf6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca783caf6) Merge branch 'tibugCO' into 'master' (2021-11-09 11:58) <barroy tommy>  
`|\ \  `  
`| * | `[2a8c0dd7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a8c0dd7e) bugsnag (2021-11-09 12:57) <Tommy>  
`* | |   `[2f186ae92](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f186ae92) Merge branch 'tabVar' into 'master' (2021-11-09 11:54) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[fc2250c66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc2250c66) petite modif dans ArithmeticoGeo (2021-11-07 18:50) <Rémi Deniaud>  
`| * | `[2aac86ed5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2aac86ed5) gestion de zoneImage suite bugsnag + correction d'un pb avec DonneesPartielles dans tabVarLecture (2021-11-07 15:36) <Rémi Deniaud>  
`| * | `[900e05478](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/900e05478) attente du stor.fctsValid pour les connexions lentes (2021-11-07 14:37) <Rémi Deniaud>  
`| * | `[5ca179132](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ca179132) modi arbreDeuxEpreuvesIndep pour éviter le setTimeout non concluant (2021-11-07 12:04) <Rémi Deniaud>  
`| * | `[8f434d91c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f434d91c) appel récursif pour garantir que les zones de saisie sont bien construites (2021-11-07 11:10) <Rémi Deniaud>  
`* | |   `[e1a6967c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1a6967c3) Merge branch 'eslintprio' into 'master' (2021-11-09 11:52) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[7a0182beb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a0182beb) eslint (2021-11-09 08:41) <Tommy>  
`* | | |   `[c2d7ee0ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2d7ee0ae) Merge branch 'ZonesSuite' into 'master' (2021-11-09 11:51) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[98c51476a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98c51476a) fix import css (2021-11-09 12:33) <Daniel Caillibaud>  
`| * | | `[f447af9d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f447af9d7) rationalisation css (renommage des classes avec des noms intelligibles, factorisation de code) (2021-11-09 12:12) <Daniel Caillibaud>  
`| * | | `[3950497ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3950497ac) renommage avec des noms intelligibles pour les angles et arcs, modif des noms de classes css en conséquence (2021-11-09 11:42) <Daniel Caillibaud>  
`| * | | `[d54ba255f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d54ba255f) renommages des angles(2|3|4|5) avec des noms plus évocateurs de leurs contenus, harmonisation des noms de classes css correspondantes (2021-11-09 11:23) <Daniel Caillibaud>  
`| * | | `[6eef2df44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6eef2df44) zsmAngle et zsmAngleX fusionnés (identiques) (2021-11-09 11:15) <Daniel Caillibaud>  
`| * | | `[d0bafe723](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0bafe723) styles spécifiques à outils/zoneStyleMathquill déplacés de themes/table.scss vers outils/zoneStyleMathquill/zoneStyleMathquill.css (2021-11-09 10:57) <Daniel Caillibaud>  
`| * | | `[6335c41ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6335c41ae) tdcont2 viré ou remplacé par tdcont (identique) (2021-11-09 10:52) <Daniel Caillibaud>  
`| * | | `[80f9a7370](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80f9a7370) rectif commentaires et factorisation margin/padding (2021-11-09 10:44) <Daniel Caillibaud>  
`| * | | `[4ecfd85ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ecfd85ac) il manquait elle je sais pas pourquoi (2021-11-09 08:22) <Tommy>  
`| * | | `[41df6e0f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41df6e0f5) et apres je met tout dans une (2021-11-09 08:08) <Tommy>  
`| * | |   `[0f5449d0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f5449d0f) Merge branch 'master' into ZonesSuite (2021-11-08 16:29) <Daniel Caillibaud>  
`| |\ \ \  `  
`| * | | | `[49163bc6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49163bc6f) plus qu'une (2021-11-08 08:58) <Tommy>  
`* | | | |   `[b03138ecd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b03138ecd) Merge branch '2bugsnag' into 'master' (2021-11-09 10:43) <barroy tommy>  
`|\ \ \ \ \  `  
`| * | | | | `[814c252fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/814c252fe) bugsnag (2021-11-09 11:42) <Tommy>  
`|/ / / / /  `  
`* | | | |   `[75c904fa6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75c904fa6) Merge branch 'bugsnagValAp' into 'master' (2021-11-09 07:49) <barroy tommy>  
`|\ \ \ \ \  `  
`| |_|_|/ /  `  
`|/| | | |   `  
`| * | | | `[6512e93d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6512e93d7) bugsnag (2021-11-09 08:49) <Tommy>  
`|/ / / /  `  
`* | | |   `[88f9e334c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88f9e334c) Merge branch 'Correction_Exos_Produit_Scalaire' into 'master' (2021-11-08 20:47) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[bf5f7753f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf5f7753f) Correction de deux exos (fichiers annexes) (2021-11-08 21:45) <Yves Biton>  
`| | |_|/  `  
`| |/| |   `  
`* | | | `[a598dd362](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a598dd362) retag 1.0.47 (2021-11-08 17:53) <robot j3p@dev>  
`* | | | `[a172fd38f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a172fd38f) retag 1.0.46 (2021-11-08 16:48) <robot j3p@dev>  
`* | | |   `[a13410e0d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a13410e0d) Merge branch 'eslintProgConst' into 'master' (2021-11-08 15:21) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[46e63b669](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46e63b669) jai modif j3pImage pour au cas ou j'ai pas d id (2021-11-07 14:55) <Tommy>  
`| |/ /  `  
`* | | `[035ecfe7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/035ecfe7e) encore des styletext vides virés (2021-11-08 16:20) <Daniel Caillibaud>  
`* | | `[e27449263](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e27449263) console.log virés (2021-11-08 16:19) <Daniel Caillibaud>  
`* | | `[b043ae176](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b043ae176) fix plantage de build à cause d'une image manquante (2021-11-08 16:18) <Daniel Caillibaud>  
`* | |   `[e62fee25f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e62fee25f) Merge branch 'fixEcrisBienMathquill' into 'master' (2021-11-08 15:07) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[e5eb16d8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5eb16d8d) fct j3pNumToStr virée (remplacée par j3pNombreBienEcrit qui fait la même chose) (2021-11-08 16:05) <Daniel Caillibaud>  
`| * | | `[9e887a897](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e887a897) ecrisBienMathquill utilise j3pNombreBienEcrit (2021-11-08 16:01) <Daniel Caillibaud>  
`| * | | `[46bc1cab7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46bc1cab7) j3pEntierBienEcrit utilise j3pNombreBienEcrit (2021-11-08 15:58) <Daniel Caillibaud>  
`| * | | `[e746d96d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e746d96d0) Réécriture de j3pNombreBienEcrit, avec tests unitaires (2021-11-08 15:39) <Daniel Caillibaud>  
`|/ / /  `  
`* | / `[9131de97f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9131de97f) fix jsdoc Parcours.donnees (2021-11-08 11:18) <Daniel Caillibaud>  
`| |/  `  
`|/|   `  
`* |   `[8d9e720b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d9e720b9) Merge branch 'zoneForm3SansId(presque)' into 'master' (2021-11-07 23:24) <barroy tommy>  
`|\ \  `  
`| * | `[ef0452e53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef0452e53) support (2021-11-08 00:23) <Tommy>  
`* | | `[d941e540c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d941e540c) Merge branch 'zoneForm3SansId(presque)' into 'master' (2021-11-07 23:23) <barroy tommy>  
`|\| | `  
`| * | `[5179bf520](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5179bf520) support (2021-11-08 00:20) <Tommy>  
`| * | `[c59eb9552](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c59eb9552) vireId (2021-11-07 00:22) <Tommy>  
`* | |   `[79c16fd46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79c16fd46) Merge branch 'supportNomme' into 'master' (2021-11-07 21:52) <barroy tommy>  
`|\ \ \  `  
`| * | | `[b05c238df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b05c238df) support (2021-11-07 22:51) <Tommy>  
`|/ / /  `  
`* | |   `[d029beef7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d029beef7) Merge branch 'SnagValAp' into 'master' (2021-11-07 20:53) <barroy tommy>  
`|\ \ \  `  
`| * | | `[a4174030e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a4174030e) snag (2021-11-07 21:51) <Tommy>  
`|/ / /  `  
`* | |   `[2db9df706](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2db9df706) Merge branch 'grosNotifiPourbugsnag' into 'master' (2021-11-07 19:39) <barroy tommy>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[d02b58a05](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d02b58a05) grosnotifypourbugsnag (2021-11-07 20:38) <Tommy>  
`|/ /  `  
`* |   `[a39306849](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a39306849) Merge branch 'tjrslemmbugsnagstat01' into 'master' (2021-11-07 00:19) <barroy tommy>  
`|\ \  `  
`| * | `[45b768933](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45b768933) un bugsnag collant (2021-11-07 01:18) <Tommy>  
`|/ /  `  
`* |   `[1fbad3ef8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1fbad3ef8) Merge branch 'modifEncadre' into 'master' (2021-11-07 00:13) <barroy tommy>  
`|\ \  `  
`| * | `[37ae9cf86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37ae9cf86) modiftext (2021-11-07 01:11) <Tommy>  
`|/ /  `  
`* |   `[a337e943f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a337e943f) Merge branch 'modifDecomp' into 'master' (2021-11-07 00:02) <barroy tommy>  
`|\ \  `  
`| * | `[a04f21ae4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a04f21ae4) modificulte (2021-11-07 01:01) <Tommy>  
`|/ /  `  
`* |   `[9a804b4aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a804b4aa) Merge branch 'fixConUitC' into 'master' (2021-11-06 23:41) <barroy tommy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[7cf3d4931](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7cf3d4931) fix (2021-11-07 00:40) <Tommy>  
`|/  `  
`*   `[967ad7536](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/967ad7536) Merge branch 'autreBugsnag' into 'master' (2021-11-06 18:32) <Rémi Deniaud>  
`|\  `  
`| *   `[2570c944f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2570c944f) Merge branch 'master' into 'autreBugsnag' (2021-11-06 18:32) <Rémi Deniaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* | `[4d9d8aed7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d9d8aed7) retag 1.0.45 (2021-11-06 17:05) <robot j3p@dev>  
`* |   `[3b72f9146](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b72f9146) Merge branch 'bpStyleBugsnag' into 'master' (2021-11-06 16:03) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[0142cfeec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0142cfeec) viré tous les styletexte: {} inutiles, et viré aussi storage.afficheOptions (j3pAffiche y stockait le dernier styletexte passé en argument, et c'était récupéré par j3pCorrige, ça pouvait donc donner à peu près n'importe quoi, autant laisser le style courant) (2021-11-06 17:02) <Daniel Caillibaud>  
`| * | `[f31d8400f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f31d8400f) inutile de préciser boundingContainer quand c'est MG ou MD (2021-11-06 16:43) <Daniel Caillibaud>  
`| * | `[81bd9f3bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81bd9f3bd) mutualisation du code de décalage automatique (on avait rectifié dans show mais pas dans onResize, désormais c'est toujours onResize qui recale) (2021-11-06 16:35) <Daniel Caillibaud>  
`* | |   `[7aeaac228](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7aeaac228) Merge branch 'fixCbienEnBleu' (2021-11-06 11:32) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[753413e39](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/753413e39) fix du 'c bien' qui était en bleu et pas vert + nettoyage (2021-11-06 11:21) <Daniel Caillibaud>  
`| * | | `[961d79b02](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/961d79b02) commentaires (2021-11-06 11:19) <Daniel Caillibaud>  
`|/ / /  `  
`| | * `[2cd23cbc6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2cd23cbc6) petite modif dans le constructeur du tableau de variation (2021-11-06 19:22) <Rémi Deniaud>  
`| | * `[90424efab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90424efab) petite modif dans InstructionSi (2021-11-06 19:05) <Rémi Deniaud>  
`| | * `[c21a60ca1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c21a60ca1) petite modif dans bouclePourListe (2021-11-06 18:50) <Rémi Deniaud>  
`| | * `[d3590ca9b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3590ca9b) correction espace_repere7 (2021-11-06 18:46) <Rémi Deniaud>  
`| | * `[80ed20fe7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80ed20fe7) correction sommeTermesSuite (2021-11-06 18:03) <Rémi Deniaud>  
`| | * `[31ea2f6f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31ea2f6f3) nettoyage tauxReciproque (2021-11-06 17:43) <Rémi Deniaud>  
`| |/  `  
`| * `[51933ae26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51933ae26) intersectionReunion ajout clavier virtuel (2021-11-06 12:30) <Rémi Deniaud>  
`| * `[c20d8bc4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c20d8bc4b) nettoyage limiteComparaison avec ajout clavier virtuel (2021-11-06 12:14) <Rémi Deniaud>  
`| * `[e28a6db11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e28a6db11) nettoyage calculTerme (2021-11-06 11:45) <Rémi Deniaud>  
`| * `[cdbac272a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdbac272a) ajout clavier virtuel SuiteTermeGeneral (2021-11-06 11:41) <Rémi Deniaud>  
`| * `[238eda4c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/238eda4c3) ajout clavier virtuel natureSuite (2021-11-06 11:41) <Rémi Deniaud>  
`| * `[8a49b348a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a49b348a) nettoyage ArithmeticoGeo (2021-11-06 10:15) <Rémi Deniaud>  
`| * `[f58fb2f54](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f58fb2f54) evolutionTableur (2021-11-06 10:04) <Rémi Deniaud>  
`|/  `  
`*   `[0336b6658](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0336b6658) Merge branch 'bugSat1snag' into 'master' (2021-11-05 23:39) <barroy tommy>  
`|\  `  
`| * `[e976197ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e976197ee) bugsnag (2021-11-06 00:38) <Tommy>  
`|/  `  
`*   `[615ea8516](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/615ea8516) Merge branch 'style_sur_null' into 'master' (2021-11-05 21:37) <Rémi Deniaud>  
`|\  `  
`| * `[dd1d748f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd1d748f3) petit décalage dans tabVarLecture (2021-11-05 22:36) <Rémi Deniaud>  
`| * `[4b0b58d23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b0b58d23) des modifs dans canonique_tabvar et le constructeur des tableau de var pour essayer de gérer de l'asynchrone (2021-11-05 22:28) <Rémi Deniaud>  
`|/  `  
`*   `[6b286e57b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b286e57b) Merge branch 'tradNbLettres' into 'master' (2021-11-05 19:51) <Daniel Caillibaud>  
`|\  `  
`| * `[2e42a6d3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e42a6d3b) On utilise ces fonctions entierEnMot et nombreEnMots à la place de convertirEnLettres (2021-11-05 20:50) <Daniel Caillibaud>  
`| * `[5923a0d4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5923a0d4c) faut finalement mettre toujours des tirets autour de millions et milliards (+tests des options maxDecimales, garderZerosNonSignificatifs et ajouterUnites avec qq correctifs de code au passage) (2021-11-05 20:08) <Daniel Caillibaud>  
`| * `[6362b9372](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6362b9372) fix pbs d'arrondis sur la partie décimale (2021-11-04 20:12) <Daniel Caillibaud>  
`| * `[6a7d51d4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a7d51d4b) fix ortographe (2021-11-04 13:03) <Daniel Caillibaud>  
`| * `[2124f2783](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2124f2783) Première version de entierEnMots et nombreEnMots avec leurs tests unitaires, reste le cas millièmes + millionièmes + milliardièmes à gérer (2021-11-04 13:02) <Daniel Caillibaud>  
`* |   `[18af776a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/18af776a3) Merge branch 'correction_Focus_Clavier_Virtuel' into 'master' (2021-11-05 19:43) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[10797dde8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10797dde8) Version pour gérer le cas où on donne le focus à une éditeur non Mathquill alors que des éditeurs Mathquill avec clavier virtuel associés sont présents (problème signalé par Rémi). Version pour prendre en compte le cas oudocument.activeElement est null. (2021-11-05 20:35) <Yves Biton>  
`| * | `[a2ac57d25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2ac57d25) Version pour gérer le cas où on donne le focus à une éditeur non Mathquill alors que des éditeurs Mathquill avec clavier virtuel associés sont présents (problème signalé par Rémi). Correction étourderie ... (2021-11-05 20:14) <Yves Biton>  
`| * | `[39d270d9e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39d270d9e) Version pour gérer le cas où on donne le focus à une éditeur non Mathquill alors que des éditeurs Mathquill avec clavier virtuel associés sont présents (problème signalé par Rémi). (2021-11-05 20:04) <Yves Biton>  
`* | | `[abf883955](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/abf883955) retag 1.0.44 (2021-11-05 20:24) <robot j3p@dev>  
`* | | `[e666ea929](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e666ea929) hotfix ZoneFormuleStyleMathquill3 qui plante à cause d'un param manquant dans des listeners (2021-11-05 20:23) <Daniel Caillibaud>  
`* | |   `[0d3b86416](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d3b86416) Merge branch 'eslintPerimIAre' into 'master' (2021-11-05 19:18) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[372878a1e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/372878a1e) bugsnag (2021-11-05 14:50) <Tommy>  
`* | | |   `[b5ff077fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5ff077fa) Merge branch 'eslintDecoup' into 'master' (2021-11-05 19:17) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[4b6680fa0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b6680fa0) eslint (2021-11-05 15:48) <Tommy>  
`|/ / /  `  
`* | | `[b694c74d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b694c74d8) retag 1.0.43 (2021-11-05 13:49) <robot j3p@dev>  
`* | |   `[03580b281](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03580b281) Merge branch 'suiteBugsnag' into 'master' (2021-11-05 12:48) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[d330daa3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d330daa3d) nettoyage homographique_domaine (2021-11-04 23:28) <Rémi Deniaud>  
`| * | | `[1e39d78bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e39d78bc) nettoyage tauxCoefMult + liste non freezée (2021-11-04 22:18) <Rémi Deniaud>  
`| * | | `[6af3051ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6af3051ae) nettoyage affineounon (2021-11-04 21:45) <Rémi Deniaud>  
`| * | | `[2448bc5f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2448bc5f3) gestion typeReponse de dte_dte (2021-11-04 21:09) <Rémi Deniaud>  
`| * | | `[42268c51d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42268c51d) correction probaconditionnelles suite bugsnag (2021-11-04 18:24) <Rémi Deniaud>  
`| * | | `[163757eea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/163757eea) correction arbreDeuxEpreuvesIndep suite bugsnag (2021-11-04 18:01) <Rémi Deniaud>  
`* | | |   `[aa857c4f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa857c4f5) Merge branch 'zoneStyleSansParcours' into 'master' (2021-11-05 12:11) <barroy tommy>  
`|\ \ \ \  `  
`| * | | | `[d0d00e3b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0d00e3b1) bugsnag (2021-11-04 21:33) <Tommy>  
`| * | | | `[843a98c3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/843a98c3a) bugsnag (2021-11-04 20:35) <Tommy>  
`| * | | | `[1ada796d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ada796d4) bugsnag (2021-11-04 20:32) <Tommy>  
`| * | | | `[1a8e25d6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a8e25d6f) bugsnag (2021-11-04 20:27) <Tommy>  
`| * | | | `[d789b04bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d789b04bf) Merge branch 'master' into zoneStyleSansParcours (2021-11-04 19:12) <Tommy>  
`| |\| | | `  
`| * | | | `[4c3c33ee9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c3c33ee9) bugsnag (2021-11-04 18:57) <Tommy>  
`| * | | | `[225f831fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/225f831fb) on utilise getZoneParente plutôt que de chercher #MepMG (2021-11-04 13:47) <Daniel Caillibaud>  
`| * | | | `[4c3ed551f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c3ed551f) MqVirtualKeyboard utilise getZoneParente pour récupérer un boundingContainer si on ne l'avait pas précisé (2021-11-04 13:41) <Daniel Caillibaud>  
`| * | | | `[fb923323e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb923323e) Aj des fcts génériques getFirstParent, getJ3pConteneur, getZoneParente (2021-11-04 13:40) <Daniel Caillibaud>  
`* | | | |   `[a7cd24b3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7cd24b3d) Merge branch 'eslintProb' into 'master' (2021-11-05 12:10) <barroy tommy>  
`|\ \ \ \ \  `  
`| |_|_|/ /  `  
`|/| | | |   `  
`| * | | | `[94f628e72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94f628e72) bugsnag (2021-11-05 13:06) <Tommy>  
`|/ / / /  `  
`* | | |   `[86a0e6804](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86a0e6804) Merge branch 'CoEncadreEtnew' into 'master' (2021-11-05 01:26) <barroy tommy>  
`|\ \ \ \  `  
`| * | | | `[20367f244](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20367f244) bugsnag (2021-11-05 02:25) <Tommy>  
`|/ / / /  `  
`* | | |   `[fa9adeb79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa9adeb79) Merge branch 'oubliparam' into 'master' (2021-11-04 22:12) <barroy tommy>  
`|\ \ \ \  `  
`| * | | | `[bbac7e434](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bbac7e434) bugsnag (2021-11-04 23:10) <Tommy>  
`|/ / / /  `  
`* | | |   `[bdee99b83](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bdee99b83) Merge branch 'bugsNagPropor02' into 'master' (2021-11-04 21:27) <barroy tommy>  
`|\ \ \ \  `  
`| * | | | `[f206035de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f206035de) bugsnag (2021-11-04 22:26) <Tommy>  
`|/ / / /  `  
`* | | |   `[335ed15c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/335ed15c5) Merge branch 'bugde_1' into 'master' (2021-11-04 17:59) <barroy tommy>  
`|\ \ \ \  `  
`| |_|/ /  `  
`|/| | |   `  
`| * | | `[5d7826189](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d7826189) bugsnag (2021-11-04 18:59) <Tommy>  
`|/ / /  `  
`* | |   `[13684bf24](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13684bf24) Merge branch 'Ajout_Exercices_Produit_Scalaire' into 'master' (2021-11-04 15:52) <Yves Biton>  
`|\ \ \  `  
`| * | | `[b1ea6bbfe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1ea6bbfe) Ajout de deux exercices sur le produit scalaire. (2021-11-04 16:50) <Yves Biton>  
`* | | |   `[7f51bb12a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f51bb12a) Merge branch 'pbFreeze' into 'master' (2021-11-04 15:47) <Rémi Deniaud>  
`|\ \ \ \  `  
`| * | | | `[fe8a0faba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe8a0faba) le innerHTML peut planter dans freezeElt (2021-11-04 16:44) <Rémi Deniaud>  
`* | | | | `[ac6180766](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac6180766) retag 1.0.42 (2021-11-04 16:06) <Daniel Caillibaud>  
`* | | | |   `[42c7944f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42c7944f0) Merge branch 'hotfix' (2021-11-04 16:05) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`| * | | | `[c0042d919](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0042d919) fix bugsnag 6183f195f80dd40007993809 (2021-11-04 15:47) <Daniel Caillibaud>  
`| * | | | `[97dfd83bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97dfd83bc) rectif nomSection (2021-11-04 15:44) <Daniel Caillibaud>  
`* | | | |   `[6b8bf42c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b8bf42c2) Merge branch 'pbFreeze' into 'master' (2021-11-04 15:00) <Rémi Deniaud>  
`|\ \ \ \ \  `  
`| * \ \ \ \   `[535275d52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/535275d52) Merge branch 'pbsBugsnag' into 'master' (2021-11-04 15:55) <Rémi Deniaud>  
`| |\ \ \ \ \  `  
`| | * | | | | `[f5fb7b3b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5fb7b3b5) dernière modif dans EtudeFonction_signederivee (2021-11-04 15:55) <Rémi Deniaud>  
`| | * | | | | `[9e3a3b3db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e3a3b3db) oubli d'une modif dans EtudeFonction_limite (2021-11-04 15:55) <Rémi Deniaud>  
`| | * | | | | `[77be16435](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77be16435) deux pbs de nettoyage dans la dossier algorithmique (2021-11-04 15:55) <Rémi Deniaud>  
`| | * | | | | `[d6ff03fb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d6ff03fb5) dernière correction dans signederivee (2021-11-04 15:55) <Rémi Deniaud>  
`| | * | | | | `[c0e276e8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0e276e8e) correction dans EtudeFonction_signederivee du pb du signe qui disparaissait à la validation (2021-11-04 15:55) <Rémi Deniaud>  
`| | * | | | | `[90f595629](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90f595629) utilisation de stor plutôt que sec dans EtudeFonction_limite (2021-11-04 15:55) <Rémi Deniaud>  
`| | * | | | | `[c03ce9ef2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c03ce9ef2) correction de la gestion de stor.modele pour les études de fonction (2021-11-04 15:55) <Rémi Deniaud>  
`| | * | | | | `[57410531a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57410531a) suppression d'un console log.dans l'outil tableauSignesVariations (2021-11-04 15:55) <Rémi Deniaud>  
`| | * | | | | `[1db35a379](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1db35a379) correction pb comparaisonTableau (2021-11-04 15:55) <Rémi Deniaud>  
`| * | | | | |   `[5aed2f82b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5aed2f82b) Merge branch 'eslintPropor3' into 'master' (2021-11-04 15:55) <Daniel Caillibaud>  
`| |\ \ \ \ \ \  `  
`| | * | | | | | `[c9f33ec7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9f33ec7a) eslint (2021-11-04 15:55) <Tommy>  
`| * | | | | | |   `[3055acc3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3055acc3b) Merge branch 'thPythagore' into 'master' (2021-11-04 15:55) <Daniel Caillibaud>  
`| |\ \ \ \ \ \ \  `  
`| | * | | | | | | `[43a3a796f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43a3a796f) modif th de pythagore (2021-11-04 15:55) <Rémi Deniaud>  
`| | | |/ / / / /  `  
`| | |/| | | | |   `  
`| * | | | | | |   `[531027fbc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/531027fbc) Merge branch 'hotfix' into 'master' (2021-11-04 15:55) <Daniel Caillibaud>  
`| |\ \ \ \ \ \ \  `  
`| | * | | | | | | `[3f0b123db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f0b123db) fix erreurs eslint (reste plein de warnings) (2021-11-04 15:55) <Daniel Caillibaud>  
`| | * | | | | | | `[184cea975](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/184cea975) fix bugsnag 5d45817c26c58a7bd541326d (me dans un onclick en string) (2021-11-04 15:55) <Daniel Caillibaud>  
`| | * | | | | | | `[28c1401c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28c1401c9) Remplacement de tous les 'MepMG' par zonesElts.MG (2021-11-04 15:55) <Daniel Caillibaud>  
`| | * | | | | | | `[dc31a9cce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc31a9cce) fix bugsnag 6183cf209a00f10007da37b2 (élément invalide) (2021-11-04 15:55) <Daniel Caillibaud>  
`| | * | | | | | | `[086469d5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/086469d5e) fix erreur bugsnag 6183bcdc3c081400074aedbe (2021-11-04 15:55) <Daniel Caillibaud>  
`| |/ / / / / / /  `  
`| * | | | | | |   `[edc5855a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edc5855a8) Merge branch 'decompbugsna' into 'master' (2021-11-04 15:55) <barroy tommy>  
`| |\ \ \ \ \ \ \  `  
`| | * | | | | | | `[cf2b59601](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf2b59601) bugsnag (2021-11-04 15:55) <Tommy>  
`| |/ / / / / / /  `  
`| * | | | | | |   `[fed946a6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fed946a6e) Merge branch 'tatab' into 'master' (2021-11-04 15:55) <barroy tommy>  
`| |\ \ \ \ \ \ \  `  
`| | * | | | | | | `[bec33f733](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bec33f733) bugsnag (2021-11-04 15:55) <Tommy>  
`| |/ / / / / / /  `  
`| * | | | | | |   `[d52c3eb51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d52c3eb51) Merge branch 'ReTrigoBugAffNomAngle' into 'master' (2021-11-04 15:55) <barroy tommy>  
`| |\ \ \ \ \ \ \  `  
`| | * | | | | | | `[0a0b70a2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a0b70a2b) bug (2021-11-04 15:55) <Tommy>  
`| | | |/ / / / /  `  
`| | |/| | | | |   `  
`| * | | | | | | `[8c5760c5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c5760c5e) On ajoute une classe j3pConteneur au conteneur de chaque instance de Parcours (2021-11-04 15:55) <Daniel Caillibaud>  
`| | |/ / / / /  `  
`| |/| | | | |   `  
`| * | | | | |   `[8e3c65391](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e3c65391) Merge branch 'ineqDomaine' into 'master' (2021-11-04 15:55) <Rémi Deniaud>  
`| |\ \ \ \ \ \  `  
`| | * | | | | | `[2e08399f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e08399f4) correction inequationdomaine (2021-11-04 15:55) <Rémi Deniaud>  
`| |/ / / / / /  `  
`| * | | | | |   `[30ff6e06b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30ff6e06b) Merge branch 'reEslllll' into 'master' (2021-11-04 15:55) <barroy tommy>  
`| |\ \ \ \ \ \  `  
`| | * | | | | | `[01aa2bd44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01aa2bd44) eslint pas fini... (2021-11-04 15:55) <Tommy>  
`| |/ / / / / /  `  
`| * | | | | |   `[8eaf0dd72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8eaf0dd72) Merge branch 'eslint_et_bugzoneMathquill2' into 'master' (2021-11-04 15:55) <barroy tommy>  
`| |\ \ \ \ \ \  `  
`| | * | | | | | `[24efda1ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24efda1ad) autre bug (2021-11-04 15:55) <Tommy>  
`| | * | | | | | `[d151c4c6b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d151c4c6b) bug (2021-11-04 15:55) <Tommy>  
`| |/ / / / / /  `  
`| * | | | | |   `[3b5dede88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b5dede88) Merge branch 'nvlleortho' into 'master' (2021-11-04 15:55) <barroy tommy>  
`| |\ \ \ \ \ \  `  
`| | |/ / / / /  `  
`| |/| | | | |   `  
`| | * | | | | `[8d8f668d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d8f668d2) bug (2021-11-04 15:55) <Tommy>  
`| |/ / / / /  `  
`| * | | | | `[a9d60a27d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9d60a27d) tentative de gestion du freezeElt (2021-11-03 23:27) <Rémi Deniaud>  
`| * | | | | `[92f63dcb0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92f63dcb0) ajout clavier virtuel + correction j3pFocus dans l' outil tableauSignes (2021-11-03 19:10) <Rémi Deniaud>  
`| * | | | | `[0e044b17d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e044b17d) nettoyage outil tableauSignes (2021-11-03 18:56) <Rémi Deniaud>  
`| * | | | | `[0e2a9f0b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e2a9f0b9) nettoyage signeCourbe (2021-11-03 18:51) <Rémi Deniaud>  
`* | | | | |   `[41cbdfd89](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41cbdfd89) Merge branch 'pbsBugsnag' into 'master' (2021-11-04 14:53) <Rémi Deniaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[2927faf03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2927faf03) dernière modif dans EtudeFonction_signederivee (2021-11-04 15:52) <Rémi Deniaud>  
`| * | | | | | `[a6181f290](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6181f290) oubli d'une modif dans EtudeFonction_limite (2021-11-04 14:59) <Rémi Deniaud>  
`| * | | | | | `[7b66e4383](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b66e4383) deux pbs de nettoyage dans la dossier algorithmique (2021-11-04 14:46) <Rémi Deniaud>  
`| * | | | | | `[ab0687a64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab0687a64) dernière correction dans signederivee (2021-11-04 14:37) <Rémi Deniaud>  
`| * | | | | | `[bddcec225](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bddcec225) correction dans EtudeFonction_signederivee du pb du signe qui disparaissait à la validation (2021-11-04 11:26) <Rémi Deniaud>  
`| * | | | | | `[e06eb0bf1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e06eb0bf1) utilisation de stor plutôt que sec dans EtudeFonction_limite (2021-11-04 10:40) <Rémi Deniaud>  
`| * | | | | | `[12b15e0fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12b15e0fe) correction de la gestion de stor.modele pour les études de fonction (2021-11-04 10:33) <Rémi Deniaud>  
`| * | | | | | `[f2937bb47](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2937bb47) suppression d'un console log.dans l'outil tableauSignesVariations (2021-11-04 09:48) <Rémi Deniaud>  
`| * | | | | | `[fb5cf9111](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb5cf9111) correction pb comparaisonTableau (2021-11-04 09:14) <Rémi Deniaud>  
`| | |_|/ / /  `  
`| |/| | | |   `  
`* | | | | |   `[f12cb9439](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f12cb9439) Merge branch 'eslintPropor3' into 'master' (2021-11-04 14:32) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[871c44128](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/871c44128) eslint (2021-11-03 21:40) <Tommy>  
`| | |/ / / /  `  
`| |/| | | |   `  
`* | | | | |   `[32ca188ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32ca188ca) Merge branch 'thPythagore' into 'master' (2021-11-04 14:31) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[02aa015ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02aa015ff) modif th de pythagore (2021-11-04 09:00) <Rémi Deniaud>  
`| | |/ / / /  `  
`| |/| | | |   `  
`* | | | | |   `[8ca598799](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8ca598799) Merge branch 'hotfix' into 'master' (2021-11-04 14:30) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| | |_|/ / /  `  
`| |/| | | |   `  
`| * | | | | `[65c184bda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65c184bda) fix erreurs eslint (reste plein de warnings) (2021-11-04 15:24) <Daniel Caillibaud>  
`| * | | | | `[7ba1c7f74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ba1c7f74) fix bugsnag 5d45817c26c58a7bd541326d (me dans un onclick en string) (2021-11-04 15:23) <Daniel Caillibaud>  
`| * | | | | `[a62d97e03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a62d97e03) Remplacement de tous les 'MepMG' par zonesElts.MG (2021-11-04 15:23) <Daniel Caillibaud>  
`| * | | | | `[12c8c2463](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12c8c2463) fix bugsnag 6183cf209a00f10007da37b2 (élément invalide) (2021-11-04 15:22) <Daniel Caillibaud>  
`| * | | | | `[24d1df501](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24d1df501) fix erreur bugsnag 6183bcdc3c081400074aedbe (2021-11-04 15:01) <Daniel Caillibaud>  
`|/ / / / /  `  
`* | | | |   `[1e5935d96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e5935d96) Merge branch 'decompbugsna' into 'master' (2021-11-04 13:18) <barroy tommy>  
`|\ \ \ \ \  `  
`| * | | | | `[b45979c37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b45979c37) bugsnag (2021-11-04 14:16) <Tommy>  
`|/ / / / /  `  
`* | | | |   `[85d2a6b0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85d2a6b0f) Merge branch 'tatab' into 'master' (2021-11-04 12:52) <barroy tommy>  
`|\ \ \ \ \  `  
`| * | | | | `[23eb8d7b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23eb8d7b7) bugsnag (2021-11-04 13:44) <Tommy>  
`|/ / / / /  `  
`* | | | |   `[5485754bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5485754bc) Merge branch 'ReTrigoBugAffNomAngle' into 'master' (2021-11-04 12:41) <barroy tommy>  
`|\ \ \ \ \  `  
`| |_|_|/ /  `  
`|/| | | |   `  
`| * | | | `[51277f445](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51277f445) bug (2021-11-03 21:50) <Tommy>  
`| | |/ /  `  
`| |/| |   `  
`* | | | `[27524a260](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27524a260) On ajoute une classe j3pConteneur au conteneur de chaque instance de Parcours (2021-11-04 13:25) <Daniel Caillibaud>  
`| |/ /  `  
`|/| |   `  
`* | |   `[5adba19c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5adba19c1) Merge branch 'ineqDomaine' into 'master' (2021-11-04 07:21) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[bf940be5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf940be5e) correction inequationdomaine (2021-11-04 08:20) <Rémi Deniaud>  
`|/ / /  `  
`* | |   `[5822554b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5822554b7) Merge branch 'reEslllll' into 'master' (2021-11-03 22:34) <barroy tommy>  
`|\ \ \  `  
`| * | | `[d928d68bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d928d68bf) eslint pas fini... (2021-11-03 23:34) <Tommy>  
`|/ / /  `  
`* | |   `[cc352b7ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc352b7ce) Merge branch 'eslint_et_bugzoneMathquill2' into 'master' (2021-11-03 22:22) <barroy tommy>  
`|\ \ \  `  
`| * | | `[0c4937bb7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c4937bb7) autre bug (2021-11-03 23:20) <Tommy>  
`| * | | `[8a3080614](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a3080614) bug (2021-11-03 23:18) <Tommy>  
`|/ / /  `  
`* | |   `[80e1e2895](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80e1e2895) Merge branch 'nvlleortho' into 'master' (2021-11-03 21:33) <barroy tommy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[b53885b46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b53885b46) bug (2021-11-03 22:32) <Tommy>  
`|/ /  `  
`* |   `[93af960b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93af960b5) Merge branch 'manipulerListe' into 'master' (2021-11-03 17:19) <Rémi Deniaud>  
`|\ \  `  
`| * | `[dbec10eaf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dbec10eaf) remise en état de manipulerListe (2021-11-03 18:17) <Rémi Deniaud>  
`* | |   `[6264b2b0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6264b2b0e) Merge branch 'newEncadrer' into 'master' (2021-11-03 17:18) <barroy tommy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[52051b1ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52051b1ef) je mets une nvelle en plus mais elle est pas finie, mais fallait que je l'adapte aux nvelles zones (2021-11-03 18:17) <Tommy>  
`|/ /  `  
`* |   `[f092de35b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f092de35b) Merge branch 'Correction_Consigne_Exos_Produit_Scalaire' into 'master' (2021-11-03 15:59) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[c139ed5a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c139ed5a9) Amélioration de correction de 3 exercices (2021-11-03 16:56) <Yves Biton>  
`|/  `  
`*   `[15e4c5948](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15e4c5948) Merge branch 'addQueue' into 'master' (2021-11-03 14:37) <Daniel Caillibaud>  
`|\  `  
`| * `[a2c4d95b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2c4d95b7) Aj de Queue et ses tests unitaires (2021-11-03 15:08) <Daniel Caillibaud>  
`|/  `  
`*   `[dee48af01](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dee48af01) Merge branch 'refactoZoneFormuleStyleMathquill' into 'master' (2021-11-03 09:37) <barroy tommy>  
`|\  `  
`| * `[6b5fd0816](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b5fd0816) je mets une nvelle en plus mais elle est pas finie, mais fallait que je l'adapte aux nvelles zones (2021-11-03 10:36) <Tommy>  
`| * `[a9d291405](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9d291405) console.log virés (2021-11-03 10:24) <Daniel Caillibaud>  
`| *   `[054bc7c00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/054bc7c00) Merge branch 'master' into refactoZoneFormuleStyleMathquill (2021-11-03 10:15) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[a88d9f2b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a88d9f2b4) Merge branch 'ptitCoTrigoEtPaint' into 'master' (2021-11-02 22:47) <barroy tommy>  
`|\ \  `  
`| * | `[b2d696d43](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2d696d43) T'embete pas avec paint je referai comme t'as fait aux zonestylltruc, mais plus tard (2021-11-02 23:46) <Tommy>  
`|/ /  `  
`| *   `[5a079c88d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a079c88d) Merge branch 'master' into 'refactoZoneFormuleStyleMathquill' (2021-11-02 22:03) <barroy tommy>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[6d7e7a8a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d7e7a8a1) Merge branch 'EslintEtParcous_ATTENTION_WARNING' into 'master' (2021-11-02 20:17) <barroy tommy>  
`|\ \  `  
`| * | `[a384f5b5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a384f5b5e) modif de forme (2021-11-02 13:33) <Daniel Caillibaud>  
`| * | `[c5af921a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5af921a8) on préserve les classes css sur nos zones (MG & co) lors d'un resize (2021-11-02 13:32) <Daniel Caillibaud>  
`| * | `[d46c1b1de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d46c1b1de) Attention à VERIFIER (2021-11-01 23:01) <Tommy>  
`* | |   `[200fed8a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/200fed8a1) Merge branch 'rerereeslintStat01' into 'master' (2021-11-02 20:11) <barroy tommy>  
`|\ \ \  `  
`| * | | `[8cc8ab919](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cc8ab919) 3 bugs (2021-11-02 21:09) <Tommy>  
`| * | | `[ef540cf56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef540cf56) fix wrap des étiquettes (2021-10-30 19:58) <Daniel Caillibaud>  
`| * | | `[b0ebe9245](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0ebe9245) nettoyage et simplification : propriété lalalidTom remplacée par svg pour l'élément svg; propriétés idXxx renommées en eltXxx (c'est des éléments et pas des ids), findPos mutualisé; eslint multiples; … (2021-10-30 19:25) <Daniel Caillibaud>  
`| * | | `[cc2b925b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc2b925b8) finish pour l'instant (2021-10-30 16:38) <Tommy>  
`| * | | `[36647cf7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/36647cf7a) en cours (2021-10-30 11:29) <Tommy>  
`| * | | `[4d97d0d9a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d97d0d9a) en cours (2021-10-30 05:30) <Tommy>  
`* | | |   `[fba2fa2c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fba2fa2c5) Merge branch 'prb_clavierVirtuel' into 'master' (2021-11-02 16:47) <Rémi Deniaud>  
`|\ \ \ \  `  
`| * \ \ \   `[5a45a8a2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a45a8a2e) Merge branch 'master' into 'prb_clavierVirtuel' (2021-11-02 16:45) <Rémi Deniaud>  
`| |\ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`* | | | |   `[f3606638c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3606638c) Merge branch 'prb2_clavier' into 'master' (2021-11-02 16:03) <Rémi Deniaud>  
`|\ \ \ \ \  `  
`| * | | | | `[16bd62480](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16bd62480) dernière modif sur probaCondSuite (2021-11-02 17:00) <Rémi Deniaud>  
`| * | | | | `[7016806af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7016806af) nettoyage vaLoiProba (2021-11-02 16:35) <Rémi Deniaud>  
`| * | | | | `[6b7d723ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b7d723ab) nettoyage tabDoubleEntree (2021-11-02 16:29) <Rémi Deniaud>  
`| * | | | | `[cadb521ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cadb521ba) ajout claviervirtuel dans simulerVa (2021-11-02 16:19) <Rémi Deniaud>  
`| * | | | | `[96cef881c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96cef881c) nettoyage probaTableau (2021-11-02 16:16) <Rémi Deniaud>  
`| * | | | | `[b3554a44d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3554a44d) nettoyage probaLoiGeo (2021-11-02 16:02) <Rémi Deniaud>  
`| * | | | | `[58044a7e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58044a7e5) nettoyage probaCondTab (2021-11-02 15:53) <Rémi Deniaud>  
`| * | | | | `[aa467f68f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa467f68f) nettoyage probaCondSuite (2021-11-02 15:40) <Rémi Deniaud>  
`| * | | | | `[ea7f3ba18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea7f3ba18) nettoyage probaconditionnelles2 (2021-11-02 15:01) <Rémi Deniaud>  
`| * | | | | `[5f0118efd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f0118efd) nettoyage probaconditionnelles (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[0dee91cbb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0dee91cbb) nettoyage probabiliteLoi (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[125387d8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/125387d8d) nettoyage probabiliteCalculSimple (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[2abb4a223](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2abb4a223) nettoyage probabiliteArbre (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[c6589a956](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6589a956) nettoyage probaArbrePondere (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[df0fcec2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df0fcec2b) nettoyage nonEquiProba (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[280fca8aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/280fca8aa) nettoyage modeliserLoi (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[ddfd79487](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ddfd79487) nettoyage loinormaleSigma (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[a49ffa11a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a49ffa11a) nettoyage loinormalePrbSigma (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[ebc56569d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ebc56569d) nettoyage loinormaleCalculs (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[655382594](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/655382594) j'ai enlevé j3pRandom dans loinormaleAire (2021-11-02 14:45) <Rémi Deniaud>  
`| * | | | | `[93e8c9991](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93e8c9991) nettoyage loinormaleAire (2021-11-02 14:45) <Rémi Deniaud>  
`|/ / / / /  `  
`| * | | | `[22b8dcfc3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22b8dcfc3) ajout commentaires dans lecturesGraphiques/sectiontabVarLecture.js (2021-11-02 17:42) <Rémi Deniaud>  
`| * | | | `[504d7f8a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/504d7f8a2) petites modifs sur fluctuationBinomiale (2021-11-02 17:33) <Rémi Deniaud>  
`| * | | |   `[d7eebcbb4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7eebcbb4) Merge branch 'prb2_clavier' into 'master' (2021-11-02 17:12) <Rémi Deniaud>  
`| |\ \ \ \  `  
`| | * | | | `[400ce80e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/400ce80e7) dernière modif sur probaCondSuite (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[555343d8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/555343d8a) nettoyage vaLoiProba (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[77b8f875b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77b8f875b) nettoyage tabDoubleEntree (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[4b7fa0d37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b7fa0d37) ajout claviervirtuel dans simulerVa (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[a51d5b124](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a51d5b124) nettoyage probaTableau (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[06ef2b40a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06ef2b40a) nettoyage probaLoiGeo (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[fd2316e51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd2316e51) nettoyage probaCondTab (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[738995977](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/738995977) nettoyage probaCondSuite (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[a7c8cb8a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7c8cb8a6) nettoyage probaconditionnelles2 (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[c0f1faf22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0f1faf22) nettoyage probaconditionnelles (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[4d429261b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d429261b) nettoyage probabiliteLoi (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[58667eee0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58667eee0) nettoyage probabiliteCalculSimple (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[9e5fc0a2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e5fc0a2e) nettoyage probabiliteArbre (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[109f33795](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/109f33795) nettoyage probaArbrePondere (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[3ab938573](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ab938573) nettoyage nonEquiProba (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[17208c0eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17208c0eb) nettoyage modeliserLoi (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[1d238262c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d238262c) nettoyage loinormaleSigma (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[a5d7b9027](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5d7b9027) nettoyage loinormalePrbSigma (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[0a2dfa3e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a2dfa3e0) nettoyage loinormaleCalculs (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[5afc9d9f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5afc9d9f0) j'ai enlevé j3pRandom dans loinormaleAire (2021-11-02 17:12) <Rémi Deniaud>  
`| | * | | | `[7f19f7e42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f19f7e42) nettoyage loinormaleAire (2021-11-02 17:12) <Rémi Deniaud>  
`| |/ / / /  `  
`| * | | |   `[25f08ff89](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25f08ff89) Merge branch 'EslintPropor01' into 'master' (2021-11-02 17:12) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`| | * | | | `[d031c710b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d031c710b) eslint (2021-11-02 17:12) <Tommy>  
`| * | | | |   `[d27c9d24f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d27c9d24f) Merge branch 'eslintEtPtetBugsnagPremier01' into 'master' (2021-11-02 17:12) <Daniel Caillibaud>  
`| |\ \ \ \ \  `  
`| | * | | | | `[38ef594fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38ef594fb) eslint (2021-11-02 17:12) <Tommy>  
`| * | | | | |   `[436f059bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/436f059bc) Merge branch 'ReEsintPropor02' into 'master' (2021-11-02 17:12) <Daniel Caillibaud>  
`| |\ \ \ \ \ \  `  
`| * \ \ \ \ \ \   `[7e800ce22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e800ce22) Merge branch 'lecGraph_clavierVirtuel' into 'master' (2021-11-02 17:12) <Daniel Caillibaud>  
`| |\ \ \ \ \ \ \  `  
`| | * \ \ \ \ \ \   `[0cddff3b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0cddff3b6) Merge branch 'inequations_clavierVirtuel' into 'master' (2021-11-02 17:12) <Rémi Deniaud>  
`| | |\ \ \ \ \ \ \  `  
`| | | * | | | | | | `[43145e139](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43145e139) j'ai viré les j3pRandom des sections Inequations (2021-11-02 17:12) <Rémi Deniaud>  
`| | | * | | | | | |   `[3d462370e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d462370e) Merge remote-tracking branch 'origin/inequations_clavierVirtuel' into inequations_clavierVirtuel (2021-11-02 17:12) <Rémi Deniaud>  
`| | | |\ \ \ \ \ \ \  `  
`| | | | * | | | | | | `[3387028a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3387028a7) renommage zonesElts dans signeInegalites (2021-11-02 17:12) <Rémi Deniaud>  
`| | | | * | | | | | | `[bc0b38670](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc0b38670) clavier virtuel dans intervallesUnionInter (2021-11-02 17:12) <Rémi Deniaud>  
`| | | | * | | | | | | `[449a2dfbc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/449a2dfbc) rebase de inequationdomaine + inegalite_intervalle en camelCase (2021-11-02 17:12) <Rémi Deniaud>  
`| | | | * | | | | | | `[1474c390a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1474c390a) clavier virtuel dans inegalite_intervalle (2021-11-02 17:12) <Rémi Deniaud>  
`| | | | * | | | | | | `[8592c4ddc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8592c4ddc) permutation des éléments des deux zones pour que les mq soient à gauche (2021-11-02 17:12) <Rémi Deniaud>  
`| | | | * | | | | | | `[f329c1a6b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f329c1a6b) petite modif dans inequations1 (2021-11-02 17:12) <Rémi Deniaud>  
`| | | | * | | | | | | `[8fb9e9b0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8fb9e9b0f) mqRestriction dans inequations1 et inequations2 (2021-11-02 17:12) <Rémi Deniaud>  
`| | | * | | | | | | | `[0ae1907c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ae1907c5) renommage zonesElts dans signeInegalites (2021-11-02 17:12) <Rémi Deniaud>  
`| | | * | | | | | | | `[e1d441120](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1d441120) clavier virtuel dans intervallesUnionInter (2021-11-02 17:12) <Rémi Deniaud>  
`| | | * | | | | | | | `[a07b643c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a07b643c9) rebase de inequationdomaine + inegalite_intervalle en camelCase (2021-11-02 17:12) <Rémi Deniaud>  
`| | | * | | | | | | | `[17847a6b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17847a6b6) clavier virtuel dans inegalite_intervalle (2021-11-02 17:12) <Rémi Deniaud>  
`| | | * | | | | | | | `[a5c22565f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5c22565f) permutation des éléments des deux zones pour que les mq soient à gauche (2021-11-02 17:12) <Rémi Deniaud>  
`| | | * | | | | | | | `[32fe21ea2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32fe21ea2) petite modif dans inequations1 (2021-11-02 17:12) <Rémi Deniaud>  
`| | | * | | | | | | | `[47be4bf5b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47be4bf5b) mqRestriction dans inequations1 et inequations2 (2021-11-02 17:12) <Rémi Deniaud>  
`| | |/ / / / / / / /  `  
`| | * | | | | | | |   `[b62fae059](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b62fae059) Merge branch 'Amelioration_Squelette_Produit_Scalaire' into 'master' (2021-11-02 17:12) <Yves Biton>  
`| | |\ \ \ \ \ \ \ \  `  
`| | | |/ / / / / / /  `  
`| | |/| | | | | | |   `  
`| * | | | | | | | |   `[2b650c6ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b650c6ab) Merge branch '2ptiEslintEt_UnbugdepointerSouris' into 'master' (2021-11-02 17:12) <Daniel Caillibaud>  
`| |\ \ \ \ \ \ \ \ \  `  
`| | |_|_|_|_|/ / / /  `  
`| |/| | | | | | | |   `  
`| * | | | | | | | |   `[26dbf9961](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26dbf9961) Merge branch 'finalNomme02' into 'master' (2021-11-02 17:12) <barroy tommy>  
`| |\ \ \ \ \ \ \ \ \  `  
`| | * | | | | | | | | `[e34a9b378](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e34a9b378) pavedroit (2021-11-02 17:12) <Tommy>  
`| |/ / / / / / / / /  `  
`| * | | | | | | | |   `[f6701c956](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6701c956) Merge branch 'nomme02PrekFinish' into 'master' (2021-11-02 17:12) <barroy tommy>  
`| |\ \ \ \ \ \ \ \ \  `  
`| | * | | | | | | | | `[66bb6d336](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66bb6d336) pavedroit (2021-11-02 17:12) <Tommy>  
`| |/ / / / / / / / /  `  
`| * | | | | | | | |   `[243f09395](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/243f09395) Merge branch 'nomme2BugdeBug' into 'master' (2021-11-02 17:12) <barroy tommy>  
`| |\ \ \ \ \ \ \ \ \  `  
`| | |_|_|_|_|/ / / /  `  
`| |/| | | | | | | |   `  
`| | * | | | | | | | `[27413adae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27413adae) bug de bug (2021-11-02 17:12) <Tommy>  
`| |/ / / / / / / /  `  
`| * | | | | | | |   `[07e741d2a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07e741d2a) Merge branch 'nomm02Toprod' into 'master' (2021-11-02 17:12) <barroy tommy>  
`| |\ \ \ \ \ \ \ \  `  
`| | * | | | | | | | `[da09df2d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da09df2d1) pour bugsnag que je croyais avoir co (2021-11-02 17:12) <Tommy>  
`| |/ / / / / / / /  `  
`| * | | | | | | | `[56d03b766](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56d03b766) renommage des regex (test|capture)Intervalle en (test|capture)IntervalleFerme + aj de qq regex prenant des intervalles fermés ou ouverts + fix usage de ces regex dans fluctuationBinomiale (2021-11-02 12:25) <Daniel Caillibaud>  
`| * | | | | | | | `[6f623b8ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f623b8ee) Fix calcul du décalage du clavier lorsqu'il faut le translater (et que boundingContainer n'est pas à gauche) + boundingContainer devient facultatif (si non fourni on prend le premier .divZone parent) (2021-11-02 11:22) <Daniel Caillibaud>  
`| * | | | | | | | `[0bbb2bf37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0bbb2bf37) dernière modif dans arbreDeuxEpreuvesIndep (2021-11-01 23:09) <Rémi Deniaud>  
`| * | | | | | | | `[23bdd0394](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23bdd0394) nettoyage loiExpoTrouverLambda (2021-11-01 22:54) <Rémi Deniaud>  
`| * | | | | | | | `[a128c2355](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a128c2355) nettoyage loiExpoProbaCond (2021-11-01 22:26) <Rémi Deniaud>  
`| * | | | | | | | `[d5bc562d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5bc562d9) nettoyage loiExpoEsperance (2021-11-01 22:12) <Rémi Deniaud>  
`| * | | | | | | | `[6f547b0d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f547b0d6) nettoyage loiExpoCalculProba (2021-11-01 22:01) <Rémi Deniaud>  
`| * | | | | | | | `[ef35be3dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef35be3dc) nettoyage loiBinomiale (2021-11-01 21:41) <Rémi Deniaud>  
`| * | | | | | | | `[5a9341d7c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a9341d7c) nettoyage inverseBinomiale (2021-11-01 19:38) <Rémi Deniaud>  
`| * | | | | | | | `[26b97496b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26b97496b) nettoyage fluctuationBinomiale (2021-11-01 19:31) <Rémi Deniaud>  
`| * | | | | | | | `[4c37ff9a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c37ff9a7) nettoyage espeVarLoisIndep (2021-11-01 19:12) <Rémi Deniaud>  
`| * | | | | | | | `[747955ca0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/747955ca0) nettoyage espeEcartTypeVa (2021-11-01 19:02) <Rémi Deniaud>  
`| * | | | | | | | `[420b6e1a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/420b6e1a2) nettoyage diagrammeVenn (2021-11-01 19:00) <Rémi Deniaud>  
`| * | | | | | | | `[be25c2ea7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be25c2ea7) nettoyage denombrementSimple (2021-11-01 18:47) <Rémi Deniaud>  
`| * | | | | | | | `[87c0a8090](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87c0a8090) nettoyage denombrementArbre (2021-11-01 18:12) <Rémi Deniaud>  
`| * | | | | | | | `[1c66cd353](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c66cd353) nettoyage combinaisonsMultiples (2021-11-01 18:03) <Rémi Deniaud>  
`| * | | | | | | | `[01664479a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01664479a) nettoyage combinaisons (2021-11-01 17:55) <Rémi Deniaud>  
`| * | | | | | | | `[47342b97b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47342b97b) nettoyage calculPrbBinomiale (2021-11-01 17:47) <Rémi Deniaud>  
`| * | | | | | | | `[b6a54d88b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6a54d88b) nettoyage BienaymeTchebychev (2021-11-01 12:20) <Rémi Deniaud>  
`| * | | | | | | | `[49b15f793](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49b15f793) nettoyage arbreProbaCond + overline de la bonne couleur (2021-11-01 12:12) <Rémi Deniaud>  
`| * | | | | | | | `[3f88c1106](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f88c1106) nettoyage arbreDeuxEpreuvesIndep + clavier virtuel (2021-10-31 19:30) <Rémi Deniaud>  
`| * | | | | | | | `[1a786cc77](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a786cc77) nettoyage arbreDeuxEpreuvesIndep (2021-10-31 17:24) <Rémi Deniaud>  
`| * | | | | | | | `[5eddd9354](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5eddd9354) pb du focus que j'ai en partie réglé (2021-10-31 16:06) <Rémi Deniaud>  
`| * | | | | | | | `[227d8d794](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/227d8d794) nettoyage IntersectionDroitesAxes (2021-10-31 16:06) <Rémi Deniaud>  
`| * | | | | | | | `[9dda8d376](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9dda8d376) nettoyage imageAntecedents (2021-10-31 16:06) <Rémi Deniaud>  
`| * | | | | | | | `[ab2aa5e81](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab2aa5e81) nettoyage ExtremumLecture + dernières modifs comparaisonTableau (2021-10-31 16:06) <Rémi Deniaud>  
`| * | | | | | | | `[68d765c16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68d765c16) nettoyage equationFctsLecture (2021-10-31 16:06) <Rémi Deniaud>  
`| * | | | | | | | `[647338209](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/647338209) nettoyage comparaisonTableau (2021-10-31 16:06) <Rémi Deniaud>  
`| | | | | | | | * `[f33927096](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f33927096) je crois que c'est tout bon, j'ai pas commit le fichier sectionmanipulerListe.js, mais il buggait quand j'ai fait pull (2021-11-02 22:58) <Tommy>  
`| | | | | | | | * `[fcede440a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fcede440a) On crée de vraies méthodes pour ZoneFormuleStyleMathquill3 (dans le prototype), this est toujours l'objet ZoneFormuleStyleMathquill3 dans tout le fichier (on passe par bind pour que les listeners s'y retrouvent, et on utilise currentTarget pour récupérer l'élément html que le listener aurait eu en this sans ce bind) (2021-11-02 18:12) <Daniel Caillibaud>  
`| | | | | | | | *   `[66ea749a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66ea749a3) Merge branch 'master' into refactoZoneFormuleStyleMathquill (2021-11-02 17:21) <Daniel Caillibaud>  
`| | | | | | | | |\  `  
`| |_|_|_|_|_|_|_|/  `  
`|/| | | | | | | |   `  
`* | | | | | | | |   `[76a147e79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76a147e79) Merge branch 'EslintPropor01' into 'master' (2021-11-02 11:45) <Daniel Caillibaud>  
`|\ \ \ \ \ \ \ \ \  `  
`| * | | | | | | | | `[25362029b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25362029b) eslint (2021-11-02 00:31) <Tommy>  
`| | |_|_|_|_|_|/ /  `  
`| |/| | | | | | |   `  
`* | | | | | | | |   `[4063489a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4063489a2) Merge branch 'eslintEtPtetBugsnagPremier01' into 'master' (2021-11-02 11:45) <Daniel Caillibaud>  
`|\ \ \ \ \ \ \ \ \  `  
`| * | | | | | | | | `[bacdb2a15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bacdb2a15) eslint (2021-11-01 00:15) <Tommy>  
`* | | | | | | | | |   `[c484573ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c484573ff) Merge branch 'ReEsintPropor02' into 'master' (2021-11-02 11:44) <Daniel Caillibaud>  
`|\ \ \ \ \ \ \ \ \ \  `  
`| | |_|_|_|_|_|/ / /  `  
`| |/| | | | | | | |   `  
`| * | | | | | | | | `[42f75f6cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42f75f6cc) j'y reviendrai mais dans longtemps, là vaut mieux tout de suite (2021-10-31 21:35) <Tommy>  
`* | | | | | | | | |   `[e4673b12c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4673b12c) Merge branch 'lecGraph_clavierVirtuel' into 'master' (2021-11-02 11:43) <Daniel Caillibaud>  
`|\ \ \ \ \ \ \ \ \ \  `  
`| * \ \ \ \ \ \ \ \ \   `[f8fbc124a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8fbc124a) Merge branch 'inequations_clavierVirtuel' into 'master' (2021-10-31 17:25) <Rémi Deniaud>  
`| |\ \ \ \ \ \ \ \ \ \  `  
`| | * | | | | | | | | | `[2a6a1c029](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a6a1c029) j'ai viré les j3pRandom des sections Inequations (2021-10-31 17:25) <Rémi Deniaud>  
`| | * | | | | | | | | |   `[4b14bbfd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b14bbfd0) Merge remote-tracking branch 'origin/inequations_clavierVirtuel' into inequations_clavierVirtuel (2021-10-31 17:25) <Rémi Deniaud>  
`| | |\ \ \ \ \ \ \ \ \ \  `  
`| | | * | | | | | | | | | `[af2ec5d30](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af2ec5d30) renommage zonesElts dans signeInegalites (2021-10-31 17:25) <Rémi Deniaud>  
`| | | * | | | | | | | | | `[1301c2f67](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1301c2f67) clavier virtuel dans intervallesUnionInter (2021-10-31 17:25) <Rémi Deniaud>  
`| | | * | | | | | | | | | `[51a3fbb23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51a3fbb23) rebase de inequationdomaine + inegalite_intervalle en camelCase (2021-10-31 17:25) <Rémi Deniaud>  
`| | | * | | | | | | | | | `[319a55953](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/319a55953) clavier virtuel dans inegalite_intervalle (2021-10-31 17:25) <Rémi Deniaud>  
`| | | * | | | | | | | | | `[1db8bea4f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1db8bea4f) permutation des éléments des deux zones pour que les mq soient à gauche (2021-10-31 17:25) <Rémi Deniaud>  
`| | | * | | | | | | | | | `[fa2225b8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa2225b8b) petite modif dans inequations1 (2021-10-31 17:25) <Rémi Deniaud>  
`| | | * | | | | | | | | | `[f9236e0a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9236e0a9) mqRestriction dans inequations1 et inequations2 (2021-10-31 17:25) <Rémi Deniaud>  
`| | * | | | | | | | | | | `[5448cde2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5448cde2f) renommage zonesElts dans signeInegalites (2021-10-31 17:25) <Rémi Deniaud>  
`| | * | | | | | | | | | | `[6dbccdd9d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6dbccdd9d) clavier virtuel dans intervallesUnionInter (2021-10-31 17:25) <Rémi Deniaud>  
`| | * | | | | | | | | | | `[846aa19b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/846aa19b3) rebase de inequationdomaine + inegalite_intervalle en camelCase (2021-10-31 17:25) <Rémi Deniaud>  
`| | * | | | | | | | | | | `[d96ecd6b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d96ecd6b4) clavier virtuel dans inegalite_intervalle (2021-10-31 17:25) <Rémi Deniaud>  
`| | * | | | | | | | | | | `[331250f16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/331250f16) permutation des éléments des deux zones pour que les mq soient à gauche (2021-10-31 17:25) <Rémi Deniaud>  
`| | * | | | | | | | | | | `[5d2874341](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d2874341) petite modif dans inequations1 (2021-10-31 17:25) <Rémi Deniaud>  
`| | * | | | | | | | | | | `[cfa7a0b3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfa7a0b3a) mqRestriction dans inequations1 et inequations2 (2021-10-31 17:25) <Rémi Deniaud>  
`| |/ / / / / / / / / / /  `  
`| * | | | | | | | | | |   `[c9180d01e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9180d01e) Merge branch 'Amelioration_Squelette_Produit_Scalaire' into 'master' (2021-10-31 17:25) <Yves Biton>  
`| |\ \ \ \ \ \ \ \ \ \ \  `  
`| | |/ / / / / / / / / /  `  
`| |/| | | | | | | / / /   `  
`| | | |_|_|_|_|_|/ / /    `  
`| | |/| | | | | | | |     `  
`| * | | | | | | | | | `[3c1f0b73c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c1f0b73c) pb du focus que j'ai en partie réglé (2021-10-30 18:48) <Rémi Deniaud>  
`| * | | | | | | | | | `[3e9ed3bab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e9ed3bab) nettoyage IntersectionDroitesAxes (2021-10-30 18:48) <Rémi Deniaud>  
`| * | | | | | | | | | `[5e6254d3c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e6254d3c) nettoyage imageAntecedents (2021-10-30 18:48) <Rémi Deniaud>  
`| * | | | | | | | | | `[17131c2ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17131c2ab) nettoyage ExtremumLecture + dernières modifs comparaisonTableau (2021-10-30 18:48) <Rémi Deniaud>  
`| * | | | | | | | | | `[ecc7fb9d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecc7fb9d8) nettoyage equationFctsLecture (2021-10-30 18:48) <Rémi Deniaud>  
`| * | | | | | | | | | `[f3276092f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3276092f) nettoyage comparaisonTableau (2021-10-30 18:48) <Rémi Deniaud>  
`| | |_|_|_|_|_|/ / /  `  
`| |/| | | | | | | |   `  
`* | | | | | | | | |   `[9f684b483](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f684b483) Merge branch '2ptiEslintEt_UnbugdepointerSouris' into 'master' (2021-11-02 11:42) <Daniel Caillibaud>  
`|\ \ \ \ \ \ \ \ \ \  `  
`| |_|_|_|_|/ / / / /  `  
`|/| | | | | | / / /   `  
`| | |_|_|_|_|/ / /    `  
`| |/| | | | | | |     `  
`| * | | | | | | | `[0947878a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0947878a0) finish pour l'instant (2021-10-30 17:24) <Tommy>  
`| |/ / / / / / /  `  
`* | | | | | | |   `[1acffaadd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1acffaadd) Merge branch 'finalNomme02' into 'master' (2021-11-01 20:40) <barroy tommy>  
`|\ \ \ \ \ \ \ \  `  
`| * | | | | | | | `[c706e1c23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c706e1c23) pavedroit (2021-11-01 21:38) <Tommy>  
`|/ / / / / / / /  `  
`* | | | | | | |   `[a3e39028b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3e39028b) Merge branch 'nomme02PrekFinish' into 'master' (2021-11-01 14:10) <barroy tommy>  
`|\ \ \ \ \ \ \ \  `  
`| * | | | | | | | `[ccbdf1ade](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ccbdf1ade) pavedroit (2021-11-01 15:09) <Tommy>  
`|/ / / / / / / /  `  
`* | | | | | | |   `[aeeb45499](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aeeb45499) Merge branch 'nomme2BugdeBug' into 'master' (2021-10-31 23:24) <barroy tommy>  
`|\ \ \ \ \ \ \ \  `  
`| |_|_|_|/ / / /  `  
`|/| | | | | | |   `  
`| * | | | | | | `[ea2714cfa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea2714cfa) bug de bug (2021-11-01 00:23) <Tommy>  
`|/ / / / / / /  `  
`* | | | | | |   `[6b53dc330](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b53dc330) Merge branch 'nomm02Toprod' into 'master' (2021-10-31 20:44) <barroy tommy>  
`|\ \ \ \ \ \ \  `  
`| |_|_|_|/ / /  `  
`|/| | | | | |   `  
`| * | | | | | `[5611e2d30](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5611e2d30) pour bugsnag que je croyais avoir co (2021-10-31 21:43) <Tommy>  
`|/ / / / / /  `  
`* | | | | |   `[725c883b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/725c883b7) Merge branch 'inequations_clavierVirtuel' into 'master' (2021-10-31 14:09) <Rémi Deniaud>  
`|\ \ \ \ \ \  `  
`| |_|_|/ / /  `  
`|/| | | | |   `  
`| * | | | | `[c53b2fe27](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c53b2fe27) j'ai viré les j3pRandom des sections Inequations (2021-10-31 15:08) <Rémi Deniaud>  
`| * | | | |   `[daa2227ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/daa2227ff) Merge remote-tracking branch 'origin/inequations_clavierVirtuel' into inequations_clavierVirtuel (2021-10-31 14:55) <Rémi Deniaud>  
`| |\ \ \ \ \  `  
`| | * | | | | `[0b8984920](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b8984920) renommage zonesElts dans signeInegalites (2021-10-30 18:43) <Rémi Deniaud>  
`| | * | | | | `[ca9d49fb2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca9d49fb2) clavier virtuel dans intervallesUnionInter (2021-10-30 18:43) <Rémi Deniaud>  
`| | * | | | | `[95d738fac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95d738fac) rebase de inequationdomaine + inegalite_intervalle en camelCase (2021-10-30 18:38) <Rémi Deniaud>  
`| | * | | | | `[0e9d86b12](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e9d86b12) clavier virtuel dans inegalite_intervalle (2021-10-30 18:32) <Rémi Deniaud>  
`| | * | | | | `[62664e821](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62664e821) permutation des éléments des deux zones pour que les mq soient à gauche (2021-10-30 18:32) <Rémi Deniaud>  
`| | * | | | | `[4b61c5159](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b61c5159) petite modif dans inequations1 (2021-10-30 18:32) <Rémi Deniaud>  
`| | * | | | | `[15481175b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15481175b) mqRestriction dans inequations1 et inequations2 (2021-10-30 18:32) <Rémi Deniaud>  
`| | |/ / / /  `  
`| * | | | | `[b179cade7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b179cade7) renommage zonesElts dans signeInegalites (2021-10-31 14:55) <Rémi Deniaud>  
`| * | | | | `[9509f2dbe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9509f2dbe) clavier virtuel dans intervallesUnionInter (2021-10-31 14:55) <Rémi Deniaud>  
`| * | | | | `[a28991e1e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a28991e1e) rebase de inequationdomaine + inegalite_intervalle en camelCase (2021-10-31 14:55) <Rémi Deniaud>  
`| * | | | | `[4122622f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4122622f3) clavier virtuel dans inegalite_intervalle (2021-10-31 14:55) <Rémi Deniaud>  
`| * | | | | `[9a6476add](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a6476add) permutation des éléments des deux zones pour que les mq soient à gauche (2021-10-31 14:55) <Rémi Deniaud>  
`| * | | | | `[fa6857631](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa6857631) petite modif dans inequations1 (2021-10-31 14:55) <Rémi Deniaud>  
`| * | | | | `[f284dfb28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f284dfb28) mqRestriction dans inequations1 et inequations2 (2021-10-31 14:55) <Rémi Deniaud>  
`|/ / / / /  `  
`* | | | |   `[e75003cf3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e75003cf3) Merge branch 'Amelioration_Squelette_Produit_Scalaire' into 'master' (2021-10-30 19:37) <Yves Biton>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| / / /   `  
`| |/ / /    `  
`| * / / `[6afc5ac90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6afc5ac90) Version du squelette de calcul de produit scalaire permettant d'utiliser des carrés scalaires. Des ajouts au moteur de calcul de mtg32 ont été nécessaires et la version correspondante a été déployée. (2021-10-30 20:30) <Yves Biton>  
`|  / /  `  
`* | | `[1e76cda89](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e76cda89) fct pgcd locale remplacée par j3pPGCD avec les bonnes options (2021-10-30 16:04) <Daniel Caillibaud>  
`* | |   `[64d1117b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64d1117b8) Merge branch 'fixPgcdLaxiste' into 'master' (2021-10-30 13:52) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[6dc1eb051](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6dc1eb051) On remet les fonctionnements un peu bizarres de calcul de pgcd qui préexistaient à la mutualisation via j3pPGCD, en ajoutant des options à cette fonctions (quoi faire si l'un des deux est nul, accepterles négatifs ou pas, retourner undefined ou planter en cas de calcul impossible) (2021-10-30 15:40) <Daniel Caillibaud>  
`|/ / /  `  
`| | * `[5db007598](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5db007598) création de vraies méthodes (via prototype) pour ZoneFormuleStyleMathquill2 (2021-10-30 13:48) <Daniel Caillibaud>  
`| | * `[57edf6efb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57edf6efb) fix qq pb de this qui restaient (2021-10-30 13:25) <Daniel Caillibaud>  
`| | *   `[b726c2565](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b726c2565) Merge branch 'master' into refactoZoneFormuleStyleMathquill (2021-10-30 12:58) <Daniel Caillibaud>  
`| | |\  `  
`| |_|/  `  
`|/| |   `  
`* | | `[47d805a0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47d805a0c) bricoles (2021-10-30 12:57) <Daniel Caillibaud>  
`|/ /  `  
`| * `[30fb987e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30fb987e3) On crée de vraies méthodes pour ZoneFormuleStyleMathquill (dans le prototype), this est toujours l'objet ZoneFormuleStyleMathquill dans tout le fichier (on passe par bind pour que les listeners s'y retrouvent, et on utilise currentTarget pour récupérer l'élément html que le listener aurait eu en this sans ce bind) (2021-10-30 12:57) <Daniel Caillibaud>  
`| * `[bdb5bae0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bdb5bae0c) bricoles (2021-10-30 12:57) <Daniel Caillibaud>  
`| * `[ac18e0dca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac18e0dca) forceblur viré + mod css (2021-10-30 11:39) <Daniel Caillibaud>  
`| * `[912987569](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/912987569) restait du Etendre (2021-10-30 11:35) <Daniel Caillibaud>  
`| *   `[e85a2c2ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e85a2c2ab) Merge branch 'master' into refactoZoneFormuleStyleMathquill (2021-10-30 10:56) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[cfdba0e3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfdba0e3d) Merge branch 'decomp02ETbugsnagZone2' into 'master' (2021-10-30 03:20) <barroy tommy>  
`|\ \  `  
`| * | `[9eff45d84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9eff45d84) en cours (2021-10-30 05:19) <Tommy>  
`|/ /  `  
`* |   `[476842b64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/476842b64) Merge branch 'newDecomp' into 'master' (2021-10-30 00:56) <barroy tommy>  
`|\ \  `  
`| * | `[b19eed3f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b19eed3f5) en cours (2021-10-30 02:54) <Tommy>  
`| * | `[bebc7a48d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bebc7a48d) eslint sans div et affnomangle (2021-10-29 20:58) <Tommy>  
`|  /  `  
`* |   `[243e3bfa7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/243e3bfa7) Merge branch 'bugsnagDecomp01Encore' into 'master' (2021-10-29 19:43) <barroy tommy>  
`|\ \  `  
`| * | `[c54583a4f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c54583a4f) to prod (2021-10-29 21:41) <Tommy>  
`|/ /  `  
`* |   `[4ac4bbd78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ac4bbd78) Merge branch 'bugsNaGGnomme0202' into 'master' (2021-10-29 19:31) <barroy tommy>  
`|\ \  `  
`| * | `[667606df1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/667606df1) to prod encore (2021-10-29 21:30) <Tommy>  
`|/ /  `  
`* |   `[bf3624268](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf3624268) Merge branch 'buGsnagNomme02' into 'master' (2021-10-29 19:13) <barroy tommy>  
`|\ \  `  
`| * | `[ea80af86e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea80af86e) to prod (2021-10-29 21:13) <Tommy>  
`|/ /  `  
`* | `[50fc3c377](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50fc3c377) aj de l'option alsoFloat à j3pGetBornesIntervalle (2021-10-29 19:56) <Daniel Caillibaud>  
`* | `[c3115f2b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c3115f2b8) retag 1.0.41 (2021-10-29 19:32) <robot j3p@dev>  
`* | `[ce4a489a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce4a489a7) Merge branch 'hotfix' into 'master' (2021-10-29 17:32) <Daniel Caillibaud>  
`* | `[b8ed4c196](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8ed4c196) fix plantage sur les couleurs (couleurs est pas un array donc couleurs[i][j] plante) (2021-10-29 19:11) <Daniel Caillibaud>  
`* | `[e2f683bc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2f683bc4) fix options par défaut j3pPGCD (2021-10-29 19:10) <Daniel Caillibaud>  
` /  `  
`* `[462a58538](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/462a58538) Merge branch 'master' into refactoZoneFormuleStyleMathquill (2021-10-27 18:36) <Daniel Caillibaud>  
`* `[719773b22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/719773b22) Merge branch 'master' into refactoZoneFormuleStyleMathquill (2021-10-27 18:10) <Daniel Caillibaud>  
`* `[b5ca2b6bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5ca2b6bb) Merge branch 'master' into refactoZoneFormuleStyleMathquill (2021-10-26 18:51) <Daniel Caillibaud>  
`* `[96ceb3cb1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96ceb3cb1) ZoneFormuleStyleMathquill se passe de j3p (avec Etendre => etendre) (2021-10-26 18:40) <Daniel Caillibaud>  
`* `[1158c6847](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1158c6847) ZoneFormuleStyleMathquill2 et 3 se passent de j3p (2021-10-26 18:40) <Daniel Caillibaud>  
`* `[55ba52d66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55ba52d66) export des styles de l'instance j3p directement (2021-10-26 18:35) <Daniel Caillibaud>  
`* `[927d9579d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/927d9579d) NombreBienEcritEspace => nombreBienEcritEspace et EcrisbienMAthquill => ecrisBienMathquill (2021-10-26 17:11) <Daniel Caillibaud>  
`* `[dd1451f9c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd1451f9c) bugfix affcofo (variable repeleve non initialisée) (2021-10-26 17:04) <Daniel Caillibaud>  
`* `[44098a189](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44098a189) bugfix var cip undef (2021-10-26 16:32) <Daniel Caillibaud>  
`* `[50e9eb043](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50e9eb043) refacto des 3 ZoneFormuleStyleMathquill, chacune dans son fichier js, avec css externalisées + mutualisation de affNomAngle dans les sections (qui n'appellent plus directement les png) (2021-10-26 16:15) <Daniel Caillibaud>  

## version 1.0.40

`* `[5de9bc47b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5de9bc47b) retag 1.0.40 (2021-10-29 17:14) <robot j3p@dev>  
`*   `[8d0fa59b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d0fa59b7) Merge branch 'nomme02Cyl' into 'master' (2021-10-29 15:13) <Daniel Caillibaud>  
`|\  `  
`| *   `[eb5991d15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb5991d15) Merge branch 'master' into nomme02Cyl (2021-10-29 17:13) <Daniel Caillibaud>  
`| |\  `  
`| * | `[fb21a450e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb21a450e) simplification de code (2021-10-29 17:11) <Daniel Caillibaud>  
`| * | `[0ac6a8335](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ac6a8335) cyl (2021-10-27 23:52) <Tommy>  
`* | |   `[bd2038e73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd2038e73) Merge branch 'hotfix' into 'master' (2021-10-29 14:59) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[9f3bdf7a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f3bdf7a2) j3pDesactive ne plante plus si on l'appelle sur un élément déjà gelé (il râle en console), cf https://app.bugsnag.com/sesamath/j3p/errors/617b98edab259e000871d4fd (2021-10-29 16:53) <Daniel Caillibaud>  
`| * | | `[d3a995030](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3a995030) fix bugsnag 617ba7b7a27e34000862c687 (cip undef), c'était déjà corrigé mais dans une branche pas encore mergée (2021-10-29 16:46) <Daniel Caillibaud>  
`| * | | `[90a395e1e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90a395e1e) calcul de pgcd mutualisé entre toutes les sections qui le redéfinissait, aj d'un pgcdMulti, avec tests unitaires (2021-10-29 16:44) <Daniel Caillibaud>  
`| * | | `[a267e6578](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a267e6578) alert() virés ou remplacés par du console.error ou j3pShowError (2021-10-29 15:14) <Daniel Caillibaud>  
`| * | | `[ffc2be4c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffc2be4c9) check alert() mis en commentaires dans le hook pre-commit (2021-10-29 11:12) <Daniel Caillibaud>  
`| | |/  `  
`| |/|   `  
`* | |   `[795d25bda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/795d25bda) Merge branch 'newElsintTrigo' into 'master' (2021-10-29 14:58) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[3788e5ee7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3788e5ee7) eslint sans div et affnomangle (2021-10-29 12:55) <Tommy>  
`| * | | `[6fc62f04c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6fc62f04c) eslint sans div et affnomangle (2021-10-29 12:55) <Tommy>  
`| * | | `[37bec04a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37bec04a5) eslintTrigo2 (2021-10-29 11:41) <Tommy>  
`* | | |   `[47f765a53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47f765a53) Merge branch 'Correction_Squelettes_Vecteurs' into 'master' (2021-10-29 11:42) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[615c1f9e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/615c1f9e0) Correction squelette de calcul vectoriel suite rapport LaboMep (2021-10-29 13:40) <Yves Biton>  
`|/ / / /  `  
`* | | |   `[1aa31c795](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1aa31c795) Merge branch 'decomp2' into 'master' (2021-10-29 09:49) <barroy tommy>  
`|\ \ \ \  `  
`| |_|/ /  `  
`|/| | |   `  
`| * | | `[2bef95732](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2bef95732) pour prod (2021-10-29 11:48) <Tommy>  
`|/ / /  `  
`* | |   `[56ff38a38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56ff38a38) Merge branch 'docParcoursDonnees' into 'master' (2021-10-29 07:33) <Rémi Deniaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[81eb8b2f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81eb8b2f5) Etendre => etendre (2021-10-27 18:05) <Daniel Caillibaud>  
`| * |   `[f1e21e25f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1e21e25f) Merge branch 'master' into docParcoursDonnees (2021-10-27 18:04) <Daniel Caillibaud>  
`| |\ \  `  
`| * | | `[e93d0b3b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e93d0b3b5) gros chantier pour les fonctions communes des études de fonctions avec suppression de tous les appels j3p et du nettoyage de code (2021-10-27 16:05) <Rémi Deniaud>  
`| * | | `[62c3ca431](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62c3ca431) derniers ajustements dans les sections sur l'étude de fonction (2021-10-27 16:03) <Rémi Deniaud>  
`| * | | `[b65786117](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b65786117) ajout de stor dans EtudeFonction_variations (2021-10-27 14:40) <Rémi Deniaud>  
`| * | | `[991057d46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/991057d46) nettoyage EtudeFonction_signederivee (2021-10-27 14:08) <Rémi Deniaud>  
`| * | | `[bd2a3f3e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd2a3f3e5) nettoyage EtudeFonction_facteursderivee (2021-10-27 09:12) <Rémi Deniaud>  
`| * | | `[118401032](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/118401032) nettoyage EtudeFonction_derivee (2021-10-27 08:59) <Rémi Deniaud>  
`| * | | `[7ddd3da9b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ddd3da9b) nouvelles modifs de nettoyage dans EtudeFonction_variations (2021-10-26 23:04) <Rémi Deniaud>  
`| * | | `[2d8d201ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d8d201ca) nettoyage EtudeFonction_limite (2021-10-26 22:55) <Rémi Deniaud>  
`| * | | `[fc752316a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc752316a) nettoyage EtudeFonction_variations (2021-10-26 22:11) <Rémi Deniaud>  
`| * | |   `[fe19140cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe19140cf) Merge branch 'docParcoursDonnees' of framagit.org:Sesamath/sesaparcours into docParcoursDonnees (2021-10-26 21:26) <Rémi Deniaud>  
`| |\ \ \  `  
`| | * | | `[99e4c0d8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99e4c0d8a) aj de doc (2021-10-06 11:18) <Daniel Caillibaud>  
`| | * | | `[75b21da2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75b21da2f) Simplification et fixes eslint (2021-10-06 10:28) <Daniel Caillibaud>  
`| |  / /  `  
`| * | | `[d06df41d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d06df41d5) gestion deriveeRestante (2021-10-26 21:20) <Rémi Deniaud>  
`| * | | `[818c4ba7c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/818c4ba7c) aj de doc (2021-10-26 21:20) <Daniel Caillibaud>  
`| * | | `[4572f98fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4572f98fa) Simplification et fixes eslint (2021-10-26 21:20) <Daniel Caillibaud>  
`| | |/  `  
`| |/|   `  
`* | | `[9947f4433](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9947f4433) retag 1.0.39 (2021-10-28 18:16) <robot j3p@dev>  
`* | |   `[96280ee30](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96280ee30) Merge branch 'Correction_Squelette_Produit_Scalaire' into 'master' (2021-10-28 16:09) <Yves Biton>  
`|\ \ \  `  
`| * | | `[c6dfd7f3c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6dfd7f3c) Correction squelette de calcul de produit scalaire. Il restait des défauts. (2021-10-28 18:01) <Yves Biton>  
`* | | | `[a51e3ab98](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a51e3ab98) retag 1.0.38 (2021-10-28 16:23) <robot j3p@dev>  
`* | | | `[42e7246f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42e7246f8) Merge branch 'Correction_Squelette_Produit_Scalaire' into 'master' (2021-10-28 14:22) <Yves Biton>  
`|\| | | `  
`| * | | `[9b5422299](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b5422299) Correction squelette de calcul de produit scalaire. ON gère maintenant tout type de produit scalaire, y-compris avec des parenthèses. (2021-10-28 16:19) <Yves Biton>  
`* | | | `[1656c7c69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1656c7c69) retag 1.0.37 (2021-10-28 11:07) <robot j3p@dev>  
`|/ / /  `  
`* | |   `[de086d474](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de086d474) Merge branch 'Correction_Suite_Rapport_Labomep' into 'master' (2021-10-27 16:46) <Yves Biton>  
`|\ \ \  `  
`| * | | `[ae931b563](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae931b563) Correction squelette. Le symbole union doit être présent par défaut. (2021-10-27 18:44) <Yves Biton>  
`| |/ /  `  
`* | |   `[1c8a90d0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c8a90d0e) Merge branch 'suites' (2021-10-27 18:27) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[8cc469b53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cc469b53) manquait ce fichier après le merge de master (2021-10-27 18:27) <Daniel Caillibaud>  
`* | | | `[7d7f0edd2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d7f0edd2) Merge branch 'suites' into 'master' (2021-10-27 16:25) <Daniel Caillibaud>  
`|\| | | `  
`| * | |   `[6e888642e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e888642e) Merge branch 'master' into suites (2021-10-27 18:25) <Daniel Caillibaud>  
`| |\ \ \  `  
`| | | |/  `  
`| | |/|   `  
`| * | | `[782d5a0e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/782d5a0e6) nettoyage + modif de la validation pour corriger un bug (2021-10-25 16:16) <Rémi Deniaud>  
`| * | | `[5b3aba56f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b3aba56f) modif j3pFreezeElt (2021-10-25 11:07) <Rémi Deniaud>  
`| * | | `[eda749cac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eda749cac) nettoyage convergenceSuite (2021-10-25 11:04) <Rémi Deniaud>  
`* | | |   `[78b617704](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78b617704) Merge branch 'eslintVolumeCompt' into 'master' (2021-10-27 16:23) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * \ \ \   `[7bb535c17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7bb535c17) Merge branch 'master' into eslintVolumeCompt (2021-10-27 18:18) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`| | | |/ /  `  
`| | |/| |   `  
`| * | | | `[a72c39a2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a72c39a2d) eslint (2021-10-26 11:25) <Tommy>  
`| | |/ /  `  
`| |/| |   `  
`* | | |   `[3299ddf53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3299ddf53) Merge branch 'eslintPerpe01' into 'master' (2021-10-27 16:22) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * \ \ \   `[4c753309a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c753309a) Merge branch 'master' into eslintPerpe01 (2021-10-27 18:22) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`| | | |/ /  `  
`| | |/| |   `  
`| * | | | `[9f398cbe9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f398cbe9) eslint (2021-10-26 14:06) <Tommy>  
`* | | | |   `[fd004940e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd004940e) Merge branch 'eslintTabll' into 'master' (2021-10-27 16:21) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| |_|/ / /  `  
`|/| | | |   `  
`| * | | |   `[613ff6ef4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/613ff6ef4) Merge branch 'master' into eslintTabll (2021-10-27 18:20) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`* | | | |   `[664799485](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/664799485) Merge branch 'exportStyles' (2021-10-27 18:02) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[a92b9fb1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a92b9fb1f) Etendre => etendre (2021-10-27 18:01) <Daniel Caillibaud>  
`| * | | | | `[6c55294a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c55294a4) StylesJ3p exporte le style par défaut (le seul utilisé directement) pour éviter de devoir référer à l'instance du parcours pour y accéder (2021-10-27 18:00) <Daniel Caillibaud>  
`|/ / / / /  `  
`* | | | / `[045b84eda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/045b84eda) tentative de fix du hook pre-commit pour mac (2021-10-27 10:55) <Daniel Caillibaud>  
`| |_|_|/  `  
`|/| | |   `  
`* | | |   `[683b64c78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/683b64c78) Merge branch 'eslintDecom01' into 'master' (2021-10-26 14:40) <barroy tommy>  
`|\ \ \ \  `  
`| |_|/ /  `  
`|/| | |   `  
`| * | | `[ead6226b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ead6226b1) on remet les listeners ajoutés en sortie de boucle pour le i = n+1, avec factorisation de code (2021-10-26 11:31) <Daniel Caillibaud>  
`| * | | `[0432e18e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0432e18e7) simplification (2021-10-25 17:19) <Daniel Caillibaud>  
`| * | | `[73d0b2ca8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73d0b2ca8) eslint et modif les deux autres zones pour accepter elemet et id (2021-10-24 11:31) <Tommy>  
`| | * | `[e783c3a8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e783c3a8b) eslint (2021-10-26 16:36) <Tommy>  
`| |/ /  `  
`|/| |   `  
`* | |   `[7070a68ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7070a68ef) Merge branch 'Nouveaux_Exercices_Produit_Scalaire' into 'master' (2021-10-26 08:57) <Yves Biton>  
`|\ \ \  `  
`| * | | `[48edb1c4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48edb1c4b) Ajout de 3 exercices sur la linéarité du produit scalaire. Correction du css pour les flèches de vecteurs. (2021-10-26 10:55) <Yves Biton>  
`* | | |   `[f391236c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f391236c9) Merge branch 'bugsnagpoint2ViaEslint' into 'master' (2021-10-26 08:54) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[660ea1a59](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/660ea1a59) ptite co (2021-10-25 23:19) <Tommy>  
`| * | | | `[489373b69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/489373b69) aj jsdoc (2021-10-25 16:40) <Daniel Caillibaud>  
`| * | | | `[cfe28e0d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfe28e0d7) simplification (plus de j3pDiv ni d'ids) (2021-10-25 16:40) <Daniel Caillibaud>  
`| * | | | `[cef8688d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cef8688d5) fix des derniers warnings eslint (2021-10-25 14:18) <Daniel Caillibaud>  
`| * | | | `[66f78d6e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66f78d6e1) eslint (2021-10-23 17:14) <Tommy>  
`* | | | |   `[1bbe96fc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bbe96fc4) Merge branch 'Correction_MathQuill' into 'master' (2021-10-26 08:50) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| |_|/ / /  `  
`|/| | | |   `  
`| * | | | `[8f3bd539e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f3bd539e) Correction dans mathquill du widehat pour ne plus avoir d'affichages de largeur aberrante sous FireFox de façon aléatoire. Le chapeau des maintenant contenu dans un div et ce qui est-dessous dans un span affiché en inline-block au lieu de block. (2021-10-25 15:14) <Yves Biton>  
`| | |_|/  `  
`| |/| |   `  
`* | | |   `[2ce2c531a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ce2c531a) Merge branch 'Correction_Squelette_Produit_Scalaire' into 'master' (2021-10-26 08:33) <Yves Biton>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[80924f3bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80924f3bf) Correction du squelette de calcul de produit scalaire. (2021-10-26 10:31) <Yves Biton>  
`|/ / /  `  
`* | |   `[c4fca0094](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4fca0094) Merge branch 'textDecoupe' into 'master' (2021-10-25 07:18) <barroy tommy>  
`|\ \ \  `  
`| * | | `[d4b8513e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4b8513e0) string (2021-10-25 09:17) <Tommy>  
`|/ / /  `  
`* | |   `[5a637dce2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a637dce2) Merge branch 'python' into 'master' (2021-10-24 16:52) <Daniel Caillibaud>  
`|\ \ \  `  
`| * \ \   `[ded545c6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ded545c6e) Merge branch 'python' of framagit.org:Sesamath/sesaparcours into python (2021-10-24 18:44) <Rémi Deniaud>  
`| |\ \ \  `  
`| | * | | `[9ce4e93af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ce4e93af) nettoyage + petit correctif progSiBloc (2021-10-24 11:02) <Rémi Deniaud>  
`| | * | | `[547c89b5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/547c89b5a) nettoyage + petit correctif progCalculBloc (2021-10-24 10:48) <Rémi Deniaud>  
`| | * | | `[ac947a8a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac947a8a2) nettoyage + petit correctif InstructionTantque (2021-10-24 10:29) <Rémi Deniaud>  
`| | * | | `[f5ca3f386](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5ca3f386) nettoyage + petit correctif InstructionSi (2021-10-24 10:08) <Rémi Deniaud>  
`| | * | | `[3c314357a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c314357a) nettoyage + petit correctif variableAlgo (2021-10-24 09:51) <Rémi Deniaud>  
`| | * | | `[fa449a22a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa449a22a) nettoyage InstructionPour (2021-10-23 19:02) <Rémi Deniaud>  
`| | * | | `[ceb0236c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ceb0236c0) nettoyage commandePython (2021-10-23 18:38) <Rémi Deniaud>  
`| | * | | `[a15e7a505](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a15e7a505) correction algoAirePerimetre (2021-10-23 17:42) <Rémi Deniaud>  
`| | |/ /  `  
`| * | | `[bd328a9fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd328a9fd) dernières petites modifs du dossier Python (2021-10-24 18:44) <Rémi Deniaud>  
`| * | | `[7cfa258a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7cfa258a9) nettoyage simulDeuxIssues (2021-10-24 18:43) <Rémi Deniaud>  
`| * | | `[9c76ebaf6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c76ebaf6) nettoyage manipulerListe (2021-10-24 18:13) <Rémi Deniaud>  
`| * | | `[c67b10738](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c67b10738) nettoyage instructionsListe (2021-10-24 17:59) <Rémi Deniaud>  
`| * | | `[6275c47b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6275c47b5) nettoyage bouclePourListe (2021-10-24 17:26) <Rémi Deniaud>  
`| * | | `[56660e9ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56660e9ab) nettoyage python_delta (2021-10-24 17:22) <Rémi Deniaud>  
`| * | | `[996c1e386](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/996c1e386) nettoyage + petit correctif progSiBloc (2021-10-24 11:59) <Rémi Deniaud>  
`| * | | `[275130b42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/275130b42) nettoyage + petit correctif progCalculBloc (2021-10-24 11:59) <Rémi Deniaud>  
`| * | | `[bbe4a601b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bbe4a601b) nettoyage + petit correctif InstructionTantque (2021-10-24 11:59) <Rémi Deniaud>  
`| * | | `[f5051b54e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5051b54e) nettoyage + petit correctif InstructionSi (2021-10-24 11:59) <Rémi Deniaud>  
`| * | | `[f93695349](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f93695349) nettoyage + petit correctif variableAlgo (2021-10-24 11:59) <Rémi Deniaud>  
`| * | | `[5c0bac118](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c0bac118) nettoyage InstructionPour (2021-10-24 11:59) <Rémi Deniaud>  
`| * | | `[7c9730460](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c9730460) nettoyage commandePython (2021-10-24 11:59) <Rémi Deniaud>  
`| * | | `[6457e122d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6457e122d) correction algoAirePerimetre (2021-10-24 11:59) <Rémi Deniaud>  
`| | |/  `  
`| |/|   `  
`* | |   `[b4a4594fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4a4594fb) Merge branch 'tab_variation' into 'master' (2021-10-24 16:52) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[6f6fc3234](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f6fc3234) renommage tableauSignesVariations (2021-10-23 17:20) <Rémi Deniaud>  
`| * | | `[b6cad3bf9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6cad3bf9) dernières modifs tableauSignesVariations (2021-10-23 17:16) <Rémi Deniaud>  
`| * | | `[095c97c90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/095c97c90) ajoute du clavier dans les tableaux de variations (2021-10-23 09:52) <Rémi Deniaud>  
`| * | | `[d9e1a1b06](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9e1a1b06) nettoyage tableauSignesVariations (2021-10-23 09:07) <Rémi Deniaud>  
`| * | | `[9958a3053](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9958a3053) nettoyage tabVarLecture (2021-10-23 09:07) <Rémi Deniaud>  
`* | | |   `[c0894f59b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0894f59b) Merge branch 'eslintperms' into 'master' (2021-10-24 14:44) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[bb3c6caa0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb3c6caa0) eslint (2021-10-23 16:04) <Tommy>  
`* | | | |   `[8f4a6b53f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f4a6b53f) Merge branch 'CorrecNomm' into 'master' (2021-10-24 10:11) <barroy tommy>  
`|\ \ \ \ \  `  
`| |_|_|/ /  `  
`|/| | | |   `  
`| * | | | `[47abf9001](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47abf9001) bug jl (2021-10-24 12:10) <Tommy>  
`|/ / / /  `  
`* | | |   `[08a11de08](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/08a11de08) Merge branch 'bugsnagTrigo' into 'master' (2021-10-23 17:00) <barroy tommy>  
`|\ \ \ \  `  
`| * | | | `[0c7ded31d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c7ded31d) bugsnag (2021-10-23 18:58) <Tommy>  
`* | | | |   `[3e2667d18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e2667d18) Merge branch 'ptifixDecoup' into 'master' (2021-10-23 15:53) <barroy tommy>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`| * | | | `[6684a61b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6684a61b9) ptifix (2021-10-23 17:52) <Tommy>  
`|/ / / /  `  
`* | | |   `[1894545ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1894545ba) Merge branch 'bugsnagDem01' into 'master' (2021-10-23 15:28) <barroy tommy>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[21b5c2a3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21b5c2a3d) bufsnag (2021-10-23 17:26) <Tommy>  
`|/ / /  `  
`* | |   `[6fbdde24e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6fbdde24e) Merge branch 'bugsnagstat01' into 'master' (2021-10-23 14:31) <barroy tommy>  
`|\ \ \  `  
`| * | | `[e533f174d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e533f174d) bugsnag (2021-10-23 16:29) <Tommy>  
`|/ / /  `  
`* | |   `[0696eacef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0696eacef) Merge branch 'bugsnagPoint' into 'master' (2021-10-23 14:23) <barroy tommy>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[532208d1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/532208d1d) bugsnag (2021-10-23 16:22) <Tommy>  
`|/ /  `  
`* | `[d29e9697a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d29e9697a) retag 1.0.36 (2021-10-22 21:56) <robot j3p@dev>  
`* | `[06022bd4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06022bd4c) hotfix incrémentation des idRepere (2021-10-22 21:54) <Daniel Caillibaud>  
`* | `[76e45eefd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76e45eefd) fix ajout de prop sur l'objet j3p (2021-10-22 21:47) <Daniel Caillibaud>  
`* | `[3d3af753c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d3af753c) hotfix id foireux (2021-10-22 21:47) <Daniel Caillibaud>  
`* | `[4e75567c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e75567c5) hotfix stats01 (merge foireux me vs this) (2021-10-22 21:47) <Daniel Caillibaud>  
`* |   `[ddd1ef9a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ddd1ef9a4) Merge branch 'vireIsFirefox' into 'master' (2021-10-22 19:46) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[8a667d4d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a667d4d6) isFireFox restant viré de deux squelettes. (2021-10-22 20:02) <Yves Biton>  
`| * | `[48a21745f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48a21745f) if vide viré (2021-10-22 19:35) <Daniel Caillibaud>  
`| * | `[dfbb69ae4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfbb69ae4) j3pIsFirefox virée (utilisée dans une seule section où c'était anecdotique) (2021-10-22 19:35) <Daniel Caillibaud>  
`| * | `[a5372b08f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5372b08f) code mort viré (j3pCreeclassecss jamais utilisée et indiquée comme ne fonctionnant pas) (2021-10-22 19:33) <Daniel Caillibaud>  
`| * | `[80c1df064](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80c1df064) j3pIsFirefox virée (utilisée dans une seule section où c'était anecdotique) (2021-10-22 19:32) <Daniel Caillibaud>  
`* | | `[48816af97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48816af97) retag 1.0.35 (2021-10-22 19:08) <robot j3p@dev>  
`|/ /  `  
`* |   `[b8a6e1d57](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8a6e1d57) Merge branch 'fixCommandePython' into 'master' (2021-10-22 17:06) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[f2f80b8de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2f80b8de) on utilise j3pAutoSizeInput pour les deux autres sections qui avaient une fct reecrireZoneInput qui plantait (2021-10-22 19:06) <Daniel Caillibaud>  
`| * | `[2a9574523](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a9574523) simplification de l'autoresize des input, qui marche aussi sur tablette ou avec un collage souris (event input et plus keydown) (2021-10-22 19:00) <Daniel Caillibaud>  
`| * | `[6148eac68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6148eac68) aj de j3pAutoSizeInput (2021-10-22 18:59) <Daniel Caillibaud>  
`| * | `[8729bebbf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8729bebbf) fix d'un plantage (réécrit le code de reecrireZoneInput qui était vraiment louche) (2021-10-21 01:26) <Daniel Caillibaud>  
`* | |   `[55b2fada9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55b2fada9) Merge branch 'eslintTrigo' into 'master' (2021-10-22 15:35) <barroy tommy>  
`|\ \ \  `  
`| * | | `[ef5ac2667](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef5ac2667) fix check conteneur (manquait une négation) (2021-10-22 17:26) <Daniel Caillibaud>  
`| * | | `[7db83a2f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7db83a2f2) dernier warnings eslint virés (var) (2021-10-21 20:33) <Daniel Caillibaud>  
`| * | | `[babee034b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/babee034b) usages inutiles d'id virés (2021-10-21 20:23) <Daniel Caillibaud>  
`| * | | `[7426075a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7426075a4) BoutonStyleMathquill accepte un conteneur sans id (il sera généré) (2021-10-21 20:17) <Daniel Caillibaud>  
`| * | | `[e31859032](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e31859032) eslint (2021-10-21 15:51) <Tommy>  
`* | | |   `[a27d84591](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a27d84591) Merge branch 'Adaptation_Section_Remi_Clavier_Virtuel' into 'master' (2021-10-22 15:14) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[6ed792922](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ed792922) correction d'un dernier pb avec pgcdAvecAlgoEuclide (2021-10-22 15:34) <Rémi Deniaud>  
`| * | | |   `[5ce453374](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ce453374) Merge branch 'Adaptation_Section_Remi_Clavier_Virtuel' of framagit.org:Sesamath/sesaparcours into Adaptation_Section_Remi_Clavier_Virtuel (2021-10-22 15:19) <Rémi Deniaud>  
`| |\ \ \ \  `  
`| | * | | | `[cb1e6bcc6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb1e6bcc6) Adaptation de la section de Rémi sur l'algorithme d'Euclide au clavier virtuel mais cette section ne focntionnait déjà pas à la fin quand on se trompe sur le PGCD et qu'on a droit à une autre chance. A corriger donc. (2021-10-22 10:38) <Yves Biton>  
`| * | | | | `[9603106ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9603106ad) nettoyage pgcdAvecAloEuclide (2021-10-22 14:37) <Rémi Deniaud>  
`| * | | | | `[931d46834](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/931d46834) Adaptation de la section de Rémi sur l'algorithme d'Euclide au clavier virtuel mais cette section ne focntionnait déjà pas à la fin quand on se trompe sur le PGCD et qu'on a droit à une autre chance. A corriger donc. (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[35d4a9df1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35d4a9df1) retag 1.0.31 (2021-10-22 14:14) <robot j3p@dev>  
`| * | | | | `[7a1d86a92](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a1d86a92) eslint et ptet bugsnag mais pas sur (2021-10-22 14:14) <Tommy>  
`| * | | | | `[3092c3ed0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3092c3ed0) notify (2021-10-22 14:14) <Tommy>  
`| * | | | | `[e898287ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e898287ba) bug sur cooplace et disabled (2021-10-22 14:14) <Tommy>  
`| * | | | | `[a9e19e8a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9e19e8a3) bug sur cooplace et disabled (2021-10-22 14:14) <Tommy>  
`| * | | | | `[e35ad0016](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e35ad0016) avec le bon batx... (2021-10-22 14:14) <Tommy>  
`| * | | | | `[8e036ef74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e036ef74) nom des rectangles carres (2021-10-22 14:14) <Tommy>  
`| * | | | | `[257bbf442](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/257bbf442) fix 614d45aee99816000765fc7e (pas vraiment, on plante juste plus tôt avec un message plus explicite) (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[b5673dae4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5673dae4) fix 616ef04b3833ba0008a5ba01 (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[34f08cecc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34f08cecc) fix 612517089e701b00074a6b66 (si pas de nœud suivant on l'a déjà signalé, pas la peine de planter une 2e fois) (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[1fecd8cd1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1fecd8cd1) retag 1.0.30 (2021-10-22 14:14) <user j3p @dev>  
`| * | | | | `[1bd492a69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bd492a69) suppression du dernier warning eslint (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[4a53db1cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a53db1cf) blindage (au cas où Number(parcours.DonneesSection.nbLatex) donnerait NaN) (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[20ffaed35](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20ffaed35) Section corrigée suite à rapport d'erreur de Sébastien Deshayes (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[99b5a9f6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99b5a9f6f) fix changement du onclick au shift, qui plantait (err bugsnag 6045f569df128100181ad9f2) (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[75a132d7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75a132d7f) retag 1.0.29 (2021-10-22 14:14) <user j3p @dev>  
`| * | | | | `[96d359a66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96d359a66) on vire la règle eslint curly qu'on avait ajouté pour revenir à standardJs (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[58393a591](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58393a591) Aj de jsdoc sur l'ensemble du code (avant seuls certains outils étaient parsés), aj du tuto sur le clavier virtuel (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[18be9e353](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/18be9e353) on notifie plus bugsnag pour les graphes sans nœud suivant s'ils ne sont pas dans sesabibli (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[b8f732d31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8f732d31) eslint et widehat corrige (2021-10-22 14:14) <Tommy>  
`| * | | | | `[5512f0e60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5512f0e60) eslint et widehat corrige (2021-10-22 14:14) <Tommy>  
`| * | | | | `[e7d6574de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e7d6574de) retag 1.0.28 (2021-10-22 14:14) <Compte unix pour site j3p>  
`| * | | | | `[0913b1064](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0913b1064) Dans j3PAffiche on ne donn e plus le focus par défaut au premier éditeur MathQuill rencontré car avec certaines sections on se retrouve avec plusieurs curseurs clignotants. (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[d7f6d7b93](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7f6d7b93) je sais pas pourquoi le foreach plante (2021-10-22 14:14) <Tommy>  
`| * | | | | `[22b746797](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22b746797) retag 1.0.27 | fix récup lastResultat (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[f8763b777](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8763b777) retag 1.0.26 (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[44a81e58b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44a81e58b) hotfix des j3p.adresse qui restaient (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[a6a96af99](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6a96af99) recupereParametres n'utilise que 2 params (on vire les 4 inutiles) (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[69e1b50a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69e1b50a5) fix listeners etiquettes (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[85c28c647](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85c28c647) code mort viré (méthodes de Parcours jamais utilisées) (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[f3cc5bf00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3cc5bf00) hotfix container => conteneur (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[1caf6a750](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1caf6a750) retag 1.0.25 (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[0bcef7b63](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0bcef7b63) méthode EcritQuestion virée (jamais utilisée) (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[ffec176d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffec176d1) le conteneur devient une propriété du parcours (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[0e3b2ec3c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e3b2ec3c) on calme un peu les plantages mathquill (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[4f921ffa2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f921ffa2) bugfix xmlText undefined (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[f79023dcc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f79023dcc) j3pInputDyn3 mis en privé pour j3pAffiche (faudrait le virer complètement), et passé en synchrone (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[514c4a66f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/514c4a66f) import addTable + fix eslint (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[6bf08f19e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6bf08f19e) code mort viré (y'avait jamais d'élément #fixFocusListener) (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[e6f054ac3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e6f054ac3) bugsnag (2021-10-22 14:14) <Tommy>  
`| * | | | | `[4077c0b71](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4077c0b71) eslint (2021-10-22 14:14) <Tommy>  
`| * | | | | `[6092c5564](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6092c5564) J'ai roujouté le bouton Racine carrée par défaut ce qui devait être le cas avant (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[98a888520](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98a888520) hotfix variable réaffectée avec undefined (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[bdfca5d78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bdfca5d78) hotfix code js dans des string (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[d7825672e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7825672e) fix code js dans une string (pb bugsnag stor undefined) (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[1e604ce06](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e604ce06) retag 1.0.24 (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[58573af51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58573af51) fauxtableur.js viré (remplacé par Tableur.js depuis belle lurette (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[ff9d8ca41](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff9d8ca41) nom des rectangles carres (2021-10-22 14:14) <Tommy>  
`| * | | | | `[8d7c8ba85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d7c8ba85) Correction d'un ?. oublié dans j3PDesactive (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[7e924f5aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e924f5aa) Annulation d'erreur (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[43394e65d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43394e65d) Correction de j3PDesactive (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[0f28577c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f28577c2) Ajout d'un commentaire en début des sections qui ont été adaptées au clavier virtuel (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[28c6ca8f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28c6ca8f8) Modif pour essayer de mieux contrer le problème du curseur fantôme non clignotant sur tablette. (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[919e6d478](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/919e6d478) Amélioration du focus sur erreur de 4 des sections et squelettes de Yves (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[dfd4a2346](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfd4a2346) Petite modif d clavier virtuel es espérant éliminer le cas de l'éditeur Mathquill qui devient riquiqui. (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[18a4fe12b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/18a4fe12b) Modifications de forme simplement (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[443dbc0fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/443dbc0fc) Modifications de forme simplement (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[57289bb27](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57289bb27) nettoyage tauxEvolution + petite modif calculatrice (2021-10-22 14:14) <Rémi Deniaud>  
`| * | | | | `[f90c74694](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f90c74694) Ajout d'une variable isActive au clavier virtuel pour qu'on puisse le désactiver et masquer la flèche le faisant appraître dans j3PDesactive. Avec ces modifs la section de Rémi natureSuite semble bien marcher. (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[d7dc19810](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7dc19810) Rajout de const parcours = this en début de section (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[0dbf313b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0dbf313b7) Adaptaton de la section de Tommy airelateraleprimsecylindre au clavier virtuel. (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[ce0cad36e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce0cad36e) Adaptation de deux sectiosn  de Rémi au clavier virtuel mais je le laisse corriger le reste pour compatibilté Sésaparcours (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[f1552d5ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1552d5ff) commentaires foireux virés (2021-10-22 14:14) <Daniel Caillibaud>  
`| * | | | | `[5ff0a19d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ff0a19d7) Moodification de J"PAffiche pour que le focus soit donné à un seul éditeur MathQuill (s'il y en a) et qu'il soit appelé via j3PFocus (Nécessaire pour compatibilité avec clavier virtuel). (2021-10-22 14:14) <Yves Biton>  
`| * | | | | `[d13f1b78b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d13f1b78b) defaulttable si pas de classname et corrige ptit bug decoupe (2021-10-22 14:14) <Tommy>  
`| * | | | | `[eddac2d86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eddac2d86) sphere (2021-10-22 14:14) <Tommy>  
`| * | | | | `[675e7fb44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/675e7fb44) cube (2021-10-22 14:14) <Tommy>  
`| * | | | | `[7b21bbe03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b21bbe03) bugsnag (2021-10-22 14:14) <Tommy>  
`|  / / / /  `  
`* | | | |   `[8f5e660e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f5e660e1) Merge branch 'convexite' into 'master' (2021-10-22 15:10) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[cd1eb4137](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd1eb4137) nettoyage code convexiteDerivee (2021-10-21 19:17) <Rémi Deniaud>  
`* | | | | |   `[baafe2b55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/baafe2b55) Merge branch 'eslintETmodifZoneEtiq' into 'master' (2021-10-22 15:08) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[11fc027af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11fc027af) eslint formules ( reste un id a cause de Zoneclik ) Zoneclik je feraiplus tard eslint (2021-10-22 14:48) <Tommy>  
`* | | | | | |   `[84216f771](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84216f771) Merge branch 'Adaptation_Squelettes_Clavier_Virtuel' into 'master' (2021-10-22 15:04) <Yves Biton>  
`|\ \ \ \ \ \ \  `  
`| |_|_|_|_|_|/  `  
`|/| | | | | |   `  
`| * | | | | | `[ba12d618a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba12d618a) Optimisation de contientSomOuDifPoints dans les deux squelettes (2021-10-22 16:51) <Yves Biton>  
`| * | | | | | `[7c1de554d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c1de554d) Correction du squelette de calcul sur le produit scalaire et correction d'un fichier annexe (pour le fichier annexe, quand le résultat était nul une réponse fausse était acceptée...) (2021-10-22 16:23) <Yves Biton>  
`| * | | | | | `[ca1b3f481](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca1b3f481) Amélioration du squemlette de calcul sur le produit scalaire (2021-10-22 15:27) <Yves Biton>  
`| * | | | | | `[51f544ec7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51f544ec7) Même si ce squelette ne plantait pas quand on entrait comme réponse le vecteur nul et cliquait sur OK, correction analogue à celle du squelette Calc_Param_Vect (2021-10-22 14:03) <Yves Biton>  
`| * | | | | | `[0c32cc0fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c32cc0fb) fix dernier warning eslint (2021-10-22 13:49) <Daniel Caillibaud>  
`| * | | | | | `[1145983a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1145983a2) aj commentaire (et viré _stopTimer, c'est le modèle qui gère ça, il le fait dans finCorrection) (2021-10-22 13:48) <Daniel Caillibaud>  
`| * | | | | | `[705d46876](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/705d46876) Correction du squelette Calc_Param dna sl ecas où l'élèbe mettait comme réponse le vecteur nul (2021-10-22 13:34) <Yves Biton>  
`|/ / / / / /  `  
`* | | | | | `[67ad607fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67ad607fd) retag 1.0.34 (2021-10-22 11:50) <robot j3p@dev>  
`* | | | | |   `[a1a6b7d66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1a6b7d66) Merge branch 'Adaptation_Sections_Clavier_Virtuel' into 'master' (2021-10-22 09:50) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | | | |   `  
`| * | | | |   `[ad3bacec3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad3bacec3) Merge branch 'master' into Adaptation_Sections_Clavier_Virtuel (2021-10-22 11:49) <Daniel Caillibaud>  
`| |\ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | | | |   `  
`* | | | | |   `[0a1ea56db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a1ea56db) Merge branch 'nomme02pyra' into 'master' (2021-10-21 22:50) <barroy tommy>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[6ac2489e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ac2489e7) pyramieux (2021-10-22 00:48) <Tommy>  
`* | | | | | | `[0c96af30c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c96af30c) Merge branch 'nomme02pyra' into 'master' (2021-10-21 22:46) <barroy tommy>  
`|\| | | | | | `  
`| * | | | | | `[ed51c7a33](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed51c7a33) pyramieux (2021-10-22 00:44) <Tommy>  
`| * | | | | | `[02ce96a13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02ce96a13) pyra (2021-10-21 23:45) <Tommy>  
`|/ / / / / /  `  
`* | | | | |   `[9dee83033](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9dee83033) Merge branch 'eslinttriangleegaux01' into 'master' (2021-10-21 18:56) <barroy tommy>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[2fa919da8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2fa919da8) bug ok (2021-10-21 20:54) <Tommy>  
`| * | | | | | `[ad9610994](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad9610994) fix warnings eslint qui restaient (des var) (2021-10-21 19:06) <Daniel Caillibaud>  
`| * | | | | | `[3c724ab3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c724ab3d) usage de stor.lesdiv.travail.id viré (il n'existe pas, et on en a pas besoin) (2021-10-21 18:48) <Daniel Caillibaud>  
`| * | | | | |   `[43fef1c4d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43fef1c4d) Merge branch 'master' into eslinttriangleegaux01 (2021-10-21 18:23) <Daniel Caillibaud>  
`| |\ \ \ \ \ \  `  
`| | | |_|_|/ /  `  
`| | |/| | | |   `  
`| * | | | | | `[7deeb8d95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7deeb8d95) eslint (2021-10-21 02:57) <Tommy>  
`* | | | | | |   `[777d74ade](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/777d74ade) Merge branch 'eslinttriegau02' into 'master' (2021-10-21 18:48) <barroy tommy>  
`|\ \ \ \ \ \ \  `  
`| |_|/ / / / /  `  
`|/| | | | | |   `  
`| * | | | | | `[15bb26da7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15bb26da7) bug raz (2021-10-21 20:47) <Tommy>  
`| * | | | | | `[06fd135e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06fd135e9) fix des derniers warnings eslint (2021-10-21 19:55) <Daniel Caillibaud>  
`| * | | | | | `[70cb498ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70cb498ec) on utilise les éléments plutôt que leur id (il n'en ont pas) (2021-10-21 19:46) <Daniel Caillibaud>  
`| * | | | | | `[93f1a8a1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93f1a8a1b) idDivRepere devient facultatif pour Repere (2021-10-21 19:45) <Daniel Caillibaud>  
`| * | | | | |   `[8725c9531](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8725c9531) Merge branch 'master' into eslinttriegau02 (2021-10-21 19:13) <Daniel Caillibaud>  
`| |\ \ \ \ \ \  `  
`| |/ / / / / /  `  
`|/| | | | | |   `  
`* | | | | | |   `[d81386325](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d81386325) Merge branch 'nomme02tierce' into 'master' (2021-10-21 09:56) <barroy tommy>  
`|\ \ \ \ \ \ \  `  
`| * | | | | | | `[83bca0fc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83bca0fc4) cone2 (2021-10-21 11:53) <Tommy>  
`|/ / / / / / /  `  
`* | | | | | |   `[4fab316d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fab316d5) Merge branch 'vireIdsAleatoiresDansAddDefaultTable' into 'master' (2021-10-21 06:36) <barroy tommy>  
`|\ \ \ \ \ \ \  `  
`| |_|/ / / / /  `  
`|/| | | | | |   `  
`| * | | | | | `[c7bc5a81d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7bc5a81d) on appelle pas j3pElement si on a pas d'id, pour éviter de le faire grogner pour rien (2021-10-21 00:42) <Daniel Caillibaud>  
`| * | | | | | `[0646b7ee4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0646b7ee4) addDefaultTable n'ajoute plus d'id aléatoire (2021-10-20 19:34) <Daniel Caillibaud>  
`| * | | | | | `[1c76d1695](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c76d1695) on utilise les éléments plutôt que leurs ids (2021-10-20 19:33) <Daniel Caillibaud>  
`| * | | | | | `[f74e66f4a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f74e66f4a) fix "à le" => "au" + pb de typo (espaces avant virgule) (2021-10-20 19:32) <Daniel Caillibaud>  
`| * | | | | | `[2a75a45af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a75a45af) ListeDeroulante accepte un conteneur ou son id (et ajoute un id s'il n'en avait pas) (2021-10-20 19:20) <Daniel Caillibaud>  
`| * | | | | | `[2aac9b29f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2aac9b29f) on utilise les éléments plutôt que leurs ids (2021-10-20 18:55) <Daniel Caillibaud>  
`| * | | | | | `[373ac90c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/373ac90c0) on utilise les éléments plutôt que leurs ids (2021-10-20 18:55) <Daniel Caillibaud>  
`| * | | | | | `[3e02308da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e02308da) zoneStyleMathquill accepte aussi un conteneur (et pas seulement un id, il ajoutera l'id s'il n'en a pas) (2021-10-20 18:52) <Daniel Caillibaud>  
`| | * | | | | `[8f4646357](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f4646357) eslint (2021-10-21 08:34) <Tommy>  
`| |/ / / / /  `  
`|/| | | | |   `  
`* | | | | |   `[b0c7bebfc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0c7bebfc) Merge branch 'nomme02cone1' into 'master' (2021-10-21 00:13) <barroy tommy>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[58ca3cdd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58ca3cdd0) cone rev (2021-10-21 02:11) <Tommy>  
`* | | | | | | `[760bdc068](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/760bdc068) retag 1.0.33 (2021-10-21 00:38) <robot j3p@dev>  
`| |_|_|_|_|/  `  
`|/| | | | |   `  
`* | | | | | `[095b8fdf8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/095b8fdf8) oubli du commit précédent sur les zIndex (2021-10-21 00:37) <Daniel Caillibaud>  
`* | | | | | `[e896d55e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e896d55e1) retag 1.0.32 (2021-10-21 00:35) <robot j3p@dev>  
`* | | | | |   `[d0676a906](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0676a906) Merge branch 'refixZindex' into 'master' (2021-10-20 22:34) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | | | |   `  
`| * | | | | `[c220c3291](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c220c3291) rationalisation des zIndex (2021-10-21 00:27) <Daniel Caillibaud>  
`| * | | | | `[5171e6733](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5171e6733) www/css inutile viré (2021-10-21 00:26) <Daniel Caillibaud>  
`| * | | | | `[a1fc86e7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1fc86e7d) zindex (2021-10-20 15:51) <Tommy>  
`* | | | | |   `[debab6f4f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/debab6f4f) Merge branch 'eslintsymetriequadbbb' into 'master' (2021-10-20 20:08) <barroy tommy>  
`|\ \ \ \ \ \  `  
`| |_|/ / / /  `  
`|/| | | | |   `  
`| * | | | | `[70e3fdfe8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70e3fdfe8) derniers warnings eslint virés (2021-10-20 18:01) <Daniel Caillibaud>  
`| * | | | | `[9f71ad21b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f71ad21b) id inutiles virés (2021-10-20 17:57) <Daniel Caillibaud>  
`| * | | | | `[44ffe48d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44ffe48d5) eslint bugsnag ? (2021-10-20 17:02) <Tommy>  
`* | | | | |   `[7a48c607f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a48c607f) Merge branch 'Correction_Fichiers_Annexes' into 'master' (2021-10-20 15:33) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[78902a1a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78902a1a1) Correction du titre de deux ressources suite à rapport LaboMep. (2021-10-20 14:35) <Yves Biton>  
`| | |_|_|_|/  `  
`| |/| | | |   `  
`* | | | | |   `[d84849343](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d84849343) Merge branch 'fixBugsnaGBienEcrirehum' into 'master' (2021-10-20 15:32) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| |_|/ / / /  `  
`|/| | | | |   `  
`| * | | | | `[1b63e7a5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b63e7a5a) fix info paramétrage foireuse. (2021-10-20 17:24) <Daniel Caillibaud>  
`| * | | | | `[abed0f02e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/abed0f02e) css externalisées (2021-10-20 17:15) <Daniel Caillibaud>  
`* | | | | |   `[3e2207c76](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e2207c76) Merge branch 'fixBugsnaGBienEcrirehum' into 'master' (2021-10-20 14:26) <barroy tommy>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[8786a15e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8786a15e7) ptit trucs (2021-10-20 16:26) <Tommy>  
`| |/ / / / /  `  
`| * | | | | `[1b48aff18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b48aff18) modif de forme (2021-10-20 15:15) <Daniel Caillibaud>  
`| * | | | | `[cc0e70a19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc0e70a19) manquait un renommage tt => ttCells (2021-10-20 15:10) <Daniel Caillibaud>  
`| * | | | | `[a10cde8fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a10cde8fa) fix pb de tt qui était affecté bizarrement (provoquant des plantages) (2021-10-20 15:06) <Daniel Caillibaud>  
`| * | | | | `[09ebb38e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09ebb38e2) stor.lesdiv est un objet, pas un array (2021-10-20 14:09) <Daniel Caillibaud>  
`| * | | | |   `[2dfcbf800](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2dfcbf800) Merge branch 'master' into fixBugsnaGBienEcrirehum (2021-10-20 14:04) <Daniel Caillibaud>  
`| |\ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | | | |   `  
`* | | | | |   `[b31182511](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b31182511) Merge branch 'remplaceRemplace' into 'master' (2021-10-20 12:02) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| |_|/ / / /  `  
`|/| | | | |   `  
`| * | | | |   `[faaf63b8f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/faaf63b8f) Merge branch 'master' into remplaceRemplace (2021-10-20 13:54) <Daniel Caillibaud>  
`| |\ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | | | |   `  
`* | | | | | `[d8c8f9bdd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8c8f9bdd) retag 1.0.31 (2021-10-20 00:54) <robot j3p@dev>  
`* | | | | |   `[c2cc181f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2cc181f8) Merge branch 'eslintpyth' into 'master' (2021-10-19 22:53) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[92571fb25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92571fb25) eslint et ptet bugsnag mais pas sur (2021-10-19 22:36) <Tommy>  
`| | |_|/ / /  `  
`| |/| | | |   `  
`* | | | | |   `[b021f9581](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b021f9581) Merge branch 'notify_pour_complete' into 'master' (2021-10-19 21:13) <barroy tommy>  
`|\ \ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | | | |   `  
`| * | | | | `[0f1db11d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f1db11d4) notify (2021-10-19 23:11) <Tommy>  
`|/ / / / /  `  
`* | | | |   `[940179424](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/940179424) Merge branch 'un-nom-de-nouvelle-branche' into 'master' (2021-10-19 20:12) <barroy tommy>  
`|\ \ \ \ \  `  
`| * | | | | `[e12efe60b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e12efe60b) bug sur cooplace et disabled (2021-10-19 22:10) <Tommy>  
`| * | | | | `[fb4f02d8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb4f02d8a) bug sur cooplace et disabled (2021-10-19 21:59) <Tommy>  
`|/ / / / /  `  
`* | | | |   `[233597fd2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/233597fd2) Merge branch 'refixsta02avec_addtablecorrig' into 'master' (2021-10-19 19:20) <barroy tommy>  
`|\ \ \ \ \  `  
`| * | | | | `[8a78dd5ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a78dd5ca) avec le bon batx... (2021-10-19 21:16) <Tommy>  
`| * | | | | `[e166ff372](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e166ff372) nom des rectangles carres (2021-10-18 13:08) <Tommy>  
`|  / / / /  `  
`* | | | | `[0e55e7f7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e55e7f7e) Merge branch 'multipleFixesBugsnag' into 'master' (2021-10-19 18:59) <Daniel Caillibaud>  
`* | | | | `[3c37251dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c37251dc) fix 614d45aee99816000765fc7e (pas vraiment, on plante juste plus tôt avec un message plus explicite) (2021-10-19 20:49) <Daniel Caillibaud>  
`* | | | | `[2eb055d66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2eb055d66) fix 616ef04b3833ba0008a5ba01 (2021-10-19 20:22) <Daniel Caillibaud>  
`* | | | | `[146f1d81c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/146f1d81c) fix 612517089e701b00074a6b66 (si pas de nœud suivant on l'a déjà signalé, pas la peine de planter une 2e fois) (2021-10-19 19:52) <Daniel Caillibaud>  
` / / / /  `  
`* | | | `[6015af269](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6015af269) simplification du code, id virés (2021-10-18 11:16) <Daniel Caillibaud>  
`* | | | `[83f3d3b9f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83f3d3b9f) Merge branch 'master' into remplaceRemplace (2021-10-18 11:09) <Daniel Caillibaud>  
`* | | | `[184ed5726](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/184ed5726) mutualisation addTable (2021-10-14 14:52) <Daniel Caillibaud>  
`* | | | `[9a55bf611](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a55bf611) Merge branch 'master' into remplaceRemplace (2021-10-14 14:50) <Daniel Caillibaud>  
`* | | | `[649549446](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/649549446) remis l'ancien addtable pour virer les lignes en trop (2021-10-05 14:03) <Tommy>  
` / / /  `  
`* | | `[81c9041e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81c9041e9) modif du merge précédent oubliées (2021-10-18 11:05) <Daniel Caillibaud>  
`* | | `[273352731](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/273352731) Merge branch 'master' into fixBugsnaGBienEcrirehum (avec résolution de conflit à contrôler) (2021-10-18 10:51) <Daniel Caillibaud>  
`* | | `[38447348a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38447348a) Merge branch 'master' into fixBugsnaGBienEcrirehum (2021-10-14 15:58) <Daniel Caillibaud>  
`* | | `[41ed4abde](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41ed4abde) mutualisation addTable (2021-10-14 15:58) <Daniel Caillibaud>  
`* | | `[23c24c53b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23c24c53b) un peu d'eslint et de ménage (2021-10-09 23:25) <Tommy>  
` / /  `  
`* | `[371a37677](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/371a37677) Deux sections de Rémi sur les dérivées adaptées au clavier virtuel. (2021-10-22 09:08) <Yves Biton>  
`* | `[a291243aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a291243aa) Adaptation au clavier virtuel d'anciennes sections sur les nombres complexes et pour certaines correction. (2021-10-21 18:26) <Yves Biton>  
`|/  `  
`* `[6fe9d786d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6fe9d786d) Adaptation de sections au clavier virtuel (2021-10-21 15:04) <Yves Biton>  
`* `[3626b3e39](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3626b3e39) Adaptation de la section au clavier virtuel (2021-10-21 13:45) <Yves Biton>  
`* `[9e4372609](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e4372609) Dans la section temps écoulé j'ai mis les éditeurs à droite des horloges pour que les claviers virtuels soient totalement visibles et j'ai augmenté le ration de la fenêtre de gauche. (2021-10-21 12:04) <Yves Biton>  
`* `[2988e53f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2988e53f8) Adaptation d'une section de primaire au clavier virtuel (durée écoulée) Le maxchars des éditeurs mathQuill n'a pas l'air de fonctionner. (2021-10-21 11:38) <Yves Biton>  
`* `[917348c96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/917348c96) Adaptation d'une section de primaire au clavier virtuel (durée écoulée) (2021-10-21 11:04) <Yves Biton>  
`* `[64db82f72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64db82f72) Amélioration d'un squelette. (2021-10-20 22:53) <Yves Biton>  
`* `[7f12402a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f12402a2) avec prises plus grosses (2021-10-20 21:59) <Tommy>  
`* `[e2dc7db1e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2dc7db1e) gros eslint et co bug truc vide (2021-10-20 21:26) <Tommy>  
`* `[a503e7fc1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a503e7fc1) eslint bugsnag ? (2021-10-20 21:19) <Tommy>  
`* `[27ae46bb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27ae46bb8) deplace diag circ (2021-10-20 14:05) <Tommy>  
`* `[9f8195df1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f8195df1) Pb du déplacement au touc des diagrammes circulaires résolu. (2021-10-19 16:31) <Yves Biton>  
`* `[75d4e1a08](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75d4e1a08) Je remets j3PAffiche dans l'état où il est dans master. (2021-10-19 16:03) <Yves Biton>  
`* `[71015970e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/71015970e) Essai de gestion des événements touch. Ca marche un peu mais pas assez bien. Un bug à corriger : Quand on demande que toutes les graduations soient automatiques on a encore en abscisses certains éditeurs de graduation sur lesquels on a appelé desactive. (2021-10-19 15:26) <Yves Biton>  
`* `[995ae370d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/995ae370d) Corrections à sectionstat01 Adapatation de sectionDiviseursEntier au clavier virtuel. (2021-10-19 07:54) <Yves Biton>  
`* `[466195dec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/466195dec) Ajout d'un commentaire au début de la section. (2021-10-18 17:21) <Yves Biton>  
`* `[8d668b21f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d668b21f) Adaptation d'une ressource de Tommy au clavier virtuel (sectionstat01) (2021-10-18 17:19) <Yves Biton>  
`* `[8cfdca32c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cfdca32c) Adaptation d'une ressource de Tommy au clavier virtuel (2021-10-18 12:11) <Yves Biton>  

## version 1.0.30

`* `[145ad9485](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/145ad9485) retag 1.0.30 (2021-10-19 19:08) <user j3p @dev>  
`*   `[24d9d3c40](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24d9d3c40) Merge branch 'Correction_Squelette' into 'master' (2021-10-19 17:07) <Daniel Caillibaud>  
`|\  `  
`| * `[5a7645617](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a7645617) suppression du dernier warning eslint (2021-10-19 19:07) <Daniel Caillibaud>  
`| * `[73911290f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73911290f) blindage (au cas où Number(parcours.DonneesSection.nbLatex) donnerait NaN) (2021-10-19 19:06) <Daniel Caillibaud>  
`| * `[79218b5f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79218b5f8) Section corrigée suite à rapport d'erreur de Sébastien Deshayes (2021-10-19 18:42) <Yves Biton>  
`* |   `[dd8ea8cb9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd8ea8cb9) Merge branch 'fixShiftCalculatrice' into 'master' (2021-10-19 17:01) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[6f050fac8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f050fac8) fix changement du onclick au shift, qui plantait (err bugsnag 6045f569df128100181ad9f2) (2021-10-19 19:00) <Daniel Caillibaud>  
`|/ /  `  
`* | `[841544654](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/841544654) retag 1.0.29 (2021-10-19 17:00) <user j3p @dev>  
`* | `[97e17759d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97e17759d) on vire la règle eslint curly qu'on avait ajouté pour revenir à standardJs (2021-10-19 16:55) <Daniel Caillibaud>  
`* |   `[cf81739fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf81739fc) Merge branch 'jsdocComplet' into 'master' (2021-10-19 14:52) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[fee80c3d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fee80c3d9) Aj de jsdoc sur l'ensemble du code (avant seuls certains outils étaient parsés), aj du tuto sur le clavier virtuel (2021-10-19 16:43) <Daniel Caillibaud>  
`| * | `[4e2084a48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e2084a48) on notifie plus bugsnag pour les graphes sans nœud suivant s'ils ne sont pas dans sesabibli (2021-10-19 12:34) <Daniel Caillibaud>  
`* | |   `[fd2b43ab5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd2b43ab5) Merge branch 'bugsnagbizar' into 'master' (2021-10-19 14:34) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[7aafa4070](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7aafa4070) eslint et widehat corrige (2021-10-19 15:51) <Tommy>  
`| |/ /  `  
`* | |   `[7376ace8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7376ace8e) Merge branch 'refaisTriEg02' into 'master' (2021-10-19 14:33) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[07b2eb6eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07b2eb6eb) eslint et widehat corrige (2021-10-19 15:34) <Tommy>  
`|/ /  `  
`* | `[8181412a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8181412a2) retag 1.0.28 (2021-10-19 11:56) <Compte unix pour site j3p>  
`* |   `[f2fc38633](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2fc38633) Merge branch 'Correction_j3PAffiche' into 'master' (2021-10-19 09:45) <Daniel Caillibaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[c04e152dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c04e152dd) Dans j3PAffiche on ne donn e plus le focus par défaut au premier éditeur MathQuill rencontré car avec certaines sections on se retrouve avec plusieurs curseurs clignotants. (2021-10-19 10:57) <Yves Biton>  
`|/  `  
`*   `[3fc4cf716](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3fc4cf716) Merge branch 'etiquetteencore' into 'master' (2021-10-19 07:08) <Daniel Caillibaud>  
`|\  `  
`| * `[c4f3a2817](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4f3a2817) je sais pas pourquoi le foreach plante (2021-10-18 21:40) <Tommy>  
`* | `[cbd5b48e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbd5b48e3) retag 1.0.27 | fix récup lastResultat (2021-10-18 23:33) <Daniel Caillibaud>  
`|/  `  
`* `[dd6b36e4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd6b36e4c) retag 1.0.26 (2021-10-18 20:20) <Daniel Caillibaud>  
`* `[dff75ce4e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dff75ce4e) hotfix des j3p.adresse qui restaient (2021-10-18 20:20) <Daniel Caillibaud>  
`* `[48eb5065b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48eb5065b) recupereParametres n'utilise que 2 params (on vire les 4 inutiles) (2021-10-18 20:20) <Daniel Caillibaud>  
`*   `[6720d2209](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6720d2209) Merge branch 'fixEtiquettes' into 'master' (2021-10-18 17:31) <barroy tommy>  
`|\  `  
`| * `[4595efe6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4595efe6e) fix listeners etiquettes (2021-10-18 16:45) <Daniel Caillibaud>  
`* | `[497ed58e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/497ed58e8) code mort viré (méthodes de Parcours jamais utilisées) (2021-10-18 17:51) <Daniel Caillibaud>  
`* | `[7af6766a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7af6766a9) hotfix container => conteneur (2021-10-18 17:44) <Daniel Caillibaud>  
`* | `[735f19ade](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/735f19ade) retag 1.0.25 (2021-10-18 17:20) <Daniel Caillibaud>  
`* |   `[7005d293f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7005d293f) Merge branch 'multiplesFixesBugsnag' into 'master' (2021-10-18 14:57) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[3c342b7ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c342b7ef) méthode EcritQuestion virée (jamais utilisée) (2021-10-18 15:51) <Daniel Caillibaud>  
`| * | `[32ee02a0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32ee02a0a) le conteneur devient une propriété du parcours (2021-10-18 15:49) <Daniel Caillibaud>  
`| * | `[aea8f32c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aea8f32c6) on calme un peu les plantages mathquill (2021-10-18 15:11) <Daniel Caillibaud>  
`| * | `[6b333ac3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b333ac3f) bugfix xmlText undefined (2021-10-18 15:08) <Daniel Caillibaud>  
`| * | `[0ab472333](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ab472333) j3pInputDyn3 mis en privé pour j3pAffiche (faudrait le virer complètement), et passé en synchrone (2021-10-18 14:59) <Daniel Caillibaud>  
`| * | `[88ca9ec0b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88ca9ec0b) import addTable + fix eslint (2021-10-18 14:32) <Daniel Caillibaud>  
`| * | `[cd45eca05](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd45eca05) code mort viré (y'avait jamais d'élément #fixFocusListener) (2021-10-18 13:54) <Daniel Caillibaud>  
`| |/  `  
`* |   `[67992868f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67992868f) Merge branch 'bugsnagRacineZone' into 'master' (2021-10-18 14:55) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[56becc63b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56becc63b) bugsnag (2021-10-18 16:11) <Tommy>  
`| |/  `  
`* |   `[acf7774b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/acf7774b9) Merge branch 'eslintTriang2etbugBoutons' into 'master' (2021-10-18 14:54) <Daniel Caillibaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[df10541cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df10541cf) eslint (2021-10-18 14:50) <Tommy>  
`* |   `[098749064](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/098749064) Merge branch 'Correction_Squelettes' into 'master' (2021-10-18 11:42) <Daniel Caillibaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[d985fb2d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d985fb2d0) J'ai roujouté le bouton Racine carrée par défaut ce qui devait être le cas avant (2021-10-18 12:21) <Yves Biton>  
`* | `[ce047c32f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce047c32f) hotfix variable réaffectée avec undefined (2021-10-18 12:36) <Daniel Caillibaud>  
`* | `[96fed5ea7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96fed5ea7) hotfix code js dans des string (2021-10-18 12:32) <Daniel Caillibaud>  
`* | `[a527bdc3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a527bdc3f) fix code js dans une string (pb bugsnag stor undefined) (2021-10-18 12:31) <Daniel Caillibaud>  
`* | `[ce2460d0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce2460d0e) retag 1.0.24 (2021-10-18 11:41) <Daniel Caillibaud>  
`|/  `  
`* `[2468e96ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2468e96ed) fauxtableur.js viré (remplacé par Tableur.js depuis belle lurette (2021-10-18 10:12) <Daniel Caillibaud>  
`*   `[91ab5cd29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91ab5cd29) Merge branch 'labocematinnomme2' into 'master' (2021-10-18 08:06) <barroy tommy>  
`|\  `  
`| * `[7adba3fa0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7adba3fa0) nom des rectangles carres (2021-10-18 10:05) <Tommy>  
`|/  `  
`*   `[90c1c15ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90c1c15ab) Merge branch 'Adaptation_Sections_Clavier_Virtuel' into 'master' (2021-10-18 07:57) <Yves Biton>  
`|\  `  
`| * `[b06b15225](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b06b15225) Correction d'un ?. oublié dans j3PDesactive (2021-10-18 09:53) <Yves Biton>  
`| * `[1492463c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1492463c4) Annulation d'erreur (2021-10-18 09:48) <Yves Biton>  
`| * `[99d7c8d82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99d7c8d82) Correction de j3PDesactive (2021-10-18 09:46) <Yves Biton>  
`| * `[16ad816c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16ad816c9) Ajout d'un commentaire en début des sections qui ont été adaptées au clavier virtuel (2021-10-18 09:11) <Yves Biton>  
`| * `[ef39c288f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef39c288f) Modif pour essayer de mieux contrer le problème du curseur fantôme non clignotant sur tablette. (2021-10-17 16:53) <Yves Biton>  
`| * `[d646d2d8f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d646d2d8f) Amélioration du focus sur erreur de 4 des sections et squelettes de Yves (2021-10-17 11:28) <Yves Biton>  
`| * `[b47891100](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b47891100) Petite modif d clavier virtuel es espérant éliminer le cas de l'éditeur Mathquill qui devient riquiqui. (2021-10-17 09:32) <Yves Biton>  
`| * `[874f9d3aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/874f9d3aa) Modifications de forme simplement (2021-10-17 09:06) <Yves Biton>  
`| * `[49be8edc9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49be8edc9) Modifications de forme simplement (2021-10-17 09:06) <Yves Biton>  
`| * `[2db13c857](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2db13c857) nettoyage tauxEvolution + petite modif calculatrice (2021-10-16 12:27) <Rémi Deniaud>  
`| * `[dd310936a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd310936a) Ajout d'une variable isActive au clavier virtuel pour qu'on puisse le désactiver et masquer la flèche le faisant appraître dans j3PDesactive. Avec ces modifs la section de Rémi natureSuite semble bien marcher. (2021-10-16 10:19) <Yves Biton>  
`| * `[afceb124a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/afceb124a) Rajout de const parcours = this en début de section (2021-10-15 17:31) <Yves Biton>  
`| * `[02a318d98](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02a318d98) Adaptaton de la section de Tommy airelateraleprimsecylindre au clavier virtuel. (2021-10-15 15:55) <Yves Biton>  
`| * `[86fb244ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86fb244ae) Adaptation de deux sectiosn  de Rémi au clavier virtuel mais je le laisse corriger le reste pour compatibilté Sésaparcours (2021-10-15 14:31) <Yves Biton>  
`* |   `[c3d046033](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c3d046033) Merge branch 'Amelioration_Focus_J3PAffiche' into 'master' (2021-10-18 07:39) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[ec20fb585](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec20fb585) commentaires foireux virés (2021-10-18 09:39) <Daniel Caillibaud>  
`| * | `[c2b31e398](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2b31e398) Moodification de J"PAffiche pour que le focus soit donné à un seul éditeur MathQuill (s'il y en a) et qu'il soit appelé via j3PFocus (Nécessaire pour compatibilité avec clavier virtuel). (2021-10-15 17:42) <Yves Biton>  
`| |/  `  
`* |   `[2aaa0b781](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2aaa0b781) Merge branch 'feraisDEcoupe' into 'master' (2021-10-18 07:38) <barroy tommy>  
`|\ \  `  
`| * | `[a7a129eea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7a129eea) defaulttable si pas de classname et corrige ptit bug decoupe (2021-10-18 09:37) <Tommy>  
`|/ /  `  
`* |   `[90a3df2e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90a3df2e6) Merge branch 'nomm2sphere' into 'master' (2021-10-18 00:55) <barroy tommy>  
`|\ \  `  
`| * | `[8c1b4d043](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c1b4d043) sphere (2021-10-18 02:54) <Tommy>  
`|/ /  `  
`* |   `[ec94323dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec94323dd) Merge branch 'birdynom2' into 'master' (2021-10-16 20:33) <barroy tommy>  
`|\ \  `  
`| * | `[5d5676747](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d5676747) cube (2021-10-16 22:31) <Tommy>  
`* | |   `[db0f81507](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db0f81507) Merge branch 'ptibugEtiq' into 'master' (2021-10-15 20:58) <barroy tommy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[c4710d1e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4710d1e9) bugsnag (2021-10-15 22:57) <Tommy>  
`|/ /  `  
`* |   `[a32c546c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a32c546c5) Merge branch 'rerebienecrire' into 'master' (2021-10-15 15:52) <barroy tommy>  
`|\ \  `  
`| * | `[a5faeb376](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5faeb376) ajout d'un margin0 ca fasait nimp (2021-10-15 17:51) <Tommy>  
`|/ /  `  
`* |   `[d4cae41fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4cae41fd) Merge branch 'mutuAddTableSuite' into 'master' (2021-10-15 15:43) <barroy tommy>  
`|\ \  `  
`| * | `[42631aa11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42631aa11) id bidons virés (on récupère directement les cellules construite par addTable) (2021-10-15 12:07) <Daniel Caillibaud>  
`| * | `[80a536ba2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80a536ba2) id bidons virés (on récupère directement les cellules construite par addTable) (2021-10-15 11:56) <Daniel Caillibaud>  
`| * | `[d0282a5f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0282a5f4) manquait mathquill (2021-10-14 23:31) <Tommy>  
`| * | `[98b60622b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98b60622b) couleur et eslint en retard (2021-10-14 23:16) <Tommy>  
`| * | `[3c8f9bba7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c8f9bba7) un petit bug d'etape et couleur (2021-10-14 22:58) <Tommy>  
`| * | `[f58ef0241](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f58ef0241) pareil (2021-10-14 22:37) <Tommy>  
`| * | `[ddc1ef3c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ddc1ef3c3) pareil (2021-10-14 22:24) <Tommy>  
`| * | `[7c7963e90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c7963e90) couleurs (2021-10-14 22:07) <Tommy>  
`| * | `[75803162a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75803162a) mutualisation addTable suite (2021-10-14 19:13) <Daniel Caillibaud>  
`| * | `[e871e502c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e871e502c) nettoyage (2021-10-14 18:56) <Daniel Caillibaud>  
`| * | `[abc0a6804](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/abc0a6804) nettoyage et fix eslint (2021-10-14 18:49) <Daniel Caillibaud>  
`| * | `[a83708a8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a83708a8a) nettoyage (2021-10-14 18:35) <Daniel Caillibaud>  
`| * | `[df3b48142](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df3b48142) aj mutualisation addTable suite (2021-10-14 14:40) <Daniel Caillibaud>  
`| * | `[52df68ee0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52df68ee0) aj mutualisation addTable trianglesegaux (2021-10-14 14:40) <Daniel Caillibaud>  
`* | |   `[6d759395c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d759395c) Merge branch 'eslintSommeA' into 'master' (2021-10-15 15:41) <barroy tommy>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[1aa73698b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1aa73698b) le bug de bugsnag (2021-10-15 17:40) <Tommy>  
`| * | `[b2ec45ba4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2ec45ba4) pour sauvegarde (2021-10-15 12:42) <Tommy>  
`* | |   `[d75426b8f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d75426b8f) Merge branch 'assLaboNomme2' into 'master' (2021-10-15 10:54) <barroy tommy>  
`|\ \ \  `  
`| * | | `[19873cc48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19873cc48) pour sauvegarde (2021-10-15 12:54) <Tommy>  
`|/ / /  `  
`* | |   `[c63e43820](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c63e43820) Merge branch 'Correction_De_Ressources' into 'master' (2021-10-15 10:02) <Yves Biton>  
`|\ \ \  `  
`| * | | `[c2c133038](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2c133038) Correction de ressource suite à rapport LaboMep (2021-10-15 11:59) <Yves Biton>  
`| * | | `[85e47d14f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85e47d14f) Correction d'un énoncé (2021-10-15 11:56) <Yves Biton>  
`|/ / /  `  
`* | |   `[62c2b6176](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62c2b6176) Merge branch 'Amelioration_Squelettes' into 'master' (2021-10-15 08:43) <Yves Biton>  
`|\ \ \  `  
`| * | | `[caa4900fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/caa4900fb) Amélioration de la gestion de la largeur et hauteur de la figure affihant la figure et la solution (qui peuvent être nulles pour certaines sections ou squelettes quand ils affichent la solution via MathQuill) (2021-10-15 10:39) <Yves Biton>  
`|/ / /  `  
`* | |   `[400fee989](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/400fee989) Merge branch 'rerererererBIenEcr' into 'master' (2021-10-15 06:08) <barroy tommy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[f13e95fc8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f13e95fc8) mise a jour addtable (2021-10-15 08:07) <Tommy>  
`|/ /  `  
`* | `[ff18bc45b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff18bc45b) retag 1.0.23 | faut gérer le static pour es5 aussi (2021-10-15 02:16) <Daniel Caillibaud>  
`* |   `[b0c7d2669](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0c7d2669) Merge branch 'ptibugtable' into 'master' (2021-10-14 19:41) <barroy tommy>  
`|\ \  `  
`| * | `[9b2662e41](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b2662e41) estab (2021-10-14 21:40) <Tommy>  
`| * |   `[344f2d937](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/344f2d937) Merge branch 'master' of https://framagit.org/Sesamath/sesaparcours (2021-10-14 21:36) <Tommy>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[e54cbeb01](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e54cbeb01) Merge branch 'rereprobleme' into 'master' (2021-10-14 19:35) <barroy tommy>  
`|\ \ \  `  
`| * | | `[7179e6401](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7179e6401) estab (2021-10-14 21:33) <Tommy>  
`| * | | `[b951359f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b951359f8) estab (2021-10-14 21:21) <Tommy>  
`| | |/  `  
`| |/|   `  
`| | * `[b2c2401f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2c2401f7) estab (2021-10-14 21:24) <Tommy>  
`| |/  `  
`|/|   `  
`* |   `[4fbe50549](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fbe50549) Merge branch 'fixLoadingErrors' into 'master' (2021-10-14 18:32) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[fa54b1a48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa54b1a48) fix chargement du html, nettoyage (2021-10-14 20:31) <Daniel Caillibaud>  
`| * | `[b97884779](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b97884779) nettoyage + loading propre du html (2021-10-14 20:15) <Daniel Caillibaud>  
`| * | `[56321c6be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56321c6be) aj du loader html (2021-10-14 20:14) <Daniel Caillibaud>  
`| * | `[65693aa16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65693aa16) fix warning d'un style en string (2021-10-14 19:46) <Daniel Caillibaud>  
`| * | `[a92beb0d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a92beb0d2) fix pb de chargement des sons, nettoyage (2021-10-14 19:43) <Daniel Caillibaud>  
`| * | `[524c7aff7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/524c7aff7) hotfix erreur bugsnag 613b2b416a041300072d1257 (elt inexistant) (2021-10-14 19:24) <Daniel Caillibaud>  
`|/ /  `  
`* | `[8b648cd56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b648cd56) hotfix parentheses en double (2021-10-14 19:18) <Daniel Caillibaud>  
`* | `[0d54048f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d54048f7) hotfix pour les j3pFocus sur inputMq sans clavier virtuel (2021-10-14 18:19) <Daniel Caillibaud>  
`* | `[00cffa1c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00cffa1c3) retag 1.0.22 (2021-10-14 18:14) <Daniel Caillibaud>  
`* |   `[75437209e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75437209e) Merge branch 'hotfixBugsnag' into 'master' (2021-10-14 16:13) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[36430b2db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/36430b2db) fix addTable2 inexistant (2021-10-14 18:11) <Daniel Caillibaud>  
`| * | `[59de009df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/59de009df) nettoyage (sans autre modif de code) (2021-10-14 18:11) <Daniel Caillibaud>  
`| * | `[7a0e280fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a0e280fe) fix image d'aide pas chargée (faut mettre les suffixes en minuscule dans les noms de fichier et passer par un import) + fix bouton brouillon (un appel de stor.truc passé dans une string) (2021-10-14 18:10) <Daniel Caillibaud>  
`| * | `[8e2f29d68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e2f29d68) fix bugsnag 6168388522e29f00077c0d01 + eslint fixes (mais il reste des var) (2021-10-14 18:10) <Daniel Caillibaud>  
`| * | `[19c79c258](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19c79c258) fix addTable2 qui n'existait plus + eslint (2021-10-14 18:10) <Daniel Caillibaud>  
`| * | `[d49882452](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d49882452) fix dernier pb eslint (2021-10-14 18:10) <Daniel Caillibaud>  
`| * | `[5e1f0d61d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e1f0d61d) parenthèse en double virées (2021-10-14 18:10) <Daniel Caillibaud>  
`| |/  `  
`* |   `[38cfd58f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38cfd58f5) Merge branch 'Correction_Squelettes_Apres_Migration_Clavier_Virtuel' into 'master' (2021-10-14 16:02) <Yves Biton>  
`|\ \  `  
`| * | `[7c3fd6a0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c3fd6a0f) Correction de focsu sur erreur de syntaxe. (2021-10-14 18:01) <Yves Biton>  
`* | | `[13770facd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13770facd) Merge branch 'Correction_Squelettes_Apres_Migration_Clavier_Virtuel' into 'master' (2021-10-14 13:12) <Yves Biton>  
`|\| | `  
`| |/  `  
`|/|   `  
`| * `[e18c23330](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e18c23330) Correction d'un fichier annexe (2 boutons en trop). (2021-10-14 15:11) <Yves Biton>  
`* | `[4947ee286](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4947ee286) Merge branch 'Correction_Squelettes_Apres_Migration_Clavier_Virtuel' into 'master' (2021-10-14 12:31) <Yves Biton>  
`|\| `  
`| * `[304ac07a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/304ac07a7) Correction d'un squelette et un fichier annexe (2021-10-14 14:27) <Yves Biton>  
`* |   `[06a767d86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06a767d86) Merge branch 'remet_addtableds_decoupe' into 'master' (2021-10-14 12:31) <barroy tommy>  
`|\ \  `  
`| * | `[edf5f60c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edf5f60c9) done (2021-10-14 14:29) <Tommy>  
`|/ /  `  
`* |   `[2d88183d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d88183d9) Merge branch 'dedecoup01' into 'master' (2021-10-14 12:04) <barroy tommy>  
`|\ \  `  
`| * | `[51c258ca8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51c258ca8) avec remise de structure (2021-10-13 23:01) <Tommy>  
`| * | `[4bd4aebc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4bd4aebc7) pour sauvegarde (2021-10-13 08:43) <Tommy>  
`| * | `[4d3ea1b00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d3ea1b00) pour sauvegarde (2021-10-11 22:25) <Tommy>  
`| * | `[b28301eb7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b28301eb7) pour sauvegarde (2021-10-11 22:22) <Tommy>  
`| * | `[3d7365c1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d7365c1f) pour sauvegarde (2021-10-11 19:50) <Tommy>  
`| * | `[b0b7df208](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0b7df208) pour sauvegarde (2021-10-11 10:44) <Tommy>  
`| * | `[d607ea624](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d607ea624) pour sauvegarde (2021-10-10 23:31) <Tommy>  
`| * | `[d76fc34a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d76fc34a8) pour sauvegarde (2021-10-10 15:10) <Tommy>  
`| * | `[380f5b9d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/380f5b9d9) sauvegarde (2021-09-29 23:55) <Tommy>  
`| * | `[c90940e62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c90940e62) pour sauvegarde (2021-09-26 16:54) <Tommy>  
`| * | `[c302c0c26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c302c0c26) notify pour bugsnag (2021-09-25 14:03) <Tommy>  
`|  /  `  
`* |   `[b6ee6df81](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6ee6df81) Merge branch 'fixAddTable' into 'master' (2021-10-14 12:01) <barroy tommy>  
`|\ \  `  
`| * | `[73edc4591](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73edc4591) aj addDefaultTable (2021-10-14 11:54) <Daniel Caillibaud>  
`| * | `[c5c1cf1dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5c1cf1dc) ass labomep et un autre bug que j'ai vu hier, le addtable ca a l'air de marcher, est ce que on peut rajouter dedans un addtable2 pour les feignasses avec juste (idconteneur, nblignes, nbcolonnes) il choisira une id au pif que on s'en fout, qui renvoie un tableau [] nbligne nbcolonne, avec les td dedans ? (2021-10-14 08:11) <Tommy>  
`| * | `[702508d8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/702508d8e) fix import manquant (2021-10-14 01:45) <Daniel Caillibaud>  
`| * | `[2cab59ace](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2cab59ace) fix css (2021-10-14 01:45) <Daniel Caillibaud>  
`| * | `[a2737488d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2737488d) mutualisation addTable (2021-10-14 01:35) <Daniel Caillibaud>  
`| * | `[7d0e0fe93](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d0e0fe93) mutualisation addTable (2021-10-14 00:51) <Daniel Caillibaud>  
`| * | `[541708b56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/541708b56) harmonisation else avec if (2021-10-14 00:48) <Daniel Caillibaud>  
`| * | `[1130fda97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1130fda97) css mise avec addTable (2021-10-11 20:56) <Daniel Caillibaud>  
`| * | `[2ef0602de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ef0602de) nomme02 externalise addTable et sa css (2021-10-11 20:20) <Daniel Caillibaud>  
`| * | `[906cb6393](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/906cb6393) fix addTable (qui reste générique, et n'applique aucun style ni props autres que ceux indiqués par la doc (2021-10-11 20:20) <Daniel Caillibaud>  
`* | |   `[3a6cff827](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a6cff827) Merge branch 'Correction_Squelettes_Apres_Migration_Clavier_Virtuel' into 'master' (2021-10-14 11:36) <Yves Biton>  
`|\ \ \  `  
`| | |/  `  
`| |/|   `  
`| * | `[8d17233dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d17233dd) Correction de squelettes après adaptation clavier virtuel. Correction de focus n'utilisant pas j3PFocus (indispensable pour clavier virtuel). Passage d'un section à ES6. Plus  corrections diverses (2021-10-14 13:30) <Yves Biton>  
`|/ /  `  
`* |   `[771ea1af2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/771ea1af2) Merge branch 'Adaptation_Squelettes_Clavier_Virtuel' into 'master' (2021-10-14 09:20) <Yves Biton>  
`|\ \  `  
`| * | `[362f88269](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/362f88269) Amélioration du squelette de calcul de somme ou différence de vractions (plus de bouton frac à l'étape 1 et meilleure gestion du focus) (2021-10-14 10:33) <Yves Biton>  
`| * | `[c57f15030](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c57f15030) Adaptation de la dernière section permettant de faire des ressources utilisateur à mqRestriction. (2021-10-14 09:48) <Yves Biton>  
`| * | `[a4c139661](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a4c139661) Version qui marche à peu près sauf le problème de changement de focus sur FireFox avec écran tactile. Il y a encore sur tablette très rarement un cuseur fanome non clignotant quand on se déplace avec le doigt dans une formule (très rare). Il s'agit d'un bug MathQuill et j'abandonne de vouloir le ocrriger car mes corrections apportent à chaque fois des problèmes ailleurs ... J'ai viré les console.log mis par Daniel pour la correction du pb de regexp. (2021-10-13 17:03) <Yves Biton>  
`| * | `[44a086703](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44a086703) Pour tester je mest les deux isTouchDevice à false et le onclick de virtualKeyboard vide (2021-10-13 15:15) <Yves Biton>  
`| * | `[0256d4b12](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0256d4b12) modif de la regexp pour régler le plantage du chargement des dépendances de j3pStart sur iPad (pas trouvé pourquoi, surtout que le plantage survient avant que ce js ne soit chargé) (2021-10-13 14:55) <Daniel Caillibaud>  
`| * | `[9966f10a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9966f10a6) on commente le param babel loose (2021-10-13 14:37) <Daniel Caillibaud>  
`| * | `[41446a3cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41446a3cc) update dependencies (2021-10-13 12:23) <Daniel Caillibaud>  
`| * | `[6955722ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6955722ff) Suppression d'un test inutile (2021-10-13 10:32) <Yves Biton>  
`| * | `[807ea9366](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/807ea9366) Version qui fontionne avec FireFox mais il reste un problèmesous FireFox avce Tab et Shft-Tab qui ne fonctionne pas en tout cas sur mon PC à écran tactile. (2021-10-13 08:49) <Yves Biton>  
`| * | `[bf36a34c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf36a34c2) Amélioration de section (2021-10-11 15:43) <Yves Biton>  
`| * | `[232d947ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/232d947ab) modif regex de remplacement des caractères à échapper (2021-10-11 15:43) <Daniel Caillibaud>  
`| * | `[f48778633](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f48778633) modif regex de remplacement des caractères à échapper (2021-10-11 15:38) <Daniel Caillibaud>  
`| * | `[53aef5205](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53aef5205) Mise à jour de sections pour créer ses propres ressources Pas encore fini. (2021-10-11 14:54) <Yves Biton>  
`| * | `[ba18e0868](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba18e0868) Adapatation de tous les squelettes à mqRestriction. Amélioration de certains et correction de fichiers annexes. Adaptation de trois autres sections pour faire ses propres ressources. Amélioration de mQRestriction pour filtrer les caractères nécéessitant un \ d'échapement. (2021-10-11 10:54) <Yves Biton>  
`| * | `[cb2a8bc78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb2a8bc78) aj boutouns xyz et puissance (2021-10-07 10:05) <Daniel Caillibaud>  
`| * | `[5e36bdc80](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e36bdc80) fix puissance rognée (2021-10-07 10:05) <Daniel Caillibaud>  
`|  /  `  
`* |   `[ab0e51800](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab0e51800) Merge branch 'eslintvalap' into 'master' (2021-10-13 14:17) <barroy tommy>  
`|\ \  `  
`| * | `[9c5748724](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c5748724) avec remise de structure (2021-10-13 16:16) <Tommy>  
`| * | `[038859447](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/038859447) eslint (2021-10-12 12:47) <Tommy>  
`* | |   `[df56359fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df56359fc) Merge branch 'eslintpoint' into 'master' (2021-10-13 09:33) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[613ccdca1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/613ccdca1) eslint (2021-10-12 21:29) <Tommy>  
`| |/ /  `  
`* | |   `[4b44e1f01](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b44e1f01) Merge branch 'refixEcrChifMiniBug' into 'master' (2021-10-12 19:38) <barroy tommy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[d816831ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d816831ed) fix this metblanc (via setTimeout) (2021-10-12 02:35) <Daniel Caillibaud>  
`| * | `[299457101](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/299457101) fix this cassé avec la factorisation des méthodes dans le prototype (2021-10-11 21:17) <Daniel Caillibaud>  
`| * | `[17d311fbb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17d311fbb) bugfix gereVireListener (2021-10-11 18:15) <Daniel Caillibaud>  
`| * | `[1e70d4c56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e70d4c56) méthodes de Ref et Ref2 délarées comme méthodes (du prototype) et plus comme propriété fonctionnelle de chaque instance de l'objet (2021-10-11 11:37) <Daniel Caillibaud>  
`| * | `[6164ac4ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6164ac4ad) simplification (2021-10-11 11:10) <Daniel Caillibaud>  
`| * | `[4a48a00d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a48a00d9) bugsnag (2021-10-09 13:03) <Tommy>  
`|  /  `  
`* / `[4fbde7b16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fbde7b16) retag 1.0.21 (2021-10-12 02:36) <Daniel Caillibaud>  
`|/  `  
`*   `[0286ca66e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0286ca66e) Merge branch 'nomme02toujurs' into 'master' (2021-10-11 14:34) <barroy tommy>  
`|\  `  
`| * `[c9ba9959a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9ba9959a) toutes les figures 2D (2021-10-09 12:55) <Tommy>  
`| * `[77dcebdc0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77dcebdc0) sauv la sauvegarde (2021-10-07 22:40) <Tommy>  
`| * `[5331d5e10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5331d5e10) pour sauvegarde encore (2021-10-06 00:20) <Tommy>  
`| * `[47c1446e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47c1446e8) pour sauvegarde (2021-10-05 13:36) <Tommy>  
`*   `[2cefce023](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2cefce023) Merge branch 'fixTomblok' into 'master' (2021-10-11 14:33) <barroy tommy>  
`|\  `  
`| * `[0ffcf3731](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ffcf3731) retirer le jquery en trop je commit push merge pour voir l'erreur dans bugsnag (2021-10-11 16:32) <Tommy>  
`| * `[7d21056fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d21056fd) correction d'un bug de correction, faut prod le tomblok qui va avec, j'ai mis des consoles pour capter un autre bug (2021-09-27 17:14) <Tommy>  
`| * `[33e7d154b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33e7d154b) rajout dun console.log pour le prochain bugsnag, NB je sais pas par quoi remplacer les j3p NB2 je sais pas le eslint jquery en haut (2021-09-23 14:55) <Tommy>  
`*   `[39da3ba88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39da3ba88) Merge branch 'eslintfraction02' into 'master' (2021-10-11 10:46) <Daniel Caillibaud>  
`|\  `  
`| * `[43c6e00db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43c6e00db) fix derniers pbs eslint (2021-10-11 12:45) <Daniel Caillibaud>  
`| * `[16e29e46a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16e29e46a) renommage yareponse => yaReponseOuTropTard (2021-10-11 12:42) <Daniel Caillibaud>  
`| * `[35f905b6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35f905b6a) eslint (2021-10-06 20:52) <Tommy>  
`*   `[b1bd8bba1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1bd8bba1) Merge branch 'eslintprop01' into 'master' (2021-10-11 10:24) <Daniel Caillibaud>  
`|\  `  
`| * `[be37cea86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be37cea86) simplification (2021-10-11 12:23) <Daniel Caillibaud>  
`| * `[1198bc187](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1198bc187) pour sauvegarde (2021-10-10 18:46) <Tommy>  
`* |   `[631e1683f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/631e1683f) Merge branch 'eslintQuad01' into 'master' (2021-10-11 10:12) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[cdec964e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdec964e7) un peu d'eslint et de ménage (2021-10-10 12:42) <Tommy>  
`| |/  `  
`* / `[a45d7a0db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a45d7a0db) aj règle curly consistent (2021-10-11 12:06) <Daniel Caillibaud>  
`|/  `  
`*   `[136da3cca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/136da3cca) Merge branch 'eslintDevelop' into 'master' (2021-10-09 13:12) <barroy tommy>  
`|\  `  
`| * `[7f94feb8f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f94feb8f) eslint et deux correc d'etiquettes ( je m'étais pas mis dans la bonne branche pour tester merger ) (2021-10-09 15:11) <Tommy>  
`|/  `  
`* `[9f785a545](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f785a545) Merge branch 'refactoEtiquettes' into 'master' (2021-10-09 10:59) <barroy tommy>  
`* `[e73ac753c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e73ac753c) aj jsdoc (2021-10-06 23:37) <Daniel Caillibaud>  
`* `[b238cc07f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b238cc07f) afaire peut être undefined (2021-10-06 23:13) <Daniel Caillibaud>  
`* `[e98c709c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e98c709c7) y'avait du this.this.maouvellediv et j'ai mis une fonction vide quand y'a pas de fonction dans afaire Apperemment y'a que 3 sections qui l'utilisent (2021-10-06 22:22) <Tommy>  
`* `[20d428065](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20d428065) nettoyage quad02 (2021-10-06 13:02) <Daniel Caillibaud>  
`* `[b586701a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b586701a5) découpage de etiquettes.js en deux fichiers, chacun sa classe, Etiquettes et PlaceEtiquettes, avec des méthodes qui passent dans le prototype (plutôt que de les redéfinir sur chaque objet), afaire toujours une fonction quand ça existe, plus de eval (+fixes eslint) (2021-10-06 12:43) <Daniel Caillibaud>  

## version 1.0.20

`* `[630486008](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/630486008) retag 1.0.20 (2021-10-07 13:22) <Daniel Caillibaud>  
`*   `[db8af9f41](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db8af9f41) Merge branch 'fixScorePresentation' into 'master' (2021-10-07 11:22) <Daniel Caillibaud>  
`|\  `  
`| * `[75de130ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75de130ca) fix score presentation (pas de score, faut pas baisser la moyenne) (2021-10-07 13:21) <Daniel Caillibaud>  
`|/  `  
`*   `[d888327a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d888327a3) Merge branch 'fixSokoban' into 'master' (2021-10-07 10:48) <Daniel Caillibaud>  
`|\  `  
`| * `[3fd389e79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3fd389e79) fix chargement sokoban (2021-10-07 12:46) <Daniel Caillibaud>  
`| * `[53b1338ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53b1338ed) fichiers inutiles virés (2021-10-07 10:06) <Daniel Caillibaud>  
`| * `[b565ad6c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b565ad6c2) on utilise this.storage (2021-10-07 10:06) <Daniel Caillibaud>  
`|/  `  
`*   `[19b49114a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19b49114a) Merge branch 'eslintfraction05' into 'master' (2021-10-06 21:00) <Daniel Caillibaud>  
`|\  `  
`| * `[17901f96a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17901f96a) j'avais eslinté qu'une motiée... alors ca bugguait (2021-10-06 21:40) <Tommy>  
`* |   `[5519be446](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5519be446) Merge branch 'fixFraction07' into 'master' (2021-10-06 20:46) <barroy tommy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[f7489eaab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7489eaab) c'était les espaces dans la réponse que j'avais oublié de virer (2021-10-06 22:45) <Tommy>  
`| * `[bb2aff754](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb2aff754) fix jsdoc j3pArrondi (2021-10-05 14:41) <Daniel Caillibaud>  
`| * `[1f898c4bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f898c4bd) bugfix nb de décimales (2021-10-05 14:34) <Daniel Caillibaud>  
`| * `[d2560a1c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2560a1c4) on appelle plus sectionCourante avant finCorrection (toléré mais risqué) (2021-10-05 14:29) <Daniel Caillibaud>  
`| * `[e04ed6d5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e04ed6d5a) nettoyage (2021-10-05 14:11) <Daniel Caillibaud>  
`* |   `[c14845bb1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c14845bb1) Merge branch 'fixDem01LaVraie' into 'master' (2021-10-06 19:19) <barroy tommy>  
`|\ \  `  
`| * | `[b8ad9ebe3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8ad9ebe3) j'avais eslinté qu'une motiée... alors ca bugguait (2021-10-06 21:17) <Tommy>  
`|/ /  `  
`* | `[47f3dea03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47f3dea03) retag 1.0.19 (2021-10-06 13:18) <Daniel Caillibaud>  
`* |   `[0c9d8d606](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c9d8d606) Merge branch 'snagProp' into 'master' (2021-10-06 11:17) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[c97a34a28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c97a34a28) j3pElement inutiles virés (on a déjà l'élément dans me.zonesElts.MG) (2021-10-06 13:15) <Daniel Caillibaud>  
`| * | `[c120675d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c120675d3) j'ai corrigé un truc, mais je sais plus quoi... (2021-10-06 08:30) <Tommy>  
`| * | `[1bbaa747a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bbaa747a) bugsnag dans enoncemain (2021-10-06 08:20) <Tommy>  
`* | |   `[8f5ff93ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f5ff93ef) Merge branch 'snagParaperp' into 'master' (2021-10-06 11:05) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[778bae5c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/778bae5c4) bugsnag dans undlecancel (2021-10-06 08:17) <Tommy>  
`| |/ /  `  
`* | |   `[9dc39239f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9dc39239f) Merge branch 'eslintDem01PlusBUgCo' into 'master' (2021-10-06 11:03) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[04d32e281](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04d32e281) vire eval etiquettes pour cette section (2021-10-06 08:08) <Tommy>  
`| * | | `[08d01b238](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/08d01b238) eslint et la phrase modifiée et quand il faut (2021-10-06 01:12) <Tommy>  
`| |/ /  `  
`* | |   `[ca0b52285](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca0b52285) Merge branch 'ameliorationMqVirtualKeyboard' into 'master' (2021-10-06 09:09) <Yves Biton>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[396be6708](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/396be6708) Correction de squelettes. Spécification de boundingContainer lors de l'appel de mqRestriction (2021-10-06 10:50) <Yves Biton>  
`| * | `[f63f52d52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f63f52d52) Règle le fait que que, dans le clavier virtuel, on clique sur un bouton de commande, le curseur clignotant disparaissait. (2021-10-05 19:18) <Yves Biton>  
`| * | `[57d049330](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57d049330) J'ai trouvé un work-around pour éviter le curseur fantôme quand on tape à toute vitesse dans le clavier virtuel sur une touche pour insérer un caractère. Amélioration de deux squelettes. (2021-10-05 19:12) <Yves Biton>  
`| * | `[092bba211](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/092bba211) Modification de squelettes. Modification provisoire d'un fichier annexe pour faire des tests sur squelettemtg32_Calc_Multi-Edit. (2021-10-05 14:47) <Yves Biton>  
`| * | `[31bf045ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31bf045ff) Modification de j3pFocus (2021-10-05 12:39) <Yves Biton>  
`| * | `[eb0083225](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb0083225) Version qui semble enfin fonctionner à la fois sur PC et sur tablette. A tester quand me^me sur PC sans écran tactile ... (2021-10-05 11:14) <Yves Biton>  
`| * | `[b3a64bf77](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3a64bf77) onFocusCallback mis dans les options aussi pour le constructeur (2021-10-04 20:30) <Daniel Caillibaud>  
`| * | `[5ca4c7c5d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ca4c7c5d) Le onFocusCallback est maintenant dans options dans mqRestricton (2021-10-04 15:55) <Yves Biton>  
`| * | `[7611b4a36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7611b4a36) Version qui semble résoudre le problème aléatoire du curseur fantôme en plus du curseur clignotant après un touch sur l'éditeur (principalement sur iPad) (2021-10-04 13:23) <Yves Biton>  
`| * | `[93e24d98d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93e24d98d) Suppression du listener sur focusout mais le problème du curseur en double au touch  (dont l'un pas clignotant) subsiste sur ipad de façon rare mais tout de même ... Modification de j3PFocus. Marche mieux sur Android. (2021-10-04 09:55) <Yves Biton>  
`| * | `[2326452b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2326452b5) Suppression du listener sur focusout mais le problème du curseur en double au touch  (dont l'un pas clignotant) subsiste. (2021-10-03 09:35) <Yves Biton>  
`| * | `[49b08ec73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49b08ec73) Il y a un problème rédhibitoire : Une fois sur 100 quand on déplace le curseur au doigt, l'ancieb curseur ne disparait pas (et ne clignote pas) (2021-10-02 17:43) <Yves Biton>  
`| * | `[538fce6c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/538fce6c4) Version qui tient bien la route sur PC et tablette où on n'utilise plus dans fakeFocus  une simulation de touce espace suivi d'un backspace car lors d'une sélection à la souris par  exemple tout le contenu disparaissait. Version qui accepte aussi au clavier la touche Delete sur PC. Modification de la section pour ne plus appeler le focus directement mais appeler j3pFocus() (2021-10-02 11:32) <Yves Biton>  
`| * | `[7e9cfa62e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e9cfa62e) Version qui gère correctement les touches Tab et Shift-Tab à l'intérieur d'un éditeur MathQuill (2021-10-01 18:33) <Yves Biton>  
`| * | `[72d4cc4f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72d4cc4f5) Version qui gère correctement les touches Tab et Shift-Tab à l'intérieur d'un éditeur MathQuill (2021-10-01 18:33) <Yves Biton>  
`| * | `[1c4bed9a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c4bed9a9) Ajout d'une quatrième paramètre à mqRestriction qui est une fonction de callback à appeler quand on donne le focus (ou le psudo focus) à cer éditeur MathQuill. Servira dans certaines sections où on a des boutons MathQuill qui sont partagés entre plusieurs éditeurs MathQuill comme squelettemtg32_Calc_Multi_Edit_Multi_Etapes (2021-10-01 11:21) <Yves Biton>  
`| * | `[ae6738a56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae6738a56) Plus de isFocusedByTouch sur les inputMQ puisque, au  premier focus, on ne peut pas savoir si il a été activé par un touch (puisque activé par J3PFocus). (2021-10-01 08:33) <Yves Biton>  
`| * | `[ed8b2a2b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed8b2a2b5) Modification du squelette pour utiliser j3PFocus pour donner le focsu au premeir éditeur. Quand on utilise j3pFocus sur une machine avec touch  device on ouvre le clavier virtuel par défaut. (2021-09-30 22:29) <Yves Biton>  
`| * | `[8848019a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8848019a9) J'adapte ce qu'a fait Daniel pour draguer le clavier virtuel. (2021-09-30 21:46) <Yves Biton>  
`| * | `[ce59caf2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce59caf2d) Modification du sccs à l'identique de ce que Daniel a fait. (2021-09-30 21:34) <Yves Biton>  
`| * | `[31a9e391a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31a9e391a) Au deuxième appel de j3pFocus, si on est sur PC avec écran tactile et que le clavier virtuel a été replié, on ne l'affiche pas par défaut. (2021-09-30 18:58) <Yves Biton>  
`| * | `[2bfb159b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2bfb159b4) Problème du clavier virtuel lent résolu sur tablettes. Pour J3PFocus, on regarde si la machine a un écran sensitif et, dans ce cas, on puvre son clavier virtuel par défaut. La section squelettemtg32_Calc_Multi_Edit a été revue pour utiliser mqRestrictio mais des touches sortent de leur div et le clavier virtuel n'est plus déplaçable. (2021-09-30 17:01) <Yves Biton>  
`| * | `[4f26faa6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f26faa6a) j3pFocus utilise fakeFocus si c'est dispo, pour ne jamais déclencher le clavier virtuel natif de la tablette (2021-09-29 16:35) <Daniel Caillibaud>  
`| * | `[9ad43c98c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ad43c98c) Version où, si on a déjà utilisé un éditeur MathQuill ayant utilisé mqRestriction via un événement touch, quand on lui redonne le focus via j3pFocus on active le clavier virtuel. (2021-09-29 15:34) <Yves Biton>  
`| * | `[39b90beec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39b90beec) aj d'une méthode fakeFocus à l'inputMq associé à un VirtualKeyboard (2021-09-29 14:31) <Daniel Caillibaud>  
`* | |   `[07d3f6c51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07d3f6c51) Merge branch 'progconstprogcont' into 'master' (2021-10-05 12:50) <Daniel Caillibaud>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[acebe6ad9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/acebe6ad9) remplace const par let pour bugsnag (2021-10-05 14:11) <Tommy>  
`|/ /  `  
`* |   `[52b417545](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52b417545) Merge branch 'new_fracrep' into 'master' (2021-10-04 19:56) <barroy tommy>  
`|\ \  `  
`| * | `[81cb9971c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81cb9971c) eslint et mais finenonce dans enoncemain ( c'est ptet la cause du bugsnag si j'ai de la chance ) (2021-10-04 21:54) <Tommy>  
`|/ /  `  
`* | `[05469e5de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05469e5de) retag 1.0.18 (2021-10-04 18:20) <Daniel Caillibaud>  
`* |   `[8487c3cef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8487c3cef) Merge branch 'eslintComplete' into 'master' (2021-10-04 16:19) <Daniel Caillibaud>  
`|\ \  `  
`| * \   `[909bc4147](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/909bc4147) Merge branch 'master' into eslintComplete (2021-10-04 18:18) <Daniel Caillibaud>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[7d727fe8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d727fe8e) Merge branch 'eslintFractionscalculs' into 'master' (2021-10-04 16:15) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[7e1147e99](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e1147e99) lignes avec seulement un point virgule virées (2021-10-04 18:15) <Daniel Caillibaud>  
`| * | |   `[845b640e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/845b640e5) Merge branch 'master' into eslintFractionscalculs (2021-10-04 18:14) <Daniel Caillibaud>  
`| |\ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[477871e20](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/477871e20) On se calme sur les zIndex :-D (2021-10-04 18:07) <Daniel Caillibaud>  
`| * | | `[c90232bcd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c90232bcd) eslint, couleurs et un bouton OK qui apparaissait pas parfois (2021-10-04 00:06) <Tommy>  
`| * | | `[118a59d22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/118a59d22) ajout gros zindex pour passer au dessus boutons J3P (2021-10-03 23:36) <Tommy>  
`| | * |   `[9632f8c06](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9632f8c06) Merge branch 'master' into eslintComplete (2021-10-04 18:16) <Daniel Caillibaud>  
`| | |\ \  `  
`| |_|/ /  `  
`|/| | |   `  
`* | | |   `[53e39fc4e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53e39fc4e) Merge branch 'eslintParaPerp' into 'master' (2021-10-04 15:58) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[d8a8ff8b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8a8ff8b7) eslint et un return sur goingtouche pour zapper bugsnag (2021-10-03 23:20) <Tommy>  
`| |/ / /  `  
`* | | |   `[e591b7da9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e591b7da9) Merge branch 'rereposeOp' into 'master' (2021-10-04 15:56) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[b332b8785](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b332b8785) ajout qq colonnes (2021-10-03 22:12) <Tommy>  
`| |/ / /  `  
`* | | |   `[ab166dcd5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab166dcd5) Merge branch 'chgNomme02' into 'master' (2021-10-04 15:55) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[474e18a7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/474e18a7f) mg32 sur qqs figures, je continuerai si ya pas trop de bugsnag (2021-10-03 21:40) <Tommy>  
`| |/ / /  `  
`* | | |   `[2dd188e42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2dd188e42) Merge branch 'cm2' into 'master' (2021-10-04 15:54) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[ad1b54049](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad1b54049) derniers nettoyages du dossier cahiercm2 (2021-10-02 18:25) <Rémi Deniaud>  
`| * | | | `[33b412d7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33b412d7f) nettoyage cm2exM3_12 (2021-10-02 17:20) <Rémi Deniaud>  
`| * | | | `[ad6be91fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad6be91fc) nettoyage cm2exN1_18 (2021-10-02 16:31) <Rémi Deniaud>  
`| * | | | `[0119ce9b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0119ce9b4) nettoyage cm2exN4_76 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[75cd893f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75cd893f0) nettoyage cm2exN4_68 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[d3a9ed31a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3a9ed31a) nettoyage cm2exN4_65 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[315516b93](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/315516b93) nettoyage cm2exN4_41 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[14787c6ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14787c6ba) nettoyage cm2exN4_22 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[dad4cdfee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dad4cdfee) nettoyage cm2exN1_33 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[f0755eb29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0755eb29) nettoyage cm2exN1_32 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[84b5456ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84b5456ba) nettoyage cm2exN1_31 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[3c455dccb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c455dccb) nettoyage cm2exN1_26 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[1f07196a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f07196a4) nettoyage cm2exN1_23 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[70e9a10a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70e9a10a4) nettoyage cm2exN1_16 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[b477aba2c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b477aba2c) nettoyage cm2exN1_13 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[99a82c88b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99a82c88b) nettoyage cm2exN1_13 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[b81cda4b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b81cda4b5) nettoyage cm2exM3_11 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[5bf306750](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5bf306750) nettoyage cm2exA1_12 (2021-10-02 15:18) <Rémi Deniaud>  
`| * | | | `[308ee1206](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/308ee1206) nettoyage cm2exA1_11 (2021-10-02 15:18) <Rémi Deniaud>  
`| |/ / /  `  
`* | | |   `[ac6a66f91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac6a66f91) Merge branch 'python' into 'master' (2021-10-04 15:51) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[51249fb2c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51249fb2c) nettoyage algoSeuilSuite, mais il faut encore contrôler les graphes qui l'utilisent (2021-10-02 22:50) <Rémi Deniaud>  
`| * | | | `[569047d19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/569047d19) je vire la console si je tombe sur un pb avec les chevrons (2021-10-02 22:12) <Rémi Deniaud>  
`| |/ / /  `  
`* | | |   `[cced1cb97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cced1cb97) Merge branch 'secondDegre' into 'master' (2021-10-04 15:49) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[dc3b573ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc3b573ef) correction pb lorsqu'il n'y a qu'une racine non simplifiée (2021-10-02 15:16) <Rémi Deniaud>  
`| |/ / /  `  
`* | | |   `[467cd2c61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/467cd2c61) Merge branch 'parcoursSuite' into 'master' (2021-10-04 15:28) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[97f0f1acf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97f0f1acf) modif de forme (2021-10-04 17:26) <Daniel Caillibaud>  
`| * | | `[b92a27513](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b92a27513) init de la figure une seule fois au début, pas à chaque répétition (2021-10-04 17:19) <Daniel Caillibaud>  
`| * | | `[ba89f6688](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba89f6688) peut-être était un pb d'asynchrone (2021-10-02 21:27) <Rémi Deniaud>  
`| * | | `[905601097](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/905601097) petites modifs dans SuiteTermeGeneral (2021-10-02 14:48) <Rémi Deniaud>  
`| * | | `[32f65a8ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32f65a8ac) nettoyage natureSuite (2021-10-02 14:22) <Rémi Deniaud>  
`|/ / /  `  
`| * / `[25df8da44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25df8da44) eslint et mais finenonce dans enoncemain ( c'est ptet la cause du bugsnag si j'ai de la chance ) (2021-10-04 13:09) <Tommy>  
`|/ /  `  
`* | `[a9477624b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9477624b) outilsexternes ignorés par eslint (sinon ça fait fumer l'IDE) (2021-09-30 17:13) <Daniel Caillibaud>  
`* |   `[35979cd36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35979cd36) Merge branch 'rererefixBienEcrirerz' into 'master' (2021-09-30 14:41) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[e76bde8ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e76bde8ae) juste créa getdonnees, je voulais trouver pouquoi donneessection.nbitems etait null mais pas vu (2021-09-30 15:09) <Tommy>  
`| * | `[96cd38919](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96cd38919) juste créa getdonnees, je voulais trouver pouquoi donneessection.nbitems etait null mais pas vu (2021-09-30 15:08) <Tommy>  
`* | |   `[dd6d06b00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd6d06b00) Merge branch 'ptifixEcrireChiffres' into 'master' (2021-09-30 14:40) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[30e64751c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30e64751c) petit bug , le mot unité pouvait apparaitre au singulier dans la partie décimale (2021-09-30 14:36) <Tommy>  
`| * | | `[6928bfe36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6928bfe36) creation initsection , enoncemain et getdonnees (2021-09-30 14:30) <Tommy>  
`| |/ /  `  
`* | |   `[f7d064b28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7d064b28) Merge branch 'tabloPropor' into 'master' (2021-09-30 14:38) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[4603c338b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4603c338b) ajout de "height" dans un tableau pour voir la barre de fraction inversion de barre ou corrige et disabled pour que ca barre ou corrige avant le disabled virer les 'in' devant zinput, c'était un vieux truc (2021-09-29 22:53) <Tommy>  
`| * | | `[c73f93fcd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c73f93fcd) changer dans tableOptions pour retrouver les tableaux d'avant (2021-09-29 22:25) <Tommy>  
`| |/ /  `  
`* | |   `[782d809b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/782d809b9) Merge branch 'fixBugSnagProgConst' into 'master' (2021-09-30 14:32) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[744681c18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/744681c18) simplification du code qui nettoie gpos des elt pt (2021-09-30 14:31) <Daniel Caillibaud>  
`| * | | `[1ed3797c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ed3797c3) changer un truc au dessus des notify, ca marchera ptet ( pour virer les objet.t === 'pt' (2021-09-29 22:05) <Tommy>  
`| * | | `[98866c90d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98866c90d) changer un truc au dessus des notify, ca marchera ptet ( pour virer les objet.t === 'pt' (2021-09-29 22:02) <Tommy>  
`| |/ /  `  
`* | |   `[bdc7faac3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bdc7faac3) Merge branch 'notifiyPourprogconst' into 'master' (2021-09-30 14:16) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[10f77ed70](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10f77ed70) les notify sont revenus, j'en ai ajouté un (2021-09-29 19:10) <Tommy>  
`* | |   `[4c2fba21f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c2fba21f) Merge branch 'onvoyaitpasbarrefraction' into 'master' (2021-09-29 17:02) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[8adfeac53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8adfeac53) barre fraction plus visible (2021-09-29 17:00) <Tommy>  
`|/ /  `  
`* |   `[442271b23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/442271b23) Merge branch 'fixbugsnaposeop' into 'master' (2021-09-29 14:39) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[a70b3f8ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a70b3f8ff) avant c'était un bug bugsnag, là c'est eslint (2021-09-29 14:39) <barroy tommy>  
`|/ /  `  
`* | `[10cffc892](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10cffc892) retag 1.0.17 (2021-09-29 14:26) <Daniel Caillibaud>  
`* | `[0fb07fe60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0fb07fe60) Merge branch 'ameliorationMqVirtualKeyboard' (2021-09-29 14:26) <Daniel Caillibaud>  
`|\| `  
`| * `[6e6f50ecc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e6f50ecc) on commente addConsoleOnScreen (2021-09-29 14:11) <Daniel Caillibaud>  
`| * `[aa5314d9c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa5314d9c) aj de setTimeout dans j3pFocus pour contrôler que c'est OK, ça évite d'avoir à le refaire une 2e fois car c'est visiblement pas complètement synchrone (c'est pas systématique, ni avant ni maintenant) (2021-09-29 14:10) <Daniel Caillibaud>  
`| * `[1d67a5a20](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d67a5a20) amélioration addConsoleOnScreen (avec aussi console.error et console.warn) (2021-09-29 13:36) <Daniel Caillibaud>  
`| * `[d47b2e474](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d47b2e474) Déplacement de la création du div correction dans initMtg (pour éviter de mélanger le code avant/après/pendant initMtg qui est async, ça change rien c'est juste pour la lisibilité et la compréhension) (2021-09-29 13:11) <Daniel Caillibaud>  
`| * `[6dd1a5fb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6dd1a5fb5) Aj de la fct addConsoleOnScreen (pour le debug sur tablettes) (2021-09-29 12:02) <Daniel Caillibaud>  
`| * `[6e18ae2c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e18ae2c0) màj doc pour lancer devServer sur l'ip locale (et pas localhost) (2021-09-29 11:58) <Daniel Caillibaud>  
`| * `[06aeeda50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06aeeda50) amélioration de l'url devServer dans le cas d'une BASE_URL imposée via la ligne de commande (2021-09-29 11:05) <Daniel Caillibaud>  
`| * `[87113b00f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87113b00f) le décalage auto latéral ne doit pas annuler les décalages auto verticaux éventuels (on annule seulement sur un décalage manuel) (2021-09-29 10:09) <Daniel Caillibaud>  
`| * `[8744544f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8744544f5) aj commentaires sur le changement d'orientation (2021-09-28 23:43) <Daniel Caillibaud>  
`| * `[b226f71d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b226f71d6) On repositionne le clavier virtuel par rapport à un conteneur dont on essaie de ne pas déborder (aj de l'option boundingContainer) (2021-09-28 22:47) <Daniel Caillibaud>  
`| * `[1f10817fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f10817fb) On ouvre le clavier au touch même s'il avait déjà le focus (2021-09-28 22:43) <Daniel Caillibaud>  
`| * `[0d5f934e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d5f934e7) aj j3pFocus sur le 1er input (2021-09-28 22:41) <Daniel Caillibaud>  
`* | `[4931ba5fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4931ba5fe) console.log virés (2021-09-29 13:03) <Daniel Caillibaud>  
`* |   `[ce23dfe13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce23dfe13) Merge branch 'fixProportionnalite02' into 'master' (2021-09-29 11:02) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[07c1ee55f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07c1ee55f) simplification du choix de la structure (2021-09-29 12:57) <Daniel Caillibaud>  
`| * | `[7f3414828](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f3414828) simplification du nettoyage de la scène (on laisse le modèle vider MG et MD ou MG seulement suivant la structure) (2021-09-29 12:56) <Daniel Caillibaud>  
`| * | `[471aacd30](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/471aacd30) fix affectation de propriété sur le parcours (2021-09-29 12:54) <Daniel Caillibaud>  
`| * | `[3a6052dd9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a6052dd9) amélioration de l'url devServer dans le cas d'une BASE_URL imposée via la ligne de commande (2021-09-29 10:17) <Daniel Caillibaud>  
`| |/  `  
`* |   `[7759e8196](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7759e8196) Merge branch 'parcoursSuite' into 'master' (2021-09-29 09:45) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[7038730a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7038730a7) en espérant que cela suffise... (2021-09-28 18:50) <Rémi Deniaud>  
`| * | `[a55414225](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a55414225) palette cours caché au clic sur SectionSuivante (2021-09-28 18:31) <Rémi Deniaud>  
`| * | `[5d5a0ac03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d5a0ac03) correction nomSection (2021-09-28 18:15) <Rémi Deniaud>  
`* | |   `[c5912736f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5912736f) Merge branch 'etudeFcts' into 'master' (2021-09-29 09:39) <Daniel Caillibaud>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[aa55f600f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa55f600f) nettoyage code (2021-09-28 19:13) <Rémi Deniaud>  
`| * | `[1b4c8b0f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b4c8b0f8) nettoyage EtudeFonction_facteursderivee (2021-09-28 19:13) <Rémi Deniaud>  
`|/ /  `  
`* | `[9bab143a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9bab143a4) aj du clavier virtuel (2021-09-28 18:29) <Daniel Caillibaud>  
`* | `[b5ee846de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5ee846de) fix focus qui se mettait quand même au 2e clic sur PC et au double clic sur tablette + bord bleu systématique sur l'input courant (2021-09-28 18:16) <Daniel Caillibaud>  
`* |   `[3b522f186](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b522f186) Merge branch 'fixNommeBugsnag' into 'master' (2021-09-28 15:03) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[96a9bfe73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96a9bfe73) eslint (2021-09-28 12:54) <Tommy>  
`| * | `[772d4841a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/772d4841a) un bugsnag de moi (2021-09-28 12:33) <Tommy>  
`| |/  `  
`* |   `[6d7d75c52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d7d75c52) Merge branch 'fixVirtualKeyboard' into 'master' (2021-09-28 15:02) <Daniel Caillibaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[3835588fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3835588fb) Renommage VirtualKeyboard => MqVirtualKeyboard (2021-09-28 17:01) <Daniel Caillibaud>  
`| * `[ba2dd299f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba2dd299f) on vire le param de show qui servait à rien (2021-09-28 15:38) <Daniel Caillibaud>  
`| * `[a5cd9148f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5cd9148f) on ajoute un param au show pour ne pas ajouter le listener keyup (2021-09-28 15:24) <Daniel Caillibaud>  
`| * `[7062c90f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7062c90f9) console.log virés (2021-09-28 14:51) <Daniel Caillibaud>  
`| * `[9fcd727a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9fcd727a2) enfin un comportement qui semble fonctionner comme on voulait (reste plein de console.log) (2021-09-28 14:50) <Daniel Caillibaud>  
`| * `[d8d43dc69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8d43dc69) fix caractères en double (2021-09-28 14:49) <Daniel Caillibaud>  
`| * `[e740fcc8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e740fcc8e) on déplace les 3 listeners qui affectent isFocusedByTouch dans le constructeur de VirtualKeyboard (2021-09-28 14:10) <Daniel Caillibaud>  
`| * `[ac6a3bcad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac6a3bcad) la console onScreen se vide toute seule quand y'a trop de monde dedans (2021-09-28 14:10) <Daniel Caillibaud>  
`| * `[e8f007845](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8f007845) fix des pb de caractères en double au clavier physique (2021-09-28 13:26) <Daniel Caillibaud>  
`| * `[52a890183](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52a890183) modif commentaires (2021-09-28 13:03) <Daniel Caillibaud>  
`| * `[eb7782283](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb7782283) WIP on remet le setTimeout pour réactiver le curseur, mais il reste un pb car on arrive parfois à mettre le focus sur l'input avec du touch, et alors les frappes clavier sont doublées (version de test avec console.log pour tracer sur tablette) (2021-09-28 13:00) <Daniel Caillibaud>  
`|/  `  
`* `[ef455a6e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef455a6e4) fix bouton backspace + factorisation de l'appel de fakeKeyup (2021-09-28 12:03) <Daniel Caillibaud>  
`* `[ac247d3b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac247d3b1) retag 1.0.16 (2021-09-28 11:09) <Daniel Caillibaud>  
`*   `[39368919e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39368919e) Merge branch 'refixBugsnagApp' into 'master' (2021-09-28 09:08) <Daniel Caillibaud>  
`|\  `  
`| * `[d82a6b7ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d82a6b7ed) un bug avec ressources mal faites (2021-09-28 07:50) <Tommy>  
`* |   `[079bfc79b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/079bfc79b) Merge branch 'modVirtualKeyboard' into 'master' (2021-09-28 09:08) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[83b2a3321](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83b2a3321) aj de commentaires sur le fonctionnement, un setTimeout qui sert plus viré (2021-09-28 11:05) <Daniel Caillibaud>  
`| * | `[d49cc448e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d49cc448e) fix focusin listener (fct fléchée pour conserver notre this) (2021-09-28 10:20) <Daniel Caillibaud>  
`| * | `[1ed4a0773](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ed4a0773) Le clavier physique fonctionne quand même avec un clavier virtuel ouvert (2021-09-28 10:12) <Daniel Caillibaud>  
`| * | `[a04e615a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a04e615a4) Aj d'un listener keyup pour récupérer les evts du clavier physique et les répercuter sur l'input mathquill qui a perdu le focus à cause du clavier virtuel ouvert (2021-09-27 21:06) <Daniel Caillibaud>  
`| * | `[762c85537](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/762c85537) meilleur affichage console to screen (2021-09-27 21:01) <Daniel Caillibaud>  
`| * | `[61f868862](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61f868862) On impose la police roboto pour nos boutons (histoire de pas être dépendant des polices installées) (2021-09-27 20:35) <Daniel Caillibaud>  
`| * | `[e50ab324b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e50ab324b) On ajoute la police Roboto à nos dépendances (2021-09-27 20:34) <Daniel Caillibaud>  
`| * | `[48c755f57](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48c755f57) amélioration sortie console à l'écran (2021-09-27 20:33) <Daniel Caillibaud>  
`* | |   `[102d2dd43](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/102d2dd43) Merge branch 'refixForm02' into 'master' (2021-09-28 06:02) <barroy tommy>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[61a1f0545](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61a1f0545) un bugsnag de moi (2021-09-28 08:00) <Tommy>  
`|/ /  `  
`* / `[b4c2159f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4c2159f8) try/catch inutile viré (2021-09-27 21:06) <Daniel Caillibaud>  
`|/  `  
`* `[edc5e8353](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edc5e8353) on passe toujours false en 2e param d'une commande mathquill (2021-09-27 18:49) <Daniel Caillibaud>  
`* `[78155d411](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78155d411) fix du clic sur les boutons avec images (2021-09-27 17:40) <Daniel Caillibaud>  
`* `[1edd31fd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1edd31fd0) amélioration de la sortie console à l'écran (2021-09-27 17:39) <Daniel Caillibaud>  
`* `[e0ed1125a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e0ed1125a) restait un only mis pendant le dev d'un test unitaire et oublié ensuite (2021-09-27 16:33) <Daniel Caillibaud>  
`* `[554f88040](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/554f88040) retag 1.0.15 (2021-09-27 16:31) <Daniel Caillibaud>  
`*   `[05ee78d45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05ee78d45) Merge branch 'fixTableur01' into 'master' (2021-09-27 14:29) <Daniel Caillibaud>  
`|\  `  
`| * `[dacf5bba4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dacf5bba4) eslint et couleurs (2021-09-27 16:19) <Tommy>  
`| * `[39467f31d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39467f31d) on dirait que c'est bon (2021-09-27 16:05) <Tommy>  
`* |   `[7aad5a318](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7aad5a318) Merge branch 'bugbugNomme2' into 'master' (2021-09-27 14:28) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[8949294be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8949294be) plus d'info dans notify, je trouve pas, et javais oublié de débloqué une fig (2021-09-27 15:41) <Tommy>  
`| * | `[be25d4e13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be25d4e13) plus d'info dans notify, je trouve pas (2021-09-27 13:04) <Tommy>  
`* | |   `[ae65c35ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae65c35ad) Merge branch 'AmeliorationClavierVirtuel' into 'master' (2021-09-27 14:27) <Daniel Caillibaud>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[a32e6268a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a32e6268a) factorisation de code avec la méthode statique addInto (2021-09-27 16:25) <Daniel Caillibaud>  
`| * | `[6e65af99b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e65af99b) aj de la sortie console sur l'écran pour debug iPad (2021-09-27 16:10) <Daniel Caillibaud>  
`| * | `[97baed12e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97baed12e) fix drag au touch sur iPad (2021-09-27 16:10) <Daniel Caillibaud>  
`* | | `[ecc6fb7e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecc6fb7e6) Merge branch 'AmeliorationClavierVirtuel' into 'master' (2021-09-27 13:02) <Yves Biton>  
`|\| | `  
`| * | `[6f134c369](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f134c369) Version où on peut déplacer le clavier virtuel sur tablette. (2021-09-27 14:08) <Yves Biton>  
`| |/  `  
`* |   `[87461343f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87461343f) Merge branch 'rereEslintBienEcrire' into 'master' (2021-09-27 12:18) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[c6990ab0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6990ab0e) eslint et initsection et enoncemain (2021-09-26 23:45) <Tommy>  
`* | |   `[16529badb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16529badb) Merge branch 'eslintAppartenance' into 'master' (2021-09-27 12:17) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[e96185187](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e96185187) eslint, puis je me suis emballé, ajout d'un param schema et couleurs fond (2021-09-26 23:31) <Tommy>  
`| |/ /  `  
`* | |   `[cf4173e3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf4173e3a) Merge branch 'couleur,_et_deux_ptits_fix_bizarre_mathquil' into 'master' (2021-09-27 12:16) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[b48b8716b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b48b8716b) eslint et ptits bug, dsl j'ai pas commit en route, mais y'a plusiseurs petits changements, au début je testais pas puis j'ai vu un truc bizarre, je l'ai corrigé , mais ca fait bcp de modifs (2021-09-26 18:31) <Tommy>  
`| |/ /  `  
`* | |   `[679fa1a68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/679fa1a68) Merge branch 'assLaboRelatifsOppo' into 'master' (2021-09-27 12:16) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[183b9aa3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/183b9aa3a) eslint (2021-09-27 10:49) <Tommy>  
`| * | | `[48f00e45d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48f00e45d) juste co bug correction (2021-09-27 10:39) <Tommy>  
`* | | |   `[b2e06bc52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2e06bc52) Merge branch 'parcoursSuite' into 'master' (2021-09-27 12:15) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[48eb603ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48eb603ba) simplification regexp (2021-09-27 14:14) <Daniel Caillibaud>  
`| * | | `[a5a7b5baf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5a7b5baf) usage de la regexp testIntervalleDecimaux (de lib/utils/regexp) (2021-09-27 14:14) <Daniel Caillibaud>  
`| * | | `[3053483b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3053483b4) usage des regexp de lib/utils/regexp dans j3pGetBornesIntervalle (2021-09-27 14:09) <Daniel Caillibaud>  
`| * | | `[f28018b50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f28018b50) Aj de regex sur les intervalles, avec leurs tests unitaires (2021-09-27 13:06) <Daniel Caillibaud>  
`| * | | `[17e297733](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17e297733) nettoyage dans tauxEvolutionSuite (2021-09-25 22:09) <Rémi Deniaud>  
`| * | | `[7de48249f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7de48249f) modif donnees_parcours dans sommeTermesSuite (2021-09-25 21:55) <Rémi Deniaud>  
`| * | | `[dc109081c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc109081c) nettoyage de limiteArithGeo (2021-09-25 21:53) <Rémi Deniaud>  
`| * | | `[bfd983e29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bfd983e29) nettoyage de limiteSuite (2021-09-25 21:32) <Rémi Deniaud>  
`| * | | `[7758edc61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7758edc61) correction pour donnees_parcours (2021-09-25 21:17) <Rémi Deniaud>  
`| * | | `[42e59fef8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42e59fef8) nettoyage de nouveau (2021-09-25 18:08) <Rémi Deniaud>  
`| * | | `[0f1d0c478](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f1d0c478) modif des RegExp (2021-09-25 18:03) <Rémi Deniaud>  
`| * | | `[88afdf9cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88afdf9cf) nettoyage (2021-09-25 18:02) <Rémi Deniaud>  
`| * | | `[f455b7b32](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f455b7b32) nettoyage sommeTermesSuites (2021-09-25 17:44) <Rémi Deniaud>  
`| | |/  `  
`| |/|   `  
`* | |   `[d475303ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d475303ab) Merge branch 'testClavierVirtuel' into 'master' (2021-09-27 08:49) <Yves Biton>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[ce0a5f9f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce0a5f9f9) throw Error si on veut affecter un 2e clavier virtuel au même inputMq (2021-09-25 15:06) <Daniel Caillibaud>  
`| * | `[147312655](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/147312655) J'avais encore des problèmes où, en touchant l'éditeur Mathquill, il recevait le clavier. Ca semble résolu avec cette version. Il reste à tester sur de variens tablettes. (2021-09-25 13:06) <Yves Biton>  
`| * | `[9024a5524](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9024a5524) Version qui analyse si un événement touch a été envoyé à un éditeur MathQuill et, si oui, refuse de lui donner le vrai focus mais fait apparaître le clavier virtuel associé s'il n'est pas déjà présent. Marche bien avec mon écran tactile. A vérifier avec tablettes en dev. (2021-09-25 10:14) <Yves Biton>  
`| * |   `[23c04e722](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23c04e722) Merge branch 'master' into testClavierVirtuel (2021-09-24 18:49) <Daniel Caillibaud>  
`| |\ \  `  
`| * | | `[fbd626cac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fbd626cac) fix commande mathquill appelée avec l'event en 2e argument (2021-09-24 18:49) <Daniel Caillibaud>  
`| * | | `[5b677e300](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b677e300) fix alignement vertical des fcts mathquill (sin, cos & co) (2021-09-24 14:38) <Daniel Caillibaud>  
`| * | | `[b4a89f728](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4a89f728) Modif pour que les fonctions associées à des boutobs mathquill aient bien le bouton correspondant dans le clavier virtuel (2021-09-24 14:32) <Yves Biton>  
`| * | | `[c14b8a1db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c14b8a1db) Ajout d'un deuxième paramètre aux mqAjoute de façon à ce que les boutons actionnés dans le clavier virtuel ne donnent pas le focus clavier. (2021-09-24 14:11) <Yves Biton>  
`* | | |   `[6b61c173a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b61c173a) Merge branch 'nettoieInequations' into 'master' (2021-09-27 07:35) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[ddb12ff2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ddb12ff2b) nettoyage signeInegalites (2021-09-25 17:10) <Rémi Deniaud>  
`| * | | `[759579a05](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/759579a05) nettoyage intervallesUnionInter (2021-09-25 16:54) <Rémi Deniaud>  
`| * | | `[d6aa734f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d6aa734f2) nettoyage inequation2 (2021-09-25 16:34) <Rémi Deniaud>  
`| * | | `[dfc772730](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfc772730) nettoyage inequation1 (2021-09-25 16:09) <Rémi Deniaud>  
`| * | | `[98d6d173b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98d6d173b) nettoyage inequationdomaine (2021-09-25 15:25) <Rémi Deniaud>  
`|/ / /  `  
`* | | `[168753296](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/168753296) retag 1.0.14 (2021-09-25 14:55) <Daniel Caillibaud>  
`* | |   `[cd8edd0a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd8edd0a4) Merge branch 'fixUnionInter' into 'master' (2021-09-25 12:52) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[52865eb98](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52865eb98) barres de la bonne couleur + petite modif pour évier un bug (2021-09-25 12:00) <Rémi Deniaud>  
`| * | | `[70097ee58](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70097ee58) nettoyage code (2021-09-25 11:45) <Rémi Deniaud>  
`* | | |   `[d43ae5591](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d43ae5591) Merge branch 'secondDegre' into 'master' (2021-09-25 12:51) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[d45ce7511](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d45ce7511) correction effectuée (2021-09-25 11:03) <Rémi Deniaud>  
`| * | | | `[329fed4f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/329fed4f5) nettoyage again (2021-09-25 11:00) <Rémi Deniaud>  
`| * | | | `[f0747d5ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0747d5ac) gros nettoyage (2021-09-25 10:52) <Rémi Deniaud>  
`| |/ / /  `  
`* | | |   `[689066228](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/689066228) Merge branch 'fixConstBugsnag' into 'master' (2021-09-25 12:50) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[e582ae954](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e582ae954) Il ne faut utiliser la propriété j3p dans les datas supplémentaires envoyées à bugsnag (l'onglet j3p existe déjà) (2021-09-25 14:48) <Daniel Caillibaud>  
`| * | | |   `[2c21da049](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c21da049) Merge branch 'master' into fixConstBugsnag (2021-09-25 14:45) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`| | | |_|/  `  
`| | |/| |   `  
`| * | | | `[fc4ac5e4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc4ac5e4c) un marqueur pour bugsnag (2021-09-24 22:46) <Tommy>  
`* | | | |   `[4321e500c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4321e500c) Merge branch 'rererefixNomme02buggggsnag' into 'master' (2021-09-25 12:50) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[7d8d7c715](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d8d7c715) notify pour bugsnag (2021-09-25 13:50) <Tommy>  
`| * | | | | `[aee029fb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aee029fb8) notify pour bugsnag (2021-09-25 10:56) <Tommy>  
`| | |_|/ /  `  
`| |/| | |   `  
`* | | | |   `[965b97bbb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/965b97bbb) Merge branch 'rerefixEcrichifBugsnag' into 'master' (2021-09-25 12:49) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[b76a45612](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b76a45612) bug bugsnag (2021-09-25 10:06) <Tommy>  
`| |/ / / /  `  
`* | | | |   `[66cb99247](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66cb99247) Merge branch 'eslintConversion' into 'master' (2021-09-25 12:43) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`| * | | | `[a1d4f6fcf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1d4f6fcf) ajout couleur (2021-09-24 22:30) <Tommy>  
`| * | | | `[a74345834](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a74345834) ajout couleur (2021-09-24 22:04) <Tommy>  
`| * | | | `[3c22aa009](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c22aa009) eslint + vire tactil ( je sais pas pourquoi je l'avais laissé, y'a rien de tactil dedans ) (2021-09-24 21:58) <Tommy>  
`* | | | |   `[df86a2dcb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df86a2dcb) Merge branch 'fixInegaliteIntervalle' into 'master' (2021-09-25 07:59) <Rémi Deniaud>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`| * | | | `[d0ca91217](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0ca91217) espace après < (2021-09-25 09:44) <Rémi Deniaud>  
`| * | | | `[db18466a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db18466a6) nettoyage (2021-09-25 09:43) <Rémi Deniaud>  
`| * | | | `[d388ccd98](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d388ccd98) j3pRandom remplacé par j3pGetRandomInt (2021-09-24 19:14) <Daniel Caillibaud>  
`| * | | | `[706d5219b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/706d5219b) nettoyage et fixes eslint (2021-09-24 19:06) <Daniel Caillibaud>  
`| | |/ /  `  
`| |/| |   `  
`* | | |   `[4822d151e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4822d151e) Merge branch 'refixpropor2' into 'master' (2021-09-24 19:15) <barroy tommy>  
`|\ \ \ \  `  
`| * | | | `[43166c9c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43166c9c7) un bug debile (2021-09-24 21:13) <Tommy>  
`| |/ / /  `  
`* | | |   `[45850a2cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45850a2cd) Merge branch 'externalisationBoutonsTrianglesSemblables' into 'master' (2021-09-24 17:06) <barroy tommy>  
`|\ \ \ \  `  
`| * \ \ \   `[97bfdbd0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97bfdbd0f) Merge branch 'master' into externalisationBoutonsTrianglesSemblables (2021-09-22 11:46) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`| * | | | | `[803e1f470](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/803e1f470)  echanger disable et barre ou couleur (2021-09-16 22:07) <Tommy>  
`| * | | | | `[7469d1778](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7469d1778) capitalize externalisé dans lib/utils/string (2021-09-15 23:55) <Daniel Caillibaud>  
`| * | | | | `[d4139d80b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4139d80b) on déplace le helper dessin dans un dossier, avec ses images et sa css (2021-09-15 23:44) <Daniel Caillibaud>  
`| * | | | | `[e014ab0a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e014ab0a8) imports inutiles virés (2021-09-15 23:34) <Daniel Caillibaud>  
`| * | | | | `[f2aa0d8c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2aa0d8c7) Mutualisation de code, simplification, externalisation du css (2021-09-15 23:33) <Daniel Caillibaud>  
`* | | | | |   `[858f8375e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/858f8375e) Merge branch 'suite' into 'master' (2021-09-24 14:12) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| |_|_|/ / /  `  
`|/| | | | |   `  
`| * | | | | `[ea1d6e0b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea1d6e0b5) petite modif (2021-09-24 14:38) <Rémi Deniaud>  
`| * | | | | `[cc7c970d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc7c970d5) nettoyage ArithmeticoGeo (2021-09-24 14:27) <Rémi Deniaud>  
`|/ / / / /  `  
`* | | | |   `[bc98f0807](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc98f0807) Merge branch 'fixAffectationsJ3p' (2021-09-24 13:28) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[b41aa0ae8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b41aa0ae8) fix affectations j3p + erreurs eslint (651) (2021-09-24 13:27) <Daniel Caillibaud>  
`| * | | | | `[4eae3ea12](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4eae3ea12) fix affectations j3p + erreurs eslint (630) (2021-09-24 13:27) <Daniel Caillibaud>  
`| * | | | | `[d54ee6350](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d54ee6350) fix affectations j3p + erreurs eslint (611) (2021-09-24 13:27) <Daniel Caillibaud>  
`| * | | | | `[de54159de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de54159de) fix indice foireux (2021-09-24 13:27) <Daniel Caillibaud>  
`| * | | | | `[17ba5475e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17ba5475e) fix affectations j3p + erreurs eslint (603) (2021-09-24 13:27) <Daniel Caillibaud>  
`| * | | | | `[f41376ee2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f41376ee2) fix affectations j3p + erreurs eslint (600) (2021-09-24 13:27) <Daniel Caillibaud>  
`| * | | | | `[7ea9e9d65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ea9e9d65) fix affectations j3p + erreurs eslint (2021-09-24 13:27) <Daniel Caillibaud>  
`|/ / / / /  `  
`* | | | | `[e1bbe2e36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1bbe2e36) fix jsdoc foireux (2021-09-24 12:20) <Daniel Caillibaud>  
`* | | | |   `[f3afe739f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3afe739f) Merge branch 'testClavierVirtuel' (2021-09-24 12:15) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| | |_|_|/  `  
`| |/| | |   `  
`| * | | | `[9a64e0e4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a64e0e4c) faut pas effacer de caractère si on en ajoute pas, mais envoyer une chaîne vide marche pas, faut bien mettre une espace et la virer (2021-09-24 12:05) <Daniel Caillibaud>  
`| * | | | `[dbe8adcef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dbe8adcef) modif css pour mettre les boutons un peu plus gros et conserver les images de j3pPaletteMathquill dans leur taille d'origine (2021-09-24 12:01) <Daniel Caillibaud>  
`| * | | | `[4a6c41474](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a6c41474) On n'appelle pas le focus MathQuill à la fin d'un drag car il donne le focus clavier (2021-09-24 12:00) <Yves Biton>  
`| * | | | `[c2f11997d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2f11997d) Dans show on ne donne plus le focus mathquill mais on simule un espace tapé et un backspace poir ne pas donner un vrai focus clavier. (2021-09-24 11:55) <Yves Biton>  
`| * | | | `[75603eb9d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75603eb9d) fix du curseur qui restait clignotant dans 1 input au changement de focus. (2021-09-24 11:36) <Daniel Caillibaud>  
`| * | | | `[0d28ccadc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d28ccadc) On déplie le 1er clavier au chargement (2021-09-24 11:08) <Daniel Caillibaud>  
`| * | | | `[382eba22b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/382eba22b) On déplie le 1er clavier au chargement (2021-09-24 11:03) <Daniel Caillibaud>  
`| * | | | `[dd6915615](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd6915615) changement d'ordre des touches char (2021-09-24 10:52) <Daniel Caillibaud>  
`| * | | |   `[c51b57d28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c51b57d28) Merge branch 'master' into testClavierVirtuel (2021-09-24 10:47) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`* | | | | `[bcb6d7834](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bcb6d7834) hotfix operations.scss (2021-09-24 10:46) <Daniel Caillibaud>  
`| * | | | `[9443977f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9443977f7) fix pb de focus (2021-09-24 10:41) <Daniel Caillibaud>  
`| * | | | `[316509382](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/316509382) Aj d'une section pour tester le clavier virtuel (2021-09-24 10:31) <Daniel Caillibaud>  
`|/ / / /  `  
`* | | |   `[dc8838b9f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc8838b9f) Merge branch 'fixRemplace' into 'master' (2021-09-24 08:11) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[143a09ec2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/143a09ec2) externalisation css (2021-09-24 10:08) <Daniel Caillibaud>  
`| * | | | `[e0a400a50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e0a400a50) mis stor = me.storage et déplacé déclarations functions qq eslint modif  zonestylle3 pour qu'elle efface pas la couleur au disabled ( par ontre je l'ai pas eslint, y'a des j3p. mais je peux pas mettre me. ) (2021-09-23 16:16) <Tommy>  
`* | | | |   `[9db940cc0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9db940cc0) Merge branch 'bugsnagNomme03' into 'master' (2021-09-24 07:59) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[d026fc878](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d026fc878) bugsnag (2021-09-24 08:44) <Tommy>  
`* | | | | |   `[be1f13379](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be1f13379) Merge branch 'eslintPropor2' into 'master' (2021-09-24 07:58) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[5cd54fb31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5cd54fb31) modif mineure (1 ligne inutile virée) (2021-09-24 09:58) <Daniel Caillibaud>  
`| * | | | | | `[4c80e5e8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c80e5e8c) eslint et couleurs Ca fait bcp de changements, ca a l'air de marcher (2021-09-24 08:36) <Tommy>  
`| |/ / / / /  `  
`* | | | | |   `[06f7bc048](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06f7bc048) Merge branch 'eslintNomme02' into 'master' (2021-09-24 07:55) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[750a5d051](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/750a5d051) fix erreurs de typo, simplification if/else avec return, fix erreur eslint (reste 118 warnings) (2021-09-24 09:55) <Daniel Caillibaud>  
`| * | | | | | `[942867ad7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/942867ad7) eslint et stor et initsection avec enoncemain pour ptet bugsnag ( au cas ou y aurait un truc asynchrone, j'ai mis fix dans bugsnag mais suis pas sur ) (2021-09-23 22:43) <Tommy>  
`| |/ / / / /  `  
`* | | | | |   `[54880a66a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54880a66a) Merge branch 'fctReference' into 'master' (2021-09-24 07:39) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[9190a9135](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9190a9135) correction appel j3pShuffleMulti (2021-09-23 22:28) <Rémi Deniaud>  
`| |/ / / / /  `  
`* / / / / / `[6099d0db8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6099d0db8) hotfix on remet j3pRestriction (2021-09-24 09:17) <Daniel Caillibaud>  
`|/ / / / /  `  
`* | | | | `[a58b6e530](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a58b6e530) aj de options.commandes à mqRestriction (oublié) (2021-09-23 20:39) <Daniel Caillibaud>  
`* | | | |   `[6b297ecad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b297ecad) Merge branch 'secondDegre' into 'master' (2021-09-23 18:25) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[2aa0e1449](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2aa0e1449) petite erreur corrigée (2021-09-23 19:54) <Rémi Deniaud>  
`| * | | | | `[8bb7e62e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8bb7e62e3) petit nettoyage (2021-09-23 19:40) <Rémi Deniaud>  
`* | | | | |   `[3d8b1f1fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d8b1f1fe) Merge branch 'suite' into 'master' (2021-09-23 18:24) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[747bad484](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/747bad484) nettoyage code (2021-09-23 19:24) <Rémi Deniaud>  
`| * | | | | | `[54c1ad408](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54c1ad408) nettoyage code (2021-09-23 19:22) <Rémi Deniaud>  
`* | | | | | | `[424ca0633](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/424ca0633) retag 1.0.13 (2021-09-23 20:22) <Daniel Caillibaud>  
`* | | | | | | `[112841131](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/112841131) retag 1.0.12 (2021-09-23 19:36) <Daniel Caillibaud>  
`* | | | | | |   `[28b41c968](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28b41c968) Merge branch 'Amélioration_Editeurs_MathQuill' into 'master' (2021-09-23 17:35) <Daniel Caillibaud>  
`|\ \ \ \ \ \ \  `  
`| |_|/ / / / /  `  
`|/| | | | | |   `  
`| * | | | | |   `[c1ba08f26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1ba08f26) Merge branch 'modVirtualKeyboard' into 'Amélioration_Editeurs_MathQuill' (2021-09-23 17:29) <Daniel Caillibaud>  
`| |\ \ \ \ \ \  `  
`| | * | | | | | `[bc4cacaab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc4cacaab) Suprresion de deux lignes du constructeur de ButtonArrow avec lesquelles les flèches de dépalcement du curseur n'étaient plus centrées dans leur bouton. (2021-09-23 19:21) <Yves Biton>  
`| | * | | | | | `[6af4eb686](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6af4eb686) Suprresion de deux lignes du constructeur de ButtonArrow avec lesquelles les flèches de dépalcement du curseur n'étaient plus centrées dans leur bouton. (2021-09-23 17:08) <Yves Biton>  
`| | * | | | | | `[e98546302](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e98546302) le clavier suit l'input s'il change de taille, + améliorations cosmétiques (2021-09-23 14:15) <Daniel Caillibaud>  
`| | * | | | | | `[185756b7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/185756b7d) Ajout d'une option commandes (et d'une méthode addCommand) pour ajouter des commandes mathquill au clavier virtuel (2021-09-23 13:20) <Daniel Caillibaud>  
`| | * | | | | | `[ae874b548](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae874b548) on noircit un peu la flèche pour qu'elle sorte lorsque le png est réduit (2021-09-23 13:19) <Daniel Caillibaud>  
`| | * | | | | | `[4a0e3aff4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a0e3aff4) mise à 32×32 de tous les boutons mathquill (2021-09-23 13:03) <Daniel Caillibaud>  
`| | * | | | | | `[25f8abbd4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25f8abbd4) fix changement de * en × et / en ÷ (faut le faire sur l'affichage du bouton mais pas le caractère envoyé dans l'input) (2021-09-23 12:47) <Daniel Caillibaud>  
`| | * | | | | | `[51e3de980](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51e3de980) Suppression du listener touchstart qui agissait en doublon avec le onclick dans MtgButton. Suppression de l'appel à inputMq.addEventListener dans Button inutile. (2021-09-23 12:39) <Yves Biton>  
`| | * | | | | | `[4fd9f9776](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fd9f9776) fix changement d'input (2021-09-23 11:43) <Daniel Caillibaud>  
`| | * | | | | | `[6d5c02503](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d5c02503) récup du fix de j3pShuffleMulti (déjà dans master) (2021-09-23 11:22) <Daniel Caillibaud>  
`| | * | | | | | `[6beb91497](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6beb91497) Mathquill exporte sa liste de commandes (2021-09-23 11:18) <Daniel Caillibaud>  
`| | * | | | | | `[a70442768](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a70442768) Les fonctions mathquill prennent désormais aussi un élément (et plus seulement un id) (2021-09-23 11:13) <Daniel Caillibaud>  
`| | * | | | | | `[5e583c7dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e583c7dd) améliorations cosmétiques et * devient × et / devient ÷ (2021-09-23 10:55) <Daniel Caillibaud>  
`| | * | | | | | `[0215f1107](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0215f1107) le clavier devient déplaçable (2021-09-23 10:37) <Daniel Caillibaud>  
`| | * | | | | | `[20aa7e9aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20aa7e9aa) layout du clavier en flex (plus de tableaux), listes séparées des caractères pour retour à la ligne entre les boutons de chaque catégorie (2021-09-23 10:14) <Daniel Caillibaud>  
`| | * | | | | | `[d94ff0602](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d94ff0602) attachKeyboard devient la méthode static create, le triggerButton change suivant ouvert/fermé (2021-09-22 18:38) <Daniel Caillibaud>  
`| | * | | | | | `[35f987140](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35f987140) fix focus Mq (au clic et au changement d'input) (2021-09-22 17:17) <Daniel Caillibaud>  
`| | * | | | | | `[7fd1d4c05](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7fd1d4c05) faut garder la bordure bleue du focus Mq au clic pour déplier le clavier (2021-09-22 17:07) <Daniel Caillibaud>  
`| | * | | | | | `[587d8ddee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/587d8ddee) On ouvre le clavier au focusin sur un input s'il y avait déjà un clavier ouvert (2021-09-22 16:48) <Daniel Caillibaud>  
`| | * | | | | | `[6a1cf8541](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a1cf8541) on ferme le clavier au clic sur buttonEnter (2021-09-22 16:41) <Daniel Caillibaud>  
`| | * | | | | | `[80598a3b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80598a3b5) modif commentaire (2021-09-21 19:22) <Daniel Caillibaud>  
`| | * | | | | | `[e9668897b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9668897b) console.log viré (2021-09-21 19:19) <Daniel Caillibaud>  
`| | * | | | | | `[47aa2358b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47aa2358b) Les boutons ne doivent être focusable pour qu'on pjuisse passer de l'un à l'autre via la touche Tab (2021-09-21 17:22) <Yves Biton>  
`| | * | | | | | `[6ac08f014](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ac08f014) refacto en séparant l'instanciation des objets et l'ajout dans le dom (2021-09-21 16:12) <Daniel Caillibaud>  
`| |/ / / / / /  `  
`| * | | | | |   `[8c0d937b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c0d937b0) Merge branch 'master' into Amélioration_Editeurs_MathQuill (2021-09-21 14:21) <Daniel Caillibaud>  
`| |\ \ \ \ \ \  `  
`| * | | | | | | `[de9409d97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de9409d97) Modif de l'ordre d'apparition des touches correspondant aux opérations (2021-09-21 03:01) <Yves Biton>  
`| * | | | | | | `[076addacd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/076addacd) Petite modif pour que les caractères touches soient centrés horizontalement. (2021-09-21 02:53) <Yves Biton>  
`| * | | | | | | `[50bbd3b14](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50bbd3b14) Modification pour que la petite flèche  qui, quand on la clique, fait apparaître un clavier virtuel fonctionne en mode bascule. (2021-09-21 02:48) <Yves Biton>  
`| * | | | | | | `[e2a6d5dfc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2a6d5dfc) J'(avais oublié les signes opératoires dans la palette de boutons (2021-09-19 18:24) <Yves Biton>  
`| * | | | | | | `[dd82cebbe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd82cebbe) Ajouts pour avoir un clavier virtuel appelé lors de j3pRestriction (2021-09-19 17:06) <Yves Biton>  
`| * | | | | | | `[f7749042e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7749042e) Ajout d'une classe Button pour gérer un clavier virtuel (2021-09-17 13:05) <Yves Biton>  
`* | | | | | | | `[412bad874](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/412bad874) hotfix 614aff5afd7ac50007cf0291 (2021-09-23 18:31) <Daniel Caillibaud>  
`* | | | | | | | `[6ebb8a22f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ebb8a22f) hotfix bugsnag 614af7a253bc1300074e2ee9 (2021-09-23 18:26) <Daniel Caillibaud>  
`* | | | | | | | `[50626f4fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50626f4fb) hotfix j3pAfficheCorrige (2021-09-23 18:00) <Daniel Caillibaud>  
`| |_|/ / / / /  `  
`|/| | | | | |   `  
`* | | | | | |   `[0f96d0105](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f96d0105) Merge branch 'fixCm2exN4_22' into 'master' (2021-09-23 15:55) <Daniel Caillibaud>  
`|\ \ \ \ \ \ \  `  
`| * | | | | | | `[2ad97a326](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ad97a326) fix eslint (2021-09-23 17:55) <Daniel Caillibaud>  
`| * | | | | | | `[3cb33f512](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3cb33f512) fix bugsnag 6148cebd67d8000007ca896 (2021-09-23 17:50) <Daniel Caillibaud>  
`|/ / / / / / /  `  
`* | | | | | |   `[9b26b57b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b26b57b9) Merge branch 'tauxEvol' into 'master' (2021-09-23 15:24) <Daniel Caillibaud>  
`|\ \ \ \ \ \ \  `  
`| |_|_|/ / / /  `  
`|/| | | | | |   `  
`| * | | | | | `[81429e40d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81429e40d) nettoyage + suppression d'une fonction (2021-09-23 15:00) <Rémi Deniaud>  
`|/ / / / / /  `  
`* | | | | |   `[e161fabe7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e161fabe7) Merge branch 'fixConstBugsnag' into 'master' (2021-09-23 06:50) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[e01d79dfe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e01d79dfe) Majuscule virées pour les fonctions qui ne sont pas des constructeurs (2021-09-23 08:47) <Daniel Caillibaud>  
`| | |_|_|_|/  `  
`| |/| | | |   `  
`| * | | | | `[90f6d5840](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90f6d5840) 2 bugsnag, remplace me par stor parfois, +1 notify pour un bugsnag que je pige pas (2021-09-22 22:28) <Tommy>  
`* | | | | |   `[8e00e153a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e00e153a) Merge branch 'fixMe2this' into 'master' (2021-09-22 19:07) <barroy tommy>  
`|\ \ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | | | |   `  
`| * | | | | `[adad7fca7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/adad7fca7) remplace this avec me devant fin enonce, je merge direct parceque ca passe pas en prod (2021-09-22 21:06) <Tommy>  
`|/ / / / /  `  
`* | | | | `[4b5e47c03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b5e47c03) hotfix init des props créées par sectionCourante avant de les mémoriser (2021-09-22 13:20) <Daniel Caillibaud>  
`* | | | | `[74763dfc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/74763dfc4) retag 1.0.13 (2021-09-22 12:57) <Daniel Caillibaud>  
`* | | | |   `[3967ffb3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3967ffb3f) Merge branch 'checkParcoursIntegrity' into 'master' (2021-09-22 10:56) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[46f4db07a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46f4db07a) On vérifie que la section ne tripote pas l'instance courante de parcours avec ses mains sales (2021-09-22 12:56) <Daniel Caillibaud>  
`|/ / / / /  `  
`* | | | |   `[ac8e7d23c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac8e7d23c) Merge branch 'fixPyth01' into 'master' (2021-09-22 10:39) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[8a0337bec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a0337bec) me & stor en const, fct internes remis en interne de l'instance (et pas en global à la section) (2021-09-22 12:37) <Daniel Caillibaud>  
`| * | | | | `[10b8bb5a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10b8bb5a4) vire les console (2021-09-21 22:32) <Tommy>  
`| * | | | | `[bf4cccd45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf4cccd45) bugsnag eslint et un bug de etape, et un bug d'init (2021-09-21 22:28) <Tommy>  
`* | | | | |   `[5e357b112](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e357b112) Merge branch 'fixEcrirechiffreBugsnag' into 'master' (2021-09-22 10:31) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[674851117](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/674851117) derniers warnings eslint corrigés (2021-09-22 12:29) <Daniel Caillibaud>  
`| * | | | | | `[c6836470b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6836470b) encore un peu de nettoyage (2021-09-22 12:21) <Daniel Caillibaud>  
`| * | | | | |   `[d7be44b37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7be44b37) Merge branch 'fixEcrirechiffreBugsnagCorrectif' into fixEcrirechiffreBugsnag (2021-09-22 12:12) <Daniel Caillibaud>  
`| |\ \ \ \ \ \  `  
`| | * | | | | | `[dd18ba4db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd18ba4db) récup d'autres modifs faites dans la branche fixEcrirechiffreBugsnag d'origine (2021-09-22 12:08) <Daniel Caillibaud>  
`| | * | | | | | `[a92d68a72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a92d68a72) nettoyage (2021-09-22 11:54) <Daniel Caillibaud>  
`| * | | | | | | `[7b4993ef6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b4993ef6) bugsnag et mise en page tableau en mode zoneavec (2021-09-21 12:23) <Tommy>  
`| |/ / / / / /  `  
`* | | | | | |   `[98a3dd27a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98a3dd27a) Merge branch 'fixNomme03Bugsnag' into 'master' (2021-09-22 09:42) <Daniel Caillibaud>  
`|\ \ \ \ \ \ \  `  
`| |_|_|_|_|_|/  `  
`|/| | | | | |   `  
`| * | | | | | `[9b742da6c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b742da6c) un bugsnag, un autre bug que j'ai oublié, et des zonecouleurs (2021-09-21 23:10) <Tommy>  
`| | |/ / / /  `  
`| |/| | | |   `  
`* | | | | |   `[a58e9d9b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a58e9d9b2) Merge branch 'fixProgConsBugsnag' into 'master' (2021-09-22 09:39) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[02d6aa67f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02d6aa67f) fix j utilisé en dehors de sa boucle for (2021-09-21 10:32) <Daniel Caillibaud>  
`| * | | | | | `[64a4cd6b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64a4cd6b7) bugsnag, des fois faut par mettre let j dans un for, quand le j il sert aussi après la boucle (2021-09-20 20:57) <Tommy>  
`| | |/ / / /  `  
`| |/| | | |   `  
`* | | | | |   `[989be5b96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/989be5b96) Merge branch 'fixShuffleMulti' into 'master' (2021-09-22 09:36) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[45a78bb45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45a78bb45) Aj de tests unitaires pour j3pShuffle et j3pShuffleMulti (2021-09-22 11:35) <Daniel Caillibaud>  
`| * | | | | | `[63a5e09d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63a5e09d6) fix j3pShuffleMulti (2021-09-22 11:34) <Daniel Caillibaud>  
`* | | | | | |   `[246288809](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/246288809) Merge branch 'complexes' into 'master' (2021-09-22 07:20) <Daniel Caillibaud>  
`|\ \ \ \ \ \ \  `  
`| |/ / / / / /  `  
`|/| | | | | |   `  
`| * | | | | | `[b54b6520e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b54b6520e) nettoyage code (2021-09-21 22:24) <Rémi Deniaud>  
`| * | | | | | `[d641c92e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d641c92e8) nettoyage code (2021-09-21 22:24) <Rémi Deniaud>  
`| | |/ / / /  `  
`| |/| | | |   `  
`* | | | | |   `[e57cb43e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e57cb43e9) Merge branch 'fixAsyncStartMed01' into 'master' (2021-09-22 07:05) <Daniel Caillibaud>  
`|\ \ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | | | |   `  
`| * | | | | `[e683e84c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e683e84c2) fix chargement async (correction était appelé avant la fin du chargement) (2021-09-22 09:04) <Daniel Caillibaud>  
`|/ / / / /  `  
`* | | / / `[97daf02fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97daf02fa) retag 1.0.12 (2021-09-21 19:51) <Daniel Caillibaud>  
`| |_|/ /  `  
`|/| | |   `  
`* | | |   `[0e2542cd1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e2542cd1) Merge branch 'pythagore' into 'master' (2021-09-21 08:40) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[4c17bf4cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c17bf4cf) correction fcts_valid + nettoyage theoremedepythagore (2021-09-20 21:49) <Rémi Deniaud>  
`| |/ / /  `  
`* | | |   `[936e43f19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/936e43f19) Merge branch 'fixBienEcrireBugsnag' into 'master' (2021-09-21 07:48) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[467f45908](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/467f45908) bugsnag (2021-09-20 20:43) <Tommy>  
`|/ / /  `  
`* | |   `[afc161872](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/afc161872) Merge branch 'modDerangeTab' into 'master' (2021-09-20 16:33) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[f65c5f6a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f65c5f6a6) Nettoyage section 751 (2021-09-20 11:27) <Daniel Caillibaud>  
`| * | | `[07726a3c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07726a3c0) j3pDerangeTab remplacé par j3pShuffle (qui existait déjà) quand y'a un seul tableau à mélanger, et par j3pShuffleMulti quand y'en a plusieurs (autant qu'on veut) (2021-09-20 11:27) <Daniel Caillibaud>  
`| * | | `[a94468599](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a94468599) Ajout j3pSplitIntervalleEntiers (2021-09-20 11:05) <Daniel Caillibaud>  
`| * | | `[03be8d11c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03be8d11c) meilleure regexp pour détecter un intervalle d'entiers (2021-09-20 10:58) <Daniel Caillibaud>  
`| * | | `[9feb7271a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9feb7271a) simplification regexp (2021-09-20 10:11) <Daniel Caillibaud>  
`* | | | `[c079e8d02](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c079e8d02) erreurs mathquill en console (sans remonter jusqu'à busgnag) (2021-09-20 16:46) <Daniel Caillibaud>  
`* | | | `[2cad0d9a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2cad0d9a3) simplification & fix eslint (2021-09-20 16:38) <Daniel Caillibaud>  
`* | | |   `[fb64ec2dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb64ec2dd) Merge branch 'fixReducFrac' into 'master' (2021-09-20 14:11) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[45ce09493](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45ce09493) fix bugsnag 614844482c319b0007dba910 (2021-09-20 16:07) <Daniel Caillibaud>  
`| * | | | `[acabc1d40](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/acabc1d40) fix bugsnag 614844482c319b0007dba910 (2021-09-20 16:06) <Daniel Caillibaud>  
`|/ / / /  `  
`* / / / `[f2afbb510](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2afbb510) hotfix orthographe (moitiée => moitié) (2021-09-20 11:36) <Daniel Caillibaud>  
`|/ / /  `  
`* | |   `[4cea4d9b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4cea4d9b2) Merge branch 'tauxEvol' into 'master' (2021-09-20 07:33) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[38bbd0cdf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38bbd0cdf) nettoyage tauxSuccessifs (2021-09-19 14:36) <Rémi Deniaud>  
`* | | |   `[53e02871b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53e02871b) Merge branch 'recur' into 'master' (2021-09-20 07:32) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[beec73ca3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/beec73ca3) dernière modif dans recu + me.indication dans Parcours.js (2021-09-19 22:00) <Rémi Deniaud>  
`|/ / /  `  
`* | |   `[b8d2daa4e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8d2daa4e) Merge branch 'refixValeurApprochee' into 'master' (2021-09-17 21:52) <barroy tommy>  
`|\ \ \  `  
`| * | | `[bb7ea9dcd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb7ea9dcd) pour zoneavecimage et fix bug correction ( ca prenait toujours 0 comme réponse ) (2021-09-17 23:44) <Tommy>  
`|/ / /  `  
`* | |   `[e85e69d5f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e85e69d5f) Merge branch 'fctReference' into 'master' (2021-09-17 17:42) <Daniel Caillibaud>  
`|\ \ \  `  
`| * \ \   `[1a35e07ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a35e07ca) Merge branch 'master' into fctReference (2021-09-17 19:39) <Daniel Caillibaud>  
`| |\ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`* | | |   `[74da3a9b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/74da3a9b1) Merge branch 'fixValeurApprochee' into 'master' (2021-09-17 16:44) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[699ba4698](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/699ba4698) bugfix oublié (introduit au commit précédent avec le passage de new Donnees à getDonnees) (2021-09-17 10:38) <Daniel Caillibaud>  
`| * | | | `[3a7d8d2cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a7d8d2cb) fin du nettoyage, plus de pb eslint (2021-09-17 10:30) <Daniel Caillibaud>  
`| * | | | `[6657e9a72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6657e9a72) fix tabConvFixe (qui peut prendre un HTMLElement puisque valeurApprochee lui en donne un) (2021-09-17 10:16) <Daniel Caillibaud>  
`| | |/ /  `  
`| |/| |   `  
`* | | |   `[4c627ea50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c627ea50) Merge branch 'cleaning' into 'master' (2021-09-17 16:39) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[8aece38bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8aece38bb) fix des 532 pbs de camelcase et 104 pb eqeqeq (il en reste 5 qu'il est trop risqué d'éliminer à cause d'un joyeux mélange string/number, ignorés avec commentaire dans le code) (2021-09-17 18:12) <Daniel Caillibaud>  
`| * | | | `[192e1f3ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/192e1f3ba) derniers pbs eslint réglés (sur les 324 errors et 353 warnings initiaux), restera camelcase et eqeqeq qui restent en très grand nombre (ignorés pour ce fichier) (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[76607c9c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76607c9c4) commentaires faisant référence à des fonctions disparues virés (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[6d3c41e71](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d3c41e71) Simplifications RegExp (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[134544496](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/134544496) Fonction inutile supprimée j3pDecomp (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[ee9a42a96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee9a42a96) Fonctions inutiles virées : j3pEstentier, j3pContenulatex, j3pEcritmathquill, j3pMathsEdite, j3pMathsEditeSpan, j3pObjetEtendre, j3pGetNbsFromIntervalle, j3pNombreDans, j3pAnimationVecteur (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[f361424c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f361424c0) j3pEcritDans viré (servait une seule fois pour faire du innerHTML=…) (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[54b91dff5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54b91dff5) Parcours.prototype.indication se passe désormais de j3pEcritDans, j3pAffiche et jQuery (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[8d6afc1e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d6afc1e9) j3pSimplifieUnionIntersection déplacé dans la seule section qui l'utilise (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[92ac66b4d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92ac66b4d) j3pRenvoieTabgeometrie viré (personne s'en servait) (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[aa7947a7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa7947a7a) j3pVariables viré (personne s'en servait) (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[7e7dceebf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e7dceebf) j3pPlay déplacé dans la seule section qui l'utilisait (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[9188467d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9188467d5) j3pSupprimeToutEspaces viré (remplacé par un `.replace(/\s/g, '')` dans les 2 sections qui l'utilisaient (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[b3cde7d01](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3cde7d01) j3pAfficheCorrige viré dans des commentaires (dans des sections qui ne l'importe pas) (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[ef0d4ae34](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef0d4ae34) aj d'une regex pour passer de snake_case en camelCase (2021-09-17 16:01) <Daniel Caillibaud>  
`| * | | | `[ef9162d04](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef9162d04) nettoyage (2021-09-17 16:01) <Daniel Caillibaud>  
`* | | | | `[a9b1b7816](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9b1b7816) Merge branch 'modJ3pAffiche' into 'master' (2021-09-17 16:36) <Yves Biton>  
`|\| | | | `  
`| * | | | `[345d2a9fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/345d2a9fe) fix nom de la section dans un commentaire (2021-09-17 12:55) <Daniel Caillibaud>  
`| * | | | `[240b8fb23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/240b8fb23) fix bug introduit par la simplification précédente, et explications sur les inputmqef qui sont désormais également retournés (2021-09-17 12:55) <Daniel Caillibaud>  
`| * | | | `[98a1a8042](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98a1a8042) simplification (2021-09-17 12:18) <Daniel Caillibaud>  
`| * | | | `[8c704ee80](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c704ee80) j3pAffiche retourne les elts qu'il crée (2021-09-17 12:08) <Daniel Caillibaud>  
`| |/ / /  `  
`| | * | `[a56f4771a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a56f4771a) plus de snake_case dans ValidationZones (répercuté sur les sections qui utilisaient ces méthodes et propriétés) (2021-09-17 19:19) <Daniel Caillibaud>  
`| | * | `[63bb55d62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63bb55d62) correction bonneReponse (2021-09-17 16:28) <Rémi Deniaud>  
`| |/ /  `  
`|/| |   `  
`* | |   `[3730eeace](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3730eeace) Merge branch 'diagVenn' into 'master' (2021-09-17 13:44) <Rémi Deniaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[81b8f2e31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81b8f2e31) enfin une faute idiote sur setText dans compositionCourbes (2021-09-17 15:29) <Rémi Deniaud>  
`| * | `[b961b622c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b961b622c) encore un pb de j3pFocus sur encadreFctRef (2021-09-17 15:20) <Rémi Deniaud>  
`| * | `[6f0689edc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f0689edc) correction des pbs de j3pFocus sur 3 sections (2021-09-17 15:00) <Rémi Deniaud>  
`|/ /  `  
`* |   `[df9a76187](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df9a76187) Merge branch 'cleanProgConstruction' into 'master' (2021-09-16 20:00) <barroy tommy>  
`|\ \  `  
`| * | `[c35d06adf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c35d06adf) aj d'une constante pour svgId (+ de 1700 occurrences !) (2021-09-16 19:05) <Daniel Caillibaud>  
`| * | `[33ba7178e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33ba7178e) me/stor & co deviennent des const (2021-09-16 19:02) <Daniel Caillibaud>  
`| * | `[ad2592ecc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad2592ecc) passage du style directement à j3pCreeSVG (2021-09-16 18:57) <Daniel Caillibaud>  
`| * | `[7452c45b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7452c45b9) manquait css fond (2021-09-16 15:25) <Tommy>  
`| * | `[9f7a0f346](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f7a0f346) enleve le fixme (2021-09-16 14:50) <Tommy>  
`| * | `[eef8c857b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eef8c857b) eslint fixes, reste plus que 80 warnings no-var (2021-09-15 17:20) <Daniel Caillibaud>  
`|  /  `  
`* |   `[c3f63299b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c3f63299b) Merge branch 'fixMed01Bugsnag' into 'master' (2021-09-16 19:50) <barroy tommy>  
`|\ \  `  
`| * | `[7feddc747](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7feddc747) viré une console et echanger disable et barre ou couleur (2021-09-16 21:49) <Tommy>  
`| * | `[dd54af2eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd54af2eb) derniers warnings eslint virés (2021-09-16 18:47) <Daniel Caillibaud>  
`| * | `[a658e7078](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a658e7078) manquait css fond (2021-09-16 15:28) <Tommy>  
`| * | `[6d59581e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d59581e6) fix med01 bugsnag, et clean un peu (2021-09-16 15:08) <Tommy>  
`* | |   `[5dca83b84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5dca83b84) Merge branch 'fixBienecrire' into 'master' (2021-09-16 19:23) <barroy tommy>  
`|\ \ \  `  
`| * | | `[10b141e12](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10b141e12) jai modif un peu les bulles d aides (2021-09-16 21:17) <Tommy>  
`| * | | `[1a50fec26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a50fec26) relocalisation de variable (2021-09-16 18:25) <Daniel Caillibaud>  
`| * | | `[6dd6a5d74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6dd6a5d74) styletexte vides virés (2021-09-16 18:22) <Daniel Caillibaud>  
`| * | | `[1ab2ce595](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ab2ce595) Aj commentaire FIXME (2021-09-16 18:22) <Daniel Caillibaud>  
`| * | | `[a8da072ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8da072ef) suite et fin du nettoyage eslint (avec qq modifs de code) (2021-09-16 18:12) <Daniel Caillibaud>  
`| * | | `[6674f5d1c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6674f5d1c) bugsnag, eslint et fond (2021-09-16 16:00) <Tommy>  
`| |/ /  `  
`* | | `[ca972e246](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca972e246) retag 1.0.11 (2021-09-16 19:29) <Daniel Caillibaud>  
`* | |   `[3d965b3f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d965b3f3) Merge branch 'fixDecal01' into 'master' (2021-09-16 15:46) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[a8281b840](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8281b840) virer un collapse dans table général et un border 0 dans td général pour que les espacement horizontaux restent les même theme standart ou theme zoneavecimagedefond (2021-09-16 14:33) <Tommy>  
`|/ /  `  
`* |   `[bbb275e94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bbb275e94) Merge branch 'Fix_Squelette' into 'master' (2021-09-16 12:16) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[e7fa95b96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e7fa95b96) Correction de focus donnés à des éléments pas encore existants. Amélioration code ES6. (2021-09-16 11:18) <Yves Biton>  
`* | |   `[2b62b9bf2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b62b9bf2) Merge branch 'ajoutTestUnLatexify' into 'master' (2021-09-16 08:10) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[5a110b4cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a110b4cc) /\\pi[ ]*/ => /\\pi/ + aj rmq (2021-09-15 19:35) <Daniel Caillibaud>  
`| * | `[6e0780577](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e0780577) Suppression des autres paramètres d'appel de unlatexify qui ne sont pas utilisés (autres que le premier paramètre) (2021-09-15 11:38) <Yves Biton>  
`| * | `[592592359](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/592592359) Suppression d'une ligne inutile dans unlatexify et rajout d'un text poiur vérifier que le nouveau MathQuill traite bien par exemple 2^12 3^5 (sans signe de multiplication) (2021-09-15 11:30) <Yves Biton>  
`| * | `[f3a56f00d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3a56f00d) aj d'une base de test unitaire de unLatexify (2021-09-15 10:38) <Daniel Caillibaud>  
`|  /  `  
`* |   `[59dfc1db5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/59dfc1db5) Merge branch 'refontzone' into 'master' (2021-09-16 06:37) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[b933f9c6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b933f9c6e) On remplace gris.png (127×77 et 8826 octets) par sa version gris.jpg (16×16 et 347 octets) (2021-09-16 08:29) <Daniel Caillibaud>  
`| * |   `[71701667f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/71701667f) Merge branch 'master' into refontzone (2021-09-16 08:19) <Daniel Caillibaud>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[1a34e9df0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a34e9df0) Merge branch 'dropNomVariable' (2021-09-16 00:36) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[09f44291f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09f44291f) On vire le passage du parametre nomvariable à Repere et Tableauconversion qui ne l'utilise pas (ça tombe bien car ce nom sensé être celui d'une var globale n'existait souvent pas) (2021-09-16 00:35) <Daniel Caillibaud>  
`|/ / /  `  
`* | | `[843a3163e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/843a3163e) hotfix, manquait une image, ça cassait le build (2021-09-16 00:03) <Daniel Caillibaud>  
`* | |   `[6a43479e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a43479e7) Merge branch 'fixNommeE' into 'master' (2021-09-15 20:49) <barroy tommy>  
`|\ \ \  `  
`| * | | `[f50442aff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f50442aff) fix (2021-09-15 22:48) <Tommy>  
`|/ / /  `  
`* | |   `[1a9ccc8bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a9ccc8bb) Merge branch 'refontzone' into 'master' (2021-09-15 20:05) <barroy tommy>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| | * `[a77177167](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a77177167) gris (2021-09-16 07:46) <Tommy>  
`| |/  `  
`| * `[c7e3cf6e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7e3cf6e3) fdskqljl (2021-09-15 22:04) <Tommy>  
`|/  `  
`* `[c0774618b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0774618b) hotfix classe css avec espace (2021-09-15 21:03) <Daniel Caillibaud>  

## version 1.0.10

`* `[c97d1da0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c97d1da0e) retag 1.0.10 (2021-09-15 17:51) <Daniel Caillibaud>  
`*   `[ef5bfab24](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef5bfab24) Merge branch 'recur' into 'master' (2021-09-15 15:46) <Daniel Caillibaud>  
`|\  `  
`| *   `[2b59cd841](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b59cd841) Merge branch 'master' into recur (2021-09-15 17:45) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* | `[93f7e3d52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93f7e3d52) hotfix plantage j3pAffiche (2021-09-15 17:09) <Daniel Caillibaud>  
`* | `[81c39bf49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81c39bf49) retag 1.0.9 (2021-09-15 10:55) <Daniel Caillibaud>  
`* |   `[a2b22f15c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2b22f15c) Merge branch 'modeliserLoi' into 'master' (2021-09-15 08:54) <Rémi Deniaud>  
`|\ \  `  
`| * \   `[a17dc56f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a17dc56f6) Merge branch 'master' into 'modeliserLoi' (2021-09-15 08:52) <Rémi Deniaud>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[bd8f4bc6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd8f4bc6e) Merge branch 'fix_Squelette' into 'master' (2021-09-15 08:41) <Yves Biton>  
`|\ \ \  `  
`| * | | `[9feb4d615](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9feb4d615) Correction des méfaits de la duplication de code ;-) (2021-09-15 09:43) <Daniel Caillibaud>  
`| * | | `[9e2515065](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e2515065) Correction des méfaits du unlatexify ... (2021-09-14 21:02) <Yves Biton>  
`* | | |   `[a0e1b9504](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0e1b9504) Merge branch 'miniFixInequationFctRef' into 'master' (2021-09-14 19:54) <Rémi Deniaud>  
`|\ \ \ \  `  
`| * | | | `[e8473ddf8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8473ddf8) fix des pbs eslint, nettoyage (2021-09-13 15:19) <Daniel Caillibaud>  
`| * | | | `[57118aac8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57118aac8) fix j3pFocus avec un id foireux (2021-09-13 15:02) <Daniel Caillibaud>  
`* | | | |   `[e1b06d414](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1b06d414) Merge branch 'fixEspeEcartTypeVa' into 'master' (2021-09-14 19:50) <Rémi Deniaud>  
`|\ \ \ \ \  `  
`| * | | | | `[8725fb649](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8725fb649) nettoyage & fix eslint (2021-09-13 15:56) <Daniel Caillibaud>  
`* | | | | |   `[e48cc496f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e48cc496f) Merge branch 'fixModeliserLoi' into 'master' (2021-09-14 19:42) <Rémi Deniaud>  
`|\ \ \ \ \ \  `  
`| * | | | | | `[c4628b26a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4628b26a) factorisation de code (2021-09-14 17:33) <Daniel Caillibaud>  
`| * | | | | | `[cabdbbd37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cabdbbd37) fix appel de ecoute qui se faisait toujours avec la dernière valeur prise par i dans la boucle (merci à Rémi qui a trouvé ce bug introduit récemment) (2021-09-14 17:29) <Daniel Caillibaud>  
`| * | | | | | `[9b6213666](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b6213666) blindage (on plante plus quand un elt n’existe pas) (2021-09-13 15:39) <Daniel Caillibaud>  
`| | |/ / / /  `  
`| |/| | | |   `  
`| | | | * | `[1c705364e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c705364e) modification de l'appel à la fonction ecoute (2021-09-14 16:10) <Rémi Deniaud>  
`| |_|_|/ /  `  
`|/| | | |   `  
`* | | | | `[a866db057](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a866db057) hotfix css de zoneStyleMathquill(1|2|3) (2021-09-13 20:12) <Daniel Caillibaud>  
`* | | | | `[4e17e7fa5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e17e7fa5) viré le warning sur l'outil calculatrice (faut d'abord nettoyer le code qui met mepcalculatrice en global) (2021-09-13 16:50) <Daniel Caillibaud>  
`| |/ / /  `  
`|/| | |   `  
`* | | | `[fba06fce4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fba06fce4) fix error skulpt inconnu (ça chargeait correctement quand même) (2021-09-13 15:45) <Daniel Caillibaud>  
`|/ / /  `  
`* | | `[25b195d52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25b195d52) bugfix chargement mathgraph avec getMtgCore (pb du preloader qui ne fonctionne qu'avec une callback) (2021-09-13 14:40) <Daniel Caillibaud>  
`* | | `[ffa404ff1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffa404ff1) fix message d'erreur à propos de mtg32 (qui n'a plus lieu d'être depuis qu'on l'a rétabli, il râlait pour rien) (2021-09-13 12:25) <Daniel Caillibaud>  
`* | | `[dc59b031f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc59b031f) simplification d'écriture mineure (2021-09-13 12:09) <Daniel Caillibaud>  
`* | | `[8a39d5e43](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a39d5e43) console.log viré (2021-09-13 12:09) <Daniel Caillibaud>  
`* | |   `[f562068ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f562068ff) Merge branch 'recur' into 'master' (2021-09-13 08:19) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[2753b0c1c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2753b0c1c) fix regexp pour j3pRestriction (2021-09-13 10:18) <Daniel Caillibaud>  
`* | | |   `[eb4f071e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb4f071e5) Merge branch 'annuleSuppressionOutilMtg32' into 'master' (2021-09-13 07:53) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |_|/ /  `  
`|/| | |   `  
`| * | | `[a358ee5eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a358ee5eb) On remet l'outil mtg32 (faudra le virer progressivement pour utiliser getMtgCore), quasiment un revert de aa9f6459. (2021-09-10 19:38) <Daniel Caillibaud>  
`| * | | `[1194cac03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1194cac03) section 602ter revue pour utiliser getMtgCore (plutôt qu'un mtg32App global qui n'existe plus) (2021-09-10 19:13) <Daniel Caillibaud>  
`| * | | `[4f40034e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f40034e8) aj d'un warning lors de l'usage de l'outil mathgraph pour inciter à utiliser l'import de la fct qui va retourner directement l'instance (2021-09-10 19:10) <Daniel Caillibaud>  
`| * | | `[9ed290fff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ed290fff) aj de loaders pour mathgraph, pour simplifier le code dans les sections (retourne directement l'instance mtg plutôt que de gérer le chargement dans la section à partir d'un mtgLoad global) (2021-09-10 19:09) <Daniel Caillibaud>  
`| * | | `[eedbc6fe3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eedbc6fe3) aj de l'option timeout à load (2021-09-10 19:07) <Daniel Caillibaud>  
`* | | |   `[e057bde17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e057bde17) Merge branch 'Fix_Squelette_Section' into 'master' (2021-09-11 12:08) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[4c36f5f53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c36f5f53) Modification du positionnement initial de la calculatrice dans ces sections et squelettes. Pour certains j'avais oublié de réactiver la calculatrice lors des répétitions suivantes. (2021-09-11 14:07) <Yves Biton>  
`|/ / / /  `  
`* | | |   `[fdea23906](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fdea23906) Merge branch 'Fix_Squelette' into 'master' (2021-09-11 11:31) <Yves Biton>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[2c8d13c33](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c8d13c33) Modification du positionnement initial de la calculatrice dans cette section (2021-09-11 13:29) <Yves Biton>  
`|/ / /  `  
`* | | `[d0b43cb0b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0b43cb0b) retag 1.0.8 (2021-09-10 17:03) <Daniel Caillibaud>  
`* | |   `[6fef65fa9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6fef65fa9) Merge branch 'rationalisation' into 'master' (2021-09-10 15:02) <Daniel Caillibaud>  
`|\ \ \  `  
`| * \ \   `[713e9ea28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/713e9ea28) Merge branch 'master' into rationalisation (2021-09-10 17:01) <Daniel Caillibaud>  
`| |\ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`* | | |   `[bd58856ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd58856ef) Merge branch 'diagVenn' into 'master' (2021-09-10 14:26) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[c21f47fd4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c21f47fd4) setText avec false en 4ème arg + renommage (2021-09-10 14:51) <Rémi Deniaud>  
`* | | | | `[8e00ef179](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e00ef179) refreshUsage filtre les sections inconnues pour les mettre séparément (2021-09-10 15:35) <Daniel Caillibaud>  
`|/ / / /  `  
`| * | |   `[3f682a3a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f682a3a3) Merge branch 'ajDuree01' into 'rationalisation' (2021-09-10 14:59) <Daniel Caillibaud>  
`| |\ \ \  `  
`| | * | | `[ede6caf19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ede6caf19) aj section duree01 (prise dans j3p et adaptée) (2021-09-10 14:41) <Daniel Caillibaud>  
`| * | | | `[1306baf1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1306baf1f) màj usage avec les sections inconnues (2021-09-10 16:58) <Compte unix pour site j3p>  
`| * | | | `[0380051ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0380051ec) refreshUsage filtre les sections inconnues pour les mettre séparément (2021-09-10 16:37) <Daniel Caillibaud>  
`| * | | | `[23f802ff6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23f802ff6) mod refreshUsage.js pour indiquer les sections inconnues utilisées dans des ressources (2021-09-10 16:19) <Daniel Caillibaud>  
`| |/ / /  `  
`| * | | `[728ea9327](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/728ea9327) manquait ces modifs de master lors du merge précédent (2021-09-10 10:21) <Daniel Caillibaud>  
`| * | |   `[1eafcb7f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1eafcb7f6) Merge branch 'master' into rationalisation (2021-09-10 10:16) <Daniel Caillibaud>  
`| |\ \ \  `  
`| * | | | `[25e81d1c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25e81d1c6) fix loadMathquill (il veut un jQuery en global) (2021-09-10 09:01) <Daniel Caillibaud>  
`| * | | | `[24e75bdd6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24e75bdd6) fix séparateurs foireux dans les déclarations d'outils (2021-09-10 08:56) <Daniel Caillibaud>  
`| * | | | `[125be2d3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/125be2d3a) gros ménage dans adresses.js (on vire toutes les sections qui ne sont pas là, car pas passées dans sesaparcours, voire déjà supprimées dans j3p mais laissées dans la liste) (2021-09-09 20:36) <Daniel Caillibaud>  
`| * | | | `[e377ad95a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e377ad95a) fichiers orphelins virés (les sections qui les utilisent n'ont pas encore été migrées dans sesaparcours, car pas utilisées en prod au moment de la migration) (2021-09-09 20:35) <Daniel Caillibaud>  
`| * | | | `[1a86e616c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a86e616c) on vire src/legacy/sections/arborescence qui sert à rien (2021-09-09 20:34) <Daniel Caillibaud>  
`| * | | | `[51a2c1c95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51a2c1c95) encore un peu de ménage (2021-09-09 19:53) <Daniel Caillibaud>  
`| * | | | `[c129e29c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c129e29c8) nettoyage de virgule restante en fin de liste d'outils (2021-09-09 19:50) <Daniel Caillibaud>  
`| * | | | `[b4b8383d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4b8383d9) correction des derniers warnings eslint (2021-09-09 19:42) <Daniel Caillibaud>  
`| * | | | `[1bb6f61a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bb6f61a5) blindage contre un risque de boucle infinie + eslint fixes (2021-09-09 19:40) <Daniel Caillibaud>  
`| * | | | `[ca7a7f080](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca7a7f080) fct j3pOuvrePopup virée (jamais utilisée) (2021-09-09 19:08) <Daniel Caillibaud>  
`| * | | | `[6c07eeb86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c07eeb86) outil zoneStyleMathquill2 viré des déclarations (2021-09-09 19:03) <Daniel Caillibaud>  
`| * | | | `[8249a4cff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8249a4cff) outil zoneStyleMathquill viré des déclarations (2021-09-09 19:02) <Daniel Caillibaud>  
`| * | | | `[b29e2b04e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b29e2b04e) outil zoneclick viré des déclarations (2021-09-09 19:02) <Daniel Caillibaud>  
`| * | | | `[8e8cdcd23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e8cdcd23) outil tableur viré des déclarations (2021-09-09 18:53) <Daniel Caillibaud>  
`| * | | | `[e960b2634](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e960b2634) outil tableauSignesVariations viré des déclarations (2021-09-09 18:52) <Daniel Caillibaud>  
`| * | | | `[12312085c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12312085c) outil tableauconversion viré des déclarations (2021-09-09 18:50) <Daniel Caillibaud>  
`| * | | | `[599b79269](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/599b79269) outil menuContextuel viré des déclarations (2021-09-09 18:49) <Daniel Caillibaud>  
`| * | | | `[854cc4aa8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/854cc4aa8) outil listeDeroulante viré des déclarations (2021-09-09 18:48) <Daniel Caillibaud>  
`| * | | | `[10652bb8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10652bb8c) outil geometrie viré des déclarations (2021-09-09 18:44) <Daniel Caillibaud>  
`| * | | | `[f51b36df9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f51b36df9) outils fctsGestionParametres, fctsTableauSignes et fctsGestionParametres virés des déclarations (2021-09-09 18:44) <Daniel Caillibaud>  
`| * | | | `[5eb49b724](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5eb49b724) outil etiquette viré des déclarations (2021-09-09 18:41) <Daniel Caillibaud>  
`| * | | | `[f960368cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f960368cc) outil droitegraduee viré des déclarations (2021-09-09 18:41) <Daniel Caillibaud>  
`| * | | | `[445add7d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/445add7d8) outils brouillon et bulleAide virés des déclarations (2021-09-09 18:39) <Daniel Caillibaud>  
`| * | | | `[0a1b03196](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a1b03196) outil boutonsStyleMatquill viré des déclarations (2021-09-09 18:37) <Daniel Caillibaud>  
`| * | | | `[07ec85932](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07ec85932) outil boulier viré des déclarations (2021-09-09 18:35) <Daniel Caillibaud>  
`| * | | | `[f519797d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f519797d6) outil blockly viré des déclarations (2021-09-09 18:34) <Daniel Caillibaud>  
`| * | | | `[273d8ad8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/273d8ad8c) outil algebrite viré des déclarations (2021-09-09 18:33) <Daniel Caillibaud>  
`| * | | | `[b4f879c0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4f879c0e) outil 3D viré des déclarations (2021-09-09 18:32) <Daniel Caillibaud>  
`| * | | | `[087383770](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/087383770) déclaration outil blockly inutile virée (2021-09-09 17:54) <Daniel Caillibaud>  
`| * | | | `[a0ee659ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0ee659ce) On signale les incohérences dans les déclarations d’outils des sections (2021-09-09 17:54) <Daniel Caillibaud>  
`| * | | | `[aa9f6459c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa9f6459c) outil mtg32 remplacé par mathgraph (qui existait déjà) (2021-09-09 16:43) <Daniel Caillibaud>  
`| * | | | `[1602576e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1602576e5) renommage outils maths => mathquill, et lib/matquill/mathquill.js en lib/matquill/loadMathquill.js, avec un peu d'optimisation dans loadMathquill (2021-09-09 16:40) <Daniel Caillibaud>  
`| | | | * `[c15d75008](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c15d75008) console.log viré (2021-09-15 17:43) <Daniel Caillibaud>  
`| | | | * `[cccfec2be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cccfec2be) toujours cette gestion de la main à redonner pour continuer (2021-09-15 15:48) <Rémi Deniaud>  
`| | | |/  `  
`| | | * `[8ff7a9132](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8ff7a9132) suppression des div en double qui bloquaient la section (2021-09-11 17:24) <Rémi Deniaud>  
`| | | * `[274d947ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/274d947ba) renommage div pour que 2 div n'aient pas le même id (2021-09-11 10:28) <Rémi Deniaud>  
`| | | * `[b7f6c3bef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7f6c3bef) renommage + positionnement en relatif (2021-09-11 10:22) <Rémi Deniaud>  
`| |_|/  `  
`|/| |   `  
`* | | `[654acfe82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/654acfe82) manquait le loader j3pBuildUsageTree pour liste.html (2021-09-10 11:21) <Daniel Caillibaud>  
`* | |   `[8b8b4da24](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b8b4da24) Merge branch 'Fix_Squelette' into 'master' (2021-09-10 08:51) <Daniel Caillibaud>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[7f9dae5b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f9dae5b8) Suppression de variables globales (2021-09-10 10:43) <Yves Biton>  
`|/ /  `  
`* | `[76cdd42a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76cdd42a9) hotfix vérif des données du fichier annexe (2021-09-10 09:07) <Daniel Caillibaud>  
`* |   `[a6d217f65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6d217f65) Merge branch 'Fix_Css' into 'master' (2021-09-09 16:30) <Yves Biton>  
`|\ \  `  
`| * | `[c70f34fa8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c70f34fa8) Supression du vertical-alignment : middle dans .mq-root-block,                                                .mq-math-mode .mq-root-block (2021-09-09 18:27) <Yves Biton>  
`|/ /  `  
`* |   `[42f6b4caf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42f6b4caf) Merge branch 'fix_Css_Alignment' into 'master' (2021-09-09 15:33) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[9c634d1a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c634d1a7) Pour réparer pbs d'alignement ds fractions entre autres (2021-09-09 17:31) <Yves Biton>  
`|/  `  
`*   `[4e5aa198e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e5aa198e) Merge branch 'modSqCalcParam1Edit' into 'master' (2021-09-09 13:56) <Yves Biton>  
`|\  `  
`| * `[e5cbb0d8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5cbb0d8d) fix warnings eslint (2021-09-09 11:19) <Daniel Caillibaud>  
`| * `[f0ca9cd81](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0ca9cd81) On découpe un peu plus les différentes étapes du chargement, datas puis mtg puis init du dom puis init de l'énoncé (2021-09-09 11:17) <Daniel Caillibaud>  
`* | `[27d8211d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27d8211d8) annulation d'un truc passé dans master par erreur (2021-09-09 14:52) <Daniel Caillibaud>  
`* | `[6b29b0f31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b29b0f31) retag 1.0.7 | qq hotfixes (2021-09-09 13:24) <Daniel Caillibaud>  
`* | `[bf8bbf994](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf8bbf994) hotfix chaînage cmd latex, pour autoriser .mathquill('latex', 'blabla').blur() (2021-09-09 13:24) <Daniel Caillibaud>  
`|/  `  
`* `[fe7734205](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe7734205) hotfix import jquery manquant (2021-09-09 10:21) <Daniel Caillibaud>  
`*   `[f09b1f68c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f09b1f68c) Merge branch 'fixSqCalcParamMultiEdit' (2021-09-09 10:17) <Daniel Caillibaud>  
`|\  `  
`| * `[8e2ead2bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e2ead2bb) j3pDiv remplacé par j3pAddElt quand y'a pas de traitement sur le contenu (2021-09-09 10:12) <Daniel Caillibaud>  
`| * `[0e11d66bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e11d66bb) fix pb async (focus sur un elt qui existe pas encore => faut pas écrire de code après initMtg() mais le mettre dans sa callback) (2021-09-09 10:12) <Daniel Caillibaud>  
`| * `[19938effa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19938effa) on ajoute un contrôle dans j3pFocus sur les input mathquill, et si ça rate on tente une 2e fois (2021-09-09 10:12) <Daniel Caillibaud>  
`|/  `  
`* `[ffd59b93b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffd59b93b) hotfix try/catch dans le select de mathquill pour calmer bugsnag (2021-09-09 10:12) <Daniel Caillibaud>  
`* `[e7bd67503](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e7bd67503) retag 1.0.6 | oubli d'incrément de la version (2021-09-09 07:48) <Daniel Caillibaud>  
`*   `[a7a49eaa7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7a49eaa7) Merge branch 'refixAngles' into 'master' (2021-09-08 22:13) <barroy tommy>  
`|\  `  
`| * `[1121ef244](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1121ef244) jl bug (2021-09-09 00:12) <Tommy>  
`* |   `[9c073e7ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c073e7ac) Merge branch 'refixStat2' into 'master' (2021-09-08 21:59) <barroy tommy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[fe30e38b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe30e38b0) bug jl (2021-09-08 23:59) <Tommy>  
`|/  `  
`*   `[9f66dea27](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f66dea27) Merge branch 'fixSquelette' into 'master' (2021-09-08 15:15) <Yves Biton>  
`|\  `  
`| * `[717b2599a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/717b2599a) Utilisation de storage et optimisation de code es6 (2021-09-08 16:54) <Yves Biton>  
`| * `[f466b064c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f466b064c) Utilisation de storage et optimisation de code es6 (2021-09-08 16:51) <Yves Biton>  
`| * `[d2726c4cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2726c4cc) Utilisation de storage et optimisation de code es6 (2021-09-08 16:38) <Yves Biton>  
`| * `[6fc733296](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6fc733296) Utilisation de storage et optimisation de code es6 (2021-09-08 16:32) <Yves Biton>  
`| * `[b480d8cc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b480d8cc7) Utilisation de storage et optimisation de code (2021-09-08 16:22) <Yves Biton>  
`| * `[8f9b51b3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f9b51b3d) Utilisation de storage pour cette section (2021-09-08 16:16) <Yves Biton>  
`| * `[31eda25cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31eda25cd) Utilisation de storage et optimisation de code (2021-09-08 16:14) <Yves Biton>  
`| * `[971145209](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/971145209) Suppression de variables globales (2021-09-08 16:04) <Yves Biton>  
`| * `[43731c998](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43731c998) Utilisation de storage. Optimisation es6 (2021-09-08 15:58) <Yves Biton>  
`| * `[6c294482f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c294482f) Cette section n'avait pas été revue pour Sésaparcours en es6. Correction du focus lors d'une erreur de syntaxe dans un des éditeurs (2021-09-08 15:47) <Yves Biton>  
`| * `[9e38e7220](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e38e7220) Cette section n'avait pas été revue pour Sésaparcours en es6. Correction du focus lors d'une erreur de syntaxe dans un des éditeurs (2021-09-08 15:23) <Yves Biton>  
`| * `[79935d6b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79935d6b3) Cette section n'avait pas été revue pour Sésaparcours et par exemple le bouton Recopier ne fonctionnait pas. (2021-09-08 14:50) <Yves Biton>  
`| * `[50d7afe85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50d7afe85) Juste un var remplacé par un const (2021-09-07 21:09) <Yves Biton>  
`| * `[8aca12d65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8aca12d65) J'apporte à cette section les même améliorations que le squelette correspondant et optimisation du code (2021-09-07 17:22) <Yves Biton>  
`| * `[e1dea6cc8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1dea6cc8) Nouvelle correction de ce squelette pour refuser comme incorrect un vecteur avec dessous un vecteur mélangé à un point ou deux vecteurs (2021-09-07 17:02) <Yves Biton>  
`| * `[f5a82624e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5a82624e) Correction d'un défaut ancien qui faisiant que si le vecteur i est autorisé dan la réponse, une réponse avec des parenthèses était jugée fausse. (2021-09-07 16:43) <Yves Biton>  
`| * `[e7ab94a1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e7ab94a1f) Il y avait deux problèmes dans cette section : Le MathQuill nouveau ne renvoie pas la même chose quand il y a des vecteurs et il ne fallait pas appeler unLatexify dans traiteMathQuill (problème de virgules remplacées par des pojnts et ici il ne faut pas). (2021-09-07 16:00) <Yves Biton>  
`| * `[a95b08f41](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a95b08f41) Modification du squelette Calc_param8Vect_Prosca pour les mêmes raisons que les squelettes précédents et optimisation code ES6 Modifications mineures du squelette Calc_Param_MathQuill (2021-09-07 15:33) <Yves Biton>  
`| * `[a0ed8b13d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0ed8b13d) Modification de ce squelette pour les mêmes raisons que les squelettes précédents et optimisation code ES6 (2021-09-07 15:11) <Yves Biton>  
`| *   `[d6ce2b52b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d6ce2b52b) Merge remote-tracking branch 'origin/fixSquelette' into fixSquelette (2021-09-07 13:12) <Yves Biton>  
`| |\  `  
`| | * `[aec35f347](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aec35f347) mutualisation unlatexify (2021-09-07 13:06) <Daniel Caillibaud>  
`| * | `[8fa2e3352](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8fa2e3352) Je fais les mêmes modifs d'appel de validation que dans le squelette squeletteCalc_Param_MathQuill (2021-09-07 13:11) <Yves Biton>  
`| |/  `  
`| * `[41a7bb96a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41a7bb96a) Les deux return rajoutés ne doivent être exécutés que si on a rappelé la section en mode correction. (2021-09-07 12:45) <Yves Biton>  
`| * `[994d9f182](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/994d9f182) Ajout de deux return après appel de validation pour que ke comportement soit le même qu'avant les modifications au squelette. Refactoring du code. (2021-09-07 11:52) <Yves Biton>  
`* |   `[45ad0873a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45ad0873a) Merge branch 'multiFixes' into 'master' (2021-09-08 13:48) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[2a4a092e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a4a092e0) j3pAffiche retourne le span qu'il crée (2021-09-08 15:18) <Daniel Caillibaud>  
`| * | `[ef90a91e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef90a91e6) blindage j3pAffiche, on génère un nouvel id si celui qu'on nous donne est celui d'un élément existant (2021-09-08 11:29) <Daniel Caillibaud>  
`| * | `[93099a170](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93099a170) il faut une phrase de conclusion qui s'adapte au genre de la consigne (est égal ou est égale) (2021-09-08 09:46) <Daniel Caillibaud>  
`| * | `[6107bf121](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6107bf121) modif mineure de libellé ("égal" => "est égal à) (2021-09-08 08:50) <Daniel Caillibaud>  
`* | |   `[645c1db9f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/645c1db9f) Merge branch 'fixAppartenance' into 'master' (2021-09-08 12:11) <barroy tommy>  
`|\ \ \  `  
`| * | | `[d721331ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d721331ac) bugsnag (2021-09-08 14:09) <Tommy>  
`|/ / /  `  
`* | |   `[9bfb21007](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9bfb21007) Merge branch 'fixFocusModeliserLoi' into 'master' (2021-09-08 06:51) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[a28959d6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a28959d6f) fix id invalide passé à j3pFocus + qq bricoles (2021-09-07 09:08) <Daniel Caillibaud>  
`| * | | `[954963a8f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/954963a8f) commentaires (2021-09-07 09:08) <Daniel Caillibaud>  
`* | | |   `[2e10751b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e10751b4) Merge branch 'fixEquerre01' into 'master' (2021-09-08 06:51) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |_|/ /  `  
`|/| | |   `  
`| * | | `[98baaff5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98baaff5a) fix finEnonce (2021-09-06 20:06) <Daniel Caillibaud>  
`| * | | `[e305926e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e305926e0) suite du merge + fixes eslint (2021-09-06 19:59) <Daniel Caillibaud>  
`| * | | `[dadc24ecc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dadc24ecc) Merge branch 'master' into fixEquerre01 (2021-09-06 19:50) <Daniel Caillibaud>  
`| |\| | `  
`| * | | `[e45fcb82c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e45fcb82c) fix variable inexistante, normalisation du code, reste un pb pédago à régler par ailleurs (il veut que ce soit le grand coté de l'équerre sur la droite) (2021-09-01 08:05) <Daniel Caillibaud>  
`* | | |   `[c27db7840](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c27db7840) Merge branch 'fixDist' into 'master' (2021-09-07 20:18) <barroy tommy>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[8eed6e87f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8eed6e87f) bugsnag (2021-09-07 22:15) <Tommy>  
`|/ / /  `  
`* | |   `[3094e199d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3094e199d) Merge branch 'fixStat02' into 'master' (2021-09-06 21:44) <barroy tommy>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[67ef3f803](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67ef3f803) petit fix (2021-09-06 23:43) <Tommy>  
`|/ /  `  
`* |   `[16af40104](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16af40104) Merge branch 'fixProbleme' into 'master' (2021-09-06 17:31) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[ceb28b0bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ceb28b0bb) ajout d'espace avant les : (2021-09-06 19:26) <Daniel Caillibaud>  
`| * | `[9c1216f75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c1216f75) remplacement de * par × et : par ÷ dans les énoncés (2021-09-06 19:18) <Daniel Caillibaud>  
`* | | `[b27232b48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b27232b48) Merge branch 'fixStat01' into 'master' (2021-09-06 17:31) <Daniel Caillibaud>  
`|\| | `  
`| * | `[45e375bee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45e375bee) nettoyage de code (2021-09-06 18:17) <Daniel Caillibaud>  
`| * | `[508f80d48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/508f80d48) afindPos était définie 4× !!! (2021-09-06 15:39) <Daniel Caillibaud>  
`| * | `[09742537f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09742537f) fix appel de BoutonStyleMathquill sans new (2021-09-06 15:29) <Daniel Caillibaud>  
`* | |   `[764f5769f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/764f5769f) Merge branch 'fixMq' into 'master' (2021-09-06 17:31) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[a243e40a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a243e40a5) fix clignotement du curseur dans les champs mathquill lorsqu'ils sont vides (2021-09-06 19:03) <Daniel Caillibaud>  
`| * | | `[f53526c25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f53526c25) simplification j3pFocus, pareil pour tout le monde, on donne le focus dès la première question même pour firefox (2021-09-06 18:38) <Daniel Caillibaud>  
`| |/ /  `  
`* | |   `[9fb55efa6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9fb55efa6) Merge branch 'ameliorationSquelMtg' into 'master' (2021-09-06 12:41) <Yves Biton>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[eb224b55c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb224b55c) Petites modifs de Calc_Param_Multi_Edit_Apres_Interm  restées à l'écart (2021-09-06 12:41) <Daniel Caillibaud>  
`* | | `[67241a54a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67241a54a) hotfix import manquant pour j3pAffiche (j3pAddElt probablement disparu lors d'une résolution de conflit précédente un peu trop hative) (2021-09-06 13:51) <Daniel Caillibaud>  
`* | |   `[cbba8b3f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbba8b3f8) Merge branch 'multiFixes' into 'master' (2021-09-06 10:48) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * |   `[9497c05cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9497c05cc) Merge branch 'master' into multiFixes (2021-09-06 12:47) <Daniel Caillibaud>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[8a324a92c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a324a92c) Merge branch 'fixSommeFracUpdate' into 'master' (2021-09-06 10:34) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[e1e6a80b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1e6a80b5) appels de cacheBoutonValider en double virés (2021-09-06 12:30) <Daniel Caillibaud>  
`| * | | `[3881cc90a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3881cc90a) fix propriété nonSimplifie affectée parfois à parcours et parfois à sq (2021-09-06 12:30) <Daniel Caillibaud>  
`| * | | `[a9a031684](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9a031684) on cache le bouton valider après affichage de l'énoncé (2021-09-06 12:30) <Daniel Caillibaud>  
`| * | | `[08463bb96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/08463bb96) fixes eslint + petites optimisations (2021-09-06 12:30) <Daniel Caillibaud>  
`| * | | `[f28bbe391](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f28bbe391) aj finCorrection manquant (2021-09-06 12:29) <Daniel Caillibaud>  
`|/ / /  `  
`* | |   `[77275cd8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77275cd8a) Merge branch 'mutualisationUnLatexifyUpdate' into 'master' (2021-09-06 10:26) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[c0487bd17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0487bd17) addImplicitMult => unLatexifyAndAddImplicitMult (pour le helper, pas la méthode d'origine) (2021-09-06 12:23) <Daniel Caillibaud>  
`| * | | `[2ee3e5517](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ee3e5517) appels à Dimensions virés (2021-09-06 12:18) <Daniel Caillibaud>  
`| * | | `[e15b955e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e15b955e7) const sq = this.storage pour tous les squelettes (2021-09-06 12:18) <Daniel Caillibaud>  
`| * | | `[da8683eed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da8683eed) manquait le fichier commun (2021-09-06 12:18) <Daniel Caillibaud>  
`| * | | `[8813422cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8813422cb) fix des dernières erreur eslint (reste plein de warnings mais plus d'erreurs) (2021-09-06 12:18) <Daniel Caillibaud>  
`| * | | `[00a4d8f3c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00a4d8f3c) Mutualisation de code des squelettes mathgraph Ajout d'une fct générique unLatexify pour mutualiser la suite de replace à appliquer sur le latex venant de mathquill et mutualisation d'une fct addImplicitMult pour tous les squelettes dont le traiteMathquill faisait simplement unLatexify +  addImplicitMult (2021-09-06 12:17) <Daniel Caillibaud>  
`| * | | `[ff26481ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff26481ef) Modif des commentaires pour tester j3p avec un mathgraph local (2021-09-06 11:49) <Daniel Caillibaud>  
`|/ / /  `  
`| * | `[c445f105f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c445f105f) fixes eslint (2021-09-06 11:03) <Daniel Caillibaud>  
`| * | `[f5d2d48cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5d2d48cd) suppression du warning si j3pGetRandomInt est appelé avec un intervale de longueur 0 (2021-09-06 10:54) <Daniel Caillibaud>  
`| * | `[c643aaaca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c643aaaca) nettoyage (2021-09-06 10:46) <Daniel Caillibaud>  
`| * | `[38b06c327](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38b06c327) op++ n'est pas un id valide (en tout cas jQuery l'aime pas) (2021-09-06 10:31) <Daniel Caillibaud>  
`| * | `[e06197841](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e06197841) aj d'un test avec message d'erreur en console si on ne retrouve pas ses petits (id invalide) (2021-09-06 10:30) <Daniel Caillibaud>  
`| * | `[db51b9b6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db51b9b6f) fix erreurs si anticursor.ancestors est undefined (2021-09-06 10:14) <Daniel Caillibaud>  
`| * | `[9969b3d6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9969b3d6a) jsdoc (2021-09-06 10:06) <Daniel Caillibaud>  
`| * | `[46265d5c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46265d5c2) fixes eslint (2021-09-06 10:06) <Daniel Caillibaud>  
`| * | `[c7aa460c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7aa460c7) fix appels foireux de fg.png (mis dans la css) (2021-09-06 09:59) <Daniel Caillibaud>  
`| * | `[9ef04b524](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ef04b524) optimisation (mutualisation de selecteur jquery) (2021-09-06 09:46) <Daniel Caillibaud>  
`| * | `[3870b1652](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3870b1652) Commentaires obsolètes (souvent devenus complètement faux) virés (2021-09-06 09:46) <Daniel Caillibaud>  
`| * | `[145aa0483](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/145aa0483) màj des urls de tests en commentaire (2021-09-06 09:46) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[2e5f63fe7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e5f63fe7) Merge branch 'fixParent' into 'master' (2021-09-05 20:00) <barroy tommy>  
`|\ \  `  
`| * | `[6672f09d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6672f09d3) co bug jl (2021-09-05 21:59) <Tommy>  
`|/ /  `  
`* |   `[cc86918eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc86918eb) Merge branch 'fixProgconst' into 'master' (2021-09-05 19:13) <barroy tommy>  
`|\ \  `  
`| * | `[72f41b6f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72f41b6f1) jk (2021-09-05 21:12) <Tommy>  
`* | | `[c8603c792](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8603c792) Merge branch 'fixProgconst' into 'master' (2021-09-05 14:20) <barroy tommy>  
`|\| | `  
`| * | `[3e7ef0b10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e7ef0b10) bug jl (2021-09-05 16:19) <Tommy>  
`* | |   `[8b57771e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b57771e5) Merge branch 'fixCalcul01' into 'master' (2021-09-05 12:27) <barroy tommy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[3c09565ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c09565ad) un bug JL (2021-09-05 14:27) <Tommy>  
`|/ /  `  
`* |   `[b88018980](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b88018980) Merge branch 'fixAngle04' into 'master' (2021-09-05 09:45) <barroy tommy>  
`|\ \  `  
`| * | `[f6e3fb5ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6e3fb5ba) hj (2021-09-05 11:44) <Tommy>  
`* | | `[626c64987](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/626c64987) hotFix chargement jquery/jquery.textWidth/jquery-ui (2021-09-04 15:46) <Daniel Caillibaud>  
`* | | `[6e38c98e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e38c98e7) retag 1.0.5 (2021-09-04 14:53) <Daniel Caillibaud>  
`* | |   `[9647409d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9647409d6) Merge branch 'fixImportJquery' into 'master' (2021-09-04 12:52) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[39bcbbe37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39bcbbe37) aj de l'import jquery pour toutes les sections (2021-09-04 14:51) <Daniel Caillibaud>  
`| * | | `[c275aeaba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c275aeaba) aj de l'import jquery pour toutes les sections (2021-09-04 14:51) <Daniel Caillibaud>  
`| * | | `[496b0a635](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/496b0a635) on change le chemin du jquery pour prendre notre jQuery étendu avec l'ajout de la méthode textWidth (2021-09-04 14:51) <Daniel Caillibaud>  
`| * | | `[cee833e27](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cee833e27) aj du script qui va ajouter les import jQuery qui manquent (2021-09-04 14:51) <Daniel Caillibaud>  
`* | | |   `[6787c5607](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6787c5607) Merge branch 'hotFixConstruitStructurePage' into 'master' (2021-09-04 12:50) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[63ec27f97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63ec27f97) appel de construitStructurePage en version simplifiée (seulement la structure mais pas un objet à une seule propriété structure), pour être homogène avec toutes les autres sections. (2021-09-04 14:36) <Daniel Caillibaud>  
`| * | | `[b79f7b496](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b79f7b496) typofix (ça plantait pas mais ratioGauche était ignoré car nommé rationGauche) (2021-09-04 14:34) <Daniel Caillibaud>  
`| * | | `[256787708](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/256787708) fix pb de syntaxe (valeur sans propriété) (2021-09-04 14:33) <Daniel Caillibaud>  
`|/ / /  `  
`* | |   `[8bcd45b3c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8bcd45b3c) Merge branch 'fixDimensions' into 'master' (2021-09-04 11:46) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[84cc1cec9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84cc1cec9) restait deux appels de Dimensions sans argument (2021-09-04 13:40) <Daniel Caillibaud>  
`| * | | `[ddbfe26a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ddbfe26a7) Récup des ratioGauche avec une regex plus compliquée (2021-09-04 13:39) <Daniel Caillibaud>  
`| * | | `[e39d5f0c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e39d5f0c9) Récup des ratioGauche facile à rechercher / remplacer (2021-09-04 13:12) <Daniel Caillibaud>  
`| * | | `[d67a3fd6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d67a3fd6e) wip (2021-09-04 09:40) <Daniel Caillibaud>  
`* | | |   `[bb8ea488f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb8ea488f) Merge branch 'fixAngle04' into 'master' (2021-09-03 23:06) <barroy tommy>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| / /   `  
`| |/ /    `  
`| * / `[093d618c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/093d618c3) angle04 (2021-09-04 00:54) <Tommy>  
`|/ /  `  
`* |   `[459a09539](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/459a09539) Merge branch 'eqeq' into 'master' (2021-09-03 18:24) <Daniel Caillibaud>  
`|\ \  `  
`| * \   `[3d2388a9d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d2388a9d) Merge branch 'master' into eqeq (2021-09-03 20:24) <Daniel Caillibaud>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[9cfc74374](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9cfc74374) Merge branch 'Amelioration_Squelette' into 'master' (2021-09-03 14:00) <Yves Biton>  
`|\ \ \  `  
`| * | | `[d069fe7dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d069fe7dc) modif commentaires (2021-09-03 15:58) <Daniel Caillibaud>  
`| * | | `[84ddad8cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84ddad8cb) Deux constantes pour deux chaines utilisées en plusieurs endroits. J'ai aussi rajouté un paramètre infoParam qu'il faudrait mettre ds ttes les sections utilisant des paramètres. (2021-09-03 14:46) <Yves Biton>  
`| * | | `[38c248679](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38c248679) Je revies sur les modifs précédentes en initialisant sq à this.storage (2021-09-02 14:06) <Yves Biton>  
`| * | | `[7ab13d7d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ab13d7d7) Remplacement de toutes les références à parcours par sq Suppression de deux var (2021-09-02 13:19) <Yves Biton>  
`| * | | `[9401a9aff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9401a9aff) Amélioration de squelette et de fichiers annexes (2021-09-02 11:52) <Yves Biton>  
`* | | | `[876ef7596](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/876ef7596) fix regex d'intervalle (ça déconnait avec des bornes à plusieurs chiffres) (2021-09-01 20:31) <Daniel Caillibaud>  
`|/ / /  `  
`* | |   `[eebe90844](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eebe90844) Merge branch 'Nouveaux_Fichiers_Annexes' into 'master' (2021-09-01 11:09) <Yves Biton>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[95dd2fc77](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95dd2fc77) Nouveaux fichiers annexes (2021-09-01 13:05) <Yves Biton>  
`| * | `[add30433e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/add30433e) Nouveaux fichiers annexes (2021-09-01 13:04) <Yves Biton>  
`|/ /  `  
`| * `[310726e50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/310726e50) viré images & css inutilisées mais laissées en vrac à la racine de sections/ (2021-09-03 20:17) <Daniel Caillibaud>  
`| * `[682c14844](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/682c14844) fond.png toujours mis avec ajout de la classe css .fond (2021-09-03 20:13) <Daniel Caillibaud>  
`| * `[25fe32da1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25fe32da1) nettoyage (2021-09-03 20:10) <Daniel Caillibaud>  
`| * `[6133d9022](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6133d9022) paramètre MiseEnPagePlusClaire remplacé par le choix du thème partout (2021-09-03 19:47) <Daniel Caillibaud>  
`| * `[ffcc4bd38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffcc4bd38) theme oldMep renommé en zonesAvecImageDeFond (2021-09-03 19:33) <Daniel Caillibaud>  
`| * `[7dbc7a02e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7dbc7a02e) re miseeneq01 (2021-09-03 09:43) <Tommy>  
`| * `[595228dca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/595228dca) une nouvelle et des petits changements (2021-09-03 09:39) <Tommy>  
`| * `[68634bd6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68634bd6e) fix resetCss (2021-09-01 15:08) <Daniel Caillibaud>  
`| * `[5ff9e63a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ff9e63a5) optimisation des images du theme oldMep (2021-09-01 14:53) <Daniel Caillibaud>  
`| * `[83cd84770](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83cd84770) renommage de la classe css cbiencfaux en feedback (2021-09-01 14:45) <Daniel Caillibaud>  
`| * `[396fb5444](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/396fb5444) fix theme standard path (2021-09-01 14:31) <Daniel Caillibaud>  
`| * `[261f905de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/261f905de) rationalisation de la gestion des thèmes (aj du theme standard comme theme par défaut, vide pour le moment) (2021-09-01 11:50) <Daniel Caillibaud>  
`| * `[dde778464](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dde778464) j3pRemoveClass remplacé par classList.remove (on avait déjà les éléments, inutile de faire une requête à partir de leur id pour les retrouver) (2021-09-01 02:27) <Daniel Caillibaud>  
`| *   `[67139fdcf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67139fdcf) Merge branch 'master' into eqeq (2021-09-01 02:04) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* | `[9ddae9fef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ddae9fef) retag 1.0.4 (2021-09-01 02:02) <Daniel Caillibaud>  
`* | `[8d1d1aa2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d1d1aa2d) amélioration message logAll (2021-09-01 02:02) <Daniel Caillibaud>  
`* |   `[af50513ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af50513ab) Merge branch 'bugFixJ3pGetSystem' into 'master' (2021-08-31 23:55) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[5e77f3328](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e77f3328) qq fixes dans Pourcentages1 (2021-09-01 01:53) <Daniel Caillibaud>  
`| * | `[10333862e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10333862e) j3pGetSystem viré (ça plantait) et remplacé par j3pIsFirefox() (ça ne servait qu'à ça), mais son usage reste à proscrire (2021-09-01 01:52) <Daniel Caillibaud>  
`|/ /  `  
`* | `[bcdc540a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bcdc540a6) optimization.splitChunks.chunks = all semble mettre un beau bazar (2021-09-01 01:21) <Daniel Caillibaud>  
`| *   `[41d698501](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41d698501) Merge branch 'master' into eqeq (2021-09-01 00:39) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[fce6d75a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fce6d75a1) Merge branch 'fixLoader' into 'master' (2021-08-31 22:38) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[a8d8748f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8d8748f2) enfin une conf webpack qui fonctionne avec notre globalLoader (2021-09-01 00:37) <Daniel Caillibaud>  
`* | |   `[265179c12](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/265179c12) Merge branch 'python' into 'master' (2021-08-31 20:36) <Daniel Caillibaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[fa9f9ee0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa9f9ee0f) bouclePourListe -> modifs variables + gestion >>> (2021-08-31 15:50) <Rémi Deniaud>  
`| * |   `[adb7ffae1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/adb7ffae1) Merge branch 'master' into python (2021-08-31 15:48) <Rémi Deniaud>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[7984ba2d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7984ba2d6) correction equationFctLecture + mise en forme (2021-08-31 09:31) <Rémi Deniaud>  
`| * | `[850305ffe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/850305ffe) mise en forme modeliserLoi + racine_trinome (2021-08-30 23:41) <Rémi Deniaud>  
`| | * `[f061ee700](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f061ee700) Le if MiseEnPagePlusClaire dégage pour les modifs de classes css (theme oldMep ou pas autant les mettre, les autres themes pourront l'implémenter plus tard s'ils veulent), il ne reste que pour les modifs de propriétés de styles faites manuellement (sans classe css). Méthode Dimensions virée au passage (en servait que pour préciser la valeur par défaut). (2021-08-31 23:53) <Daniel Caillibaud>  
`| | * `[9440dcce2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9440dcce2) j3pAddClass amélioré (2021-08-31 23:47) <Daniel Caillibaud>  
`| | *   `[5c646be79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c646be79) Merge branch 'master' into eqeq (avec résolution des conflits sur src/legacy/core/Parcours.js et src/loader.js) (2021-08-31 20:38) <Daniel Caillibaud>  
`| | |\  `  
`| |_|/  `  
`|/| |   `  
`* | |   `[093a2edab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/093a2edab) Merge branch 'refactoPresentation' into 'master' (2021-08-30 19:43) <Daniel Caillibaud>  
`|\ \ \  `  
`| * \ \   `[e48b83cd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e48b83cd0) Merge branch 'master' into refactoPresentation (2021-08-30 21:42) <Daniel Caillibaud>  
`| |\ \ \  `  
`| | * | | `[e821f8908](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e821f8908) mod mineure label dans le log (2021-08-30 21:39) <Daniel Caillibaud>  
`| |/ / /  `  
`|/| | |   `  
`* | | |   `[0b0108bdd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b0108bdd) Merge branch 'bugFixes' into 'master' (2021-08-30 19:39) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[bda31a5a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bda31a5a4) jsdoc (2021-08-30 20:29) <Daniel Caillibaud>  
`| * | | | `[fc9c85ed6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc9c85ed6) moins de console.log (2021-08-30 20:29) <Daniel Caillibaud>  
`| * | | | `[4a8527d15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a8527d15) bugfix arrondi du score affiché (2021-08-30 20:28) <Daniel Caillibaud>  
`| * | | | `[138bc77fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/138bc77fd) enfin le bugfix du "il faut donner une réponse" intempestif alors qu'on vient de démarrer l'énoncé. (2021-08-30 20:28) <Daniel Caillibaud>  
`| * | | | `[688631f2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/688631f2e) modif de log (2021-08-30 20:28) <Daniel Caillibaud>  
`| * | | | `[def186fff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/def186fff) aj splitchunks (2021-08-30 20:28) <Daniel Caillibaud>  
`| * | | | `[d732086f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d732086f3) classe vide virée (2021-08-30 20:28) <Daniel Caillibaud>  
`| * | | | `[2b0f78e0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b0f78e0c) fix largeur #MG pour présentation3 (sans MD) (2021-08-30 20:28) <Daniel Caillibaud>  
`| * | | | `[c6f4e55da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6f4e55da) indexOf remplacé par includes (2021-08-30 20:27) <Daniel Caillibaud>  
`* | | | |   `[11c052333](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11c052333) Merge branch 'refacto501' into 'master' (2021-08-30 19:38) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[4f5526a72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f5526a72) refacto de la section 501 avec pas mal de bugfixes (2021-08-30 20:19) <Daniel Caillibaud>  
`| |/ / / /  `  
`* | | | |   `[7c749ee73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c749ee73) Merge branch 'modeliserLoi' into 'master' (2021-08-30 18:36) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| | / /   `  
`| | |/ /    `  
`| |/| |     `  
`| * | | `[7358385ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7358385ef) correction bug (2021-08-30 19:14) <Rémi Deniaud>  
`| * | | `[2f11a68d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f11a68d5) correction bug (2021-08-30 19:10) <Rémi Deniaud>  
`|/ / /  `  
`| * / `[0e7f3a2d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e7f3a2d4) refacto complète de squelettepresentation (2021-08-30 20:33) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[89daca78f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89daca78f) Merge branch 'Modification_Fichier_Annexe' into 'master' (2021-08-30 06:54) <Yves Biton>  
`|\ \  `  
`| * | `[f173c7530](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f173c7530) Correction fichier annexe (2021-08-30 08:50) <Yves Biton>  
`|/ /  `  
`* | `[fa19a9321](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa19a9321) fix détail dans la conclusion du log (2021-08-30 03:12) <Daniel Caillibaud>  
`* | `[22577f0c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22577f0c2) retag 1.0.3 | on compte aussi le nb de warnings dans loadAll (2021-08-30 02:57) <Daniel Caillibaud>  
`* | `[f160d50dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f160d50dc) rectif des messages en cas d'appel de j3pGetRandomInt avec un intervalle ne contenant qu'un seul entier, warning et plus error (2021-08-30 02:26) <Daniel Caillibaud>  
`* | `[de54ce7cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de54ce7cf) retag 1.0.3 | fix loader s'il fini son chargement avant le dom (par ex si chargé dans le <head> du html et déjà en cache, avec un js sync chargé ensuite) (2021-08-30 02:19) <Daniel Caillibaud>  
`* | `[1253723b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1253723b5) retag 1.0.2 | fix args du loader (2021-08-29 21:24) <Daniel Caillibaud>  
`* | `[f37da72de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f37da72de) retag 1.0.2 (2021-08-29 20:48) <Daniel Caillibaud>  
`* |   `[a62bb8135](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a62bb8135) Merge branch 'bugfixesMulti' into 'master' (2021-08-29 18:10) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[b982612a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b982612a1) console.log oublié (2021-08-29 20:10) <Daniel Caillibaud>  
`| * | `[c371dda1c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c371dda1c) fix énoncé tordu ;-) (+eslint fixes) (2021-08-29 20:10) <Daniel Caillibaud>  
`| * | `[c71918524](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c71918524) aj remplacement version (2021-08-29 20:09) <Daniel Caillibaud>  
`| * | `[e960f0a05](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e960f0a05) fix path loader (2021-08-29 20:09) <Daniel Caillibaud>  
`|/ /  `  
`* | `[a2444df04](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2444df04) console.log oublié (2021-08-29 18:35) <Daniel Caillibaud>  
`* |   `[1da1bb525](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1da1bb525) Merge branch 'modWebpackBabel' into 'master' (2021-08-29 16:33) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[6a658641b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a658641b) le seul js resté dans www/ rejoint les autres dans src/ (2021-08-29 18:31) <Daniel Caillibaud>  
`| * | `[6762ecdc9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6762ecdc9) On passe désormais par un loader unique pour tous nos loaders (2021-08-29 18:31) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[e737ac6ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e737ac6ce) Merge branch 'fixNomme03' into 'master' (2021-08-29 16:28) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[312667e1c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/312667e1c) Ajout import manquant Repere, variables me/stor/ds ajouté pour la lisibilité et la simplification, fix ajout des css (head non défini) (2021-08-28 10:40) <Daniel Caillibaud>  
`| * | `[2fcc8613f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2fcc8613f) fix pb typo espaces (2021-08-28 10:25) <Daniel Caillibaud>  
`* | |   `[6bfb15a1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6bfb15a1f) Merge branch 'fixProduitScalaire' into 'master' (2021-08-29 16:27) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[c7af54eda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7af54eda) On mutualise du code dans helper.js (marre de devoir corriger 5 fois de plus des trucs déjà corrigés), et on corrige toutes les erreurs eslint qui restent (plus que des warnings) (2021-08-29 17:43) <Daniel Caillibaud>  
`| * | | `[69047aedd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69047aedd) fix espace manquant dans du html (2021-08-29 16:58) <Daniel Caillibaud>  
`|/ / /  `  
`* | |   `[6d7adc7ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d7adc7ed) Merge branch 'Nouveaux_Fichiers_Annexes' into 'master' (2021-08-29 12:44) <Yves Biton>  
`|\ \ \  `  
`| * | | `[fc2086f91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc2086f91) Correction fichier annexe (2021-08-29 14:39) <Yves Biton>  
`* | | |   `[917f5fdfb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/917f5fdfb) Merge branch 'progco' into 'master' (2021-08-28 20:47) <barroy tommy>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| | | * `[b23494554](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b23494554) Draft: Le param MiseEnPagePlusClaire devient le thème oldMep (2021-08-31 20:31) <Daniel Caillibaud>  
`| | | * `[42647cb5c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42647cb5c) aj de qq regex en doc pour remplacer automatiquement les appels de addTable(conteneur, unId, monNbDeLignes, unNbDeColonnes) par addTable(conteneur, { id, nbLignes, nbColonnes }) (2021-08-31 17:56) <Daniel Caillibaud>  
`| | | * `[e42482da8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e42482da8) modifs pour une fct addTable plus générique (2021-08-31 17:50) <Daniel Caillibaud>  
`| | | * `[ae1eb8f00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae1eb8f00) rr (2021-08-30 16:38) <Tommy>  
`| | | * `[b101353fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b101353fe) rr (2021-08-30 16:36) <Tommy>  
`| | | * `[c5df54128](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5df54128) jj (2021-08-30 10:33) <Tommy>  
`| | | * `[0645fe8ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0645fe8ab) pour sauv eqeq (2021-08-29 15:58) <Tommy>  
`| | | * `[22978d171](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22978d171) pour sauv eqeq (2021-08-29 15:56) <Tommy>  
`| | |/  `  
`| |/|   `  
`| * | `[f7071a72e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7071a72e) pour co progco (2021-08-28 22:46) <Tommy>  
`|/ /  `  
`* / `[aadacaf7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aadacaf7a) pour le moment on passe les règles eslint camelcase et eqeqeq en warning, y'en a bcp trop pour le moment, plus besoin de passer de --no-verify pour les commits (2021-08-28 10:49) <Daniel Caillibaud>  
`|/  `  
`* `[5194c6cd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5194c6cd7) retag 1.0.1 | passage en prod (2021-08-27 23:18) <Daniel Caillibaud>  
`* `[2a070533a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a070533a) petites modifs sur la détection d'urls dev/prod (2021-08-27 23:08) <Daniel Caillibaud>  
`* `[90b415d05](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90b415d05) bugfix dans _initDom (introduit par 9fc12e7e) (2021-08-27 21:44) <Daniel Caillibaud>  
`*   `[88cbd8717](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88cbd8717) Merge branch 'fixProduitScalaire3pts' into 'master' (2021-08-27 19:00) <Daniel Caillibaud>  
`|\  `  
`| * `[64a2f2d6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64a2f2d6e) fix qq syntax errors + var mal déclarées + tous les pbs eslint + simplifications (2021-08-26 14:54) <Daniel Caillibaud>  
`* |   `[67dcd8581](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67dcd8581) Merge branch 'rmqsPad' into 'master' (2021-08-27 18:58) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[b9d33f811](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9d33f811) Récupération des modifs de Rémi (dans le commit précédent) (2021-08-26 12:58) <Daniel Caillibaud>  
`| * | `[ce07d016f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce07d016f) Merge branch 'master' into rmqsPad (2021-08-26 12:54) <Daniel Caillibaud>  
`| |\| `  
`| * | `[7ff680a14](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ff680a14) outil geometrie (2021-08-26 11:54) <Rémi Deniaud>  
`* | |   `[b1b5bd56f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1b5bd56f) Merge branch 'ptitsfix' into 'master' (2021-08-27 18:58) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[3c1d1a46d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c1d1a46d) simplification et un peu de clarification (2021-08-27 18:09) <Daniel Caillibaud>  
`| * | | `[e386399ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e386399ff) simplification et un peu de clarification (2021-08-27 18:01) <Daniel Caillibaud>  
`| * | | `[dd4806f7b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd4806f7b) code inutile viré (2021-08-27 17:46) <Daniel Caillibaud>  
`| * | | `[a24bde2c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a24bde2c0) Ajout j3pAlert et j3pGetCssProp (et j3pAjouteListeDeroulante viré car plus utilisé par personne) (2021-08-27 17:22) <Daniel Caillibaud>  
`| * | | `[c966c8590](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c966c8590) qq simplifications + les derniers fix eslint (2021-08-27 10:36) <Daniel Caillibaud>  
`| * | | `[d2e8234a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2e8234a5) qq simplifications (2021-08-27 10:33) <Daniel Caillibaud>  
`| * | | `[288814e1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/288814e1a) blabla (2021-08-26 23:19) <Tommy>  
`* | | |   `[84556dc3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84556dc3a) Merge branch 'fixEditionParams' into 'master' (2021-08-27 18:57) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[61da1a4f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61da1a4f4) refacto export de getAdresse, au format esm comme le reste (webpack utilise le module esm pour convertir ça en cjs) (2021-08-27 20:53) <Daniel Caillibaud>  
`| * | | | `[c0c2ea410](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0c2ea410) Un peu de nettoyage (qq fixes eslint + viré j3pPositionneBouton & j3pPositionneTag) (2021-08-27 19:56) <Daniel Caillibaud>  
`| * | | | `[80f666109](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80f666109) abandon de l'import de j3pPositionneTag (on était la seule section à mal l'utiliser, ça fix un bug au passage) (2021-08-27 19:38) <Daniel Caillibaud>  
`| * | | | `[b23af6052](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b23af6052) typofix j3pSetProps (2021-08-27 18:42) <Daniel Caillibaud>  
`| * | | | `[9ec90334b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ec90334b) bugfix édition des paramètres (affichage de la description à droite) (2021-08-27 18:41) <Daniel Caillibaud>  
`|/ / / /  `  
`* | | | `[76b204109](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76b204109) aj d'un max de 50 fichiers à passer à eslint en pre-commit (+bcp de commentaires dans ce hook pre-commit) (2021-08-27 09:47) <Daniel Caillibaud>  
`* | | |   `[00e66480d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00e66480d) Merge branch 'fixNomme03' into 'master' (2021-08-27 07:44) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * \ \ \   `[4bdf69f3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4bdf69f3b) Merge branch 'master' into fixNomme03 (2021-08-27 09:43) <Daniel Caillibaud>  
`| |\ \ \ \  `  
`| * | | | | `[94c3d7b53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94c3d7b53) essai tom2 (2021-08-24 09:58) <Tommy>  
`| | |_|/ /  `  
`| |/| | |   `  
`* | | | |   `[cfa109d65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfa109d65) Merge branch 'betterInitDom' into 'master' (2021-08-27 07:10) <Yves Biton>  
`|\ \ \ \ \  `  
`| |_|/ / /  `  
`|/| | | |   `  
`| * | | | `[9fc12e7e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9fc12e7e5) factorisation de code et nettoyage dans l'initialisation du dom par le modèle (2021-08-26 19:38) <Daniel Caillibaud>  
`* | | | |   `[90eee1b8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90eee1b8c) Merge branch 'cleanSquelettemtg32_Calc_Param_MathQuill' into 'master' (2021-08-27 06:15) <Yves Biton>  
`|\ \ \ \ \  `  
`| |_|_|/ /  `  
`|/| | | |   `  
`| * | | | `[ad1f6eb2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad1f6eb2d) rectif d'erreurs mises dans le commit précédent (validation) (2021-08-26 20:05) <Daniel Caillibaud>  
`| * | | | `[19744c2ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19744c2ad) rectif d'erreurs mises dans le commit précédent (2021-08-26 19:57) <Daniel Caillibaud>  
`| * | | | `[3318d5f5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3318d5f5a) fix de tous les pbs eslint, fusion des appels de construitStructurePage et Dimensions, un peu de simplification du code (2021-08-26 16:59) <Daniel Caillibaud>  
`| |/ / /  `  
`* | | | `[a6b34221e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6b34221e) fix param openPage qui a disparu dans webpack-dev-server 4 (utilisé si on restreint à une section) (2021-08-26 19:47) <Daniel Caillibaud>  
`* | | |   `[288b79efe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/288b79efe) Merge branch 'cleanUnusedImports' into 'master' (2021-08-26 17:45) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[4da86d7d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4da86d7d6) Import de Parcours inutile dans les sections (2021-08-26 19:44) <Daniel Caillibaud>  
`|/ / /  `  
`* | / `[95f45dd7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95f45dd7f) essai de fix du pre-commit sous windows (2021-08-26 14:59) <Daniel Caillibaud>  
`| |/  `  
`|/|   `  
`* |   `[4a2afd551](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a2afd551) Merge branch 'betterLoadAll' into 'master' (2021-08-26 09:42) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[3967d8927](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3967d8927) Amélioration de loadAll (log mieux formaté) (2021-08-26 11:41) <Daniel Caillibaud>  
`|/ /  `  
`* | `[a7972c1fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7972c1fa) fix déploiement de certains fichiers de log en prod (2021-08-26 10:27) <Daniel Caillibaud>  
`* | `[0997d6798](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0997d6798) màj urls de test (2021-08-25 21:58) <Daniel Caillibaud>  
`* |   `[45522bd8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45522bd8c) Merge branch 'Fix_Fichiers_Annexes' into 'master' (2021-08-25 17:46) <Yves Biton>  
`|\ \  `  
`| * | `[b1552b788](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1552b788) Correction fichier annexe (2021-08-25 19:43) <Yves Biton>  
`|/ /  `  
`* |   `[666b5807f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/666b5807f) Merge branch 'fixTestsMochaOnWindows' into 'master' (2021-08-25 16:37) <Yves Biton>  
`|\ \  `  
`| * | `[6496d8106](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6496d8106) enfin un fix pour les tests unitaires qui plantaient sous windows (2021-08-25 17:56) <Daniel Caillibaud>  
`* | |   `[8211e2f64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8211e2f64) Merge branch 'fixenlettres' into 'master' (2021-08-25 16:15) <Xavier Jerome>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[5c920b469](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c920b469) Coquille dans nombreEnLettres (2021-08-25 17:58) <Xavier JEROME>  
`|/ /  `  
`* | `[192fc2fc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/192fc2fc7) bugfix appel incorrect de convertirEnLettres (2021-08-25 13:33) <Daniel Caillibaud>  
`* | `[eebd66ba9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eebd66ba9) fix appel foireux de j3pAffiche (2021-08-25 10:21) <Daniel Caillibaud>  
`* | `[76dc002d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76dc002d9) fix bugs introduit en 95c2c33e (2021-08-25 08:26) <Daniel Caillibaud>  
`* | `[e8e3d9e8f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8e3d9e8f) àj des params vscode à .gitignore (2021-08-25 07:12) <Daniel Caillibaud>  
`* | `[88c42cc15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88c42cc15) aj de .rsync_dev2prod.exclude (2021-08-25 07:09) <Daniel Caillibaud>  
`* |   `[fdd7f3397](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fdd7f3397) Merge branch 'bugfix' into 'master' (2021-08-25 05:06) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[95c2c33e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95c2c33e7) commentaires obsolètes virés (eslint global pour des trucs désormais importés, use strict inutile car d'office en version module, référence à des iife disparues dans cette version module, etc.) (2021-08-25 07:02) <Daniel Caillibaud>  
`| * | `[87859160f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87859160f) fct redéclarées localement ET importées virées (après avoir vérifié qu'elles étaient rigoureusement identique à l'import) (2021-08-24 18:15) <Daniel Caillibaud>  
`| * | `[4f730cbd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f730cbd0) fix plantage au drop d'une ressource j3p sans graphe (2021-08-24 17:43) <Daniel Caillibaud>  
`| * | `[b78813961](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b78813961) commentaires (2021-08-24 17:42) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[fe0e11366](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe0e11366) Merge branch 'enlettres' into 'master' (2021-08-24 14:48) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[5c7ea25bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c7ea25bb) qq fixes eslint (2021-08-24 16:46) <Daniel Caillibaud>  
`* | | `[ba2a77ff3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba2a77ff3) Merge branch 'enlettres' into 'master' (2021-08-24 14:45) <Xavier Jerome>  
`|\| | `  
`| * |   `[91af3a17f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91af3a17f) Merge branch 'master' into enlettres (2021-08-24 16:38) <Daniel Caillibaud>  
`| |\ \  `  
`| * | | `[58c7fe21b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58c7fe21b) enlettres (2021-08-24 13:10) <Xavier JEROME>  
`| | |/  `  
`| |/|   `  
`* | |   `[3eb4c0c46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3eb4c0c46) Merge branch 'varGlobales' into 'master' (2021-08-24 14:33) <Xavier Jerome>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[7f757925c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f757925c) encore qq fixes eslint (plus de undef à part j3p et \$ (pas très grave car ils sont globaux, même si c'est mieux de les déclarer dans chaque section (2021-08-24 16:25) <Daniel Caillibaud>  
`| * | `[1594e9f97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1594e9f97) un peu de simplification du code et qq fixes eslint (2021-08-24 16:03) <Daniel Caillibaud>  
`| * |   `[de2c80441](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de2c80441) Merge branch 'master' into varGlobales (2021-08-24 15:42) <Daniel Caillibaud>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[9888893ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9888893ea) Merge branch 'blindageConstructeurs' into 'master' (2021-08-24 11:40) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[c8c37803e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8c37803e) fix appels de BoutonStyleMathquill sans new (2021-08-24 13:38) <Daniel Caillibaud>  
`| * | | `[83a5eb1f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83a5eb1f2) fix appels de Paint sans new (2021-08-24 13:33) <Daniel Caillibaud>  
`| * | | `[35eb1243c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35eb1243c) blindage des constructeurs (throw si appelés sans new) (2021-08-24 13:25) <Daniel Caillibaud>  
`|/ / /  `  
`* | | `[0de1abee3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0de1abee3) migration webpack-dev-server v3 => v4 (les deux avec webpack5) (2021-08-24 11:13) <Daniel Caillibaud>  
`* | | `[ff958995b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff958995b) ajout de commentaires sur les dépendances, et màj deps (2021-08-24 10:45) <Daniel Caillibaud>  
`* | | `[740becbb9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/740becbb9) légère modif du html de la home (pour la sonde qui check) (2021-08-24 00:39) <Daniel Caillibaud>  
`* | |   `[74a577bea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/74a577bea) Merge branch 'bugFixChangementRang' (2021-08-23 20:18) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[60184b1bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/60184b1bc) bugfix du changement de rang, on retire les snn quand on regarde les suivants/précédents (et on décale d'un cran supplémentaire en cas d'échange avec un snn) (2021-08-23 20:17) <Daniel Caillibaud>  
`|/ / /  `  
`* | | `[69c9fcdb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69c9fcdb8) viré les derniers innerHTML += qui cassent tout (2021-08-23 18:22) <Daniel Caillibaud>  
`* | |   `[ae7a488b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae7a488b8) Merge branch 'bugfix' (2021-08-23 16:44) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[9d7755575](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d7755575) fix du listener perdu sur sansCondition, dans le cas des branchements qui bouclent (2021-08-23 16:44) <Daniel Caillibaud>  
`|/ / /  `  
`* | |   `[3899cf9e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3899cf9e9) Merge branch 'bugfix' (2021-08-23 15:09) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[c7c5364cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7c5364cc) bugfix pour les cas où nn ou snn ne sont pas des string (ça arrive que ce soit des number) (2021-08-23 15:08) <Daniel Caillibaud>  
`| * | | `[fcc2296e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fcc2296e0) j3pAddElt accepte d'avoir props en 3e param (avec content en 4e ou pas) (2021-08-23 14:49) <Daniel Caillibaud>  
`| * | | `[0399b02f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0399b02f5) fix dialogue branchements pour section quali (2021-08-23 14:48) <Daniel Caillibaud>  
`|/ / /  `  
`* | |   `[aba2a2720](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aba2a2720) Merge branch 'fixSuppressionBranchement' (2021-08-23 13:56) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[8ff972849](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8ff972849) modif mineure de css (2021-08-23 13:56) <Daniel Caillibaud>  
`| * | | `[0c7a4577a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c7a4577a) fix pb de sans condition score qui activait pas l'input (2021-08-23 13:54) <Daniel Caillibaud>  
`| * | | `[5e59ddbd8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e59ddbd8) aj des fct de debug pour tracker les modifs du dom (2021-08-23 13:54) <Daniel Caillibaud>  
`| * | | `[98f19c8a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98f19c8a7) bugfix suppression branchement (faut fermer la boite de dialogue, sinon un clic sur valider plante puisque le branchement a disparu) (2021-08-23 12:19) <Daniel Caillibaud>  
`|/ / /  `  
`* | | `[cd1cece56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd1cece56) eslint fixes (2021-08-23 11:57) <Daniel Caillibaud>  
`* | | `[3023feed8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3023feed8) restait un j3pRepere (2021-08-23 11:28) <Daniel Caillibaud>  
`* | | `[d033a08e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d033a08e8) retag 1.0.0 | doc & commentaires (2021-08-23 11:10) <Daniel Caillibaud>  
`* | | `[f23698828](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f23698828) j3pRepere => Repere (2021-08-23 10:16) <Daniel Caillibaud>  
`* | | `[5e9821bd6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e9821bd6) toilettage (2021-08-23 09:49) <Daniel Caillibaud>  
`* | | `[9746856a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9746856a7) eslint --fix (2021-08-23 09:17) <Daniel Caillibaud>  
`* | | `[30efc1d2c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30efc1d2c) fix retour à la ligne au milieu d'une string (2021-08-23 09:17) <Daniel Caillibaud>  
`* | |   `[55dc0bb34](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55dc0bb34) Merge branch 'bugfix' (2021-08-23 07:33) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[86f16ab00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86f16ab00) console.log virés (2021-08-23 07:32) <Daniel Caillibaud>  
`| * | | `[b6d50026e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6d50026e) restait un localhost de test (2021-08-23 07:31) <Daniel Caillibaud>  
`| * | | `[0cc4d0ad4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0cc4d0ad4) getParams retourne {} pour une section fin (en râlant) (2021-08-23 07:30) <Daniel Caillibaud>  
`|/ / /  `  
`* | | `[95076cf30](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95076cf30) retag 1.0.0 (2021-08-22 23:42) <Daniel Caillibaud>  
`* | |   `[3d72d44e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d72d44e3) Merge branch 'bugDoubleBranch' into 'master' (2021-08-22 21:41) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[9db07c353](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9db07c353) bugfix édition de paramètres (2021-08-22 23:39) <Daniel Caillibaud>  
`| * | | `[5016d30aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5016d30aa) bugFix, lorsqu'il n'y a pas de params, le dernier branchement ne se retrouve plus en params (2021-08-22 23:38) <Daniel Caillibaud>  
`| * | | `[6ba1d8246](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ba1d8246) test et modale des params ok, branchement en double se retrouve fusionné dans les params (2021-08-22 16:36) <Daniel Caillibaud>  
`| * | | `[a4ab645ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a4ab645ee) extract css aussi en mode devServer (2021-08-21 19:06) <Daniel Caillibaud>  
`|/ / /  `  
`* | |   `[419acd96b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/419acd96b) Merge branch 'bugFix' into 'master' (2021-08-21 13:46) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[fd172f270](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd172f270) le preloader ajoute la bonne css tout seul (2021-08-21 15:45) <Daniel Caillibaud>  
`| * | | `[308d882dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/308d882dd) amélioration doc (2021-08-21 15:43) <Daniel Caillibaud>  
`|/ / /  `  
`* | |   `[1fbcc77c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1fbcc77c5) Merge branch 'improveDevServer' into 'master' (2021-08-21 13:41) <Daniel Caillibaud>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[04fa58e94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04fa58e94) préciser SECTION= limite à la section et au player (sinon editGraphe charge quand même toutes les sections) (2021-08-21 15:32) <Daniel Caillibaud>  
`|/ /  `  
`| * `[a09135a90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a09135a90) Variables globales (2021-08-24 12:34) <Xavier JEROME>  
`| * `[ba5e17b53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba5e17b53) Variables globales (2021-08-24 12:32) <Xavier JEROME>  
`| * `[7c6046aac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c6046aac) Variables globales (2021-08-24 12:14) <Xavier JEROME>  
`|/  `  
`* `[2561598dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2561598dc) bugfix preloader (2021-08-21 00:16) <Daniel Caillibaud>  
`*   `[bc9822bc2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc9822bc2) Merge branch 'mergeEditor' into 'master' (2021-08-20 19:01) <Daniel Caillibaud>  
`|\  `  
`| * `[f6e3e07ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6e3e07ee) adresseInit viré (2021-08-20 18:06) <Daniel Caillibaud>  
`| * `[414043978](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/414043978) WIP première intégration OK de showParcours et editGraphe (2021-08-20 16:54) <Daniel Caillibaud>  
`| * `[80a489165](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80a489165) typofix (2021-08-20 09:56) <Daniel Caillibaud>  
`| * `[290b5ac55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/290b5ac55) fixes eslint (2021-08-20 09:43) <Daniel Caillibaud>  
`| * `[d372b1351](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d372b1351) bugsnag fix 611eacf4d4399b0007169ff3 (2021-08-20 09:13) <Daniel Caillibaud>  
`| * `[e02d53415](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e02d53415) fixes eslint (2021-08-19 21:01) <Daniel Caillibaud>  
`| * `[d16ed6f6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d16ed6f6e) 1er import brut de sesaeditgraphe (2021-08-19 20:45) <Daniel Caillibaud>  
`* |   `[7d147fae3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d147fae3) Merge branch 'fixLimAlign' into 'master' (2021-08-20 08:48) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[8dbae000e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8dbae000e) Correction css pour alignement limites. (2021-08-20 10:39) <Yves Biton>  
`|/ /  `  
`* | `[5a179e31f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a179e31f) modifs de mathquill qui manquaient dans la merge request précédente (2021-08-20 09:33) <Daniel Caillibaud>  
`* |   `[65ed35f52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65ed35f52) Merge branch 'fixLim2' into 'master' (2021-08-20 07:20) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[4801869d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4801869d7) Fix lim2 (2021-08-20 07:20) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[01981c964](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01981c964) Merge branch 'bugFix' into 'master' (2021-08-20 07:14) <Daniel Caillibaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[ecb920dcc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecb920dcc) bugsnag fix 611eacf4d4399b0007169ff3 (2021-08-20 09:13) <Daniel Caillibaud>  
`|/  `  
`* `[8d7082dcd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d7082dcd) restait un /fichiersannexes/ (2021-08-19 16:30) <Daniel Caillibaud>  
`* `[a3dab5413](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3dab5413) annulation modif temporaire qui avait été commitée par erreur (2021-08-19 16:08) <Daniel Caillibaud>  
`* `[15c685f42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15c685f42) répercussion des dernières modifs du svn j3p qui n'avaient pas été importées dans sesaparcours (2021-08-19 16:07) <Daniel Caillibaud>  
`* `[1848f72f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1848f72f0) qq fixes (2021-08-19 15:48) <Daniel Caillibaud>  
`* `[be0e84d76](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be0e84d76) aj de sectionsquelettemtg32_Systeme_33.js qui n'avait pas été incorporé à la mise en prod (2021-08-19 15:38) <Daniel Caillibaud>  
`* `[a1949b74e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1949b74e) ajout du test de j3pGetRandomInt (et fix d'un petit bug au passage sur les bornes non entières) (2021-08-19 15:05) <Daniel Caillibaud>  
`* `[24f1cadc6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24f1cadc6) eslint fixes (2021-08-19 14:05) <Daniel Caillibaud>  
`* `[40b7a5460](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40b7a5460) fix test de l'url en cas d'inclusion en iframe (top n'est pas accessible) (2021-08-19 13:51) <Daniel Caillibaud>  
`* `[0472ad8c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0472ad8c1) modifs de forme (2021-08-19 13:37) <Daniel Caillibaud>  
`* `[45c7193ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45c7193ef) modif pour le test local d'une seule section, màj de la doc (2021-08-19 13:37) <Daniel Caillibaud>  
`*   `[519d16d53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/519d16d53) Merge branch 'fixVector' into 'master' (2021-08-19 11:31) <Daniel Caillibaud>  
`|\  `  
`| * `[6826050ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6826050ee) Modification de MathQuill par Yves pour un alignement correct des flèches de vecteurs. (2021-08-19 11:52) <Yves Biton>  
`|/  `  
`* `[25f248c83](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25f248c83) typofix (2021-08-19 10:49) <Daniel Caillibaud>  
`* `[aa3d1c121](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa3d1c121) harmonisation du marquage des modifs de mathquill (2021-08-19 10:35) <Daniel Caillibaud>  
`* `[d3121912c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3121912c) Fix d'Yves pour les bouts de texte avec widehat qui se retrouvaient plus bas que le reste sur la ligne de texte. (2021-08-19 10:35) <Daniel Caillibaud>  
`* `[7c8e7a739](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c8e7a739) fix erreur remontée par bugsnag (611d1d8ea309df0007a2abff) (2021-08-19 10:27) <Daniel Caillibaud>  
`* `[51f0394c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51f0394c8) fix pre-commit hook pour windows (2021-08-19 10:04) <Daniel Caillibaud>  
`* `[9b26d0118](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b26d0118) faut plus utiliser scripts/legacyImport.zsh (2021-08-19 10:00) <Daniel Caillibaud>  
`* `[1d044ac51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d044ac51) harmonisation des noms des fcts j3p (camelCase pour toutes, une majuscule après le préfixe j3p) (2021-08-19 09:30) <Daniel Caillibaud>  
`* `[3b8c0a898](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b8c0a898) fixes eslint (2021-08-19 08:20) <Daniel Caillibaud>  
`* `[42a4623bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42a4623bf) plus de lint sur tout src en pre-push (c'est fait en pre-commit sur les fichiers concernés seulement) (2021-08-18 18:55) <Daniel Caillibaud>  
`* `[4042e6129](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4042e6129) fix eslint (2021-08-18 18:53) <Daniel Caillibaud>  
`* `[4193b6fac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4193b6fac) modif de forme (2021-08-18 18:52) <Daniel Caillibaud>  
`* `[d2eb5e630](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2eb5e630) màj des commentaires de sections/modele.js (2021-08-18 18:51) <Daniel Caillibaud>  
`* `[0a7b99742](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a7b99742) on applique aussi eslint à legacy maintenant qu'on peut le modifier (2021-08-18 18:51) <Daniel Caillibaud>  
`* `[bfc89b28f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bfc89b28f) màj url parcours.sesamath.dev => j3p.sesamath.dev (2021-08-18 16:27) <Daniel Caillibaud>  
`* `[c43ce40d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c43ce40d6) manquait les fichiers déplacés :-/ (2021-08-18 16:08) <Daniel Caillibaud>  
`* `[7345631f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7345631f5) On sort fichiersannexes de sections/ pour essayer de régler les pbs de RAM de webpack (2021-08-18 16:03) <Daniel Caillibaud>  
`* `[b3bf84727](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3bf84727) qq modifs pour le css principal qui ne veut pas se charger tout seul… (2021-08-18 15:35) <Daniel Caillibaud>  
`* `[e303b482c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e303b482c) fix preload (2021-08-17 15:06) <Daniel Caillibaud>  
`* `[c00c06e78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c00c06e78) fix devServer (2021-08-17 13:14) <Daniel Caillibaud>  
`* `[71e315586](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/71e315586) màj mocha (2021-08-17 12:49) <Daniel Caillibaud>  
`* `[140eeab7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/140eeab7a) fix eslint (2021-08-17 12:42) <Daniel Caillibaud>  
`* `[e772bed6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e772bed6a) aj des hooks husky (2021-08-17 12:42) <Daniel Caillibaud>  
`* `[1a238e03f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a238e03f) màj deps, dont passage à webpack5 (2021-08-17 12:42) <Daniel Caillibaud>  
`* `[7a7cca866](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a7cca866) aj preloader (2021-08-17 12:42) <Daniel Caillibaud>  
`* `[28a80921e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28a80921e) màj j3p (2021-08-17 12:09) <Compte unix pour site j3p>  
`* `[f438824ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f438824ae) màj j3p (2021-08-17 10:09) <Compte unix pour site j3p>  
`* `[75a4cf517](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75a4cf517) màj j3p (2021-08-16 10:42) <Compte unix pour site j3p>  
`* `[78ed9511e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78ed9511e) màj j3p (2021-08-13 11:15) <Compte unix pour site j3p>  
`* `[f34ef1f0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f34ef1f0f) màj j3p (2021-08-06 18:53) <Compte unix pour site j3p>  
`* `[720f06c5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/720f06c5e) màj j3p (2021-08-06 15:59) <Compte unix pour site j3p>  
`* `[4df84f264](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4df84f264) maj j3p (2021-08-06 13:55) <Daniel Caillibaud>  
`* `[ed6a45805](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed6a45805) aj de jpg dans j3pcss (2021-08-06 13:42) <Daniel Caillibaud>  
`* `[dc08bdc53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc08bdc53) j3p.html?rid=… => index.html?rid=… (2021-07-30 18:15) <Daniel Caillibaud>  
