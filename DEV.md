Développement
=============

Évolutions
----------
Depuis début 2023, il est inutile d’affecter donneesSection et d’appeler surcharge, c'est automatiquement fait avant le premier appel de la section d’après les paramètres du graphe et les valeurs par défaut de l’export `params` de la section.
Le seul cas où l’appel de surcharge peut être utile, c'est pour imposer un param initialisé par le modèle (limite|nbrepetitions|nbetapes|nbchances) qui ne serait pas paramétrable et différent de sa valeur par défaut.

Par ex pour imposer nbetapes = 3 sans le rendre paramétrable, il faut appeler `this.surcharge({ nbetapes: 3 })`

Il faut donc virer tous les `Donnees` ou `getDonnees`, en vérifiant avant que ce qu'elles retourne est bien dans les valeurs par défaut des params (et adapter sinon, en ajoutant des constantes par ex)

Regexp utiles
-------------

Qq regex pratiques pour faire évoluer le code

- remplacer indexOf par includes quand c'est ce qu'on voulait tester
([\d\w.]+)\.indexOf\(([^)]+\)) !==? -1    => $1.includes($2)
([\d\w.]+)\.indexOf\(([^)]+)\) ===? -1      => !$1.includes($2)

- remplacer les appels de fonction qui changent de signature, dans cet exemple remplacer
```javascript
addTable(conteneur, unId, monNbDeLignes, unNbDeColonnes)
```
par
```javascript
addTable(conteneur, { id: unId, nbLignes: monNbDeLignes, nbColonnes: unNbDeColonnes })
```
rechercher `addTable\(([^,]+), ([^,]+), ([^,]+), ([^)]+)\)` que l’on remplace par `addTable($1, { id: $2, nbLignes: $3, nbColonnes: $4 })`

- passer les variables en camelCase, attention à bien vérifier chaque remplacement, il ne faut en général pas le faire pour les propriétés (définies dans les paramètres, ou par d’autres fichiers js qui retournent des objets avec des propriétés en snake_case)
  `([^.])([a-zA-Z0-9]+)_([a-zA-Z0-9]+)` => ```$1$2\u$3` (chercher hors string litteral pour ne pas modifier les noms des paramètres ou les id)

- remplacer du j3pDiv par j3pAddElt
  - pour les cas simples (div vide) remplacer `j3pDiv\(([^,]+), ([^,)]+)(, '')?\)` par `j3pAddElt($1, 'div', '', { id: $2 })`
  - les options en 2e param, virer la ligne des styles manuellement (dans 99% des cas elle ne sert à rien) puis remplacer `j3pDiv\(([^,]+), \{(?:[^}]|\n)*id: ([^,})]+)[,}][^)]+\)` par `j3pAddElt($1, 'div', '', { id: $2 })` (le faire en contrôlant chaque occurence dans le fichier, puis retester la section pour vérifier qu'il n’y a pas de pbs de positionnement ou de marges, ajouter du style si besoin)

Migrer vers typescript
----------------------

Le core v2 (lib/core vs legacy/core) est entièrement en [typescript](https://www.typescriptlang.org/fr/)

On va cependant conserver le code legacy en js (sauf les fichiers qu'on voudra migrer), mais ça ne l’empêche pas d’importer des classes / fonctions en ts, et l’ensemble du code js|ts profite de ces vérifications de type (faites à partir du jsdoc pour les fichiers js)

=> c'est important d’avoir du jsdoc précis et complet pour toutes les fonctions

Les sections v2 pourront être écrites en js ou en ts, au choix du développeur, même si ts serait à privilégier.

Quelques pages de docs bien utiles
* [démarrer en ts](https://www.typescriptlang.org/fr/docs/handbook/typescript-in-5-minutes.html)
* [jsdoc utilisé par ts](https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html)
* [migrer de js à ts](https://www.typescriptlang.org/docs/handbook/migrating-from-javascript.html)

Tooling
-------

Le mix js/ts complique pas mal le tooling

### tests (mocha|vitest)

Après des tonnes d’essais, impossible de s'en sortir proprement avec mocha (notamment les alias). C'était @babel/register qui se chargeait de définir les alias dans test/initMocha.js, mais il est cjs only, et ça posait d’autres pbs.

=> On a finalement remplacé mocha par vitest, tout devient bcp plus simple (et y’a quasiment pas eu de test à modifier pour que ça fonctionne car vitest utilise aussi chai)

### documentation

Cf [jsdoc/README](jsdoc/README)
